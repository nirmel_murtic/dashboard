// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'client/bower_components/jquery/dist/jquery.js',
      'client/bower_components/angular/angular.js',
      'client/bower_components/angular-mocks/angular-mocks.js',
      'client/bower_components/angular-resource/angular-resource.js',
      'client/bower_components/angular-cookies/angular-cookies.js',
      'client/bower_components/angular-sanitize/angular-sanitize.js',
      'client/bower_components/angular-route/angular-route.js',
      'client/bower_components/angular-animate/angular-animate.js',
      'client/bower_components/lodash/lodash.js',
      'client/bower_components/angular-translate/angular-translate.js',
      'client/bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
      'http://www.google.com/recaptcha/api/js/recaptcha_ajax.js',
      'https://js.recurly.com/v3/recurly.js',
      'client/bower_components/angular-ui-grid/ui-grid.js',
      'client/bower_components/angular-recaptcha/release/angular-recaptcha.js',
      'client/bower_components/ng-token-auth/dist/ng-token-auth.js',
      'client/bower_components/angular-ui-router/release/angular-ui-router.js',
      'client/bower_components/ng-simplePagination/simplePagination.js',
      'client/bower_components/angular-facebook/lib/angular-facebook.js',
      'client/bower_components/ngDialog/js/ngDialog.js',
      'client/bower_components/angucomplete-alt/angucomplete-alt.js',
      'client/bower_components/ng-file-upload/angular-file-upload.js',
      'client/bower_components/ng-file-upload-shim/angular-file-upload-shim.js',
      'client/bower_components/moment/moment.js',
      'client/bower_components/angular-moment/angular-moment.js',
      'client/bower_components/angular-messages/angular-messages.js',
      'client/bower_components/angular-linkify/angular-linkify.js',
      'client/bower_components/angular-dragdrop/src/angular-dragdrop.js',
      'client/bower_components/jquery-ui/jquery-ui.js',
      'client/bower_components/ng-atp/dist/js/ng-atp-bundle.js',
      'client/bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
      'client/bower_components/jwt-decode/build/jwt-decode.js',
      'client/bower_components/d3/d3.js',
      'client/bower_components/react/react.js',
      'client/bower_components/angular-cookie/angular-cookie.js',
      'client/bower_components/quick-ng-repeat/quick-ng-repeat.js',
      'client/bower_components/zeroclipboard/dist/ZeroClipboard.js',
      'client/bower_components/ng-clip/src/ngClip.js',
      'client/bower_components/angular-loading-bar/build/loading-bar.js',
      'client/bower_components/angular-tooltips/src/js/angular-tooltips.js',
      'client/bower_components/ng-slider/dist/ng-slider.min.js',
      'client/bower_components/uri.js/src/SecondLevelDomains.js',
      'client/bower_components/uri.js/src/URI.js',
      'client/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
      'client/bower_components/angular-awesome-slider/dist/angular-awesome-slider.js',
      'client/bower_components/bootstrap-daterangepicker/daterangepicker.js',
      'client/app/app.js',
      'client/app/**/*.js',
      'client/components/**/*.js',
      'client/app/**/*.jade',
      'client/components/**/*.jade',
      '.tmp/templates.js'
    ],

    preprocessors: {
      '**/*.jade': 'ng-jade2js',
      '**/*.coffee': 'coffee',
      '**/app/**/*.js': ['coverage'],
      '**/components/**/*.js': ['coverage']
      // '**/app/js/modules/*/*.js' : 'coverage',
      // '**/app/js/services/*/*.js' : 'coverage'
    },

    ngJade2JsPreprocessor: {
      stripPrefix: 'client/'
    },

    coverageReporter: {
        type: 'html',
        dir: 'test/reports/unit/coverage'
    },

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],

    // Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-mocha',
      'karma-coverage',
      'karma-spec-reporter'
    ],
    reporters : ['spec', 'coverage'],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
