// Generated on 2014-10-01 using generator-angular-fullstack 2.0.13
'use strict';

module.exports = function (grunt) {
  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    replace: 'grunt-text-replace',
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn',
    protractor: 'grunt-protractor-runner',
    injector: 'grunt-injector',
    configureProxies: 'grunt-connect-proxy'
  });

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Configurable paths for the application
  var clientConfig = {
    client: require('./bower.json').appPath || 'client',
    dist: 'dist',
    environment: 'local'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    pkg: grunt.file.readJSON('package.json'),
    yeoman: clientConfig,
    watch: {
      jsx : {
        files: [
          '<%= yeoman.client %>/{app,components}/**/*.jsx'
        ],
        tasks: ['react']
      },
      injectJS: {
        files: [
          '<%= yeoman.client %>/{app,components}/**/*.js',
          '!<%= yeoman.client %>/{app,components}/**/*.spec.js',
          '!<%= yeoman.client %>/{app,components}/**/*.mock.js',
          '!<%= yeoman.client %>/app/app.js'],
        tasks: ['injector:scripts']
      },
      injectCss: {
        files: [
          '<%= yeoman.client %>/{app,components}/**/*.css'
        ],
        tasks: ['injector:css']
      },
      jsTest: {
        files: [
          '<%= yeoman.client %>/{app,components}/**/*.spec.js',
          '<%= yeoman.client %>/{app,components}/**/*.mock.js'
        ],
        tasks: ['newer:jshint:all', 'karma']
      },
      injectLess: {
        files: [
          '<%= yeoman.client %>/{app,components}/**/*.less'],
        tasks: ['injector:less']
      },
      less: {
        files: [
          '<%= yeoman.client %>/{app,components}/**/*.less'],
        tasks: ['less', 'autoprefixer']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        files: [
          '{.tmp,<%= yeoman.client %>}/{app,components}/**/*.css',
          '{.tmp,<%= yeoman.client %>}/{app,components}/**/*.html',
          '{.tmp,<%= yeoman.client %>}/{app,components}/**/*.js',
          '!{.tmp,<%= yeoman.client %>}{app,components}/**/*.spec.js',
          '!{.tmp,<%= yeoman.client %>}/{app,components}/**/*.mock.js',
          '<%= yeoman.client %>/assets/images/{,*//*}*.{png,jpg,jpeg,gif,webp,svg}',
          '<%= yeoman.client %>/{,*/}*.html'
        ],
        options: {
          livereload: true
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '<%= yeoman.client %>/.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        '<%= yeoman.client %>/{app,components}/**/*.js',
        '!<%= yeoman.client %>/{app,components}/**/*.spec.js',
        '!<%= yeoman.client %>/{app,components}/**/*.mock.js'
      ],
      git : [],
      test: {
        src: [
          '<%= yeoman.client %>/{app,components}/**/*.spec.js',
          '<%= yeoman.client %>/{app,components}/**/*.mock.js'
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*',
            '!<%= yeoman.dist %>/Procfile'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/',
          src: '{,*/}*.css',
          dest: '.tmp/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      target: {
        src: '<%= yeoman.client %>/index.html',
        ignorePath: '<%= yeoman.client %>/',
        exclude: [/bootstrap-sass-official/, /bootstrap.js/, '/json3/', '/es5-shim/', /bootstrap.css/, /font-awesome.css/, /jquery\.URI/]
      }
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= yeoman.dist %>/public/{,*/}*.js',
            '<%= yeoman.dist %>/public/{,*/}*.css',
            '<%= yeoman.dist %>/public/app/**/{,*/}*labels.*.json',
            '<%= yeoman.dist %>/public/assets/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= yeoman.dist %>/public/assets/fonts/*'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: ['<%= yeoman.client %>/index.html'],
      options: {
        dest: '<%= yeoman.dist %>/public'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/public/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/public/{,*/}*.css'],
      js: ['<%= yeoman.dist %>/public/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>/public/app/main/align/support',
          '<%= yeoman.dist %>/public/app/main/align',
          '<%= yeoman.dist %>/public/app/main/act',
          '<%= yeoman.dist %>/public/app/main/analyze',
          '<%= yeoman.dist %>/public/app/main/library',
          '<%= yeoman.dist %>/public/app/main',
          '<%= yeoman.dist %>/public/app/anonymous',
          '<%= yeoman.dist %>/public/app',
          '<%= yeoman.dist %>/public',
          '<%= yeoman.dist %>/public/assets/images'
        ],
        // This is so we update image references in our ng-templates
        patterns: {
          js: [
            [/(app\/.*?\.labels\..*?\.json)/gm, 'Update the JS to reference our revved labels'],
            [/(assets\/images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images'],
            [/(assets\/images\/navbar\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images']
          ],
          css: [
            [/(assets\/images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the CSS to reference our revved images'],
            [/(ui-grid\.(?:svg|woff|ttf|eot))/gm, 'Update CSS of angular-ui-grid to reference correct fonts urls', null, function(fontFile) {
              return '/fonts/' + fontFile;
            }]
          ]
        }
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.client %>/assets/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/public/assets/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.client %>/assets/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/public/assets/images'
        }]
      }
    },

    // Allow the use of non-minsafe AngularJS files. Automatically makes it
    // minsafe compatible so Uglify does not destroy the ng references
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat',
          src: '*/**.js',
          dest: '.tmp/concat'
        }]
      }
    },

    // Package all the html partials into a single javascript payload
    ngtemplates: {
      options: {
        // This should be the name of your apps angular module
        module: 'common',
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: false,
          removeAttributeQuotes: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: false,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        },
        usemin: 'app/app.js'
      },
      main: {
        cwd: '<%= yeoman.client %>',
        src: ['{app,components}/**/*.html'],
        dest: '.tmp/templates.js'
      },
      tmp: {
        cwd: '.tmp',
        src: ['{app,components}/**/*.html'],
        dest: '.tmp/tmp-templates.js'
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/public/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.client %>',
          dest: '<%= yeoman.dist %>/public',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'assets/images/{,*/}*.{webp}',
            'assets/images/navbar/*.svg',
            'assets/fonts/**/*',
            'assets/png/**/*',
            'assets/svg/**/*',
            'index.html',
            'sample.csv',
            'emailSample.csv',
            'phoneSample.csv',
            'registration_email_image_1.png',
            'AdWordsKeywordBulkUploadTemplate-Accomplice.csv',
            'AdWordsCreativeBulkUploadTemplate-Accomplice.csv',
            'FB Creative Bulk Upload Template - Website Goal (Accomplice).csv'
          ]
        }, {
          expand: true,
          cwd: '<%= yeoman.client %>',
          flatten: true,
          dest: '<%= yeoman.dist %>/public/fonts',
          src: ['bower_components/font-awesome/fonts/*']
         }, {
          expand: true,
          cwd: '<%= yeoman.client %>',
          flatten: true,
          dest: '<%= yeoman.dist %>/public/fonts',
          src: ['bower_components/angular-ui-grid/*.{svg,woff,ttf,eot}']
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/public/assets/images',
          src: ['generated/*']
        }, {
          expand: true,
          flatten: true,
          cwd: '<%= yeoman.client %>',
          dest: '<%= yeoman.dist %>/public/assets',
          src: ['bower_components/zeroclipboard/dist/ZeroClipboard.swf']
        }, {
          expand: true,
          dest: '<%= yeoman.dist %>',
          src: [
            'package.json'
          ]
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.client %>',
        dest: '.tmp/',
        src: ['{app,components}/**/*.css']
      },
      labels: {
        expand: true,
        cwd: '<%= yeoman.client %>',
        dest: '<%= yeoman.dist %>/public',
        src: ['{app,components}/**/*labels.*.json']
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'less',
        'copy:styles'
      ],
      test: [
        'less'
      ],
      dist: [
        'less',
        'imagemin',
        'svgmin'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },

    protractor: {
      options: {
        configFile: 'protractor.conf.js'
      },
      chrome: {
        options: {
          args: {
            browser: 'chrome'
          }
        }
      }
    },

    // Compiles Less to CSS
    less: {
      options: {
        paths: [
          '<%= yeoman.client %>/bower_components',
          '<%= yeoman.client %>/app',
          '<%= yeoman.client %>/components'
        ]
      },
      server: {
        files: {
          '.tmp/app/app.css' : '<%= yeoman.client %>/app/app.less'
        }
      }
    },

    injector: {
      options: {

      },

      // Inject configuration file separately before app.js file
      configScript: {
        options: {
          transform: function(filePath) {
            var isConfig = filePath.indexOf('config.') !== -1;
            var isCorrespondingConfig = filePath.indexOf('config.' + clientConfig.environment + '.js') !== -1;

            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');

            if(isConfig && !isCorrespondingConfig) {
              return '';
            } else {
              return '\n<script src="' + filePath + '"></script>\n';
            }
          },
          lineEnding: '',
          starttag: '<!-- injector:config -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          '<%= yeoman.client %>/index.html': [
            ['{.tmp,<%= yeoman.client %>}/app/config.*.js']
          ]
        }
      },

      // Inject application script files into index.html (doesn't include bower)
      scripts: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');

            return '<script src="' + filePath + '"></script>';
          },
          starttag: '<!-- injector:js -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          '<%= yeoman.client %>/index.html': [
              ['{.tmp,<%= yeoman.client %>}/{app,components}/**/*.js',
               '!{.tmp,<%= yeoman.client %>}/app/config.*.js',
               '!{.tmp,<%= yeoman.client %>}/app/app.js',
               '!{.tmp,<%= yeoman.client %>}/{app,components}/**/*.spec.js',
               '!{.tmp,<%= yeoman.client %>}/{app,components}/**/*.mock.js']
            ]
        }
      },

      // Inject component less into app.less
      less: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/app/', '');
            filePath = filePath.replace('/client/components/', '');
            return '@import \'' + filePath + '\';';
          },
          starttag: '// injector',
          endtag: '// endinjector'
        },
        files: {
          '<%= yeoman.client %>/app/app.less': [
          // load base.less first and less source files from all modules afterwards
            '<%= yeoman.client %>/app/base_less/base.less',
            '<%= yeoman.client %>/{app,components}/**/*.less',
            // only include less source files whose name and directory name does not
            // start with underscore (_), files or folders marked by '_' are considered private
            '!<%= yeoman.client %>/{app,components}/**/_**/*.less',
            '!<%= yeoman.client %>/{app,components}/**/_*.less',
            '!<%= yeoman.client %>/app/app.less'
          ]
        }
      },

      // Inject component css into index.html
      css: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');
            return '<link rel="stylesheet" href="' + filePath + '">';
          },
          starttag: '<!-- injector:css -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          '<%= yeoman.client %>/index.html': [
            '<%= yeoman.client %>/{app,components}/**/*.css'
          ]
        }
      }
    },

    react : {
      files : {
        expand: true,
        cwd: '<%= yeoman.client %>/',
        src: ['{app,components}/**/*.jsx'],
        dest: '<%= yeoman.client %>/',
        ext: '.js'
      }
    },

    // Syntax check on all labels JSON files
    jsonlint: {
      sample: {
          src: [ '**/*labels*.json' ]
      }
    },

    replace: {
      dist: {
        src: ['<%= yeoman.dist %>/public/app/*.js'],
        overwrite: true,
        replacements: [{
          from: '%%%BUILD_TIMESTAMP%%%',
          to: new Date().getTime()
        }]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        // hostname: 'localhost',
        hostname: 'accomplice.local',
        livereload: 35729
      },
      local : {
        proxies : [
          {
            context : '/api/',
            host : 'localhost',
            port : 8083
          },
          {
            context : '/pixel/',
            host : 'localhost',
            port : 8083
          },
          {
            context : '/pixelmapgen/',
            host : 'localhost',
            port : 8083
          }
        ]
      },
      dev1 : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'dev1.accomplice.io',
            port : 8080
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'dev1.accomplice.io',
            port : 8080
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'dev1.accomplice.io',
            port : 8080
          }
        ]
      },
	    dev2 : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'dev2api.accomplice.io',
            port : 80
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'dev2api.accomplice.io',
            port : 80
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'dev2api.accomplice.io',
            port : 80
          }
        ]
      },
	    qa : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'qaapi.accomplice.io',
            port : 80
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'qaapi.accomplice.io',
            port : 80
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'qaapi.accomplice.io',
            port : 80
          }
        ]
      },
	    beta : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'betaapi.accomplice.io',
            port : 8080
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'betaapi.accomplice.io',
            port : 8080
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'betaapi.accomplice.io',
            port : 8080
          }
        ]
      },
      prod : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'api.accomplice.io',
            port : 80
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'api.accomplice.io',
            port : 80
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'api.accomplice.io',
            port : 80
          }
        ]
      },
      alpha : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'alpha.accomplice.io',
            port : 8080
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'alpha.accomplice.io',
            port : 8080
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'alpha.accomplice.io',
            port : 8080
          }
        ]
      },
      demo : {
        proxies : [
          {
            context : '/api/',
            // host : 'localhost',
            host: 'demoapi.accomplice.io',
            port : 8080
          },
          {
            context : '/pixel/',
            // host : 'localhost',
            host: 'demoapi.accomplice.io',
            port : 8080
          },
          {
            context : '/pixelmapgen/',
            // host : 'localhost',
            host: 'demoapi.accomplice.io',
            port : 8080
          }
        ]
      },
      /**********************************************************************/
      // KEEP CHANGES RELATED TO HOST PARAMS (SWITCHING TO ALPHA ENV) LOCALLY
      // KEEP CHANGES RELATED TO HOST PARAMS (SWITCHING TO ALPHA ENV) LOCALLY
      // KEEP CHANGES RELATED TO HOST PARAMS (SWITCHING TO ALPHA ENV) LOCALLY
      // KEEP CHANGES RELATED TO HOST PARAMS (SWITCHING TO ALPHA ENV) LOCALLY
      /**********************************************************************/
      livereload: {
        options: {
          open: true,
          middleware: function (connect, options) {
            if (!Array.isArray(options.base)) {
              options.base = [options.base];
            }

            // Setup the proxy
            var middlewares = [require('grunt-connect-proxy/lib/utils').proxyRequest];

            middlewares = middlewares.concat([
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(clientConfig.client)
            ]);
            return middlewares;
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(clientConfig.client)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      },
      graph : {
        options : {
          open: true,
          port: 9009,
          keepalive: true,
          middleware: function(connect, options, middlewares) {
            middlewares.unshift(
              graphProxy(require('https'), options)
            );
            return middlewares;
          }
        }
      }
    }
  });

  // custom middleware for proxy facebook graph apis
  function graphProxy (https, options) {
    function parseCookies(request) {
      var list = {},
          rc = request.headers.cookie;

      rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
      });

      return list;
    }
    function createPermissionUrl() {
      // request ads_managerment permission
      var base = 'https://www.facebook.com/dialog/oauth?client_id=1522282324703960&scope=ads_management,publish_actions&';
      return base + 'redirect_uri=http://' + options.hostname + ':' + options.port;
    }
    function extractCode(url) {
      var queryRegEx = /^.+?\?code=/;
      if(!queryRegEx.test(url)) {
        return false;
      } else {
        return url.replace(queryRegEx, '');
      }
    }
    function urlHasQuery(url) {
      return url.indexOf('?') > -1;
    }
    return function(req, res){
      var cookies = parseCookies(req);
      if(cookies.access_token) {
        var url = req.url.replace(/\/?dummyapi\/+/, '/');
        // console.log('Attemp to call path: ', url, 'on fb graph.');
        if(url === '/') {
          res.end('Graph api proxy now is ready.');
        }
        var base = 'https://graph.facebook.com' + url;
        var graph_url = base + (urlHasQuery(req.url) ? '&' : '?') + 'access_token=' + cookies.access_token;
        https.get(graph_url, function(resObj) {
          var str = '';
          resObj.on('data', function (chunk) {
            str += chunk;
          });
          resObj.on('end', function() {
            res.writeHead(200, {
              'Content-Type': 'application/json'
            });
            res.end(str);
          });
        });
      } else {
        var code = extractCode(req.url);
        if(code) {
          var graph_url = 'https://graph.facebook.com/oauth/access_token?' +
                          'client_id=1522282324703960&' +
                          'client_secret=5d3b0aa2f047e656bb05c67e57583246&' +
                          'redirect_uri=http://' + options.hostname + ':' + options.port + '/&' +
                          'code=' + code;
          var atRegEx = /^\s*access_token=/i;
          https.get(graph_url, function(resObj) {
            var str = '';
            resObj.on('data', function (chunk) {
              str += chunk;
            });
            resObj.on('end', function() {
              console.log(str);
              if(atRegEx.test(str)) {
                var token = str.replace(atRegEx, '');
                var d = new Date();
                d.setTime(d.getTime() + 60*1000); // in milliseconds
                d = '; expires='+d.toGMTString()+';';
                res.writeHead(301, [
                  ['Set-Cookie', 'access_token=' + token + '; httponly' + d ],
                  ['Location', '/']
                ]);
                res.end();
              } else {
                res.writeHead(500, {});
                res.end('Failed to accquire fb access_token.');
              }
            });
          });
          return;
        }
        res.end('<a href="'+ createPermissionUrl() +'">Click here for fb permission</a>');
      }
    };
  };

  // Used for delaying livereload until after server has restarted
  grunt.registerTask('wait', function () {
    grunt.log.ok('Waiting for server reload...');

    var done = this.async();

    setTimeout(function () {
      grunt.log.writeln('Done waiting!');
      done();
    }, 1500);
  });

  grunt.registerTask('serve', function (target) {
    clientConfig.environment = 'local';

    if (target === 'dist') {
      return grunt.task.run(['build', 'wait', 'connect:dist:keepalive']);
    }

    if (target === 'debug') {
      return grunt.task.run([
        'clean:server',
        'injector:less',
        'react',
        'concurrent:server',
        'injector',
        'wiredep',
        'autoprefixer',
        'concurrent:debug',
        'configureProxies:local',
        'connect:livereload'
      ]);
    }

    if (target === 'alpha' || target === 'dev1' || target === 'prod' || target === 'beta' || target === 'dev2' || target === 'qa' || target === 'demo') {
      grunt.task.run([
        'clean:server',
        'injector:less',
        'react',
        'concurrent:server',
        'injector',
        'wiredep',
        'autoprefixer',
        'wait',
        'configureProxies:' + target,
        'connect:livereload',
        'watch'
      ]);
    }


    grunt.task.run([
      'clean:server',
      'injector:less',
      'react',
      'concurrent:server',
      'injector',
      'wiredep',
      'autoprefixer',
      'wait',
      'configureProxies:local',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  grunt.registerTask('test', function(target) {
    if (target === 'client') {
      return grunt.task.run([
        'clean:server',
        'injector:less',
        'react',
        'ngtemplates',
        'concurrent:test',
        'injector',
        'autoprefixer',
        'connect:test',
        'karma'
      ]);
    }

    else if (target === 'e2e') {
      return grunt.task.run([
        'clean:server',
        'injector:less',
        'react',
        'concurrent:test',
        'injector',
        'wiredep',
        'autoprefixer',
        'protractor',
        'connect:test'
      ]);
    }

    else grunt.task.run([
      'test:client'
    ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'injector:less',
    'react',
    'concurrent:dist',
    'injector',
    'wiredep',
    'useminPrepare',
    'autoprefixer',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'copy:labels',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    'rev',
    'usemin',
    'jsonlint',
    'replace'
  ]);

  grunt.registerTask('default', function () {
    clientConfig.environment = 'production';

    grunt.task.run([
      'newer:jshint',
      'test',
      'build'
    ]);
  });

  grunt.registerTask('dev1', function () {
    clientConfig.environment = 'dev1';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });

    grunt.registerTask('dev2', function () {
    clientConfig.environment = 'dev2';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });

    grunt.registerTask('beta', function () {
    clientConfig.environment = 'beta';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });

    grunt.registerTask('qa', function () {
    clientConfig.environment = 'qa';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });

  grunt.registerTask('demo', function () {
    clientConfig.environment = 'demo';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });

  grunt.registerTask('production', function () {
    clientConfig.environment = 'production';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });

  grunt.registerTask('staging', function () {
    clientConfig.environment = 'staging';

    grunt.task.run([
      'newer:jshint',
      //'test',
      'build'
    ]);
  });


  // jshint on uncommited js files
  grunt.registerTask('jshintgit', function() {
    
    // this will be an async task, call done when 
    // we are good
    var done = this.async();

    var child_process = require('child_process');
    // execute shell command to get current working directory 
    child_process.exec('pwd', function(err, stdout) {
      var gitRoot = stdout.toString();
      gitRoot = gitRoot.replace(/\/dashboard.*\n*/g, '');
    // git root is v2_frontend/fractal, where the .git folder is located
    // set GIT_DIR env variable to our gitRoot, and run git status
    // to get a list of uncommited (and untracked) files
    // --porcelain flag makes the output easily parsable by scripts
    // here we use awk to grab the second column, which are the actual file paths
      child_process.exec('unset GIT_DIR && GIT_DIR="' + gitRoot + '" && git status --porcelain | awk \'{ print $2 }\'', 
        function(err, stdout) {
          var files = stdout.toString(); 
          // split filePaths into array of lines
          var filePaths = files.split('\n')
          // only interested in *.js files, but not Gruntfile.js itself
            .filter(function(filePath) {
              return /\.js$/.test(filePath) && !(/Gruntfile/ig.test(filePath)); 
          // set yeoman.client 
            }).map(function(jsPath) {
              return jsPath.replace(/^.*?dashboard\/client\//, '<%= yeoman.client %>/');
            });

          // if nothing has changed skip linting
          if(!filePaths.length) {
            done();
            return;
          }

          console.log('Uncommited js files: ');
          console.log(filePaths.join('\n'));
          console.log('Running lint...');

          // set grunt config for jshint:git task to run only on uncommited and untracked js files
          grunt.config.set(['jshint', 'git'], grunt.config.process(filePaths))
          // run the task (in package.json, pre-commit hook will run lint command
          // which is set to: "grunt jshintgit"
          grunt.task.run(['jshint:git']);
          done();
        });
    });
  });
};
