'use strict';

describe('Directive: fsAppIdInlineDropdown', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('components/directives/fsAppIdInlineDropdown/fsAppIdInlineDropdown.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));
});
