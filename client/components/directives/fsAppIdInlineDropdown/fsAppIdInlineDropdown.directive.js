'use strict';

angular.module('common')
  .directive('fsAppIdInlineDropdown', function (regex) {
  return {
    restrict: 'A',
    replace: true,
    templateUrl: 'components/directives/fsAppIdInlineDropdown/fsAppIdInlineDropdown.html',
    scope: {
      value: '=fsAppIdInlineDropdown',
      onSave: '=',
      user: '=',
      naming: '=inputPlaceholder'
    },
    link: function (scope, element, attrs) {

      element.bind('keydown keypress', function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.saveProject();
          });

          return false;
        }
      });

      scope.numbersRegex = regex.APPNUMBERS;

      scope.view = {
        editableValue: scope.value,
        editorEnabled: false
      };

      scope.enableEditor = function () {
        scope.view.editorEnabled = true;
        scope.view.editableValue = scope.value;
        setTimeout(function () {
          element.find('input')[0].focus();
        });
      };

      scope.disableEditor = function () {
        scope.appIdForm.appId.$invalid = false;
        scope.view.editorEnabled = false;
      };

      scope.saveProject = function () {
        if(scope.appIdForm.appId.$invalid) {
          return;
        }
        if(!scope.view.editableValue){
          return;
        }
        if(attrs.onSave) {
          scope.onSave(scope.view.editableValue, scope.user);
        }

        scope.value = '';
        scope.view.editorEnabled = false;
      };
    }
  };
});
