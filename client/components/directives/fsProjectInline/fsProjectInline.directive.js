'use strict';

angular.module('common')
  .directive('fsProjectInline', function ($timeout) {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: 'components/directives/fsProjectInline/fsProjectInline.html',
      scope: {
        value: '=fsProjectInline',
        onSave: '=',
        onEnabling: '=',
        disabled: '=projectDisabled',
        user: '=',
        naming: '=inputPlaceholder'
      },
      link: function (scope, element, attrs) {

        element.bind('keydown keypress', function (event) {
          if(event.which === 13) {
            $timeout(function (){
              scope.saveProject();
            });

            return false;
          }
        });

        scope.view = {
          editableValue: scope.value,
          editorEnabled: false
        };

        scope.enableEditor = function () {
          scope.onEnabling(true);
          scope.view.editorEnabled = true;
          scope.view.editableValue = scope.value;
          $timeout(function () {
            element.find('input')[0].focus();
          });
        };

        scope.disableEditor = function () {
          $timeout(function () {

            scope.view.editorEnabled = false;
            if(!scope.view.editorEnabled) {
              scope.onEnabling(false);
            }
          }, 200);
        };

        scope.saveProject = function () {
          if(!scope.view.editableValue){
            return;
          }
          if(attrs.onSave) {
            scope.onSave(scope.view.editableValue, scope.user);
          }

          scope.value = '';
          scope.disableEditor();
        };
      }
    };
  });
