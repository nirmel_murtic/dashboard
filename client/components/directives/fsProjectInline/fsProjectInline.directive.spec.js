'use strict';

describe('Directive: fsProjectInline', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('components/directives/fsProjectInline/fsProjectInline.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

});
