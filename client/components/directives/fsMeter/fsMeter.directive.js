'use strict';

angular.module('common')
  .directive('fsMeter', function($parse) {
    return {
      restrict : 'EA',
      scope : true,
      controller : function() {
        // p is a number betwee [0, 1]
        this.percentage = angular.noop;
        this.meterWidth = function(p) {
          p = p || this.percentage() || 0;
          if(p < 0) {
            p = 0;
          } 
          if(p > 1) {
            p = 1;
          }
          return p * 100 + '%';
        };
      },
      controllerAs : 'fsMeter',
      templateUrl : 'components/directives/fsMeter/fsMeter.html',
      compile : function(element, attrs) {
        var titleExpr = attrs.meterTitle;
        var percentageExpr = attrs.percentage || '1';
        var percentage = $parse(percentageExpr);
        element.find('.meter-title').attr('ng-bind', titleExpr);
        return function(scope, element, attrs, ctrl) {
          ctrl.percentage = percentage.bind(null, scope); 
          var bar = element.find('.meter-bar');
          bar.css('width', ctrl.meterWidth());
          scope.$watch(ctrl.percentage, function(p) {
            bar.css('width', ctrl.meterWidth(p));
          });
        };
      }
    };
  });
