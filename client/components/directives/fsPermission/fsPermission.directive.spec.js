'use strict';

describe('Directive: fsPermission', function () {

  // load the directive's module
  beforeEach(module('common'));

  var scope, element;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  // it('should remove if there is no LOGGED_IN permission', inject(function ($compile, $auth) {
  //   // Testing data
  //   $auth.user = {
  //     authorities: ['LIN']
  //   };

  //   element = angular.element('<div fs-permission="<LOGGED_IN> && <SOMETHING_WRONG>"></div>');
  //   element = $compile(element)(scope);

  //   expect(element.attr('data-removed')).toBe('true');
  // }));

  it('should be visible if there is LOGGED_IN permission', inject(function ($compile, $auth) {
    // Testing data
    $auth.user = {
      authorities: ['LIN']
    };

    element = angular.element('<div fs-permission="<LOGGED_IN>"></div>');
    element = $compile(element)(scope);

    expect(element.attr('data-removed')).toBe(undefined);
  }));
});
