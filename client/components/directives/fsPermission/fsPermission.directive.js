'use strict';

angular.module('common')
  .directive('fsPermission', function ($auth, $filter) {
    return {
      restrict: 'AE',
      link: function (scope, element, attrs) {
        var permissions = attrs.fsPermission;

        if(!permissions) {
          permissions = attrs.permissions;
        }

        var hasPermission = $auth.checkPermissions(permissions);

        if(!hasPermission) {
          if(attrs.fsPermissionPreventClick !== undefined) {
            element.attr('aria-label', $filter('translate')('label.common.YouhaveViewOnlyAccessforthisFeature'));
            element.addClass('tooltipped tooltipped-sw tooltipped-multiline');
            element.unbind('click');
            element.attr('disabled', 'disabled');
          } else {
            element.attr('data-removed', true);
            element.remove();
          }
        }
      }
    };
  });
