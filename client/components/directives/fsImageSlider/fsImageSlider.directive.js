'use strict';

angular.module('common')
    .directive('fsImageSlider', function (apiUrl) {

        function getMinLength(a, b) {
            if (a < b) {
                return a;
            } else {
                return b;
            }
        }

        function getImages(page, images) {
            var i = page*3;
            var minLength = getMinLength((page+1)*3, images.length);
            var filteredImages = [];
            for (; i<minLength; i++) {
                filteredImages.push(images[i]);
            }
            return filteredImages;
        }

        return {
            restrict: 'E',
            templateUrl: 'components/directives/fsImageSlider/fsImageSlider.html',
            scope: {
                images: '=images'
            },
            link : function(scope) {
                function calculateTotalPages (array) {
                  if (array.length % 3 === 0) {
                    return array.length / 3;
                  } else {
                    return Math.floor(array.length/3) + 1;
                  }
                }



              scope.currentPage = 0;

              scope.selectImage = function (index) {
                // Find the real index
                index = index + scope.currentPage * 3;
                if (scope.images[index].selected === true) {
                  scope.images[index].selected = false;
                } else {
                  for(var i=0;i<scope.images.length; i++) {
                    scope.images[i].selected = false;
                  }
                  scope.images[index].selected = true;
                }
              };

              scope.getImageLocation = function (img) {
                  return img.id ? apiUrl + '/api/v2/image/get?guid=' + img.fullGuid : img.fullGuid;
              };

              scope.nextPage = function () {
                  if (scope.currentPage !== scope.totalPages -1) {
                      scope.currentPage++;
                      scope.filteredImages=[];
                      scope.filteredImages = getImages(scope.currentPage, scope.images);
                  }
              };

              scope.prevPage = function () {
                  if (scope.currentPage !== 0) {
                      scope.currentPage--;
                      scope.filteredImages=[];
                      scope.filteredImages = getImages(scope.currentPage, scope.images);
                  }
              };

              scope.$watch('images', function (newVal) {
                scope.totalPages = calculateTotalPages(newVal);
                scope.filteredImages = getImages(scope.currentPage, newVal);
              });
            }
        };
    });
