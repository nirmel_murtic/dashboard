'use strict';

angular.module('common')
  .directive('fsNavigationConfirmation', function(fsConfirm, $filter, $location, ngDialog) {

    return {
      restrict: 'EA',
      scope: false,
      link: function (scope) {

        function handleLeavingPage( event, next ) {
          function isDirty() {
            if((scope.form.settings && scope.campaign.settingsPageHasChanged)){
              return (scope.form.settings.$dirty && !scope.form.settingsSubmitted);            
            }
          }

          function isModalOpen(){
            return angular.element('.ngdialog').length > 0;
          }

          function confirmCallBack(){
            $location.url($location.url(next).hash());
            ngDialog.close();
            stopWatchingLocation();
          }

          function cancelCallBack(){

          }

          if(isDirty() || isModalOpen()){
            event.preventDefault();
            fsConfirm('generic', {
              body: $filter('translate')('label.act.Areyousureyouwanttonavigateawayfromthispage?Yourchangeswillbelost') 
            }).then(confirmCallBack, cancelCallBack);
          }
        }

        var stopWatchingLocation = scope.$on( '$locationChangeStart', handleLeavingPage);  
      }
    };
  });


  