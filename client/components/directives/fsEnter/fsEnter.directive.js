'use strict';

angular.module('common')
  .directive('fsEnter', function ($timeout) {
    return function (scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
            if(event.which === 13) {
              $timeout(function (){
                element.val(null);
                scope.$eval(attrs.fsEnter);
              });
              event.preventDefault();
            }
        });
   	};
  });
