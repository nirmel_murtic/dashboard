'use strict';

angular.module('common')
  .service('fsNotify', function($q, fsUtils, $timeout) {
 
    var disappearAfter = 3500;
    var animationDuration = 200;
    var queue = [];
    var service;

    function remove(what) {
      var item = _.find(queue, what);
      if(item && !item.removing) {
        item.removing = true;
      } else {
        return null;
      }
      return what;
    }

    function push(promise, stayFor) {
      promise = $q.when(promise);
      var d;
      promise.then(function(x) {
        d = {
          success : true,
          value : x,
          key : _.uniqueId('q')
        };
        queue.push(d);
        $timeout(remove.bind(null, d), stayFor || disappearAfter)
          .then(function(d) {
            if(d) {
              service.emit('change');
              return $timeout(function() {
                _.remove(queue, d);
                service.emit('change');
                return d;
              }, animationDuration);
            }
          });
        service.emit('change');
      }, function(err) {
        d = {
          success : false,
          key : _.uniqueId('q'),
          value : err
        };
        queue.push(d);
        service.emit('change');
        $timeout(remove.bind(null, d), stayFor || disappearAfter)
          .then(function(d) {
            if(d) {
              service.emit('change');
              return $timeout(function() {
                _.remove(queue, d);
                service.emit('change');
                return d;
              }, animationDuration);
            }
          });
      });
    }
    service = fsUtils.Emitter({
      getQueue : function() {
        return queue;
      },
      push : push,
      remove : function(what) {
        return $timeout(remove.bind(null, what), 10)
          .then(function(d) {
            if(d) {
              service.emit('change');
              return $timeout(function() {
                _.remove(queue, d);
                service.emit('change');
                return d;
              }, animationDuration);
            }
          });
      }
    });

    return service;
  });
