'use strict';

angular.module('common')
  .run(function(fsNotify) {

    /* jshint quotmark:false, asi:true */
    var node = $('<div class="fs-notifications">').appendTo('body')[0];
    var Notification = React.createClass({
      dismiss : function() {
        fsNotify.remove(this.props.item);
      }, 
      render : function() {
        var item = this.props.item;
        var Comp = item.component;

        var value = item.value || '';
        var values = value.split('\n');

        console.log(item.removing);
        if(!Comp) {
          Comp = values.map(function(val) {
            return (
              <div key={val} className={ 'fs-notify-line ' + (item.success ? "success" : "error") }>
                { val }
              </div>
            ); 
          });
        } else {
          Comp = <Comp value={item.value} success={item.success} />
        }
        return (
          <div className={ "fs-notification " + (item.removing ? "gone-in-200" : "") }> 
            <div>
              {Comp}
            </div>
            <button  onClick={this.dismiss}
                     className="action hidden">Dismiss</button>
          </div>
        );
      }
    });

    var NotificationList = React.createClass({
      getInitialState : function() {
        fsNotify.on('change', this.forceUpdate.bind(this));
        return {
          queue : fsNotify.getQueue()
        };
      },
      render : function() {
        var queue = this.state.queue;
        var notifications = queue.map(function(q) {
          return <Notification item={q} key={q.key}/>
        });
        return (
          <div>
            {notifications}
          </div>
        );
      }
    });

    window.ntf = fsNotify;
    React.render(<NotificationList />, node);

  });
