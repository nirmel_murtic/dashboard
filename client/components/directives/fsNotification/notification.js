'use strict';

angular.module('common')
  .run(function(fsNotify) {

    /* jshint quotmark:false, asi:true */
    var node = $('<div class="fs-notifications">').appendTo('body')[0];
    var Notification = React.createClass({displayName: "Notification",
      dismiss : function() {
        fsNotify.remove(this.props.item);
      }, 
      render : function() {
        var item = this.props.item;
        var Comp = item.component;

        var value = item.value || '';
        var values = value.split('\n');

        console.log(item.removing);
        if(!Comp) {
          Comp = values.map(function(val) {
            return (
              React.createElement("div", {key: val, className:  'fs-notify-line ' + (item.success ? "success" : "error") }, 
                val 
              )
            ); 
          });
        } else {
          Comp = React.createElement(Comp, {value: item.value, success: item.success})
        }
        return (
          React.createElement("div", {className:  "fs-notification " + (item.removing ? "gone-in-200" : "") }, 
            React.createElement("div", null, 
              Comp
            ), 
            React.createElement("button", {onClick: this.dismiss, 
                     className: "action hidden"}, "Dismiss")
          )
        );
      }
    });

    var NotificationList = React.createClass({displayName: "NotificationList",
      getInitialState : function() {
        fsNotify.on('change', this.forceUpdate.bind(this));
        return {
          queue : fsNotify.getQueue()
        };
      },
      render : function() {
        var queue = this.state.queue;
        var notifications = queue.map(function(q) {
          return React.createElement(Notification, {item: q, key: q.key})
        });
        return (
          React.createElement("div", null, 
            notifications
          )
        );
      }
    });

    window.ntf = fsNotify;
    React.render(React.createElement(NotificationList, null), node);

  });
