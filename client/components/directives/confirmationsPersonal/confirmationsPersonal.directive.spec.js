'use strict';

describe('Directive: confirmationsPersonal', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('components/directives/confirmationsPersonal/confirmationsPersonal.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));
});
