'use strict';

angular.module('common')
  .directive('confirmationsPersonal', function () {
    return {
      templateUrl: 'components/directives/confirmationsPersonal/confirmationsPersonal.html',
      restrict: 'E',
      scope: {
        alertData: '='
      },
      replace: true
    };
  });
