'use strict';

angular.module('dashboardApp')
  .directive('fsBackImg', function () {

    return {
      restrict: 'EA',
      scope: {
        url: '=url'
      },
      link: function(scope, element, attrs){ //jshint ignore:line
        element.css({
            'background-image': 'url(' + scope.url +')',
            'background-size' : 'contain',
            'background-repeat': 'no-repeat',
            'background-position': 'center center'
        });
        scope.$watch('url', function(newUrl, oldUrl){
          if(newUrl !== oldUrl){
            element.css({
              'background-image': 'url(' + newUrl +')'
            });
            console.log('new url!', newUrl, oldUrl);
          }
        });
      }
    };
  });
