'use strict';

describe('Directive: fsCsvImport', function () {

  // load the directive's module
  beforeEach(module('common'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fs-csv-import></fs-csv-import>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('');
  }));
});
