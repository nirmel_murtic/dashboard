'use strict';

angular.module('common')
  .directive('fsCsvImport', function ($timeout) {
    return {
      restrict: 'E',
      transclude: true,
      replace: true,
      scope:{
        content:'@',
        header: '=',
        separator: '=',
        result: '@',
        onChange: '=',
        buttonName: '=',
        ngDisabled:'='
      },
      templateUrl: 'components/directives/fsCsvImport/fsCsvImport.html',
      link: function(scope, element) {
        scope.delegateClick = function() {
          $timeout( function() {
            element.find('input').trigger('click');
          });
        };
        element.on('change', function(onChangeEvent) {
          var reader = new FileReader();
          reader.onload = function(onLoadEvent) {
            scope.$apply(function() {
              var content = {
                csv: onLoadEvent.target.result || onLoadEvent.srcElement.result,
                header: scope.header,
                separator: scope.separator
              };

              scope.content = content.csv;
              scope.result = csvToJSON(content);

              scope.onChange();
            });
          };
          var target = onChangeEvent.target || onChangeEvent.srcElement;
          if ( (target.type === 'file') && target.files )  {
            var files = target.files;

            //check to make sure they uploaded a csv or txt
            var fileName = files.length ? files[0].name : '';
            var fileExtension = fileName.substr(fileName.lastIndexOf('.'));

            if (!(fileExtension === '.csv' || fileExtension === '.txt')){
              alert('You cannot upload a file of type \'' + fileExtension + '\', only .csv or .txt');
              console.log('You cannot upload a file of type \'' + fileExtension + '\', only .csv or .txt');
              return;
            }

            if(files.length) {
              reader.readAsText(files[0]);
              target.value = '';
            }
          } else {
            if ( scope.content ) {
              var content = {
                csv: scope.content,
                header: scope.header,
                separator: scope.separator
              };
              scope.result = csvToJSON(content);
            }
          }
        });

        var csvToJSON = function(content) {

          var result = [];
          var headers = [];
          if (content.header) {
            headers = content.header.split(content.separator);
          }

          //If there is only one header, e.g. "names" for the AdWords Key Word
          //feature, import all words, separated by commas or newlines or whatever
          //First, convert newlines to [chosen separator], then split by that separator.
          if ( headers.length === 1 ){

            // The only method I found to find the sneaky newline characters in
            // the csv contents is to test against trim().
            var contents = content.csv;

            var charSplitContents = [];
            for(var m=0; m<contents.length; m++){
              if( !contents[m].trim() && contents[m] !== ' '){
                charSplitContents.push(content.separator);
              }
              else{
                charSplitContents.push(contents[m]);
              }
            }
            var parsedContents = charSplitContents.join('');

            var header = content.header;

            var words = [];
            if (parsedContents) {
              words = parsedContents.split(content.separator);
            }
            //clear empty strings
            words = words.filter(function(q){ return q;});
            //strip leading and trailing whitespace
            words = _.map(words, function(word){
                return word.trim();
            });

            for (var n=0; n<words.length; n++){
              var obj = {};
              obj[header] = words[n];
              result.push(obj);
            }
          }

          //Otherwise, if there are more headers, parse them separately, and keep the
          //headers as keys, and the contents as values. e.g. member management and
          //anything else we need in the future
          else{

            var lines=content.csv.split('\n');
            var columnCount = lines[0].split(content.separator).length;

            if(headers.length !== columnCount) {
              return false;
            }

            for (var i=0; i<lines.length; i++) {
              if(!lines[i].trim().length) {
                console.log('Skip empty line!');
                continue;
              }

              var obj2 = {};
              var currentLine=lines[i].split(content.separator);

              if ( currentLine.length === columnCount ) {
                if (content.header) {
                  for (var j=0; j<headers.length; j++) {
                    obj2[headers[j]] = currentLine[j];
                  }
                } else {
                  for (var k=0; k<currentLine.length; k++) {
                    obj2[k] = currentLine[k];
                  }
                }
                result.push(obj2);
              } else {
                return false;
              }
            }
          }

          //returns an array of objects, with headers as keys, and values as values. One object per 'row'
          // [ { header1:value1 , header2:value2 } , { header1:value3 , header2:value4 } ]
          return result;
        };
      }
    };
  });
