/**
 * Created by samir on 02.12.2015..
 */
'use strict';

angular.module('common')
  .filter('newOldPixel', function() {

    function toTitleCase(string) {
      return string.replace(/\w\S*/g, function(word){
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
      });
    }

    var oldPixel = function(pixel) {
      return pixel.name;
    };

    var newPixel = function(pixel) {
      var name = pixel.name,
        typeName = pixel.customEventType,
        pixelName;

      typeName = typeName.replace('_', ' ');

      pixelName = name + ' - ' + toTitleCase(typeName);

      return pixelName;
    };

    var customPixel = function(pixel) {
      var name = pixel.name,
        typeName = pixel.customEventName,
        pixelName;

      typeName = typeName.replace('_', ' ');

      pixelName = name + ' - ' + toTitleCase(typeName);

      return pixelName;
    };

    return function (input) {
      if(input.fbPixelVersion === 'FB_OLD_PIXEL') {
        return oldPixel(input);
      } else if(input.fbPixelVersion === 'FB_NEW_PIXEL' && input.fbEventType === 'FB_STANDARD_EVENT') {
        return newPixel(input);
      } else if(input.fbPixelVersion === 'FB_NEW_PIXEL' && input.fbEventType === 'FB_CUSTOM_EVENT') {
        return customPixel(input);
      }

    };
  });
