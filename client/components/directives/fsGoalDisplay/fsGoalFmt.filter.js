'use strict';

angular.module('common')
  .filter('fsGoalFmt', function(GoalBlueprints) {

    /**
     * @param {Object} - goal, goal json
     * @param {Boolean} - concat, 
     *    flag to determin whether or not to return string or array of strings 
     *    default to true
     *
     *  
     * @return [[String]] | String
     */


    return function(goal, concat) {
      if(concat === 'parts') {
        concat = false;
      } else {
        concat = true;
      } 
      if(!goal) {
        return concat ? '' : [['']];
      }
      var goalParts = [];
      var fmt, goalStr, goalPart;
      for(var i=0; i<GoalBlueprints.length; i++) {
        fmt = GoalBlueprints[i]; 
        goalStr = fmt(goal);
        if(goalStr.value) {
          if(goalStr.inline) {
            goalPart.push(goalStr.value);
          } else {
            goalPart = [];
            goalParts.push(goalPart);
            goalPart.push(goalStr.value);
          } 
          if(!goalPart) {
            goalPart = [];
            goalParts.push(goalPart);
          }
        }// end if
      }// end for
      if(concat) {
        return _.flattenDeep(goalParts).join('');
      }
      return goalParts;
    };
  
  });
