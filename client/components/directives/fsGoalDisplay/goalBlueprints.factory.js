'use strict';

angular.module('common')
  .factory('GoalFmtUtils', function() {
    // utility functions to format goals, originally from projects.controller.js
    var displaySiteTrackers = [{
      key: 'AD_CHANNELS',
      displayName: 'Ad Channels'
    },
    {
      key: 'ACCOMPLICE',
      displayName: 'Accomplice Tracking Script'
    },
    {
      key: 'ACCOMPLICE_CONVERSION_PIXEL',
      displayName: 'Accomplice Conversion Pixel'
    }];

    var getSiteTrackerByKey = function(key) {
      var result = null;

      angular.forEach(displaySiteTrackers, function(tracker) {
        if(tracker.key === key) {
          result = tracker;

          return true;
        }
      });

      return result;
    };

    var displayConversionSiteTrackers = [{
      key: 'AD_CHANNELS',
      displayName: 'Facebook Conversion Pixel'
    },
    {
      key: 'ACCOMPLICE',
      displayName: 'Accomplice Tracking Script'
    },
    {
      key: 'ACCOMPLICE_CONVERSION_PIXEL',
      displayName: 'Accomplice Conversion Pixel'
    }];

    function getConversionTrackerByKey(key) {
      return _.find(displayConversionSiteTrackers, { key : key });
    }

    return {
      getSiteTrackerByKey : getSiteTrackerByKey,
      getConversionTrackerByKey : getConversionTrackerByKey
    };
  })
  /**
   * original templates from align
      <h3 ng-if="goal.host && goal.event">{{ goal.goal.displayName }} "{{ goal.event }}"</h3><h3 ng-if="goal.event" style="display:inline; margin-left: 0;"> in {{ goal.host }}</h3>
      <h3 ng-if="goal.host && !goal.event">{{ goal.goal.displayName }} {{ goal.host }}</h3>
      <h3 ng-if="goal.thirdPartyPixel">on {{ goal.thirdPartyPixel.name }} </h3>
      <h3 ng-if="goal.tracker" style="display:inline; margin-left: 0;"> tracked by {{ getSiteTrackerByKey(goal.tracker).displayName }}</h3>
      <h3 ng-if="goal.page.pageId">{{ goal.goal.displayName }} {{ goal.page.pageName }}</h3>
      <h3 ng-if="goal.twitterPromotableUser">{{ goal.goal.displayName }} {{ goal.twitterPromotableUser.screenName }}</h3>
      <h3 ng-if="goal.application.appId">{{ goal.goal.displayName }} </h3><h3 style="display:inline; margin-left: 0;">{{ goal.application.name }}</h3>
  */
  .factory('GoalBlueprints', function($parse,  $interpolate, GoalFmtUtils) {
    var blueprints = [
      {
        predicate : 'goal.host && goal.event',
        inline : false,
        template : '{{ goal.goal.displayName }} "{{ goal.event }}"'
      },
      {
        predicate : 'goal.event',
        inline : true,
        template : ' on {{ goal.host }}'
      },
      {
        predicate : 'goal.host && !goal.event',
        inline : false,
        template : '{{ goal.goal.displayName }} {{ goal.host }}'
      },

      {
        predicate : 'goal.tracker && !goal.thirdPartyPixel',
        inline : true,
        template : ' tracked by {{ getSiteTrackerByKey(goal.tracker).displayName }}'
      },
      {
        predicate : 'goal.tracker && goal.thirdPartyPixel',
        inline : true,
        template : ' tracked by {{ getConversionTrackerByKey(goal.tracker).displayName }}'
      },
      {
        predicate : 'goal.thirdPartyPixel',
        inline : false,
        template : ' "{{ goal.thirdPartyPixel | newOldPixel}}"'
      },
      {
        predicate : 'goal.page.pageId',
        inline : false,
        template : '{{ goal.goal.displayName }} {{ goal.page.pageName }}'
      },
      {
        predicate : 'goal.twitterPromotableUser',
        inline : false,
        template : '{{ goal.goal.displayName }} {{ goal.twitterPromotableUser.screenName }}'
      },
      {
        predicate : 'goal.application.id',
        inline : false,
        template : '{{ goal.goal.displayName }} '
      },
      {
        predicate : 'goal.application.id',
        inline : true,
        template : '{{ goal.application.name }}'
      }
    ];

    var scope = _.assign({}, GoalFmtUtils);

    var formatters = _.map(blueprints, function(blueprint) {
      var predicate = $parse(blueprint.predicate);
      var format = $interpolate(blueprint.template);

      return function(goal) {
        if(!goal) {
          return {
            inline : true,
            value : ''
          };
        }

        scope.goal = goal;

        // console.log(predicate(scope), format(scope));

        if(predicate(scope)) {
          return {
            inline : blueprint.inline,
            value : format(scope)
          };
        }
        return {
          inline : true,
          value : ''
        };
      };

    });

    // this factory is an array of functions, each takes goal json and returns { inline, value } object
    return formatters;
  });
