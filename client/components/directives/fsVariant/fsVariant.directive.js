'use strict';

angular.module('common')
  .directive('fsVariant', function () {
    return {
      templateUrl: 'components/directives/fsVariant/fsVariant.html',
      restrict: 'EA',
      replace: true,
      transclude: true,
      scope: false,
      require: 'ngModel',
      link: function (scope, element, attrs) {
        scope.variantType = attrs.type;
        scope.noAddVariant = attrs.noAddVariant;
      }
    };
  });
