'use strict';

angular.module('common')

.controller('fsTabsetController', ['$scope', '$timeout', function TabsetCtrl($scope, $timeout) {
  var ctrl = this,
      tabs = ctrl.tabs = $scope.tabs = [];

  var updateParentScope = $scope.updateParentScope();

  ctrl.select = function(selectedTab) {
    angular.forEach(tabs, function(tab) {
      if (tab.active && tab !== selectedTab) {
        tab.active = false;
        if(tab.onDeselect()) {
          tab.onDeselect()();
        }
        if(updateParentScope) {
          $timeout(function() {
            $scope.$parent.$digest();
          }, 0);
        }
      }
    });
    selectedTab.active = true;
    if(selectedTab.onSelect()) {
      selectedTab.onSelect()();
    }
  };

  ctrl.addTab = function addTab(tab) {
    var index = tabs.indexOf(tab);

    // Prevent adding duplicate tabs
    if(index === -1) {
      tabs.push(tab);
    } else {
      tab = tabs[index];
    }

    // we can't run the select function on the first tab
    // since that would select it twice
    if (tabs.length === 1) {
      tab.active = true;
    } else if (tab.active) {
      ctrl.select(tab);
    }
  };

  ctrl.removeTab = function removeTab(tab) {
    var index = tabs.indexOf(tab);
    //Select a new tab if the tab to be removed is selected and not destroyed
    if (tab.active && tabs.length > 1 && !destroyed) {
      //If this is the last tab, select the previous tab. else, the next tab.
      var newActiveIndex = index === tabs.length - 1 ? index - 1 : index + 1;
      ctrl.select(tabs[newActiveIndex]);
    }
    tabs.splice(index, 1);
  };

  ctrl.deregister = function() {
    tabs.forEach(function(tab) {
      if(tab.deregister) {
        tab.deregister();
      }
    });
    ctrl.deregistered = true;
  };

  var destroyed;
  $scope.$on('$destroy', function() {
    destroyed = true;
  });
}])
/**
 * @ngdoc directive
 * @name fs-tabset
 * @restrict EA
 *
 * @description
 * Tabset is the outer container for the tabs directive
 *
 * @param {boolean=} vertical Whether or not to use vertical styling for the tabs.
 * @param {boolean=} justified Whether or not to use justified styling for the tabs.
 *
 * @example
<example module="common">
  <file name="index.html">
    <fs-tabset>
      <fs-tab>
        <tab-heading><h5>This is Heading</tab-heading>
        <b>First</b> Content!
      </fs-tab>
      <fs-tab>
        <tab-heading><h5>This is Heading 2</tab-heading>
        <b>Second</b> Content!
      </fs-tab>
    </fs-tabset>
  </file>
</example>
 */
.directive('fsTabset', function() {
  return {
    restrict: 'EA',
    transclude: true,
    replace: true,
    scope: {
      type: '@',
      updateParentScope : '&',
      multipleColumns: '@'
    },
    controller: 'fsTabsetController',
    templateUrl: 'components/directives/fsTabset/fsTabset.html',
    link: function(scope, element, attrs) {
      scope.vertical = angular.isDefined(attrs.vertical) ? scope.$parent.$eval(attrs.vertical) : false;
      scope.justified = angular.isDefined(attrs.justified) ? scope.$parent.$eval(attrs.justified) : false;
      scope.tabsType = angular.isDefined(attrs.tabsType) ? attrs.tabsType : 'horizontal-tabs';
      scope.multipleColumns = angular.isDefined(attrs.multipleColumns) ? true : false;
    }
  };
})
.directive('fsTab', ['$parse', function($parse) {
  return {
    require: '^fsTabset',
    restrict: 'EA',
    replace: true,
    templateUrl: 'components/directives/fsTabset/fsTabContent.html',
    transclude: true,
    scope: {
      isOpen : '&?',
      isRemoved: '&?',
      heading: '@',
      onSelect: '&select', //This callback is called in contentHeadingTransclude
                          //once it inserts the tab's content into the dom
      onDeselect: '&deselect'
    },
    controller: function() {
    },
    compile: function(elm, attrs, transclude) {
      return function fsTab$postLink(scope, elm, attrs, tabsetCtrl) {

        if(scope.isOpen) {
          scope.active = !!scope.isOpen();

          scope.deregister = scope.$watch(scope.isOpen, function(active) {
            if (active) {
              tabsetCtrl.select(scope);
            }
          });
        } else {
          scope.active = false;
        }
        if (scope.isRemoved) {
            scope.deregisterRemove = scope.$watch(scope.isRemoved, function (isRemoved, wasRemoved) {
                if (isRemoved && !wasRemoved) {
                    tabsetCtrl.removeTab(scope);
                } else if (wasRemoved && !isRemoved) {
                    tabsetCtrl.addTab(scope);
                }
            });
        }
//
        scope.disabled = false;
        if ( attrs.disabled ) {
          scope.$parent.$watch($parse(attrs.disabled), function(value) {
            scope.disabled = !! value;
          });
        }

        scope.select = function() {
          if ( !scope.disabled ) {
            if(!tabsetCtrl.deregistered) {
              tabsetCtrl.deregister();
            }
            tabsetCtrl.select(scope);
          }
        };

        tabsetCtrl.addTab(scope);
        scope.$on('$destroy', function() {
          if(scope.isRemoved){
            scope.deregisterRemove();
            tabsetCtrl.removeTab(scope);
          }
        });

        //We need to transclude later, once the content container is ready.
        //when this link happens, we're inside a tab heading.
        scope.$transcludeFn = transclude;
      };
    }
  };
}])

.directive('tabHeadingTransclude', [function() {
  return {
    restrict: 'A',
    require: '^fsTab',
    link: function(scope, elm, attrs, tabCtrl) {
      /* jshint unused: false */
      scope.$watch('headingElement', function updateHeadingElement(heading) {
        if (heading) {
          elm.html('');
          elm.append(heading);
        }
      });
    }
  };
}])
.directive('tabContentTransclude', function() {
  /* jshint latedef: false */
  return {
    restrict: 'A',
    require: '^fsTabset',
    link: function(scope, elm, attrs) {
      var tab = scope.$eval(attrs.tabContentTransclude);

      //Now our tab is ready to be transcluded: both the tab heading area
      //and the tab content area are loaded.  Transclude 'em both.
      tab.$transcludeFn(tab.$parent, function(contents) {
        angular.forEach(contents, function(node) {
          if (isTabHeading(node)) {
            //Let tabHeadingTransclude know.
            tab.headingElement = node;
          } else {
            elm.append(node);
          }
        });
      });
    }
  };
  function isTabHeading(node) {
    return node.tagName &&  (
      node.hasAttribute('tab-heading') ||
      node.hasAttribute('data-tab-heading') ||
      node.tagName.toLowerCase() === 'tab-heading' ||
      node.tagName.toLowerCase() === 'data-tab-heading'
    );
  }
});
