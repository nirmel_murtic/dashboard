fsTabs Directive
=============

Usage: 
--------


```
  <fs-tabset>
    <fs-tab>
      <tab-heading><h5>This is heading</h5></tab-heading>
      Content Here.
    </fs-tab>
    <fs-tab>
      <tab-heading><h5>This is heading</h5></tab-heading>
      <br>
      <img src="http://placehold.it/350x150">
    </fs-tab>
  </fs-tabset>
```

```
  <fs-tabset justified="true">
    <fs-tab select="callbackOnSelect">
      <tab-heading><h5>This is Justified heading</h5></tab-heading>
      Content Here.
    </fs-tab>
    <!-- config default active tab -->
    <fs-tab active="true">
      <tab-heading><h5>This is Justified heading2</h5></tab-heading>
      <br>
      <img src="http://placehold.it/350x150">
    </fs-tab>
  </fs-tabset>
```

Important Notes:
--------------------

* Directive `<tab-heading>` directive allows for any html; however for most cases you should use `<h5>` tag for heading like the above example
* `<fs-tab>/fsTab` directive accepts attribute `select` and `deselect`, they should be callback functions that triggers then the given tab is selected/disselected
