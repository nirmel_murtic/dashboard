 'use strict';

/*
  Fractal Sciences Radio Button List Directive

  Provides a ui element that can list many elements each with check boxes.
  Has smart pagination to accommodate many elements
  Has two additional check boxes at the top to Select All and Select None

  Takes one function, 
    called addFn, is customizable
  This allows you to assign any functionality you want to the buttons, whether that's
  adding a variant to a view and model or adding the values to some random list

  Used in Twitter Keywords and Followers
*/

angular.module('common')
  .directive('fsRadioBtnList', function () {
    return {
      templateUrl: 'components/directives/fsRadioBtnList/fsRadioBtnList.html',
      restrict: 'EA',
      scope: {
        availableOptions:'=',
        config:'=',
        // toggleFn: '&',
        // allFn: '&',
        // noneFn: '&',
        addFn: '&',
        targetingSpec: '@'
      },
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        scope.config = scope.config || {};
        var defaults = {
          valueField: 'label',
          showSelectAllBtn: true,
          showSelectNoneBtn: true,
          numOfColumnItems: 5
        };

        var output=[];

        scope.config = _.assign(defaults, scope.config);

        scope.currPage = 0;
        var totalPages = scope.availableOptions.length/(scope.config.numOfColumnItems*2);
        scope.totalPages = totalPages % 1 === 0 ? totalPages : Math.floor(totalPages) + 1;

        scope.selectedItems = 0;
        _.forEach(scope.availableOptions, function(element) {
          element.selected = false;
        });

        scope.createPagesArray = function (number) {
          return new Array(number);
        };

        scope.setPage = function (page) {
          if (scope.currPage !== page) {
            scope.currPage = page;
          }
        };

        scope.nextPage = function () {
          if (scope.currPage !== scope.totalPages -1) {
            scope.currPage++;
          }
        };

        scope.prevPage = function () {
          if (scope.currPage !== 0) {
            scope.currPage--;
          }
        };

        function ngCtrlRenderView () {
          ngModel.$setViewValue(output);
          ngModel.$render();
        }

        scope.selectOption = function (opt) {
          if (opt.selected === false) {
            scope.selectedItems++;
            output.push({
              value:opt.suggestion_value //jshint ignore:line
            });
          } else {
            var index = -1;
            for (var i=0; i<output.length; i++) {
              if (output[i].value === opt.suggestion_value) { //jshint ignore:line
                index = i;
              }
            }
            if (index !== -1) {
              output.splice(index, 1);
              scope.selectedItems--;
            }
          }
          ngCtrlRenderView();
          opt.selected = !opt.selected;
        };

        scope.selectAllOptions = function () {
          output = [];
          for (var i=0; i<scope.availableOptions.length; i++) {
            scope.availableOptions[i].selected = true;
            output.push({
              value: scope.availableOptions[i].suggestion_value //jshint ignore:line
            });
          }
          scope.selectedItems = scope.availableOptions.length;
          ngCtrlRenderView();
        };

        scope.unselectAllOptions = function () {
          output = [];
          for (var i=0; i<scope.availableOptions.length; i++) {
            scope.availableOptions[i].selected = false;
          }
          scope.selectedItems = 0;
          ngCtrlRenderView();
        };
        return scope.availableOptions;
      }
    };
  });
