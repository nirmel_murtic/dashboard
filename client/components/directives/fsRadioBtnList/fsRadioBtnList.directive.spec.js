'use strict';

describe('Directive: fsRadioBtnList', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('components/directives/fsRadioBtnList/fsRadioBtnList.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

});