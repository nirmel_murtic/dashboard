'use strict';

angular.module('dashboardApp')
  .directive('fsSelect', function($parse, fsSelectParseOptions) {
    return {
      restrict : 'E',
      require: 'ngModel',
      scope : {
        defaultText : '@',
        unselect : '=',
        disableDropdown: '='
      },
      controller : function() {
        this.items = [];
        this.select = function(item) {
          this.active = item;
        };
        this.selectValue = function(val) {
          var item = _.find(this.items, { value : val });
          if(item) {
            this.select(item);
          } else if(this.items.length) {
            this.select(null);
          }
        };
      },
      controllerAs : 'selection',
      templateUrl: 'components/directives/fsSelect/fsSelect.html',
      link : function(scope, element, attrs, ngModel) {
        var selection = scope.selection;
        
        var ast = fsSelectParseOptions.parse(attrs.fsOptions);

        selection.select = function(item) {
          this.active = item;
          if(item && item.value) {
            ngModel.$setViewValue(item.value);
          }
        };

        scope.$watch('disableDropdown', function(value) {
          if(value){
            element.addClass('disabled-dropdown');
          } else {
            element.removeClass('disabled-dropdown');
          }
        });
        
        scope.$watch(ast.array.bind(null, scope.$parent), function() {
          selection.items = fsSelectParseOptions.evaluate(ast, scope.$parent);
          var newActive;
          if(selection.active) {
            if(ast.key) {
              newActive = _.find(selection.items, { key: selection.active.key });
            }
            if(!ast.key && !newActive){
              newActive = _.find(selection.items, { value: selection.active.value });
            }
            selection.select(newActive || null);
          }
        });
        scope.$watch(function() {
          return ngModel.$modelValue;
        }, function(val) {
          selection.selectValue(val);
        });        
      }
    };
  })
  .service('fsSelectParseOptions', function($parse) {
  
    // lexing
    function removeLeadingWhites(str) {
      var i=0;
      while(str.charAt(i) <= ' ') {
        i = i + 1;
      }
      return str.slice(i);
    }
    function keywordToken(ident) {
      return function (str) {
        var remaining;
        if(_.startsWith(str, ' ' + ident.toLowerCase() + ' ') || _.startsWith(str, ' ' + ident.toUpperCase() + ' ')) {
          remaining = str.slice(2 + ident.length);
          remaining = removeLeadingWhites(remaining);
          return {
            remaining : remaining,
            token : {
              type : 'keyword',
              value: ident
            }
          };
        }
        return false;
      };
    }
    var keywords = {
      '_trackBy_' : keywordToken('track by'),
      '_as_' : keywordToken('as'),
      '_in_' : keywordToken('in'),
      '_for_' : keywordToken('for')
    };
    function anyKeywordToken(str) {
      var k, t;
      for(k in keywords) {
        t = keywords[k](str);
        if(t) {
          return t;
        }
      }
      return false;
    }
    function exprToken(str) {
      var i=0;
      while(!anyKeywordToken(str.slice(i)) && i<str.length) {
        i++;
      }
      var t = str.slice(0, i);
      if(!t || !t.length) {
        throw 'Lexer Error';
      }
      return { 
        token: {
          type : 'expr',
          value : _.trim(t)
        },
        remaining : str.slice(i)  
      };
    }
    function tokenize(str) {
      str = removeLeadingWhites(str);
      var keywordsUsed = {};
      var t, tokens = [];
      while(str.length) {
        t = anyKeywordToken(str);
        if(t && !keywordsUsed[t.token.value]) {
          tokens.push(t.token);
          str = t.remaining;
          keywordsUsed[t.token.value] = true;
        } else {
          t = exprToken(str);
          tokens.push(t.token);
          str = t.remaining;
        }
      }
      return tokens;
    }
  
    // parsing
    // EXPR as EXPR for EXPR in EXPR [track by EXPR]
    /*
     { array : <fn>, itemAs : <string>, label : <fn>, key : <fn>, model : <fn>  }
     */
  
     // a.id as a.label for a in as [track by a.key] 
     // [0, a.id][1, as][2, a.label][3, for][4, a][5, in][6, as][7, track by][8, a.key]
  
     function parseScopedExpr(scope, expr) {
       var f = $parse(expr);
       return function(_scope) {
         var d = {};
         d[scope] = _scope;
         return f(d);
       };
     }
  
     function parse(str) {
       if(!str || !str.length) {
         throw 'Missing options expression';
       }
       var tokens = tokenize(str);
       if(tokens.length === 5) {
         tokens.unshift({
           type : 'keyword',
           value : 'as'
         });
         tokens.unshift(tokens[4]);
       }
       if(tokens.length < 7) {
         throw 'Bad options expression';
       }
       var result = {};
       var itemAs = tokens[4].value;
       result.itemAs = itemAs;
       result.array = $parse(tokens[6].value);
       result.label = parseScopedExpr(itemAs, tokens[2].value);
       result.model = parseScopedExpr(itemAs, tokens[0].value);
       if(tokens.length > 7) {
         result.key = parseScopedExpr(itemAs, tokens[9].value);
       }
       return result;
     }
     function evaluate(ast, scope) {
       var arr = ast.array(scope) || [];
       
       return _.map(arr, function(item, index) {
         return {
           label : ast.label(item),
           value : ast.model(item),
           key : ast.key ? ast.key(item) : index
         };
       });
     }
     return {
       parse : parse,
       evaluate : evaluate
     };
  });
  /*
  .controller('fsSelectCtrl', function() {
     this.optionItems = [];
     this.selectedViewValue = null;
     this.nothingSelected = function() {
       return !this.selectedViewValue; 
     };
   })
  .directive('fsSelect', function ($compile, $parse, $interpolate) {

    // jshint -W026
    // jshint maxlen: false
    var NG_OPTIONS_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;
                        // 1: value expression (valueFn)
                        // 2: label expression (displayFn)
                        // 3: group by expression (groupByFn)
                        // 4: array item variable name
                        // 5: object item key variable name
                        // 6: object item value variable name
                        // 7: collection expression
                        // 8: track by expression

    

    return {
      templateUrl: 'components/directives/fsSelect/fsSelect.html',
      restrict: 'EA',
      scope : {},
      controller : 'fsSelectCtrl',
      controllerAs : 'fsSelect',
      require : ['ngModel', '?ngRequired'],
      transclude: true,
      link: function (scope, element, attrs, controllers) {
        // jshint -W026, latedef: false 
        var ngModelCtrl = controllers[0];
        var fsSelectCtrl = scope.fsSelect;

        // angular did not expose ngOptions directive's parsing functionality as publi apis
        // we copied their source code below 
        var ngOptions = parseOptionsExpression(attrs.fsOptions || '', element, scope.$parent);
        // ngOptions : { getOptions : <function>, getWatchables : <function> }
        var options = ngOptions.getOptions();
        // ngOptions.getOptions() : { item : [...] } > item : { label: ..., viewValue : ... }

        // setup watcher, in case options on parent scope changed, utilizing ngOptions.getWatchables
        scope.$watch(function() {
          // this will return an array of keys (either specified by track by or $$hashKey values)
          return ngOptions.getWatchables(scope.$parent);
        }, updateOptions, true);

        // load displayable option items to fsSelect's controller
        fsSelectCtrl.optionItems = options.items;

        // find transcluded default labels
        var defaultLabel = element.find('.fs-select-default').find('li').text() || '""';
        var template = $interpolate(defaultLabel);
        fsSelectCtrl.getDefaultLabel = function() {
          return template(scope); 
        };
        // implement ngModelController's $render method, in our case just update a scope variable on the directive's isolates scope
        ngModelCtrl.$render = function() {
          fsSelectCtrl.selectedViewValue = options.getOptionFromViewValue(ngModelCtrl.$viewValue) || null;
        };
        // implement select method on the controller, correctly set the viewValue on ngModelController, so that validation and all can work
        fsSelectCtrl.select = function(item) {
          if(item && item.viewValue) {
            ngModelCtrl.$setViewValue(item.viewValue);
            ngModelCtrl.$render();
          } else {
            ngModelCtrl.$setViewValue(null);
            ngModelCtrl.$render();
          }
        };
        // when options change 
        // --
        function updateOptions(newKeys, oldKeys) {
          var changed = true;
          if(newKeys.length === oldKeys.length) {
            changed = false;
            for(var i=0; i<newKeys.length; i++) {
              if(newKeys[i] !== oldKeys[i]) {
                changed = true;
                continue;
              }
            }
            if(!changed) {
              return;
            }
          }
          ngOptions = parseOptionsExpression(attrs.fsOptions || '', element, scope.$parent);
          options = ngOptions.getOptions();
          fsSelectCtrl.optionItems = options.items;
          // fsSelectCtrl.select(null);
        }
      }
    };

    // taken from angular source ng-options

    function hashKey(obj, nextUidFn) {
      // jshint -W026
      var key = obj && obj.$$hashKey;

      if (key) {
        if (typeof key === 'function') {
          key = obj.$$hashKey();
        }
        return key;
      }

      var objType = typeof obj;
      if (objType === 'function' || (objType === 'object' && obj !== null)) {
        key = obj.$$hashKey = objType + ':' + (nextUidFn || _.uniqueId)();
      } else {
        key = objType + ':' + obj;
      }

      return key;
    }

    function parseOptionsExpression(optionsExp, selectElement, scope) {
      // jshint -W026

      var match = optionsExp.match(NG_OPTIONS_REGEXP);
      if (!(match)) {
        throw 'Error parsing fs-options attribute';
      }

      // Extract the parts from the ngOptions expression

      // The variable name for the value of the item in the collection
      var valueName = match[4] || match[6];
      // The variable name for the key of the item in the collection
      var keyName = match[5];

      // An expression that generates the viewValue for an option if there is a label expression
      var selectAs = / as /.test(match[0]) && match[1];
      // An expression that is used to track the id of each object in the options collection
      var trackBy = match[8];
      // An expression that generates the viewValue for an option if there is no label expression
      var valueFn = $parse(match[2] ? match[1] : valueName);
      var selectAsFn = selectAs && $parse(selectAs);
      var viewValueFn = selectAsFn || valueFn;
      var trackByFn = trackBy && $parse(trackBy);

      // Get the value by which we are going to track the option
      // if we have a trackFn then use that (passing scope and locals)
      // otherwise just hash the given viewValue
      var getTrackByValue = trackBy ?
                                function(viewValue, locals) { return trackByFn(scope, locals); } :
                                function getHashOfValue(viewValue) { return hashKey(viewValue); };
      var displayFn = $parse(match[2] || match[1]);
      var groupByFn = $parse(match[3] || '');
      var valuesFn = $parse(match[7]);

      var locals = {};
      var getLocals = keyName ? function(value, key) {
        locals[keyName] = key;
        locals[valueName] = value;
        return locals;
      } : function(value) {
        locals[valueName] = value;
        return locals;
      };


      function Option(selectValue, viewValue, label, group) {
        this.selectValue = selectValue;
        this.viewValue = viewValue;
        this.label = label;
        this.group = group;
      }

      return {
        getWatchables: $parse(valuesFn, function(values) {
          // Create a collection of things that we would like to watch (watchedArray)
          // so that they can all be watched using a single $watchCollection
          // that only runs the handler once if anything changes
          var watchedArray = [];
          values = values || [];

          Object.keys(values).forEach(function getWatchable(key) {
            var locals = getLocals(values[key], key);
            var selectValue = getTrackByValue(values[key], locals);
            watchedArray.push(selectValue);

            // Only need to watch the displayFn if there is a specific label expression
            if (match[2]) {
              var label = displayFn(scope, locals);
              watchedArray.push(label);
            }
          });
          return watchedArray;
        }),

        getOptions: function() {

          var optionItems = [];
          var selectValueMap = {};

          // The option values were already computed in the `getWatchables` fn,
          // which must have been called to trigger `getOptions`
          var optionValues = valuesFn(scope) || [];

          var keys = Object.keys(optionValues);
          keys.forEach(function getOption(key) {

            // Ignore 'angular' properties that start with $ or $$
            if (key.charAt(0) === '$') { return; } 

            var value = optionValues[key];
            var locals = getLocals(value, key);
            var viewValue = viewValueFn(scope, locals);
            var selectValue = getTrackByValue(viewValue, locals);
            var label = displayFn(scope, locals);
            var group = groupByFn(scope, locals);
            var optionItem = new Option(selectValue, viewValue, label, group);

            optionItems.push(optionItem);
            selectValueMap[selectValue] = optionItem;
          });

          return {
            items: optionItems,
            selectValueMap: selectValueMap,
            getOptionFromViewValue: function(value) {
              return selectValueMap[getTrackByValue(value, getLocals(value))];
            }
          };
        }
      };
    }
  }); */
