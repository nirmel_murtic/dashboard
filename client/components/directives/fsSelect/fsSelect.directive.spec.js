'use strict';

describe('Directive: fsSelect', function () {

  // load the directive's module and view
  beforeEach(module('dashboardApp'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
    scope.options = ['a', 'b'];
    scope.something = null;
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fs-select ng-model="something" fs-options="item for item in options"><li>Choose one</li></fs-select>');
    element = $compile(element)(scope);
    expect(!!element).toBe(true);
  }));
});
