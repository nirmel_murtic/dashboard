'use strict';

angular.module('common')
  .directive('fsTwMaxlength', function(regex) {
    return {
      require: 'ngModel',
      scope: {
        'fsTwMaxlength': '='
      },
      link: function (scope, element, attrs, ngModelCtrl) {
        // var maxlength = Number(scope.myMaxlength);

        function determineTweetTextLength(text){
          var tweetURLLength = 0; 
          var urlLength = 0;
          var textLength = 0;
          if(text){
            //if url, length of the url will be 22 characters, replace url length with 22 characters
            textLength = angular.copy(text.length);
            tweetURLLength = 22;
            if(text.match(regex.FULLURL.source)){
              urlLength = text.match(regex.FULLURL.source)[0].length;
              textLength = tweetURLLength + (textLength - urlLength);
            }
          } 
          return textLength;      
        }

         
        function fromUser(text) {
            var maxlength = scope.fsTwMaxlength;
            ngModelCtrl.$setValidity('unique', determineTweetTextLength(text) <= maxlength);


            //if user adds an image or changes to website cards, watch for this change
            scope.$watch('fsTwMaxlength', function(value){
               if(value){
                   ngModelCtrl.$setValidity('unique', determineTweetTextLength(text) <= value);
               }
             });

            return text;
        }

        ngModelCtrl.$parsers.push(fromUser);
      }
    }; 
  });
