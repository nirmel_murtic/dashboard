'use strict';

angular.module('common')
  .directive('fsSlider', function() {
    return {
      restrict : 'E',
      scope : {
        valueMin : '&',
        valueMax : '&',
        value : '=',
        realtime : '&'
      },
      templateUrl: 'components/directives/fsSlider/fsSlider.html',
      controller : function() {
        this.normalize = function(min, max, val) {
          if(!val || !max || max <= min) {
            return 0;
          }
          if(val < min) {
            return 0;
          }
          if(val > max) {
            return 100;
          }
          return (val - min) / (max - min) * 100; 
        };
        this.scaleBack = function(min, max, normVal) {
          if(!normVal || !max || max <= min) {
            return min || 0;
          }
          return min + (normVal / 100 * (max - min));
        }; 
      },
      controllerAs : 'fsSlide',
      link : function(scope, element, attrs, fsSlide) {
        if(!attrs.valueMin) {
          scope.valueMin = function() { return 0; };
        }
        if(!attrs.valueMax) {
          scope.valueMax = function() { return 1; };
        }
        if(!attrs.realtime) {
          scope.realtime = function() { return false; };
        }

        scope.value = scope.value || 0;
        scope.x = fsSlide.normalize(scope.valueMin(), scope.valueMax(), scope.value); 

        scope.active = false;
        scope.drag = false;

        var container = element.find('.fs-slide');
        var dot = container.find('.dot');

        function xFromEvent(evt) {
          var x = evt.pageX - container.offset().left;
          x = x / container.width() * 100;
          if(x < 0) {
            x = 0;
          } else if (x > 100) {
            x = 100;
          }
          return x;
        }

        function commit() {
          scope.$apply(function() {
            scope.value = fsSlide.scaleBack(
              scope.valueMin(),
              scope.valueMax(),
              scope.x);
          });
        }

        container.on('mouseleave', function() {
          scope.active = false;
          if(scope.realtime()) {
            scope.$digest();
          } else {
            commit();
          }
        });
        container.on('mousedown', function(e) {
          if($(e.target).hasClass('dot')) {
            scope.$apply(function() { 
              scope.drag = true; 
              scope.active = true; 
            });
          } else {
            scope.drag = false;
            scope.active = true;
            scope.x = xFromEvent(e);
            scope.$digest();
          }
        });
        container.on('mouseup', function(e) {
          if(scope.drag && scope.active) {
            scope.x = xFromEvent(e);
            scope.active = false;
            scope.drag = false;
            commit();
          } else if (scope.active) {
            commit();
          }
        });
        dot.on('transitionend', function() {
          if(scope.active) {
            scope.active = false;
            scope.$digest();
          }
        });
        container.on('mousemove', function(e) {
          e.stopPropagation();
          if(scope.active && scope.drag) {
            scope.x = xFromEvent(e);
            if(scope.realtime()) {
              commit();
            } else {
              scope.$digest();
            }
          }
        });

        scope.$watch('value', function(newVal) {
          var x = fsSlide.normalize(scope.valueMin(), scope.valueMax(), newVal);
          if(x !== scope.x) {
            scope.x = x;
          }
        });

      }
    };
  });
