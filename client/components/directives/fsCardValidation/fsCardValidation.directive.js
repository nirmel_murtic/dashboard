'use strict';

angular.module('common')
  .directive('fsCardValidation', function () {

    var setValidationForExp = function(valid, scope, field) {
        scope.form.billing[field].$setValidity('fs-card-validation', valid);
    };

    var getMonth = function(scope) {
      return scope.account.billing.month;
    };

    var getYear = function(scope) {
      return scope.account.billing.year;
    };

    return {
      require: 'ngModel',
      link: function(scope, elem, attr, ngModel) {
        if(attr.fsCardValidation === 'number') {
          ngModel.$parsers.unshift(function (value) {
            scope.cardType = recurly.validate.cardType(value).replace('_', '-');

            var valid = !value || (recurly.validate.cardNumber(value) && scope.cardType !== 'unknown') || value.indexOf('XXXXXX') !== -1;

            ngModel.$setValidity('fs-card-validation', valid);

            scope.cardValid = valid;

            return value;
          });

          ngModel.$formatters.unshift(function (value) {
            scope.cardType = recurly.validate.cardType(value).replace('_', '-');

            var valid = !value || (recurly.validate.cardNumber(value) && scope.cardType !== 'unknown') || value.indexOf('XXXXXX') !== -1;

            ngModel.$setValidity('fs-card-validation', valid);

            return value;
          });
        } else if(attr.fsCardValidation === 'cvv') {
          ngModel.$parsers.unshift(function (value) {
            var cvvValid = value && recurly.validate.cvv(value);

            ngModel.$setValidity('fs-card-validation', cvvValid);

            return value;
          });

          ngModel.$formatters.unshift(function (value) {
            var cvvValid = value && recurly.validate.cvv(value);

            ngModel.$setValidity('fs-card-validation', cvvValid);

            return value;
          });
        } else if(attr.fsCardValidation === 'month') {
          ngModel.$parsers.unshift(function (value) {
            var cvvValid = value && getYear(scope) && recurly.validate.expiry(value, getYear(scope));

            setValidationForExp(cvvValid, scope, attr.fsCardValidation);

            return value;
          });

          ngModel.$formatters.unshift(function (value) {
            var cvvValid = value && getYear(scope) && recurly.validate.expiry(value, getYear(scope));

            setValidationForExp(cvvValid, scope, attr.fsCardValidation);

            return value;
          });
        } else if(attr.fsCardValidation === 'year') {
          ngModel.$parsers.unshift(function (value) {
            var cvvValid = value && getMonth(scope) && recurly.validate.expiry(getMonth(scope), value);

            setValidationForExp(cvvValid, scope, attr.fsCardValidation);

            return value;
          });

          ngModel.$formatters.unshift(function (value) {
            var cvvValid = value && getMonth(scope) && recurly.validate.expiry(getMonth(scope), value);

            setValidationForExp(cvvValid, scope, attr.fsCardValidation);

            return value;
          });
        }
      }
    };
  });
