'use strict';

describe('Directive: uiBreadcrumbs', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('components/directives/uiBreadcrumbs/uiBreadcrumbs.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

});
