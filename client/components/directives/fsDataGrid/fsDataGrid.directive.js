'use strict';

angular.module('common')
  .directive('fsDataGrid', function(DataGrid, DataGridViewState, dgDispatcher, dgConstants, DataGridStore) {
    return {
      restrict : 'EA',
      scope : {
        data : '=',
        columnDefs : '&',
        maxRows : '&'
      },
      link : function(scope, element) {
        
        var tableId = _.uniqueId('fsdg');

        scope.$watch(function() { return scope.columnDefs(); }, function(newDefs) {
          dgDispatcher.dispatch({
            source : 'external',
            action : {
              type : dgConstants.INIT_TABLE,
              tableId : tableId,
              data : scope.data,
              columnDefs : newDefs
            }
          });
        });

        var dataView = DataGridStore.getDataView(tableId);
        dataView.columnDefs = scope.columnDefs();
        dataView.tableId = tableId;
        dataView.maxRows = scope.maxRows() || 50; 

        function rerender(tId, opts) {
          var el;
          if(tId === tableId) {
            dataView = DataGridStore.getDataView(tableId);
            dataView.columnDefs = scope.columnDefs();
            dataView.maxRows = scope.maxRows() || 50; 
            dataView.tableId = tableId;
            dataView.dirty = true;
            if(opts && opts.reset) {
              dataView = _.assign({ reset: true }, dataView);
            }
            el = React.createElement(DataGrid, dataView);
            React.render(el, element[0]);
          }
        }

        scope.$on('$destroy', function() {
          dgDispatcher.dispatch({
            source : 'external',
            action : {
              type : dgConstants.DESTRORY_TABLE,
              tableId : tableId
            }
          });
          DataGridStore.off('change', rerender);
          React.unmountComponentAtNode(element[0]);
        });

        scope.$watch(function() {
          return scope.maxRows();
        }, rerender.bind(null, tableId));

        DataGridStore.on('change', rerender);
        React.render(React.createElement(DataGrid, dataView), element[0]);
      }
    };
  });
