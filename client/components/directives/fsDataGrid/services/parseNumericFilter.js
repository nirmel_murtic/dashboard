'use strict';

angular.module('common')
  .factory('parseNumericFilter', function() {
    // Grammar:
    // Digit : 0|1|2|...|9
    // Number: Digit{1, }[.Digit{1, }]
    // UnaryOp : =|<|>|!=|>=|<=
    // BinaryOp : AND|OR      # operators have the same perdecence 
    // Expr: Part[BinaryOp Expr]
    // Part: (Expr)|UnaryOp Number

    var uOps = {
      '=' : true,
      '>' : true,
      '<' : true,
      '>=': true,
      '<=': true,
      '!=': true
    };
    var uOpsMaxLen = 2; // >=

    var bOps = {
      'AND' : true,
      'OR' : true
    };
    var bOpsMaxLen = 3; // AND

    var text,
        at,
        ch;

    function error(msg) {
      throw new SyntaxError(msg);
    }

    function next(c) {
      if (c && c !== ch) {
        error('Expected \'' + c + '\' instead of \'' + ch + '\'');
      }
      ch = text.charAt(at);
      at += 1;
      return ch;
    }
    // function nextn(n) {
    //  while(n-->0) {
    //    next();
    //  }
    // }
    
    function tryParse(what) {
      var _at = at, _ch = ch;
      try {
        return {
          result : what(),
          error : false
        };
      } catch(err) {
        at = _at;
        ch = _ch;
        return {
          error : true
        }; 
      }
    }
    
    function white() {
      while (ch && ch <= ' ') {
        next();
      }
    }
    function number() {
      var num,
          string = '';

      while (ch >= '0' && ch <= '9') {
        string += ch;
        next();
      }
      if (ch === '.') {
        string += '.';
        while (next() && ch >= '0' && ch <= '9') {
          string += ch;
        }
      }
      if (ch === 'e' || ch === 'E') {
        string += 'e';
        next();
        if (ch === '-' || ch === '+') {
          string += ch;
          next();
        }
        while (ch >= '0' && ch <= '9') {
          string += ch;
          next();
        }
      }
      num = +string;
      if (isNaN(num)) {
        error('Bad num');
      } else {
        return num;
      }
    }

    function unary() {
      var op = '', _op;
      for(var i=0; i<uOpsMaxLen; i++) {
        op += ch;
        next();
      }
      for(i=0; i<uOpsMaxLen; i++) {
        _op = op.slice(0, uOpsMaxLen - i);
        
        if(i>0) {
          ch = op.charAt(uOpsMaxLen - i);
          at--;
        }
        if(uOps[_op]) {
          return _op;
        }
      }
      error('Not Unary Op.');
    }
    function binary() {
      // var chs=[];
      var op = '', _op;
      for(var i=0; i<bOpsMaxLen; i++) {
        op += ch;
        next();
      }
      for(i=0; i<bOpsMaxLen; i++) {
        _op = op.slice(0, bOpsMaxLen - i);
        if(i>0) {
          ch = op.charAt(bOpsMaxLen - i);
          at--;
        }
        if(bOps[_op]) {
          return _op;
        }
      }
      error('Not Binary Op.');
    }

    function popAll() {
      var result = {}, current=result, op, nextOp;
      if(!operators.length) {
        return operands.pop();
      }
      do {
        current.type = 'binary';
        op = operators.pop();
        nextOp = operators[operators.length - 1];
        if(op !== SENTINEL) {
          current.op = op;
          current.right = operands.pop();
          current.left = (operands.length === 1 || nextOp === SENTINEL)? operands.pop() : {};
          current = current.left;
        } 
      } while(operators.length && nextOp !== SENTINEL);
      if(nextOp === SENTINEL) {
        operators.pop();
      }
      if(!result.op) {
        error('Bad parenthesis grouping');
      }
      return result;
    }
    
    function part() {
      var _uOp, _number;
      if(ch === '(') {
        next('(');
        white();
        operators.push(SENTINEL);
        expr();
        white();
        next(')');
        return popAll();
      }
      _uOp = unary();
      white();
      _number = number();
      return {
        op : _uOp,
        type : 'unary',
        operand : _number
      };
    }
    function expr() {
      var _part1, _bOp, _part2;
      _part1 = part();
      operands.push(_part1);
      white();
      _bOp = tryParse(binary);
      while(!_bOp.error) {
        operators.push(_bOp.result);
        white();
        _part2 = part();
        white();
        operands.push(_part2);
        _bOp = tryParse(binary);
      }
    }

    var SENTINEL = 'SENTINEL';
    var operators = [];
    var operands = [];
    function parse(source) {
      text = source;
      ch = ' ';
      at = 0;
      operators.length = 0;
      operands.length = 0;
      white();
      expr();
      var result = popAll();
      white();
      if(at === source.length + 1) {
        return result;
      }
      error('Parsing Error');
    }

    return parse;
  });
