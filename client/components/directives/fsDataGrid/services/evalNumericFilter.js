'use strict';

angular.module('common')
  .factory('evalNumericFilter', function() {
    return function _eval(ast, value) {
      if(ast.type === 'unary') {
        switch(ast.op) {
          case '=':
            return ast.operand === value;
          case '>':
            return value > ast.operand;
          case '<':
            return value < ast.operand;
          case '>=':
            return value >= ast.operand;
          case '<=':
            return value <= ast.operand;
          case '!=':
            return value !== ast.operand;
          default:
            return false;
        }
      }
      if(ast.type === 'binary') {
        if(ast.op === 'AND' || ast.op === '&&') {
          return _eval(ast.left, value) && _eval(ast.right, value);
        } else if (ast.op === 'OR' || ast.op === '||') {
          return _eval(ast.left, value) && _eval(ast.right, value);
        }
      }
    };
  });
