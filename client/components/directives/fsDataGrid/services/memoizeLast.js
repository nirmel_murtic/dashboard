'use strict';

angular.module('common')
  .constant('memoizeLast', function(f, resolve) {
    resolve = resolve || function(x) {
      return x;
    };
    var cache;
    return function() {
      var input, output;
      input = resolve.apply(null, arguments);
      if(!cache) {
        output = f.apply(null, arguments); 
        cache = {};
        cache.input = input;
        cache.output = output;
        return output;
      } 
      if(cache.input !== input) {
        output = f.apply(null, arguments); 
        cache.input = input;
        cache.output = output;
        return output;
      }
      return cache.output;
    };
  });
