'use strict';

angular.module('common')
  .constant('dgConstants', {
    SORT : 'SORT',
    FILTER : 'FILTER',
    GROUP_BY : 'GROUPBY',
    CHECK_ROW : 'CHECKROW',
    INIT_TABLE : 'INITTABLE',
    DESTRORY_TABLE : 'DESTRORYTABLE',
    GOTO_PAGE : 'GOTO_PAGE'
  })
  .factory('dgActions', function(dgDispatcher, dgConstants) {
    return {
      gotoPage : function(tableId, page) {
        dgDispatcher.dispatch({
          source : 'view',
          action : {
            type : dgConstants.GOTO_PAGE,
            page : page,
            tableId : tableId
          }
        });
      },
      sortColumn : function(tableId, columnId) {
        dgDispatcher.dispatch({
          source : 'view',
          action : {
            type : dgConstants.SORT,
            column : columnId,
            tableId : tableId
          }
        });
      },
      filterColumn : function(tableId, columnId, filterString) {
        dgDispatcher.dispatch({
          source : 'view',
          action : {
            type : dgConstants.FILTER,
            column : columnId,
            tableId : tableId,
            filter : filterString
          }
        });
      },
      groupByColumn : function(tableId, columnId) {
        dgDispatcher.dispatch({
          source : 'view',
          action : {
            type : dgConstants.GROUP_BY,
            column : columnId,
            tableId : tableId
          }
        });
      }
    };
  });
