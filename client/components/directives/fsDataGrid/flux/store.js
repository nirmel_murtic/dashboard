'use strict';

angular.module('common')
  .factory('DataGridStore', function(dgDispatcher, dgConstants, DataGridViewState, fsUtils) {
    // jshint newcap: false
    var Emitter = fsUtils.Emitter;
    var tables = {};  
    var noData = [];
    var emptyView = [[]];

    function getDataView(tableId) {
      if(!tables[tableId]) {
        return {
          data : noData,
          view : emptyView,
          cursor : 0,
          nrow : 0,
          sort : false,
          filters : {}
        };
      }
      var table = tables[tableId];
      var view = table.viewState.getGroupedFilteredAndSortedData(table.data);
      var nrow = _.reduce(view, function(n, group) {
        return n+group.length;
      }, 0);
      return {
        data : table.data,
        view : view,
        sort : table.viewState.sortBy && table.viewState.sortBy.by ? table.viewState.sortBy : false,
        filters : table.viewState.filterStrings,
        cursor : table.cursor,
        nrow : nrow
      };
    }

    // handle payloads

    function initTable(id, data, columnDefs) {
      var existing = tables[id], vs;
      if(existing && columnDefs === existing.columnDefs) {
        vs = existing.viewState;
      }
      tables[id] = {
        data : data, 
        columnDefs : columnDefs,
        viewState : vs || DataGridViewState.init(columnDefs)
      };
    }

    function paging(id, page) {
      var tableView = getDataView(id);
      if(tables[id] && tableView && tableView.cursor !== page && page >=0 && page <tableView.nrow) {
        tables[id].cursor = page; 
        return true; 
      }
    }

    function sortTable(id, colId) {
      var table = tables[id];
      if(table) {
        table.viewState.useSort(colId, table.viewState.sortBy.order === 'asc' ? 'desc' : 'asc');
        return true;
      }
    }

    function filterTable(id, colId, filter) {
      var table = tables[id];
      if(table) {
        table.viewState.useFilter(colId, filter);
        return true;
      }
    }

    function deleteTable(id) {
      delete tables[id];
    }

    var store = Emitter({
      getDataView : getDataView
    });

    var dispatchToken = dgDispatcher.register(function(payload) {
      var action = payload.action;
      switch(action.type) {
        case dgConstants.GOTO_PAGE:
          if(paging(action.tableId, action.page)) {
            store.emit('change', action.tableId);
          }
          return; 
        case dgConstants.INIT_TABLE: 
          initTable(action.tableId, action.data, action.columnDefs);
          store.emit('change', action.tableId, { reset: true });
          break;
        case dgConstants.DESTRORY_TABLE: 
          deleteTable(action.tableId);
          return;
        case dgConstants.SORT: 
          if(sortTable(action.tableId, action.column)) {
            break;
          } 
          return;
        case dgConstants.FILTER: 
          if(filterTable(action.tableId, action.column, action.filter)) {
            break;
          } 
          return;
        default: 
          return;
      }
      var table = tables[action.tableId];
      if(table) {
        table.cursor = 0;
      }
      store.emit('change', action.tableId);
    }); 

    store.dispatchToken = dispatchToken;

    return store;

  });
