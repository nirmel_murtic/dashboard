'use strict';

angular.module('common')
  .factory('dgDispatcher', function(Dispatcher) {
    return new Dispatcher();
  });
