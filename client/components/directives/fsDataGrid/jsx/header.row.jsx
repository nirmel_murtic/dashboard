'use strict';

angular.module('common')
  .factory('DGHeader', function(dgActions, DGColumnBorder) {
    /* jshint quotmark:false, asi:true */
    var HeaderCell = React.createClass({
      handleSortChange : function(e) {
        if(!this.props.sortable) {
          return;
        }
        e.preventDefault();
        e.stopPropagation();
        dgActions.sortColumn(this.props.tableId, this.props.column);
        // Actions.changeSort(this.props.column, this.props.sort.order === "asc" ? "desc" : "asc");
      },
      sortClass : function() {
        var sort = this.props.sort;
        var ascOrDesc = (sort.order === "desc") ? "headerSortDesc" : "headerSortAsc";
        return (this.props.column === sort.column) ? ascOrDesc : "";
      },
      render : function() {
        var showTooltip = !!this.props.tooltip;
        if(this.props.sortable === false || this.props.sort === false || this.props.sort.column !== this.props.column) {
          return (
            <div className={ "dg-"+this.props.column + " dg-table-cell header " + (this.props.sortable ? "" : "no-sort") + (showTooltip ? " tooltipped tooltipped-n " : "")} onClick={this.handleSortChange} aria-label={showTooltip ? this.props.tooltip : ""}>
              <DGColumnBorder />
              <span>
                { this.props.label }
              </span>
            </div>
          );
        }
        return (
          <div onClick={ this.handleSortChange } className = { "dg-"+this.props.column + " dg-table-cell header " + this.sortClass() + (showTooltip ? " tooltipped tooltipped-n " : "")} aria-label={showTooltip ? this.props.tooltip : ""}>
            <DGColumnBorder />
            <span>
              { this.props.label }
            </span>
          </div>
        ); 
      }
    });

    var HeaderRow = React.createClass({
      // componentDidUpdate : function() {
      // this.refs.masterCheckbox.getDOMNode().indeterminate = (!this.props.noneChecked && !this.props.allChecked);
      // },
      saveColumnWidths : function() {
        var node;
        for(var colId in this.refs) {
          node = this.refs[colId].getDOMNode();
          this.props.onMount(colId, $(node).width());
        }
      },
      render : function() {
        // var onMountHeader = this.props.onMount;
        var columnHeaders = _.map(this.props.columnDefs, function(def, colId) {
          var sort = false;
          if(this.props.sort) {
            sort = {
              column : this.props.sort.by,
              order : this.props.sort.order
            };
          }
          return <HeaderCell 
                  key={colId} 
                  sortable={def.sort !== false}
                  sort={ sort } 
                  column={colId} 
                  tableId={this.props.tableId}
                  label={def.label}
                  tooltip={def.tooltip}
                  ref={colId}/>
        }, this); 
        return (
          <div className="dg-table-row">
            {columnHeaders}
          </div>
        )
      }
    });

    return HeaderRow;
  });
