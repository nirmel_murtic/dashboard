'use strict';

angular.module('common')
  .factory('DGFilter', function(dgActions, DGColumnBorder) {
    /* jshint quotmark:false, asi:true */
    var InputFilter = React.createClass({displayName: "InputFilter",
      updateFilter : function(event) {
        var val = _.trim(event.target.value); 
        dgActions.filterColumn(this.props.tableId, this.props.column, val);
      },
      render : function() {
        var showTooltip = this.props.tooltip && (!this.props.filterValue); 
        var tooltipOrient = this.props.tooltipOrient || "s"; 

        return (
          React.createElement("div", {className:  "dg-"+this.props.column + " dg-table-cell"}, 
            React.createElement(DGColumnBorder, null), 
            React.createElement("span", {className:  showTooltip ? "tooltipped tooltipped-" + tooltipOrient + " tooltipped-multiline" : "", "aria-label": showTooltip ? this.props.tooltip : ""}, 
              React.createElement("input", {type: "text", onChange:  this.updateFilter, placeholder: this.props.placeholder, value: this.props.filterValue})
            )
          )
        ); 
      }
    });

    var DDFilter = React.createClass({displayName: "DDFilter",
    
      updateFilter : function(event) {
        var val = _.trim(event.target.value); 
        dgActions.filterColumn(this.props.tableId, this.props.column, val);
      },

      render : function() {

        var format = this.props.format;
        var opts = this.props.options.map(function(o) {
          return React.createElement("option", {name: o, key: o, value: o},  format ? format(o) : o);
        });
        opts.unshift(
          React.createElement("option", {value: "", key: "-1"})
        );
        return (
          React.createElement("div", {className:  "dg-"+this.props.column + " dg-table-cell"}, 
            React.createElement(DGColumnBorder, null), 
            React.createElement("span", null, 
              React.createElement("select", {onChange: this.updateFilter, value: this.props.filterValue}, 
                opts
              )
            )
          )
        );
      }
    });

    var NoFilter = React.createClass({displayName: "NoFilter",
      shouldComponentUpdate : function() {
        return false;
      },
      render : function() {
        return (
          React.createElement("div", {className:  "dg-"+this.props.column + " dg-table-cell"}, 
            React.createElement(DGColumnBorder, null)
          )
        );
      }
    });

    var tooltipNumeric = "Use expressions such as: '>=10 AND <50', '>100 OR <-100' to filter rows.";
    var tooltipText = "Type search terms to filter rows.";

    var FilterRow = React.createClass({displayName: "FilterRow",
      render : function() {
        var ncol = _.size(this.props.columnDefs);
        var colIndex = -1;
        var cells = _.map(this.props.columnDefs, function(def, colId) {
          colIndex++;
          if(def.filter===false) {
            return React.createElement(NoFilter, {key: colId, column: colId});
          }
          if(def.type === 'atom') {
            if(!def.levels || def.levels.length === 0) {
              return React.createElement(NoFilter, {key: colId, column: colId});
            }
            return React.createElement(DDFilter, {column: colId, 
                             key: colId, 
                             format: def.format, 
                             filterValue: this.props.filters[colId], 
                             tableId: this.props.tableId, 
                             options: def.levels})
          }
          var placeholder, tooltip, orient = "s";
          var midCol = Math.round(ncol / 2);
          if(colIndex <= midCol) {
            orient = "se";
          } else if (colIndex > midCol) {
            orient = "sw";
          }
          if(def.type === 'numeric') {
            placeholder = '>=0';
            tooltip = tooltipNumeric; 
          } else if (def.type === 'string') {
            placeholder = 'Search...';
            tooltip = tooltipText; 
          } else {
            placeholder = '';
          }
          return React.createElement(InputFilter, {
                  key: colId, 
                  column: colId, 
                  filterValue: this.props.filters[colId], 
                  placeholder: placeholder, 
                  tooltipOrient: orient, 
                  tooltip: tooltip, 
                  tableId: this.props.tableId})
        }, this); 
        return (
          React.createElement("div", {className: "dg-table-row filter"}, 
            cells
          )
        )
      }
    });

    return FilterRow;
  });
