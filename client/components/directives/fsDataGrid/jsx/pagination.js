'use strict';

angular.module('common')
  .factory('DGPagination', function(dgActions) {
    /* jshint quotmark:false, asi:true */

    return React.createClass({

      handleClick : function() {
        if(this.props.active || this.props.disabled) {
          return;
        }
        dgActions.gotoPage(this.props.tableId, this.props.cursor);
      },

      render : function() {
        var className = '';
        if(this.props.active) {
          className += 'active';
        }
        if(this.props.disabled) {
          className = 'disabled'
        }

        var content;
        if(!this.props.faIcon) {
          content = this.props.page + 1;
        } else {
          content = React.createElement("i", {className:  'fa '+ this.props.faIcon})
        }
       
        return (
          React.createElement("li", {className: className}, 
            React.createElement("span", {href: "#!", onClick: this.handleClick}, content)
          )
        );
      }

    });

  });
