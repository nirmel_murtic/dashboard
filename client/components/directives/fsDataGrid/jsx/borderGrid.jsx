'use strict';

angular.module('common')
  .factory('DGColumnBorder', function() {
    /* jshint quotmark:double, asi:true */
    return React.createClass({
      shouldComponentUpdate : function() {
        return false;
      },
      signalDrag : function(event) {
        event.dragColumn = true;
      },
      suspendClick : function(event) {
        event.preventDefault();
        event.stopPropagation();
      },
      render : function() {
        return (
          <div className="border-grid" onMouseDown={this.signalDrag} onClick={this.suspendClick}>
          </div>
        );
      }
    });
  });
