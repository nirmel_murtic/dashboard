'use strict';

angular.module('common')
  .factory('DGFilter', function(dgActions, DGColumnBorder) {
    /* jshint quotmark:false, asi:true */
    var InputFilter = React.createClass({
      updateFilter : function(event) {
        var val = _.trim(event.target.value); 
        dgActions.filterColumn(this.props.tableId, this.props.column, val);
      },
      render : function() {
        var showTooltip = this.props.tooltip && (!this.props.filterValue); 
        var tooltipOrient = this.props.tooltipOrient || "s"; 

        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <DGColumnBorder />
            <span className={ showTooltip ? "tooltipped tooltipped-" + tooltipOrient + " tooltipped-multiline" : ""} aria-label={showTooltip ? this.props.tooltip : ""}>
              <input type="text" onChange={ this.updateFilter } placeholder={this.props.placeholder} value={this.props.filterValue}/>
            </span>
          </div>
        ); 
      }
    });

    var DDFilter = React.createClass({
    
      updateFilter : function(event) {
        var val = _.trim(event.target.value); 
        dgActions.filterColumn(this.props.tableId, this.props.column, val);
      },

      render : function() {

        var format = this.props.format;
        var opts = this.props.options.map(function(o) {
          return <option name={o} key={o} value={o}>{ format ? format(o) : o }</option>;
        });
        opts.unshift(
          <option value="" key="-1"></option>
        );
        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <DGColumnBorder />
            <span>
              <select onChange={this.updateFilter} value={this.props.filterValue}>
                {opts}
              </select>
            </span>
          </div>
        );
      }
    });

    var NoFilter = React.createClass({
      shouldComponentUpdate : function() {
        return false;
      },
      render : function() {
        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <DGColumnBorder />
          </div>
        );
      }
    });

    var tooltipNumeric = "Use expressions such as: '>=10 AND <50', '>100 OR <-100' to filter rows.";
    var tooltipText = "Type search terms to filter rows.";

    var FilterRow = React.createClass({
      render : function() {
        var ncol = _.size(this.props.columnDefs);
        var colIndex = -1;
        var cells = _.map(this.props.columnDefs, function(def, colId) {
          colIndex++;
          if(def.filter===false) {
            return <NoFilter key={colId} column={colId}/>;
          }
          if(def.type === 'atom') {
            if(!def.levels || def.levels.length === 0) {
              return <NoFilter key={colId} column={colId}/>;
            }
            return <DDFilter column={colId}
                             key={colId}
                             format={def.format}
                             filterValue={this.props.filters[colId]}
                             tableId={this.props.tableId}
                             options={def.levels}/>
          }
          var placeholder, tooltip, orient = "s";
          var midCol = Math.round(ncol / 2);
          if(colIndex <= midCol) {
            orient = "se";
          } else if (colIndex > midCol) {
            orient = "sw";
          }
          if(def.type === 'numeric') {
            placeholder = '>=0';
            tooltip = tooltipNumeric; 
          } else if (def.type === 'string') {
            placeholder = 'Search...';
            tooltip = tooltipText; 
          } else {
            placeholder = '';
          }
          return <InputFilter 
                  key={colId} 
                  column={colId} 
                  filterValue={this.props.filters[colId]}
                  placeholder={placeholder}
                  tooltipOrient={orient}
                  tooltip={tooltip}
                  tableId={this.props.tableId} />
        }, this); 
        return (
          <div className="dg-table-row filter">
            {cells}
          </div>
        )
      }
    });

    return FilterRow;
  });
