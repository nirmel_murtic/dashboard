'use strict';

angular.module('common')
  .factory('DataGrid', function($window, DGHeader, DGRow, DGFilter, DGPagination) {
    /* jshint quotmark:false, asi:true */
    return React.createClass({
      getInitialState : function() {
        window.dgt = this;
        return {
          resizing : false,
          resizeColumnLeft : null,
          resizeColumnRight : null,
          resizeFrom : null,
          columnWidths : null, 
          columnWidthDeltas : {}
        };
      },
      setInitialColumnWidths : function(colId, width) {
        this.state.columnWidths = this.state.columnWidths || {};
        this.state.columnWidths[colId] = width;
        this.setState({ columnWidths : this.state.columnWidths });
      },
      generateStyles : function() {
        if(!this.state.columnWidths) {
          return '';
        }
        var colWidths = this.state.columnWidths;
        var deltaWidths = this.state.columnWidthDeltas || {};
        var css = _.reduce(colWidths, function(css, w, colId) {
          var delta = this.state.resizing ? 0 : (deltaWidths[colId] || 0);
          css += ('.' + this.props.tableId);
          css += (' .dg-' + colId + '{');
          css += 'width:';
          css += ( (w + delta) + 'px;}' );
          return css;
        }, '', this);
        return css;
      },
      normalize : function() {
        var node = this.refs.tableWrapper.getDOMNode(); 
        var width = _.reduce(this.state.columnWidths, function(width, colw, colId) {
          this.state.columnWidths[colId] = colw + (this.state.columnWidthDeltas[colId] || 0);
          return width + this.state.columnWidths[colId]; 
        }, 0, this);
        var renderedWidth = $(node).width();
        if(!renderedWidth) {
          return;
        }
        var grows, colWidths;
        if(!width) {
          width = renderedWidth;
          colWidths = _.mapValues(this.props.columnDefs, _.constant(0));
          grows = _.reduce(this.props.columnDefs, function(acc, def) {
            return acc + (def.columnGrow || 1);
          }, 0);

          if(grows > 0) {
            _.each(this.state.columnWidths, function(w, colId) {
              var grow = this.props.columnDefs[colId].columnGrow || 1;
              colWidths[colId] = width * grow / grows;
            }, this); 
          }
          this.setState({
            columnWidths : colWidths, 
            columnWidthDeltas : {} 
          });
          return; 
        }
        var f = renderedWidth / width;
        if(renderedWidth < width - 2 || renderedWidth > width + 2) {
          _.reduce(this.state.columnWidths, function(leftOvers, w, colId) {
            if(w) {
              leftOvers.before -= w; 
              w = w * f;
              w = Math.round(w);
              this.state.columnWidths[colId] = w;
              leftOvers.after -= w;
              f = leftOvers.after / leftOvers.before;
            }
            return leftOvers;
          }, {
            after : renderedWidth,
            before: width
          }, this); 
          this.setState({
            columnWidths : this.state.columnWidths, 
            columnWidthDeltas : {} 
          });
        }
      },
      fitColumn : function(colId) {
        if(!this.state.columnWidths) {
          return;
        }
        var currentWidth = this.state.columnWidths[colId];
        if(!currentWidth) {
          return;
        }
        var prevCols = this.prevColumns(colId);
        var x = _.reduce(prevCols, function(acc, col) {
          return acc + this.state.columnWidths[col];
        }, currentWidth, this);
        var node = this.refs.table.getDOMNode(); 
        var targetWidth = 0;  
        var spans = $(node).find('.data .dg-table-cell span');
        for(var i=0; i<spans.length; i++) {
          targetWidth = Math.max(targetWidth, spans[i].scrollWidth || 0);
        }
        if(targetWidth > 0) {
          this.resize(x, x - currentWidth + targetWidth, colId, this.nextColumn(colId), true);
        }
      },
      prevColumn : function(colId) {
        if(!this.state.columnWidths) {
          return null;
        }
        var prevCol, col;
        for(col in this.state.columnWidths) {
          if(prevCol && col === colId) {
            return prevCol;
          }
          prevCol = col;
        } 
        return null;
      },
      prevColumns : function(colId) {
        var cols = [], col = colId; 
        var prevCol = this.prevColumn(col);
        while(prevCol) {
          cols.push(prevCol);
          col = prevCol;
          prevCol = this.prevColumn(col);
        }
        cols.reverse();
        return cols;
      },
      nextColumn : function(colId) {
        if(!this.state.columnWidths) {
          return null;
        }
        var prevCol, col;
        for(col in this.state.columnWidths) {
          if(prevCol && prevCol === colId) {
            return col;
          }
          prevCol = col;
        } 
        return null;
      },
      nextColumns : function(colId) {
        var cols = [], col = colId; 
        var nextCol = this.nextColumn(col);
        while(nextCol) {
          cols.push(nextCol);
          col = nextCol;
          nextCol = this.nextColumn(col);
        }
        return cols;
      },
      resize : function(from, to, leftCol, rightCol, isFinal) {
        var deltas = this.state.columnWidthDeltas;
        var widths = this.state.columnWidths;

        var dist = Math.abs(from - to);
        var cols;

        if(to > from)  {
          if(widths[rightCol] + (deltas[rightCol] || 0) - dist < 30) {
            cols = this.nextColumns(rightCol);
            if(cols.length) {
              deltas[leftCol] = dist;
              _.each(cols, function(c) {
                if(widths[c] + (deltas[c] || 0) - dist / cols.length >= 30) {
                  deltas[c] = -dist / cols.length;
                } else {
                  deltas[leftCol] -= (dist / cols.length);
                }
              }); 
            }
          }
        }
        if(to <= from)  {
          if(widths[leftCol] + (deltas[leftCol] || 0) - dist < 30) {
            cols = this.prevColumns(leftCol);
            if(cols.length) {
              deltas[rightCol] = dist;
              _.each(cols, function(c) {
                if(widths[c] + (deltas[c] || 0) - dist / cols.length >= 30) {
                  deltas[c] = -dist / cols.length;
                } else {
                  deltas[rightCol] -= (dist / cols.length);
                }
              }); 
            }
          }
        }
        if(to <= from) {
          dist = -dist;
        }
        if(!cols) {
          deltas[leftCol] = (dist || 0);
          deltas[rightCol] = (-dist || 0);
        }
        if(isFinal) {
          for(var colId in widths) {
            widths[colId] = widths[colId] + (deltas[colId] || 0);
          }
          this.setState({
            columnWidths : widths,
            columnWidthDeltas : {}
          });
        } else {
          this.setState({
            columnWidthDeltas : deltas
          });
        }
      },
      onDragEnd : function(event) {
        event.dragColumn = false; 
        if(!this.state.resizing) {
          return;
        }
        event.preventDefault();
        event.stopPropagation();
        var domNode = React.findDOMNode(this);
        var x = event.pageX - $(domNode).offset().left;
        this.resize(this.state.resizeFrom, x, this.state.resizeColumnLeft, this.state.resizeColumnRight, true);
        this.setState({
          resizing : false,
          resizeFrom : null,
          resizeColumnLeft : null,
          resizeColumnRight : null
        }); 
      },
      onDragStart : function(event) {
        var shouldFit;
        if(!event.dragColumn) {
          return;
        } else if(this.state.firstClick && event.timeStamp - this.state.firstClick < 300) {
          shouldFit = true;
        }
        if(this.state.resizing) {
          return;
        }
        this.normalize();
        event.preventDefault();
        event.stopPropagation();
        // this.componentDidMount();
        var domNode = React.findDOMNode(this);
        var x = event.pageX - $(domNode).offset().left;
        if(!this.state.columnWidths) {
          return;
        }
        // var deltas = this.state.columnWidthDeltas;
        var leftWidths = _(this.state.columnWidths)
          .pairs()
          .value();
        var rightWidths = leftWidths.slice(1);
        leftWidths.pop();
        var i, len, edgeX=0, leftCol, rightCol, shouldResize = false;
        for(i=0, len=leftWidths.length; i<len; i++) {
          edgeX = edgeX + leftWidths[i][1];
          if(x > edgeX - 4 && x < edgeX + 4) {
            leftCol = leftWidths[i][0];
            rightCol = rightWidths[i][0];
            x = edgeX;
            shouldResize = true;
            break;
          } 
        }
        if(shouldResize) {
          this.setState({
            resizing : true,
            firstClick : event.timeStamp,
            resizeFrom : x,
            resizeColumnLeft : leftCol,
            resizeColumnRight : rightCol
          });
        }
      },
      onDragColumn : function(event) {
        if(!this.state.resizing) {
          return;
        }
        event.preventDefault();
        event.stopPropagation();
        var domNode = React.findDOMNode(this);
        var x = event.pageX - $(domNode).offset().left;
        this.resize(this.state.resizeFrom, x, this.state.resizeColumnLeft, this.state.resizeColumnRight);
      },

      renderPagination : function(cursor, nrow, pageSize) {
        var p = cursor; 
        var pageCount = Math.ceil(nrow / pageSize);

        var currentPage = Math.floor(p / pageSize);
        var pages = [];
        if(pageCount > 1) {
          pages.push(
            <DGPagination 
              cursor = { (currentPage-1)*pageSize } 
              key={"prev"}
              faIcon="fa-chevron-left"
              tableId = {this.props.tableId}
              disabled={ currentPage === 0 } />
          );
          for(var i=0; i<pageCount; i++) {
            pages.push(
              <DGPagination page={i} 
                            active={ currentPage===i } 
                            tableId = {this.props.tableId} 
                            key={i} cursor={i * pageSize} />
            );
          }
          pages.push(
            <DGPagination 
              cursor = { (currentPage+1)*pageSize }
              faIcon="fa-chevron-right"
              key={"next"}
              tableId = {this.props.tableId}
              disabled={ currentPage === pageCount-1 } />
          );
        }
        return pages;
      },

      componentDidMount : function() {
        var self = this;
        self.setState({ columnWidthDeltas : {} });
        self.refs.header.saveColumnWidths();
        // var ncol = _.size(this.state.columnWidths);
        var width = _.reduce(this.state.columnWidths, function(width, colw) {
          return width + colw; 
        }, 0);
        var grows = _.reduce(this.props.columnDefs, function(acc, def) {
          return acc + (def.columnGrow || 1);
        }, 0);

        if(grows > 0) {
          _.each(this.state.columnWidths, function(w, colId) {
            var grow = this.props.columnDefs[colId].columnGrow || 1;
            this.state.columnWidths[colId] = width * grow / grows;
          }, this); 
        }
        $(window).resize(this.normalize);
        setTimeout(this.normalize, 1000);
      },
      componentWillUnmount : function() {
        $(window).off('resize', this.normalize);
      },
      componentWillReceiveProps : function(nextProps) {
        if(nextProps.reset) {
          this.replaceState(this.getInitialState());
        }
      },
      componentDidUpdate : function() {
        if(!this.state.resizing) {
          this.normalize();
        }
      },
      render : function() {
        var data = this.props.data;
        var colDefs = this.props.columnDefs;
        var rows, i, indices, index, nrow = 0;


        if(!this.props.dirty && this.rowsCache) {
          rows = this.rowsCache;
        } else {
          rows = [];
          indices = _.flatten(this.props.view);
          for(i=this.props.cursor; i<indices.length; i++) {
            index = indices[i];
            if(nrow >= this.props.maxRows) {
              break;
            }
            rows.push(
              <DGRow rowData={data[index]} columnDefs={colDefs} key={index}/>
            );
            nrow++;
          }
          /*
          rows = _.map(this.props.view, function(group) {
            return _.map(group, function(index) {
              return <DGRow rowData={data[index]} columnDefs={colDefs} key={index}/>
            });
          });
          rows = _.flatten(rows);
          */
          this.rowsCache = rows;
          // this is generally bad practice in React, but in this case, I know what I'm doing
          this.props.dirty = false;
        }
        var barx, barstyle = { display: 'none' };
        var col;
        if(this.state.resizing && this.state.columnWidths) {
          barx = 0;
          for(col in this.props.columnDefs) {
            if(col === this.state.resizeColumnRight) {
              break;
            }
            barx = barx + (this.state.columnWidths[col] || 0) + (this.state.columnWidthDeltas[col] || 0); 
          }
          if(barx) {
            barstyle.left = (barx-3) + 'px';
            barstyle.display = 'block';
          }
        }
        var pages = this.renderPagination(this.props.cursor, this.props.nrow, this.props.maxRows);
        return (
          <div ref="tableWrapper">
            <style>
              {this.generateStyles()}
            </style>
            <div className={"dg-table " + this.props.tableId} onMouseDown={ this.onDragStart } onMouseUp={ this.onDragEnd } onMouseMove={ this.onDragColumn } ref="table">
              <div className={ "dg-table-vbar " + this.props.tableId } style={barstyle}></div>
              <DGHeader columnDefs={colDefs} 
                        tableId = {this.props.tableId}
                        sort={this.props.sort} 
                        onMount={ this.setInitialColumnWidths } ref="header"/>
              <DGFilter columnDefs={colDefs} 
                        filters = {this.props.filters}
                        tableId = {this.props.tableId} />
              {rows}
            </div>
            <ul className="dg-pagination">{pages}</ul>
          </div>
        );
      } 
    }); 
  });
