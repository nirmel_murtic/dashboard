'use strict';

angular.module('common')
  .factory('DGRow', function(DGColumnBorder) {
    /* jshint quotmark:double, asi:true */
    var DataCell = React.createClass({
      render : function() {
        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <DGColumnBorder />
            <span className={"text-" + this.props.align}>{ this.props.value }</span>
          </div>
        ); 
      }
    }); 

    var DataRow = React.createClass({
      render : function() {
        var columnDefs = this.props.columnDefs;
        var rowData = this.props.rowData;
        var cells = _.map(columnDefs, function(def, colId) {
          var value = def.format ? def.format(rowData[colId]) : rowData[colId];
          if(def.reactComponent) {
            return React.createElement(def.reactComponent, { column: colId, value: value, rowData: rowData, columnDef: def, key: colId });
          }
          return <DataCell column={colId} value={value} key={colId} align={def.type === "numeric" ? "right" : "left"}/>
        });
        return (
          <div className="dg-table-row data">
            {cells}
          </div>
        );
      }
    });

    return DataRow;
  });
