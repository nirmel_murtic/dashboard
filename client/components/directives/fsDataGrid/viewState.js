'use strict';

angular.module('common')
  .factory('DataGridViewState', function(memoizeLast, parseNumericFilter, evalNumericFilter) {
    var constNull = _.constant(null);
    var constTrue = _.constant(true);
    function filterFunctionFactory(colId, colDef) {
      var toFilter;
      if(colDef.type === 'atom') {
        toFilter = _.memoize(function(str) {
          return function(d) {
            return d[colId] === str; 
          };
        }); 
      } else if (colDef.type === 'numeric'){
        toFilter = memoizeLast(function(str) {
          str = str ? str.toUpperCase() : '';
          if(!str) {
            return constTrue;
          }
          var ast;
          try {
            ast = parseNumericFilter(str);
            return function(d) {
              if(!_.isNumber(d[colId]) || isNaN(d[colId])) {
                return false; 
              }
              return evalNumericFilter(ast, d[colId]);
            };
          } catch(err) {
            return constTrue;
          } 
        }); 
      } else if (colDef.type === 'string') {
        toFilter = memoizeLast(function(str) {
          return function(d) {
            return d[colId].toString().toLowerCase().indexOf( str.toLowerCase() ) > -1;  
          };
        }); 
      }
      return toFilter;
    }
    var arrayIndices = _.memoize(function(arr) {
      return _.range(arr.length);
    }, function(a) {
      return a.length;
    });

    var toSorter = _.memoize(function (prop, order) {
      order = order === 'asc' ? 1 : -1;
      return function(a, b) {
        a = a[prop];
        b = b[prop];
        if(_.isNumber(a) || _.isNumber(b)) {
          if(a === null || typeof a === 'undefined' || isNaN(a) || a === Infinity || a === -Infinity) {
            return 1;
          } else if (b === null || typeof b === 'undefined' || isNaN(b) || b === Infinity || b === -Infinity) {
            return -1;
          }
        }
        if(a > b) {
          return order;
        }
        if(a < b) {
          return -order;
        }
        return 0;
      };
    }, function(p, o) {
      return p + ':' + o;
    });

    function DGViewState(columnDefs) {
      // var ncol = _.size(columnDefs);
      this.columnWidths = _.mapValues(columnDefs, _.constant(1));
      this.filters = _.mapValues(columnDefs, constNull);
      this.filterStrings = _.mapValues(columnDefs, _.constant(''));
      this.groupBy = null;
      this.sortBy = {
        by : null,
        order : 'aesc'
      };
      this.createFilterFromString = _.mapValues(columnDefs, function(def, colId) {
        return filterFunctionFactory(colId, def);
      });
      this.globalFilter = (function(d) {
        return _.reduce(this.filters, function(yes, f) {
          if(yes === false) {
            return yes;
          }
          if(f === null) {
            return yes; 
          }
          return yes && f(d); 
        }, true);
      }).bind(this);
    } 
    DGViewState.prototype = {
      useFilter : function(colId, filterStr) {
        this.filterStrings[colId] = filterStr || '';
        this.filters[colId] = filterStr ? this.createFilterFromString[colId](filterStr) : null;
        return this;
      },
      useGroupBy : function(colId) {
        this.groupBy = colId;
        return this;
      },
      useSort : function(colId, order) {
        this.sortBy.by = colId;
        this.sortBy.order = order || 'aesc';
        return this;
      },
      hasFilter : function() {
        return _.any(this.filters, function(f) {
          return f !== null;
        }); 
      },
      processGroupBy : function(data) {
        var colId = this.groupBy;
        if(!colId) {
          return [arrayIndices(data)];
        }
        return groupIndexBy(data, colId);
      },
      processFilter : function(data, indices) {
        var f = this.globalFilter; 
        if(!this.hasFilter()) {
          return indices ? indices : arrayIndices(data);
        }
        if(indices) {
          f = byIndex(data, f);
          return indices.map(function(groupData) {
            return filterIndex(groupData, f);
          });
        } else {
          return filterIndex(data, f);
        }
      },
      processSort : function(data, indices) {
        if(!this.sortBy || !this.sortBy.by) {
          return indices ? indices : arrayIndices(data);
        }
        var f = toSorter(this.sortBy.by, this.sortBy.order);    
        if(indices) {
          f = byIndex(data, f);
          return indices.map(function(groupData) {
            return stableSort(groupData, f);
          });
        } else {
          return sortIndex(data, f);
        }
      },
      getGroupedFilteredAndSortedData : function(data) {
        var indices = this.processGroupBy(data);
        indices = this.processFilter(data, indices);
        indices = this.processSort(data, indices);
        return indices;
      }
    };

    function byIndex(arr, fn) {
      function take(i) {
        return arr[i];
      }
      return function() {
        var args = _.toArray(arguments);
        args = _.map(args, take);
        return fn.apply(null, args);
      };
    }

    function filterIndex(arr, fn) {
      var i, len = arr.length;
      var out = [];
      for(i=0; i<len; i++) {
        if(fn(arr[i])) {
          out.push(i);
        }
      }
      return out;
    }

    function stableSort(arr, fn) {
      var out = _.range(arr.length);
      out.sort(function(a, b) {
        var order = fn(arr[a], arr[b]); // 0, 1, -1
        if(order === 0) {
          return a > b ? 1 : -1; // make sort stable
        }
        return order;
      });
      for(var i=0, len=out.length; i<len; i++) {
        out[i] = arr[ out[i] ];
      }
      return out;
    }

    function sortIndex(arr, fn) {
      var out = _.range(arr.length);
      out.sort(function(a, b) {
        var order = fn(arr[a], arr[b]); // 0, 1, -1
        if(order === 0) {
          return a > b ? 1 : -1; // make sort stable
        }
        return order;
      });
      return out;
    }

    function groupIndexBy(arr, prop) {
      if(!prop) {
        return [_.range(arr)];
      }
      var getter;
      if(!_.isFunction(prop)) {
        getter = function(d) {
          return d[prop];
        };
      } else {
        getter = prop;
      }
      var uniqs = {}, val;
      for(var i=0, len=arr.length; i<len; i++) {
        val = getter(arr[i]);
        if(!uniqs[val]) {
          uniqs[val] = [];
          uniqs[val].value = val;
        }
        uniqs[val].push(i);
      }
      return _.toArray(uniqs);
    }


    return {
      init : function(columnDefs) {
        return new DGViewState(columnDefs);
      },
      // shim to treate view state as immutable object
      update : function(oldState, patch) {
        var newState = Object.create(DGViewState.prototype);
        return _.assign(newState, oldState, patch);
      }
    };

  })
  .factory('TableStore', function() {
    function TableStore(columnDefs) {
      this.columnDefs = columnDefs;
      this.groupBy = null;
      this.filters = null;
      this.sortBy = null;
    } 
    TableStore.prototype = {
    
    };
  });
