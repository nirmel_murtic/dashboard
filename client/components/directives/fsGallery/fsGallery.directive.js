'use strict';

angular.module('common')
  .constant('setElementAspect', function(domNode, aspectRatio) {
    domNode.attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
  })
  .directive('fsGallery', function() {

    var template = [ 
    '<div class="gallery-wrapper">',
    '  <div class="gallery-container fix-ratio-wrapper">',
    '    <div class="contents">',
    '      <div class="gallery-everything">',
    '      </div>',
    '    </div>',
    '    <div class="slide-left" ng-click="Gallery.slideLeft()">',
    '      <i class="large fa fa-chevron-left"></i>',
    '    </div>',
    '    <div class="slide-right" ng-click="Gallery.slideRight()">',
    '      <i class="large fa fa-chevron-right"></i>',
    '    </div>',
    '  </div>',
    '  <div class="gallery-pagers" ng-if="Gallery.showPager">',
    '    <div class="pager" ng-repeat="item in Gallery.items"',
    '         ng-if="$index < Gallery.count()"',
    '         ng-class="{', 
    '           active: $index === Gallery.getOffset() || $index === Gallery.getOffset(Gallery.offset + 1) || $index === Gallery.getOffset(Gallery.offset + 2),', 
    '           \'active-2\' : $index === Gallery.getOffset(Gallery.offset + 1),',
    '           \'active-3\' : $index === Gallery.getOffset(Gallery.offset + 2)',
    '         }"',
    '         ng-click="Gallery.slideTo($index)">',
    '      <span class="num" ng-show="$index > 0">{{ $index+1 }}</span>',
    '      <span class="num" ng-show="$index === 0">Top</span>',
    '      <sup ng-show="$index > 0">{{ $index+1 | numRankSuffix }}</sup>',
    '    </div>',
    '  </div>',
    '</div>'

    ].join('');

    return {
      restrict: 'EA',
      scope : true,
      priority : 1010, 
      compile : function(tElement) {
        var renderedEl = angular.element(template);
        var everything = renderedEl.find('.gallery-everything');
        everything.append(tElement.children());
        var circular = tElement[0].hasAttribute('circular');
        if(circular) {
          // duplicate gallery items if circular rotation is required for smooth transition
          everything.append( everything.clone().children() );
        }
        tElement.html('').append(renderedEl);
        return angular.noop;
      }
    }; 
  })
  .directive('fsGallery', function($parse, setElementAspect) {
  
    return {
      restrict: 'EA',
      priority : 0, 
      controller : function() {
        this.items = [];
        this.offset = 0;
        this.scrollCount = 1;
        this.itemsPerPage = 3;
        this.secondTrack = false;

        this.getOffset = function(x) {
          x = arguments.length ? x : this.offset;
          var n = this.circular ? this.items.length / 2 : this.items.length;
          if(this.circular) {
            return x % n;
          } else {
            return x;
          }
        };

        this.count = function() {
          return this.circular ? this.items.length / 2 : this.items.length;
        };

        this.add = function(itemScope) {
          this.items.push(itemScope);
        };
        this.remove = function(itemScope) {
          var i = _.findIndex(this.items, itemScope);
          if(i > -1) {
            this.items.splice(i, 1);
          }
        };

        this.slideLeft = function() {
          var offset;
          var n = this.items.length;
          if(this.circular) {
            n = n / 2;
            this.offset = this.offset % n;
          }
          if(this.offset >= this.scrollCount) {
            offset = this.offset - this.scrollCount;
            this.transition(offset, false);
          } else if (this.circular) {
            offset = (this.offset - this.scrollCount) % n + n;
            this.offset = this.offset + n;
            this.transition(offset, true);
          }
        };
        this.slideRight = function() {
          var n = this.items.length;
          var offset;
          if(this.circular) {
            n = n / 2;
            this.offset = this.offset % n;
          }
          if(this.offset + this.scrollCount <= n - this.itemsPerPage) {
            offset = this.offset + this.scrollCount;
            this.transition(offset);
          } else if (this.circular) {
            offset = (this.offset + this.scrollCount) % n;
            if(offset === 0) {
              offset = n;
            }
            this.transition(offset);
          }
        };

        this.slideTo = function(i) {
          var n = this.items.length;
          if(this.circular) {
            n = n / 2;
          }
          i = +i;
          if(i >= this.offset && i < this.offset + this.itemsPerPage) {
            return;
          }
          if(i > n - this.itemsPerPage) {
            i = n - this.itemsPerPage;
          }
          if(i < 0) {
            i = 0; 
          }
          this.transition(i);
        };

        this.transition = angular.noop;
        this.computeTotalWidth = function() {
          var containerAspect = this.aspectRatio;
          return _.reduce(this.items, function(w, itemScope) {
            return w + itemScope.aspectRatio / containerAspect;
          }, 0);
        };
   

      },
      controllerAs : 'Gallery',
      compile : function(tElement) {
        var circular = tElement[0].hasAttribute('circular');
        return function fsGallery$postLink(scope, element, attrs, Gallery) {
          // jshint latedef:false
          Gallery.circular = circular;
          Gallery.itemsPerPage = $parse(attrs.itemsPerPage)(scope) || 3;
          Gallery.showPager = attrs.hidePager ? !$parse(attrs.hidePager)(scope) : true;
          Gallery.scrollCount = $parse(attrs.scrollCount)(scope) || 1;
          Gallery.aspectRatio = $parse(attrs.aspectRatio)(scope) || 2 * Gallery.itemsPerPage;

          var container = element.find('.gallery-container'); 
          var everything = container.find('.gallery-everything');
          setElementAspect(container, Gallery.aspectRatio);

          var watchOffset, assignOffset;
          if(attrs.offset) {
            watchOffset = $parse(attrs.offset);
            assignOffset = watchOffset.assign.bind(null, scope);
            watchOffset = watchOffset.bind(null, scope);
            scope.$watch(watchOffset, function(offset) {
              Gallery.slideTo(offset);
            });
          }

          var width = Gallery.computeTotalWidth() * 100 + '%'; 
          everything.css('width', width);

          everything = d3.select(everything[0]);
          everything.style('left', '0');

          setTotalWidth( Gallery.computeTotalWidth() );

          scope.$watch(Gallery.computeTotalWidth.bind(Gallery), setTotalWidth);
          
          function setTotalWidth(w) {
            everything.style('width', w * 100 + '%');
            var unit = 1 / Gallery.aspectRatio / w;
            _.each(Gallery.items, function(item) {
              item.element.css('width', (unit * item.aspectRatio * 100 + '%') );
            });
          }

          Gallery.transition = function(offset, useSecondTrack) {
            var i = 0, x1=0, x2;
            var n = Gallery.items.length;
            while(i < Gallery.offset) {
              x1 = x1 + Gallery.items[i % n].aspectRatio; 
              i++;
            }
            i = 0; x2=0;
            while(i < offset) {
              x2 = x2 + Gallery.items[i % n].aspectRatio; 
              i++;
            }
            x1 = -x1*100 / Gallery.aspectRatio + '%';
            x2 = -x2*100 / Gallery.aspectRatio + '%';
            if(useSecondTrack) {
              if(this.secondTrack === false) {
                everything.style('left', x1);
              }
              this.secondTrack = true;
            } else {
              if(this.secondTrack === true) {
                everything.style('left', x1);
              }
              this.secondTrack = false;
            }


            everything.transition().styleTween('left', function() {
              return d3.interpolateString(x1, x2); 
            }).each('end', function() {
              Gallery.offset = offset;
              if(assignOffset) {
                assignOffset(offset);
              }
              scope.$digest();
            });
          }; 

          if(__DEV__) {
            window.Gallerys = window.Gallerys || [];
            window.Gallerys.push(Gallery);
          }
        };
      }
    };
  })
  .directive('fsGalleryItem', function($parse) {
    return {
      restrict : 'EA',
      require : '^fsGallery',
      scope : false,
      link : {
        post : function(scope, element, attrs, Gallery) {
          // var isoScope = $rootScope.$new(true);
          var isoScope = {};
          var watchAspectRatio = $parse(attrs.aspectRatio).bind(scope);

          var ratio = watchAspectRatio() || 2;
          isoScope.element = element;
          isoScope.aspectRatio = watchAspectRatio();
          ratio = _.isNumber(ratio) && ratio > 0 ? ratio : 2;
          isoScope.aspectRatio = ratio;

          Gallery.add(isoScope);


        },
        pre : angular.noop
      }
    }; 
  });

