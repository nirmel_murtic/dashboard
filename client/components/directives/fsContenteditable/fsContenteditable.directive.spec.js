'use strict';

describe('Directive: fsContenteditable', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('app/fsContenteditable/fsContenteditable.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));
});
