'use strict';

angular.module('common')
  .directive('fsLabels', function(){
  return {
    restrict: 'E',
    scope: {
    	ngModel: '=',
      toShow: '@'
    },
    replace:true,
    templateUrl: 'components/directives/fsLabels/fsLabels.directive.html',
    link: function(scope, element){
      scope.currentLabel = '';

      if ( !Array.isArray(scope.ngModel) ){
        console.warn('The object inputted into the fsLabels directive is NOT an array, this is corrected but for best results, please initialize an array.');
        // scope.ngModel = [{'name':'mohamed', 'age': 23, 'hobbies': ['soccer', 'paintball', 'basketball']}, {'name':'mohamed', 'age': 23, 'hobbies': ['soccer', 'paintball', 'basketball']}, {'name':'mohamed', 'age': 23, 'hobbies': ['soccer', 'paintball', 'basketball']}]
      	scope.ngModel = [];
      }

      element.on('click', function(){
        element[0].querySelector('input').focus();
      });

      scope.addLabel = function() {
        if (!scope.currentLabel.length) {
          return alert('Cannot have empty label!');
        }

        var toAppend = null;

        // Check if the input is an object or it is a string.
        try{
          toAppend = JSON.parse(scope.currentLabel);

          // Do not allow numbers to be labels
          if(getTypeName(toAppend) === 'Number') {
            return alert('Cannot have numbers as a label!');
          }
        } catch(e){
          console.log('Object not detected, must be string');

          if (angular.isString(scope.currentLabel) ){
            toAppend = scope.currentLabel;
          }

        }
        // Check that appended element matches the rest of the array, throw error if does not
        checkIfAlike(toAppend);

        scope.ngModel.push(toAppend);
        scope.currentLabel = '';
      };

      scope.deleteLabel = function(index){
        console.log('Delete label at index:' + index);
        console.log('The model array now:', scope.ngModel);
        scope.ngModel.splice(index, 1);
      };

      function checkIfAlike(newInput){
        angular.forEach(scope.ngModel, function(element){
          if( getTypeName(element) !== getTypeName(newInput) ) {
            throw 'The element inputted into the labels type does not match that of the other elements';
          }
        });
      }

      // adding the css effects for the whole box
      angular.element(element[0].querySelector('input')).on('focus', function(){
        angular.element(element).addClass('label-body-focus');
      });

      angular.element(element[0].querySelector('input')).on('blur', function(){
        angular.element(element).removeClass('label-body-focus');
      });

      /*function getClassName(obj){
          var text = Function.prototype.toString.call(obj.constructor);
          return text.match(/function (.*)\(/)[1];
      }*/

      function getTypeName(obj){
          var text = Object.prototype.toString.call(obj);
          return text.match(/ (\w+)/)[1];
      }
    }
  };
});
