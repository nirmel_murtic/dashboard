'use strict';

angular.module('common')
  .directive('fsKpiCell', function($compile, $parse, invariant) {
    return {
      scope : true,
      restrict : 'A',
      templateUrl: 'components/directives/kpiCell/fsKpiCell.html',
      compile : function(element, attrs) {
        var kpiExpr = attrs.kpi,
            labelExpr = attrs.label,
            loading = attrs.loading;

        invariant(kpiExpr && labelExpr, 'Missing attribute for kpi or kpi label when compiling fs-kpi-cell.');

        var widthExpr = attrs.widthPercentage || '1';
        var getWidth = $parse(widthExpr);

        var valueContainer = element.find('.value');
        var labelContainer = element.find('.name');
        var loadingContainer = element.find('#loading');

        valueContainer.attr('ng-bind', kpiExpr);
        valueContainer.attr('ng-hide', loading);
        labelContainer.attr('ng-bind', labelExpr);
        if (angular.isUndefined(loading)) {
          loadingContainer.attr('ng-show', false);
        } else {
          loadingContainer.attr('ng-show', loading);
        }


        return function kpiCellPostlink(scope, element) {
          element[0].style.width = getWidth(scope) * 100 + '%';
        };
      }
    };
  });
