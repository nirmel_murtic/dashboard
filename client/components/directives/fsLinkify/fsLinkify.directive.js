'use strict';

angular.module('common')
  .directive('fsLinkify', function () {
    return {
      // template: '<span style="color: blue">{{url}} {{ text }} </span>',
      restrict: 'EA',
      require: 'ngModel',
      // replace: true,
      scope: {
      	text: '=ngModel'
      },
      link: function (scope, element) {
      	scope.messageElement = element;
      	var watchText = function(){
      		return scope.messageElement.text();
      	};
      	var checkForUrl = function(oldValue ){
	      	var url = oldValue.match(/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/);
	      	if(url){
	      		var element = angular.element('.post-message');
	      		element.html("<span style='color:blue;'>" +  url[0] + '</span>'); //jshint ignore:line	
	      	}
      	};
      	scope.$watch(watchText, checkForUrl);	

      }
    };
  });
