'use strict';

angular.module('common')
  .directive('fsFocusInput', function ($timeout) {
    return {
      scope: {
        trigger: '@fsFocusInput'
      },
      link: function (scope, element) {
        scope.$watch('trigger', function (value) {
          if (value === 'true') {
            $timeout(function () {
              element[0].focus();
            });
          }
        });
      }
    };
  });
