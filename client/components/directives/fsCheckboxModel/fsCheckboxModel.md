### Usage


Initiate array used in fs-checkbox-model in the controller before use in the directive.

<div ng-repeat="c in dummy">
    <input type="checkbox" fs-checkbox-value="c"  fs-checkbox-model="goals"> Hello {{c}}
</div>
