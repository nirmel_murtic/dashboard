'use strict';

angular.module('common')
.directive('ensureHasDate', function() {
	return {
		require: '^ngModel',
		scope: {
		  'ngModel': '=',
		  'ensureHasDate': '='
		},
		link: function($scope, ele, attrs, c) {
		  $scope.$watch(attrs.ngModel, function(/*newVal*/) {
		    console.log('newVal', $scope.ensureHasDate);
		    c.$setValidity('hasDate', true);
		  });
		}
	};
})
  .directive('fsCalendar', function (engage) {
  	var months = new Array('January',' February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

  	// Array to store month days
  	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

  	// Array to store week names
  	var weekDay = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');


    return {
      require: '^ngModel',
      scope: {
      	ngModel: '='
      },
      templateUrl: 'components/directives/fsCalendar/fsCalendar.html',
      restrict: 'A',
      link: function(scope) {
        scope.currentView = 'Week';

        scope.showPostSummary = function(post){
          if(post.summary.show){
            post.summary.show = false;   
          } else {
            post.summary.show = true;
          }
        };

        scope.getPostSummary = function(post){
          
          var summary = {
            time: moment(post.scheduledTime).calendar()
          };

          for(var i = 0; i < scope.allPages.length; i++){
            if(scope.allPages[i].pageId === post.pageId){              
              summary.page = scope.allPages[i];
              summary.page.channel = 'Facebook';
            } else if(scope.allPages[i].accountId === post.pageId){
              summary.page = {
                pageName: scope.allPages[i].name,
                image: scope.allPages[i],
                channel: 'Twitter'
              };
            }
          }

          return summary;

        };

        scope.insertScheduledPostsInCells = function(interval, i){
          for(var j=0; j < scope.week.length; j++){
            var cell = {};
            var cellDate = angular.copy(scope.week[j].date);              
            cell.scheduledPosts = [];

            if(scope.hours[i].label === ('12:00 am')){
              cell.date = new Date(cellDate.setMinutes(interval));
            }

            if(scope.hours[i].label.indexOf('am') !== -1){
              cell.date = new Date(new Date(scope.week[j].date.setHours(scope.hours[i].label.substr(0, scope.hours[i].label.indexOf(':')))).setMinutes(interval));
            } else {
              cell.date = new Date(new Date(scope.week[j].date.setHours(scope.hours[i].label.substr(0, scope.hours[i].label.indexOf(':')) + 12)).setMinutes(interval));
            }

            for(var l = 0; l < scope.scheduledPosts.length; l++){               
             if(scope.scheduledPosts[l].scheduledTime >= cell.date.getTime() && scope.scheduledPosts[l].scheduledTime <= cellDate.setMinutes(30)){
              scope.scheduledPosts[l].summary = scope.getPostSummary(scope.scheduledPosts[l]);
              cell.scheduledPosts.push(scope.scheduledPosts[l]);                     
             }
            }              
            scope.hours[i].cells.push(cell);
          }
        };

        scope.applyScheduledPosts = function(result){
          scope.scheduledPosts = result.data;

          scope.cells = [];

          for(var i=0; i < scope.hours.length; i++){
            scope.insertScheduledPostsInCells(0, i);
            scope.insertScheduledPostsInCells(30, i);
          }
        };

        scope.applyPages = function(result){
          scope.allPages = result.data;
          console.log(result.data);
        };

        engage.getAllSocialPages(scope.applyPages);
        engage.getAllScheduledPosts(scope.applyScheduledPosts);

        scope.initWeek = function(date){
          if (!date) {
            date = new Date();
          }

          scope.currentView = 'Week';
          scope.selectedDate = date;

          scope.hours = new Array({cells: [], label:'12:00 am'}, {cells: [], label:'1:00 am'}, {cells: [], label:'2:00 am'}, {cells: [], label:'3:00 am'}, {cells: [], label:'4:00 am'}, {cells: [], label:'5:00 am'}, {cells: [], label:'6:00 am'}, {cells: [], label:'7:00 am'}, {cells: [], label:'8:00 am'}, {cells: [], label:'9:00 am'}, {cells: [], label:'10:00 am'}, {cells: [], label:'11:00 am'}, {cells: [], label:'12:00 pm'}, {cells: [], label:'1:00 pm'}, {cells: [], label:'2:00 pm'}, {cells: [], label:'3:00 pm'}, {cells: [], label:'4:00 pm'}, {cells: [], label:'5:00 pm'}, {cells: [], label:'6:00 pm'}, {cells: [], label:'7:00 pm'}, {cells: [], label:'8:00 pm'}, {cells: [], label:'9:00 pm'}, {cells: [], label:'10:00 pm'}, {cells: [], label:'11:00 pm'});

          var day = date.getDate(),
              month = date.getMonth(),
              year = date.getFullYear();
          scope.daysInThisMonth = monthDays[month];
          scope.monthName = months[month];

          scope.currentWeek = 0;

          scope.week = [];

          var j = 0;

          for(var i = 0; i < 7; i++ ){
            day = {};

            if(date.getDay() === i){
              day.date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
              j = 1;
            }

            if(i < date.getDay()){
              day.date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - (date.getDay() - i));
            } else if(i > date.getDay()){
              day.date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + j);
              j++;
            }

            day.weekDayName = weekDay[i];
            scope.week.push(day);
          }

          scope.year = year;
          scope.weekDays = weekDay;          
        };

        scope.prevWeek = function(){
          scope.selectedDate = new Date(scope.selectedDate.getFullYear(), scope.selectedDate.getMonth(), scope.selectedDate.getDate() - 7);
          scope.initWeek(scope.selectedDate);
          scope.applyScheduledPosts({data: scope.scheduledPosts});
        };

        scope.nextWeek = function(){
          scope.selectedDate = new Date(scope.selectedDate.getFullYear(), scope.selectedDate.getMonth(), scope.selectedDate.getDate() + 7);
          scope.initWeek(scope.selectedDate);
          scope.applyScheduledPosts({data: scope.scheduledPosts});
        };

        scope.switchView = function(view){
          scope.currentView = view;
          if(view === 'Week'){
            scope.initWeek();
          } else if(view === 'Month'){
            scope.initMonth();
          }
        };


        if(scope.currentView === 'Week'){
          scope.$watch('ngModel', scope.initWeek);
        } else if(scope.currentView === 'Month'){
          scope.$watch('ngModel', scope.initMonth);
        }
      }
    };
  })
  .filter('blankIfNegative', function() {
    return function(input) {
      return input <= 0 ? ' ' : input;
    };
  });
