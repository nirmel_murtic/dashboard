'use strict';

angular.module('common')
  .directive('fsCustomRoleInline', function (regex) {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: 'components/directives/fsCustomRoleInline/fsCustomRoleInline.html',
      scope: {
        value: '=fsCustomRoleInline',
        onSave: '=',
        user: '='
      },
      link: function (scope, element, attrs) {
        scope.alphanumericRegex = regex.ALPHANUMERIC;

        element.bind('keydown keypress', function (event) {
          console.log('keydown');
          if(event.which === 13) {
            scope.$apply(function (){
              scope.saveCustomRole();
            });

            return false;
          }
        });

        scope.view = {
          editableValue: scope.value,
          editorEnabled: false
        };

        scope.enableEditor = function () {
          scope.view.editorEnabled = true;
          scope.view.editableValue = scope.value;
          setTimeout(function () {
            element.find('input')[0].focus();
          });
        };

        scope.disableEditor = function () {
          scope.customRoleForm.customRole.$invalid = false;
          scope.view.editorEnabled = false;
        };

        scope.saveCustomRole = function () {
          if(scope.customRoleForm.customRole.$invalid) {
            return;
          }
          if(!scope.view.editableValue){
            return;
          }
          if(attrs.onSave) {
            scope.onSave(scope.view.editableValue, scope.user);
          }

          scope.value = '';
          scope.disableEditor();
        };
      }
    };
});
