'use strict';

angular.module('common')
.directive('fsClickToEdit', function ($timeout, $parse) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'components/directives/fsClickToEdit/fsClickToEdit.html',
        scope: {
            // value: '=fsClickToEdit',
            editorEnabled: '=editorEnabled',
            placeholder: '=placeholder',
            enableEditor: '=enableEditor',
            showEditButton: '=showEditButton',
            showSaveCancel: '=showSaveCancel',
            isCurrency: '=isCurrency',
            isInteger: '=isInteger'
        },
        compile : function(element, attrs) {
          // var expr = attrs.fsClickToEdit;
          var modelExpr = _.trim(attrs.fsClickToEdit).split('|')[0];
          var valuesEl = element.find('.clicktoedit-value');

          var modelIn = $parse(modelExpr); 
          var modelOut = $parse(modelExpr);

          valuesEl.each(function() {
            $(this).attr('ng-bind', attrs.fsClickToEdit.replace(modelExpr, 'value'));
          });
          return function (scope, element, attrs) {


              scope.$watch(modelIn.bind(null, scope.$parent), function(val) {
                scope.value = val;
              });

              element.bind('keydown keypress', function (event) {
                  if(event.which === 13) {
                      scope.$apply(function (){
                          scope.save();
                      });

                      event.preventDefault();
                  }
              });

              var onChange = $parse(attrs.fsChange);

              scope.view = {
                  editableValue: scope.value,
                  editorEnabled: false,
                  placeholder: '',
                  oldValue: scope.value,
                  showEditButton: scope.showEditButton,
                  showSaveCancel: scope.showSaveCancel,
                  unlimited: attrs.unlimited
              };

              if(attrs.editorEnabled) {
                scope.view.editorEnabled = scope.editorEnabled;
              }

              if(attrs.editorEnabled) {
                scope.view.placeholder = scope.placeholder;
              }

              if(attrs.enableEditor) {
                scope.enableEditor = function() {
                  scope.internalEnableEditor();
                };
              }

              scope.internalEnableEditor = function () {
                scope.view.editorEnabled = true;
                scope.view.editableValue = scope.value;
                scope.view.canceled = false;
                scope.view.saved = false;
                $timeout(function () {
                  var elem = element.find('input')[0];

                  if(attrs.selectValue) {
                    elem.select();
                  }

                  elem.focus();
                }, 100);
              };

              scope.disableEditor = function () {
                  scope.view.editorEnabled = false;
              };

              scope.cancel = function() {
                scope.view.saved = false;

                if(scope.view.oldValue && scope.view.oldValue.trim().length) {
                  scope.view.canceled = true;
                  scope.view.editableValue = scope.view.oldValue;
                  scope.disableEditor();
                } else {
                  scope.view.editableValue = '';
                }
              };

              scope.save = function () {
                if(!scope.view.saved) {
                  scope.view.saved = true;

                  $timeout(function () {
                    scope.view.editableValue = String(scope.view.editableValue);

                    if (!scope.view.canceled && scope.view.editableValue.trim().length) {
                      var temp = null;

                      if (scope.isCurrency){
                        temp = parseFloat(scope.view.editableValue);

                        if(isNaN(temp)) {
                          scope.view.editableValue = scope.view.oldValue;
                        } else {
                          scope.value = temp;
                        }
                      } if(scope.isInteger) {
                        temp = parseInt(scope.view.editableValue);

                        if(isNaN(temp)) {
                          scope.view.editableValue = scope.view.oldValue;
                        } else {
                          scope.value = temp;
                        }
                      } else {
                        scope.value = scope.view.editableValue;
                      }

                      modelOut.assign(scope.$parent, scope.value);

                      scope.disableEditor();

                      onChange(scope.$parent);
                    }
                  }, 0);

                }
              };
          };
        }
    };
});
