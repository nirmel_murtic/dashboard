'use strict';

describe('Directive: fsClickToEdit', function () {

  // load the directive's module
  beforeEach(module('common'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fs-click-to-edit>this is the fsClickToEdit directive</fs-click-to-edit>');
    element = $compile(element)(scope);

    expect(element.text()).toBe('this is the fsClickToEdit directive');
  }));
});
