'use strict';

angular.module('common')
.constant('dropdownStates', {
	mouseover: 'mouseleave',
	mouseenter: 'mouseleave',
	click: 'click'
})

.controller('dropdownCtrl', function($scope, $element, $document, dropdownStates, $timeout){
	var self = this,
	scope = $scope;

	scope.isOpen = false;

	self.eventTrigger = function(onState){
		self.onState = onState;

    if(self.isOpen && $element.hasClass('open')) {
      self.close();
    } else {
      self.open();
    }
	};

	self.open = function(){
		self.isOpen = true;
		$element.addClass('open');

		if (self.onState === 'click'){
			$document.bind(self.onState, closeDropdown);
		}else{
			$element.bind(dropdownStates[self.onState], self.close);
		}
	};

	self.close = function(){
		self.isOpen = false;
		$element.removeClass('open');

		if (self.onState === 'click'){
			$document.unbind(self.onState, closeDropdown);
		}
	};

  self.limit = function() {
    var itemsForLimit = 0;
    $timeout(function() {
      itemsForLimit = $element.find('ul')[0].children.length;
      var listItem = $( 'li' ).outerHeight();
      var listLength = (listItem*scope.dropdownLimiter)/2.35;
      if(scope.dropdownLimiter < itemsForLimit){
        $element.find('ul').addClass('dropdown-limiter');
        $element.find('ul').css('height', listLength + 'px');
      }
    }, 0);
  };

	var closeDropdown = function(event){
	    // Check if attr is present and that the clicked element is part of the dropdown ul
	    if(angular.isDefined( angular.element($element.find('ul')[0]).attr('click-stay') ) &&
	    	$element.find('ul')[0].contains(event.target)) {
	    	  return;
      } else if($element.find('ul')[0].contains(event.target)){
        self.close();
      }

	    // Making sure we don't get double event trigger
	    if ( !$element[0].contains(event.target) ){
	    	self.close();
	    }
	};
})

.directive('fsDropdown', function(){
	return {
		restrict: 'C',
		scope:{
			closeDropdown: '@',
      dropdownLimiter: '=dropdownLimiter',
      disableDropdown: '='
		},
		controller: 'dropdownCtrl',
		link: function(scope, element, attrs, dropdownCtrl) {
      if(attrs.dropdownLimiter) {
        dropdownCtrl.limit();
      }
			if(attrs.closeDropdown) {
				scope.$on(attrs.closeDropdown, function() {
					dropdownCtrl.close();
				});
			}
      scope.$watch('disableDropdown', function(value) {
        if(value){
          element.addClass('disabled-dropdown');
        } else {
          element.removeClass('disabled-dropdown');
        }
      });
		}
	};
})

.directive('fsDropdownMenu', function() {
  return {
    restrict : 'C',
    require : '?^fsDropdown',
    scope : true,
    link : function(scope, element, attrs, dropdownCtrl) {
      if(dropdownCtrl) {
        scope.dropdownApi = dropdownCtrl;
      }
    }
  };
})

.directive('fsDropdownLink', function(){
	return {
		restrict: 'C',
		require: '^fsDropdown',
		link: function(scope, elem, attrs, dropdownCtrl){

			if (attrs.onState === 'hover') {
		        throw 'Cannot have hover with Angularjs, replace with "mouseover"';
	        }

			elem.on('click', function(e){
		        e.preventDefault();
      		});

			elem.on(attrs.onState, function(event){
				event.preventDefault();

				dropdownCtrl.eventTrigger(attrs.onState);
			});
		}
	};
});
