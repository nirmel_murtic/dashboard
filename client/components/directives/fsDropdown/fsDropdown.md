## Usage:

4 different dropdown types: Default, Link, Button, and Social Pages

Add class "link", "button", or "page" for each type on fs-dropdown element

Attribute `click-stay` is used if you'd like the the dropdown to persist after click on it
Attribute `on-state` allows for `mouseenver`, `mouseover` and `click`


```

Default with Click-Stay

```
<div class="fs-dropdown">
    <div class="fs-dropdown-link" on-state="click" >
        <span>Drop me {{counter}}</span>
        <iconic data-src="caret" data-direction="bottom" class="iconic-sm goals-caret-coloring"><iconic>
    </div>
    <ul class="fs-dropdown-menu" click-stay>
        <li ng-click="counter = counter + 1">Link 1</li>
        <li>Link 2</li>
        <li>Link 3</li>
    </ul>
</div>

```
On Mouseenter

<div class="fs-dropdown">
      <div class="fs-dropdown-link" on-state="mouseenter">
        <span>{{theName ? : theName : 'Drop me'}}</span>
         <iconic data-src="caret" data-direction="bottom" class="iconic-sm goals-caret-coloring"><iconic>
      </div>
      <ul class="fs-dropdown-menu">
        <li ng-click="theName = 'Item ' ">Item 1</li>
        <li>Item 2</li>
    </ul>
</div>

```

Link

<div class="fs-dropdown link">
  <div class="fs-dropdown-link" on-state="mouseenter">
    <span>{{theName ? : theName : 'Drop me'}}</span>
    <iconic data-src="caret" data-direction="bottom" class="iconic-sm goals-caret-coloring"></iconic>
  </div>
  <ul class="fs-dropdown-menu">
      <li ng-click="theName = 'Item ' ">Item 1</li>
      <li>Item 2</li>
  </ul>
</div>

```

Button

<div class="fs-dropdown button">
  <button class="fs-dropdown-link btn-solid btn-primary" on-state="click">
    <iconic data-src="caret" data-direction="bottom" class="iconic-sm goals-caret-coloring"></iconic>
  </button>      
  <ul class="fs-dropdown-menu drop-right">
    <li ng-click="chooseTime(time)" ng-repeat="time in times">
      {{time}}
    </li>
  </ul>         
</div>

```

Social Pages

<div class="fs-dropdown page">
  <!-- Dropdown to allow the user to choose the page. Becomes hidden once the user has selected it -->
  <div class="fs-dropdown-link" on-state="click">
      <div ng-hide="selectedPage">
          <i class="pull-right fa fa-caret-down"></i>
          <img src="" alt="facebook image" class="select-page-dropdown-image">
          <span>Select Page</span>
      </div>
  </div>
  <ul class="fs-dropdown-menu">
      <li ng-repeat="page in pages" ng-click="selectedPage = page;">          
            <img ng-src="{{account.image || account.social.image}}" alt="facebook image">
            {{ account.name || account.pageName || account.groupName }}
      </li>
  </ul>
  <!-- When the user has selected a page, display the page chosen -->
  <div class="fs-dropdown-link" on-state="click">
    <div ng-show="selectedPage">
        <iconic data-src="caret" data-direction="bottom" class="iconic-sm goals-caret-coloring"></iconic>
        <img ng-src="{{selectedPage.image || selectedPage.social.image}}" alt="facebook image">
        <span>{{ selectedPage.name || selectedPage.pageName || selectedPage.groupName }}</span>
    </div>
  </div>
</div>

