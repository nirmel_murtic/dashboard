'use strict';

angular.module('common')

  .controller('fsAccordionCtrl', function($scope, $attrs){
  	this.groups = [];

  	// This will loop through all the group scopes and close them
  	this.closeAll = function(){

  		if ( angular.isDefined($attrs.closeOthers) && $scope.$eval($attrs.closeOthers)){
  			angular.forEach(this.groups, function(groupScope){
  				groupScope.isOpen = false;
  			});
  		}
  	};

  	// Adding a scope to the accordion directive
  	this.addGroup = function(group){
  		var self = this;
  		self.groups.push(group);
  	};
  })
  .directive('fsAccordion', [function () {
    return {
      templateUrl: 'components/directives/fsAccordion/fsAccordion.html',
      restrict: 'EA',
      controller: 'fsAccordionCtrl',
      transclude: true
    };
  }])
  .directive('fsAccordionGroup', [function () {
  	return {
  		restrict: 'EA',
  		require: '^fsAccordion',
  		scope: {
  			header: '@',
  			isOpen: '=',
        isDisabled: '=',
        percentage: '=',
        percentageTitle:'=',
        numOfSelected: '=',
        totalAvailable: '=',
        radioBtnFunction: '&'

  		},
      controller: function() {
        this.setHeading = function(element) {
          this.heading = element;
        };
      },
  		templateUrl: 'components/directives/fsAccordion/fsAccordionGroup.html',
  		transclude: true,
  		link: function (scope, element, attrs, fsAccordionCtrl) {


        scope.radioBtnClick = function () {
          console.log('click');
          if (scope.radioBtnFunction) {
            console.log('ima fn');
            scope.radioBtnFunction();
          }
        };

  			fsAccordionCtrl.addGroup(scope);

  			scope.toggleOpen = function(){

                if (scope.isDisabled === true){
                    return;
                }
  				// Close all first, then open self.
  				fsAccordionCtrl.closeAll();
  				scope.isOpen = !scope.isOpen;
  			};
  		}
  	};
  }])
  .directive('fsAccordionHeading', [function() {
    return {
      restrict : 'EA',
      require : '^fsAccordionGroup',
      transclude : true,
      template : '',
      link: function(scope, element, attr, accordionGroupCtrl, transclude) {
        // Pass the heading to the accordion-group controller
        // so that it can be transcluded into the right place in the template
        // [The second parameter to transclude causes the elements to be cloned so that they work in ng-repeat]
        accordionGroupCtrl.setHeading(transclude(scope, angular.noop));
      }
    };
  }])
  .directive('accordionTransclude', function() {
    return {
      require: '^fsAccordionGroup',
      link: function(scope, element, attr, controller) {
        scope.$watch(function() { return controller[attr.accordionTransclude]; }, function(heading) {
          if ( heading ) {
            element.html('');
            element.append(heading);
          }
        });
      }
    };
  });



//Usage:
// Accordion
// <fs-accordion close-others='true'>
//     <fs-accordion-group header='Some cool header over here 11111'>
//         Some transcluded text 11111
//     </fs-accordion-group>
//     <fs-accordion-group header='Some cool header over here 22222'>
//         Some transcluded text 22222
//     </fs-accordion-group>
//     <fs-accordion-group header='Some cool header over here 33333'>
//         Some transcluded text 33333
//     </fs-accordion-group>
// </fs-accordion>
