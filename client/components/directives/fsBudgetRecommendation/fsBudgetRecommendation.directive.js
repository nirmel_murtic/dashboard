/**
 * @author Sinan
 * @version 1
 **/

'use strict';

angular.module('common')
  .directive('fsBudgetRecommendation', function () {
    return {
      templateUrl: 'components/directives/fsBudgetRecommendation/fsBudgetRecommendation.html',
      restrict: 'E',
      replace: true,
      scope:{
        budgetPolicy: '=',
        budgetRecommendation: '=',
        currency: '@',
        applyBudgetRecommendation:'&'
      }
    };
  });