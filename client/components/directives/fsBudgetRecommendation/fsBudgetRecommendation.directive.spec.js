'use strict';

describe('Directive: fsBudgetRecommendation', function () {

  // load the directive's module and view
  beforeEach(module('common'));
  beforeEach(module('components/directives/fsBudgetRecommendation/fsBudgetRecommendation.html'));

  var scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

});