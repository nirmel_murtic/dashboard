'use strict';

angular.module('common')
    .directive('fsRadioToggle', function () {
        return {
            scope: {
                option1: '=',
                option2: '=',
                locked: '='
            },
            replace: true,
            require: 'ngModel',
            templateUrl: 'components/directives/fsRadioToggle/fsRadioToggle.html',
            link: function (scope, element, attrs, ngModelCtrl) { //jshint ignore:line

                ngModelCtrl.$render = function() {
                    scope.value = ngModelCtrl.$modelValue;
                };
                scope.toggleValue = function(){
                    if (!scope.locked) {
                        if(ngModelCtrl.$modelValue === scope.option1.value) {
                            ngModelCtrl.$setViewValue(scope.option2.value);
                        } else {
                            ngModelCtrl.$setViewValue(scope.option1.value);
                        }
                        ngModelCtrl.$render();
                    }
                };
            }
        };
    });
