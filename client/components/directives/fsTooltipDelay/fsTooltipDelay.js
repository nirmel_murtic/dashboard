'use strict';

angular.module('720kb.tooltips')
  .config(function($provide) {
    $provide.decorator('tooltipsDirective', function($delegate, $parse, $timeout) {  
      $delegate = $delegate[0];
      var _postLink = $delegate.link;
      $delegate.compile = function() {
        return function(scope, element, attrs, ctrls, transclude) {
          
          _postLink.call($delegate, scope, element, attrs, ctrls, transclude);

          // var hideTriggers = attrs.tooltipHideTrigger || 'mouseleave';
          var getDelay = $parse(attrs.fsTooltipDelay);
          var delay = getDelay(scope.$parent) || 0;
          // var CSS_PREFIX = 'accomplice ';
          // var speed = 450;

          var _showTooltip = scope.showTooltip;
          var _hideTooltip = scope.hideTooltip;

          var showTTPromise = null;

          scope.showTooltip = function showTooltip () {
            if(showTTPromise) {
              $timeout.cancel(showTTPromise);
              showTTPromise = null;
            }
            showTTPromise = $timeout(_showTooltip, delay);
            showTTPromise.then(function() {
              showTTPromise = null;
            });
          };
          
          scope.hideTooltip = function() {
            if(showTTPromise) {
              $timeout.cancel(showTTPromise);
              showTTPromise = null;
            }
            _hideTooltip();
          };

        };
      };
      return [$delegate];
    });
  });