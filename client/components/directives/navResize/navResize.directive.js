'use strict';

angular.module('common')
  .directive('navResize', function ($window) {
    return function (scope) {
      var w = angular.element($window);
      scope.getWindowDimensions = function () {
        return {
          'h': w.height(),
          'w': w.width()
        };
      };
      scope.$watch(scope.getWindowDimensions, function (newValue) {
        scope.windowHeight = newValue.h;
        scope.windowWidth = newValue.w;

        scope.parentStyle = function () {
          if(newValue.h >= 550 && newValue.h <= 750) {
            return {
              'height': 2.7 + 'em'
            };
          } else if (newValue.h >= 470 && newValue.h <= 550) {
            return {
              'height': 2.4 + 'em'
            };
          } else if(newValue.h <= 470) {
            return {
              'height': 2.1 + 'em'
            };
          }
        };

        scope.childStyle = function () {
          if(newValue.h >= 550 && newValue.h <= 750) {
            return {
              'margin-top': 0.35 + 'em'
            };
          } else if (newValue.h >= 470 && newValue.h <= 550) {
            return {
              'margin-top': 0.25 + 'em'
            };
          } else if(newValue.h <= 470) {
            return {
              'margin-top': 0.1 + 'em'
            };
          }
        };

        scope.iconsStyle = function () {
          var categorydivs = $('navicon svg');
          if(newValue.h >= 550 && newValue.h <= 750) {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.35 + 'em');
            });
          } else if (newValue.h >= 470 && newValue.h <= 550) {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.25 + 'em');
            });
          } else if(newValue.h <= 470) {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.1 + 'em');
            });
          } else {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.5 + 'em');
            });
          }
        };

        scope.iStyle = function () {
          var categorydivs = $('.child-section-logo i');
          if(newValue.h >= 550 && newValue.h <= 750) {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.35 + 'em');
            });
          } else if (newValue.h >= 470 && newValue.h <= 550) {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.25 + 'em');
            });
          } else if(newValue.h <= 470) {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.15 + 'em');
            });
          } else {
            $.each(categorydivs, function () {
              $(this).css('margin-top', 0.45 + 'em');
            });
          }
        };

      }, true);

      w.bind('resize', function () {
        scope.$apply();
      });
    };
  });
