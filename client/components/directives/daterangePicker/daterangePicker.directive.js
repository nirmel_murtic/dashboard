'use strict';

angular.module('common')
  // date range picker bug fixes go in here
  .config(function() {
    // create a temp data picker so we get access to DatePicker class's prototype
    // then we override it with correct version of buggy methods
    var el = $('<input>').appendTo('body').hide();
    var div = $('<div>');
    /* jshint ignore:start */
    var dateRangePrototype = Object.getPrototypeOf( el.daterangepicker({ parentEl: div }).data('daterangepicker') );

    var _renderCalendar = dateRangePrototype.renderCalendar;



    // begin fixes
    _.assign(dateRangePrototype, {

        timeChanged: function(e) {

          var cal = $(e.target).closest('.calendar'),
              isLeft = cal.hasClass('left');

          var hour = parseInt(cal.find('.hourselect').val() || 0, 10);
          var minute = parseInt(cal.find('.minuteselect').val() || 0, 10);
          var second = this.timePickerSeconds ? parseInt(cal.find('.secondselect').val() || 0, 10) : 0;

          if (!this.timePicker24Hour) {
              var ampm = cal.find('.ampmselect').val();
              if (ampm === 'PM' && hour < 12)
                  hour += 12;
              if (ampm === 'AM' && hour === 12)
                  hour = 0;
          }

          if (isLeft) {
              var start = this.startDate.clone();
              start.hour(hour);
              start.minute(minute);
              start.second(second);
              if(this.minDate && start.isBefore(this.minDate)) {
                start = this.minDate.clone();
              }
              this.setStartDate(start);
              if (this.singleDatePicker)
                  this.endDate = this.startDate.clone();
          } else if (this.endDate) {
              var end = this.endDate.clone();
              end.hour(hour);
              end.minute(minute);
              end.second(second);
              if(this.maxDate && end.isAfter(this.maxDate)) {
                end = this.maxDate.clone();
              } 
              this.setEndDate(end);
          }

          //update the calendars so all clickable dates reflect the new time component
          this.updateCalendars();

          //update the form inputs above the calendars with the new time
          this.updateFormInputs();

          //re-render the time pickers because changing one selection can affect what's enabled in another
          this.renderTimePicker('left');
          this.renderTimePicker('right');

      },

       updateCalendars: function() {
          if (this.timePicker) {
              var hour, minute, second;
              if (this.endDate) {
                  hour = parseInt(this.container.find('.left .hourselect').val() || 0, 10);
                  minute = parseInt(this.container.find('.left .minuteselect').val() || 0, 10);
                  second = this.timePickerSeconds ? parseInt(this.container.find('.left .secondselect').val() || 0, 10) : 0;
                  if (!this.timePicker24Hour) {
                      var ampm = this.container.find('.left .ampmselect').val();
                      if (ampm === 'PM' && hour < 12)
                          hour += 12;
                      if (ampm === 'AM' && hour === 12)
                          hour = 0;
                  }
              } else {
                  hour = parseInt(this.container.find('.right .hourselect').val() || 0, 10);
                  minute = parseInt(this.container.find('.right .minuteselect').val() || 0, 10);
                  second = this.timePickerSeconds ? parseInt(this.container.find('.right .secondselect').val() || 0, 10) : 0;
                  if (!this.timePicker24Hour) {
                      var ampm = this.container.find('.right .ampmselect').val();
                      if (ampm === 'PM' && hour < 12)
                          hour += 12;
                      if (ampm === 'AM' && hour === 12)
                          hour = 0;
                  }
              }
              this.leftCalendar.month.hour(hour).minute(minute).second(second);
              this.rightCalendar.month.hour(hour).minute(minute).second(second);
          }

          this.renderCalendar('left');
          this.renderCalendar('right');

          //highlight any predefined range matching the current start and end dates
          this.container.find('.ranges li').removeClass('active');
          if (this.endDate == null) return;

          var customRange = true;
          var i = 0;
          for (var range in this.ranges) {
              if (this.timePicker) {
                  if (this.startDate.isSame(this.ranges[range][0]) && this.endDate.isSame(this.ranges[range][1])) {
                      customRange = false;
                      this.chosenLabel = this.container.find('.ranges li:eq(' + i + ')').addClass('active').html();
                      break;
                  }
              } else {
                  //ignore times when comparing dates if time picker is not enabled
                  if (this.startDate.format('YYYY-MM-DD') == this.ranges[range][0].format('YYYY-MM-DD') && this.endDate.format('YYYY-MM-DD') == this.ranges[range][1].format('YYYY-MM-DD')) {
                      customRange = false;
                      this.chosenLabel = this.container.find('.ranges li:eq(' + i + ')').addClass('active').html();
                      break;
                  }
              }
              i++;
          }
          if (customRange) {
              this.chosenLabel = this.container.find('.ranges li:last').addClass('active').html();
              this.showCalendars();
          }

      },

    
      renderCalendar : function(side) {
        _renderCalendar.apply(this, arguments); 
        var calendar = side == 'left' ? this.leftCalendar : this.rightCalendar;
        var month = calendar.month.month();
        var table = this.container.find('.calendar.' + side + ' .calendar-table')
        var title, date, td, tds;
        tds = table.find('td[data-title]');
        tds.each(function() {
          td = $(this);
          title = td.attr('data-title');
          var row = title.substr(1, 1);
          var col = title.substr(3, 1);
          date = calendar.calendar[row][col];
          if(date.month() !== month && !td.hasClass('week')) {
            td.css('visibility', 'hidden');
          }
        });
      },
      clickDate: function(e) {

          if (!$(e.target).hasClass('available')) return;

          var title = $(e.target).attr('data-title');
          var row = title.substr(1, 1);
          var col = title.substr(3, 1);
          var cal = $(e.target).parents('.calendar');
          var date = cal.hasClass('left') ? this.leftCalendar.calendar[row][col] : this.rightCalendar.calendar[row][col];

          //
          // this function needs to do a few things:
          // * alternate between selecting a start and end date for the range,
          // * if the time picker is enabled, apply the hour/minute/second from the select boxes to the clicked date
          // * if autoapply is enabled, and an end date was chosen, apply the selection
          // * if single date picker mode, and time picker isn't enabled, apply the selection immediately
          //

          if (this.endDate || date.isBefore(this.startDate)) {
              if (this.timePicker) {
                  var hour = parseInt(this.container.find('.left .hourselect').val() || 0, 10);
                  if (!this.timePicker24Hour) {
                      var ampm = cal.find('.ampmselect').val();
                      if (ampm === 'PM' && hour < 12)
                          hour += 12;
                      if (ampm === 'AM' && hour === 12)
                          hour = 0;
                  }
                  var minute = parseInt(this.container.find('.left .minuteselect').val() || 0, 10);
                  var second = this.timePickerSeconds ? parseInt(this.container.find('.left .secondselect').val() || 0, 10) : 0;
                  date = date.clone().hour(hour).minute(minute).second(second);
                  /*
                   * Critical fixes for NaN bug, a.k.a: AC-3763
                   */
                  if(this.minDate && date.isBefore(this.minDate)) {
                    date = this.minDate.clone();
                  }
              }
              this.endDate = null;
              this.setStartDate(date.clone());
          } else {
              if (this.timePicker) {
                  var hour = parseInt(this.container.find('.right .hourselect').val() || 0, 10);
                  if (!this.timePicker24Hour) {
                      var ampm = this.container.find('.right .ampmselect').val();
                      if (ampm === 'PM' && hour < 12)
                          hour += 12;
                      if (ampm === 'AM' && hour === 12)
                          hour = 0;
                  }
                  var minute = parseInt(this.container.find('.right .minuteselect').val() || 0, 10);
                  var second = this.timePickerSeconds ? parseInt(this.container.find('.right .secondselect').val() || 0, 10) : 0;
                  date = date.clone().hour(hour).minute(minute).second(second);
                  /*
                   * Critical fixes for NaN bug, a.k.a: AC-3763
                   */
                  if(this.minDate && date.isBefore(this.minDate)) {
                    date = this.minDate.clone();
                  }
              }
              this.setEndDate(date.clone());
              if (this.autoApply)
                  this.clickApply();
          }

          if (this.singleDatePicker) {
              this.setEndDate(this.startDate);
              if (!this.timePicker)
                  this.clickApply();
          }

          this.updateView();

      },
      clickRange : function(e) {
        var label = e.target.innerHTML;
        this.chosenLabel = label;
        if (label === this.locale.customRangeLabel) {
          this.showCalendars();
        } else {
          var dates = this.ranges[label];
          this.startDate = dates[0];
          this.endDate = dates[1];
          this.hideCalendars();
          this.clickApply();
        }
      },
      updateMaxDate_ : function(maxDate) {
        this.maxDate = moment(maxDate);
      },
      updateRanges_ : function(ranges) {
        var range, start, end;
        ranges = _.assign({}, this.ranges || {}, ranges);
        for (range in ranges) {

          if (typeof ranges[range][0] === 'string') {
              start = moment(ranges[range][0], this.locale.format);
          } else {
              start = moment(ranges[range][0]);
          }

          if (typeof ranges[range][1] === 'string') {
              end = moment(ranges[range][1], this.locale.format);
          } else {
              end = moment(ranges[range][1]);
          }

          // If the start or end date exceed those allowed by the minDate or dateLimit
          // options, shorten the range to the allowable period.
          if (this.minDate && start.isBefore(this.minDate)) {
              start = this.minDate.clone();
          }

          var maxDate = this.maxDate;
          if (this.dateLimit && start.clone().add(this.dateLimit).isAfter(maxDate)) {
              maxDate = start.clone().add(this.dateLimit);
          }
          if (maxDate && end.isAfter(maxDate)) {
              end = maxDate.clone();
          }

          // If the end of the range is before the minimum or the start of the range is
          // after the maximum, don't display this range option at all.
          if ((this.minDate && end.isBefore(this.minDate)) || (maxDate && start.isAfter(maxDate))) {
              continue;
          }

          this.ranges[range] = [start, end];
      }

      var list = '<ul>';
      for (range in this.ranges) {
          list += '<li>' + range + '</li>';
      }
      list += '<li>' + this.locale.customRangeLabel + '</li>';
      list += '</ul>';
      this.container.find('.ranges ul').remove();
      this.container.find('.ranges').prepend(list);
      
      }
    });
    /* jshint ignore:end */
    // end fixes

    // removing temp elements

    el.remove();
    div.remove();
  })
  .directive('daterangePicker', function() {
    return {
      restrict : 'EA',
      require : 'ngModel',
      scope : {
        opts : '&?options'
      },
      template : [
        '<div class="fs-daterange dropdown">',
          '<div class="daterange-span">',
            '<i class="fa fa-calendar"></i>',
            '<span>{{ displayRange }}</span>',
            '<b class="fa fa-caret-down pull-right"></b>',
          '</div>',
        '</div>'
      ].join(''),
      link : function(scope, element, attrs, ngModelCtrl) {
        var defaults = {
          parentEl : element.find('.dropdown'),
          singleDatePicker: false,
          showDropdowns: false,
          showWeekNumbers: true,
          timePicker: true,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          opens: 'left',
          buttonClasses: ['btn', 'btn-sm'],
          applyClass: 'btn-primary btn-solid',
          cancelClass: 'btn-default btn-outline',
          separator: ' to ',
          locale: {
              applyLabel: 'Submit',
              cancelLabel: 'Cancel',
              fromLabel: 'From',
              toLabel: 'To',
              format: 'MM/DD/YYYY hh:mm A',
              customRangeLabel: 'Custom',
              daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
              monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
              firstDay: 1
          },
          dateLimit : {
            days : 365
          }
        };
        var opts = _.assign(defaults, scope.opts(),
          ngModelCtrl.$modelValue ? {
            startDate : ngModelCtrl.$modelValue[0],
            endDate : ngModelCtrl.$modelValue[1]
          }: {}
        );

        if(opts.dynamicMaxDate) {

        }

        var onSelect = function(start, end) {
          if(!isValidDate(start) && opts.minDate) {
            start = opts.minDate;
          }
          if(!isValidDate(end) && opts.maxDate) {
            end = opts.maxDate;
          }
          var newViewValue = [new Date(start), new Date(end)];
          scope.$apply(function() {
            if(!ngModelCtrl.$viewValue || !ngModelCtrl.$viewValue.length) {
              ngModelCtrl.$viewValue = [];
            }
            if(!isDatesEqual(newViewValue, ngModelCtrl.$viewValue)) {
              ngModelCtrl.$viewValue = newViewValue;
            } else {
              return;
            }
            ngModelCtrl.$setViewValue(newViewValue);
            ngModelCtrl.$validate();
            ngModelCtrl.$render();
          });
        };
        var jQueryDatepicker = element.find('.daterange-span').daterangepicker(opts, onSelect);

        scope.$watch(function() { return scope.opts(); }, function(opts) {
          opts = _.assign(defaults, scope.opts(),
            ngModelCtrl.$modelValue ? {
              startDate : ngModelCtrl.$modelValue[0],
              endDate : ngModelCtrl.$modelValue[1]
            }: {}
          );
          jQueryDatepicker.daterangepicker(opts, onSelect);
        });

        jQueryDatepicker.data('daterangepicker').hide();

        function isDateEqual(a, b) {
          return moment(a).valueOf() === moment(b).valueOf();
        }
        function isDatesEqual(a, b) {
          return  (a && b) &&
            a.length === b.length &&
            isDateEqual(a[0], b[0]) &&
            isDateEqual(a[1], b[1]);
        }
        function isValidDate(d) {
          if(!d) {
            return false;
          }
          if(d instanceof Date || d._isAMomentObject) {
            return d.toString() !== 'Invalid Date';
          }
          return false;
        }

        ngModelCtrl.$validators.boundCheck = function(modelValue) {
          return _.isArray(modelValue) &&
            modelValue.length === 2 &&
            (isValidDate(modelValue[0])) &&
            (isValidDate(modelValue[1])) &&
            (moment(modelValue[0]) <= moment(modelValue[1])) &&
            (opts.minDate ? moment(modelValue[0]) >= moment(opts.minDate) : true) &&
            (opts.maxDate ? moment(modelValue[1]) <= moment(opts.maxDate) : true);
        };


        ngModelCtrl.$render = function() {
            ngModelCtrl.$validate();
            var start, end;
            if(ngModelCtrl.$valid) {
                start = moment(ngModelCtrl.$modelValue[0]);
                end = moment(ngModelCtrl.$modelValue[1]);
                jQueryDatepicker.data('daterangepicker').setStartDate(start);
                jQueryDatepicker.data('daterangepicker').setEndDate(end);
                if (opts.singleDatePicker === true) {
                    if (moment(start).format('YYYY-MM-DD HH') === moment().format('YYYY-MM-DD HH')) {
                        scope.displayRange = 'NOW';
                    } else {
                        scope.startString = start.format('MM/D/YY h:mm a');
                        scope.displayRange = scope.startString;
                    }
                } else {
                    if (end - start <= 3600000 * 48) {
                        scope.startString = start.format(opts.format || 'MM/D h:mm a');
                        scope.endString = end.format(opts.format || 'MM/D/YY h:mm a');
                        scope.displayRange = scope.startString + ' - ' + scope.endString;
                    } else {
                        scope.startString = start.format(opts.format || 'MMMM D, YYYY');
                        scope.endString = end.format(opts.format || 'MMMM D, YYYY');
                        scope.displayRange = scope.startString + ' - ' + scope.endString;
                    }
                }
            } else {
                scope.displayRange = opts.singleDatePicker ? 'Choose date' : 'Choose Date Range';
            }
        };
        element.on('keydown keyup', function(e) {
          if(e.keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();
          }
        });
      }
    };
  });
