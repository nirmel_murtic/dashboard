'use strict';

angular.module('common')
  .directive('fsCreativePagination', function () {
    return {
      templateUrl: 'components/directives/fsCreativePagination/fsCreativePagination.html',
      restrict: 'EA',
      replace: true,
      transclude: true,
      scope: false,
      link: function (scope, element, attrs) { //jshint ignore:line
        scope.page = 1;
        scope.innerPage = 1;
        scope.pageLimit = 10;

        scope.mapCreativeList = function(){

          // //init pagination
          if(scope.combo.creativeList.length > scope.pageLimit){
            scope.innerPage = Math.floor((scope.page - 1) / scope.pageLimit) * scope.pageLimit + 1;
            // scope.outerPage = scope.innerPage + scope.pageLimit;
          } else {
            scope.innerPage = 1;
          }

          if(!scope.currentCreative.previewsLoaded){
            scope.currentCreative.getPreview();
          }

          scope.currentCreativeList = _.filter(scope.combo.creativeList, function(creative, i){
            i = i + 1;
            if(i >= scope.innerPage && i <= (scope.innerPage - 1 + scope.pageLimit)){
              return true;
            }
            return false;
          });
        };

        scope.prev = function(){
          var chunk = Math.floor((scope.page - 1) / scope.pageLimit) - 1;
          scope.page = chunk * scope.pageLimit + 1;
          if(scope.page < 1){
            scope.page = 1;
          }

          scope.currentCreative = scope.combo.creativeList[scope.page - 1];
          scope.mapCreativeList();
        };

        scope.next = function(){

          var chunk = Math.floor((scope.page - 1) / scope.pageLimit) + 1;
          scope.page = chunk * scope.pageLimit + 1;
          if(scope.page > scope.combo.creativeList.length){
            scope.page = scope.combo.creativeList.length;
          }

          scope.currentCreative = scope.combo.creativeList[scope.page - 1];
          scope.mapCreativeList();
        };

        scope.$watch('combo.creativeList.length', function(newValue, oldValue){
          if(newValue > oldValue){
            scope.goToPage(newValue);
          } else if(newValue < oldValue){
            scope.goToPage(scope.page - 1 || 1);
          }
        });

        scope.goToPage = function(page){
          scope.page = page;
          scope.currentCreative = scope.combo.creativeList[scope.page - 1];
          scope.mapCreativeList();
        };
        scope.goToPage(scope.page);
      }
    };
  });
