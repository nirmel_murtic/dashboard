'use strict';

angular.module('common')
    .directive('fsMap', function () {

        function mapCountryNamesToCodes(countryCode) {
            var map = {'BD': 'BGD', 'BE': 'BEL', 'BF': 'BFA', 'BG': 'BGR', 'BA': 'BIH', 'BB': 'BRB', 'WF': 'WLF', 'BL': 'BLM', 'BM': 'BMU', 'BN': 'BRN', 'BO': 'BOL', 'BH': 'BHR', 'BI': 'BDI', 'BJ': 'BEN', 'BT': 'BTN', 'JM': 'JAM', 'BV': 'BVT', 'BW': 'BWA', 'WS': 'WSM', 'BQ': 'BES', 'BR': 'BRA', 'BS': 'BHS', 'JE': 'JEY', 'BY': 'BLR', 'BZ': 'BLZ', 'RU': 'RUS', 'RW': 'RWA', 'RS': 'SRB', 'TL': 'TLS', 'RE': 'REU', 'TM': 'TKM', 'TJ': 'TJK', 'RO': 'ROU', 'TK': 'TKL', 'GW': 'GNB', 'GU': 'GUM', 'GT': 'GTM', 'GS': 'SGS', 'GR': 'GRC', 'GQ': 'GNQ', 'GP': 'GLP', 'JP': 'JPN', 'GY': 'GUY', 'GG': 'GGY', 'GF': 'GUF', 'GE': 'GEO', 'GD': 'GRD', 'GB': 'GBR', 'GA': 'GAB', 'SV': 'SLV', 'GN': 'GIN', 'GM': 'GMB', 'GL': 'GRL', 'GI': 'GIB', 'GH': 'GHA', 'OM': 'OMN', 'TN': 'TUN', 'JO': 'JOR', 'HR': 'HRV', 'HT': 'HTI', 'HU': 'HUN', 'HK': 'HKG', 'HN': 'HND', 'HM': 'HMD', 'VE': 'VEN', 'PR': 'PRI', 'PS': 'PSE', 'PW': 'PLW', 'PT': 'PRT', 'SJ': 'SJM', 'PY': 'PRY', 'IQ': 'IRQ', 'PA': 'PAN', 'PF': 'PYF', 'PG': 'PNG', 'PE': 'PER', 'PK': 'PAK', 'PH': 'PHL', 'PN': 'PCN', 'PL': 'POL', 'PM': 'SPM', 'ZM': 'ZMB', 'EH': 'ESH', 'EE': 'EST', 'EG': 'EGY', 'ZA': 'ZAF', 'EC': 'ECU', 'IT': 'ITA', 'VN': 'VNM', 'SB': 'SLB', 'ET': 'ETH', 'SO': 'SOM', 'ZW': 'ZWE', 'SA': 'SAU', 'ES': 'ESP', 'ER': 'ERI', 'ME': 'MNE', 'MD': 'MDA', 'MG': 'MDG', 'MF': 'MAF', 'MA': 'MAR', 'MC': 'MCO', 'UZ': 'UZB', 'MM': 'MMR', 'ML': 'MLI', 'MO': 'MAC', 'MN': 'MNG', 'MH': 'MHL', 'MK': 'MKD', 'MU': 'MUS', 'MT': 'MLT', 'MW': 'MWI', 'MV': 'MDV', 'MQ': 'MTQ', 'MP': 'MNP', 'MS': 'MSR', 'MR': 'MRT', 'IM': 'IMN', 'UG': 'UGA', 'TZ': 'TZA', 'MY': 'MYS', 'MX': 'MEX', 'IL': 'ISR', 'FR': 'FRA', 'IO': 'IOT', 'SH': 'SHN', 'FI': 'FIN', 'FJ': 'FJI', 'FK': 'FLK', 'FM': 'FSM', 'FO': 'FRO', 'NI': 'NIC', 'NL': 'NLD', 'NO': 'NOR', 'NA': 'NAM', 'VU': 'VUT', 'NC': 'NCL', 'NE': 'NER', 'NF': 'NFK', 'NG': 'NGA', 'NZ': 'NZL', 'NP': 'NPL', 'NR': 'NRU', 'NU': 'NIU', 'CK': 'COK', 'XK': 'XKX', 'CI': 'CIV', 'CH': 'CHE', 'CO': 'COL', 'CN': 'CHN', 'CM': 'CMR', 'CL': 'CHL', 'CC': 'CCK', 'CA': 'CAN', 'CG': 'COG', 'CF': 'CAF', 'CD': 'COD', 'CZ': 'CZE', 'CY': 'CYP', 'CX': 'CXR', 'CR': 'CRI', 'CW': 'CUW', 'CV': 'CPV', 'CU': 'CUB', 'SZ': 'SWZ', 'SY': 'SYR', 'SX': 'SXM', 'KG': 'KGZ', 'KE': 'KEN', 'SS': 'SSD', 'SR': 'SUR', 'KI': 'KIR', 'KH': 'KHM', 'KN': 'KNA', 'KM': 'COM', 'ST': 'STP', 'SK': 'SVK', 'KR': 'KOR', 'SI': 'SVN', 'KP': 'PRK', 'KW': 'KWT', 'SN': 'SEN', 'SM': 'SMR', 'SL': 'SLE', 'SC': 'SYC', 'KZ': 'KAZ', 'KY': 'CYM', 'SG': 'SGP', 'SE': 'SWE', 'SD': 'SDN', 'DO': 'DOM', 'DM': 'DMA', 'DJ': 'DJI', 'DK': 'DNK', 'VG': 'VGB', 'DE': 'DEU', 'YE': 'YEM', 'DZ': 'DZA', 'US': 'USA', 'UY': 'URY', 'YT': 'MYT', 'UM': 'UMI', 'LB': 'LBN', 'LC': 'LCA', 'LA': 'LAO', 'TV': 'TUV', 'TW': 'TWN', 'TT': 'TTO', 'TR': 'TUR', 'LK': 'LKA', 'LI': 'LIE', 'LV': 'LVA', 'TO': 'TON', 'LT': 'LTU', 'LU': 'LUX', 'LR': 'LBR', 'LS': 'LSO', 'TH': 'THA', 'TF': 'ATF', 'TG': 'TGO', 'TD': 'TCD', 'TC': 'TCA', 'LY': 'LBY', 'VA': 'VAT', 'VC': 'VCT', 'AE': 'ARE', 'AD': 'AND', 'AG': 'ATG', 'AF': 'AFG', 'AI': 'AIA', 'VI': 'VIR', 'IS': 'ISL', 'IR': 'IRN', 'AM': 'ARM', 'AL': 'ALB', 'AO': 'AGO', 'AQ': 'ATA', 'AS': 'ASM', 'AR': 'ARG', 'AU': 'AUS', 'AT': 'AUT', 'AW': 'ABW', 'IN': 'IND', 'AX': 'ALA', 'AZ': 'AZE', 'IE': 'IRL', 'ID': 'IDN', 'UA': 'UKR', 'QA': 'QAT', 'MZ': 'MOZ'};
            return map[countryCode];
        }

        return {
            restrict: 'E',
            template: '<div id="container" style="position: relative; width: 100%; height: 400px; background-color:#fff"></div>',
            scope: {
                data: '=data',
                selectedCountry: '=selectedCountry'
            },
            replace: true,
            link: function (scope, element, attrs) { //jshint ignore:line
                var tmpDataMap, centered, mapConfig;

                function clickZoom(d) {
                    var self = this, //jshint ignore:line
                        zoomFactor = 0.8,
                        width = self.options.element.clientWidth,
                        height = self.options.element.clientHeight,
                        bounds;
                    if (centered === d || isNaN(zoomFactor) || zoomFactor <= 0) {
                        return resetZoom.call(self);
                    }

                    self.svg.selectAll('path').classed('active', false);
                    centered = d;

                    if (d.radius) { //Circle
                        var cx = d3.select(d3.event.target).attr('cx');
                        var cy = d3.select(d3.event.target).attr('cy');
                        bounds = [
                            [ Number(cx) - d.radius, Number(cy) - d.radius ],
                            [ Number(cx) + d.radius, Number(cy) + d.radius ]
                        ];
                    } else {
                        bounds = self.path.bounds(d);
                    }

                    var dx = bounds[1][0] - bounds[0][0],
                        dy = bounds[1][1] - bounds[0][1],
                        x = (bounds[0][0] + bounds[1][0]) / 2,
                        y = (bounds[0][1] + bounds[1][1]) / 2,
                        scale = zoomFactor / Math.max(dx / width, dy / height),
                        translate = [width / 2 - scale * x, height / 2 - scale * y];

                    self.svg.selectAll('path')
                        .classed('active', centered && function (d) {
                            return d === centered;
                        });

                    self.svg.selectAll('g').transition()
                        .style('stroke-width', 1.5 / scale + 'px')
                        .attr('transform', 'translate(' + translate + ')scale(' + scale + ')');
                }

                function resetZoom() {

                    this.svg.selectAll('path') //jshint ignore:line
                        .classed('active', false);
                    centered = d3.select(null);

                    this.svg.selectAll('g').transition() //jshint ignore:line
                        .duration(750)
                        .style('stroke-width', '1.5px')
                        .attr('transform', '');
                }

                scope.$watch('selectedCountry', function (newVal, oldVal) {
                    console.log('newVal', newVal);
                    if (newVal !== undefined && newVal !== oldVal) {
                        if (newVal === '') {
                            console.log('reset');
                            resetZoom.call(tmpDataMap);
                        } else {
                            var countryCode = mapCountryNamesToCodes(newVal);
                            console.log('dat',  scope.map.svg.select('.USA')[0]);
                            console.log('data',  scope.map.svg.select('.'+countryCode)[0][0].__data__);
                            var country = scope.map.svg.select('.'+countryCode)[0][0].__data__;
                            if (country) {
                                console.log('country', country);
                                clickZoom.call(tmpDataMap, country);
                            }
                        }

                    }
                });

                scope.$watch('data', function () {
                    var element = document.getElementById('container');
                    element.innerHTML = '';
                    mapConfig = {
                        element: document.getElementById('container'),
                        done: function (datamap) {
                            tmpDataMap = datamap;
                            /*                            datamap.svg.selectAll('.datamaps-subunit').on('click', function (geo) {
                             var countryCode = geo.id;
                             var country;
                             for (var i=0; i<scope.data.countries.length; i++) {
                             if (mapCountryNamesToCodes(scope.data.countries[i].countrycode) === countryCode) {
                             country = scope.data.countries[i];
                             break;
                             }
                             }
                             if (country) {
                             console.log('geo', geo);
                             clickZoom.call(tmpDataMap, geo);
                             }
                             });*/
                        },
                        fills: {
                            countryColorForOnePercentages: 'rgb(236,237,236)',
                            countryColorForTenPercentages: 'rgb(220,220,221)',
                            countryColorForTwentyPercentages: 'rgb(204,205,205)',
                            countryColorForThirtyPercentages: 'rgb(188,189,190)',
                            countryColorForFortyPercentages: 'rgb(172,173,175)',
                            countryColorForFiftyPercentages: 'rgb(157,158,159)',
                            countryColorForSixtyPercentages: 'rgb(141,142,144)',
                            countryColorForSeventyPercentages: 'rgb(124,122,123)',
                            countryColorForEightyPercentages: 'rgb(112,111,112)',
                            countryColorForNinetyPercentages: 'rgb(102,102,102)',
                            bubbles: 'rgb(247,151,29)',
                            defaultFill: '#fff'
                        },
                        data: {},
                        geographyConfig: {
                            highlightOnHover: false,
                            popupTemplate: function (geo, data) {
                                if (data) {
                                    return ['<div style="cursor: pointer" class="hoverinfo"><strong>',
                                        geo.properties.name,
                                        '<br>Total Page Views: ',
                                        + data.count,
                                        '</strong></div>'].join('');
                                }
                            },
                            borderWidth: 1,
                            borderColor: '#ccc',
                            bubblesConfig: {
                                fillOpacity: 1
                            }
                        },
                        zoomConfig: {
                            zoomOnClick: true,
                            zoomFactor: 0.9
                        }
                    };


                    for (var i = 0; i < scope.data.countries.length; i++) {
                        var countryCode = mapCountryNamesToCodes(scope.data.countries[i].countrycode); //jshint ignore:line
                        mapConfig.data[countryCode] = {};
                        mapConfig.data[countryCode].fillKey = scope.data.countries[i].color;
                        mapConfig.data[countryCode].count = scope.data.countries[i].eventcount || 0;


                    }

                    scope.map = new Datamap(mapConfig); //jshint ignore:line
                    scope.map.bubbles(scope.data.cities, {
                        popupTemplate: function (geography, data) {
                            return ['<div class="hoverinfo"><strong>' + data.name + '</strong>',
                                    '<br/>Total Page Views:' + data.eventcount + '',
                                '</div>'].join('');
                        }
                    });
                }, true);
            }
        };
    });