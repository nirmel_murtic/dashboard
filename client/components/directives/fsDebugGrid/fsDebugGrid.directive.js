'use strict';

angular.module('common')
  .directive('fsDebugGrid', function($document) {
    return {
      restrict : 'A',
      scope : {
        gutter : '&',
        columns : '&'
      },
      compile : function() {
        if(!window.__DEV__) {
          return angular.noop;
        }
       
        return function link(scope, element) {
          var gutter = window.innerWidth >= 1872 ? 1.0625 * 16 : 16;
          if(window.gridAssistant) {
            $document.bind('keyup', function(e) {
              if(e.ctrlKey && e.keyCode === 13) { 
                if(!element[0]._gridMetaData) {
                  gridAssistant(element[0], scope.columns() || 24, scope.gutter() || gutter);
                } else {
                  element[0]._gridMetaData = null;
                  element[0].style.background = '';
                }
              }
            });
          }
        };
      }
    };
  });
