'use strict';

angular.module('common')
  .service('CallToAction', function () {

    var callsToActionList = [];
    return {
      fbCallsToAction : function(goal, adType) {
        callsToActionList = [];
        switch (goal) {
          case 'MOBILE_APP_ENGAGEMENT':
            this.addToCallsToActionList([
              '',
              'BUY_NOW', 
              'GET_OFFER',
              'OPEN_LINK']); 
            /* falls through */
          case 'MOBILE_APP_INSTALLS':
            this.addToCallsToActionList([
              '',
              'PLAY_GAME', 
              'USE_APP', 
              'SHOP_NOW',
              'LISTEN_MUSIC',
              'WATCH_VIDEO',
              'WATCH_MORE',
              'DOWNLOAD',
              'LEARN_MORE',
              'SIGN_UP'
              ]);
            break;
          case 'WEBSITE_CLICKS':
            this.addToCallsToActionList(['CONTACT_US']);
            /* falls through */
          case 'WEBSITE_CONVERSIONS':
            this.addToCallsToActionList([
              'SHOP_NOW',
              'LEARN_MORE',
              'SIGN_UP',
              'DOWNLOAD',
              'WATCH_MORE',
              'APPLY_NOW',
              'DONATE_NOW',
              'BOOK_TRAVEL',
              //not in the documentation, but available.
              'OPEN_LINK',
              'LISTEN_MUSIC',
              'WATCH_VIDEO',
              'MESSAGE_PAGE',
              'SUBSCRIBE',
              'GET_QUOTE',
              'BUY_NOW',
              'GET_OFFER',
              'PLAY_GAME',
              'USE_APP',
              //Only in one error message from fb
              // 'INSTALL_MOBILE_APP',
              // 'USE_MOBILE_APP',
              // 'FRIENDS_PHOTO',
              // 'MOBILE_DOWNLOAD',
              // 'BUY_TICKETS',
              // 'UPDATE_APP',
              // 'BET_NOW',
              // 'ADD_TO_CART',
              // 'ORDER_NOW',
              // 'SELL_NOW',
              // 'CALL',
              // 'MISSED_CALL',
              // 'CALL_ME',
              // 'BUY',
              // 'VIDEO_ANNOTATION',
            ]);
            if (adType && (adType !== 'videoAd')) {
              this.addToCallsToActionList(['']);
            }
            break;
          case 'POST_ENGAGEMENT':
          case 'VIDEO_VIEWS':
            this.addToCallsToActionList([
              '',
              'SHOP_NOW',
              'LEARN_MORE',
              'SIGN_UP',
              'DOWNLOAD',
              'WATCH_MORE',
              'BOOK_TRAVEL'
            ]);
            break;
        }
        return callsToActionList;
      },

      addToCallsToActionList : function (callsToAction) {
        //only add the call to action to the list if it isn't already there.
        _.map(callsToAction, function (CTA) {
          if (callsToActionList.indexOf(CTA) === -1) {
            callsToActionList.push(CTA);
          }
        });
        return callsToActionList;
      },

      twCallsToAction : function (goal) {
        if (goal === 'website') {
          return [
            'READ_MORE',
            'SHOP_NOW',
            'VIEW_NOW',
            'VISIT_NOW',
            'BOOK_NOW',
            'LEARN_MORE',
            'PLAY_NOW',
            'BET_NOW',
            'APPLY_HERE',
            'QUOTE_HERE',
            'ORDER_NOW',
            'ENROLL_NOW',
            'FIND_A_CARD',
            'GET_A_QUOTE',
            'GET_TICKETS',
            'LOCATE_A_DEALER',
            'ORDER_ONLINE',
            'PREORDER_NOW',
            'SCHEDULE_NOW',
            'SIGN_UP_NOW',
            'REGISTER_NOW'
          ];
        } else if (goal === 'app') {
          return [
            'INSTALL_OPEN',
            'PLAY',
            'SHOP',
            'BOOK',
            'CONNECT',
            'ORDER'
          ];
        }
      }

    };

  });