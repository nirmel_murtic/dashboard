'use strict';

describe('Service: urlValidator', function () {

  // load the service's module
  beforeEach(module('common'));

  // instantiate service
  var urlValidator;
  beforeEach(inject(function (_urlValidator_) {
    urlValidator = _urlValidator_;
  }));

  describe('Domain comparison', function () {
    var url1 = 'www.accomplice.io';
    var url2 = 'accomplice.io';
    var url3 = '24intl.com';

    it('Should return false', function () {
      expect(urlValidator.checkUrlDomainMatchesGoalDomain(url1, url2)).toBe(false);
    });

    it('Should return true', function () {
      expect(urlValidator.checkUrlDomainMatchesGoalDomain(url1, url3)).toBe(true);
    });
  });

  describe('Get get domain (including sub-domain)', function () {

    var hostnameOnly = 'accomplice.io';

    var url1 = 'http://www.accomplice.io';
    var url2 = 'accomplice.io';
    var url3 = 'accomplice.co.uk';
    var url4 = 'https://accomplice.io';
    var url5 = 'https://alpha.beta.accomplice.co.uk';

    it('Extract hostname from FQDN', function () {
      expect(urlValidator.getDomainWithSubdomain(url1)).toBe('www.'+hostnameOnly);
    });

    it('Extract hostname from hostname only url', function () {
      expect(urlValidator.getDomainWithSubdomain(url2)).toBe(hostnameOnly);
    });

    it('Extract hostname from hostname only url with sld', function () {
      expect(urlValidator.getDomainWithSubdomain(url3)).toBe('accomplice.co.uk');
    });

    it('Extract hostname from url with protocol and without www', function () {
      expect(urlValidator.getDomainWithSubdomain(url4)).toBe(hostnameOnly);
    });

    it('Extract hostname from URL with several sub-domains and SLD', function () {
      expect(urlValidator.getDomainWithSubdomain(url5)).toBe('alpha.beta.accomplice.co.uk');
    });
  });

});
