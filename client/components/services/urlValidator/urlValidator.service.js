'use strict';

angular.module('common')
  .service('urlValidator', function () {

    // Check that the URL typed's domain matches the goal url domain
    this.checkUrlDomainMatchesGoalDomain = function(url, goalUrl) {
      var destinationUrlDomain = this.getDomain(url),
        goalUrlDomain = this.getDomain(goalUrl);

      return destinationUrlDomain.toLowerCase() !== goalUrlDomain.toLowerCase();
    };

    this.getDomain = function(url) {
      url = _.contains(url, 'http') ? angular.copy(url) : 'http://'+url;
      return URI(url).domain();
    };

    this.getDomainWithSubdomain = function (url) {
      url = _.contains(url, 'http') ? angular.copy(url) : 'http://'+url;
      return URI(url).hostname();
    };

  });
