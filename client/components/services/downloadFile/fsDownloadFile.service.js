'use strict';

angular.module('common')
  .service('fsDownloadFile', function($http, fsConfirm) {

    //TO-DO: add fallback for old (<=9) IE support
    var isMordenIE = true;

    function downloadFromUrl(url, fileName, config) {


      if(!isMordenIE) {
        window.open(url, '_blank');
      } 
      return $http.get(url, config || {}).then(function(res) {
        var csvString = res.data;
        var blob = new Blob([csvString], {type: 'text/csv;charset=utf-8'});
        saveAs(blob, fileName);
        /*
        var a = document.createElement('a');
        a.href = 'data:attachment/csv,' + encodeURIComponent(csvString);
        a.target = '_blank';
        a.download = fileName;

        // document.body.appendChild(a);
        a.click(); 
        // document.body.removeChild(a);
        */
      }, function() {
        fsConfirm('genericAlert', {body:'Sorry, Download CSV Failed.'});
      }); 
    } 

    return {
      downloadFromUrl : downloadFromUrl
    };
  });
