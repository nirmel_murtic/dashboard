'use strict';

angular.module('common')
  .service('RecentDateRange', function() {
    function DateRange(now) {
      this.now = now || new Date();
      this.rangeBound = [new Date(0), this.now];
      this.recents = [];
    }

    function intersection(range1, range2) {
      var a1 = range1[0],
          a2 = range2[0],
          b1 = range1[1],
          b2 = range2[1];
      
      var a = a1 < a2 ? a2 : a1;
      var b = b1 < b2 ? b1 : b2;
      
      if(a < b) {
        return [a, b];
      }
      return null;
    }

    function rangeLength(range) {
      return range[1] - range[0];
    }

    function shouldEnable(dateRange, recent) {
      if(dateRange.isEmpty()) {
        return false;
      }
      
      var recentRange = [dateRange.now - recent.duration, dateRange.now];
      var rangeBound = dateRange.rangeBound;

      var overlap = intersection(rangeBound, recentRange);
      if(!overlap) {
        return false;
      }
      if(rangeLength(overlap) / rangeLength(recentRange) >= 0.5) {
        return true;
      }
      return false;
    }


    DateRange.prototype = {
      within : function(range) {
        this.rangePresetsCache_ = null;
        var now = this.now;
        if(range[0] >= now) {
          this.rangeBound[0] = now;
          this.rangeBound[1] = now;
        } else if(range[1] >= now) {
          this.rangeBound[0] = range[0];
          this.rangeBound[1] = now;
        } else {
          this.rangeBound[0] = range[0];
          this.rangeBound[1] = range[1];
        }
        return this;
      },
      addRecent : function(label, duration, unit) {
        var ago;
        // today, this month, this year type of things
        if (typeof duration === 'string') {
          unit = duration;
          switch(unit) {
            case 'day':
              ago = d3.time.day.floor(this.now);
              break;
            case 'week':
              ago = d3.time.week.floor(this.now);
              break;
            case 'month':
              ago = d3.time.month.floor(this.now);
              break;
            case 'year' :
              ago = d3.time.year.floor(this.now);
              break;
            default:
              ago = d3.time.day.floor(this.now);
          }
          duration = this.now - ago;
        // last 2 days, last 3 months, last 5 years type of things
        } else if(unit) {
          ago = new Date(this.now.getTime());
          switch(unit) {
            case 'days':
              ago.setDate(ago.getDate() - duration);
              break;
            case 'weeks':
              ago.setDate(ago.getDate() - duration * 7);
              break;
            case 'hours':
              ago.setHours(ago.getHours() - duration);
              break;
            case 'years':
              ago.setYear(ago.getYear() - duration);
              break;
            case 'months':
              ago.setMonth(ago.getMonth() - duration);
              break;
            default:
              ago.setTime(ago.getTime() - duration);
          }
          duration = this.now - ago;
        }
        this.recents.push({
          duration : duration, 
          label : label
        });
        this.rangePresetsCache_ = null;
        return this;
      },
      isEmpty : function() {
        return this.rangeBound[1] - this.rangeBound[0] <= 0;
      },
      isPast : function() {
        return this.rangeBound[1] < this.now;
      },
      getRangePresets : function() {
        if(this.rangePresetsCache_) {
          return this.rangePresetsCache_;
        }
        this.rangePresetsCache_ = _.map(this.recents, function(recent) {
          var enabled = shouldEnable(this, recent);
          var range = [new Date(this.now - recent.duration), this.now];
          return {
            label : recent.label,
            range : range,
            enabled : enabled
          };
        }, this);
        return this.rangePresetsCache_;
      }
    };

    this.ranges = function(now) {
      return new DateRange(now);
    };

    this.dateRangeIntersection = intersection;
    this.dateRangeLength = rangeLength;
  
  });
