'use strict';

/**
 * This factory is responsible for conversion of json structure generated using @JsonIdentityInfo on backend side
 * to javascript object structure. It will change all key references with real javascript objects.
 * This is used to prevent recursion issues while generating json files on backend side.
 *
 * NOTE: Do not convert object converted with this factory back to json because same recursion issue will occur
 * on fronted side. Instead of that you can convert back to json original object which will stay untouched.
 */

angular.module('common')
  .factory('identityConverter', function () {
    var getKeys = function(obj, key) {
        var keys = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) {
              continue;
            }
            if (typeof obj[i] === 'object') {
                keys = keys.concat(getKeys(obj[i], key));
            } else if (i === key) {
                keys.push( { key: obj[key], obj: obj });
            }
        }

        return keys;
    };

    var convertObject = function(obj, key, keys) {
        if(!keys) {
            keys = getKeys(obj, key);

            var convertedKeys = {};

            for(var i = 0; i<keys.length; i++) {
                convertedKeys[keys[i].key] = keys[i].obj;
            }

            keys = convertedKeys;
        }

        for (var j in obj) {
            if (!obj.hasOwnProperty(j)) {
              continue;
            }
            if (typeof obj[j] === 'object') {
                convertObject(obj[j], key, keys);
            } else if( j === key) {
                delete obj[j];
            } else if (keys[obj[j]]) {
                obj[j] = keys[obj[j]];
            }
        }

        return obj;
    };

    return {
        convert: function (object, key) {
            return convertObject(angular.copy(object), key);
        }
    };
});