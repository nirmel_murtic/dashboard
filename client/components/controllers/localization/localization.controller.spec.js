'use strict';

describe('Controller: LocalizationCtrl', function () {

  // load the controller's module
  beforeEach(module('common'));

  var LocalizationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LocalizationCtrl = $controller('LocalizationCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
