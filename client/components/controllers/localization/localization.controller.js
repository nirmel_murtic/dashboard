'use strict';

angular.module('common')
  .controller('LocalizationCtrl', function ($scope, $translate) {
  		$scope.changeLanguage = function (langKey) {
            $translate.use(langKey);
        };
  });
