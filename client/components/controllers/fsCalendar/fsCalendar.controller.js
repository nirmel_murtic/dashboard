'use strict';

angular.module('act')
 .controller('FrameCtrl', function($scope, $location, $timeout) {
      // $scope.currentView = 'Week';

      $scope.time = {
        today: new Date()
      };

      $scope.user = {
        timezone: 'US/Pacific'
      };

      $scope.updateClock = function() {
        $scope.time.today = new Date();
      };
      var tick = function() {
        $timeout(function() {
          $scope.$apply($scope.updateClock);
          tick();
        }, 1000);
      };
      // tick();
  })
  .controller('CalendarCtrl', function ($scope, engage) {
  	 $scope.keepDate = 'all';
      var date = new Date();
      $scope.weekDays = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
      $scope.dateFilters = {
        'all': 'all',
        'next week': new Date(date.setDate(date.getDate() + 7))
        // 'tomorrow': chrono.parseDate('tomorrow at 11:59pm'),
        // 'today': chrono.parseDate('today at 11:59pm')
      };

      engage.getAllScheduledPosts($scope.applyScheduledPosts);

      $scope.events = [];
      $scope.filterNotifications = {
        all: 0,
        today: 0,
        tomorrow: 0,
        week: 0
      };

      $scope.$watch('events', function(val) {
        if (val && val.length > 0) {
          var date = new Date();
          var nextWeek = new Date(date.setDate(date.getDate() + 7));
          // var tomorrow = chrono.parseDate('tomorrow at 11:59pm');
          // var today = chrono.parseDate('today at 11:59pm');

          $scope.filterNotifications = {
            all: 0,
            today: 0,
            tomorrow: 0,
            week: 0
          };

          angular.forEach(val, function(value){
            date = new Date(value.start.dateTime);
            $scope.filterNotifications.all += 1;
            if (date < nextWeek) {
              $scope.filterNotifications.week += 1;
              /*if (date < tomorrow) { // tommorow and today undefined!
                $scope.filterNotifications.tomorrow += 1;
                if (date < today) {
                  $scope.filterNotifications.today += 1;
                }
              }*/
            }
          });
        }
      });
  });
