'use strict';

angular.module('common', ['facebook', 'ng-token-auth', 'ui.router', 'ngDialog', 'vcRecaptcha', 'pascalprecht.translate', 'fs-visulization', 'common.providers', 'angularUtils.directives.uiBreadcrumbs', '720kb.tooltips', 'ngAnimate'])
  .constant('apiUrl', window.configuration ? window.configuration.apiUrl : '')

  .config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
      showClose: true,
      closeByDocument: false,
      closeByEscape: true
    });

}]);
