'use strict';

/**
 * List of available ng-model-options
 */

angular.module('common')
  .factory('modelOptions', function () {
    return {
      newInstance: function() {
        return {
          creativeUrl: {
            allowInvalid: true,
            updateOn: 'default blur',
            debounce: {
              default: 1000,
              blur: 0
            }
          }
        };
      }
    };
  });




