'use strict';

angular.module('common')
  .constant('targetingSpecs', 
    {
      'location': 'Location',
      'language': 'Language',
      'gender': 'Gender',
      'ageRange': 'Age Range',
      'customAudiences': 'Custom Audiences',
      'excludedCustomAudiences': 'Excluded Custom Audiences',
      'interestedIn': 'Interested In',
      'relationshipStatus': 'Relationship Status',
      'connections': 'Connections',
      'education': 'Education',
      'workEmployer': 'Work Employer',
      'workPosition': 'Work Position',
      'preciseInterests': 'Interests',
      'advancedDemographics': 'Advanced Demographics',
      'behavior': 'Behavior'
    }
  );