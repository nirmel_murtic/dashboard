'use strict';

/**
 * List of available permissions.
 * Don't change this before changing backend first. This is included to allow shorter tokens.
 */

angular.module('common')
  .constant('permissions', {
        'VTAC': 'VIEW_TERMS_AND_CONDITIONS',
        'MP': 'MASTER_PASSWORD',
        'SPRT': 'SUPPORT',
        'LIN': 'LOGGED_IN',
        'AL': 'ALIGN',
        'TM': 'TEAMS',
        'MTM': 'MANAGE_TEAMS',
        'MMB': 'MANAGE_MEMBERS',
        'AD': 'AUDIENCES',
        'MAD': 'MANAGE_AUDIENCES',
        'AAD': 'ACCESS_AUDIENCES',
        'CH': 'CHANNELS',
        'ARCH': 'ADDING_REMOVING_CHANNELS',
        'AVCP': 'ACCESS_AND_VIEW_CHANNEL_PROPERTIES',
        'MG': 'MANAGE_GOALS',
        'CL': 'CONTENT_LIBRARIES',
        'MCL': 'MANAGE_CONTENT_LIBRARIES',
        'ACL': 'ACCESS_CONTENT_LIBRARIES',
        'PS': 'PIXEL_SETUP',
        'ME': 'MANAGE_EVENTS',
        'MW': 'MANAGE_WEBSITES',
        'LOC': 'LOCALE',
        'ACT': 'ACT',
        'CM': 'CAMPAIGN_MANAGER',
        'PALC': 'PRE_APPROVED_TO_LAUNCH_CAMPAIGNS',
        'RABL': 'REQUIRE_APPROVAL_BEFORE_LUNCHING',
        'MEC': 'MANAGE_EXISTING_CAMPAIGNS',
        'ENG': 'ENGAGE',
        'RABP': 'REQUIRE_APPROVAL_BEFORE_PUBLISHING',
        'IPOC': 'IS_PREAPPROVED_FOR_ORGANIC_CONTENT',
        'APPR': 'APPROVERS',
        'CAO': 'CAN_APPROVE_ORGANIC',
        'CAP': 'CAN_APPROVE_PAID',
        'ANL': 'ANALYZE',
        'AO': 'ANALYZE_ORGANIC',
        'AP': 'ANALYZE_PAID',
        'AF': 'ANALYZE_FUNNEL',
        'CA': 'COHORT_ANALYSIS',
        'ARAR': 'ANALYZE_REQUIRE_APPROVAL_FOR_REPORTS',
        'MBAAI': 'MANAGE_BILLING_AND_ACCOUNT_INFO'
    })
  .factory('permissionUtils', function() {
    var getParentForPermission = function(permName, permissions) {
        var result = null;

        angular.forEach(permissions, function(item) {
          if(item.key === permName) {
            result = item.parentKey;

            return false;
          }

          var r = getParentForPermission(permName, item.items);

          if(r) {
            result = r;

            return false;
          }
        });

        return result;
    };

    var getChildrensOfPermission = function(permName, permissions) {
      var result = null;

      angular.forEach(permissions, function(item) {
        if(item.key === permName) {
          result = item.items;

          return false;
        }

        var r = getChildrensOfPermission(permName, item.items);

        if(r) {
          result = r;

          return false;
        }
      });

      return result;
    };

    return {
        getParentForPermission: getParentForPermission,
        getChildrensOfPermission: getChildrensOfPermission
    };
  });
