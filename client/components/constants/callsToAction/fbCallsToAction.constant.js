'use strict';

angular.module('common')
  .constant('fbCallsToAction', 
    {
      'SHOP_NOW': 'Shop Now',
      'BOOK_TRAVEL': 'Book Now',
      'LEARN_MORE': 'Learn More',
      'SIGN_UP': 'Sign Up',
      'DOWNLOAD': 'Download',
      'WATCH_MORE': 'Watch More',
      'APPLY_NOW': 'Apply Now',
      'DONATE_NOW': 'Donate Now',
      'CONTACT_US':'Contact Us',
      'PLAY_GAME': 'Play Game', 
      'USE_APP': 'Use App',
      'BUY_NOW': 'Buy Now', 
      'GET_OFFER': 'Get Offer',
      
      'OPEN_LINK': 'Open Link',
      'LISTEN_MUSIC': 'Listen Now',
      'WATCH_VIDEO': 'Watch Video',
      'CALL_NOW': 'Call Now',
      'GET_DIRECTIONS': 'Get Directions',
      'MESSAGE_PAGE': 'Send Message',
      'SUBSCRIBE': 'Subscribe',
      'GET_QUOTE': 'Get Quote',
      'RECORD_NOW': 'Record Now',
      'OPEN_MOVIES': 'Open Movies',

      'INSTALL_MOBILE_APP': 'Install Mobile App',
      'USE_MOBILE_APP': 'Use Mobile App',
      'FRIENDS_PHOTO': 'friends photo',
      'MOBILE_DOWNLOAD': 'mobile download',
      'BUY_TICKETS': 'Buy Tickets',
      'UPDATE_APP': 'Update App',
      'BET_NOW': 'Bet Now',
      'ADD_TO_CART': 'Add to Cart',
      'ORDER_NOW': 'Order Now',
      'SELL_NOW': 'Sell Now',
      'CALL': 'Call',
      'MISSED_CALL': 'Missed Call',
      'CALL_ME': 'Call Me',
      'BUY': 'Buy',
      'VIDEO_ANNOTATION': 'Video Annotation',
      '': 'No Button'

    }
  );