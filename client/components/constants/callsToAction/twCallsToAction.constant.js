'use strict';

angular.module('common')
  .constant('twCallsToAction', 
    {
      'READ_MORE': 'Read more',
      'SHOP_NOW': 'Shop now',
      'VIEW_NOW': 'View now',
      'VISIT_NOW': 'Visit now',
      'BOOK_NOW': 'Book now',
      'LEARN_MORE': 'Learn more',
      'PLAY_NOW': 'Play now',
      'BET_NOW': 'Bet now',
      'APPLY_HERE': 'Apply here',
      'QUOTE_HERE': 'Quote here',
      'ORDER_NOW': 'Order now',
      'ENROLL_NOW': 'Enroll now',
      'FIND_A_CARD': 'Find a card',
      'GET_A_QUOTE': 'Get a quote',
      'GET_TICKETS': 'Get tickets',
      'LOCATE_A_DEALER': 'Locate a dealer',
      'ORDER_ONLINE': 'Order online',
      'PREORDER_NOW': 'Preorder now',
      'SCHEDULE_NOW': 'Schedule now',
      'SIGN_UP_NOW': 'Sign up now',
      'REGISTER_NOW': 'Register now',
      'INSTALL_OPEN': 'Install/Open',
      'PLAY': 'Play',
      'SHOP': 'Shop',
      'BOOK': 'Book',
      'CONNECT': 'Connect',
      'ORDER': 'Order'
    }
  );