'use strict';

angular.module('fs-visulization-utils', []);
angular.module('fs-visulization-core', ['fs-visulization-utils']);

angular.module('fs-visulization-dataservice', ['fs-visulization-utils']);
angular.module('fs-visulization-store', ['fs-visulization-utils']);

angular.module('fs-visulization', ['fs-visulization-utils', 'fs-visulization-core', 'fs-visulization-dataservice', 'fs-visulization-store'])
  .constant('apiUrl', window.configuration ? window.configuration.apiUrl : '');
