'use strict';

angular.module('fs-visulization-core')
  .factory('isVNode', function() {
    return function(vnode) {
      return vnode.type === 'VirtualNode';
    };
  });
