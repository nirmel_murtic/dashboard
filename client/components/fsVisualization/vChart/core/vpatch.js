'use strict';

angular.module('fs-visulization-core')
  .factory('VPatch', function() {
    function VirtualPatch(type, vNode, patch) {
      this.type = Number(type);
      this.vNode = vNode;
      this.patch = patch;
    }

    VirtualPatch.NONE = 0;
    VirtualPatch.THUNK = 1;
    VirtualPatch.VNODE = 2;
    VirtualPatch.PROPS = 4;
    VirtualPatch.ORDER = 5;
    VirtualPatch.INSERT = 6;
    VirtualPatch.REMOVE = 7;
    VirtualPatch.REMOVE_TAIL = 8;
    VirtualPatch.REMOVE_HEAD = 9;
    VirtualPatch.FOLLOW_UP = 10;

    VirtualPatch.prototype.type = 'VirtualPatch';

    return VirtualPatch;

  });
