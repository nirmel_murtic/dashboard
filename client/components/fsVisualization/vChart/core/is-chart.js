'use strict';

angular.module('fs-visulization-core')
  .factory('isChart', function() {
    return function isChart(chart) {
      return chart.type === 'VirtualChart';
    };
  });
