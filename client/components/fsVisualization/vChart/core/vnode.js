'use strict';

angular.module('fs-visulization-core')
  .factory('VNode', function(isThunk, isVNode, isVTransition) {
    var noChildren = [];
    var noProps = {};

    /**
     * VNode
     *
     * Virtual Node, corresponds to a single DOM node
     *
     * properties are attributes, with a few exceptions
     * see: apply-properties.js
     *
     * VNode also stores metadata: count, number of decendennts
     * and hasThunks, boolean, which is currently not in use yet
     *
     * @constructor
     *
     * @param {string} - tagName
     * @param {object} - properties
     * @param {array} - children
     */
    var VNode = function(tagName, properties, children) {
      this.tagName = tagName;
      // this.key = key != null ? String(key) : undefined;
      this.properties = properties || noProps;
      this.children = children || noChildren;
      var child, count = 1, hasThunks = false;
      var len = this.children.length;
      for (var i=0; i<len; i++) {
        child = this.children[i];
        if(isVNode(child)) {
          count += (child.count || 0);
          if(!hasThunks && child.hasThunks) {
            hasThunks = true;
          }
        } else if(!hasThunks && isThunk(child)) {
          hasThunks = true;  
          count++;
        }
      }
      this.count = count;
      this.hasThunks = hasThunks;
      this.transition = null;
      this.nextVersion = null;
    };

    
    VNode.prototype.type = 'VirtualNode';
    VNode.prototype.namespace = d3.ns.prefix.svg;
    VNode.prototype.getProperty = function(prop) {
      return VNode.getPropertyOf(this, prop);
    };
    VNode.prototype.getProperties = function() {
      var vtrans = this.transition, properties, iteration;
      if(vtrans && isVTransition(vtrans)) {
        iteration = vtrans.current();
        properties = iteration.done ? this.properties : iteration.value;
        if(!iteration.done) {
          properties = _.assign({}, this.properties, properties);
        }
      } else {
        properties = this.properties; 
      }
      return properties;
    };

    VNode.getPropertyOf = function(vnode, prop) {
      var vtrans = vnode.transition;
      var value, properties, iteration;
      if(vtrans && isVTransition(vtrans)) {
        iteration = vtrans.current();
        properties = iteration.done ? vnode.properties : iteration.value;
        if(iteration.done) {
          vnode.transition = null;
        }
      } else {
        properties = vnode.properties; 
      }
      value = properties[prop] || vnode.properties[prop];
      return value;
    };

    VNode.propertySupportTransition = function(tagName, attr, attrVal) {
      if(tagName === 'path' && attr === 'd') {
        if(!attrVal) {
          return false;
        }
        if(attrVal.indexOf('NaN') > -1) {
          return false;
        }
        if(attrVal.indexOf('Infinity') > -1) {
          return false;
        }
        return true;
      }
      if(tagName === 'circle' && (attr === 'cx' || attr === 'cy' || attr === 'r') ) {
        attrVal = Number(attrVal);
        if(isNaN(attrVal) || attrVal >= Infinity || attrVal <= -Infinity) {
          return false;
        }
        if(attr === 'r' && attrVal <= 0) {
          return false;
        }
        return true;
      }
      if(tagName === 'rect' && (attr === 'x' || attr === 'y' || attr === 'width' || attr === 'height') ) {
        attrVal = Number(attrVal);
        if(isNaN(attrVal) || attrVal >= Infinity || attrVal <= -Infinity) {
          return false;
        }
        if((attr === 'width' || attr === 'height') && attrVal <= 0) {
          return false;
        }
        return true;
      }
      if(tagName === 'line' && (attr === 'x1' || attr === 'y1' || attr === 'x2' || attr === 'y2') ) {
        attrVal = Number(attrVal);
        if(isNaN(attrVal) || attrVal >= Infinity || attrVal <= -Infinity) {
          return false;
        }
        return true;
      }
      if(attr === 'fill-opacity') {
        attrVal = Number(attrVal);
        if(isNaN(attrVal) || attrVal >1 || attrVal <0) {
          return false;
        }
        return true;
      }
      if(tagName === 'text' && (attr === 'x' || attr === 'y' || attr === 'dx' || attr === 'dy') ) {
        attrVal = Number(attrVal);
        if(isNaN(attrVal) || attrVal >= Infinity || attrVal <= -Infinity) {
          return false;
        }
        return true;
      }
      return false;
    };

    return VNode;
  
  });
