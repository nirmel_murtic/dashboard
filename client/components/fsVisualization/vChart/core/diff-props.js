'use strict';

angular.module('fs-visulization-core')
  .factory('diffProps', function() {
    /**
     * diffProps
     *
     * @param {object} - a
     * @param {object} - b
     * @return {object} - object diff
     */
    function diffProps(a,b) {
      var diff, aKey, bKey, aValue, bValue;
      for (aKey in a) {
        if(!(aKey in b)) {
          diff = diff || {};
          diff[aKey] = undefined;
        }
        aValue = a[aKey];
        bValue = b[aKey];
        if(_.isEqual(aValue, bValue)) {
          continue;
        } else {
          diff = diff || {};
          diff[aKey] = bValue;
        }
      }
      for (bKey in b) {
        if (!(bKey in a)) {
          diff = diff || {};
          diff[bKey] = b[bKey];
        }
      }

      // noTransition property is always preserved if true
      // meaing if a.noTransition === b.noTransition === true
      // this flag tells apply-property.js not to apply transitions even if
      // some properties/attributes are transitionable
      // property patch will have VPatch{ type: VPatch.PROPS, ..., patch : { ... noTransition: true }}
      if(a.noTransition === 2 || b.noTransition) {
        diff = diff || {};
        diff.noTransition = true;
      }

      return diff;
    }
    return diffProps; 
   
  });
