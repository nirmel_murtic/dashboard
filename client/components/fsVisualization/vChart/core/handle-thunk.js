'use strict';

angular.module('fs-visulization-core')
  .factory('handleThunk', function(isThunk, invariant, isVNode) {
    // handleThunk is not used anywhere yet
    // we will revisit this when thunks are implemented at VNode level
    function handleThunk(a, b) {
      var renderedA = a;
      var renderedB = b;

      if (isThunk(b)) {
        renderedB = renderThunk(b, a);
      }

      if (isThunk(a)) {
        renderedA = renderThunk(a, null);
      }

      return {
        a: renderedA,
        b: renderedB
      };
    }

    function renderThunk(thunk, previous) {
      var renderedThunk = thunk.vNode;

      if (!renderedThunk) {
        renderedThunk = thunk.vNode = thunk.render(previous);
      }

      invariant(isVNode(renderedThunk), 'Thunk did not return a valid node.');

      return renderedThunk;
    }

    return handleThunk;
  });
