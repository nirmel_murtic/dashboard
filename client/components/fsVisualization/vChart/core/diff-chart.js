'use strict';

angular.module('fs-visulization-core')
  .factory('diffChart', function(invariant, isArray, isLayer, isVNode, isChart, VPatch, diffLayer) {

    /**
     * diffChart
     *
     * @param {VNode} - a
     * @param {VChart | VNode} - b
     * @return {object} - patch list
     */
    function diffChart(a, b) {

      invariant(isVNode(a) && (isVNode(b) || isChart(b)), 
        'diffChart expect vNode, vNode or vNode, vChart');
    
      var bIsVNode = isVNode(b);
      var patch = { left: a }; // left is the root vNode
      var apply; // patch on root node

      var aChildren = a.children;
      var bChildren = b.children;

      var bMatched = reorderToMatch(aChildren, bChildren); // reorder bChildren to "align" with aChildren, by layerId 
      var order = bMatched.shuffle; // the reordering to when applied recreates origional (before the above reordering) bChildren 

      // these are new layers/bChildren
      var bAdded = bMatched.slice(aChildren.length);
      for(var i=0; i<bAdded.length; i++) {
        // generate `INSERT` vPatchers on rootNode
        apply = appendPatch(apply, new VPatch(VPatch.INSERT, a, bIsVNode ? bAdded[i] : bAdded[i].render() ));
      }
      // add a `ORDER` (reorder) patch on rootNode to reconstruct the correct ordering of bChildren
      apply = appendPatch(apply, new VPatch(VPatch.ORDER, a, order));
      var removes = bMatched.length - bChildren.length;
      // add `REMOVE_TAIL` patch to rootNode, remove unused aChildren
      if(removes > 0) {
        for(i=0;i<removes;i++) {
          apply = appendPatch(apply, new VPatch(VPatch.REMOVE_TAIL, a, null));
        }
      }
      // assign rootNode patch to index 0
      patch[0] = apply;

      var index=1, achild, bchild;

      // bMatched has been reordered and aligned with aChildren, so the same index in 
      // aChildren and bMatched always have the same layerId unless bMatched[i] is undefined
      // in which case, we know aChildren[i] will be removed 
      for(i=0; i<aChildren.length; i++) {
        achild = aChildren[i];
        bchild = bMatched[i];
        if(achild) {
          if(bchild) {
            diffLayer(achild, bchild, patch, index); // achild and bchild are of the same layerId (same layer type)
                                                     // we do a layer diff between the two, adding necessary patches to patch list
          }
          index = index + achild.count;
        }
      }

      return patch;
    }

    /**
     * reorderToMatch
     *
     * layers of same layerId are aligned, all added children in `bs`
     * are appended after the last position of `as`
     * and all children from 
     * `as` that needs to be removed, we have their indices moved to the bottom, the original
     * position in `as` for removed children are filled with `undefined`
     *
     * @example
     *
     * // [{ layerId : 1, <a1> }, { layerId : 2, <a2> }, { layerId : 3, <a3> }] and 
     * // [{ layerId : 2, <b1>}, { layerId : 2, <b2>}, { layerId : 1, <b3>}, { layerId : 6, <b4> }]
     * // results in:
     *
     * // result = [{ layerId : 1, <b3>}, { layerId : 2, <b1> }, undefined, { layerId : 2, <b2>}, { layerId : 6, <b4>} ]
     * // result.shuffle = [1,3,0,4,2]
     * //
     * // apply result.shuffle as a reordering for result, we get:
     * // [{ layerId:2, <b1>}, { layerId : 2, <b2> }, { layerId : 1, <b3> }, { layerId : 6, <b4> }, undefined]
     * // this is the original `bs` input, will undefined corresponds to children in `as` that needs to be removed
     *
     * @param {[VNode | VLayer]} - list of VNodes children (as)
     * @param {[VNode | VLayer]} - list o VNodes children (bs)
     * @return {[VNode| VLayer]} -  array or reordered children from `bs`, the array has property .shuffle
     *                              shuffle is another array of indices or reordering when applied to 
     *                              reordered `bs` that will recreate original `bs`
     */
    function reorderToMatch(a, b) {
      var bGrouped = groupIndex(b);
      var i, aid, aIndex, bIndex;
      
      var order = [];
      
      var shuffle = [];
      var removed = [];

      var bs;
      
      for(i=0;i<a.length;i++) {
        aid = a[i].layerId;
        bs = bGrouped[aid];
        if(bs && bs.length) {
          bIndex = bs.shift();
          order.push( b[bIndex] ); 
          shuffle[bIndex] = i;
        } else {
          order.push(undefined);
          removed.push(i);
        }
      }
      aIndex = a.length;
      for(var id in bGrouped) {
        bs = bGrouped[id];
        if(bs.length) {
          for(i=0;i<bs.length;i++) {
            bIndex = bs[i];
            order.push(b[bIndex]);
            shuffle[bIndex] = aIndex;
            aIndex++;
          }
        }
      }
      
      order.shuffle = shuffle.concat(removed);
      return order;
    }

    /**
     * groupIndex
     *
     * groupBy operation with output stores indices instead of items 
     *
     * @param {array} - arr
     * @return {object}
     */
    function groupIndex(arr) {
      var ids = {};
      var id;
      for(var i=0; i<arr.length; i++) {
         id = arr[i].layerId;
         ids[id] = ids[id] || [];
         ids[id].push(i);
      }
      return ids;
    }

    /**
     * appendPatch
     *
     * helper function to append VPatches to existing one,
     * make an array of VPatches, if there are more than one
     *
     * @param {[VPatch] | VPatch} - apply
     * @param {VPatch} - patch
     * @return {[VPatch] | VPatch}
     */
    function appendPatch(apply, patch) {
      if (apply) {
        if (isArray(apply)) {
          apply.push(patch);
        } else {
          apply = [apply, patch];
        }

        return apply;
      } else {
        return patch;
      }
    }

    // Public Api
    return diffChart;
  });
