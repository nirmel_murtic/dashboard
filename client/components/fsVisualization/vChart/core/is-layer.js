'use strict';

angular.module('fs-visulization-core')
  .factory('isLayer', function() {
    return function isLayer(l) {
      return l.type === 'VirtualLayer';
    };
  });
