'use strict';

angular.module('fs-visulization-core')
  .factory('createElement', function(invariant, VNode, isVNode, applyProperties) {
    /**
     * createElement
     *
     * recursively create dom node from VNode
     *
     * @param {VNode} - vnode
     * @param {object} - opts
     * @return {DomNOde}
     */
    function createElement(vnode, opts) {
      var doc = opts ? opts.document || document : document;
      
      opts = opts || { document: doc, nextFrame : [] };

      invariant(isVNode(vnode),
        'Must create DOM element from VNode');

      var node = (vnode.namespace === null) ?
        doc.createElement(vnode.tagName) :
        doc.createElementNS(vnode.namespace, vnode.tagName);

      var props = vnode.properties;

      // previous: null, suspendTransition: true
      applyProperties(vnode, node, props, null, true);

      if(vnode.didMount) {
        opts.nextFrame.push({ vNode : vnode, domNode : node }); 
      }

      var children = vnode.children;

      for (var i = 0; i < children.length; i++) {
        var childNode = createElement(children[i], opts).domNode;
        if (childNode) {
          node.appendChild(childNode);
        }
      }


      return { domNode : node, opts : opts };
    }

    return createElement;
  });
