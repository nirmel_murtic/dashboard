'use strict';

angular.module('fs-visulization-core')
  .factory('vLayer', function(invariant, noop, True, uniqueId, isVNode, ChartsAnimator) {
    var noProps = {};
    function Layer( layerDefs ) {
      invariant(layerDefs.render, 
        'Layer must have a render method.');

      var _shouldUpdate = True;
      for(var method in layerDefs) {
        if(method === 'render') {
          this.renderVNode = layerDefs.render;
        } else if (method === 'shouldUpdate') {
          _shouldUpdate = layerDefs[method];
        } else {
          this[method] = layerDefs[method];
        }
      }

      this.shouldUpdate = function(props) {
        return ChartsAnimator._tid !== null || _shouldUpdate.call(this, props); 
      };
      this.layerId = uniqueId('layer');
    }

    Layer.prototype.type = 'VirtualLayer';
    Layer.prototype.render = function(previous) {
      if(previous && isVNode(previous) && !this.shouldUpdate.call(previous, this.props)) {
        return previous;
      }
      var vNode = this.renderVNode();
      invariant(isVNode(vNode), 
        'Ill defined render method for layer %s, expect render to return a VNode, got %s instead', 
        this.layerId, vNode);
      vNode.props = this.props;
      vNode.layerId = this.layerId;
      if(this.didMount) {
        vNode.didMount = this.didMount;
      }
      if(previous && isVNode(previous)) {
        var len1 = previous.children.length,
            len2 = vNode.children.length,
            len = len1 < len2 ? len1 : len2;
        for(var i=0; i<len; i++) {
          nextPointer(vNode.children[i], previous.children[i]);
        }
      }
      return vNode;
    };

    // a <- b
    function nextPointer(a, b) {
      b.nextVersion = a;
      for(var i=0; i<b.children.length; i++) {
        nextPointer(a.children[i], b.children[i]);
      }
    }


    function renderLayer(Layer, props) {
      
      var layer = Object.create(Layer);
      if(props.key) {
        layer.layerId = layer.layerId + String(props.key);
      }
      layer.props = props || noProps; 
      return layer;

    }

    // Public Api
    return {
      // define Layer type, an internal layerId will be assigned, this will signal the 
      // diff algorithum that two layers are compariable and should be diffed if and only if
      // they have the same layerId
      // defs should be an object with render, [ shouldUpdate, didUpdate, didMount ] methods
      define : function(defs) {
        return new Layer(defs);
      },
      // render takes a VLayer, and inputs (props) and produces a vLayer (a thunk for a layer VNode)
      render : renderLayer
    };
  });
