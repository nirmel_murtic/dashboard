'use strict';

describe('Function: createElement', function () {

  // load the directive's module and view
  beforeEach(module('fs-visulization'));
  beforeEach(module('fs-visulization-core'));


  var createElement, VNode, isVNode;

  beforeEach(inject(function (_createElement_, _VNode_, _isVNode_) {
    createElement = _createElement_;
    VNode = _VNode_;
    isVNode = _isVNode_;
  }));

  it('Should throw error when pass in first arg that is not a VNode', function() {
    expect(createElement.bind(null)).toThrow(); 
    expect(createElement.bind(null, 1)).toThrow(); 
    expect(createElement.bind(null, true)).toThrow(); 
    expect(createElement.bind(null, {})).toThrow(); 
    expect(createElement.bind(null, '<div>')).toThrow(); 
  });

  it('Should create a SvgDOM node from a VNode (non-recursive case), with proper namespace', function() {
    var g = new VNode('g', null, null);
    var domNode = createElement(g).domNode;
    expect(domNode instanceof SVGElement).toBe(true);
    expect(domNode.namespaceURI).toBe('http://www.w3.org/2000/svg');
  });

  it('Should create DOM nodes recursively', function() {
    var g = new VNode('g', null, [
      new VNode('circle', {
        cx : 10, 
        cy : 10,
        r : 5
      }), 
      new VNode('circle', {
        cx : 10, 
        cy : 10,
        r : 5
      }) 
    ]);
    var domNode = createElement(g).domNode;
    expect(domNode instanceof SVGElement).toBe(true);
    expect(domNode.namespaceURI).toBe('http://www.w3.org/2000/svg');
    expect(domNode.childNodes.length).toBe(2);
    expect(domNode.childNodes[0].tagName).toBe('circle');
    expect(domNode.childNodes[1].tagName).toBe('circle');
  });

  it('Should create DOM node that matches input VNode\'s properties/attributes', function() {
    var g = new VNode('g', { className : 'contains-rect' }, [
      new VNode('rect', {
        x: 10,
        y: 10, 
        width: 200,
        height: 50
      }) 
    ]);
    var domNode = createElement(g).domNode;
    var rectNode = domNode.childNodes[0];
    expect(domNode.tagName).toBe('g'); 
    expect(domNode.getAttribute('class')).toBe('contains-rect');
    expect(rectNode.getAttribute('x')).toEqual('10');
    expect(rectNode.getAttribute('y')).toEqual('10');
    expect(rectNode.getAttribute('width')).toEqual('200');
    expect(rectNode.getAttribute('height')).toEqual('50');
  });

});
