'use strict';

describe('Function: diffStatic', function () {

  // load the directive's module and view
  beforeEach(module('fs-visulization-core'));

  var diffStatic, VNode, VPatch;

  beforeEach(inject(function(_diffStatic_, _VNode_, _VPatch_) {
    diffStatic = _diffStatic_;
    VNode = _VNode_;
    VPatch = _VPatch_;
  }));

  it('Should report no diff for two refrencially equal VNodes', function() {
    var a = new VNode('g');
    var b = a;
    expect(diffStatic(a, b)).toBe(false);
  });

  it('Should report no diff for two empty <g> VNodes', function() {
    var a = new VNode('g');
    var b = new VNode('g');
    expect(diffStatic(a, b)).toBe(false);
  });

  it('Should report no diff for two <g> VNodes, with same className', function() {
    var a = new VNode('g', {
      className : 'a'
    });
    var b = new VNode('g', {
      className : 'a'
    });
    expect(diffStatic(a, b)).toBe(false);
  });

  it('Should produce "PROPS" patch for two <g> VNodes with different className', function() {
    var a = new VNode('g', {
      className : 'class1'
    });
    var b = new VNode('g', {
      className : 'class2'
    });
    var patches = {
      left : a
    };
    expect(diffStatic(a, b, patches)).toBe(true);
    expect(patches['0'].type).toBe(VPatch.PROPS);
  });

  it('Should diff recursively', function() {
    var rect1 = new VNode('rect', {
      x : 10
    });
    var rect2 = new VNode('rect', {
      x : 12
    });
    var a = new VNode('g', null, [rect1]);
    var b = new VNode('g', null, [rect2]);
    var patches = {
      left : a
    };
    expect(diffStatic(a, b, patches)).toBe(true);
    expect(Object.keys(patches).length).toBe(2);
    expect(patches['1'].type).toBe(VPatch.PROPS);
  });

  it('Should throw error if two input VNodes are not structrually similar', function() {
    var rect1 = new VNode('rect', {
      x : 10
    });
    var a = new VNode('g', null, [rect1]);
    var b = new VNode('g');
    expect(diffStatic.bind(null, a, b)).toThrow();
  });

});
