'use strict';

angular.module('fs-visulization-core')
  .factory('diffStatic', function(invariant, isEqual, VPatch, diffProps, handleThunk) {
    
    // jshint unused : false
    /**
     * diffStatic
     *
     * diffs two 'static' vNodes a and b, they must be structually identical, and possibly differ
     * only in properties in one or more of their child nodes
     *
     * 
     *
     * @param {VNode} - a
     * @param {VNode} - b
     * @param {object} - patch list, the function will update patch list recursively 
     * @param {number} - index, the staring index of vNode a, in the root dom tree, refer to domIndex function
     *                   for more info (dom-index.js)
     * @return {boolean} - whether a and b are different (taken children differences into account)
     */
    function diffStatic(a,b, patch, index) {
      patch = patch || { left: a };
      index = index || 0;
      return walk(a, b, patch, index);
    } 

    function walk(a,b, patch, index) {
      // be design, VNodes are immutable, therefore
      // strict equality garentees a and b are identical
      if( a=== b ) {
        return false;
      } 
      /*
      if( isThunk(b) || isThunk(a) ) {
        thunks(a, b, patch, index);
        return;
      }
      */
      invariant(
        a.tagName === b.tagName, 
        'Cannot diff different tags: %s %s',
        a.tagName,
        b.tagName
      );
      invariant(
        a.children.length === b.children.length,
        'Cannot diff structually different nodes'
      );

      // boolean flage to recored whether two VNodes are different
      // and at least one of their children are different
      // used for vLayer's didUpdate and didMount hooks
      // see layer.js
      var different = false, childDifferent = false;

      // diff properties
      var propsPatch = diffProps(a.getProperties(), b.getProperties());
      if(propsPatch) {
        different = true;
        patch[index] = new VPatch(VPatch.PROPS, a, propsPatch);
      }
      var i, len = a.children.length, aChild;
      index = index + 1;
      // recursively diff children
      if(len) {
        for(i=0;i<len;i++) {
          aChild = a.children[i];
          childDifferent = walk(aChild, b.children[i], patch, index);
          if(!different && childDifferent) {
            different = true;
          }
          index = index + aChild.count;
        }
      }
      // different === false only if vNodes are identical and all their childrens
      // are identical
      return different;
    }

    /**
     * thunks
     *
     * thunks are not implemented at VNode level yet, this function is not 
     * called by anything at this moment, it will become useful once we
     * start implement thunks
     *
     * @param a
     * @param b
     * @param patch
     * @param index
     * @return {undefined}
     */
    function thunks(a, b, patch, index) {
      var nodes = handleThunk(a, b);
      var thunkPatch = diffStatic(nodes.a, nodes.b);
      if (hasPatches(thunkPatch)) {
        patch[index] = new VPatch(VPatch.THUNK, null, thunkPatch);
      }
    }

    /**
     * hasPatches
     * 
     * unused function at this moment
     *
     * @param patch
     * @return {undefined}
     */
    function hasPatches(patch) {
      for (var index in patch) {
        if (index !== 'left') {
          return true;
        }
      }

      return false;
    }
   
    // exports/public api
    return diffStatic;
   
  });
