'use strict';

angular.module('fs-visulization')
  .factory('vChart', function(invariant, VNode, uniqueId, isVNode) {
    var noLayers = [];
    function VChart(layers) {
      this.children = layers || noLayers;
    }
    VChart.prototype.type = 'VirtualChart';
    VChart.prototype.renderVNode = function(previous) {
      var renderedLayers;
      if(previous && isVNode(previous)) {
        renderedLayers = this.children.map(function(layer, i) {
          return layer.render(previous.children[i]);
        });
      } else {
        renderedLayers = this.children.map(function(layer) {
          return layer.render();
        });
      }
      return new VNode('g', null, renderedLayers);
    };
    
    function defineChart(defs) {
      invariant(defs && defs.render, 'Must supply a render method for a chart.');
      defs.chartId = uniqueId('chart');
      return defs;
    }

    /**
     * renderChart
     *
     * @param {VChartClass} - Chart
     * @param {object} - props
     * @param {boolean} - asVNode, if true, render to VNode else to an instance of VChart
     * @return {vChart | vNode}
     */
    function renderChart(Chart, props, asVNode) {
      var chart = Object.create(Chart);
      chart.props = props;
      var vLayers = chart.render(); // vLayer instance
      return asVNode ?  ( new VChart(vLayers) ).renderVNode() : new VChart(vLayers);
    }

    // Public Api
    return {
      // VChart is constructed by supplying a render method, which should return 
      // an array of vLayers
      define : defineChart,
      render : renderChart
    };  

  });
