'use strict';

angular.module('fs-visulization-core')
  .factory('vTag', function(VNode) {
    // shorthand function for making VNodes
    return function vTag(tagName, props, children) {
      return new VNode(tagName, props, children);
    };
  });
