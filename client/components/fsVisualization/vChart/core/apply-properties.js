'use strict';

angular.module('fs-visulization-core')
  .factory('applyProperties', function(isFunction, isVNode, invariant, VTransition, ChartsAnimator, VNode) {
    /**
     * applyProperties
     *
     * @param {DomNode} - node
     * @param {object} - props, basically dom node's attributes, with a few exceptions, see below
     * @param {object} - previous props, not really used atm, will have effects if and once thunks are implemented
     * @param noTransition - flag to turn off transition (on by default)
     * @return {undefined}
     */
    function applyProperties(vnode, node, props, previous, noTransition) {
      invariant(isVNode(vnode), 'Apply property needs to know about vnode!');
      var transAttrs = {}, hasTransition = noTransition ? false : !props.noTransition;
      for (var propName in props) {
        
        var propValue = props[propName];
        // noTransition flag in properties will turn off transitions
        if (propName === 'noTransition') {
          continue;
        }
        // property names like onMouseover, onClick etc., they are event handlers
        // however, use event handler props with causion, they are bad for perf
        if (isEventHandler(propName)) {
          if(isFunction(propValue)) {
            addListener(node, toEventHandler(propName), propValue); 
          }
        // class is a reserved keyword in ES6, we use className for class attribute
        } else if(propName === 'className') {
          node.setAttribute('class', propValue || '');
        // all vNodes differ from their real counter part in that they can not have text nodes as children
        // so contents property is used to set text contents for a <text> node (or <tspan> or anyting that can container text)
        } else if(propName === 'contents') { 
          var text = document.createTextNode(propValue);
          while(node.childNodes.length) {
            node.removeChild(node.firstChild);
          }
          node.appendChild(text);
        // need to remove the attribute 
        } else if (propValue === undefined) {
          removeProperty(node, propName, propValue, previous);
        // we stringify style object into a string the brower recognizes
        } else if (propName === 'style') {
          node.setAttribute('style', stringifyStyle(propValue));
        // if given tagName and attribute/property is transitionable, and no flags tell us to turn off transition
        // add pending transition attribute and destination value to transAttrs object/map
        } else if(!props.noTransition && !noTransition && VNode.propertySupportTransition(node.tagName, propName, propValue)) {
          hasTransition = true;
          transAttrs[propName] = propValue;
        // base case: just set the attribute with property value
        } else {
          node.setAttribute(propName, propValue);
        }

        hasTransition = hasTransition || _.size(transAttrs) > 0;

        var oldAttrs, vtrans;
        if(hasTransition) {
          oldAttrs = {};
          for(var attr in transAttrs) {
            oldAttrs[attr] = vnode.getProperty(attr); 
          }
          vtrans = new VTransition(vnode, oldAttrs, transAttrs);
          ChartsAnimator.push({
            node : node,
            vnode : vnode,
            vtrans : vtrans
          });
        }
      }
    }

    function unCamel(str) {
      return str.replace(/([A-Z])/g, function($1){return '-'+$1.toLowerCase();});
    }

    function isEventHandler(str) {
      return str === 'onClick' || 
             str === 'onMouseover' || 
             str === 'onMouseout' ||
             str === 'onMousedown' ||
             str === 'onMouseup'; 
    }

    function toEventHandler(str) {
      return str.slice(2).toLowerCase();
    }

    function stringifyStyle(style) {
      return _.reduce(style, function(str, val, key) {
        return str + (unCamel(key) + ':' + val) + ';';
      }, '');
    }

    /*
    var transitionEnd = function() {
      var sel = d3.select(this);
      var attrs = sel.property('data-transition-attrs');
      var attr;
      if(attrs && Object(attrs) === attrs) {
        for(attr in attrs) {
          // jshint eqeqeq:false
          if(sel.attr(attr) != attrs[attr]) {
            if(__DEV__) {
              console.warn('Orphaned Chart Transition:', attr, attrs[attr], sel.attr(attr));
            }
            sel.attr(attr, attrs[attr]); 
          }
        }
      }
      sel.property('data-transition-attrs', null);
    };

    function applyTrans(node, attrs) {
      var sel = d3.select(node);
      var lastAttrs = sel.property('data-transition-attrs');
      if(lastAttrs) {
        sel.interrupt();
        // sel.attr(lastAttrs);
      }
      sel.property('data-transition-attrs', attrs);
      sel.transition()
        .attr(attrs)
        .each('end', transitionEnd);
    }
    */

    function removeProperty(node, propName, propValue) {
      // if (previous) {
      //  var previousValue = previous[propName];
      // }
      node.setAttribute(propName, propValue);
    }

    function addListener(node, type, callback) {
      type = type + '.chart';
      var sel = d3.select(node);
      sel.on(type, null);
      sel.on(type, callback);
      node.__hasListener__ = true;
    }

  return applyProperties;
});
