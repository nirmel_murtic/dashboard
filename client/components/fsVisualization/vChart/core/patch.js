'use strict';

angular.module('fs-visulization-core')
  .factory('patch', function(invariant, VPatch, applyProperties, domIndex, isArray, createElement) {
    // jshint bitwise:false, newcap:false, -W030
    /**
     * patch
     *
     * given a list of patches (in a hashmap), apply them in 
     * order to the rootNode (a DOM node attached in the document)
     *
     * @param {DomNode} - rootNode
     * @param {object} - patches, patch list
     * @return {DomNode} - the same rootNode passed in
     */
    var patchDefaultOpts = { transition: true };
    function patch(rootNode, patches, opts) {
      opts = opts || patchDefaultOpts;
      // get all indices the patch list contains
      // indices are used to find individual DOM nodes
      // from rootNode
      var indices = patchIndices(patches);

      // no patching required
      if (indices.length === 0) {
        return rootNode;
      }

      // index is a hashmap with indices (numbers) as key,
      // and DOM nodes as values 
      var index = domIndex(rootNode, patches.left, indices);
      // document object
      // unused for now
      // var ownerDocument = rootNode.ownerDocument;

      // for VPatches of type VPatch.FOLLOW_UP, we batch
      // apply them after all other patches have been applied
      // these patches are typically related to vLayer's didUpdate
      // and didMount methods
      // see: layer.js and diff-layer.js
      var nextFrame = [], nodePatch;

      for (var i = 0; i < indices.length; i++) {
        var nodeIndex = indices[i];
        applyPatch(rootNode,
          index[nodeIndex],
          patches[nodeIndex], nextFrame, opts);
      }
      if(nextFrame.length) {
        for (i = 0; i < nextFrame.length; i++) {
          nodePatch = nextFrame[i];
          applyPatch(rootNode,
            nodePatch.node,
            nodePatch.patch, null, opts);
        }
      }

      return rootNode;
    }

    
    /**
     * applyPatch
     *
     * @param {DomNode} - rootNode
     * @param {DomNode} - domNode, the domNode this given patch(es) should apply to
     * @param {object} - patchList, a single patch or an array of them
     * @param {array} - nextFrame, stores the VPatch.FOLLOW_UP patches
     * @param {Object} - opts
     * @return {DomNode} - the same domNode passed in
     */
    function applyPatch(rootNode, domNode, patchList, nextFrame, opts) {
      // missing domNode, do nothing
      if (!domNode) {
        return rootNode;
      }
      
      // a number of patches
      if (isArray(patchList)) {
        for (var i = 0; i < patchList.length; i++) {
          // push FOLLOW_UP patches to nextFrame
          if(nextFrame && patchList[i].type === VPatch.FOLLOW_UP) {
            // remembers which domNode are we talking about
            nextFrame.push({ node: domNode, patch : patchList[i]});
          } else {
            // the patch is applied this frame
            patchOp(domNode, patchList[i], opts);
          }
        }
      // just one patch
      } else {
        if(nextFrame && patchList.type === VPatch.FOLLOW_UP) {
          nextFrame.push({ node: domNode, patch : patchList });
        } else {
          patchOp(domNode, patchList, opts);
        }
      }

      return rootNode;
    }

    /**
     * patchIndices
     *
     * extract all keys from a patch list object
     * these keys are numbers (dom indices) used to
     * find mounted dom nodes from a single rootNode
     *
     * @param {object} - patches
     * @return {array} - indices
     */
    function patchIndices(patches) {
      var indices = [];

      for (var key in patches) {
        // patches.left is the VNode correspoinding to rootNode (a mounted dom node)
        if (key !== 'left') {
          indices.push(Number(key));
        }
      }

      return indices;
    }

    /**
     * patchOp
     *
     * patch operation based on VPatch type
     *
     * @param {DomNode} - domNode
     * @param {VPatch} - vpatch
     * @param {Object} - opts
     * @return {DomNode} - domNode
     */
    function patchOp(domNode, vpatch, opts) {
      var type = vpatch.type;
      var patch = vpatch.patch;
      var vnode = vpatch.vNode;
    
      switch (type) {
        case VPatch.ORDER:
          return reorderChildren(domNode, patch);
        case VPatch.REMOVE:
          return removeNode(domNode);
        case VPatch.REMOVE_TAIL:
          return removeChild(domNode, true);
        case VPatch.REMOVE_HEAD:
          return removeChild(domNode, false);
        case VPatch.FOLLOW_UP:
          patch.call(vnode, domNode); 
          return domNode;
        case VPatch.INSERT:
          return insertNode(domNode, patch);
        case VPatch.PROPS:
          // last argument of applyProperties is noTransition
          applyProperties(vnode, domNode, patch, null, !opts.transition);
          return domNode;
        default:
          return domNode;
      }
    }

    function removeNode(domNode) {
       var parentNode = domNode.parentNode;

       if (parentNode) {
           parentNode.removeChild(domNode);
       }
      
       return null;
    }
    // remove a childNode from domNode either at the head position 
    // or the tail position (when tail = true)
    function removeChild(domNode, tail) {
       var child = tail ? domNode.lastChild : domNode.firstChild;

       if(child.__hasListener__) {
         d3.select(child).on('.chart', null);
       }

       domNode.removeChild(child);       


       return domNode;
    }


    function insertNode(parentNode, vNode) {
       var newNode = createElement(vNode).domNode;
       
       parentNode.appendChild(newNode);
      
       return parentNode;
    }

    // O(nlog(n))
    function longestIncreasingSeqIndex(xs) {
      var lisEnd = [];
      var lisPred = [];
      var xlen = xs.length;
      var i, len, newLen, lo, hi, mid;
      len = 0;
      for(i=0;i<xlen;i++) {
        lo = 1;
        hi = len;
        
        while (lo <= hi) {
          // ceil( (lo+hi)/2 )
          mid = ( (lo+hi+1)/2 ) << 0;
          if(xs[lisEnd[mid]] < xs[i]) {
            lo = mid + 1;
          } else {
            hi = mid - 1;
          }
        }
        
        newLen = lo;
        lisEnd[newLen] = i;
        lisPred[i] = lisEnd[newLen - 1];
        newLen > len && (len = newLen);    
      }
      
      var lis = [], k=lisEnd[len];
      for(i=0;i<len;i++) {
        lis[len-i-1] = xs[k];
        k = lisPred[k];
      }
      
      return lis;
    }

    // testing the correctness of this: http://jsbin.com/wokaqi/1/edit?js,console,output
    // need to convert the above jsbin to proper unit test
    function reorderChildren(domNode, bIndex) {
      var i, j;
      var children = [];
      var len = bIndex.length;
      var childNodes = domNode.childNodes;
      var appends = [];
      var inserts;
      
      for(i=0; i<len; i++) {
        children.push(childNodes[i]);
      }
      
      var lisIndex = longestIncreasingSeqIndex(bIndex);

      j = 0, inserts=[];
      // remove childNodes not in LIS
      for(i=0; i<len; i++) {
        if(i < lisIndex[j] || j === lisIndex.length) {
          domNode.removeChild( children[i] );
        } else {
          j = j + 1;
        }
      }
      j = 0, inserts = [];
      for(i=0; i<len; i++) {
        if(bIndex[i] !== lisIndex[j]) {
          inserts.push( bIndex[i] );
        } else if (j < lisIndex.length) {
          j = j + 1;
          appends.push(inserts);
          inserts = [];
        } else {
          inserts.push( bIndex[i] );
        }
      }
      appends.push(inserts);
      
      var refChild;
      for(i=0; i<appends.length; i++) {
        inserts = appends[i];
        refChild = i >= lisIndex.length ? null : children[lisIndex[i]];
        for(j=0; j<inserts.length; j++) {
          if(refChild) {
            domNode.insertBefore(children[inserts[j]], refChild);
          } else {
            domNode.appendChild(children[inserts[j]]);
          }
        }
      }
      
      
      return domNode;
    }

    // public api
    return patch;

  });
