'use strict';

angular.module('fs-visulization-core')
  .factory('ChartsAnimator', function() {

    function Animator() {
      this._tid = null;
      this._stack = [];
      this._onEnterFrame = this._onEnterFrame.bind(this);
    }
    Animator.prototype = {
      push : function(animation) {
        this._stack.push(animation);
        if(this._tid === null) {
          this._tid = window.requestAnimationFrame(this._onEnterFrame);
        }
      },
      _onEnterFrame : function() {
        var animation;
        var nextStack = [];
        var node, vnode, vtrans, iteration, attrs, attr;
        /*jshint camelcase: false */
        /*jshint -W084 */
        while(animation = this._stack.pop()) {
          node = animation.node;
          vnode = animation.vnode;
          vtrans = animation.vtrans; 

          if(node.__last_tid__ === this._tid) {
            // DOM node has been handled this tick, newer animation overrides older ones (if present and unfinished)
            vtrans.next({ done: 0 });  
            continue;
          }

          iteration = vtrans.next();
          animation.vnode = vtrans.vnode;
          attrs = iteration.value;

          for(attr in attrs) {
            node.setAttribute(attr, attrs[attr]);
          }
          node.__last_tid__ = this._tid;
           
          if(!iteration.done) {
            nextStack.push(animation);
          }
        }

        var len = nextStack.length;
        for(var i=0; i<len; i++) {
          this._stack[i] = nextStack[len - i - 1];
        }
        nextStack.length = 0;
        
        var ended = this._stack.length === 0;
        if(!ended) {
          this._tid = window.requestAnimationFrame(this._onEnterFrame);
        } else {
          // this._tid && window.cancelAnimationFrame(this._tid);
          this._tid = null;
        }
      }
    };


    // polyfill requestanimationframe
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || 
                                   window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }
 
    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }
    // end polyfill

    var animator = new Animator(); 
    window.animator = animator;
    return animator;

  });
