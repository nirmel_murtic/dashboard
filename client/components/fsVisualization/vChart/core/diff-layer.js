'use strict';

angular.module('fs-visulization-core')
  .factory('diffLayer', function(invariant, isLayer, isVNode, VPatch, diffStatic, diffProps, isArray) {
    /**
     * diffLayer
     *
     * diff left and right, left needs to be a vNode, right can be a VLayer(thunk for vNode) or a vNode 
     *
     * @param {VNode} - left
     * @param {VNode | VLayer} - right
     * @param {object} - patch, patch list, we will update it
     * @param {number} - dom index for left vNode, index
     * @return {boolean} - different or not
     */
    function diffLayer(left, right, patch, index) {
      // type checks
      invariant(isVNode(left) && (isLayer(right) || isVNode(left)) && left.props,
        'diffLayer got wrong type of inputs, expect vNode, vLayer or vNode, vNode'
      );
      var rightVLayer;
      // saves right in the name of rightVLayer, if it is a VLayer instead of a vNode
      if(isLayer(right)) {
        rightVLayer = right;
        // render right vLayer into a vNode, given left is its previous version
        // this step allows vLayer's shouldUpdate hook to smartly optimize vNode rendering
        right = right.render(left);
      } 
      // init patch list
      patch = patch || { left: left };
      if(left === right) {
        return patch;
      }
      // the children
      var leftChildren = left.children;
      var rightChildren = right.children;
      var i,len;
      // diff the properties between left and right (now both are vNodes)
      var propsPatch = diffProps(left.properties, right.properties);
      // rootIndex is 0 if not specified (the top most level of a mounted vNode)
      var rootIndex = index || 0;
      index = index || 0;

      // flags to keep track if left and right are different
      var different, childDifferent;

      // add properties patch to patch list
      if(propsPatch) {
        different = true;
        patch[rootIndex] = new VPatch(VPatch.PROPS, left, propsPatch);
      }
      // index + 1 is the index of left's first child
      index++;

      // children diffs are native left to right
      //
      // tracking ids or keys are pointless at layer level as typically ordering is non-important
      // we track layerIds at chart diff level
      //
      // some children got removed
      if(leftChildren.length > rightChildren.length) {
        len = rightChildren.length;
        for(i=0;i<len;i++) {
          // diff i'th child
          diffStatic(leftChildren[i], rightChildren[i], patch, index);
          index += leftChildren[i].count;
        }
        // remove children at tail position
        for(i;i<leftChildren.length;i++) {
          patch[rootIndex] = appendPatch(patch[rootIndex], new VPatch(VPatch.REMOVE_TAIL, left, null));
        }
        different = true;
      // some children got added
      } else if (leftChildren.length < rightChildren.length) {
        len = leftChildren.length;
        for(i=0;i<len;i++) {
          diffStatic(leftChildren[i], rightChildren[i], patch, index);
          index += leftChildren[i].count;
        }
        for(i;i<rightChildren.length;i++) {
          patch[rootIndex] = appendPatch(patch[rootIndex],
            new VPatch(VPatch.INSERT, left, rightChildren[i]));
        }
        different = true;
      // excat same number or children, diff them one by one
      } else {
        len = leftChildren.length;
        for(i=0;i<len;i++) {
          childDifferent = diffStatic(leftChildren[i], rightChildren[i], patch, index);
          if(!different && childDifferent) {
            different = true;
          }
          index += leftChildren[i].count;
        }
      }

      // if left and right layers are different, and this layer type has shouldUpdate hook implemented
      // we need to add FOLLOW_UP patches, so that the hook gets executed after 'normal' patches 
      // got applied
      if(different && rightVLayer && rightVLayer.didUpdate) {
        patch[rootIndex] = appendPatch(patch[rootIndex], new VPatch(VPatch.FOLLOW_UP, right, rightVLayer.didUpdate));
      }

      return patch;
    }

    // helper function to push patches to the same vNode
    function appendPatch(apply, patch) {
      if (apply) {
        if (isArray(apply)) {
          apply.push(patch);
        } else {
          apply = [apply, patch];
        }

        return apply;
      } else {
        return patch;
      }
    }

    // public api
    return diffLayer;

  });
