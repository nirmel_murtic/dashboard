'use strict';

angular.module('fs-visulization-core')
  .factory('fsInterpolate', function() {

    function interpolatePath(d1, d2) {
       var n1 = nPoints(d1), n2 = nPoints(d2);
       var i, f, d, p, p1, p2;
       if(n1 === n2) {
         return d3.interpolate(d1, d2);
       }
       if(n1 < 2 || n2 < 2) {
         return d3.interpolate(d1, d2);
       }
       if(n1 > n2) {
         d = d2;
         p = lastPoint(d2, true);
         for(i=n1; i<n2; i++) {
           d += ('L' + p);
         }
         f = d3.interpolate(d1, d);
         return function(a) {
           if(a >= 1) {
             return d2;
           }
           return f(a); 
         }; 
       }
       if(n1 < n2) {
         p1 = lastPoint(d1);
         p2 = lastNPoints(d2, n2 - n1);
         for(i=n1; i<n2; i++) {
           // p = p2[i-n1][0] + ',' + p1[1];
           p = p1[0] + ',' + p2[i-n1][1];
           d1 += ('L' + p);
         }
         return d3.interpolate(d1, d2);
       }
    }

    var pointChar = /[\d\.\,]/;
    function lastPoint(d, asString) {
      var s = '';
      var n = d.length - 1;
      while( pointChar.test(d.charAt(n)) ) {
        s = d.charAt(n) + s;
        n--;
      }
      if(asString) {
        return s;
      }
      return s.split(',').map(function(x) {
        return Number(x);
      });
    }

    function lastNPoints(d, n) {
      var i = d.length - 1;
      while(i >= 0 && n>0) {
        i--;
        if(d.charAt(i) === 'L' || d.charAt(i) === 'l') {
          n--;
        }
      }
      d = d.slice(i+1, d.length); 

      return d.split(/[lL]/).map(function(x) {
        return x.split(',').map(Number);
      });

    }

    function lastChar(x) {
      if(x.length) {
        return x.charAt(x.length - 1);
      }
      return '';
    }

    function nPoints(d) {
      var n = 1;
      for(var i=0; i<d.length; i++) {
        if (d.charAt(i) === 'L') {
          n++;
        }
      } 
      return n;
    }

    return function interpolate(a, b) {
      var acopy, bcopy;
      for(var prop in a) {
        if(prop === 'd' && lastChar(a.d) !== 'Z' && lastChar(a.d) !== 'z') {
          acopy = _.assign({}, a);        
          bcopy = _.assign({}, b);        
          delete acopy.d;
          delete bcopy.d;
          break;
        } 
      }
      if(!acopy) {
        return d3.interpolate(a, b);
      }
      var pathInterpolator = interpolatePath(a.d, b.d);
      var restInterpolator = d3.interpolate(acopy, bcopy);

      return function(a) {
        var d = pathInterpolator(a);
        var rest = restInterpolator(a);
        rest.d = d;
        return rest;
      };

    };
  });
