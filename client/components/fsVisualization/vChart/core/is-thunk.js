'use strict';

angular.module('fs-visulization-core')
  .factory('isThunk', function() {
    return function isThunk(t) {
      return t && t.type === 'Thunk';
    };
  });
