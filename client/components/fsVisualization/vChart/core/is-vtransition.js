'use strict';

angular.module('fs-visulization-core')
  .factory('isVTransition', function() {
    return function(vtrans) {
      return Object(vtrans) === vtrans && vtrans.type === 'VirtualTransition';
    };
  });
