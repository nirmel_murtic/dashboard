'use strict';

angular.module('fs-visulization')
  .factory('VTransition', function(fsInterpolate) {

    function VTransition(vnode, propsFrom, propsTo) {
      this.vnode = vnode;
      this._props = _.mapValues(propsFrom, function() { return true; });
      this._ending = propsTo;
      this._done = false; 
      this._t = 0;
      this._end = this.frames;
      this.interpolator = fsInterpolate(propsFrom, propsTo);
    }

    VTransition.prototype = {
      type : 'VirtualTransition',
      frames : 12, // 60 fps, duration: 200ms 
      timestamps : [0],
      value : function() {
        return this.interpolator(this._t); 
      },
      current : function() {
        var values = null;
        if(!this._done) {
          values = this.interpolator(
            this.timestamps[this._t]
          );
        } else {
          values = this._ending;
        }
        return {
          done : this._done,
          value : values
        };
      },
      next : function(config) {
        var values = null;
        if(config && !this._done && config.done >=0) {
          this._end = Math.min(this._t + config.done, this.frames); 
          this._ending = this._end < this.frames ? this.interpolator(this._end) : this._ending;
        }
        if(!this._done && (++this._t) >= this._end) {
          this._done = true;
        }
        if(!this._done) {
          values = this.interpolator(
            this.timestamps[this._t]
          );
        } else {
          values = this._ending;
        }
        var vnode = this.vnode;
        while(vnode.nextVersion) {
          vnode.transition = null;
          vnode = vnode.nextVersion;
        }
        if(!this._done) {
          if(vnode.transition && !vnode.transition._done) {
            vnode.transition = vnode.transition._t > this._t ? this : vnode.transition;
          } else {
            vnode.transition = this;
          }
        } else if (vnode.transition === this) {
          vnode.transition = null;
        } 
        this.vnode = vnode;
        return {
          done : this._done,
          value : values
        };
      },
      hasProp : function(prop) {
        return !!this._props[prop];
      }
    };

    for (var i=0; i<VTransition.prototype.frames; i++) {
      VTransition.prototype.timestamps.push(
        1 / VTransition.prototype.frames * (i+1)
      ); 
    }

    return VTransition;

  });
