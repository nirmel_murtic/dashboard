'use strict';

angular.module('fs-visulization-core')
  .factory('domIndex', function() {
    var noChild = {}; 

    /**
     * domIndex
     *
     * important function used to find the dom nodes from a single rootNode and
     * its equivilant VNode (tree), given a list of requested indices (integers)
     *
     * @param {DomNode} - rootNode
     * @param {VNode} - tree
     * @param {array} - indices
     * @param {object} - nodes
     * @return {object} - hashmap with index as key and dom nodes as values
     */
    function domIndex(rootNode, tree, indices, nodes) {
      // asking for no indices
      if (!indices || indices.length === 0) {
        return {};
      } else {
        // sort indices in increasing order
        indices.sort(ascending);
        // recursively find dom nodes
        return recurse(rootNode, tree, indices, nodes, 0);
      }
    }

    /*
     * For example with a tree/vNode like this:
     *
     * <g>        <---- rootNode, index: 0, count: 5, including itself 
     *   <g>      <---- index : 1, count : 3, including itself
     *     <circle></circle> <--- index: 2, count : 1 (just itself)
     *     <rect></rect>     <--- index: 3, count : 1 (just itself)
     *   </g>                
     *   <path></path>  <-- index : 4, count: 1 (just itself)
     * </g>
     *
     *
     * or course, given the mounted rootNode is identical to this vNode, the same index
     * gives up the corresponding dom node for the child node in vNode tree
     */

    function recurse(rootNode, tree, indices, nodes, rootIndex) {
      // nodes will be the output map of domIndex function
      nodes = nodes || {};

      // if rootNode is given
      if (rootNode) {
        // the indices array (eg. [0, 18, 1022]) contains the close interval [rootIndex, rootIndex]
        // rootIndex = 0 for the top level rootNode
        if (indexInRange(indices, rootIndex, rootIndex)) {
          nodes[rootIndex] = rootNode;
        }

        // child vNodes
        var vChildren = tree.children;

        // the vNode tree has children
        if (vChildren) {

          // these are the mounted dom nodes correspoinding to tree vNode's children (also vNodes)
          var childNodes = rootNode.childNodes;

          for (var i = 0; i < tree.children.length; i++) {
            // rootIndex + 1 is the index for the first child's index 
            rootIndex += 1;

            var vChild = vChildren[i] || noChild;
            var nextIndex = vChild.count ? rootIndex + vChild.count - 1 : rootIndex;
            
            // skip recursion down the tree if there are no nodes down here
            if (indexInRange(indices, rootIndex, nextIndex)) {
              recurse(childNodes[i], vChild, indices, nodes, rootIndex);
            }

            rootIndex = nextIndex;
          }
        }
      }

      return nodes;
    }

    // Binary search for an index in the interval [left, right]
    function indexInRange(indices, left, right) {
      if (indices.length === 0) {
        return false;
      }

      var minIndex = 0;
      var maxIndex = indices.length - 1;
      var currentIndex;
      var currentItem;

      while (minIndex <= maxIndex) {
        // jshint bitwise : false
        currentIndex = ((maxIndex + minIndex) / 2) >> 0;
        currentItem = indices[currentIndex];

        if (minIndex === maxIndex) {
          return currentItem >= left && currentItem <= right;
        } else if (currentItem < left) {
          minIndex = currentIndex + 1;
        } else  if (currentItem > right) {
          maxIndex = currentIndex - 1;
        } else {
          return true;
        }
      }

      return false;
    }

    function ascending(a, b) {
      return a > b ? 1 : -1;
    }

    // public api
    return domIndex;

  });

