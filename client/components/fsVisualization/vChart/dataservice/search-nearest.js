'use strict';

angular.module('fs-visulization-dataservice')
   // does not requrie xs to be sorted, takes O(n) time
  .constant('searchNearest', function searchNearest(x, xs) {
    xs = xs.map(function(_x) { return x > _x ? x - _x : _x - x; }); 
    var len = xs.length, i = 0, minDiff = Infinity, minIndex = -1;
    for(i; i<len; i++) {
      if(xs[i] < minDiff) {
        minDiff = xs[i];
        minIndex = i;
      }
    }
    return minIndex;
  });
