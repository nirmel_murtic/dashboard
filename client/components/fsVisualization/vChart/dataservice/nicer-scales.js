'use strict';

angular.module('fs-visulization-dataservice')
  .service('nicerScales', function(invariant) {
    function patchDegeneratedScale(scale, opts) {
      invariant(scale.domain().length === 2, 'Sorry unknown scale given to patch.');
      opts = _.assign({
        ratioUp : 1,
        ratioDown : 1,
        valueUp : null,
        valueDown : null,
        allowNegative : false,
        nice : false
      }, opts || {});
      var domain = scale.domain();
      var newDomain;
      var baseValue;
      if(opts.valueUp !== null && opts.valueDown !== null && domain[0] === domain[1] && opts.valueUp > opts.valueDown) {
        scale.domain([opts.valueDown, opts.valueUp]);
        if(opts.nice && scale.nice) {
          scale.nice();
        }
        return scale;
      }
      if(domain[0] === domain[1]) {
        if(domain[0] < 0) {
          opts.allowNegative = true;
          baseValue = -domain[0];
        } else if(domain[0] > 0) {
          baseValue = domain[0];
        // if input domain is [0, 0] or [undefined, undefined], 
        // change domain to [0, 1]
        } else {
          baseValue = 1;
          opts.ratioDown = 1;
          opts.ratioUp = 0;
        }
        newDomain = [];
        if(opts.valueDown === null) {
          newDomain[0] = baseValue * (1 - opts.ratioDown);
        } else {
          newDomain[0] = opts.valueDown;
        }
        if(!opts.allowNegative) {
          newDomain[0] = Math.max(0, newDomain[0]);
        }
        if(opts.valueUpd === null || opts.valueUp <= newDomain[0]) {
          newDomain[1] = baseValue * (1 + opts.ratioUp);
        } else {
          newDomain[1] = opts.valueUp;
        }
        scale.domain(newDomain);
        if(opts.nice && scale.nice) {
          scale.nice();
        }
        return scale;
      }
      return scale;
    }

    function addMargins(scale, margin) {

      //configure how much blank margin you want at the top of the chart, default is 15%
      var percentBlank = margin || 15;
      var scaleFactor = 100 / (100 - percentBlank);
      var bottom = scale.domain()[0];
      var top = scale.domain()[1];
      var range = top - bottom;

      if (range > 0) {
        // always add a top margin
        scale.domain()[1] = bottom + (range * scaleFactor);
        // if the lower value is negative, add a bottom margin too
        if (bottom < 0) {
          scale.domain()[0] = top - (range * scaleFactor);
        }
      }
      return scale;
    }

    return {
      patchDegenerate : patchDegeneratedScale,
      addMargins : addMargins
    };
  });
