'use strict';

angular.module('fs-visulization-dataservice')
  .provider('Dataset', function() {

    /*jshint -W054, bitwise: false */

    /*
     * This is a thin wrapper arount an array object
     * - Store and supports basic column/field types: 'string', 'datetime', 'integer', 'float'
     * - Do type conversion automatically
     * - Compute and stores domains for each column/field
     * - has method calld3, makes calling d3 functions easier (i.e. auto config .x, .y etc.)
     */

    // private helpder functions
    var inherits; 
    if (typeof Object.create === 'function') {
      // implementation from standard node.js 'util' module
       inherits = function (ctor, superCtor) {
        ctor.super_ = superCtor;
        ctor.prototype = Object.create(superCtor.prototype, {
          constructor: {
            value: ctor,
            enumerable: false,
            writable: true,
            configurable: true
          }
        });
      };
    } else {
      // old school shim for old browsers
      inherits = function (ctor, superCtor) {
        ctor.super_ = superCtor;
        var TempCtor = function () {};
        TempCtor.prototype = superCtor.prototype;
        ctor.prototype = new TempCtor();
        ctor.prototype.constructor = ctor;
      };
    }
    function makeAccessor(keyOrAccessor) {
      if(_.isFunction(keyOrAccessor)) {
        return keyOrAccessor;
      } else {
        return new Function ('x', 'return x[\'' + keyOrAccessor.toString() + '\']' );
      }
    }

    function isDataset(d) {
      return d.type === 'DataSet';
    }

    function isDomain(d) {
      return d && d.length === 2 && d[0] !== undefined && d[1] !== undefined;
    }

    function isDate(d) {
      return d instanceof Date;
    }

    function isValidDate(d) {
      return d instanceof Date && d.toString() !== 'Invalid Date';
    }


    function pushConcat(a, b) {
      var i, len=b.length;
      for(i; i<len; i++) {
        a.push(b[i]);
      }
      return a;
    }

    function coerce(val, type) {
      switch (type) {
        case 'integer':
          val = _.isNumber(val) ? (val << 0) : (val ? parseInt(val.toString(), 10) : 0);
          return val;
        case 'string':
          val = _.isString(val) ? val : val.toString();
          return val;
        case 'float':
          val = _.isNumber(val) ? val : (val ? parseFloat(val.toString(), 10) : 0);
          return val;
        case 'datetime':
          val = isDate(val) ? val : new Date(val);
          if(!isValidDate(val)) {
            throw ('Encountered Invalid date object: ' + val);
          }
          return val;
        default:
          return val;
      }    
    }

    var noData = [];

    function Dataset(data) {
      this.data = data || noData;
      this.domains = {};
      this.init();
    }

    Dataset.prototype = {
      type : 'DataSet',
      columns : {
        x : {
          type : 'datetime'     
        },
        y : {
          type : 'integer'
        }
      },
      init : function() {
        /*jshint loopfunc: true */
        var key, accessor, type;
        for(key in this.columns) {
          this.columns[key].accessor = accessor = makeAccessor(key);
          type = this.columns[key].type;
          // convert data to correct type
          _.each(this.data, function(d) {
            d[key] = coerce(d[key], type);
          });
          this.domains[key] = d3.extent(this.data, accessor);
        }
        return this;
      },
      calld3 : function(d3Fn, scales) {
        // jshint -W030
        var key, accessor;
        for(key in this.columns) {
          accessor = this.columns[key].accessor;
          if(scales && scales[key]) {
            accessor = _.compose(scales[key], accessor);
          }
          d3Fn[key] && d3Fn[key](accessor);
        }
        return d3Fn.call(d3Fn, this.data);
      },
      join : function(ds) {
        if(isDataset(ds)) {
          this.joinDS(ds);
        } else if (_.isArray(ds)) {
          this.joinArray(ds);
        }
      },
      joinDS : function(ds) {
        var key, domain, d;
        for(key in this.columns) {
          d = ds.domains[key];
          if(!isDomain(d)) {
            return this;
          }
          domain = this.domains[key];
          domain = d3.extent(isDomain(domain)  ? d.concat(domain) : d);
          this.domains[key] = domain;
        }
        this.data = pushConcat(this.data, ds.data);
        return this;
      },
      joinArray : function(arr) {
        var key, domain, accessor, d;
        for(key in this.columns) {
          accessor = this.columns[key].accessor;
          d = d3.extent(arr, accessor);
          if(!isDomain(d)) {
            return this;
          }
          domain = this.domains[key];
          domain = d3.extent(isDomain(domain) ? d.concat(domain) : d);
          this.domains[key] = domain;
        }
        this.data = pushConcat(this.data, arr);
        return this;
      },
      getColumnNames : function() {
        return _.keys(this.columns);
      }
    };

    // Provider setup

    var Datasets = {};

    this.add = function defineDataset(name, columnDefs) {
      var Ds = function(data) {
        this.constructor.super_.call(this, data);
      };

      inherits(Ds, Dataset);
      Ds.prototype.columns = columnDefs;

      Datasets[name] = Ds;
    };

    this.$get = function() {
      return {
        getPreset : function(name) {
          return Datasets[name];
        }
      };
    }; 
  });

