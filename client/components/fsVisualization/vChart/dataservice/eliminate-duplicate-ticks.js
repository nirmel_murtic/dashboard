'use strict';

angular.module('fs-visulization-dataservice')
  .constant('eliminateDuplicateTicks', function(y, yFormat) {
    //jshint bitwise:false
    yFormat = yFormat || _.identity;
    var ticks = y.ticks().map(yFormat).map(Number);
    if(_.any(ticks, function(t) {
      return t !== (t | 0);
    })) {
      return false;
    }
    return _.uniq(ticks, true);
  });
