'use strict';

angular.module('fs-visulization-dataservice')
  .factory('dataExtent', function() {

    return function dataExtent(seriesArr, x) {
      return _.reduce(seriesArr, function(extent, series) {

        var min = d3.min(series, x);
        var max = d3.max(series, x);


        extent[0] = extent[0] === null ? min : Math.min(min, extent[0]);
        extent[1] = extent[1] === null ? max : Math.max(max, extent[1]);

        return extent;
      
      }, [null, null]);
    };    
   
  });

