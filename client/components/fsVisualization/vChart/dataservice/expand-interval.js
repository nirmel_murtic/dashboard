'use strict';

angular.module('fs-visulization-dataservice')
  .constant('expandInterval', function(interval, ratioUp, ratioDown) {
    if(arguments.length < 2) {
      ratioUp = 0;
    }
    if(arguments.length < 3) {
      ratioDown = ratioUp;
    }
    var size = interval[1] - interval[0];
    return [interval[0] - size * ratioDown, interval[1] + size * ratioUp];
  });
