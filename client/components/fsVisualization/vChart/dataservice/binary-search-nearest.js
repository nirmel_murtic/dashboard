'use strict';

angular.module('fs-visulization-dataservice')
  .constant('binarySearchNearest', function binarySearchNearest(x, xs) {
    // jshint bitwise:false
    var imin = 0,
        imax = xs.length - 1,
        imid,
        current;

    while (imin <= imax) {
      imid = ((imin + imax + 1) / 2) << 0; // ceil
      current = xs[imid];
      if(current < x) {
        imin = imid + 1;
      } else if (current > x) {
        imax = imid - 1;
      } else {
        return imid;
      } 
    }
    if(imid === imin) {
      return (imax > -1 && (current - x) > (x - xs[imax])) ? imax : imid;
    } else {
      return (imin < xs.length && (x - current) > (xs[imin] - x) ) ? imin : imid;
    }
  });
