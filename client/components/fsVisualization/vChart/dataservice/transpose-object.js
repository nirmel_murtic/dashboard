'use strict';

angular.module('fs-visulization-dataservice')
  /**
   * transposeObject
   * A function that transposes tabular data in format or array of rows into array of columns
   * Input data can have arbitrary field names, as long rows are uniform in structure
   *
   * @param data
   * @param {object} - opts
   * - opts.fixture : optional, an array of column names to add to the datums in each column
   * - opts.mapName : optional, a hashmap to rename column keys
   *
   * @example
   * var data = [ { time: '2014', clicks: 12, impressions: 13 }, { time: '2015', clicks: 22, impressions: 33 } ]
   * transposeObject(data, {
   *   fixture : 'time',
   *   mapName : {
   *     'time' : 'x',
   *     '*' : 'y' 
   *   }
   * });
   *
   * // [ [{ x: '2014', y: 12}, { 'x' : '2015', y: 22 }], [{ x: '2014', y: 13 }, { x: '2015', y : 33 }] ] 
   *
   * @return {array} - data
   */
  .constant('transposeObject', function transposeObject(data, opts) {
    if(!data.length) {
      return [];
    }
    opts = opts || {};
    // key as old name, value as new name, '*' wildcard to apply all unspecifed columns to the same name
    // eg. {
    //   'time' : 'x',
    //   '*' : 'y'
    // }
    var mapName = opts.mapName;
    // fields that should be preserved for each transposed columns
    var fixture = _.isArray(opts.fixture) ? opts.fixture : (opts.fixture ? [opts.fixture] : []);

    var datum0 = data[0];
    // all keys 
    var keys = mapName && !mapName['*'] ? _.keys(mapName) : _.keys(datum0);
    // all output columns (keys minus fixture)
    var columns = fixture.length ? _.difference(keys, fixture) : keys;    

    return _.map(columns, function(col) {
      // get a new column, with fixture fields attached
      var values = _.map(data, function(row) {
        var d = {}, key;
        _.each(fixture, function(fcol) {
          // map the name, if no mapping given, use '*' wildcard name, if no '*' given, use old name
          key = mapName ? mapName[fcol] || fcol : fcol; 
          d[key] = row[fcol];
        });
        key = mapName ? mapName[col] || mapName['*'] : col;
        if(key) {
          d[key] = row[col];
        }
        return d;
      });
      return values;
    });

  });
