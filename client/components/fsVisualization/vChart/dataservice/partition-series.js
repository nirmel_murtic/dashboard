'use strict';

angular.module('fs-visulization-dataservice')
  .factory('partitionSeries', function(invariant) {
    var t = transducers;

    /**
     * partitionSeries
     *
     * factory function to construct a partion function
     *
     * @param {string} - name
     * @param {function} - x, getter function to return x value from datum
     * @param {function} - y, getter function to return y value from datum
     * @param {function} - isNA, predicate function to return true/false for a datum
     * @return {function} - function to convert an array of series to partitioned series 
     */
    return function partitionSeries(name, x, y, isNA) {

      // na check function, if not present assign a default
      isNA = isNA || function(y) { return _.isNumber(y) && !_.isNaN(y); };

      invariant(_.isFunction(x) && _.isFunction(y), 
        'Invalid x,y getter functions.');
       
      // each datum in input series will me mapped to this format:
      // { x:<number>, y:<number>, name:<string>}
      var transform = function(d) {
        var yval = y(d);
        return {
          x : x(d),
          y : yval,
          name : name,
          na : isNA(yval)
        };
      };

      // partitioning function for transducers t.partionBy
      var partition = function(d) {
        return d.na;
      };

      // label a segment NA after partitioning
      var labelNA = t.map(function(segment) {
        if(segment && segment.length) {
          segment.NA = partition(segment[0]);
        }
        return segment;
      });

      // a transducer to compute gaps of partitioned series
      // eg. [ [d0, d1, d2], [na0, na1, na2], [d3, d4, d5]] ->
      //     [ [d2, d3] ]
      // the result is the gap that contains a entire NA segment
      var gapXf = function (xf) {
        var a = Object.create(gapXfproto);
        a.xf = xf;
        a.interval = [undefined, undefined];
        return a;
      };
      var gapXfproto = {
        '@@transducer/init' : function() {
          return this.xf['@@transducer/init']();
        },
        '@@transducer/step' : function(result, input) {
          var ret = this.xf['@@transducer/step'](result, input);
          if(input.NA) {
            this.interval[1] = this.interval[1] || _.last(input);
            this.interval[0] = this.interval[0] || _.first(input);
            input.naInterval = this.interval;
          } else {
            this.interval[1] = _.first(input);
            this.interval = [_.last(input), undefined];
          }
          return ret;
        },
        '@@transducer/result': function(result) {
          return this.xf['@@transducer/result'](result);
        }
      };

      // compose all transducers together so all data transformation
      // happens in a single iteration of input series
      var xf = t.comp(
        t.map(transform),
        t.partitionBy(partition),
        labelNA,
        gapXf
      ); 

      /**
       * partitioning and transformation function
       *
       * input : [
       *    <datum>
       * ]
       *
       * output: {
       *   name : <string>,
       *   valid : [[<datum>], [<datum>], ...], // non-NA segments
       *   invalid : [{
       *     segment : [<datum>],
       *     NA : true,
       *     NaRange : [<datum>, <datum>] // array of length 2, the interval that contains NA segemnt
       *   }, ...], // NA segments, with metadata attached (na interval as NaRange)
       *   partitioned : [ [<datum>], ... ]
       * }
       * 
       * @param {array} - data
       *
       * @return {object}
       */
      var ret = function (data) {
        var partitioned = t.into([], xf, data);
        var grouped = _.reduce(partitioned, function(g, segment) {
          if(segment.NA) {
            g.naRanges.push({
              segment : segment,
              NA : true,
              NaRange : segment.naInterval.map(function(d) {
                if(isNA(d.y)) {
                  d.y = NaN;
                }
                return d;
              })
            });
          } else {
            g.segments.push(segment);
          }
          return g;
        }, { segments: [], naRanges : [] });

        grouped.flattened = _.flatten(partitioned);
        return grouped;
      };

      ret.key = function(n) {
        name = n;   
        return ret;
      };
      ret.x = function(getterX) {
        x = getterX;
        return ret;
      };
      ret.y = function(getterY) {
        y = getterY;
        return ret;
      };
      ret.na = function(naCheck) {
        isNA = naCheck;
        return ret;
      };
      return ret;
    };
  });
