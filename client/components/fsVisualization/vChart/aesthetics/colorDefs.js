'use strict';

(function() {

  //jshint bitwise: false

  var RGBA = function(r, g, b, a) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  };

  RGBA.prototype = {
    toString : function() {
      return 'rgba(' + this.r + ',' + this.g + ',' + this.b + ',' + this.a + ')';
    },
    toRgb : function(bg) {
      if(!bg) {
        bg = {
          r : 255,
          g : 255,
          b : 255
        };
      }
      bg.r = bg.r / 255;
      bg.g = bg.g / 255;
      bg.b = bg.b / 255;

      var a = this.a;
      var r = this.r / 255 * a + (1 - a) * bg.r; 
      var g = this.g / 255 * a + (1 - a) * bg.g; 
      var b = this.b / 255 * a + (1 - a) * bg.b; 

      return d3.rgb(~~(r * 255), ~~(g * 255), ~~(b * 255));
    }
  };

  angular.module('fs-visulization')
    .constant('ColorDefs', {
      TinHeavy : d3.rgb(70, 74, 79),
      MonoMedium : d3.rgb(155, 155, 155),
      PaleDark : d3.rgb(218, 219, 219),
      PaleMedium : d3.rgb(246, 246, 246),
      NatureHeavy : d3.rgb(91, 160, 117),
      LeafHeavy : new RGBA(108, 191, 107, 1).toRgb(),
      LeafSoft : new RGBA(108, 191, 107, 0.5).toRgb(),
      LeafLight : new RGBA(108, 191, 107, 0.2).toRgb(),
      SeaLight : new RGBA(43, 174, 205, 0.2).toRgb(),
      SeaMedium : new RGBA(43, 174, 205, 0.5).toRgb(),
      SeaHeavy : d3.rgb(43, 174, 205),
      SeaDark : d3.rgb(39, 156, 183),
      // colors with alpha values
      LeafMediumAlpha : new RGBA(108, 191, 107, 1).toString(),
      LeafSoftAlpha : new RGBA(108, 191, 107, 0.5).toString(),
      LeafLightAlpha : new RGBA(108, 191, 107, 0.2).toString(),
      SeaLightAlpha : new RGBA(43, 174, 205, 0.2).toString(),
      SeaMediumAlpha : new RGBA(43, 174, 205, 0.5).toString(),
      CherryDark:                       d3.rgb(220, 75, 56),
      CherryHeavy:                      d3.rgb(220, 75, 56),
      CherryMedium:                     d3.rgb(231, 146, 135),
      CherryLight:                      d3.rgb(231, 170, 160),
      Cherry:                            d3.rgb(240, 210, 206)
    })
    .factory('colorPresets', function(ColorDefs) {

      var solids = ['SeaDark', 'LeafHeavy', 'NatureHeavy', 'MonoMedium', 'SeaHeavy', 'LeafSoft', 'PaleDark', 'SeaLight', 'LeafLight', 'SeaMedium']
      .map(function(colorName) {
        return ColorDefs[colorName];
      });

      return {
        solids : solids
      };
    });
})();
