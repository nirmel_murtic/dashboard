'use strict';

angular.module('fs-visulization')
  .directive('lineChartLegend', function() {
    return {
      restrict: 'EA',
      template : [
        '<svg width="32" height="17">',
          '<line x1="0" y1="9" x2="32" y2="9" stroke="{{ color }}" stroke-width="1"></line>', 
          '<circle cx="16" cy="9" r="5" stroke="{{ color }}" stroke-width="2" fill="#fff"></circle>', 
        '</svg>' 
      ].join(''),
      scope : {
        color : '=',
        label : '='
      }
    };
  });
