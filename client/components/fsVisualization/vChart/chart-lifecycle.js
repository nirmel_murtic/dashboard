'use strict';

angular.module('fs-visulization')
  .factory('makeChart', function(invariant, vChart, createElement, diffChart, patch, VPatch) {

    /**
     * patchVChart
     *
     * apply INSERT, ORDER, and REMOVE_TAIL patches to a vNode representing a vChart
     * 
     * the purpose of this operation is to make render vChart smarter, by matching a
     * new vChart's layers with its previous version, thus allowing for shouldUpdate 
     * optimization to kick in
     *
     *
     * @param {VNode} - vChart
     * @param {array} - patches, patches list
     * @return {VNode}
     */
    function patchVChart(vChart, patches) {
      if(!vChart || !vChart.children || !vChart.children.length) {
        return vChart;
      }
      var patch, i, len = patches.length;
      for(i=0; i<len; i++) {
        patch = patches[i];
        if(patch.type === VPatch.INSERT) {
          vChart.children.push(null); 
          // we insert null as a placeholder child, so vLayer's render function knows to discard the old vLayer and render a new one
        }
        if(patch.type === VPatch.ORDER) {
          vChart.children = d3.permute(vChart.children, patch.patch);
        }
        if(patch.type === VPatch.REMOVE_TAIL) {
          vChart.children.pop();
        }
      }
      return vChart;
    }

    /**
     * chartFactory / makeChart
     *
     * takes a Dom node / container, and returns a pure function render
     *
     * render takes a VChart and props, and is responsible for mount/update
     * charts, it does vCharts diffing and apply patches 
     *
     * @param {DomNode} - rootNode
     * @return {function} - render
     */
    return function chartFactory(rootNode, opts) {
      // jshint latedef:false
      var chartId = null;
      var chart = null;
      var domNode;
      var doTransition = true;
      if(opts && opts.transition) {
        render.transition = d3.functor(opts.transition);
      } else {
        render.transition = function(trans) {
          if(arguments.length === 0) {
            return doTransition;
          }
          doTransition = trans;
        };
      }
      function render(Chart, props) {
        if(chartId === null) {
          chartId = Chart.chartId;
        }
        invariant(chartId === Chart.chartId, 
          'Trying to render different chart types!');

        var newChart, patches, dom, nextFrame;
        // mount for the first time
        if(!chart) {
          // render chart as VNode
          chart = vChart.render(Chart, props || null, true);       
          dom = createElement(chart);
          nextFrame = dom.opts.nextFrame;
          rootNode.appendChild(dom.domNode);
          if(nextFrame && nextFrame.length) {
            for(var i = 0; i<nextFrame.length; i++) {
              nextFrame[i].vNode.didMount(nextFrame[i].domNode);
            } 
          }
          domNode = dom.domNode;
        } else {
          // render chart as VChart (layer rendering layers)
          newChart = vChart.render(Chart, props);
          patches = diffChart(chart, newChart); 
          domNode = patch(domNode, patches, { transition: render.transition() });

          var rootPatch = patches[0];
          if(rootPatch) {
            rootPatch = _.isArray(rootPatch) ? rootPatch : [rootPatch];
            // rootPatch is garenteed to list INSERT, ORDER, and REMOVE_TAIL
            // patches in this order, if such patch types exisits
            if(rootPatch.length) {
              patchVChart(chart, rootPatch);    
            }
          }

          // renderVNode takes previous vchart as arg to speeding up 
          // rendering, so unchange VNode gets reused
          var chart2 = newChart.renderVNode(chart);
          chart = chart2;
        }
      }
      render.unmount = function() {
        rootNode.removeChild(domNode);
        chart = null;
        chartId = null;
      };
      return render;
    };

  });
