'use strict';

angular.module('fs-visulization')
  .factory('StackedChart', function(accessor, vChart, vLayer, LineLayer, AreaLayer, VGridLayer, HGridLayer, PointsLayer, FullbarLayer, TooltipLayerAdvanced, GuidelineLayer, BackgroundLayer, $filter) {

    function x(d) {
      return d.x;
    }
    function y(d) {
      return d.y + d.y0;
    }
    
    var defaultTimeFormat = $filter('metricDatetime');

    return vChart.define({

      // MultiSeriesChart's render function
      render: function() {

        var data = this.props.data;
        var title = this.props.title;
        var highlights = this.props.highlights;
        var hlValues = this.props.highlightedValues;
        var xScale = this.props.xScale;
        var yScale = this.props.yScale;
        var color1 = this.props.color1;
        var color2 = this.props.color2;
        var getX = this.props.x || x;
        var getY = this.props.y || y;
        var getY0 = this.props.y0; 
        var opacity = this.props.opacity;

        var yFormat = this.props.yFormat;


        var names = this.props.names;
        var labels = this.props.labels || names;

        var bgLayer = [
          vLayer.render(BackgroundLayer, {
            xScale : xScale,
            yScale : yScale
          })
        ];

        var gridLayers = [
          vLayer.render(HGridLayer, {
            xScale: xScale,
            yScale: yScale
          }),
          vLayer.render(VGridLayer, {
            xScale: xScale,
            yScale: yScale
          })
        ];

        var lineLayers = data.map(function(series, i) {
          var color = color1(names[i]);
          return vLayer.render(LineLayer, {
            data: series,
            x: getX,
            y: getY,
            xScale: xScale,
            yScale: yScale,
            color: color,
            width: 1,
            name: names[i],
            key: names[i]
          });
        });

        var areaLayers = data.map(function(series, i) {
          var color = (color2 || color1)(names[i]);
          return vLayer.render(AreaLayer, {
            data: series,
            x: getX,
            y: getY,
            y0: getY0,
            xScale: xScale,
            yScale: yScale,
            color: color,
            opacity: opacity || 0.05,
            name: names[i],
            key: names[i]
          });
        });

        var i = highlights[0], j = highlights[1];
        var datum, ttx, tty, xmid, xrange, ymid, yrange, orient, tooltipLayer;

        var pointsLayers = data.map(function(series, k) {
          var color = (color2 || color1)(names[k]);
          var pointHL;
          if(getY0 ? ((k === i || k+1 === i) && j !== null) : (j !== null)) {
            if(series[j]) {
              pointHL = {
                time : getX(series[j]),
                index : j
              }; 
            }
          }
          return vLayer.render(PointsLayer, {
            data: series,
            x: getX,
            y: getY,
            xScale: xScale,
            yScale: yScale,
            color: color,
            name: names[k],
            key: names[k],
            pointHighlight : pointHL
          });
        });

        var lines;
        if(i !== null && j !== null) {
          datum = data[i][j];
          ttx = xScale( getX(datum) );
          tty = getY0 ? yScale( (getY0(datum) + getY(datum)) / 2 ) : (yScale.range()[0] + yScale.range()[1]) / 2;
          xrange = this.props.xScale.range(); 
          xmid = (xrange[0] + xrange[1]) / 2;
          yrange = this.props.yScale.range(); 
          ymid = (yrange[0] + yrange[1]) / 2;
          if(title) {
            lines = [
              [
                'Metric', 
                { 
                  text : title, 
                  bold : true
                },
                ''
              ],
              [
                '',
                'Date',
                defaultTimeFormat(getX(datum))
              ]
            ];
          } else {
            lines = [
              [
                'Date',
                defaultTimeFormat(getX(datum)),
                ''
              ]
            ];
          
          }
          lines = _.reduce(hlValues.concat([]).reverse(), function(lines, val, k, arr) { 
            lines.push([
              {
                text : '\u25C9',
                color : color2(names[arr.length - k -1])
              },
              {
                text : labels[arr.length - k -1],
                bold : i === arr.length - k - 1
              },
              yFormat(val)
            ]);
            return lines;
          }, lines);
          orient = [];
          if(Math.abs(tty - ymid) / Math.abs(yrange[0] - yrange[1]) < 0.15) {
          } else if(tty > ymid) {
            orient[0] = 'top';
          } else {
            orient[0] = 'bottom';
          }
          if(ttx > xmid) {
            orient.push('left');
          } else {
            orient.push('right');
          }
          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: ttx,
              y: tty,
              orient : orient,
              texts : lines
            })
          ];
        } else {
          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: 0,
              y: 0,
              hidden : true,
              texts : [['', '', '']],
              orient : ['left']
            })
          ];
        }

        var guidelineLayer;
        if(j !== null) {
          datum = data[i][j];
          guidelineLayer = [
            vLayer.render(GuidelineLayer, {
              x : ttx,
              y0 : i >=0 && getY0 ? yScale( getY0(datum) ) : null,
              y : i >=0 && getY0 ? yScale( getY(datum) ) : null,
              yScale : yScale
            })
          ];
        } else {
          guidelineLayer = [];
        }

        return bgLayer.concat(gridLayers, areaLayers, lineLayers, guidelineLayer, pointsLayers, tooltipLayer);

      }
    });    

  });
