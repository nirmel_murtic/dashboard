'use strict';

angular.module('fs-visulization')
  // just contants and a few helper functions
  // per Flux convention
  .constant('StackedChartConstants', {
    DEHIGHLIGHT : 'CLEAR_ALL_HIGHLIGHTS',
    HIGHLIGHT_XY : 'HIGHLIGHT_XY',
    MODE : 'MODE',
    STACK_NONE : 'STACK_NONE',
    STACK_BASE : 'STACK_BASE',
    STACK_ZERO : 'STACK_ZERO',
    STACK_NORMALIZE : 'STACK_NORMALIZE',
    FORCE_SCALE_DOMAIN : 'FORCE_SCALE_DOMAIN',
    DATA : 'DATA' 
  })
  .factory('StackedChartActions', function(StackedChartConstants) {
    return function actionsFactory(dispatcher) {
      return {
        forceTimerange : function(tRange) {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.FORCE_SCALE_DOMAIN, 
              scale : 'x',
              domain : tRange
            }
          });    
        },
        forceScaleDomain : function(scale, domain) {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.FORCE_SCALE_DOMAIN, 
              scale : scale,
              domain : domain
            }
          });    
        },
        clearStack : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.MODE,
              mode : StackedChartConstants.STACK_NONE,
            }
          });
        },
        zeroStack : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.MODE,
              mode : StackedChartConstants.STACK_ZERO,
            }
          });
        },
        baseStack : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.MODE,
              mode : StackedChartConstants.STACK_BASE,
            }
          });
        },
        normalizeStack : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.MODE,
              mode : StackedChartConstants.STACK_NORMALIZE,
            }
          });
        },
        highlight : function(pos) {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.HIGHLIGHT_XY,
              x : pos[0],
              y : pos[1]
            }
          });
        },
        clearHighlight : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : StackedChartConstants.DEHIGHLIGHT,
            }
          });
        }
      };
    };
  });
