'use strict';

angular.module('fs-visulization')
  .factory('StackedHighlightStore', function(invariant, accessor, pointInPolygon, fsUtils, binarySearchNearest, searchNearest, StackedChartConstants) {

    // jshint newcap: false, unused:false

    var Emitter = fsUtils.Emitter;
    var Constants = StackedChartConstants;

    return function stackedChartScaleStore(opts) {
      var dispatcher = opts.dispatcher;
      var scaleStore = opts.scaleStore;
      var dataStore = opts.dataStore;

      // states
      var row = null;
      var col = null;
      var values = null;


      function findHighlights(x, y) {
        var data= dataStore.getData();
        if(!data) {
          return false;
        }
        var i, j;
        var timestamps = dataStore.getTimestamps();
        var xScale = scaleStore.x();
        var yScale = scaleStore.y();
        var mode = dataStore.getMode();
        var accessors = dataStore.getAccessors();
        var getY = accessor('y');
        var getter;

        if(mode === Constants.STACK_NONE) {
          getter = accessors.y;
        } else {
          getter = accessors.y0;
        }
        
        x = xScale.invert(x);
        y = yScale.invert(y); 

        j = binarySearchNearest(x, timestamps);
        if(j === -1) { // time index not found
          return false;
        }
        var ys = data.map(function(s) {
          return getter(s[j]);
        });
        if(mode === Constants.STACK_NONE) {
          // ys will not be sorted
          i = searchNearest(y, ys);
        } else {
          // ys are already sorted
          i = binarySearchNearest(y, ys);
        }
        row = j;
        col = i;
        values = data.map(function(d) {
          return getY(d[j]);
        });

        return true;
      } 

      function clearHighlights() {
        row = null;
        col = null;
        values = null;
      }

      var store = Emitter({
        getHighlight : function() {
          return [col, row];
        },
        getHighlightedValues : function() {
          return values;
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken, scaleStore.dispatchToken ]);
        switch (action.type) {
          case Constants.HIGHLIGHT_XY:
            if( findHighlights(action.x, action.y) ) {
              break;
            }
            return;
          case Constants.DEHIGHLIGHT:
            clearHighlights(); 
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;
    };

  });
