'use strict';

angular.module('fs-visulization')
  .factory('StackedScaleStore', function(invariant, accessor, pointInPolygon, fsUtils, binarySearchNearest, StackedChartConstants, nicerScales) {
    // jshint newcap:false
    var Emitter = fsUtils.Emitter;
    var Constants = StackedChartConstants;
    var noDomain = [0,0];

    return function stackedChartScaleStore(opts) {
      var dispatcher = opts.dispatcher;
      var dataStore = opts.dataStore;
      var colorSets = opts.colorSets;
      var yrange = [opts.height, 0];

      var x = d3.time.scale().range([0, opts.width]);
      var y = d3.scale.linear().range(yrange);

      var xDomainForced = null, yDomainForced = null;

      var color1 = d3.scale.category20();
      var color2 = d3.scale.category20();
      var currentMode = dataStore.getMode();

      if(colorSets && colorSets.length) {
        color1.range(colorSets[0]);
        color2.range(colorSets[1] || colorSets[0]);
      }

      function switchMode(mode) {
        if(!mode) {
          return false;
        }
        if(mode === currentMode) {
          return false;
        }
        currentMode = mode;
        updateYScale();
        return true;
      }

      function updateYScale() {
        y = y.copy();
        if(!yDomainForced) {
          y.domain( dataStore.getYDomain() || noDomain );
          nicerScales.addMargins(y);
        } else {
          y.domain(yDomainForced);
        }

        nicerScales.patchDegenerate(y);
        y.nice();
      }
      function updateXScale() {
        x = x.copy();
        if (dataStore.getXDomain()[0] === dataStore.getXDomain()[1]){
          x.domain([d3.time.hour.offset(dataStore.getXDomain()[0], -3), d3.time.hour.offset(dataStore.getXDomain()[0], 3)]);
        } else {
          if(!xDomainForced) {
            x.domain( dataStore.getXDomain() || noDomain );        
          } else {
            x.domain(xDomainForced);
          }
        }
      }

      function onData() {
        color1.domain( dataStore.getNames() || noDomain );
        color2.domain( dataStore.getNames() || noDomain );
        updateXScale();
        updateYScale();
      }
      

      var store = Emitter({
        x : function() {
          return x;
        },
        y : function() {
          return y;
        },
        color1 : function() {
          return color1;
        },
        color2 : function() {
          return color2;
        },
        getMode : function() {
          return currentMode;
        }
      });  

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken ]);
        switch (action.type) {
          case Constants.FORCE_SCALE_DOMAIN:
            if(action.scale === 'x') {
              xDomainForced = action.domain;
              updateXScale();
            } else if(action.scale === 'y'){
              yDomainForced = action.domain;
              updateYScale();
            } else {
              return;
            }
            break;
          case Constants.DATA:
            onData();
            break;
          case Constants.MODE:
            if( switchMode(action.mode) ) {
              break;
            }
           return;
          case Constants.HIGHLIGHT_XY:
            break;
          case Constants.HIGHLIGHT_SERIES:
            break;
          case Constants.HIGHTLIGHT_POINT:
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;

    };

  });
