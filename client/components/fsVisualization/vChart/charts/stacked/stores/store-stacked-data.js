'use strict';

angular.module('fs-visulization')
  .config(function(DatasetProvider) {
    // jshint camelcase:false
    DatasetProvider.add('StackedDataset', {
      x : {
        type : 'datetime'
      },
      y : {
        type : 'float'
      },
      y_normalized : {
        type : 'float'
      },
      y0_normalized : {
        type : 'float'
      },
      y0_base : {
        type : 'float'
      },
      y0_zero : {
        type : 'float'
      }
    });
  })
  .factory('StackedDataStore', function(invariant, accessor, pointInPolygon, fsUtils, binarySearchNearest, StackedChartConstants, dataExtent, Dataset) {
    // jshint camelcase:false, newcap:false
    var Emitter = fsUtils.Emitter;
    var Constants = StackedChartConstants;

    var projectX = accessor('x');
    var projectY = {};
    var projectY0 = {};

    projectY[Constants.STACK_NONE] = accessor('y');
    projectY[Constants.STACK_ZERO] = accessor('y');
    projectY[Constants.STACK_BASE] = accessor('y');
    projectY[Constants.STACK_NORMALIZE] = accessor('y_normalized');

    projectY0[Constants.STACK_ZERO] = accessor('y0_zero');
    projectY0[Constants.STACK_BASE] = accessor('y0_base');
    projectY0[Constants.STACK_NORMALIZE] = accessor('y0_normalized');
    // stacked layouts helper functions for three stacked modes
    var stackZero = d3.layout.stack()
      .offset('zero')
      .x(projectX)
      .y(projectY[Constants.STACK_NONE])
      .out(function(d, y0) {
        d.y0_zero = y0;
      });
    var stackNormalize = d3.layout.stack()
      .offset('expand')
      .x(projectX)
      .y(projectY[Constants.STACK_NONE])
      .out(function(d, y0, y) {
        d.y0_normalized = y0;
        d.y_normalized = y;
      });
    // jshint unused:false
    var stackBase = d3.layout.stack()
      .offset('zero') // TO-DO: config this
      .x(projectX)
      .y(projectY[Constants.STACK_NONE])
      .out(function(d, y0) {
        d.y0_base = y0;
      });


    return function stackedDataStoreFactory(opts) {
      var dispatcher = opts.dispatcher;
      var currentMode = opts.mode || Constants.STACK_NONE;
    
      var DatasetClass = Dataset.getPreset('StackedDataset');

      var data = null;
      var names = [];
      var dataGeneration = 0;
      var timestamps = null;


      function loadData(dataArray, seriesNames) {
        /*jshint -W030 */
        dataGeneration++;
        stackZero(dataArray);
        // stackBase(dataArray);
        stackNormalize(dataArray);
        data = _.map(dataArray, function(d) {
          return new DatasetClass(d);
        });
        Object.freeze && Object.freeze(data);
        if(!data.length) {
          return;
        }
        timestamps = _.map(data[0].data, function(d) {
          return d.x;
        });
        names = (seriesNames && seriesNames.length === data.length) ? seriesNames : _.range(data.length).map(function(i) {
          return 'Series ' + (i+1);
        });
      }

      var getYDomainForMode = _.memoize(function (mode) {
        if(!mode || !data || !data.length) {
          return [0,0];
        }
        var domains, top, bottom, ymin, ymax, projY, projY0;
        if(mode === Constants.STACK_NORMALIZE) {
          return [0, 1];
        } else if(mode !== Constants.STACK_NONE) {
          projY = projectY[mode];
          projY0 = projectY0[mode];

          bottom = data[0].data;
          top = data[data.length - 1].data;

          ymin = d3.min(bottom, projY0);
          ymax = d3.max(top, function(d) {
            return projY(d) + projY0(d); 
          });
          return [ymin, ymax];
        } else {
          domains = _.map(data, function(d) {
            return d.domains.y;
          });
          return d3.extent(_.flatten(domains));
        }
      }, function (mode) {
        if(dataGeneration !== getYDomainForMode.generation) {
          getYDomainForMode.cache.__data__ = {}; // clear cache
        }
        getYDomainForMode.generation = dataGeneration;
        return mode;
      });

      function getXDomain() {
        if(!data || !data.length) {
          return [];
        }
        return data[0].domains.x;
      }

      function switchMode(mode) {
        if(mode === currentMode) {
          return false;
        }
        currentMode = mode;
        return true;
      }
     
      var accessorsForMode = _.memoize(function(mode) {
        if(!mode) {
          return null;
        }
        var projY = projectY[mode];
        var projY0 = projectY0[mode];
        return {
          x : projectX,
          y : projY,
          y0 : projY0,
          ysum : function(d) {
            return projY(d) + projY0(d);
          }
        };
      });

      var store = Emitter({
        getMode : function() {
          return currentMode;
        },
        getTimestamps : function() {
          return timestamps;
        },
        getNames : function() {
          if(!data) {
            return null;
          }
          return names;
        },
        getData : function() {
          if(!data) {
            return null;
          }
          return data.map(function(d) {
            return d.data;
          });
        },
        getXDomain : getXDomain,
        getYDomain : function() {
          if(!data) {
            return null;
          }
          return getYDomainForMode(currentMode);
        },
        getAccessors : function() {
          if(!data) {
            return null;
          }
          return accessorsForMode(currentMode);
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        switch (action.type) {
          case Constants.DATA:
            loadData(action.data, action.labels);
            break;
          case Constants.MODE:
            if(!action.mode) {
              return;
            }
            if( switchMode(action.mode) ) {
              break;
            }
           return;
          default:
            return; 
        }
        store.emit('change');
      }); 

      // handle payload

      return store;
    };

  });
