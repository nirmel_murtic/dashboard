'use strict';

angular.module('fs-visulization')
  .directive('fsVisStacked', function(
    $parse,
    zipWith, 
    transposeObject, 
    Dispatcher, 
    makeChart, 
    chartScaffolding, 
    StackedChart, 
    StackedChartConstants, 
    accessor, 
    StackedDataStore, 
    StackedScaleStore, 
    StackedChartActions, 
    StackedHighlightStore, 
    BatchRender, 
    eliminateDuplicateTicks) {
    // jshint newcap:false
    return {
      restrict : 'EA',
      require : 'fsVisConfig',
      priority : 10,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>'
                 ].join(''), 
      transclude : true,
      link : function(scope, element, attrs, configCtrl, transclude) {

        var timeRange = $parse(attrs.timeRange || '');
        scope.timeRange = timeRange.bind(null, scope);

        var dataGetter = $parse(attrs.data);

        var getMode = attrs.stackMode ? $parse(attrs.stackMode).bind(null, scope) : function(){
          return StackedChartConstants.STACK_NONE;
        };

        scope.data = function() {
          return dataGetter(scope);
        };

        var containerWidth = 
          parseInt(
            window.getComputedStyle(element.parent()[0], null).getPropertyValue('width'), 
          10);

        if (!containerWidth || containerWidth < 360) {
          containerWidth = 900;
        }

        var aspectRatio;

        if (containerWidth > 117 * 16) {
          aspectRatio = 3;
        } else if (containerWidth > 75 * 16) {
          aspectRatio = 5 / 2;
        } else if (containerWidth > 60 * 16) {
          aspectRatio = 16 / 9;
        } else {
          aspectRatio = 4 / 3;
        }

        var config = configCtrl.$mergeDefaults({
          timeField : 'time',
          valueFields : null,
          labels : null,
          width : containerWidth,
          aspectRatio : aspectRatio,
          height: 900,
          margin : {
            top : 25,
            bottom : 60,
            left : 55,
            right: 55 
          },
          xAxisOffset : {
            top: 25,
            left : 25
          },
          xFormat : null,
          yFormat : null,
          colors : null,
          secondaryColors : null
        });
        config.opacity = $parse(attrs.opacity)(scope);

        // prepare mapping from raw data fileds to uniformed property identifiers (x & y)
        // which the dataStore expects
        var title = config.chartTitle;
        var seriesLabels = config.labels;
        var timeField = config.timeField;
        var valueFields = config.valueFields;
        var mapName = {};
        mapName[config.timeField] = 'x';  // label time field/column x
        if(_.isArray(config.valueFields) && config.valueFields.length) {
          _.reduce(config.valueFields, function(m, field) {
            m[field] = 'y';
            return m;
          }, mapName);
        } else if (valueFields) {
          mapName[valueFields] = 'y';
        } else {
          mapName['*'] = 'y';
        }

        // DOM operations to make svg display in specified aspectRatio and make it responsive
        config.height = config.width / config.aspectRatio;
        var svg = element.find('svg');
        svg.parent().attr('style', 'padding-bottom: ' + 1/config.aspectRatio * 100 + '%');

        // init dispatcher, central hub for messaging and view actions within the scope of this chart
        var dispatcher = new Dispatcher();
        // init action creator, which is a helper object for dispatching view actions
        var actionCreator = StackedChartActions(dispatcher);
      
        // based on chart dimensions from config, create various  containers inside <svg> (these are <g> tags)
        var containers = chartScaffolding(svg[0], config.width, config.height, config.margin, config.xAxisOffset); 
        // init a store for data
        var dataStore = StackedDataStore({
          dispatcher : dispatcher,
          mode : getMode()
        });

        // init a store for scales, seperate view states (scales, colors) from data (which is manageed in dataStore)
        var colors = config.colors;
        var colors2 = config.secondaryColors;
        var colorSets = null;
        if(colors) {
          colorSets = [colors];
          if(colors2) {
            colorSets.push(colors2);
          }
        }
        var scaleStore = StackedScaleStore({
          dispatcher : dispatcher,
          dataStore : dataStore,
          width : containers.chartSize.width,
          height : containers.chartSize.height,
          colorSets : colorSets
        });
        // init a store for view states / highlights, that handles actions created on mousemove and such
        var highlightStore = StackedHighlightStore({
          dispatcher : dispatcher,
          dataStore : dataStore,
          scaleStore : scaleStore
        });

        // bind these things on directive's isolated scope, so angular can use them as well
        scope.actionCreator = actionCreator;
        scope.dataStore = dataStore;
        scope.scaleStore = scaleStore;
        scope.legendData = [];

        // once scales store are setuop, load scales into d3 svg axis
        var xAxis = d3.svg.axis().scale(scaleStore.x()).orient('bottom');
        var yAxis = d3.svg.axis().scale(scaleStore.y()).orient('left');

        if(config.xFormat) {
          xAxis.tickFormat(config.xFormat);
        }
        if(config.yFormat) {
          yAxis.tickFormat(config.yFormat);
        }

        // create a chart managed by virtual chart on specified container containers.chart
        // which at this point is an empty <g> tag positioned right (margins and such taken care of)
        var renderChart = makeChart(containers.chart.node(), {
          transition: configCtrl.transition 
        }); 

        var formatPercentage = d3.format('.2%');

        // timer id indicating whether there's a pending render or not initiated by requestAnimationFrame
        var batchRenderer = new BatchRender(function render() {
          // get scales from store
          var x = scaleStore.x();
          var y = scaleStore.y();
          // accessors are getter functions that extract values from datum
          var accessors = dataStore.getAccessors();
          // non-stacked, stacked zero, stacked base, stacked normalize
          var mode = dataStore.getMode();
          // the data!
          var data = dataStore.getData();
          if(!data) {
            return;
          }
          // update d3 axis with scales in case they change
          xAxis.scale(x);
          yAxis.scale(y);
          if(config.xFormat) {
            xAxis.tickFormat(config.xFormat);
          }
          var yTicks;
          yAxis.tickValues(null);
          if(config.yFormat && mode !== StackedChartConstants.STACK_NORMALIZE) {
            yTicks = eliminateDuplicateTicks(y, config.yFormat);
            if(yTicks !== false) {
              yAxis.tickValues(yTicks);
            }
            yAxis.tickFormat(config.yFormat);
          } else if (mode === StackedChartConstants.STACK_NORMALIZE) {
            yAxis.tickFormat(formatPercentage);
          }
          // dom operations here, managed by d3
          containers.xAxis.call(xAxis);
          containers.yAxis.call(yAxis);
          // use vChart engine to do vChart diff, and apply patches to real DOM if anything changed
          renderChart(StackedChart, {
            data : data,
            title : title,
            xScale : x,
            yScale : y,
            yFormat : config.yFormat || String,
            labels : seriesLabels,
            x : accessors.x,
            y : (mode === StackedChartConstants.STACK_NONE ? accessors.y : accessors.ysum),
            y0 : (mode === StackedChartConstants.STACK_NONE ? null : accessors.y0),
            names : dataStore.getNames(),
            color1 : scaleStore.color1(),
            color2 : scaleStore.color2(),
            highlights : highlightStore.getHighlight(),
            highlightedValues : highlightStore.getHighlightedValues(),
            opacity : config.opacity
          });
        });


        // if data changes, rerender
        dataStore.on('change', batchRenderer.render);
        // if scales changes, rerender
        scaleStore.on('change', batchRenderer.render);
        // if highlights changes, rerender
        highlightStore.on('change', batchRenderer.render);
        // if scales change, inform angular about new legend data
        scaleStore.on('change', function() {
          scope.legendData = zipWith(dataStore.getNames(), scaleStore.color1().range(), scaleStore.color2().range(), function(name, color1, color2) {
            return {
              name : name,
              color: color2,
              colorSecondary : color1
            };
          }); 
        });

        // throttle mouse move at ~30 fps
        var mouseMoveCallback = _.throttle(function(coords) {
          // dispatch a highlight action for given mouse coordinates
          actionCreator.highlight(coords);
        }, 33);
        containers.chart.on('mousemove', function() {
          var coords = d3.mouse(this);
          mouseMoveCallback(coords);
        });
        svg.on('mouseleave', function() {
          actionCreator.clearHighlight();
        });

        // something from angular side changed data, dispatch the DATA action
        scope.$watchGroup([dataGetter, configCtrl.timeField, configCtrl.valueFields, configCtrl.formatX, configCtrl.yFormat, configCtrl.labels, configCtrl.chartTitle], function(values) {

          var newData = values[0];

          config.timeField = values[1];
          config.valueFields = values[2];
          config.xFormat = values[3];
          config.yFormat = values[4];
          seriesLabels = values[5];
          title =  values[6];

          if(!newData) {
            return;
          }

          var labels = seriesLabels || _.filter(_.keys(newData[0]), function(key) {
            return key !== timeField;
          });
          mapName = {};
          mapName[config.timeField] = 'x';  // label time field/column x
          if(_.isArray(config.valueFields) && config.valueFields.length) {
            _.reduce(config.valueFields, function(m, field) {
              m[field] = 'y';
              return m;
            }, mapName);
          } else if (valueFields) {
            mapName[valueFields] = 'y';
          } else {
            mapName['*'] = 'y';
          }

          newData = transposeObject(newData, {
            fixture : timeField,
            mapName : mapName
          });


          dispatcher.dispatch({
            source : 'external',
            action : {
              type : StackedChartConstants.DATA,
              data : newData || [],
              labels : labels
            }
          });
          var range = scope.timeRange();
          if(range) {
            actionCreator.forceTimerange(range);
          }
        });

 
        // put directives contents into transcluded elements
        // specify attribute 'append-after' if you want the html to display **after** the chart/svg
        transclude(scope, function(tcEl) {
          var len = tcEl.length;
          var before = element.find('.chart-control-before')[0];
          var after = element.find('.chart-control-after')[0];
          var container;
          for(var i=0; i<len; i++) {
            if(tcEl[i] && tcEl[i].hasAttribute && tcEl[i].hasAttribute('append-after')) {
              container = after;
            } else {
              container = before;
            }
            container.appendChild(tcEl[i]);
          }
        });
       
      }
    };
  });
