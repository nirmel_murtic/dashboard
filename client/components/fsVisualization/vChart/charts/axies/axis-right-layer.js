'use strict';

angular.module('fs-visulization')
  .factory('AxisRightLayer', function(vLayer, VNode) {

    return vLayer.define({
      defaults : {
        color : '#000'
      },
      render : function() {
        var tickSize = this.props.tickSize || 6,
           tickPadding = this.props.tickPadding || 3,
           scale = this.props.scale,
           range = scale.range(),
           // domain = scale.domain(),
           color = this.props.color || this.defaults.color,
           ticks = this.props.tickValues || scale.ticks(),
           tickFormat = this.props.tickFormat;
       
        var yBottom = range[0] > range[1] ? range[0] : range[1];
    
        if(!tickFormat) {
          tickFormat = scale.tickFormat( ticks.length );
        }

        var axisLine = new VNode('path', {
          d : 'M' + [tickSize, 0] + 'H0V' + yBottom + 'L' + [tickSize, yBottom],
          style : {
            stroke : color
          },
          className : 'domain'
        }, null);

        var tickLines = _.reduce(ticks, function(nodes, t) {
          var y = scale(t); 
          var node;
          var noTick = false;

          node = new VNode('g', {
            className : 'tick' + (noTick ? ' hidden' : '')
          }, [
            new VNode('line', {
              x1 : tickSize,
              y1 : y,
              x2 : 0,
              style : {
                stroke : color
              },
              y2 : y
            }, null),
            new VNode('text', {
              contents : tickFormat(t),
              'text-anchor' : 'start',
              y : y,
              dy: '.32em',
              style : {
                fill : color
              },
              dx : tickPadding + tickSize
            }, null)
          ]);
          nodes.push(node);
          return nodes;
        }, [axisLine]);

        return new VNode('g', {
          className : 'axis'
        }, tickLines);
      }
    });

  });
