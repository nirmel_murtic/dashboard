'use strict';

angular.module('fs-visulization')
  .factory('AxisBottomLayer', function(vLayer, VNode, $filter) {

    return vLayer.define({
      defaults : {
        color : '#000'
      },
      render : function() {
        var tickSize = this.props.tickSize || 6,
           tickPadding = this.props.tickPadding || 3,
           scale = this.props.scale,
           range = scale.range(),
           // domain = scale.domain(),
           ticks = scale.ticks(),
           color = this.props.color || this.defaults.color,
           tickFormat = this.props.tickFormat;
       
        var xRight = range[0] > range[1] ? range[0] : range[1];
    
        if(!tickFormat) {
          tickFormat = scale.tickFormat( ticks.length );
        }

        var firstT = scale.domain()[0],
            lastT = scale.domain()[1];

        var tickDist;
        if(ticks.length) {
          tickDist = ticks.length > 1 ? Math.abs(ticks[1] - ticks[0]) : Infinity;
          if(ticks[0] - firstT > tickDist / 2) {
            ticks.unshift(firstT);
          } else {
            ticks.shift();
            ticks.unshift(firstT);
          }
          if(lastT - ticks[ticks.length-1] > tickDist / 2) {
            ticks.push(lastT);
          } else {
            ticks.pop();
            ticks.push(lastT);
          }
        }
        


        var axisLine = new VNode('path', {
          d : 'M' + [0, tickSize] + 'V0H' + xRight + 'V' + (tickSize),
          stroke : color,
          className : 'domain'
        }, null);

        var tickLines = _.reduce(ticks, function(nodes, t, j) {
          var x = scale(t); 
          var node;
          var noTick = false;

          node = new VNode('g', {
            className : 'tick' + (noTick ? ' hidden' : '')
          }, [
            new VNode('line', {
              x1 : x,
              y1 : 0,
              x2 : x,
              stroke : color,
              className : (j === 0 || j === ticks.length) ? 'hidden' : '', 
              y2 : tickSize
            }, null),
            new VNode('text', {
              contents : (t===firstT || t === lastT) ? $filter('metricDate')(t)  :tickFormat(t),
              'text-anchor' : 'middle',
              x: x,
              dy: '1em',
              fill : color,
              y : tickPadding + tickSize
            }, null)
          ]);
          nodes.push(node);
          return nodes;
        }, [axisLine]);

        return new VNode('g', {
          className : 'axis'
        }, tickLines);
      }
    });

  });

