'use strict';

angular.module('fs-visulization')
  .factory('BrokenAxisLeftLayer', function(vLayer, VNode) {

    return vLayer.define({
      defaults : {
        color : '#000'
      },
      render : function() {
        var tickSize = this.props.tickSize || 6,
           tickPadding = this.props.tickPadding || 3,
           scale = this.props.scale,
           range = scale.range(),
           color = this.props.color || this.defaults.color,
           domain = scale.domain(),

           ticks = scale.ticks(this.props.ticks),
           tickFormat = this.props.tickFormat,
           breakHeight = this.props.breakHeight,
           breakWidth = this.props.breakWidth;
       
        var yBottom = range[0] > range[2] ? range[0] : range[2];
        var yBreak = breakHeight/7;
        var xBreak = breakWidth/2;

        if(!tickFormat) {
          tickFormat = scale.tickFormat( ticks.length );
        }


        var axisLine = new VNode('path', {
          d : 'M' + [-tickSize, 0] + 'H0V' + range[1] +
              'l' + [0,2*yBreak] + 'l' + [-xBreak,yBreak] + 'l' + [2*xBreak,yBreak] + 
              'l' + [-xBreak,yBreak] + 'l' + [0,2*yBreak] + 'L' + [-tickSize, yBottom],
          stroke : color,
          className : 'domain'
        }, null);

        

        var zeroTick = new VNode('g', { className : 'tick' }, [new VNode('text', {
            contents : tickFormat(domain[0]),
            'text-anchor' : 'end',
            y : range[0],
            dy: '.32em',
            fill : color,
            dx : -tickPadding - tickSize
          })
        ]);
        var minTick = new VNode('g', { className : 'tick' }, [new VNode('text', {
            contents : tickFormat(domain[1]),
            'text-anchor' : 'end',
            y : range[1],
            dy: '.32em',
            fill : color,
            dx : -tickPadding - tickSize
          }), new VNode('path',{
            d : 'M' + [-tickSize, range[1]] + 'L' + [0, range[1]],
            stroke : color,
            className : 'domain'
          })
        ]);

        /*
        var maxTick = new VNode('g', { className : 'tick' }, [new VNode('text', {
            contents : tickFormat(domain[2]),
            'text-anchor' : 'end',
            y : range[2],
            dy: '.32em',
            fill : color,
            dx : -tickPadding - tickSize
          })
        ]);
        */

        var tickLines = _.reduce(ticks, function(nodes, t) {
          var y = scale(t); 
          var node;
          var noTick = false;

          /*
          if(i === 0 && range[0] - y < 8) {
            noTick = true;
          }
          if(i === ticks.length - 1 && y - range[1] < 8) {
            noTick = true;
          }
          */
          node = new VNode('g', {
            className : 'tick' + (noTick ? ' hidden' : '')
          }, [
            new VNode('line', {
              x1 : -tickSize,
              y1 : y,
              x2 : 0,
              y2 : y,
              stroke : color
            }, null),
            new VNode('text', {
              contents : tickFormat(t),
              'text-anchor' : 'end',
              y : y,
              dy: '.32em',
              fill : color,
              dx : -tickPadding - tickSize
            }, null)
          ]);
          nodes.push(node);
          return nodes;
        }, [axisLine, minTick, zeroTick]);

        return new VNode('g', {
          className : 'axis'
        }, tickLines);
      }
    });

  });
