'use strict';

angular.module('fs-visulization')
  .constant('fsVisCommonConfig', {
    width : null,
    aspectRatio : null,
    chartTitle : '',
    formatX : null,
    xFormat : null,
    yFormat : null,
    y1Format : null,
    margin: null,
    xAxisOffset : null,
    timeField : null,
    valueFields : null,
    labels : null,
    transition : true,
    // tooltipsFields : null,
    // tooltipsLabels : null,
    colors : null,
    secondaryColors : null,
    description : null
  }) 
  .directive('fsVisConfig', function($parse, fsVisCommonConfig) {
    return {
      restrict : 'A',
      priority : 0,
      scope : true,
      controller : function() {
        this.$collect = function() {
          var config = {};
          for(var key in this) {
            if(!_.startsWith(key, '$')) {
              config[key] = this[key]();
            }
          }

          return config;
        };
        this.$watchables = function() {
          return _.reduct(this, function(arr, watchable, key) {
            if(!_.startsWith(key, '$')) {
              arr.push(watchable);
            }
            return arr;
          }, []);
        };

        this.$mergeDefaults = function(defaultConfig) {
          var config = this.$collect();
          for(var key in defaultConfig) {
            config[key] = config[key] || defaultConfig[key];
          }
          return config;
        };
      },
      link : function(scope, element, attrs, controller) {
        var key;

        for(key in fsVisCommonConfig) {
          if(attrs[key]) {
            // this creates a '&' binding on the controller for given config object key
            controller[key] = $parse(attrs[key]).bind(null, scope);
          } else {
            // if no config is passed to the directive set its default value
            controller[key] = d3.functor(fsVisCommonConfig[key] || null);
          }
        }
 
      }
    };
  });
