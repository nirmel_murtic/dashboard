'use strict';

angular.module('fs-visulization')
  .factory('VHistBarLayer', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        var data = this.props.data;
        var xScale = this.props.xScale,
          yScale = this.props.yScale,
          color = this.props.color || '#b4b4b4';

        return new VNode('g', null, data.map(function(d) {
       
          var x = xScale(d.y);
          var y1 = yScale(d.x);
          var y2 = yScale(d.x + d.dx);
          var ymin = Math.min(y1, y2);
          var ymax = Math.max(y1, y2);

          return new VNode('rect', {
            className : 'bar',
            x : 0,
            y : ymin+2,
            fill : color,
            width : x, 
            height : ymax - ymin - 2
          });
        }));
      }
    });
  });
