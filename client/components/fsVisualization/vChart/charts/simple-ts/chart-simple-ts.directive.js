'use strict';

angular.module('fs-visulization')
  .directive('fsVisSimpleTs', function(
    $parse, 
    DoubleAxisY1,
    XAxis,
    SimpleTsChart,
    processSimpleTsData,
    zipWith, 
    accessor, 
    makeChart, 
    pointInPolygon,
    chartScaffolding,
    ColorDefs,
    BatchRender) {
    // jshint -W026, latedef: false, newcap: false
    return {
      restrict : 'EA',
      priority : 10,
      require : 'fsVisConfig',
      scope : false,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>',
                 ].join(''), 
      transclude: true,
      link : function(scope, element, attrs, configCtrl, transclude) {
        var dataGetter = $parse(attrs.data);
        scope.data = function() {
          return dataGetter(scope);
        };

        // get configuration
        var containerWidth = 
          parseInt(
            window.getComputedStyle(element.parent()[0], null).getPropertyValue('width'), 
          10);

        if (!containerWidth || containerWidth < 360) {
          containerWidth = 900;
        }

        var aspectRatio;

        if (containerWidth > 117 * 16) {
          aspectRatio = 9 / 2;
        } else if (containerWidth > 75 * 16) {
          aspectRatio = 7 / 2;
        } else if (containerWidth > 60 * 16) {
          aspectRatio = 5 / 2;
        } else {
          aspectRatio = 16 / 9;
        }

        var config = configCtrl.$mergeDefaults({
          timeField : 'time',
          valueFields : null,
          labels : null,
          width : containerWidth,
          aspectRatio : aspectRatio,
          margin : {
            top : 25,
            bottom : 35,
            left : 35,
            right: 15 
          },
          xAxisOffset : {
            top: 5,
            left : 5
          },
          formatX : null,
          yFormat : null,
          colors : null
        });

        // scaffolding
        var svg = element.find('svg');
        svg.parent().attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
        var containers = chartScaffolding(svg[0], config.width, config.width / aspectRatio, config.margin, config.xAxisOffset); 

        var renderChart = makeChart(containers.chart.node());
        var renderY = makeChart(containers.yAxis.node());
        var renderX = makeChart(containers.xAxis.node());

        var batchRenderer = new BatchRender(function() {
          if(!state) {
            return;
          }
          renderY(DoubleAxisY1, { yScale : state.yScale, ticks : 4 });
          renderX(XAxis, { xScale : state.xScale });
          renderChart(SimpleTsChart, {
            title : config.title,
            data : state.data,
            bins : state.freqData,
            median : state.median,
            xScale : state.xScale,
            xScaleHist : state.xScaleHist,
            yScale : state.yScale,
            color : ColorDefs.LeafHeavy,
            highlight : state.highlight
          }); 
        });

        var state;
        var constructVoronoi = d3.geom.voronoi()
          .x(function(d) { return state.xScale(d.x); }) 
          .y(function(d) { return state.yScale(d.y); })
          .clipExtent([[0, 0], [containers.chartSize.width, containers.chartSize.height]]);
        var voronois;

        // throttle mouse move at ~30 fps
        var mouseMoveCallback = _.throttle(function(coords) {
          var i;
          if(voronois && state) {
            for(i=0; i<voronois.length; i++) {
              if(voronois[i] && pointInPolygon(coords, voronois[i])) {
                break; 
              }
            }
            if(i < voronois.length) {
              state.highlight = {
                time : state.data[i].x,
                value : state.data[i].y
              }; 
              batchRenderer.render();
            } else {
              state.hightlight = {};
              batchRenderer.render();
            }
          }  
        }, 33);
        containers.chart.on('mousemove', function() {
          var coords = d3.mouse(this);
          mouseMoveCallback(coords);
        });
        svg.on('mouseleave', function() {
          if(state) {
            state.highlight = {};
            batchRenderer.render();
          }
        });

        scope.$watchGroup([
            'data()', configCtrl.chartTitle, configCtrl.formatX, configCtrl.yFormat
        ], 
        function(values) {
          if(!values || !values[1]) {
            return;
          }
          config.title = values[1] || config.title || '';
          state = processSimpleTsData(values[0], config.timeField, _.isArray(config.valueFields) ? config.valueFields[0] : config.valueFields);
          state.xScale.range([0, containers.chartSize.width]);
          state.yScale.range([containers.chartSize.height, 0]);
          voronois = constructVoronoi(state.data);
          state.xScaleHist.range([0, containers.chartSize.width / 2]);
          batchRenderer.render();
        });
        // put directives contents into transcluded elements
        // specify attribute 'append-after' if you want the html to display **after** the chart/svg
        transclude(scope, function(tcEl) {
          var len = tcEl.length;
          var before = element.find('.chart-control-before')[0];
          var after = element.find('.chart-control-after')[0];
          var container;
          for(var i=0; i<len; i++) {
            if(tcEl[i] && tcEl[i].hasAttribute && tcEl[i].hasAttribute('append-after')) {
              container = after;
            } else {
              container = before;
            }
            container.appendChild(tcEl[i]);
          }
        });

      }
    };
  });
