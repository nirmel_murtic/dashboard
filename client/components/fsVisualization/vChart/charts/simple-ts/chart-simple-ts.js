'use strict';

angular.module('fs-visulization')
  .factory('VHistBarLayer', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        var data = this.props.data;
        var xScale = this.props.xScale,
          yScale = this.props.yScale,
          color = this.props.color || '#b4b4b4';

        return new VNode('g', null, data.map(function(d) {
       
          var x = xScale(d.y);
          var y1 = yScale(d.x);
          var y2 = yScale(d.x + d.dx);
          var ymin = Math.min(y1, y2);
          var ymax = Math.max(y1, y2);

          return new VNode('rect', {
            className : 'bar',
            x : 0,
            y : ymin+2,
            fill : color,
            width : x + 2, 
            height : ymax - ymin - 2
          });
        }));
      }
    });
  })
  .factory('SimpleTsChart', function(accessor, vChart, vLayer, VHistBarLayer, PointsLayer, LineLayer, AreaLayer, VGridLayer, HGridLayer, TooltipLayerAdvanced, GuidelineLayerHorizontal, GuidelineLayer, BackgroundLayer, ColorDefs) {

    // var defaultTimeFormat = d3.time.format('%m/%d/%y %I:%M %p');
    var defaultDateFormat = d3.time.format('%m/%d/%y');

    var x = accessor('x');
    var y = accessor('y');

    return vChart.define({

      // MultiSeriesChart's render function
      render: function() {

        var data = this.props.data;
        var bins = this.props.bins;
        var median = this.props.median;
        var xScale = this.props.xScale;
        var yScale = this.props.yScale;
        var xScaleHist = this.props.xScaleHist;
        var color = this.props.color;
        var getX = this.props.x || x;
        var getY = this.props.y || y;

        var yFormat = this.props.yFormat;
        var xFormat = this.props.xFormat;

        var title = this.props.title;

        var bgLayer = [
          vLayer.render(BackgroundLayer, {
            xScale : xScale,
            yScale : yScale
          })
        ];

        var gridLayers = [
          vLayer.render(HGridLayer, {
            xScale: xScale,
            yScale: yScale
          }),
          vLayer.render(VGridLayer, {
            xScale: xScale,
            yScale: yScale
          })
        ];

        var histLayer = [];
        if(xScaleHist && bins) {
          histLayer.push(
            vLayer.render(VHistBarLayer, {
              xScale : xScaleHist,
              yScale : yScale,
              data : bins,
              color : ColorDefs.PaleDark
            })
          );
        }

        var lineLayer = [
          vLayer.render(LineLayer, {
            data: data,
            x: getX,
            y: getY,
            xScale: xScale,
            yScale: yScale,
            color: color,
            width: data.length > 100 ? 1 : 2
          }),
          vLayer.render(LineLayer, {
            data: median,
            x: getX,
            y: getY,
            xScale: xScale,
            yScale: yScale,
            color: color,
            width: 1
          })
        ];

        var areaLayer = [
          vLayer.render(AreaLayer, {
            data: data,
            x: getX,
            y: getY,
            xScale: xScale,
            yScale: yScale,
            color: d3.rgb(color).brighter().brighter(),
            opacity: 0.25,
          })
        ];

        var highlight = this.props.highlight;       
        var time = highlight.time;
        var value = highlight.value; 
        var texts;
        var xrange, xmid, ttx, tty, tooltipLayer;
        var yrange, ymid;

        var pointsLayers = [
          vLayer.render(PointsLayer, {
            data: data,
            xScale: xScale,
            yScale: yScale,
            color: color,
            x : x,
            y : y,
            pointHighlight : time ? { time : time } : null
          })
        ];

        if(time && !_.isUndefined(value)) {
          ttx = xScale( time );
          tty = yScale( value );

          xrange = this.props.xScale.range(); 
          xmid = (xrange[0] + xrange[1]) / 2;

          yrange = this.props.yScale.range(); 
          ymid = (yrange[0] + yrange[1]) / 2;

          texts = [
            [
              {
                text : 'Date',
                bold : true
              }, 
              xFormat ? xFormat(time) : defaultDateFormat(time),
            ],
            [
              title,
              yFormat ? yFormat(value) : String(value)
            ],
            [
              {
                text : 'Median',
                italic : true
              },
              {
                text : yFormat ? yFormat(median[0].y) : String(median[0].y),
                italic : true
              }
            ]
          ];

          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: ttx,
              y: tty,
              orient : [tty > ymid ? 'top' : 'bottom', ttx > xmid ? 'left' : 'right'],
              texts : texts
            })
          ];
        } else {
          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: 0,
              y: 0,
              texts : [['', '']],
              hidden : true,
              orient : ['left']
            })
          ];
        }

        var guidelineLayer;
        if(time && !_.isUndefined(value)) {
          guidelineLayer = [
            vLayer.render(GuidelineLayer, {
              x : ttx,
              y : tty,
              yScale : yScale
            }),
            vLayer.render(GuidelineLayerHorizontal, {
              x : ttx,
              x0 : -this.props.nudgeX || 0,
              y : tty,
              xScale : xScale
            })
          ];
        } else {
          guidelineLayer = [];
        }
        return bgLayer.concat(gridLayers, histLayer, areaLayer, lineLayer, guidelineLayer, pointsLayers, tooltipLayer);

      }
    });    

  });
