'use strict';

angular.module('fs-visulization') 
  .factory('processSimpleTsData', function(expandInterval) { 
    return function(inputs, timeField, valueField) {
      var data = _.map(inputs, function(d) {
        return {
          x : d[timeField],
          y : d[valueField]
        };
      });
      var median = d3.median(data, function(d) {
        return d.y;
      });

      var x = d3.time.scale();
      var y = d3.scale.linear();
      var yDomain;
      if(data.length === 1){
        x.domain([d3.time.day.offset(data[0].x, -1), d3.time.day.offset(data[0].x, 1)]);
        yDomain = [0, data[0].y];
      }
      else {
        x.domain(d3.extent(data, function(d) {
          return d.x;
        }));

        yDomain = d3.extent(data, function(d) {
          return d.y;
        });
      }

      if(0 < yDomain[0] && yDomain[0] * 2 < yDomain[1] - yDomain[0]) {
        yDomain[0] = 0;
        yDomain = expandInterval(yDomain, 0.5, 0); 
      } else {
        yDomain = expandInterval(yDomain, 0.75, 0); 
      }

      y.domain(yDomain).nice().ticks(4);

      var hist = d3.layout.histogram()
        .bins(8)
        .range(y.domain())
        .value(function(d) {
          return d.y;
        });

      var freqData = hist(data);

      var xHist = d3.scale.linear().domain([0, d3.max(freqData, function(d) { return d.y; }) + 1]);

      return {
        data : data,
        freqData : freqData,
        median : _.range(data.length).map(function(i) {
          return {
            x : data[i].x,
            y : median
          };
        }),
        xScale : x,
        yScale : y,
        xScaleHist : xHist,
        highlight : {}
      };
    };
  });


