'use strict';

angular.module('fs-visulization') 
  .factory('processMissingData', function() { 
    return _.memoize(function(chartType) {

      var n, a, b, a2, b2, x, y;
      var data1 = [];
      var data2 = [];
      var dy;
      var ymin = Infinity, ymax=-Infinity;

      //Fake data for bar chart
      if(chartType === 'bar'){
        n = 70;

        var bins = n,
          bin,
          accuracy = 50,
          target = bins * accuracy,
          stddev = bins * 0.15,
          generator = d3.random.normal(bins/2, stddev);
        x=0;
        y=0;

        for ( x = 0; x < n; x++) {
          data1.push({'x': x , 'y': 0});
        }
        // add numbers to random bins, normally distributed. Do it "target" times.
        for ( y = 0; y < target; y++) {
          // get a new number
          var number = generator();
          // don't allow numbers outside the desired range
          bin = Math.floor(Math.max(0, Math.min(number, bins-1)));

          data1[bin].y++;

          if(data1[bin].y < ymin) {
            ymin = data1[bin].y;
          }
          if(data1[bin].y > ymax) {
            ymax = data1[bin].y;
          }

        }
      }
      //Fake data for line chart
      else{

        n = 125;
        a = 10; 
        b = -4.5;
        a2 = 15; 
        b2 = -7;
        y=10;
        for ( x = 1; x <= n; x++) {
          dy = Math.random() * a + b; 
          y = y + dy;

          if(y < ymin) {
            ymin = y;
          }
          if(y > ymax) {
            ymax = y;
          }
          
          data1.push({'x': x  , 'y': y });
        }
        y = 12;
        for (var x2 = 1; x2 <= n; x2++) {
          dy = Math.random() * a2 * 2 + b2 * 2; 
          y = y + dy;
          
          if(y < ymin) {
            ymin = y;
          }
          if(y > ymax) {
            ymax = y;
          }

          data2.push({'x': x2  , 'y': y });
        }
      }

      var xScale; 
      if(chartType === 'bar'){
        xScale = d3.scale.ordinal()
          .domain(d3.range(data1.length));
      }
      else{
        xScale = d3.scale.linear()
          .domain([0, data1.length + 1]);
      }

      var yScale = d3.scale.linear()
          .domain([ymin - 4, ymax * 1.25 + 4]);

      return {
        data1 : data1,
        data2 : data2,
        xScale : xScale,
        yScale : yScale
      };
    });
  });