'use strict';
/*
  
  Use this directive whenever there is a chart that has no data.  It will display
  a "greyed out" fake chart with fake data behind your custom "description"
  Use like this:

    <fs-vis-missing-data
       description="'There is no data for the selected time period'"
       chart-type="'bar'"  <!-- optional, default is "line" -->
       fs-vis-config
       >
    </fs-vis-missing-data>

*/

angular.module('fs-visulization')
  .directive('fsVisMissingData', function(
    $parse,
    XAxis,
    MissingDataChart,
    zipWith, 
    accessor, 
    makeChart, 
    processMissingData,
    chartScaffolding,
    ColorDefs,
    BatchRender) {
    // jshint -W026, latedef: false, newcap: false

    return {
      restrict : 'EA',
      priority : 10,
      require : 'fsVisConfig',
      scope : false,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>',
                 ].join(''), 
      // transclude: true,
      link : function(scope, element, attrs, configCtrl /*, transclude */) {
        

        // get configuration
        var containerWidth = 
          parseInt(
            window.getComputedStyle(element.parent()[0], null).getPropertyValue('width'), 
          10);

        if (!containerWidth || containerWidth < 360) {
          containerWidth = 900;
        }

        var aspectRatio;

        if (containerWidth > 117 * 16) {
          aspectRatio = 3;
        } else if (containerWidth > 75 * 16) {
          aspectRatio = 5 / 2;
        } else if (containerWidth > 60 * 16) {
          aspectRatio = 16 / 9;
        } else {
          aspectRatio = 4 / 3;
        }

        var config = configCtrl.$mergeDefaults({
          description : 'Data currently missing or unavailable',
          timeField : 'time',
          valueFields : null,
          labels : null,
          width : containerWidth,
          aspectRatio : aspectRatio,
          margin : {
            top : 0,
            bottom : 0,
            left : 0,
            right: 0 
          },
          xAxisOffset : {
            top: 0,
            left : 0
          },
          xFormat : null,
          yFormat : null,
          colors : null
        });

        


        // scaffolding
        var svg = element.find('svg');
        svg.parent().attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
        var containers = chartScaffolding(svg[0], config.width, config.width / aspectRatio, config.margin, config.xAxisOffset); 
        
        var renderChart = makeChart(containers.chart.node());
        
        var batchRenderer = new BatchRender(function() {
          if(!state) {
            return;
          }

          renderChart(MissingDataChart, {
            message : config.description,
            chartType : chartType,
            data1 : state.data1,
            data2 : state.data2,
            xScale : state.xScale,
            yScale : state.yScale,
            color1 : ColorDefs.LeafHeavy,
            color2 : ColorDefs.SeaHeavy
          });
        });

        var state;

        var getChartType = $parse(attrs.chartType);
        var chartType = getChartType(scope) || 'line';

        scope.$watch(getChartType, function(newType) {
          state = processMissingData(newType);
          if (chartType === 'bar'){
            state.xScale.rangeRoundBands([0, containers.chartSize.width],0.05, 0);
          }
          else{
            state.xScale.range([0, containers.chartSize.width]);
          } 
          state.yScale.range([containers.chartSize.height, 0]);
          batchRenderer.render();
        });
      
      }
    };
  });
