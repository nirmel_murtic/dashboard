'use strict';

angular.module('fs-visulization')
  .factory('missingDataLineLayerGroup', function(accessor, vLayer, LineLayer, AreaLayer, VGridLayer, HGridLayer, BackgroundLayer, CenteredTextLayer) {
    
    var x = accessor('x');
    var y = accessor('y');


    return function(props) {
      var data1 = props.data1;
      var data2 = props.data2;
      var message = props.message;
      var xScale = props.xScale;
      var yScale = props.yScale;
      var color1 = props.color1;
      var color2 = props.color2;
      var getX = props.x || x;
      var getY = props.y || y;

      var bgLayer = [
        vLayer.render(BackgroundLayer, {
          xScale : xScale,
          yScale : yScale
        })
      ];

      var gridLayers = [
        vLayer.render(HGridLayer, {
          xScale: xScale,
          yScale: yScale
        }),
        vLayer.render(VGridLayer, {
          xScale: xScale,
          yScale: yScale
        })
      ];

      //DATA 1
      var lineLayer = [
        vLayer.render(LineLayer, {
          data: data1,
          x: getX,
          y: getY,
          xScale: xScale,
          yScale: yScale,
          color: color1,
          width : 2
        }),
      ];

      var areaLayer = [
        vLayer.render(AreaLayer, {
          data: data1,
          x: getX,
          y: getY,
          xScale: xScale,
          yScale: yScale,
          color: d3.rgb(color1).brighter().brighter(),
          opacity: 0.6,
        })
      ];


      // DATA 2
      var lineLayer2 = [
        vLayer.render(LineLayer, {
          data: data2,
          x: getX,
          y: getY,
          xScale: xScale,
          yScale: yScale,
          color: color2,
          width : 2
        }),
      ];

      var areaLayer2 = [
        vLayer.render(AreaLayer, {
          data: data2,
          x: getX,
          y: getY,
          xScale: xScale,
          yScale: yScale,
          color: d3.rgb(color2).brighter().brighter(),
          opacity: 0.2,
        })
      ];

      var fgLayer = [
        vLayer.render(BackgroundLayer, {
          xScale : xScale,
          yScale : yScale,
          color : 'rgba(239, 239, 239, 0.8)'
        })
      ];

      var centeredTextLayer = [
        vLayer.render(CenteredTextLayer, {
          message : message,
          xScale : xScale,
          yScale : yScale,
          color : 'rgba(0, 0, 0, 1)'
        })
      ];

      return bgLayer.concat(gridLayers, areaLayer, lineLayer, areaLayer2, lineLayer2, fgLayer, centeredTextLayer);
    };
  })
  .factory('missingDataBarLayerGroup', function(accessor, vChart, vLayer, VGridLayer, HGridLayer, BarsLayer, BackgroundLayer, CenteredTextLayer) {

    var x = accessor('x');
    var y = accessor('y');

    return function(props) {
      var data1 = props.data1;
      //var data2 = props.data2;
      var message = props.message;
      var xScale = props.xScale;
      var yScale = props.yScale;
      //var color1 = props.color1;
      var color2 = props.color2;
      var getX = props.x || x;
      var getY = props.y || y;

      var bgLayer = [
        vLayer.render(BackgroundLayer, {
          xScale : xScale,
          yScale : yScale
        })
      ];

      var gridLayers = [
        vLayer.render(HGridLayer, {
          xScale: xScale,
          yScale: yScale
        }),
        vLayer.render(VGridLayer, {
          xScale: xScale,
          yScale: yScale
        })
      ];

      var barsLayer = [
        vLayer.render(BarsLayer, {
          data: data1,
          x: getX,
          y: getY,
          xScale: xScale,
          yScale: yScale,
          color: color2
          // y0 : 10
        })
      ];

      var fgLayer = [
        vLayer.render(BackgroundLayer, {
          xScale : xScale,
          yScale : yScale,
          color : 'rgba(239, 239, 239, 0.8)'
        })
      ];

      var centeredTextLayer = [
        vLayer.render(CenteredTextLayer, {
          message : message,
          xScale : xScale,
          yScale : yScale,
          color : 'rgba(0, 0, 0, 1)'
        })
      ];
      
      return bgLayer.concat(gridLayers, barsLayer, fgLayer, centeredTextLayer);

    };
  })
  .factory('MissingDataChart', function(vChart, missingDataLineLayerGroup, missingDataBarLayerGroup) {

   
    return vChart.define({

      // MultiSeriesChart's render function
      render: function() {

        var chartType = this.props.chartType;
        if(chartType === 'bar') {
          return missingDataBarLayerGroup(this.props);
        } else {
          return missingDataLineLayerGroup(this.props);
        }
      }
    });


  });
  