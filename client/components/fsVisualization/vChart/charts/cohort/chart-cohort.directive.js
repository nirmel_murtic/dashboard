'use strict';

angular.module('fs-visulization')
  .directive('fsVisCohort', function(
    CohortChart, 
    $parse, 
    zipWith, 
    accessor, 
    CohortActions, 
    CohortDataStore, 
    CohortScaleStore, 
    CohortFocusStore, 
    makeChart, 
    Dispatcher, 
    CohortChartConstants, 
    chartScaffolding, 
    BatchRender, 
    eliminateDuplicateTicks) {
    // jshint -W026, latedef: false, newcap: false
    return {
      restrict : 'EA',
      priority : 10,
      require : 'fsVisConfig',
      scope : false,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>',
                 ].join(''), 
      transclude: true,
      link : function(scope, element, attrs, configCtrl, transclude) {
        var dataGetter = $parse(attrs.data);
        scope.data = function() {
          return dataGetter(scope);
        };

        // get configuration
        var containerWidth = 
          parseInt(
            window.getComputedStyle(element.parent()[0], null).getPropertyValue('width'), 
          10);

        if (!containerWidth || containerWidth < 360) {
          containerWidth = 900;
        }

        var aspectRatio;

        if (containerWidth > 117 * 16) {
          aspectRatio = 3;
        } else if (containerWidth > 75 * 16) {
          aspectRatio = 5 / 2;
        } else if (containerWidth > 60 * 16) {
          aspectRatio = 16 / 9;
        } else {
          aspectRatio = 4 / 3;
        }

        var defaultDate = d3.time.format('%m/%d/%y');
        var config = configCtrl.$mergeDefaults({
          timeField : 'time',
          valueFields : null,
          labels : null,
          width : containerWidth,
          aspectRatio : aspectRatio,
          margin : {
            top : 25,
            bottom : 60,
            left : 75,
            right: 75 
          },
          xAxisOffset : {
            top: 25,
            left : 25
          },
          xFormat : function(d) { 
            if(!d) {
              return '';
            }
            return defaultDate(d);
          },
          yFormat : null,
          colors : null
        });

        // scaffolding
        var svg = element.find('svg');
        svg.parent().attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
        var containers = chartScaffolding(svg[0], config.width, config.width / aspectRatio, config.margin, config.xAxisOffset); 

        // setup stores
        var dispatcher = new Dispatcher();
        var dataStore = CohortDataStore({
          dispatcher : dispatcher,
          timeField : config.timeField,
          valueFields : config.valueFields,
          freq : 24 * 36000000
        });

        var scaleStore = CohortScaleStore({
          dispatcher : dispatcher,
          dataStore : dataStore,
          colorSets : config.colors ? [config.colors] : null,
          width : containers.chartSize.width,
          height : containers.chartSize.height
        });

        var focusStore = CohortFocusStore({
          dispatcher : dispatcher,
          dataStore : dataStore,
          scaleStore : scaleStore
        });

        var actionCreator = CohortActions(dispatcher);

        if(__DEV__) {
          window.ChD = dataStore;
          window.ChS = scaleStore;
          window.ChF = focusStore;
        }
        var xAxis = d3.svg.axis().orient('bottom').tickSize(0);
        var yAxis = d3.svg.axis().orient('left');

        var renderChart = makeChart(containers.chart.node()); 
        var batchRenderer = new BatchRender(function() {
          if(!scaleStore.x().domain().length) {
            return;
          }

          var yFormat = dataStore.getYFormat();
          var xFormat = dataStore.getXFormat();

          // xAxis.scale(scaleStore.x()).tickValues(dataStore.xTickValues());
          xAxis.scale(scaleStore.xTime());
          xAxis.tickValues(dataStore.xTickValues());
          yAxis.scale(scaleStore.y());

          if(xFormat) {
            xAxis.tickFormat(xFormat);
          } else {
            xAxis.tickFormat(null);
          }
          var yTicks;
          yAxis.tickValues(null);
          if(yFormat) {
            yTicks = eliminateDuplicateTicks(scaleStore.y(), yFormat);
            if(yTicks !== false) {
              yAxis.tickValues(yTicks);
            }
            yAxis.tickFormat(yFormat);
          } else {
            yAxis.tickFormat(null);
          }

          containers.xAxis.call(xAxis);
          containers.yAxis.call(yAxis);
 
          renderChart(CohortChart, {
            data : dataStore.getData(),
            xScale : scaleStore.x(),
            yScale : scaleStore.y(),
            color1 : scaleStore.color1(),
            title : config.chartTitle,
            x : dataStore.xAccessor(),
            y : dataStore.yAccessor(),
            y0 : dataStore.y0Accessor(),
            yFormat : yFormat,
            labels : dataStore.getFilteredLabels(),
            keys : dataStore.getFilteredFields(),
            focus : focusStore.getFocus()
          });
        });

        dispatcher.dispatch({
          source : 'external',
          action : {
            type : CohortChartConstants.FORMATTER,
            x : config.xFormat,
            y : config.yFormat
          }
        });

        dataStore.on('change', batchRenderer.render);
        scaleStore.on('change', batchRenderer.render);
        focusStore.on('change', batchRenderer.render);

        scaleStore.on('change', function() {
          scope.legendData = 
            zipWith(
              dataStore.getAllFields(), 
              dataStore.getLabels(), 
              dataStore.getFilters(), 
              scaleStore.color1().range(), 
          function(key, label, active, color) {
            return {
              key : key,
              name : label,
              active : active,
              color: color.toString()
            };
          }); 
        });

        // throttle mouse move at ~30 fps
        var mouseMoveCallback = _.throttle(function(coords) {
          // dispatch a highlight action for given mouse coordinates
          actionCreator.focus(coords);
        }, 33);
        containers.chart.on('mousemove', function() {
          var coords = d3.mouse(this);
          mouseMoveCallback(coords);
        });
        svg.on('mouseleave', function() {
          actionCreator.clearFocus();
        });

        scope.actionCreator = actionCreator;
        scope.dataStore = dataStore;
        scope.scaleStore = scaleStore;
        scope.legendData = [];

        // setup watchers

        scope.$watchGroup([
            'data()', configCtrl.labels, configCtrl.chartTitle, configCtrl.formatX, configCtrl.yFormat
        ], 
        function(values) {
          if(!values || !values[0] || !values[1]) {
            return;
          }
          config.chartTitle = values[2] || config.chartTitle || '';
          if(values[3] || values[4]) {
            dispatcher.dispatch({
              source : 'external',
              action : {
                type : CohortChartConstants.FORMATTER,
                x : config.xFormat,
                y : values[4]
              }
            });
          }
          dispatcher.dispatch({
            source : 'external',
            action : {
              type : CohortChartConstants.DATA,
              data : values[0],
              labels : values[1]
            }
          });
        });
        //
        // put directives contents into transcluded elements
        // specify attribute 'append-after' if you want the html to display **after** the chart/svg
        transclude(scope, function(tcEl) {
          var len = tcEl.length;
          var before = element.find('.chart-control-before')[0];
          var after = element.find('.chart-control-after')[0];
          var container;
          for(var i=0; i<len; i++) {
            if(tcEl[i] && tcEl[i].hasAttribute && tcEl[i].hasAttribute('append-after')) {
              container = after;
            } else {
              container = before;
            }
            container.appendChild(tcEl[i]);
          }
        });
      }
    };
  });
