'use strict';

angular.module('fs-visulization')
  .factory('CohortScaleStore', function(invariant, accessor, fsUtils, binarySearchNearest, CohortChartConstants, colorPresets, nicerScales) {

    var noDomain = [0,0];
    return function scaleStoreFactory(opts) {
      var dispatcher = opts.dispatcher;
      var dataStore = opts.dataStore;
      var colorSets = opts.colorSets;
      var yrange = [opts.height, 0];

      var x = d3.scale.ordinal().rangeBands([0, opts.width], 0.05, 0);
      var xTime = d3.time.scale();
      var y = d3.scale.linear().range(yrange);

      var xDomainForced = null, yDomainForced = null;

      var color1 = d3.scale.category20();
      var color2 = d3.scale.category20();

      if(colorSets && colorSets.length) {
        color1.range(colorSets[0]);
        color2.range(colorSets[1] || colorSets[0]);
      } else {
        color1.range(colorPresets.solids);
        color2.range(colorPresets.solids);
      }

      var store = fsUtils.Emitter({
        x : function() {
          return x;
        },
        xTime : function() {
          return xTime;
        },
        y : function() {
          return y;
        },
        color1 : function() {
          return color1;
        },
        color2 : function() {
          return color2;
        }
      });  

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken ]);
        switch (action.type) {
          case CohortChartConstants.FORCE_SCALE_DOMAIN:
            if(action.scale === 'x') {
              xDomainForced = action.domain;
              updateXScale();
            } else if(action.scale === 'y'){
              yDomainForced = action.domain;
              updateYScale();
            } else {
              return;
            }
            break;
          case CohortChartConstants.DATA:
            onData();
            break;
          case CohortChartConstants.FILTER:
            onData();
            break;
          case CohortChartConstants.MODE:
            updateYScale();
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      function updateYScale() {
        y = y.copy();
        if(!yDomainForced) {
          y.domain( dataStore.yDomain() || noDomain );        
        } else {
          y.domain(yDomainForced);
        }
        nicerScales.patchDegenerate(y);
        y.nice();
      }

      function updateXScale() {
        x = x.copy();
        if (dataStore.xDomain().length < 2) {
          x.domain([dataStore.xDomain()[0], d3.time.day.offset(dataStore.xDomain()[0], 1)]);
        } else {
          if(!xDomainForced) {
            x.domain( dataStore.xDomain() || noDomain );        
          } else {
            x.domain(xDomainForced);
          }
        }
        xTime.domain( x.domain() );
        var rb = x.rangeBand();
        xTime.range( x.range().map(function(r) {
          return r + rb / 2;
        }) );
      }

      function onData() {
        color1.domain( dataStore.getAllFields() || noDomain );
        color2.domain( dataStore.getAllFields() || noDomain );
        updateXScale();
        updateYScale();
      }

      return store;

    };
  });
