'use strict';

angular.module('fs-visulization')
  .factory('CohortDataStore', function(invariant, accessor, fsUtils, CohortChartConstants, transposeObject) {
    // jshint camelcase:false, newcap:false, bitwise : false, latedef:false

    // define accessor functions

    var Constants = CohortChartConstants;

    var resolver = function() {
      return _.toArray(arguments).join(',');
    };

    var projectX = accessor('x');
    var projectRawY = accessor('y');
    var projectRawYAcc = accessor('yAcc');

    var projectY = _.memoize(function(accMode, stackMode) {
      if(accMode === Constants.NON_ACCUMULATIVE) {
        switch (stackMode) {
          case Constants.STACK_BASE:
            return function(d) {
              return d.y + d.y0b;
            };
          case Constants.STACK_NORM:
            return function(d) {
              return d.yn + d.y0n;
            };
          default: // STACK_ZERO
            return function(d) {
              return d.y + d.y0;
            };
        }
      } else {
         switch (stackMode) {
          case Constants.STACK_BASE:
            return function(d) {
              return d.yAcc + d.y0bAcc;
            };
          case Constants.STACK_NORM:
            return function(d) {
              return d.ynAcc + d.y0nAcc;
            };
          default: // STACK_ZERO
            return function(d) {
              return d.yAcc + d.y0Acc;
            };
        }
      }
    }, resolver);

    var projectY0 = _.memoize(function(accMode, stackMode) {
      if(accMode === Constants.NON_ACCUMULATIVE) {
         switch (stackMode) {
          case Constants.STACK_BASE:
            return accessor('y0b');
          case Constants.STACK_NORM:
            return accessor('y0n');
          default: // STACK_ZERO
            return accessor('y0');
        }
      } else {
         switch (stackMode) {
          case Constants.STACK_BASE:
            return accessor('y0bAcc');
          case Constants.STACK_NORM:
            return accessor('y0nAcc');
          default: // STACK_ZERO
            return accessor('y0Acc');
        }
      }
    }, resolver);


    // stacked layouts helper functions for stack modes
    //
    //              | Stack Zero       | Stack Normalize
    // -------------------------------------------------------
    // Accumulative | stackZeroAcc     | stackNormalizeAcc
    // -------------------------------------------------------
    // Delta/Non-acc| stackZero        | stackNormalize
    // -------------------------------------------------------
    var stackZero = d3.layout.stack()
      .offset('zero')
      .x(projectX)
      .y(projectRawY)
      .out(function(d, y0) {
        d.y0 = y0;
      });
    var stackZeroAcc = d3.layout.stack()
      .offset('zero')
      .x(projectX)
      .y(projectRawYAcc)
      .out(function(d, y0) {
        d.y0Acc = y0;
      });
    var stackNormalize = d3.layout.stack()
      .offset('expand')
      .x(projectX)
      .y(projectRawY)
      .out(function(d, y0, y) {
        d.y0n = y0;
        d.yn = y;
      });
    var stackNormalizeAcc = d3.layout.stack()
      .offset('expand')
      .x(projectX)
      .y(projectRawYAcc)
      .out(function(d, y0, y) {
        d.y0nAcc = y0;
        d.ynAcc = y;
      });

    var formatPercentage = d3.format('.2%f');


    return function cohortDataStoreFactory(opts) {
      var dispatcher = opts.dispatcher;
      var timeField = opts.timeField;
      var valueFields = opts.valueFields;
      // var freq = opts.freq;

      var filterIndex = [];
      var currentMode = [ Constants.NON_ACCUMULATIVE, Constants.STACK_ZERO ];
      var data = null;
      var getXDomain, getYDomain, getData;
      var names = [];

      var xFormat, yFormat;

      var store = fsUtils.Emitter({
        yDomain : function() {
          return getYDomain ? getYDomain(currentMode[0], currentMode[1]) : [0,0];
        },
        xDomain : function() {
          return getXDomain ? getXDomain() : [0, 1];
        },
        xTickValues : function() {
          var every, days;
          if(getXDomain) {
            days = getXDomain();
            if(days.length <= 5) {
              return days;
            }
            every = ~~(days.length / 5);
            return _.map(days, function(t, i){
              if(i % every === 0) {
                return t;
              } else {
                return null;
              }
            });
          } 
        },
        getData : function() {
          return getData ? getData(filterIndex) : [];
        },
        xAccessor : function() {
          return projectX;
        },
        yAccessor : function() {
          return projectY(currentMode[0], currentMode[1]);
        },
        y0Accessor : function() {
          return projectY0(currentMode[0], currentMode[1]);
        },
        getXFormat : function() {
          return xFormat;
        },
        getYFormat : function() {
          return currentMode[1] === Constants.STACK_NORM ? formatPercentage : yFormat;
        },
        getLabels : function() {
          return names;
        },
        getFilteredLabels : function() {
          return _.filter(names, function(__, i) {
            return filterIndex[i];
          });
        },
        getAllFields : function() {
          return valueFields;
        },
        getFilters : function() {
          return filterIndex;
        },
        getFilteredFields : function() {
          return _.filter(valueFields, function(__, i) {
            return filterIndex[i];
          });
        },
        getStackMode : function() {
          return currentMode[1];
        },
        getAccumulativeMode : function() {
          return currentMode[0];
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        switch (action.type) {
          case Constants.DATA:
            initData(action.data);
            names = action.labels;
            break;
          case Constants.MODE:
            if(_.isEqual(action.mode, currentMode)) {
              return;
            }
            currentMode[0] = action.mode[0] || currentMode[0];
            currentMode[1] = action.mode[1] || currentMode[1];
            break;
          case Constants.FILTER:
            if(action.filter) {
              filterIndex = action.filter;
            } else if ( _.isNumber(action.index) && 
              (filterIndex[action.index] ? _.filter(filterIndex).length > 1 : true) ) {
              filterIndex[action.index] = !filterIndex[action.index];
            } else {
              return;
            }
            applyFilter(filterIndex);
            break;
          case Constants.FORMATTER:
            xFormat = action.x || xFormat;
            yFormat = action.y || yFormat;
            break;
          default:
            return; 
        }
        store.emit('change');
      }); 

      return store;

      function initData(rawData) {

        // var filled = freq ? fillGap(rawData, freq, timeField, valueFields) : rawData;      
        var filled = rawData;

        // all unique timestamps
        getXDomain = _.memoize(function() {
          return _.map(filled, accessor(timeField));
        });

        var mapName =  _(valueFields).map(function(field) { return [field, 'y']; }).zipObject().value();
        mapName[timeField] = 'x';
        var byColumn = 
          transposeObject(filled, {
            fixture : timeField,
            mapName : mapName
          });

        _.each(byColumn, function(column, j) {
          column.key = valueFields[j];
          _.reduce(column, function(acc, d) {
            d.yAcc = acc + d.y;
            return d.yAcc;
          }, 0);
        });

        data = byColumn;
        filterIndex = _.map(data, _.constant(true));
        applyFilter(filterIndex);
      }

      function applyFilter(filters) {
        filterIndex = filters;
        getData = _.memoize(filterColumns); 
        getData(filterIndex);
      } 

      function filterColumns(filterIndex) {

        var filteredColumns = _.filter(data, function(__, i) {
          return filterIndex[i];
        });

        stackZero(filteredColumns);
        stackNormalize(filteredColumns);
        stackZeroAcc(filteredColumns);
        stackNormalizeAcc(filteredColumns);

        getYDomain = _.memoize(function(accMode, stackMode) {
          var domains;
          if(accMode === Constants.NON_ACCUMULATIVE) {
             switch (stackMode) {
              case Constants.STACK_NONE:
                domains = _.map(filteredColumns, function(col) {
                  return d3.extent(col, projectRawY);
                });
                return d3.extent(_.flatten(domains));
              case Constants.STACK_NORM:
                return [0, 1];
              default: // STACK_ZERO
                return [
                  0,
                  d3.max(filteredColumns[filteredColumns.length - 1], projectY(accMode, stackMode))
                ];
            }
          } else {
            switch(stackMode) {
              case Constants.STACK_NONE:
                domains = _.map(filteredColumns, function(col) {
                  return d3.extent(col, projectRawYAcc);
                });
                return d3.extent(_.flatten(domains));
              case Constants.STACK_NORM:
                return [0, 1];
              default: // STACK_ZERO
                return [
                  0,
                  d3.max(filteredColumns[filteredColumns.length - 1], projectY(accMode, stackMode))
                ];
            }
          }
        }, resolver);

        return filteredColumns;
      }

      /*
      function fillGap(dataArr, freq, timeField, valueFields) {
        if(dataArr.length <= 1) {
          return dataArr;
        }
        for(var i=0;;) {
          i = i + 1 + _fillGap(i, dataArr, freq, timeField, valueFields);
          if(i >= dataArr.length - 1) {
            break;
          }
        } 
        return dataArr;
      }
      function _fillGap(index, dataArr, freq, timeField, valueFields) {
        var i, a, b, d, k=0;
        a = dataArr[index];
        b = dataArr[index + 1];
        var isDate = a[timeField] instanceof Date;
        if(b[timeField] - a[timeField] > freq) {
          do {
            k++;
            d = {};
            d[timeField] = isDate ? new Date(a[timeField] + k * freq) : a[timeField] + k * freq;
            for(i=0; i<valueFields.length; i++) {
              d[ valueFields[i] ] = 0;
            }
          } while (d[timeField] < b[timeField] && dataArr.splice(index+k, 0, d));
          return k-1;
        }
        return 0;
      }
      */


      // handle payload

      return store;
    };

  });
