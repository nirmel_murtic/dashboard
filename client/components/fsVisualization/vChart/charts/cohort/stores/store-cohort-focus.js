'use strict';

angular.module('fs-visulization')
  .factory('CohortFocusStore', function(invariant, accessor, fsUtils, binarySearchNearest, searchNearest, CohortChartConstants) {

    // jshint newcap: false, unused:false, latedef: false

    var Emitter = fsUtils.Emitter;
    var Constants = CohortChartConstants;

    return function stackedChartScaleStore(opts) {
      var dispatcher = opts.dispatcher;
      var scaleStore = opts.scaleStore;
      var dataStore = opts.dataStore;

      // states
      var focus; 

      var store = Emitter({
        getFocus : function() {
          return focus;
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken, scaleStore.dispatchToken ]);
        switch (action.type) {
          case Constants.HIGHLIGHT_XY:
            findFocus(action.x, action.y); 
            break;
          case Constants.DEHIGHLIGHT:
            clearFocus(); 
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;

      function findFocus(x, y) {
        var accMode = dataStore.getAccumulativeMode();
        var xScale = scaleStore.x();
        var ranges = xScale.range();
        for(var i=0; i<ranges.length; i++) {
          if(x >= ranges[i] && (i === ranges.length - 1 || x < ranges[i+1])) {
            break;
          }
        } 
        if(i < 0 || i >= ranges.length) {
          focus = null;
          return;
        }
        focus = {};
        focus.time = xScale.domain()[i];
        var datums = dataStore.getData().map(function(column) {
          return column[i];
        });
        focus.values = datums.map(function(d) {
          return d.y;
        });
        focus.percentages = datums.map(function(d) {
          // accMode === Constants.NON_ACCUMULATIVE ? d.yn : d.ynAcc;
          return d.yn;
        });
        var ys = _(datums)
         .map(dataStore.y0Accessor())
         .map(scaleStore.y())
         .value();
        var j = binarySearchNearest(y, ys);
        focus.index = j;
      }
      function clearFocus() {
        focus = null;
      }

    };
  });
