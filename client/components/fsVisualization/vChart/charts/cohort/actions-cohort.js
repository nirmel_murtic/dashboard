'use strict';

angular.module('fs-visulization')
  .constant('CohortChartConstants', {
    DEHIGHLIGHT : 'CLEAR_ALL_HIGHLIGHTS',
    HIGHLIGHT_XY : 'HIGHLIGHT_XY',
    STACK_BASE : 'STACK_BASE',
    STACK_ZERO : 'STACK_ZERO',
    STACK_NORM : 'STACK_NORM',
    ACCUMULATIVE : 'ACCUMULATIVE',
    NON_ACCUMULATIVE : 'NON_ACCUMULATIVE',
    MODE : 'MODE_COHORT',
    FILTER : 'FILTER_COHORT',
    FORMATTER : 'FORMAT_COHORT',
    DATA : 'DATA_COHORT'
  })
  .service('CohortActions', function(CohortChartConstants) {
    return function actionsFactory(dispatcher) {
      return {
        toggleAccumulative : function(on) {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CohortChartConstants.MODE,
              mode : [on ? CohortChartConstants.ACCUMULATIVE : CohortChartConstants.NON_ACCUMULATIVE, null]
            }
          }); 
        },
        toggleCohort : function(index) {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CohortChartConstants.FILTER,
              index : index
            }
          }); 
        },
        stack : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CohortChartConstants.MODE,
              mode : [null, CohortChartConstants.STACK_ZERO]
            }
          }); 
        },
        normalize : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CohortChartConstants.MODE,
              mode : [null, CohortChartConstants.STACK_NORM]
            }
          }); 
        },
        focus : function(pos) {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CohortChartConstants.HIGHLIGHT_XY,
              x : pos[0],
              y : pos[1]
            }
          });
        },
        clearFocus : function() {
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CohortChartConstants.DEHIGHLIGHT,
            }
          });
        }
      };
    };
  });

