'use strict';

angular.module('fs-visulization')
  .factory('CohortChart', function(accessor, vChart, vLayer, BarsLayer, VGridLayer, HGridLayer, TooltipLayer, TooltipLayerAdvanced, GuidelineLayer, BackgroundLayer, $filter) {

    function x(d) {
      return d.x;
    }
    function y(d) {
      return d.y + d.y0;
    }
    
    // var defaultTimeFormat = d3.time.format('%m/%d/%y %I:%M %p');
    var defaultDateFormat = $filter('metricDate');
    var formatPercentage = d3.format('.2%f');

    return vChart.define({

      // MultiSeriesChart's render function
      render: function() {

        var data = this.props.data;
        // var highlights = this.props.highlights;
        // var hlValues = this.props.highlightedValues;
        var xScale = this.props.xScale;
        var yScale = this.props.yScale;
        var color1 = this.props.color1;
        var getX = this.props.x || x;
        var getY = this.props.y || y;
        var getY0 = this.props.y0; 

        var focus = this.props.focus;
        var labels = this.props.labels;
        var keys = this.props.keys;

        var yFormat = this.props.yFormat;


        // var names = this.props.names;
        // var labels = this.props.labels || names;

        var bgLayer = [
          vLayer.render(BackgroundLayer, {
            xScale : xScale,
            yScale : yScale
          })
        ];

        var gridLayers = [
          vLayer.render(HGridLayer, {
            xScale: xScale,
            yScale: yScale
          }),
          vLayer.render(VGridLayer, {
            xScale: xScale,
            yScale: yScale
          })
        ];

        var bars = data.map(function(d) {
          var color = color1(d.key);
          return vLayer.render(BarsLayer, {
            xScale : xScale,
            yScale : yScale,
            data : d,
            x : getX,
            y : getY,
            color : color,
            y0 : getY0,
            key : d.key 
          });
        });

        var ttx, tty, xmid, xrange, texts, tooltipLayer;

        if(focus) {
          ttx = xScale( focus.time ) + xScale.rangeBand() / 2;

          tty = (yScale.range()[0] + yScale.range()[1]) / 2;

          xrange = this.props.xScale.rangeExtent(); 
          xmid = (xrange[0] + xrange[1]) / 2;

          texts = focus.values.map(function(val, i) {
            return [
              {
                text : '\u25A3',
                color : color1(keys[i])
              },
              {
                text : labels[i],
                color : color1(keys[i]),
                bold : true
              },
              yFormat ? yFormat(val) : String(val),
              formatPercentage(focus.percentages[i])
            ];
          });
          texts.unshift([
            '',
            {
              text : 'Date',
              bold : true
            }, 
            defaultDateFormat(focus.time),
            ''
          ]);
          if(this.props.title) {
            texts.unshift(['Metric', {
              text: this.props.title, 
              bold : true
            }, '', '']);
          }

          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: ttx,
              y: tty,
              orient : [ttx > xmid ? 'left' : 'right'],
              texts : texts
            })
          ];
        } else {
          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: 0,
              y: 0,
              texts : [['', '', '', '']],
              hidden : true,
              orient : ['left']
            })
          ];
        }
        var guidelineLayer;
        if(focus) {
          guidelineLayer = [
            vLayer.render(GuidelineLayer, {
              x : ttx,
              yScale : yScale
            })
          ];
        } else {
          guidelineLayer = [];
        }

        return bgLayer.concat(gridLayers, bars, tooltipLayer, guidelineLayer);

      }
    });    

  });
