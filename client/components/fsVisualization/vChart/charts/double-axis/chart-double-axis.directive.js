'use strict';

angular.module('fs-visulization')
  .directive('fsVisDoubleAxis', function(
    XAxis,
    DoubleAxisY1,
    DoubleAxisY2,
    DoubleAxisChart,
    $parse, 
    zipWith, 
    accessor, 
    DoubleAxisActions, 
    DoubleAxisDataStore, 
    DoubleAxisScaleStore, 
    DoubleAxisHighlightStore, 
    makeChart, 
    Dispatcher, 
    DoubleAxisConstants, 
    chartScaffoldingDoubleAxis, 
    BatchRender,
    eliminateDuplicateTicks) {
    // jshint -W026, latedef: false, newcap: false
    return {
      restrict : 'EA',
      priority : 10,
      require : 'fsVisConfig',
      scope : false,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>',
                 ].join(''), 
      transclude: true,
      link : function(scope, element, attrs, configCtrl, transclude) {
        var dataGetter = $parse(attrs.data);
        scope.data = function() {
          return dataGetter(scope);
        };

        // get configuration
        var containerWidth = 
          parseInt(
            window.getComputedStyle(element.parent()[0], null).getPropertyValue('width'), 
          10);

        if (!containerWidth || containerWidth < 360) {
          containerWidth = 900;
        }

        var aspectRatio;

        if (containerWidth > 117 * 16) {
          aspectRatio = 3;
        } else if (containerWidth > 75 * 16) {
          aspectRatio = 5 / 2;
        } else if (containerWidth > 60 * 16) {
          aspectRatio = 16 / 9;
        } else {
          aspectRatio = 4 / 3;
        }

        var config = configCtrl.$mergeDefaults({
          timeField : 'time',
          valueFields : null,
          labels : null,
          width : containerWidth,
          aspectRatio : aspectRatio,
          margin : {
            top : 25,
            bottom : 60,
            left : 75,
            right: 75 
          },
          xAxisOffset : {
            top: 25,
            left : 25
          },
          formatX : null,
          yFormat : null,
          y1Format : null,
          colors : null
        });

        // scaffolding
        var svg = element.find('svg');
        svg.parent().attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
        var containers = chartScaffoldingDoubleAxis(svg[0], config.width, config.width / aspectRatio, config.margin, config.xAxisOffset); 
        // setup stores
        var dispatcher = new Dispatcher();

        var dataStore = DoubleAxisDataStore({
          dispatcher : dispatcher,
          timeField : config.timeField,
          valueFields : config.valueFields,
          seriesLabels : config.labels,
          y1Format : config.yFormat,
          y2Format : config.y1Format,
          xFormat : config.formatX,
        });

        var scaleStore = DoubleAxisScaleStore({
          dispatcher : dispatcher,
          dataStore : dataStore,
          colorSets : config.colors ? [config.colors] : null,
          width : containers.chartSize.width,
          height : containers.chartSize.height
        });

        var highlightStore = DoubleAxisHighlightStore({
          dispatcher : dispatcher,
          dataStore : dataStore,
          scaleStore : scaleStore
        });

        var actionCreator = DoubleAxisActions(dispatcher);

        var renderY1 = makeChart(containers.yAxis1.node());
        var renderY2 = makeChart(containers.yAxis2.node());
        var renderX = makeChart(containers.xAxis.node());
        var renderChart = makeChart(containers.chart.node());

        var batchRenderer = new BatchRender(function() {
          var y1Ticks, 
              y2Ticks,
              y1Format = dataStore.getY1Format(), 
              y2Format = dataStore.getY2Format();

          if(y1Format) {
            y1Ticks = eliminateDuplicateTicks(scaleStore.y1(), y1Format);
          }
          if(y2Format) {
            y2Ticks = eliminateDuplicateTicks(scaleStore.y2(), y2Format);
          }

          renderY1(DoubleAxisY1, { yScale : scaleStore.y1(), color: scaleStore.color1().range()[0], format : y1Format, tickValues : y1Ticks});
          if(dataStore.isDouble()) {
            renderY2(DoubleAxisY2, { yScale : scaleStore.y2(), color: scaleStore.color1().range()[1], format : y2Format, tickValues : y2Ticks });
          } else {
            renderY2(DoubleAxisY2, { yScale : null });
          }
          renderX(XAxis, { xScale : scaleStore.x() });


          renderChart(DoubleAxisChart, {
            title : config.title,
            data : dataStore.getData(),
            xScale : scaleStore.x(),
            y1Scale : scaleStore.y1(),
            y2Scale : scaleStore.y2(),
            color1 : scaleStore.color1(),
            xFormat : dataStore.getXFormat(),
            y1Format : y1Format,
            y2Format : y2Format,
            highlight : highlightStore.getHighlight(),
            keys : dataStore.getValueFields(),
            labels : dataStore.getLabels()
          }); 
        });

        dataStore.on('change', batchRenderer.render);
        scaleStore.on('change', batchRenderer.render);
        highlightStore.on('change', batchRenderer.render);

        scaleStore.on('change', function() {
          scope.legendData = zipWith(dataStore.getValueFields(), dataStore.getLabels(), scaleStore.color1().range(), function(key, label, color) {
            return {
              key : key,
              name : label,
              color: color.toString()
            };
          }); 
        });

        // throttle mouse move at ~30 fps
        var mouseMoveCallback = _.throttle(function(coords) {
          // dispatch a highlight action for given mouse coordinates
          actionCreator.highlightXY(coords);
        }, 33);
        containers.chart.on('mousemove', function() {
          var coords = d3.mouse(this);
          mouseMoveCallback(coords);
        });
        svg.on('mouseleave', function() {
          actionCreator.clearHighlight();
        });

        scope.actionCreator = actionCreator;
        scope.dataStore = dataStore;
        scope.scaleStore = scaleStore;
        scope.legendData = [];

        scope.$watchGroup([
            'data()', configCtrl.labels, configCtrl.chartTitle, configCtrl.formatX, configCtrl.yFormat, configCtrl.valueFields, configCtrl.timeField, configCtrl.y1Format
        ], 
        function(values, oldValues) {
          if(!values || !values[0] || !values[1]) {
            return;
          }
          config.title = values[2] || config.title || '';
          // changing valueFields and/or timeField
          if(values[5] !== oldValues[5] || !_.isEqual(values[6], oldValues[6])) {
            config.valueFields = values[5];
            config.timeField = values[6];
            dispatcher.dispatch({
              source : 'external',
              action : {
                type : DoubleAxisConstants.CHANGE_FIELDS,
                valueFields : values[5],
                timeField : values[6]
              }
            });
          }
          if(values[3] || values[4] || values[7]) {
            dispatcher.dispatch({
              source : 'external',
              action : {
                type : DoubleAxisConstants.FORMATTERS,
                x : values[3],
                y1 : values[4],
                y2 : values[7]
              }
            });
          }
          dispatcher.dispatch({
            source : 'external',
            action : {
              type : DoubleAxisConstants.DATA,
              data : values[0],
              labels : values[1]
            }
          });
        });
        // put directives contents into transcluded elements
        // specify attribute 'append-after' if you want the html to display **after** the chart/svg
        transclude(scope, function(tcEl) {
          var len = tcEl.length;
          var before = element.find('.chart-control-before')[0];
          var after = element.find('.chart-control-after')[0];
          var container;
          for(var i=0; i<len; i++) {
            if(tcEl[i] && tcEl[i].hasAttribute && tcEl[i].hasAttribute('append-after')) {
              container = after;
            } else {
              container = before;
            }
            container.appendChild(tcEl[i]);
          }
        });

      }
    };
  });
