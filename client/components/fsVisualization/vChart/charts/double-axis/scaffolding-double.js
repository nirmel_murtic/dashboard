'use strict';

angular.module('fs-visulization')
  .factory('chartScaffoldingDoubleAxis', function() {
    return function scaffolding(element, width, height, margin, xAxisOffset) {
      var sel = d3.select(element);
      var svg;
      if(element.tagName === 'svg') {
        svg = sel;
      } else {
        svg = sel.select('svg').empty() ? sel.html('').append('svg') : svg;
      }

      svg.attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('viewBox', '0 0 ' + width + ' ' + height);

      var chartArea, xAxis, yAxis1, yAxis2, leftCover, rightCover;

    
      var chartAreaWidth = width - margin.left - margin.right - xAxisOffset.left * 2;
      var chartAreaHeight = height - margin.top - margin.bottom - xAxisOffset.top; 



      chartArea = svg.select('g.chart-area'); 
      chartArea = chartArea.empty() ? svg.append('g').attr('class', 'chart-area') : chartArea;
      chartArea.attr('transform', 'translate(' + (margin.left + xAxisOffset.left) + ',' + margin.top + ')' );

      leftCover = svg.select('.left-cover');
      leftCover = leftCover.empty() ? svg.append('rect').attr('class', 'chart-cover left-cover') : leftCover;
      leftCover.attr('width', margin.left + xAxisOffset.left - 13).attr('height', chartAreaHeight + margin.top + xAxisOffset.top);

      rightCover = svg.select('.right-cover');
      rightCover = rightCover.empty() ? svg.append('rect').attr('class', 'chart-cover right-cover') : rightCover;
      rightCover.attr('width', margin.right - 12)
        .attr('transform', 
          'translate(' + (margin.left + xAxisOffset.left + chartAreaWidth + 12) + ',0)')
        .attr('height', chartAreaHeight + margin.top + xAxisOffset.top);

      yAxis1 = svg.select('g.y1');
      yAxis1 = yAxis1.empty() ? svg.append('g').attr('class', 'y1 axis') : yAxis1;
      yAxis1.attr('transform', 'translate(' + (margin.left) + ',' + margin.top + ')' );

      yAxis2 = svg.select('g.y2');
      yAxis2 = yAxis2.empty() ? svg.append('g').attr('class', 'y2 axis') : yAxis2;
      yAxis2.attr('transform', 'translate(' + (margin.left + chartAreaWidth + xAxisOffset.left * 2) + ',' + margin.top + ')' );

      xAxis = svg.select('g.x.axis');
      xAxis = xAxis.empty() ? svg.append('g').attr('class', 'x axis') : xAxis;
      xAxis.attr('transform', 'translate(' + (margin.left + xAxisOffset.left) + ',' + (margin.top + chartAreaHeight + xAxisOffset.top) + ')' );

      return {
        chart : chartArea,
        xAxis : xAxis,
        yAxis1 : yAxis1,
        yAxis2 : yAxis2,
        chartSize : { width: chartAreaWidth, height: chartAreaHeight },
      };
    };
  });
