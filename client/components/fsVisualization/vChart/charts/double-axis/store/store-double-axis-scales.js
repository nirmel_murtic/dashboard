'use strict';

angular.module('fs-visulization')
  .factory('DoubleAxisScaleStore', function(invariant, accessor, pointInPolygon, fsUtils, binarySearchNearest, DoubleAxisConstants, colorPresets, nicerScales) {

    var noDomain = [0,0];
    return function scaleStoreFactory(opts) {
      var dispatcher = opts.dispatcher;
      var dataStore = opts.dataStore;
      var colorSets = opts.colorSets;
      var yrange = [opts.height, 0];

      var x = d3.time.scale().range([0, opts.width]);
      var y1 = d3.scale.linear().range(yrange);
      var y2 = d3.scale.linear().range(yrange);

      var xDomainForced = null, yDomainForced = null;

      var color1 = d3.scale.category20();
      var color2 = d3.scale.category20();

      if(colorSets && colorSets.length) {
        color1.range(colorSets[0]);
        color2.range(colorSets[1] || colorSets[0]);
      } else {
        color1.range(colorPresets.solids);
        color2.range(colorPresets.solids);
      }

      function updateYScale() {
        y1 = y1.copy();
        y2 = y2.copy();
        if(!yDomainForced) {
          y1.domain( dataStore.getY1Domain() || noDomain );
          y2.domain( dataStore.getY2Domain() || noDomain );
          nicerScales.addMargins(y1);
          nicerScales.addMargins(y2);
        } else {
          y1.domain(yDomainForced);
          y2.domain(yDomainForced);
        }

        nicerScales.patchDegenerate(y1);
        nicerScales.patchDegenerate(y2);
        y1.nice();
        y2.nice();
      }
      function updateXScale() {
        x = x.copy();
        //if only one data point, add 3 hours on each side.
        if (dataStore.getXDomain()[0] && dataStore.getXDomain()[0] === dataStore.getXDomain()[1]){
          x.domain([d3.time.hour.offset(dataStore.getXDomain()[0], -3), d3.time.hour.offset(dataStore.getXDomain()[0], 3)]);
        } else {
          if(!xDomainForced) {
            x.domain( dataStore.getXDomain() || noDomain );        
          } else {
            x.domain(xDomainForced);
          }
        }
      }

      function onData() {
        color1.domain( dataStore.getValueFields() || noDomain );
        color2.domain( dataStore.getValueFields() || noDomain );
        updateXScale();
        updateYScale();
      }
      

      var store = fsUtils.Emitter({
        x : function() {
          return x;
        },
        y1 : function() {
          return y1;
        },
        y2 : function() {
          return y2;
        },
        color1 : function() {
          return color1;
        },
        color2 : function() {
          return color2;
        }
      });  

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken ]);
        switch (action.type) {
          case DoubleAxisConstants.FORCE_SCALE_DOMAIN:
            if(action.scale === 'x') {
              xDomainForced = action.domain;
              updateXScale();
            } else if(action.scale === 'y'){
              yDomainForced = action.domain;
              updateYScale();
            } else {
              return;
            }
            break;
          case DoubleAxisConstants.DATA:
            onData();
            break;
          case DoubleAxisConstants.CHANGE_FIELDS:
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;

    };
  });
