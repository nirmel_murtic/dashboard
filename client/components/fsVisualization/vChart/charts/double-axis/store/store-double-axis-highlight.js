'use strict';

angular.module('fs-visulization')
  .factory('DoubleAxisHighlightStore', function(invariant, accessor, fsUtils, binarySearchNearest, searchNearest, DoubleAxisConstants) {

    // jshint newcap: false, unused:false

    var Emitter = fsUtils.Emitter;
    var Constants = DoubleAxisConstants;

    return function stackedChartScaleStore(opts) {
      var dispatcher = opts.dispatcher;
      var scaleStore = opts.scaleStore;
      var dataStore = opts.dataStore;

      // states
      var time = null;
      var values = null;


      var projectY = accessor('y');
      function findHighlights(x, y) {
        var data= dataStore.getData();
        if(!data) {
          return false;
        }
        var key, j;
        var timestamps = dataStore.getTimestamps();
        var xScale = scaleStore.x();

        var getter;
        
        x = xScale.invert(x);

        j = binarySearchNearest(x, timestamps);
        if(j === -1) { // time index not found
          clearHighlights();
          return false;
        }

        time = timestamps[j];

        values = _.map(data, function(d) {
          var series = d.flattened;
          d = series[j];
          return (!d || d.na) ? 'N/A' : projectY(d);
        });
        return true;
      } 

      function clearHighlights() {
        time = null;
        values = null;
      }

      var store = Emitter({
        getHighlight : function() {
          return {
            time : time,
            values : values,
          };
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken, scaleStore.dispatchToken ]);
        switch (action.type) {
          case Constants.HIGHLIGHT_XY:
            findHighlights(action.x, action.y); 
            break;
          case Constants.DEHIGHLIGHT:
            clearHighlights(); 
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;
    };

  });
