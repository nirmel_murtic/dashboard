'use strict';

angular.module('fs-visulization-store')
  .factory('DoubleAxisDataStore', function(invariant, zipWith, accessor, DoubleAxisConstants, partitionSeries, fsUtils) {

    // jshint newcap : false, unused : false

    var noDomain = [0,0];
    var noData = {};
    var empty = [];

    var transformData = partitionSeries('', angular.noop, angular.noop, null); 

    /*
     * {
     *   config : {
     *     timeField,
     *     valueFields,
     *     seriesLabels : {
     *       <series id> : <String>
     *     }
     *   },
     *   data : {
     *     <series id> : {
     *       segments : [
     *         [<datum>],
     *         [<datum>]
     *       ],
     *       naRanges : [
     *         [<Date>, <Date>],    
     *         [<Date>, <Date>] 
     *       ]
     *     }
     *   },
     *   xDomain : [<Date>, <Date>],
     *   yDomain : [<Value>, <Value>]
     * }
     */
    function State(timeField, valueFields, seriesLabels) {
      this.config = {
        timeField : timeField,
        valueFields : valueFields,
        seriesLabels : zipWith(valueFields, seriesLabels, function(key, val) {
          var o = {};
          o[key] = val;
          return o;
        })
      }; 
      this.data = noData;
      this.xDomain = noDomain;
      this.y1Domain = noDomain;
      this.y2Domain = noDomain;
      this.timestamps = empty;
    }
    State.prototype = {
      loadData : function(series) {

        var projectX = accessor(this.config.timeField);
        
        transformData.x(projectX);

        this.originalData = series;
        this.timestamps = _.map(series, projectX);
        this.data = _.reduce(this.config.valueFields, function(data, key, i) {
          transformData
            .y(accessor(key))
            .key(key);

          data[key] = transformData(series);
          return data;
        }, {});

        this.xDomain = d3.extent(series, projectX);

        var key1 = this.config.valueFields[0],
            key2 = this.config.valueFields.length > 1 ? this.config.valueFields[1] : null;

        var points1;
        if(series.length) {
          points1 = _(this.data[key1].segments)
            .flattenDeep()
            .value();
        } else {
          points1 = [];
        }
        
        var points2;
        if(key2 && series.length) {
          points2 = _(this.data[key2].segments)
            .flattenDeep()
            .value();
        } else {
          points2 = [];
        }

        this.y1Domain = d3.extent(points1, accessor('y'));
        this.y2Domain = key2 ? d3.extent(points2, accessor('y')) : noDomain;

      },
       
    };

    return function serieStoreFactory(opts) {

      var dispatcher = opts.dispatcher; 
      var isNA = opts.isNA || function(y) {
        return !_.isNumber(y) || isNaN(y) || y >= Infinity || y <= -Infinity;
      };

      if(isNA) {
        transformData.na(isNA); // specify how we want NA values to be checked
      }

      var state = new State(opts.timeField, opts.valueFields, opts.seriesLabels);

      var store = fsUtils.Emitter({
        getData : function() {
          return state.data;
        }, 
        getXDomain : function() {
          return state.xDomain;
        },
        getXFormat : function() {
          return _.isFunction(opts.xFormat) ? opts.xFormat : null;
        },
        getY1Domain : function() {
          return state.y1Domain;
        },
        getY2Domain : function() {
          return state.y2Domain;
        },
        getY1Format : function() {
          return _.isFunction(opts.y1Format) ? opts.y1Format : null;
        },
        getY2Format : function() {
          return _.isFunction(opts.y2Format) ? opts.y2Format : null;
        },
        getNAChecker : function() {
          return isNA;
        },
        getValueFields : function() {
          return state.config.valueFields;
        },
        getLabels : function() {
          return state.config.seriesLabels;
        },
        getTimestamps : function() {
          return state.timestamps;
        },
        isDouble : function() {
          return state.y2Domain !== noDomain;
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        switch (action.type) {
          case DoubleAxisConstants.FORMATTERS:
            opts.xFormat = action.x || opts.xFormat;
            opts.y1Format = action.y1 || opts.y1Format; 
            opts.y2Format = action.y2 || opts.y2Format; 
            break;
          case DoubleAxisConstants.CHANGE_FIELDS:
            state.config.timeField = action.timeField;
            state.config.valueFields = action.valueFields;
            return;
          case DoubleAxisConstants.DATA:
            state.loadData(action.data);
            if(action.labels) {
              state.config.seriesLabels = action.labels;
            }
            break;
          default:
            return true;
        }
        store.emit('change');
      });

      return store;
    };
  });

