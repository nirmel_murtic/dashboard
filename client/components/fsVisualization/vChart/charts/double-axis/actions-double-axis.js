'use strict';

angular.module('fs-visulization')
  // just contants and a few helper functions
  // per Flux convention
  .constant('DoubleAxisConstants', {
    DATA : 'DATA',
    HIGHLIGHT_XY : 'HIGHLIGHT_XY',
    DEHIGHLIGHT : 'DEHIGHLIGHT',
    CHANGE_FIELDS : 'CHANGE_FIELDS',
    FORMATTERS : 'FORMATTERS',
    FORCE_SCALE_DOMAIN : 'FORCE_SCALE_DOMAIN',
  })
  .factory('DoubleAxisActions', function (DoubleAxisConstants) {
    function dispatchViewAction(dispatcher, action) {
      dispatcher.dispatch({ source : 'view', action : action }); 
    }
    return function msActionsFactory(dispatcher) {
      return {
        clearHighlight : function() {
          dispatchViewAction(dispatcher, {
            type : DoubleAxisConstants.DEHIGHLIGHT,
          }); 
        },
        highlightXY : function(coords) {
          dispatchViewAction(dispatcher, {
            type : DoubleAxisConstants.HIGHLIGHT_XY,
            x : coords[0],
            y : coords[1]
          }); 
        },
        forceTimerange : function(tRange) {
          dispatchViewAction(dispatcher, {
            type : DoubleAxisConstants.FORCE_SCALE_DOMAIN,
            scale : 'x',          
            domain : tRange
          }); 
        },
        changeFields : function(timeField, valueFields) {
          dispatchViewAction(dispatcher, {
            type : DoubleAxisConstants.CHANGE_FIELDS,
            timeField : timeField,
            valueFields : valueFields
          }); 
        }
      };
    };
  });
