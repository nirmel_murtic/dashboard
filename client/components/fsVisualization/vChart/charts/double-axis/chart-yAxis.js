'use strict';

angular.module('fs-visulization')
  .factory('DoubleAxisY1', function(accessor, vChart, vLayer, BrokenAxisLeftLayer, AxisLeftLayer) {

    return vChart.define({
      render : function() {

        var breakHeight = this.props.breakHeight || 20;
        var breakWidth = this.props.breakWidth || 16;

        var LayerType = this.props.yScale.range().length === 3 ? BrokenAxisLeftLayer : AxisLeftLayer;

        return [
          vLayer.render(LayerType, {
            color : this.props.color,
            scale : this.props.yScale,
            ticks : this.props.ticks,
            tickValues : this.props.tickValues,
            tickFormat : this.props.format,
            breakHeight : breakHeight,
            breakWidth : breakWidth
          })

        ];
      }
    });

  })
  .factory('DoubleAxisY2', function(accessor, vChart, vLayer, AxisRightLayer) {

    var empty = [];

    return vChart.define({
      render : function() {
        if(!this.props.yScale) {
          return empty;
        }
        return [
          vLayer.render(AxisRightLayer, {
            color : this.props.color,
            scale : this.props.yScale, 
            tickValues : this.props.tickValues,
            tickFormat : this.props.format,
            ticks : this.props.ticks
          })
        ];
      }
    });

  })
  .factory('XAxis', function(accessor, vChart, vLayer, AxisBottomLayer) {

    return vChart.define({
      render : function() {
        return [
          vLayer.render(AxisBottomLayer, {
            scale : this.props.xScale, 
            color : this.props.color,
            ticks : this.props.ticks
          })
        ];
      }
    });

  });
