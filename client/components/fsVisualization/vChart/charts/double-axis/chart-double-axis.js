'use strict';

angular.module('fs-visulization')
  .factory('DoubleAxisChart', function(accessor, vChart, vLayer, VGridLayer, HGridLayer, LineLayer, AreaLayer, PointsLayer, TooltipLayerAdvanced, GuidelineLayer, BackgroundLayer, DashedLinesLayer, $filter) {


    // var defaultTimeFormat = d3.time.format('%m/%d/%y %I:%M %p');
    var getX = accessor('x');
    var getY = accessor('y');
    // var defaultTimeFormat = d3.time.format('%m/%d/%y %I:%M %p');
    var defaultTimeFormat = $filter('metricDatetime');

    var xNaStart = function(d) {
      return d.NaRange[0].x;
    };
    var yNaStart = function(d) {
      return d.NaRange[0].y;
    };
    var xNaEnd = function(d) {
      return d.NaRange[1].x;
    };
    var yNaEnd = function(d) {
      return d.NaRange[1].y;
    };

    return vChart.define({
      render : function() {
        var data = this.props.data;
        var labels = this.props.labels;
        var keys = this.props.keys;

        var xFormat = this.props.xFormat;
        var y1Format = this.props.y1Format || String;
        var y2Format = this.props.y2Format || String;

        var projectX = this.props.x || getX;
        var projectY = this.props.y || getY;

        var color1 = this.props.color1;
        // var color2 = this.props.color2;

        var xScale = this.props.xScale;
        var y1Scale = this.props.y1Scale;
        var y2Scale = this.props.y2Scale;

        var bgLayer = [
          vLayer.render(BackgroundLayer, {
            xScale : xScale,
            yScale : y1Scale
          })
        ];

        var gridLayers = [
          vLayer.render(HGridLayer, {
            xScale: xScale,
            yScale: y1Scale
          }),
          vLayer.render(VGridLayer, {
            xScale: xScale,
            yScale: y1Scale
          })
        ];

        var firstSeries = true;
        var lineLayers = _(data)
          .map(function(series, seriesKey) {
            var yScale, segments = series.segments;
            if(firstSeries) {
              yScale = y1Scale;
              firstSeries = false;
            } else {
              yScale = y2Scale;
            }
            return _.map(segments, function(segment) {
              return vLayer.render(LineLayer, {
                data : segment,
                xScale : xScale,
                yScale : yScale,
                x : projectX,
                y : projectY,
                width : 2,
                color : color1(seriesKey)
                // key : seriesKey
              });
            });
          })
          .flatten()
          .value();

        firstSeries = true;
        var areaLayers = _(data)
          .map(function(series, seriesKey) {
            var yScale, segments = series.segments;
            if(firstSeries) {
              yScale = y1Scale;
              firstSeries = false;
            } else {
              yScale = y2Scale;
            }
            return _.map(segments, function(segment) {
              return vLayer.render(AreaLayer, {
                data : segment,
                xScale : xScale,
                yScale : yScale,
                x : projectX,
                y : projectY,
                opacity : 0.1,
                color : color1(seriesKey)
                // key : seriesKey
              });
            });
          })
          .flatten()
          .value();

        firstSeries = true;
        var naLayers = _(data)
          .map(function(series, seriesKey) {
            var yScale;
            if(firstSeries) {
              yScale = y1Scale;
              firstSeries = false;
            } else {
              yScale = y2Scale;
            }
            return vLayer.render(DashedLinesLayer, {
              data : series.naRanges,
              xScale : xScale,
              yScale : yScale,
              x1 : xNaStart,
              x2 : xNaEnd,
              y1 : yNaStart,
              y2 : yNaEnd,
              color : color1(seriesKey),
              key : seriesKey
            });
          })
          .value();

        var highlight = this.props.highlight;
        var time = highlight.time;
        var values = highlight.values;
        var texts;
        var xrange, xmid, ttx, tty, tooltipLayer;


        firstSeries = true;
        var r, showPoint;
        var pointsLayers = _(data)
          .map(function(series, seriesKey) {
            var yScale, segments = series.segments;
            if(firstSeries) {
              yScale = y1Scale;
              firstSeries = false;
            } else {
              yScale = y2Scale;
            }
            var firstSeg, x0, x1;

            if(!r) {
              firstSeg = segments[0];
              if(firstSeg && firstSeg.length) {
                x0 = xScale(firstSeg[0].x);
                x1 = xScale(firstSeg[firstSeg.length - 1].x);
                r = Math.min(6, (x1 - x0) / firstSeg.length / 4.2);
                if(r < 4) {
                  r = 4;
                }
                showPoint = (      r < ( (x1 - x0) / firstSeg.length / 3.5 )       );
              } else {
                r = 6;
                showPoint = false;
              }
            }

            return _.map(segments, function(segment){
              return vLayer.render(PointsLayer, {
                data: segment,
                xScale: xScale,
                yScale: yScale,
                // x : projectX,
                // y : projectY,
                color: color1(seriesKey),
                name: seriesKey,
                // key: seriesKey,
                r : r,
                show : showPoint,
                pointHighlight : time ? { time : time } : null
              });
            });
          })
          .flatten()
          .value();

        if(time && values) {
          ttx = xScale( time );

          tty = (y1Scale.range()[0] + y1Scale.range()[1]) / 2;

          xrange = this.props.xScale.range();
          xmid = (xrange[0] + xrange[1]) / 2;

          texts = values.map(function(val, i) {
            return [
              {
                text : '\u25C9',
                color : color1(keys[i])
              },
              {
                text : labels[i],
                color : color1(keys[i]),
                bold : true
              },
              i===0 ? y1Format(val) : y2Format(val)
            ];
          });
          texts.unshift([
            {
              text : 'Date',
              bold : true
            },
            (xFormat || defaultTimeFormat)(time),
            ''
          ]);
          if(this.props.title) {
            texts.unshift([this.props.title, '', '']);
          }

          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: ttx,
              y: tty,
              orient : [ttx > xmid ? 'left' : 'right'],
              texts : texts
            })
          ];
        } else {
          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: 0,
              y: 0,
              texts : [['', '', '']],
              hidden : true,
              orient : ['left']
            })
          ];
        }

        var guidelineLayer;
        if(time) {
          guidelineLayer = [
            vLayer.render(GuidelineLayer, {
              x : ttx,
              yScale : y1Scale
            })
          ];
        } else {
          guidelineLayer = [];
        }

        return bgLayer.concat(gridLayers, lineLayers, areaLayers, naLayers, guidelineLayer, pointsLayers, tooltipLayer);
      }
    });

  });
