'use strict';

angular.module('fs-visulization')
  .factory('LegendChart', function(accessor, vChart, vLayer, VNode) {
    var LegendLayer = vLayer.define({
      render : function() {
        var colorFn = this.props.color;

        var lineHeight = 16;
        var legends = this.props.data.map(function(name, i) {
          return new VNode('g', null, [
            new VNode('line', {
              x1 : 20,
              y1 : i * lineHeight + 8,
              x2 : 100,
              y2 : i * lineHeight + 8,
              stroke : colorFn(name),
              className : 'legent-line' 
            }),
            new VNode('text', {
              contents : name,
              dx : 120,
              dy : (i+1) * lineHeight
            })
          ]);
        });

        return new VNode('g', null, legends);
      }
    });

    var LegendChart = vChart.define({
      render : function() {
        var color = this.props.color;
        var data = this.props.data;
        return [vLayer.render(LegendLayer, { data : data, color : color })];
      }
    });

    return LegendChart;
  });


