'use strict';

angular.module('fs-visulization')
  .factory('chartScaffolding', function() {
    return function scaffolding(element, width, height, margin, xAxisOffset) {
      var sel = d3.select(element);
      var svg;
      if(element.tagName === 'svg') {
        svg = sel;
      } else {
        svg = sel.select('svg').empty() ? sel.html('').append('svg') : svg;
      }

      svg.attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('viewBox', '0 0 ' + width + ' ' + height);

      var chartArea, xAxis, yAxis, leftCover, rightCover;

    
      var chartAreaWidth = width - margin.left - margin.right - xAxisOffset.left;
      var chartAreaHeight = height - margin.top - margin.bottom - xAxisOffset.top; 



      chartArea = svg.select('g.chart-area'); 
      chartArea = chartArea.empty() ? svg.append('g').attr('class', 'chart-area') : chartArea;
      chartArea.attr('transform', 'translate(' + (margin.left + xAxisOffset.left) + ',' + margin.top + ')' );

      leftCover = svg.select('.left-cover');
      leftCover = leftCover.empty() ? svg.append('rect').attr('class', 'chart-cover left-cover') : leftCover;
      leftCover.attr('width', Math.max(margin.left + xAxisOffset.left - 13, 0)).attr('height', chartAreaHeight + margin.top + xAxisOffset.top);

      rightCover = svg.select('.right-cover');
      rightCover = rightCover.empty() ? svg.append('rect').attr('class', 'chart-cover right-cover') : rightCover;
      rightCover.attr('width', Math.max(0, margin.right - 12))
        .attr('transform', 
          'translate(' + (margin.left + xAxisOffset.left + chartAreaWidth + 12) + ',0)')
        .attr('height', chartAreaHeight + margin.top + xAxisOffset.top);

      yAxis = svg.select('g.y.axis');
      yAxis = yAxis.empty() ? svg.append('g').attr('class', 'y axis') : yAxis;
      yAxis.attr('transform', 'translate(' + (margin.left) + ',' + margin.top + ')' );

      xAxis = svg.select('g.x.axis');
      xAxis = xAxis.empty() ? svg.append('g').attr('class', 'x axis') : xAxis;
      xAxis.attr('transform', 'translate(' + (margin.left + xAxisOffset.left) + ',' + (margin.top + chartAreaHeight + xAxisOffset.top) + ')' );

      return {
        chart : chartArea,
        xAxis : xAxis,
        yAxis : yAxis,
        chartSize : { width: chartAreaWidth, height: chartAreaHeight },
      };
    };
  });
