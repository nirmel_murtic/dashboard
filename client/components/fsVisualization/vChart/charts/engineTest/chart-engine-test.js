'use strict';

angular.module('fs-visulization')
  .factory('ChartEngineTest', function(vChart, vLayer, OLayer) {

    return vChart.define({
      render : function() {
        return [vLayer.render(OLayer, { r : this.props.r })];
      }
    });
  })
  .directive('fsChartEngineTest', function(ChartEngineTest, chartScaffolding, BatchRender, makeChart) {
    return {
      restrict : 'EA',
      scope : false,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>',
                 ].join(''), 
      link : function(scope, element) {
      
        var aspectRatio = 1;
        var config = {
          width : 800,
          margin : {
            top : 25,
            bottom : 35,
            left : 35,
            right: 15 
          },
          xAxisOffset : {
            top: 5,
            left : 5
          }
        };
        var svg = element.find('svg');
        svg.parent().attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
        var containers = chartScaffolding(svg[0], config.width, config.width / aspectRatio, config.margin, config.xAxisOffset); 
        var renderChart = makeChart(containers.chart.node());
        var batchRenderer = new BatchRender(function() {
          renderChart(ChartEngineTest, { r : scope.radius });
        });
        scope.$watch('radius', function(r) {
          if(r >= 1) {
            batchRenderer.render();
          } 
        });
      }
    };
  
  });
