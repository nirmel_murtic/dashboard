'use strict';

angular.module('fs-visulization-store')
  .factory('MultiSeriesDataStore', function(invariant, zipWith, accessor, dataExtent, partitionSeries, MSConstants, pointInPolygon, fsUtils) {

    // jshint newcap : false, unused : false

    var noDomain = [0,0];
    var noData = {};
    var empty = [];

    var transformData = partitionSeries('', angular.noop, angular.noop, null); 

    /*
     * {
     *   config : {
     *     timeField,
     *     valueFields,
     *     seriesLabels : {
     *       <series id> : <String>
     *     }
     *   },
     *   data : {
     *     <series id> : {
     *       segments : [
     *         [<datum>],
     *         [<datum>]
     *       ],
     *       naRanges : [
     *         [<Date>, <Date>],    
     *         [<Date>, <Date>] 
     *       ],
     *       flattened : [{
     *         x,
     *         y,
     *         name,
     *         na
     *       }]
     *     }
     *   },
     *   xDomain : [<Date>, <Date>],
     *   yDomain : [<Value>, <Value>]
     * }
     */
    function State(timeField, valueFields, seriesLabels) {
      this.config = {
        timeField : timeField,
        valueFields : valueFields,
        seriesLabels : zipWith(valueFields, seriesLabels, function(key, val) {
          var o = {};
          o[key] = val;
          return o;
        })
      }; 
      this.data = noData;
      this.xDomain = noDomain;
      this.yDomain = noDomain;
      this.timestamps = empty;
    }
    State.prototype = {
      loadData : function(series) {

        series = series || [];
        var projectX = accessor(this.config.timeField);
        
        transformData.x(projectX);

        this.originalData = series;
        this.timestamps = _.map(series, projectX);
        this.data = _.reduce(this.config.valueFields, function(data, key, i) {
          transformData
            .y(accessor(key))
            .key(key);

          data[key] = transformData(series);
          return data;
        }, {});

        this.xDomain = d3.extent(series, projectX);

        var allPoints = _(this.data)
          .toArray()
          .map(accessor('segments'))
          .flattenDeep()
          .value();

        this.yDomain = d3.extent(allPoints, accessor('y'));
      },
       
    };

    return function serieStoreFactory(opts) {

      var dispatcher = opts.dispatcher; 
      var isNA = opts.isNA || function(y) {
        return !_.isNumber(y) || isNaN(y) || y >= Infinity || y <= -Infinity;
      };

      if(isNA) {
        transformData.na(isNA); // specify how we want NA values to be checked
      }

      var state = new State(opts.timeField, opts.valueFields, opts.seriesLabels);

      var store = fsUtils.Emitter({
        getData : function() {
          return state.data;
        }, 
        getXDomain : function() {
          return state.xDomain;
        },
        getXFormat : function() {
          return opts.xFormat;
        },
        getYDomain : function() {
          return state.yDomain;
        },
        getYFormat : function() {
          return opts.yFormat;
        },
        getNAChecker : function() {
          return isNA;
        },
        getValueFields : function() {
          return state.config.valueFields;
        },
        getSeriesLabels : function() {
          return state.config.seriesLabels;
        },
        getTimestamps : function() {
          return state.timestamps;
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        switch (action.type) {
          case MSConstants.DATA:
            state.loadData(action.data);
            break;
          case MSConstants.VALUE_FIELDS:
            state.config.valueFields = action.fields;
            state.config.seriesLabels = zipWith(action.fields, action.labels, function(key, val) {
              var o = {};
              o[key] = val;
              return o;
            });
            break;
          default:
            return true;
        }
        store.emit('change');
      });

      return store;
    };
  });

