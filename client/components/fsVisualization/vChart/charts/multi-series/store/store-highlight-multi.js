'use strict';

angular.module('fs-visulization')
  .factory('MultiSeriesHighlightStore', function(invariant, accessor, pointInPolygon, fsUtils, binarySearchNearest, searchNearest, MSConstants) {

    // jshint newcap: false, unused:false

    var Emitter = fsUtils.Emitter;
    var Constants = MSConstants;

    return function stackedChartScaleStore(opts) {
      var dispatcher = opts.dispatcher;
      var scaleStore = opts.scaleStore;
      var dataStore = opts.dataStore;

      // states
      var row = null;
      var colKey = null;
      var values = null;


      var projectY = accessor('y');
      function findHighlights(x, y) {
        var data= dataStore.getData();
        if(!data) {
          return false;
        }
        var key, j;
        var timestamps = dataStore.getTimestamps();
        var xScale = scaleStore.x();
        var yScale = scaleStore.y();

        var getter;
        
        x = xScale.invert(x);
        y = yScale.invert(y); 

        j = binarySearchNearest(x, timestamps);
        if(j === -1) { // time index not found
          clearHighlights();
          return false;
        }

        row = timestamps[j];
        // find the nearest y
        colKey = _.reduce(data, function(currKey, series, seriesKey) {
          var datum = series.flattened[j];
          var dist = currKey.dist;
          if(datum) {
            if(!datum.na && Math.abs(y - datum.y) < dist) {
              currKey.key = seriesKey;
              currKey.dist = Math.abs(y - datum.y);
            } else if (datum.na && !currKey.key) {
              currKey.key = seriesKey;
            }
          }
          return currKey;
        }, {
          key : null,
          dist : Infinity
        }).key;


        values = _.map(data, function(d) {
          var series = d.flattened;
          d = series[j];
          return (!d || d.na) ? 'N/A' : projectY(d);
        });
        return true;
      } 

      function clearHighlights() {
        row = null;
        colKey = null;
        values = null;
      }

      var store = Emitter({
        getHighlight : function() {
          return {
            time : row,
            values : values,
            seriesKey : colKey
          };
        }
      });

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken, scaleStore.dispatchToken ]);
        switch (action.type) {
          case Constants.HIGHLIGHT_XY:
            findHighlights(action.x, action.y); 
            break;
          case Constants.DEHIGHLIGHT:
            clearHighlights(); 
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;
    };

  });
