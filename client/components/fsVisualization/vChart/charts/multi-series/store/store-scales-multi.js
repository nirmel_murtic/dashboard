'use strict';

angular.module('fs-visulization')
  .factory('MultiSeriesScaleStore', function(invariant, accessor, pointInPolygon, fsUtils, binarySearchNearest, MSConstants, colorPresets, nicerScales) {

    var noDomain = [0,0];
    return function scaleStoreFactory(opts) {
      var dispatcher = opts.dispatcher;
      var dataStore = opts.dataStore;
      var colorSets = opts.colorSets;
      var yrange = [opts.height, 0];

      var x = d3.time.scale().range([0, opts.width]);
      var y = d3.scale.linear().range(yrange);

      var xDomainForced = null, yDomainForced = null;

      var color1 = d3.scale.category20();
      var color2 = d3.scale.category20();

      if(colorSets && colorSets.length) {
        color1.range(colorSets[0]);
        color2.range(colorSets[1] || colorSets[0]);
      } else {
        color1.range(colorPresets.solids);
        color2.range(colorPresets.solids);
      }

      function updateYScale() {
        y = y.copy();
        if(!yDomainForced) {
          y.domain( dataStore.getYDomain() || noDomain );
          nicerScales.addMargins(y);
        } else {
          y.domain(yDomainForced);
        }

        nicerScales.patchDegenerate(y);
        y.nice();
      }
      function updateXScale() {
        x = x.copy();
        // if user selects range such that only one datapoint appears, it default to adding one day on either side
        if (dataStore.getXDomain()[0] && dataStore.getXDomain()[0] === dataStore.getXDomain()[1]){
          x.domain([d3.time.day.offset(dataStore.getXDomain()[0], -1), d3.time.day.offset(dataStore.getXDomain()[0], 1)]);
        } else {
          if(!xDomainForced) {
            x.domain( dataStore.getXDomain() || noDomain );        
          } else {
            x.domain(xDomainForced);
          }
        }
      }

      function onData() {
        color1.domain( dataStore.getValueFields() || noDomain );
        color2.domain( dataStore.getValueFields() || noDomain );
        updateXScale();
        updateYScale();
      }
      

      var store = fsUtils.Emitter({
        x : function() {
          return x;
        },
        y : function() {
          return y;
        },
        color1 : function() {
          return color1;
        },
        color2 : function() {
          return color2;
        }
      });  

      store.dispatchToken = dispatcher.register(function(payload) {
        var action = payload.action;
        dispatcher.waitFor([ dataStore.dispatchToken ]);
        switch (action.type) {
          case MSConstants.FORCE_SCALE_DOMAIN:
            if(action.scale === 'x') {
              xDomainForced = action.domain;
              updateXScale();
            } else if(action.scale === 'y'){
              yDomainForced = action.domain;
              updateYScale();
            } else {
              return;
            }
            break;
          case MSConstants.DATA:
            onData();
            break;
          default:
            return; 
        }
        store.emit('change');
      });

      return store;

    };
  });
