'use strict';

angular.module('fs-visulization')
  .directive('fsVisMultiseries', function(
    $parse, 
    zipWith, 
    accessor, 
    MultiSeriesDataStore, 
    MultiSeriesScaleStore, 
    MultiSeriesHighlightStore, 
    MultiSeriesChart, 
    makeChart, 
    Dispatcher, 
    MSConstants, 
    MSActions, 
    chartScaffolding, 
    BatchRender,
    eliminateDuplicateTicks) {
    // jshint -W026, latedef: false, newcap: false
    return {
      restrict : 'EA',
      priority : 10,
      require : 'fsVisConfig',
      scope : false,
      template : [
                   '<div class="chart-control-before"></div>',
                   '<div class="chart-wrapper">',
                     '<svg class=\'chart\'></svg>',
                   '</div>',
                   '<div class="chart-control-after"></div>',
                 ].join(''), 
      transclude: true,
      compile : function() {
        return function multiSeriesPostLink(scope, element, attrs, configCtrl, transclude) {
          var dataGetter = $parse(attrs.data);
          scope.getData = dataGetter.bind(null, scope);

          var timeRange = $parse(attrs.timeRange || '');
          scope.timeRange = timeRange.bind(null, scope);

          var title = configCtrl.chartTitle();
          // var config = lookupConfig();
          var timeField = configCtrl.timeField();
          var valueFields = configCtrl.valueFields();
          var labels = configCtrl.labels();
          var colors = configCtrl.colors();
          var xFormat = configCtrl.formatX();
          // formatter for tooltip
          var yFormat = configCtrl.yFormat();

          // formatter for axis
          var y1Format = configCtrl.y1Format() || yFormat;

          //

          var isNA = $parse(attrs.isNa)(scope) || null;
          var aspectRatio = $parse(attrs.aspectRatio)(scope) || null;
          // find parent container width 
          var containerWidth = $parse(attrs.width)(scope) ||  
            parseInt(
              window.getComputedStyle(element.parent()[0], null).getPropertyValue('width'), 
            10);
          // if no config for aspect ratio figure out a good one responsively
          if(!aspectRatio) {
            if (containerWidth > 117 * 16) {
              aspectRatio = 3;
            } else if (containerWidth > 75 * 16) {
              aspectRatio = 5 / 2;
            } else if (containerWidth > 60 * 16) {
              aspectRatio = 16 / 9;
            } else {
              aspectRatio = 4 / 3;
            }
          }
          // scaffolding the chart  
          var height = containerWidth / aspectRatio;
          var margin = $parse(attrs.margin)(scope) || {
            top : 25,
            bottom : 60,
            left : 55,
            right: 55 
          };
          var xAxisOffset = $parse(attrs.xAxisOffset)(scope) || {
            top: 25,
            left : 25
          };
          var svg = element.find('svg');
          svg.parent().attr('style', 'padding-bottom: ' + 1/aspectRatio * 100 + '%');
          var containers = chartScaffolding(svg[0], containerWidth, height, margin, xAxisOffset); 
          // finish scaffolding the chart
          
          var dispatcher = new Dispatcher();
          var actionCreator = MSActions(dispatcher);

          var dataStore = MultiSeriesDataStore({
            dispatcher : dispatcher,
            timeField : timeField,
            valueFields : valueFields,
            seriesLabels : labels,
            xFormat : xFormat || _.identity,
            yFormat : yFormat || _.identity,
            isNA : isNA
          });
          var scaleStore = MultiSeriesScaleStore({
            dispatcher : dispatcher,
            dataStore : dataStore,
            width : containers.chartSize.width,
            height : containers.chartSize.height,
            colorSets : colors ? [colors] : null
          });
          var highlightStore = MultiSeriesHighlightStore({
            dispatcher : dispatcher,
            dataStore : dataStore,
            scaleStore : scaleStore
          });
          if(__DEV__ && title==='Cost per Thousand Impressions (CPM)') {
            window.Dstore = dataStore;
            window.Sstore = scaleStore;
            window.Hstore = highlightStore;
          }
          // once scales store are setuop, load scales into d3 svg axis
          var xAxis = d3.svg.axis().orient('bottom');
          var yAxis = d3.svg.axis().orient('left');

          scope.actionCreator = actionCreator;
          scope.dataStore = dataStore;
          scope.scaleStore = scaleStore;
          scope.legendData = [];
          // create a chart managed by virtual chart on specified container containers.chart
          // which at this point is an empty <g> tag positioned right (margins and such taken care of)
          var renderChart = makeChart(containers.chart.node(), {
            transition: configCtrl.transition
          }); 
          var projectX = accessor('x');
          var projectY = accessor('y');
          var batchRenderer = new BatchRender(function() {
            // get scales from store
            var x = scaleStore.x();
            var y = scaleStore.y();
            // accessors are getter functions that extract values from datum
            // the data!
            var data = dataStore.getData();
            if(!data) {
              return;
            }
            // update d3 axis with scales in case they change
            xAxis.scale(x);
            yAxis.scale(y);
            if(xFormat) {
              xAxis.tickFormat(xFormat);
            }
            var yTicks;
            yAxis.tickValues(null);
            if(y1Format) {
              yTicks = eliminateDuplicateTicks(y, y1Format);
              if(yTicks !== false) {
                yAxis.tickValues(yTicks);
              }

              yAxis.tickFormat(y1Format);
            }
            // dom operations here, managed by d3
            containers.xAxis.call(xAxis);
            containers.yAxis.call(yAxis);
            // use vChart engine to do vChart diff, and apply patches to real DOM if anything changed
            
            var labels = dataStore.getSeriesLabels().map(function(o) {
              return _.values(o)[0];
            });

            renderChart(MultiSeriesChart, {
              data : data,
              title : title,
              xScale : x,
              yScale : y,
              xFormat : xFormat || xAxis.tickFormat() || String,
              yFormat : yFormat || yAxis.tickFormat() || String,
              y1Format : y1Format || yAxis.tickFormat() || String,
              x : projectX,
              y : projectY,
              labels : labels,
              keys : dataStore.getValueFields(),
              color1 : scaleStore.color1(),
              highlight : highlightStore.getHighlight()
              // highlights : highlightStore.getHighlight(),
              // highlightedValues : highlightStore.getHighlightedValues()
            });
          });
          dataStore.on('change', batchRenderer.render);
          scaleStore.on('change', batchRenderer.render);
          highlightStore.on('change', batchRenderer.render);
          scaleStore.on('change', function() {
            scope.legendData = zipWith(dataStore.getValueFields(), dataStore.getSeriesLabels(), scaleStore.color1().range(), function(key, labelObject, color) {
              return {
                key : key,
                name : labelObject[key],
                color: color.toString()
              };
            }); 
          });
          //
          // throttle mouse move at ~30 fps
          var mouseMoveCallback = _.throttle(function(coords) {
            // dispatch a highlight action for given mouse coordinates
            actionCreator.highlightXY(coords);
          }, 33);
          containers.chart.on('mousemove', function() {
            var coords = d3.mouse(this);
            mouseMoveCallback(coords);
          });
          svg.on('mouseleave', function() {
            actionCreator.clearHighlight();
          });

          scope.$watchGroup([configCtrl.valueFields, 'getData()', configCtrl.labels], function(vals) {
            var newFields = vals[0];
            var newData = vals[1];
            var newLabels = vals[2];
            if(!_.isEqual(newFields, valueFields) || !_.isEqual(newLabels, labels)) {
              dispatcher.dispatch({
                source : 'extrenal',
                action : {
                  type : MSConstants.VALUE_FIELDS,
                  fields : newFields,
                  labels : newLabels
                }
              }); 
              valueFields = newFields;
              labels = newLabels;
            }
            dispatcher.dispatch({
              source : 'extrenal',
              action : {
                type : MSConstants.DATA,
                data : newData
              }
            }); 
            var range = scope.timeRange();
            if(range) {
              actionCreator.forceTimerange(range);
            }
          });

          scope.$watchGroup([configCtrl.formatX, configCtrl.yFormat, configCtrl.y1Format, configCtrl.chartTitle], function(formatters) {
            xFormat = formatters[0];
            yFormat = formatters[1];
            y1Format = formatters[2] || yFormat;
            title = formatters[3];
            batchRenderer.render();
          });

          // put directives contents into transcluded elements
          // specify attribute 'append-after' if you want the html to display **after** the chart/svg
          transclude(scope, function(tcEl) {
            var len = tcEl.length;
            var before = element.find('.chart-control-before')[0];
            var after = element.find('.chart-control-after')[0];
            var container;
            for(var i=0; i<len; i++) {
              if(tcEl[i] && tcEl[i].hasAttribute && tcEl[i].hasAttribute('append-after')) {
                container = after;
              } else {
                container = before;
              }
              container.appendChild(tcEl[i]);
            }
          });

        };
      }
    };
  });
