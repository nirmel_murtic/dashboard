'use strict';

angular.module('fs-visulization')
  .factory('MultiSeriesChart', function(rangeSize, accessor, vChart, vLayer, LineLayer, AreaLayer, VGridLayer, HGridLayer, PointsLayer, BackgroundLayer, TooltipLayerAdvanced, GuidelineLayer, DashedLinesLayer, HorizontalLineLayer, $filter) {

    //jshint newcap: true, latedef: true
    
    var getX = accessor('x');
    var getY = accessor('y');
    var defaultTimeFormat = $filter('metricDatetime');

    var xNaStart = function(d) {
      return d.NaRange[0].x;
    };
    var yNaStart = function(d) {
      return d.NaRange[0].y;
    };
    var xNaEnd = function(d) {
      return d.NaRange[1].x;
    };
    var yNaEnd = function(d) {
      return d.NaRange[1].y;
    };

    return vChart.define({

      // MultiSeriesChart's render function
      render: function() {

        var data = this.props.data;

        // var xFormat = this.props.xFormat;
        var yFormat = this.props.yFormat;
        var y1Format = this.props.y1Format;

        var projectX = this.props.x || getX;
        var projectY = this.props.y || getY;

        var color1 = this.props.color1;
        // var color2 = this.props.color2;

        var xScale = this.props.xScale;
        var yScale = this.props.yScale;
        var title = this.props.title;
        var keys = this.props.keys;

        var bgLayer = [
          vLayer.render(BackgroundLayer, {
            xScale : xScale,
            yScale : yScale
          })
        ];

        var gridLayers = [
          vLayer.render(HGridLayer, {
            xScale: xScale,
            yScale: yScale
          }),
          vLayer.render(VGridLayer, {
            xScale: xScale,
            yScale: yScale
          })
        ];

        var baselineLayer;

        if(yScale.domain()[0] < 0 && yScale.domain()[1] > 0) {
          baselineLayer = [
            vLayer.render(HorizontalLineLayer, {
              xScale : xScale,
              y : yScale(0)
            })  
          ];
        } else {
          baselineLayer = [];
        }

        var lineLayers = _(data)
          .map(function(series, seriesKey) {
            var segments = series.segments;
            return _.map(segments, function(segment) {
              return vLayer.render(LineLayer, {
                data : segment,
                xScale : xScale,
                yScale : yScale,
                x : projectX,
                y : projectY,
                width : 1,
                color : color1(seriesKey) 
              });
            });
          })
          .flatten()
          .value();

        var areaLayers = _(data)
          .map(function(series, seriesKey) {
            var segments = series.segments;
            return _.map(segments, function(segment) {
              return vLayer.render(AreaLayer, {
                data : segment,
                xScale : xScale,
                yScale : yScale,
                x : projectX,
                y : projectY,
                opacity : 0.02,
                color : color1(seriesKey) 
              });
            });
          })
          .flatten()
          .value();

        var naLayers = _(data)
          .map(function(series, seriesKey) {
            return vLayer.render(DashedLinesLayer, {
              data : series.naRanges,
              xScale : xScale,
              yScale : yScale,
              x1 : xNaStart,
              x2 : xNaEnd,
              y1 : yNaStart,
              y2 : yNaEnd,
              color : color1(seriesKey),
              key : seriesKey
            });
          })
          .value();

        var highlight = this.props.highlight;       
        var time = highlight.time;
        var values = highlight.values; 
        var seriesKeys = _.keys(data);
        var lines;
        var xrange, xmid, ttx, tty, tooltipLayer;
        var labels = this.props.labels;

        var pointsLayers = _(data)
          .map(function(series, seriesKey) {
            var segments = series.segments;
            var r, showPoint;
            var firstSeg, x0, x1;

            if(!r) {
              firstSeg = segments[0];
              if(firstSeg && firstSeg.length) {
                x0 = xScale(firstSeg[0].x);
                x1 = xScale(firstSeg[firstSeg.length - 1].x);
                r = Math.min(6, (x1 - x0) / firstSeg.length / 4.2);
                if(r < 4) {
                  r = 4;
                }
                showPoint = (      r < ( (x1 - x0) / firstSeg.length / 3.5 )       );
              } else {
                r = 4;
                showPoint = false;
              }
            }

            var data = _.flatten(segments);
            return vLayer.render(PointsLayer, {
              data: data,
              xScale: xScale,
              yScale: yScale,
              // x : projectX,
              // y : projectY,
              color: color1(seriesKey),
              name: seriesKey,
              key: seriesKey,
              r : r,
              show : showPoint,
              pointHighlight : time ? { time : time } : null
            });
          })
          .value();

        if(time) {
          ttx = xScale( time );

          tty = (yScale.range()[0] + yScale.range()[1]) / 2;

          xrange = this.props.xScale.range(); 
          xmid = (xrange[0] + xrange[1]) / 2;
          if(title) {
            lines = [
              [
                'Metric',
                title 
              ],
              [
                {
                  text : 'Date',
                  bold : true
                },
                defaultTimeFormat(time)
              ]
            ];
          } else {
            lines = [
              [
                'Date',
                defaultTimeFormat(time)
              ]
            ];
          
          }
          lines = _.reduce(values, function(lines, val, i) { 
            var fmt = yFormat;
            // dirty hack specify for fanGrowth chart
            if (seriesKeys[i] === 'fanGrowth') {
              fmt = y1Format;  
            }
            lines.push([
              {
                text : labels[i],
                color : color1(keys[i]),
                bold : true
              },
              // dirty hack for just fanGrowth chart
              fmt(val)
            ]);
            return lines;
          }, lines);

          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: ttx,
              y: tty,
              orient : [ttx > xmid ? 'left' : 'right'],
              texts : lines
            })
          ];
        } else {
          tooltipLayer = [
            vLayer.render(TooltipLayerAdvanced, {
              x: 0,
              y: 0,
              texts : [['', '']],
              hidden : true,
              orient : ['left']
            })
          ];
        }
        var guidelineLayer;
        if(time) {
          guidelineLayer = [
            vLayer.render(GuidelineLayer, {
              x : ttx,
              yScale : yScale
            })
          ];
        } else {
          guidelineLayer = [];
        }

        return bgLayer.concat(gridLayers, baselineLayer, lineLayers, areaLayers, naLayers, guidelineLayer, pointsLayers, tooltipLayer); 

      }
    });    

  });
