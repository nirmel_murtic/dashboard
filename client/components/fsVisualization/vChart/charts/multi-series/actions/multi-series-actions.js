'use strict';

angular.module('fs-visulization')
  // just contants and a few helper functions
  // per Flux convention
  .constant('MSConstants', {
    DATA : 'DATA',
    VALUE_FIELDS : 'VALUE_FIELDS',
    // CLEAR_ALL_SERIES : 'CLEAR_ALL_SERIES',
    // CLEAR_ALL_HIGHLIGHTS : 'CLEAR_ALL_HIGHLIGHTS',
    HIGHLIGHT_XY : 'HIGHLIGHT_XY',
    DEHIGHLIGHT : 'DEHIGHLIGHT',
    FORCE_SCALE_DOMAIN : 'FORCE_SCALE_DOMAIN',
    // HIGHLIGHT_POINT : 'HIGHLIGHT_POINT',
    // HIGHLIGHT_SERIES: 'HIGHLIGHT_SERIES',
    // DEHIGHLIGHT_POINT : 'DEHIGHLIGHT_POINT',
    // DEHIGHLIGHT_SERIES: 'DEHIGHLIGHT_SERIES',
    // HIGHLIGHT_NARANGE : 'HIGHLIGHT_NARANGE',
    // ADD_SERIES : 'ADD_SERIES',
    // REMOVE_SERIES : 'REMOVE_SERIES',
    // SHOW_SERIES : 'SHOW_SERIES',
    // HIDE_SERIES : 'HIDE_SERIES',
    // SET_VORONOI : 'SET_VORONOI'
  })
  .factory('MSActions', function (MSConstants) {
    function dispatchViewAction(dispatcher, action) {
      dispatcher.dispatch({ source : 'view', action : action }); 
    }
    return function msActionsFactory(dispatcher) {
      return {
        clearHighlight : function() {
          dispatchViewAction(dispatcher, {
            type : MSConstants.DEHIGHLIGHT,
          }); 
        },
        highlightXY : function(coords) {
          dispatchViewAction(dispatcher, {
            type : MSConstants.HIGHLIGHT_XY,
            x : coords[0],
            y : coords[1]
          }); 
        },
        forceTimerange : function(tRange) {
          dispatchViewAction(dispatcher, {
            type : MSConstants.FORCE_SCALE_DOMAIN,
            scale : 'x',          
            domain : tRange
          }); 
        }
      };
    };
  });
