'use strict';

angular.module('fs-visulization')
  .service('BatchRender', function(invariant) {

    function BatchRender(render) {
      if(__DEV__) {
        invariant(_.isFunction(render), 'Batcher rendering must be initialized with a render function.');
      }

      this.renderSync = function() {
        this.tid = null;
        render();
      }.bind(this);

      this.render = this.render.bind(this);
      this.tid = null;
    }

    BatchRender.prototype = {
      render : function() {
        if(!this.tid) {
          this.tid = window.requestAnimationFrame(this.renderSync); 
        } 
      },
      abort : function() {
        if(this.tid) {
          window.cancelAnimationFrame(this.tid);
        }
      }
    };

    return BatchRender;
  
  });
