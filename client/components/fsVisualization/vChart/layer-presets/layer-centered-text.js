'use strict';

angular.module('fs-visulization')
  .factory('CenteredTextLayer', function(vLayer, VNode) {
    // message does not go on to a second line, so keep the message short
    return vLayer.define({
      render : function() {
        var xScale = this.props.xScale,
           yScale = this.props.yScale,
           color = this.props.color,
           message = this.props.message;
  
        var xrange;
        if(xScale.rangeBand) {
          xrange = xScale.rangeExtent();
        } else {
          xrange = xScale.range();
        }
        var yrange = yScale.range();

        var xMid = (xrange[1] - xrange[0]) / 2;
        var yMid = (yrange[0] - yrange[1]) / 2;

        //Background rectangle is ghetto responsive, based on average character width.
        var messageSize = (message.length) * 6 + 100;

        return new VNode('g', null, [
          new VNode('rect', {
            rx : 3,
            ry : 3,
            x : xMid - messageSize/2,
            y : yMid - 40,
            width : messageSize,
            height : 70,
            'text-anchor' : 'middle',
            fill : 'rgb(218, 219, 219)', // ColorDefs.PaleDark
            className : 'centerRect'
          }),
          new VNode('text', {
            contents : message,
            'font-family' : '"Source Sans Pro", sans-serif',
            x : xMid,
            y : yMid,
            'text-anchor' : 'middle',
            fill : color || '#e3e3e3',
            className : 'centerText'
          })
        ]);
      }
    });
  });
