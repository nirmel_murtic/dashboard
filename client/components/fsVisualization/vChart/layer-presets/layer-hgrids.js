'use strict';

angular.module('fs-visulization')
  .factory('HGridLayer', function(vLayer, VNode) {

    return vLayer.define({
    
      /**
       * render
       *
       * this layer types take xScale and yScale (both need to be d3 scales)
       * and create <line> vNodes from yScale's ticks() values
       *  
       * @return {VNode}
       */
      render : function() {
        var x = this.props.xScale, y = this.props.yScale;

        var ys = y.ticks().map(y);

        var xrange; 
        if(x.rangeExtent) {
          xrange = x.rangeExtent();
        } else {
          xrange = x.range();
        }
        var x1 = xrange[0], x2 = xrange[1];

        var lastMajorGridY;
        var grids = _.reduce(ys, function(lines, t, i) {
          if(i === 0 || i === ys.length - 1) {
            return lines;
          }
          var isMajorTick;
       
          if(i === 1) {
            isMajorTick = true;
            lastMajorGridY = t;
          } else {
            if(Math.abs(t - lastMajorGridY) > 60) {
              lastMajorGridY = t;
              isMajorTick = true;
            } else {
              isMajorTick = false;
            }
          }

          var gridMajor = new VNode('line', {
              className: 'grid',
              y1: t,
              y2: t,
              x1: x1,
              x2: x2,
              stroke : isMajorTick ? '#fff' : '#ebebeb',
              'stroke-width' : 1
          }, []); 

          lines.push(gridMajor);

          var gridMinor;
          if(i > 0) {
            gridMinor = new VNode('line', {
                className: 'grid',
                y1: (t + ys[i-1]) / 2,
                y2: (t + ys[i-1]) / 2,
                x1: x1,
                x2: x2,
                stroke : '#ebebeb',
                'stroke-width' : 1
            }, []); 
            lines.push(gridMinor);
          }

          return lines;
        }, []);

        return new VNode('g', { className : 'grids' }, grids);
      }
    });

  });
