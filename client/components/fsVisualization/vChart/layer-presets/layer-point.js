'use strict';

angular.module('fs-visulization')
  .factory('PointsLayer', function(vLayer, VNode) {

    return vLayer.define({

      /**
       * render
       *
       * this layer type looks like, in jsx-like syntax:
       * <g>
       *   <circle cx={ this.props.xScale(d.x) } cy={ this.props.yScale(d.y) } r={ this.props.r || 6 } fill={this.props.color} className={
       *   'point' + <if not highlighted>'hidden'}></circle>
       *   ...
       * </g>
       *
       * PointsLayer takes input data, transform it with xScale and yScale, and creates <circle> VNode.
       *
       * the fill attribute is controlled by props.color, and by default all points are hidden unless highlighted
       *
       * @return {VNode}
       */
      render : function() {
        var data = this.props.data,
           xScale = this.props.xScale,
           yScale = this.props.yScale,
           x = this.props.x,
           y = this.props.y,
           color = this.props.color || '#bfbfff',
           pointHL = this.props.pointHighlight,
           show = this.props.show,
           r = this.props.r;

        var x0, xn, pointsPixelDistance;
        if(data.length) {
          x0 = x ? x(data[0]) : data[0].x;
          xn = x ? x(data[data.length - 1]) : data[data.length - 1];
        } else {
          x0 = 0;
          xn = 0;
        }
        pointsPixelDistance = (xScale(xn) - xScale(x0)) / data.length;
        pointsPixelDistance = pointsPixelDistance > 0 ? pointsPixelDistance : -pointsPixelDistance;
        if(data.length === 1) {
          show = true;
          r = r || 6;
        } else if(typeof r === 'undefined') {
          r = Math.round(pointsPixelDistance / 4.2); 
          r = r > 6 ? 6 : r;
          if(r < 4) {
            r = 4;
            show = (typeof show === 'undefined') ? false : show;
          } else {
            show = (typeof show === 'undefined') ? true : show;
          }
        } else {
          show = (typeof show === 'undefined') ? (pointsPixelDistance > r * 4.2) : show;
        }

        return new VNode('g', null, _.map(data, function(d, k) {
          var hl = pointHL && (pointHL.index === k || pointHL.time === (x ? x(d) : d.x));
          var cx = xScale(x ? x(d) : d.x);
          var cy = yScale(y ? y(d) : d.y);

          var xrange = xScale.range();
          var yrange = yScale.range();

          var inChart = cx >= xrange[0] && 
            cx <= xrange[1] &&
            cy >= yrange[1] && 
            cy <= yrange[0];

          var circle1 = new VNode('circle', {
              cx : cx,
              cy : cy,
              noTransition : inChart ? (!show) : 2,
              r : hl ? r * 2 : r,
              fill : '#fff', 
              'stroke' : color,
              'stroke-width' : hl ? 2 : 1,
              // className : 'point highlight ' + (d.name || '') + ((show || hl) ? '' : ' hidden')
          }, []); 

          var circle2 = new VNode('circle', {
              cx : cx,
              cy : cy,
              noTransition : inChart ? (!show) : 2, 
              r : hl ? r+1 : r-1,
              fill : hl ? color : '#fff', 
              'stroke' : hl ? color : d3.rgb(color).darker(),
              'stroke-width' : hl ? 0 : 1,
              // className : 'point ' + (d.name || '') + (show ? '' : ' hidden')
          }, []); 

          return new VNode('g', {
            className : inChart && (show || hl) ? '' : 'hidden'
          }, [ circle1, circle2 ]);

        }));
      }
    });
  });
