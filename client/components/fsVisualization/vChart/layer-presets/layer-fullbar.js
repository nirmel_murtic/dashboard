'use strict';

angular.module('fs-visulization')
  .factory('FullbarLayer', function(vLayer, VNode) {
    return vLayer.define({
    
      render : function() {
        var data = this.props.data,
           xScale = this.props.xScale,
           yScale = this.props.yScale,
           x1 = this.props.x1,
           x2 = this.props.x2,
           opacity = this.props.opacity || 1;
 
        var yrange = yScale.range(),
          h = yrange[0] - yrange[1],
          y0 = yrange[1];


        return new VNode('g', { className : this.props.hidden ? 'hidden' : '' }, data.map(function(d) {
          var x0 = xScale(x1(d));
          var width = xScale(x2(d)) - x0;
          var color = d.color || '#ffbfbf';
  
          return new VNode('g', null, 
            [
              new VNode('rect', {
                  x: x0,
                  y: y0,
                  width : width,
                  height : h,
                  fill : color,
                  'fill-opacity' : opacity,
                  'stroke-width' : '0' 
              }, []),
              new VNode('line', {
                  x1 : x0,
                  y1 : y0,
                  x2 : x0,
                  y2 : y0 + h,
                  'stroke-width' : '2',
                  'stroke-dasharray' : '5, 5',
                  stroke : color
              }),
              new VNode('line', {
                  x1 : x0 + width,
                  y1 : y0,
                  x2 : x0 + width,
                  y2 : y0 + h,
                  'stroke-width' : '2',
                  'stroke-dasharray' : '5, 5',
                  stroke : color
              })
            ]
          );
  
        }));
      }
    });
  });
