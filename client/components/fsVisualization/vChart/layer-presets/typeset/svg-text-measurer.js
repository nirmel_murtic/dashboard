'use strict';

angular.module('fs-visulization-core')
  .factory('SVGTextMeasurer', function() {

    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    function Measurer(className) {
      this.className = className;
      this.createContainer();
      this.measured = {};
    }

    Measurer.prototype = {
      createContainer : function() {
        this.container = d3.select('body')
        .append('svg')
        .attr('width', 250)
        .attr('height', 100)
        .attr('style', 'position: fixed; top: -9999px; left: -9999px;')
        .append('g')
        .attr('class', this.className || '')
        .append('text');
      },
      measure : function(string, bold, italic) {
        if(!string) {
          return {
            width: 0,
            height: 0
          };
        }
        var bb = {
          width : 0,
          height : 0
        };
        var charBb;
        for(var i=0; i<string.length; i++) {
          charBb = this.measureChar( string.charAt(i), bold, italic );
          bb.width += charBb.width;
          bb.height = bb.height > charBb.height ? bb.height : charBb.height;
        }
        return bb;
      },
      measureChar : function(char, bold, italic) {
        bold = !!bold;
        italic = !!italic;
        var key = char + Number(bold) + Number(italic);
        if(this.measured[key]) {
          return this.measured[key];
        }
        var tspan = this.container.append('tspan');
        if(bold) {
          tspan.attr('font-weight', 'bold');
        }
        if(italic) {
          tspan.attr('font-style', 'italic');
        }
        tspan.text(char);
        var bb;
        if(!isFirefox) {
          bb = tspan.node().getBBox();
        } else {
          bb = tspan.node().getBoundingClientRect();
        }
        this.measured[key] = bb = {
          width: bb.width,
          height: bb.height
        };
        tspan.remove();
        return bb; 
      }
    };

    return Measurer;
  
  });
