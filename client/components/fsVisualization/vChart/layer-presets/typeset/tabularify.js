'use strict';

angular.module('fs-visulization-core')
  .constant('tabularify', function(texts, length) {
    var nrow = texts.length,
      ncol = texts[0].length,
      len;
 
    var colLen = [], row, col;
    for(var i=0; i<ncol; i++) {
      colLen[i] = 0;
    }
    
    colLen = _.reduce(texts, function(colLen, row) {
      for(var i=0; i<ncol; i++) {
        len = length(row[i]);
        colLen[i] = len > colLen[i] ? len : colLen[i];
      }
      return colLen;
    }, colLen);

    var rowOffset, offsets = [];
    for(var j=0; j<nrow; j++) {
      row = texts[j];
      rowOffset = [0];
      for(i=0; i<ncol; i++) {
        col = row[i];
        if(i+1 < ncol) {
          rowOffset[i+1] = (colLen[i] - length(col));
        }
      }
      offsets.push(rowOffset);
    }
    return {
      offsets : offsets,
      colWidths : colLen
    };
  });
