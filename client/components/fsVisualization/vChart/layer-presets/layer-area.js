'use strict';

angular.module('fs-visulization')
  .factory('AreaLayer', function(vLayer, VNode, isScaleEqual) {

    return vLayer.define({
      shouldUpdate : function(nextProps) {
        if(this.props.data !== nextProps.data) {
          return true;
        }
        if(!isScaleEqual(this.props.xScale, nextProps.xScale)) {
          return true;
        }
        if(!isScaleEqual(this.props.yScale, nextProps.yScale)) {
          return true;
        }
        if(this.props.x !== nextProps.x) {
          return true;
        }
        if(this.props.y !== nextProps.y) {
          return true;
        }
        if(this.props.y0 !== nextProps.y0) {
          return true;
        }
        if(this.props.opacity !== nextProps.opacity) {
          return true;
        }
        if(this.props.color !== nextProps.color) {
          return true;
        }
        return false;
      },
      render : function() {
        var data = this.props.data,
           xScale = this.props.xScale,
           yScale = this.props.yScale,
           x = this.props.x,
           y = this.props.y,
           y0 = this.props.y0;

        var area = d3.svg.area().x(_.compose(xScale, x)).y1(_.compose(yScale, y)).y0(_.isFunction(y0) ? _.compose(yScale, y0) : yScale.range()[0]);

        var opacity = 0.05;
        if(this.props.opacity) {
          opacity = this.props.opacity;
        }

        return new VNode('g', null, [
          new VNode('path', {
              d: area(data),
              'stroke-width': '0',
              fill : this.props.color,
              className: 'area',
              'fill-opacity' : opacity
          }, [])
        ]);
      }
    });
  });
