'use strict';

angular.module('fs-visulization')
  .factory('VGridLayer', function(vLayer, VNode) {

    return vLayer.define({
    
      /**
       * render
       *
       * Layer for vertical grids
       * 
       * pass xScale and yScale through props interface for vLayer
       * both needs to be valid d3 scales
       *
       * uses xScale's ticks() to draw <line> elements
       * all rendered <lines> have the class 'grid'
       *
       * layer container (<g>) has the class 'grids'
       *
       * use these two classes to style grids
       *
       * @return {VNode}
       */
      render : function() {
        var x = this.props.xScale, y = this.props.yScale;

        var xs, w;
        if(x.ticks) {
          xs = x.ticks().map(x);
        } else {
          w = x.rangeBand();
          xs = x.range().map(function(r) {
            return r + w/2; 
          });
        }

        var xrange = x.range(), x1 = xrange[0], x2 = xrange[1];

        if(x1 !== xs[0]) {
          xs.unshift(x1);
        }
        if(x2 !== xs[xs.length - 1]) {
          xs.push(x2);
        }

        var yrange = y.range(), y1 = yrange[0], y2 = yrange[1];

        var lastMajorGridX;
        var grids = _.reduce(xs, function(lines, t, i) {
          if(i === 0 || i === xs.length - 1) {
            return lines;
          }
          var isMajorTick;
       
          if(i === 1) {
            isMajorTick = true;
            lastMajorGridX = t;
          } else {
            if(Math.abs(t - lastMajorGridX) > 100) {
              lastMajorGridX = t;
              isMajorTick = true;
            } else {
              isMajorTick = false;
            }
          }

          var gridMajor = new VNode('line', {
              className: 'grid',
              x1: t,
              x2: t,
              y1: y1,
              y2: y2,
              stroke : isMajorTick ? '#fff' : '#ebebeb',
              'stroke-width' : 1
          }, []); 

          lines.push(gridMajor);

          var gridMinor;
          if(i > 0) {
            gridMinor = new VNode('line', {
                className: 'grid',
                x1: (t + xs[i-1]) / 2,
                x2: (t + xs[i-1]) / 2,
                y1: y1,
                y2: y2,
                stroke : '#ebebeb',
                'stroke-width' : 1
            }, []); 
            lines.push(gridMinor);
          }

          return lines;
        }, []);

        return new VNode('g', { className : 'grids' }, grids);
      }
    });

  });
