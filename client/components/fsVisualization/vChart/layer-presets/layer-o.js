'use strict';

angular.module('fs-visulization')
  .factory('OLayer', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        var r = this.props.r;
        return new VNode('g', null, [
          new VNode('circle', {
            r : r,
            cx : 200,
            cy : 200,
            fill : '#22ef4a'
          }),
          new VNode('circle', {
            r : r / 2,
            cx : 200,
            cy : 200,
            fill : '#ddee4a'
          }),
          new VNode('text', {
            contents: r,
            fill : '#2000ba',
            dy : '1em'
          })
        ]);
      }
    });
  });
