'use strict';

angular.module('fs-visulization')
  .factory('BackgroundLayer', function(vLayer, VNode) {

    return vLayer.define({
      render : function() {
        var xScale = this.props.xScale,
           yScale = this.props.yScale,
           color = this.props.color;
  
        var xrange;
        if(xScale.rangeBand) {
          xrange = xScale.rangeExtent();
        } else {
          xrange = xScale.range();
        }
        var yrange = yScale.range();

        return new VNode('g', null, [
          new VNode('rect', {
            x : xrange[0],
            y : yrange[1],
            width: xrange[1] - xrange[0],
            height: yrange[0] - yrange[1],
            'stroke-width' : 0,
            fill : color || '#e3e3e3',
            className : 'bg'
          })
        ]);
      }
    });
  });
