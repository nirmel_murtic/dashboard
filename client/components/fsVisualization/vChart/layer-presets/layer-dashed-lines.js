'use strict';

angular.module('fs-visulization')
  .factory('DashedLinesLayer', function(vLayer, VNode) {

    return vLayer.define({

      defaults : {
        dasharray : [5, 5],
        color : '#bfbfff',
        width : 2
      },

      render : function() {
        var data = this.props.data,
           xScale = this.props.xScale,
           yScale = this.props.yScale,
           x1 = this.props.x1,
           y1 = this.props.y1,
           x2 = this.props.x2,
           y2 = this.props.y2,
           width = this.props.width || this.defaults.width,
           dasharray = this.props.dash || this.defaults.dasharray,
           color = this.props.color || this.defaults.color;

        return new VNode('g', null, _.map(data, function(d) {
           
          var X1 = x1 ? x1(d) : d[0];
          var Y1 = y1 ? y1(d) : d[1];
          var X2 = x2 ? x2(d) : d[2];
          var Y2 = y2 ? y2(d) : d[3];

          var YMid = ( yScale.range()[0] + yScale.range()[1] ) / 2;

          return new VNode('line', {
            x1 : xScale(X1),
            x2 : xScale(X2),
            y1 : isNaN(Y1) ? (isNaN(Y2) ? YMid : yScale(Y2)) : yScale(Y1),
            y2 : isNaN(Y2) ? (isNaN(Y1) ? YMid : yScale(Y1)) : yScale(Y2),
            'stroke-width' : width,
            'stroke-dasharray' : dasharray.toString(),
            'stroke' : color
          });
        
        }));
      }
    });
  });
