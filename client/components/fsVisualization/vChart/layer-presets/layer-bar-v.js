'use strict';

angular.module('fs-visulization')
  .factory('VBarsLayer', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        var data = this.props.data;
        var xScale = this.props.xScale,
          yScale = this.props.yScale,
          color = this.props.color || '#f00',
          projX = this.props.x,
          projY = this.props.y,
          projX0 = this.props.x0;

        var height = yScale.rangeBand();

        return new VNode('g', null, data.map(function(d) {
       
          var x = xScale(projX(d));
          var y = yScale(projY(d));
          var x0 = projX0 ? xScale(projX0(d)) : xScale.range()[0];            
          var w = x - x0;

          return new VNode('rect', {
            className : 'bar',
            x : x0,
            y : y,
            fill : color,
            width : w, 
            height : height
          });
        }));
      }
    });
  });
