'use strict';

angular.module('fs-visulization')
  .factory('PolygonLayer', function(vLayer, VNode) {
    // this layer is a stub, at the moment it's not used by any chart
    // it's functional, however, its api will most likely to change
    // when it becomes needed somewhere
    return vLayer.define({

      render : function() {
        var data = _.filter(this.props.data);
        var onMouseover = this.props.onMouseover,
            onMouseout = this.props.onMouseout;

        var className = this.props.className || '';

        var paths = data.map(function(pathArray) {
          var polygon = new VNode('path', {
              d : 'M' + pathArray.join('L') + 'Z',
              className : 'polygon ' + className,
              'pointer-events' : 'all',
              noTransition : true,
              'data-name' : pathArray.point.name,
              'data-time' : pathArray.point.x.getTime(),
              onMouseover : onMouseover || null,
              onMouseout : onMouseout || null
          }); 
          return polygon;
        });
          
        return new VNode('g', null, paths);
      }
    });
  });
