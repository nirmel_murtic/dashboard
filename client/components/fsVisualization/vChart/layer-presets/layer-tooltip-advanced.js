'use strict';

angular.module('fs-visulization')
  .factory('TooltipLayerAdvanced', function(invariant, vLayer, VNode, SVGTextMeasurer, tabularify, ColorDefs) {
    // jshint unused:false
    // jshint -W026
    //

    /**
     *
     * This is the layer for advanced typesetting of tooltip layer
     *
     * It needs to use a monospaced font, and have font-size and font-family pre-defined
     * so we can know the precise dimensions of <text> and <tspan> before mounting
     * them into the DOM, it can be used to make table like layouts
     *
     */

    var measurer = new SVGTextMeasurer('fs-chart-tooltip');

    var length = function(textToken) {
      if(_.isString(textToken)) {
        return measurer.measure(textToken).width;
      } else {
        return measurer.measure(textToken.text, textToken.bold, textToken.italic).width;
      }
    };

    var sum = function(arr) {
      return _.reduce(arr, function(sum, a) {
        return sum + a;
      }, 0);
    };

    return vLayer.define({
     
      /**
       * render
       *
       * @method
       *
       * this layer type looks like:
       *   <g>
       *     <path class="bubble"></path>
       *     <text>tooltip line 1<text>
       *     <text>tooltip line 2<text>
       *     <text>tooltip line 3<text>
       *     ...
       *   </g>
       *
       * @return {VNode}
       */

      defaults : {
        padding : {
          vertical : 6,
          horizontal : 12
        },
        emSize : 12.8
      },
       
      render : function() {

        var hidden = this.props.hidden || this.props.texts.length === 0;
        var padding = this.props.padding || this.defaults.padding; 
        var emSize = this.defaults.emSize;
        var orient = this.props.orient;
 
        var texts = this.props.texts;
        var tb = tabularify(texts, length),
            offsets = tb.offsets,
            colWidths = tb.colWidths;

        var width = sum(colWidths) + padding.horizontal * (1 + colWidths.length); 
        var height = texts.length * 1.5 * emSize + padding.vertical * 2;
        var adj = emSize, opp = emSize/2; 

        var pathAttrs;
        if(orient[0] === 'top') {
          if(orient[1] === 'left') {
            pathAttrs = topLeft(width, height, opp, adj);
          } else if (orient[1] === 'right') {
            pathAttrs = topRight(width, height, opp, adj);
          } else {
            pathAttrs = top(width, height, opp, adj);
          }
        } else if (orient[0] === 'bottom') {
          if(orient[1] === 'left') {
            pathAttrs = bottomLeft(width, height, opp, adj);
          } else if (orient[1] === 'right') {
            pathAttrs = bottomRight(width, height, opp, adj);
          } else {
            pathAttrs = bottom(width, height, opp, adj);
          }
        } else if (orient[0] === 'left') {
          pathAttrs = left(width, height, opp, adj);  
        } else if (orient[0] === 'right') {
          pathAttrs = right(width, height, opp, adj);  
        } else {
          throw 'Bad Orient for tooltips';
        }

        var bgNode = new VNode('path', {
          d : pathAttrs.d,
          fill : ColorDefs.PaleMedium,
          stroke : ColorDefs.SeaLight,
          noTransition : true,
          transform : 'translate(' + (-pathAttrs.x) + ',' + (-pathAttrs.y) + ')'
        });
  
        var nrow = texts.length,
            ncol = texts[0].length;        

        // text nodes
        var dx = 0, color, b, italic, text, textNode, textNodes = [bgNode], tspans;
 
        for(var j=0; j<nrow; j++) {
          tspans = [];
          for(var i=0; i<ncol; i++) {
            text = texts[j][i]; 
            if(i > 0) {
              dx = offsets[j][i] + padding.horizontal;
            } else {
              dx = 0;
            }
            if(isNaN(dx)) {
              console.log(texts, dx, tb, this.props);
            }
            if(!_.isString(text)) {
              b = text.bold;
              italic = text.italic;
              color = text.color;
              text = text.text;
              tspans.push(new VNode('tspan', {
                contents : text,
                'font-weight' : b ? 'bold' : 'normal',
                'font-style' : italic ? 'italic' : '',
                'fill' : color || ColorDefs.TinHeavy,
                dx : dx,
                noTransition : true
              }));
            } else {
              tspans.push(new VNode('tspan', { 
                contents: text,
                dx : dx, 
                noTransition : true
              }));
            }
          } // end for i
          textNode = new VNode('text', {
            x : padding.horizontal,
            y : j * 1.5 * emSize + padding.vertical,
            dy : '1em',
            noTransition : true
          }, tspans);
          textNodes.push(textNode);
        }

        return new VNode('g', { 
          className : 'fs-chart-tooltip ' + (hidden ? 'hidden' : ''),  
          transform : 'translate(' + (this.props.x + pathAttrs.x) + ',' + (this.props.y + pathAttrs.y) + ')' 
        }, textNodes);
      }
    });

    // the following functions generate the path data for background <path> in tooltip layer
    // based on orient, the shape of resulting bubble/path is documented as rough ascii shape
    // in the comments


    /*
     * adjacent side and opposite side
            |\
            | \
    adj ->  |  \
            ----
              ^
              |
             opp 
    */

    function left(width, height, opp, adj) {
      var pathData = [
        [0, 0],
        [-adj, -opp],
        [-adj, -height/2],
        [-width-adj, -height/2],
        [-width-adj, height/2],
        [-adj, height/2],
        [-adj, opp]
      ].join('L');

      return {
        d : 'M' + pathData + 'Z',
        x : -width-adj ,
        y : -height / 2
      };
    }

    function right(width, height, opp, adj) {
      var pathData = [
        [0, 0],
        [adj, -opp],
        [adj, -height/2],
        [width+adj, -height/2],
        [width+adj, height/2],
        [adj, height/2],
        [adj, opp],
      ].join('L');

      return {
        d : 'M' + pathData + 'Z',
        x : adj,
        y : -height / 2
      };
    }

    function top(width, height, opp, adj) {
      /**
         ---------
         |       |
         |       |
         ----v----
       *
       * top bubble
       */
      
      opp = Math.min(opp, width/2);  
      var pathData = [
        [ 0, 0 ],
        [ -opp, -adj ],
        [ -width / 2, -adj],
        [ -width / 2, -adj],
        [ -width / 2, -adj - height ],
        [ width / 2, -adj - height ],
        [ width / 2, -adj ],
        [ opp, -adj ]
      ].join('L');
      
      var d = 'M' + pathData + 'Z';
      return {
        d : d,
        x : -width/2,
        y : -adj - height
      };
    }

    function bottom(width, height, opp, adj) {
      /*
       *     ^
       * ---- ----
       * |       |
       * |       |
       * ---------
       */
       opp = Math.min(opp, width/2);
       var pathData = [
         [0, 0],
         [-opp, adj],
         [-width/2, adj],
         [-width/2, adj+height],
         [ width/2, adj+height],
         [ width/2, adj],
         [ opp, adj]
       ].join('L');
       
       var d = 'M' + pathData + 'Z';
       return {
         d : d,
         x : -width / 2,
         y : adj
       };
    }

    function topLeft(width, height, opp, adj) {

      /*
       *   _______
       *  |       |
       *  |       |
       *  --------\
       */


      opp = d3.min([opp, width/2, height/2]);
      var pathData = [
        [0,0],
        [-adj - opp, -adj],
        [-width - adj, -adj],
        [-width - adj, -adj - height],
        [-adj, -adj - height],
        [-adj, -adj-opp]
      ].join('L');
      
      return {
        d : 'M' + pathData + 'Z',
        x : - width - adj,
        y : - height - adj
      };
    }

    function topRight(width, height, opp, adj) {
      /*
       *   ______
       *  |      |
       *  |      |
       *  /------
       */
      opp = d3.min([opp, width/2, height/2]);
      
      var pathData = [
        [0, 0],
        [adj + opp, -adj],
        [width+adj, -adj],
        [width+adj, -adj-height],
        [adj, -adj-height],
        [adj, -adj-opp]
      ].join('L');
      
      return {
        d : 'M' + pathData + 'Z',
        x : adj,
        y : -height - adj
      };
    }

    function bottomLeft(width, height, opp, adj) {
      /*
       *   ______/
       *  |      |
       *  |      |
       *  --------
       */
       
      opp = d3.min([opp, width/2, height/2]);
      
      var pathData = [
        [0,0],
        [-adj-opp, adj],
        [-width-adj, adj],
        [-width-adj, adj+height],
        [-adj, adj+height],
        [-adj, adj+opp]
      ].join('L');
      
      return {
        d : 'M' + pathData + 'Z',
        x : -width - adj,
        y : adj
      };      
    }

    function bottomRight(width, height, opp, adj) {
      /*
       *  \ ______
       *  |      |
       *  |      |
       *  --------
       */
       
       opp = d3.min([opp, width/2, height/2]);
       var pathData = [
         [0,0],
         [adj+opp, adj],
         [width+adj, adj],
         [width+adj, adj+height],
         [adj, adj+height],
         [adj, adj+opp]
       ].join('L');
       
       return {
         d : 'M' + pathData + 'Z',
         x : adj,
         y : adj
       };
    }
  });
