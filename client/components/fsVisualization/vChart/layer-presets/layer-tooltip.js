'use strict';

angular.module('fs-visulization')
  .factory('TooltipLayer', function(invariant, vLayer, VNode) {
    // jshint unused:false
    // jshint -W026
    //
    /**
     * @method
     * postProcessDOM
     *
     * this method will serve as both didMount and didUpdate hooks for tooltip vLayer
     *
     * see TooltipLayer's render method for the rendered contents/tags
     *
     * The first element <path> serve as a background for tooltip texts
     * The elements that follows are all <text> tags, each correspoinding to a single line
     * of tooltip contents.
     *
     * postProcessDOM will:
     * 1. call function svgListView on all <text> nodes inside the layer, and position them 
     *    (i.e. set the 'x', and 'y' attributes of <text> so that they flow natrually instead
     *     of overlaying on top of each other)
     * 2. after <text>s are positioned right, get the 'bounding box' of all texts (returned from 
     *    svgListView function, in the format of { width: <number>, height: <number> })
     * 3. use the above dimensions and vLayer's props.orient and props.padding to update <path>
     *    element's 'd' attribute 
     * (orient can be: top, bottom, left, right, topLeft, bottomRight, bottomLeft, 
     *  useful for different chart types)
     *
     * @param {DomNode} - domNode the <g> domNode this vLayer is rendered in
     * @return {undefined}
     */
    function postProcessDOM(domNode) {
      // jshint validthis:true
      var children = domNode.childNodes;
      invariant(children.length, 'Tooltip layer is empty!');
      var bgPath = children[0];
      invariant(bgPath.tagName === 'path', 'Tooltip background missing!');

      var padding = this.props.padding;
      var orient = this.props.orient;
      var dimensions = svgListView(domNode, 'text', 2, { x: padding.top, y: padding.left });

      var x = this.props.x, y = this.props.y;

      dimensions.width += (padding.left * 2);
      dimensions.height += (padding.top * 2);
 
      var pathAttrs;
      if(orient[0] === 'top') {
        if(orient[1] === 'left') {
          pathAttrs = topLeft(dimensions.width, dimensions.height, 8, 16);
        } else if (orient[1] === 'right') {
          pathAttrs = topRight(dimensions.width, dimensions.height, 8, 16);
        } else {
          pathAttrs = top(dimensions.width, dimensions.height, 8, 16);
        }
      } else if (orient[0] === 'bottom') {
        if(orient[1] === 'left') {
          pathAttrs = bottomLeft(dimensions.width, dimensions.height, 8, 16);
        } else if (orient[1] === 'right') {
          pathAttrs = bottomRight(dimensions.width, dimensions.height, 8, 16);
        } else {
          pathAttrs = bottom(dimensions.width, dimensions.height, 8, 16);
        }
      } else if (orient[0] === 'left') {
        pathAttrs = left(dimensions.width, dimensions.height, 8, 16);  
      } else if (orient[0] === 'right') {
        pathAttrs = right(dimensions.width, dimensions.height, 8, 16);  
      } else {
        throw 'Bad Orient for tooltips';
      }

      bgPath.setAttribute('d', pathAttrs.d);

      bgPath.setAttribute('transform', 'translate(' + (-pathAttrs.x) + ',' + (-pathAttrs.y) + ')');
      domNode.setAttribute('transform', 'translate(' + (x + pathAttrs.x) + ',' + (y + pathAttrs.y) + ')');
    }

    return vLayer.define({
     
      didUpdate : postProcessDOM,
      didMount : postProcessDOM,
      /**
       * render
       *
       * @method
       *
       * this layer type looks like:
       *   <g>
       *     <path class="bubble"></path>
       *     <text>tooltip line 1<text>
       *     <text>tooltip line 2<text>
       *     <text>tooltip line 3<text>
       *     ...
       *   </g>
       *
       * @return {VNode}
       */
      render : function() {

        var hidden = this.props.hidden || this.props.lines.length === 0;
        var bg = [new VNode('path', { className: 'bubble' }, null)];

        var texts = this.props.lines.map(function(line) {
          return new VNode('text', {
            contents : line
          });
        }); 

        return new VNode('g', { 
          className : 'chart-tooltip ' + (hidden ? 'hidden' : ''),  
          'data-x' : this.props.x,
          'data-y' : this.props.y
        }, bg.concat(texts));
      }
    });

    // -- 
    // jshint -W026
    /**
     * svgListView
     *
     * this function takes a parent domNode (svg node), and repositions its children
     * of a specific tag so that they follow vertically from top to bottom, without
     * overlapping
     *
     *
     * @param {DomNode} - domNode
     * @param {string} - childTagName
     * @param {number} - margin 
     * @param {object} - origin { x: <number>, y: <number> }
     * @return {object} - dimensions { width: <number>, height : <number> } size of all repositioned children
     */
    function svgListView(domNode, childTagName, margin, origin) {
    // jshint -W026
      margin = margin || 5;
      origin = origin || { x: 0, y: 0 };
      childTagName = childTagName || 'g';
      var childNodes = domNode.childNodes;
      var i, len = childNodes.length;
      var child, y = origin.y, bb, w = 0;
      for(i=0; i<len; i++) {
        child = childNodes[i];
        if(child.tagName === childTagName && y>0) {
          if(childTagName === 'text') {
            child.setAttribute('y', y);
            child.setAttribute('x', origin.x);
            bb = child.getBBox();
            y = y + (child.getAttribute('dy') || 0) + bb.height + margin;
            w = bb.width > w ? bb.width : w;
          } else {
            child.setAttribute('transform', 
              'translate(' + origin.x + ',' + y + ')');
            bb = child.getBBox();
            y = y + bb.height + margin;
            w = bb.width > w ? bb.width : w;
          } 
        }
      }
      return {
        width : w,
        height: y - origin.y
      };
    }

    // the following functions generate the path data for background <path> in tooltip layer
    // based on orient, the shape of resulting bubble/path is documented as rough ascii shape
    // in the comments


    /*
     * adjacent side and opposite side
            |\
            | \
    adj ->  |  \
            ----
              ^
              |
             opp 
    */

    function left(width, height, opp, adj) {
      var pathData = [
        [0, 0],
        [-adj, -opp],
        [-adj, -height/2],
        [-width-adj, -height/2],
        [-width-adj, height/2],
        [-adj, height/2],
        [-adj, opp]
      ].join('L');

      return {
        d : 'M' + pathData + 'Z',
        x : -width-adj ,
        y : -height / 2
      };
    }

    function right(width, height, opp, adj) {
      var pathData = [
        [0, 0],
        [adj, -opp],
        [adj, -height/2],
        [width+adj, -height/2],
        [width+adj, height/2],
        [adj, height/2],
        [adj, opp],
      ].join('L');

      return {
        d : 'M' + pathData + 'Z',
        x : adj,
        y : -height / 2
      };
    }

    function top(width, height, opp, adj) {
      /**
         ---------
         |       |
         |       |
         ----v----
       *
       * top bubble
       */
      
      opp = Math.min(opp, width/2);  
      var pathData = [
        [ 0, 0 ],
        [ -opp, -adj ],
        [ -width / 2, -adj],
        [ -width / 2, -adj],
        [ -width / 2, -adj - height ],
        [ width / 2, -adj - height ],
        [ width / 2, -adj ],
        [ opp, -adj ]
      ].join('L');
      
      var d = 'M' + pathData + 'Z';
      return {
        d : d,
        x : -width/2,
        y : -adj - height
      };
    }

    function bottom(width, height, opp, adj) {
      /*
       *     ^
       * ---- ----
       * |       |
       * |       |
       * ---------
       */
       opp = Math.min(opp, width/2);
       var pathData = [
         [0, 0],
         [-opp, adj],
         [-width/2, adj],
         [-width/2, adj+height],
         [ width/2, adj+height],
         [ width/2, adj],
         [ opp, adj]
       ].join('L');
       
       var d = 'M' + pathData + 'Z';
       return {
         d : d,
         x : -width / 2,
         y : adj
       };
    }

    function topLeft(width, height, opp, adj) {

      /*
       *   _______
       *  |       |
       *  |       |
       *  --------\
       */


      opp = d3.min([opp, width/2, height/2]);
      var pathData = [
        [0,0],
        [-adj - opp, -adj],
        [-width - adj, -adj],
        [-width - adj, -adj - height],
        [-adj, -adj - height],
        [-adj, -adj-opp]
      ].join('L');
      
      return {
        d : 'M' + pathData + 'Z',
        x : - width - adj,
        y : - height - adj
      };
    }

    function topRight(width, height, opp, adj) {
      /*
       *   ______
       *  |      |
       *  |      |
       *  /------
       */
      opp = d3.min([opp, width/2, height/2]);
      
      var pathData = [
        [0, 0],
        [adj + opp, -adj],
        [width+adj, -adj],
        [width+adj, -adj-height],
        [adj, -adj-height],
        [adj, -adj-opp]
      ].join('L');
      
      return {
        d : 'M' + pathData + 'Z',
        x : adj,
        y : -height - adj
      };
    }

    function bottomLeft(width, height, opp, adj) {
      /*
       *   ______/
       *  |      |
       *  |      |
       *  --------
       */
       
      opp = d3.min([opp, width/2, height/2]);
      
      var pathData = [
        [0,0],
        [-adj-opp, adj],
        [-width-adj, adj],
        [-width-adj, adj+height],
        [-adj, adj+height],
        [-adj, adj+opp]
      ].join('L');
      
      return {
        d : 'M' + pathData + 'Z',
        x : -width - adj,
        y : adj
      };      
    }

    function bottomRight(width, height, opp, adj) {
      /*
       *  \ ______
       *  |      |
       *  |      |
       *  --------
       */
       
       opp = d3.min([opp, width/2, height/2]);
       var pathData = [
         [0,0],
         [adj+opp, adj],
         [width+adj, adj],
         [width+adj, adj+height],
         [adj, adj+height],
         [adj, adj+opp]
       ].join('L');
       
       return {
         d : 'M' + pathData + 'Z',
         x : adj,
         y : adj
       };
    }
  });
