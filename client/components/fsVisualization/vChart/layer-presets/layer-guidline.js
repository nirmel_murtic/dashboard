'use strict';

angular.module('fs-visulization')
  .factory('GuidelineLayer', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        
        //var xScale = this.props.xScale, 
        var yScale = this.props.yScale,
            x = this.props.x;

        var y0 = this.props.y0 || yScale.range()[0], 
            y1=  this.props.y || yScale.range()[1];

        return new VNode('g', null, [
          new VNode('line', {
            noTransition: true,
            className : 'guideline',
            x1 : x,
            y1 : y0,
            x2 : x,
            y2 : y1,
          })
        ]);
      } // end of render function

    });
  })
  .factory('GuidelineLayerHorizontal', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        
        //var xScale = this.props.xScale, 
        var xScale = this.props.xScale,
            y = this.props.y;

        var x0 = this.props.x0 || xScale.range()[0], 
            x1=  this.props.x || xScale.range()[1];

        return new VNode('g', null, [
          new VNode('line', {
            noTransition: true,
            className : 'guideline',
            x1 : x0,
            y1 : y,
            x2 : x1,
            y2 : y,
          })
        ]);
      } // end of render function

    });
  });
