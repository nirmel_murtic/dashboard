'use strict';

angular.module('fs-visulization')
  .factory('HorizontalLineLayer', function(vLayer, VNode) {
    return vLayer.define({

      render : function() {

        var xScale = this.props.xScale,
            y = this.props.y;

        var x0 = this.props.x0 || xScale.range()[0], 
            x1=  this.props.x || xScale.range()[1];

        var color = this.props.color || 'rgba(102, 102, 212, .4)';
        var width = this.props.width || 1;
        var dasharray = this.props.dasharray || '5, 5'; 

        return new VNode('g', null, [
          new VNode('line', {
            stroke : color,
            'stroke-width' : width,
            'stroke-dasharray' : dasharray,
            x1 : x0,
            y1 : y,
            x2 : x1,
            y2 : y,
          })
        ]);
      } // end of render function
        

    });
  });
