'use strict';

angular.module('fs-visulization')
  .factory('BarsLayer', function(vLayer, VNode) {
    return vLayer.define({
      render : function() {
        var data = this.props.data;
        var xScale = this.props.xScale,
          yScale = this.props.yScale,
          color = this.props.color || '#f00',
          projX = this.props.x,
          projY = this.props.y,
          projY0 = this.props.y0;

        var width = xScale.rangeBand();

        return new VNode('g', null, data.map(function(d) {
       
          var x = xScale(projX(d));
          var y = yScale(projY(d));
          var y0 = projY0 ? yScale(projY0(d)) : yScale.range()[0];            
          var h = y0 - y;

          if(isNaN(h) || isNaN(y)) {
            y = -10;
            h = 0;
          }

          return new VNode('rect', {
            className : 'bar',
            x : x,
            y : y,
            fill : color,
            width : width, 
            height : h
          });
        }));
      }
    });
  });
