'use strict';

angular.module('fs-visulization-utils')
  .factory('noop', function() {
    return function(){};
  });
