'use strict';

angular.module('fs-visulization-utils')
  .constant('zipWith', function zipWith() {
    var args = Array.prototype.slice.call(arguments);
    var zipFn = args.pop();
    var len, i, j, elements, ret = [];  
    if(args.length) {
      len = args[0].length;
      for(i=0; i<len; i++) {
        elements = [];
        for(j=0; j<args.length; j++) {
          elements.push(args[j][i]);
        }
        ret.push( zipFn.apply(null, elements) );
      } 
    }
    return ret;
  });
