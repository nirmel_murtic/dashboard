'use strict';

angular.module('fs-visulization-utils')
  .constant('rangeSize', function rangeSize(range) {
    if(!range || !_.isArray(range) || range.length < 2) {
      return 0;
    }
    range = range.length > 2 ? d3.extent(range) : range;
    return Math.abs(range[0] - range[1]);
  });
