'use strict';

angular.module('fs-visulization-utils')
  .factory('isScaleEqual', function(isEqual) {
    // d3 scales are mutable, this function checks their equality
    return function isScaleEqual(s1, s2) {
      if(s1 === s2) {
        return true;
      }
      return isEqual(s1.domain(), s2.domain()) && isEqual(s1.range(), s2.range());
    };
  });
