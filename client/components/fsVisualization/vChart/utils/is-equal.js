'use strict';

angular.module('fs-visulization-utils')
  .factory('isEqual', function() {
    return _.isEqual;
  });
