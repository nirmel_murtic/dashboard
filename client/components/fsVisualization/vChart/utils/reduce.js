'use strict';

angular.module('fs-visulization-utils')
  .factory('reduce', function() {
    return _.reduce;
  });
