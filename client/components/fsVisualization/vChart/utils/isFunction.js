'use strict';

angular.module('fs-visulization-utils')
  .constant('isFunction', _.isFunction);
