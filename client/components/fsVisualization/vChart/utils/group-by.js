'use strict';

angular.module('fs-visulization-utils')
  .factory('indexGroupBy', function() {
    function id(x) { return x; }
    return function indexGroupBy(arr, accessor) {
      accessor = accessor || id;
      var grouped = {};
      var len = arr.length;
      var key;
      for(var i=0; i<len; i++) {
        key = accessor(arr[i]);
        grouped[key] = grouped[key] || [];
        grouped[key].push(i);
      }
      var ordered = {};
      var keys = Object.keys(grouped).sort();
      for(i=0; i<keys.length; i++) {
        ordered[keys[i]] = grouped[keys[i]];
      }
      return ordered;
    };
  });
