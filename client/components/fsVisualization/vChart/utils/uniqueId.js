'use strict';

angular.module('fs-visulization-utils')
  .factory('uniqueId', function() {
    return _.uniqueId;
  });
