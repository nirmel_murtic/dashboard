'use strict';

describe('Function: isScaleEqual', function () {

  var isScaleEqual;
  // load module
  beforeEach(module('fs-visulization-utils'));

  beforeEach(inject(function(_isScaleEqual_) {
    isScaleEqual = _isScaleEqual_;
  }));


  it('Should acknowledge referencial equality for d3 scales', function() {
    var scale1 = d3.scale.linear();
    var scale2 = scale1;

    expect(isScaleEqual(scale1, scale2)).toBe(true);
  });

  it('Should consider scales equal if they have identical domain and range', function() {
    var scale1 = d3.scale.linear().domain([1, 1000]).range([0, 100]);
    var scale2 = d3.scale.linear().domain([1, 1000]).range([0, 100]);

    expect(isScaleEqual(scale1, scale2)).toBe(true);
  });

  it('Should consider scales not equal if they have different domain or range', function() {
    var scale1 = d3.scale.linear().domain([1, 1000]).range([0, 100]);
    var scale2 = d3.scale.linear().domain([1, 1000]).range([0, 200]);

    var scale3 = d3.scale.linear().domain([1, 999]).range([0, 100]);
    var scale4 = d3.scale.linear().domain([1, 1000]).range([0, 200]);

    expect(isScaleEqual(scale1, scale2)).not.toBe(true);
    expect(isScaleEqual(scale3, scale4)).not.toBe(true);
  });

  it('Should work on time scales', function() {
    var scale1 = d3.time.scale();
    var scale2 = d3.time.scale();

    expect(isScaleEqual(scale1, scale2)).toBe(true);
    scale2.range([1, 1000]);
    expect(isScaleEqual(scale1, scale2)).not.toBe(true);
  });
});
