'use strict';

describe('Function: rangeSize', function () {

  var rangeSize;
  // load module
  beforeEach(module('fs-visulization-utils'));

  beforeEach(inject(function(_rangeSize_) {
    rangeSize = _rangeSize_;
  }));

  it('Should calculate the size of a range (array with two number elements)', function() {
    var range = [0, 1];
    expect(rangeSize(range)).toBe(1);
  });

  it('Should calculate the size of an array\'s range', function() {
    var range = [0, -1, 2, 3];
    // d3.extent(range) === 4
    expect(rangeSize(range)).toBe(4);
  });

  it('Should always return a non-negative number', function() {
    var range = [1, 0];
    expect(rangeSize(range) >= 0).toBe(true);
    range=[0, 0];
    expect(rangeSize(range) >= 0).toBe(true);
  });

  it('Should return 0 for empty array', function() {
    expect(rangeSize([])).toBe(0);
  });

});
