'use strict';

angular.module('fs-visulization-utils')
  .factory('accessor', function() {
    /*jshint -W054 */
    /**
     * makeAccessor
     *
     * helper function to work with getter/accessors (common constructs in d3 land)
     * 
     *
     * @param {function | string} - keyOrAccessor if a string make a getter function that returns the property value given under the string key, if a function just returns itself  
     * @return {function} - accessor
     */
    return function makeAccessor(keyOrAccessor) {
      if(angular.isFunction(keyOrAccessor)) {
        return keyOrAccessor;
      } else {
        return new Function ('x', 'return x[\'' + keyOrAccessor.toString() + '\']' );
      }
    };
  });
