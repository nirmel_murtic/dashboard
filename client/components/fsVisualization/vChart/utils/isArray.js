'use strict';

angular.module('fs-visulization-utils')
  .factory('isArray', function() {
    return _.isArray;
  });
