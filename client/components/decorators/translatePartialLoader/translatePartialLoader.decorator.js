'use strict';

angular.module('common')
  .config(function ($provide) {
    $provide.decorator('$translatePartialLoader', function ($delegate) {
        var $delegateOld = $delegate;

        $delegate = function(options) {
          window.configuration.currentLang = options.key;

          return $delegateOld(options);
        };

        $delegate.isPartAvailable = $delegateOld.isPartAvailable;
        $delegate.addPart = $delegateOld.addPart;
        $delegate.deletePart = $delegateOld.deletePart;

        $delegate.addPartOld = $delegate.addPart;

        $delegate.addPart = function(name) {
          var partLabels = window.configuration.labels[name.replace(/\//g, '.')];

          if(!partLabels) {
            console.log('Invalid part added: ' + name + '.');
            return;
          }

          if(!partLabels[window.configuration.currentLang]) {
            console.log('Invalid lang ' + window.configuration.currentLang + ' for part ' + name + ' added.');
            return;
          }

          name = partLabels[window.configuration.currentLang];

          return $delegate.addPartOld(name);
        };

        return $delegate;
    });
  });
