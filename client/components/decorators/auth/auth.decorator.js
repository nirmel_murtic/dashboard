'use strict';

angular.module('common')
  .config(function ($provide) {
    $provide.decorator('$auth', function ($delegate, $rootScope, $parse, permissions, $http, $state, $q, $location, $timeout) {
          $delegate.switchToRefreshUrl = function() {
              $delegate.refreshToken = true;
              $delegate.refreshing = $q.defer();

              $delegate.getConfig('default').remmemberedPath = $delegate.getConfig('default').tokenValidationPath;
              $delegate.getConfig('default').tokenValidationPath = $delegate.getConfig('default').tokenRefreshPath + '?refreshToken=' + this.retrieveData('refresh_token');
          };

          $delegate.switchToValidateUrl = function() {
              delete $delegate.refreshToken;
              if($delegate.refreshing) {
                $delegate.refreshing.resolve(true);
                delete $delegate.refreshing;
              }

              if($delegate.getConfig('default').remmemberedPath) {
                $delegate.getConfig('default').tokenValidationPath = $delegate.getConfig('default').remmemberedPath;
                delete $delegate.getConfig('default').remmemberedPath;
              }
          };

          $delegate.checkPermissions = function(permissionsTemplate) {
              if(!permissionsTemplate) {
                  return true;
              }

              var template = permissionsTemplate.trim();

              var authorities = this.user.authorities;

              if(!authorities) {
                return false;
              }

              if(template.length === 0) {
                  return true;
              }

              for(var i = 0; i < authorities.length; i++) {
                  if(permissions[authorities[i]]) {
                      template = template.replace(new RegExp('<' + permissions[authorities[i]] + '>', 'g'), 'true');
                  }
              }

              template = template.replace(new RegExp('<', 'g'), '');
              template = template.replace(new RegExp('>', 'g'), '');

              if($parse(template)()) {
                return true;
              } else {
                return false;
              }
          };

          $delegate.signOut = function(redirectToLogin) {
              this.deleteData('refresh_token');

              var promise = this.rejectDfd({
                reason: 'unauthorized',
                errors: ['Forcing logging out']
              });

              $rootScope.$broadcast('auth:logout-success');

              if(redirectToLogin) {
                return $state.go('anonymous.login');
              } else {
                return promise;
              }
          };

          $delegate.resolveMain = function($rootScope, $location, $state, $translatePartialLoader, refreshLabels) {
            return $delegate.validateUser().then(function() {
              if(!$delegate.checkPermissions($rootScope.toState.permissions)) {
                $location.path('/login');
                $state.go('anonymous.login');
              } else if(refreshLabels){
                $translatePartialLoader.addPart('app/common');
                $translatePartialLoader.addPart('app/main/main');
                return $rootScope.refreshTranslations();
              }
            }, function(reason) {
              try {
                if(reason.errors) {
                  for(var i = 0; i<reason.errors.length; i++) {
                    console.log(reason.errors[i]);
                  }
                }
              } catch(err) {}
              // $location.path('/login');
              var nextState = $state.next,
                  nextParams = $state.toParams;
              if(nextState.name.indexOf('main') > -1) {
                // $state.transition is almost always a promise, that will be rejected 
                // due do no credentials
                // once the current transition fail, we go to login, with instructions 
                // where to return in "main" state once login is successful
                if($state.transition) {
                  $state.transition.finally(function() {
                    $state.go('anonymous.login', {
                      redirectState : nextState.name,
                      redirectParams : nextParams
                    }, {
                      reload : true
                    });
                  });
                // just in case $state.transition gets lost, we set a timeout
                } else {
                  $timeout(200).then(function() {
                    $state.go('anonymous.login', {
                      redirectState : nextState.name,
                      redirectParams : nextParams
                    }, {
                      reload : true
                    });
                  }); 
                }
                if(__DEV__) {
                  console.log('Failed resolve main, redirect to login, after login, should go to: ', nextState.name, nextParams);
                }
              } else {
                ($state.transition || $timeout(200)).finally(function() {
                  $state.go('anonymous.login');
                });
              }
              return $q.reject(reason);
            });
          };

          $delegate.resolveAnonymous = function($rootScope, $location, $state, $translatePartialLoader) {
            return $delegate.validateUser().then(function() {
              $location.path('/setup');
              $state.go('main.align');
            }, function() {
              $translatePartialLoader.addPart('app/common');
              $translatePartialLoader.addPart('app/anonymous/anonymous');
              return $rootScope.refreshTranslations();
            });
          };

          $delegate.validateToken = function(opts) {
            if (opts === null || opts === undefined) {
              opts = {};
            }

            if (this.refreshToken || (!this.tokenHasExpired() && $location.path() !== '/activate')) {
              return $delegate.callValidationEndpoint(opts);
            } else {
              return this.rejectDfd({
                reason: 'unauthorized',
                errors: ['Expired credentials']
              });
            }
          };

          $delegate.callValidationEndpoint = function(opts) {
            // dfd := deferred
            // there's a bug when dfd is null when calling refreshToken endpoint
            // we need to call initDfd when this happends
            if(!$delegate.dfd) {
              $delegate.initDfd();
            }
            return $http.get($delegate.apiUrl(opts.config) + $delegate.getConfig(opts.config).tokenValidationPath).success((function(_this) {
              return function(resp) {
                var authData;
                authData = _this.getConfig(opts.config).handleTokenValidationResponse(resp);
                _this.handleValidAuth(authData);
                if (_this.firstTimeLogin) {
                  $rootScope.$broadcast('auth:email-confirmation-success', _this.user);
                }
                if (_this.mustResetPassword) {
                  $rootScope.$broadcast('auth:password-reset-confirm-success', _this.user);
                }

                $delegate.switchToValidateUrl();

                return $rootScope.$broadcast('auth:validation-success', _this.user);
              };
            })(this)).error((function(_this) {
              return function(data) {
                if(!$delegate.refreshToken) {
                  console.log('User validation failed. We will try to refresh token!');

                  $delegate.switchToRefreshUrl();

                  $delegate.deleteData('auth_headers');

                  return $delegate.callValidationEndpoint(opts);
                }

                $delegate.deleteData('refresh_token');
                if($delegate.refreshing) {
                  $delegate.refreshing.reject(false);
                } 

                console.log('User validation failed again. Refresh token is not valid! Loging out!');

                $delegate.switchToValidateUrl();

                if (_this.firstTimeLogin) {
                  $rootScope.$broadcast('auth:email-confirmation-error', data);
                }
                if (_this.mustResetPassword) {
                  $rootScope.$broadcast('auth:password-reset-confirm-error', data);
                }
                $rootScope.$broadcast('auth:validation-error', data);
                return _this.rejectDfd({
                  reason: 'unauthorized',
                  errors: data.errors
                });
              };
            })(this));
          };

          return $delegate;
    });
  });
