'use strict';

describe('Decorator: auth', function () {

  // load the directive's module
  beforeEach(module('common'));

  it('check if checkPermissions method works fine for positive case', inject(function ($compile, $auth) {
    // Testing data
    $auth.user = {
      authorities: ['LIN']
    };

    // Check if checkPermissions method works fine for positive case
    expect($auth.checkPermissions('<LOGGED_IN>')).toBe(true);
  }));

  it('check if checkPermissions method works fine for negative case', inject(function ($compile, $auth) {
    // Testing data
    $auth.user = {
      authorities: ['LIN']
    };

    // Check if checkPermissions method works fine for negative case
    expect($auth.checkPermissions('<SOMETHING_WRONG>')).toBe(false);
  }));
});
