'use strict';

/*global FB */

angular.module('common')
  .run(function($window, $rootScope, $interval) {
    $window.fbAsyncInit2 = $window.fbAsyncInit;

    $window.fbAsyncInit = function() {
      console.log('Wait for configuration to be loaded before initialising facebook provider!');
    };

    $rootScope.$on('Configuration:loaded', function(event, data) {

      var initialiseFacebook = function() {
        if(window.FacebookProvider.initialised || !window.FB) {
          return;
        }

        window.FacebookProvider.setAppIds(
          data.facebookAppId,
          data.facebookAdsAppId,
          'publish_pages,manage_pages,read_insights',
          'ads_management,publish_pages',
          'onConnect'
        );

        if ($window.fbAsyncInit2) {
          $window.fbAsyncInit = $window.fbAsyncInit2;

          delete $window.fbAsyncInit2;
        }

        if ($window.fbAsyncInit) {
          $window.fbAsyncInit();

          window.FacebookProvider.initialised = true;
        }

        console.log('Facebook connector initialised!');

        $interval.cancel(window.FacebookProvider.initialisationPromise);
      };

      window.FacebookProvider.initialisationPromise = $interval(initialiseFacebook, 100);

      console.log('Configuration loaded!');
    });
  })
  .config(function ($provide, $injector) {
    var service = $injector.get('FacebookProvider');

    window.FacebookProvider = service;

    service.setSdkVersion('v2.4');

    service.setAppIds = function(organicAppId, adsAppId, organicPermissions, adsPermissions, callback) {
      this.organicPermissions = organicPermissions;
      this.adsPermissions = adsPermissions;
      this.organicId = organicAppId;
      this.adsAppId = adsAppId;

      this.init({ 'appId': this.organicId });

      this.callback = callback;
    };

    $provide.decorator('Facebook', function ($delegate, $rootScope, $filter, fsConfirm) {
        $rootScope.$on('Facebook:authResponseChange', function(event, data) {
          if(FB.loggingOut) {
            return;
          }

          if(data.authResponse) {
            $delegate.checkPermissions(service.getAppId(), function(response) {
              if(response && service.organicId === service.getAppId()) {
                FB.organicToken = data.authResponse.accessToken;

                if(!FB.adsToken) {
                  service.setAppId(service.adsAppId);

                  FB.init({
                    appId: service.getAppId(),
                    status: true,
                    xfbml: true,
                    version: service.getSdkVersion()
                  });
                }
              } else if(!response && service.organicId === service.getAppId()) {
                $delegate.login(function(organicResponse) {
                  if(!organicResponse.authResponse && service.callback) {
                    return false;
                  }

                  $delegate.checkPermissions(service.getAppId(), function(response) {
                    if(response) {
                      console.log('Facebook organic status: ' + organicResponse.status);
                      FB.organicToken = data.authResponse.accessToken;
                    }
                  });
                }, { scope : service.organicPermissions });
              } else if(!response && service.adsAppId === service.getAppId()){
                $delegate.login(function(adsResponse) {
                  if(!adsResponse.authResponse && service.callback) {
                    return;
                  }

                  $delegate.checkPermissions(service.getAppId(), function(response) {
                    if(response) {
                      console.log('Facebook ads status: ' + adsResponse.status);
                      FB.adsToken = adsResponse.authResponse.accessToken;
                    }
                  });
                }, { scope : service.adsPermissions });
              } else if(response && service.adsAppId === service.getAppId()) {
                FB.adsToken = data.authResponse.accessToken;

                if(!FB.organicToken) {
                  service.setAppId(service.organicId);

                  FB.init({
                    appId: service.getAppId(),
                    status: true,
                    xfbml: true,
                    version: service.getSdkVersion()
                  });
                }
              }

              if(service.callback && FB.adsToken && FB.organicToken) {
                $delegate.success();
              }
            });
          } else if(service.adsAppId === service.getAppId()) {
            $delegate.login(function(adsResponse) {
              if(adsResponse.authResponse) {
                $delegate.checkPermissions(service.getAppId(), function(response) {
                  if(response) {
                    console.log('Facebook ads status: ' + adsResponse.status);
                  }
                });
              }
            }, { scope : service.adsPermissions });
          } else if(service.organicId === service.getAppId()) {
            $delegate.login(function(organicResponse) {
              if(organicResponse.authResponse) {
                $delegate.checkPermissions(service.getAppId(), function(response) {
                  if(response) {
                    console.log('Facebook organic status: ' + organicResponse.status);
                  }
                });
              }
            }, { scope : service.organicPermissions });
          }
        });

        $delegate.checkPermissions = function(appId, callback) {
          $delegate.api('/me?fields=id,name,permissions', function(result) {
              if(result.error && (result.error.code === 2 || result.error.code === 4 || result.error.code === 17 || result.error.code === 341)) {
                fsConfirm('genericAlert', {
                  body: $filter('translate')('label.align.addSocialAccount.Serviceistemporaryunavailable')
                });

                return;
              }

              if(!result.permissions || !result.permissions.data) {
                callback(false);
                return;
              }

              var permissions = null;

              if (service.organicId === service.getAppId()) {
                permissions = service.organicPermissions.split(',');
              } else {
                permissions = service.adsPermissions.split(',');
              }

              var foundedPermissionsCount = 0;

              for (var i = 0; i < result.permissions.data.length; i++) {
                if(permissions.indexOf(result.permissions.data[i].permission) !== -1 && result.permissions.data[i].status === 'granted') {
                  foundedPermissionsCount++;
                }
              }

              FB.accountName = result.name;

              if (service.adsAppId === service.getAppId()) {
                FB.userId = result.id;
              }

              callback(foundedPermissionsCount === permissions.length);
          });
        };

        $delegate.fullLogin = function() {
          delete FB.organicToken;
          delete FB.adsToken;

          if(service.organicId === service.getAppId()) {
            $delegate.login(function() { }, { scope : service.organicPermissions });
          } else if(service.adsAppId === service.getAppId()) {
            $delegate.login(function() { }, { scope : service.adsPermissions });
          }
        };

        $delegate.success = function() {
          $rootScope.$broadcast('Facebook:' + service.callback, {
              organicToken: FB.organicToken,
              adsToken: FB.adsToken,
              accountName: FB.accountName,
              userId: FB.userId
            }
          );
        };

      $delegate.fullLogout = function(callback) {
        FB.loggingOut = true;
        $delegate.logout(function(result) {
          delete FB.loggingOut;
          callback(result);
        });
      };

        return $delegate;
    });
  });
