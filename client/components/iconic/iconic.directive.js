'use strict';

angular.module('common')
  .directive('iconic', function($interpolate) {

    /* jshint newcap: false */
    var iconic = IconicJS();
    var svgDirStatic = 'assets/svg/static/';
    var svgDirSmart = 'assets/svg/smart/';

    function startsWith(str, start) {
      var len1 = str.length, len2 = start.length;
      return len1 >= len2 && str.slice(0, len2) === start;
    }

    function trimAndLower(str) {
      return str.length ? str.replace(/(^\s+|\s+$)/g, '').toLowerCase() : '';
    }

    return {
      restrict: 'EA', 
      scope : {},
      compile : function(element, attrs) {
        var camelAttr, attrGetters=attrs.class ? [$interpolate(attrs.class)] : [angular.noop], attrNames=['class'];
        var smart = !attrs.hasOwnProperty('static');
        for(camelAttr in attrs.$attr) {
          if ( startsWith(attrs.$attr[camelAttr], 'data-') ) {
            attrGetters.push( $interpolate(attrs[camelAttr]) );
            attrNames.push( trimAndLower(attrs.$attr[camelAttr]) );
          }
        }
        var imgNode = angular.element('<img data-src="" />')[0];
        return function(scope, element) {
          var watchGroup = attrGetters.map(function(getter) {
            return function(scope) {
              return getter(scope.$parent);
            };
          });
          scope.$watchGroup(watchGroup, function(newVals) {
            var i = 0, len = newVals.length, newVal;
            var newImgNode = imgNode.cloneNode();
            for (i; i<len; i++) {
              newVal = newVals[i];
              if (attrNames[i] === 'data-src') {
                newVal = (smart ? svgDirSmart : svgDirStatic) + newVal.replace(/\.svg$/i, '') + '.svg';
              }
              if(newVal) {
                newImgNode.setAttribute(attrNames[i], newVal);    
              }
            }
            element.html('').append(newImgNode);
            iconic.inject(newImgNode);
          });
        };
      }
    };

  });
