'use strict';

angular.module('common')
  .filter('numRankSuffix', function() {
    var mapping = {
      '0' : 'th',
      '1' : 'st',
      '2' : 'nd',
      '3' : 'rd',
      '4' : 'th',
      '5' : 'th',
      '6' : 'th',
      '7' : 'th',
      '8' : 'th',
      '9' : 'th'
    };

    return function(d) {
      // jshint bitwise : false
      d = ~~d;
      d = d.toString(10);
      var lastDigit = d.charAt( d.length - 1 );
      return mapping[lastDigit];
    };
  });
