'use strict';

angular.module('common')
  .filter('reverse', function() {
      return function(items) {
          return items.slice().reverse();
      };
  });