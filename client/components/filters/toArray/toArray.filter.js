/**
 * Created by Sinan on 1/8/15.
 * This is to convert an object to an array in order to loop through it in ng-repeat and allow us to sort through keys
 * https://github.com/petebacondarwin/angular-toArrayFilter
 */

'use strict';

angular.module('common')

    .filter('toArray', function () {
        return function (obj, addKey) {
            if( Object(obj) !== obj ) {return obj;}
            if ( addKey === false ) {
                return Object.keys(obj).map(function(key) {
                    return obj[key];
                });
            } else {
                var keys = Object.keys(obj);
                keys = _.filter(keys, function(key) {
                    return _.isObject(obj[key]);
                });
                return keys.map(function (key) {
                    return Object.defineProperty(obj[key], '$key', { enumerable: false, value: key});
                });
            }
        };
    });
