'use strict';

angular.module('common')
  .filter('metricDate', function() {
    var fallback = d3.time.format('%Y-%m-%d');
    if(Date.prototype.toLocaleDateString) {
      return function(d) {
        return d.toLocaleDateString();
      };
    } else {
      return fallback;
    }
  })
  .filter('metricTime', function() {
    return d3.time.format('%I:%M %p');
  })
  .filter('metricDatetime', function($filter) {
    var date = $filter('metricDate');
    var time = $filter('metricTime');
    return function(d) {
      if(d.toString() === 'Invalid Date') {
        return 'Invalid Date';
      }
      return date(d) + ' ' + time(d);
    };
  });
