'use strict';

angular.module('common')
  .filter('metricCurrency', function() {
    var currency = d3.format('$,.2f');
    return function(d) {
      if(!_.isNumber(d) || isNaN(d) || d >= Infinity || d <= -Infinity) {
        return 'N/A';
      }
      return currency.apply(null, _.toArray(arguments));
    };
  });
