'use strict';

angular.module('common')
  .filter('metricPicker', function($filter) {
    return function(d, filterName) {
      return $filter(filterName)(d);
    };
  });
