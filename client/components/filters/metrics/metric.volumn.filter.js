'use strict';

angular.module('common')
  .filter('metricVolume', function() {
    // positive integer, comma
    var int = d3.format(',d'); 
    return function(d) {
      if(_.isNumber(d) && !isNaN(d) && d < Infinity && d > -Infinity) {
        return int(Math.round(d));
      } else {
        return 'N/A';
      }
    };
  })
  .filter('metricVolumeCompact', function() {
    // positive integer shorten to 1.2 k / 1.3 m

    // jshint bitwise : false
    var int = d3.format(',d');
    // 1,000.1 | comma, 1 decimal
    var float1 = d3.format('.1f');
    var float2 = d3.format('.2f');

    return function integerFormatter(d) {
      if(!_.isNumber(d) || isNaN(d) || d >= Infinity || d <= -Infinity) {
        return 'N/A';
      }
      var dd = Math.abs(d);
      if(dd < 1000) {
        return int(d | 0);
      } else if (dd < 1e4) {
        return float2(d / 1000) + ' k';
      } else if (dd < 1e5) {
        return float1(d / 1000) + ' k';
      } else if (dd < 1e6) {
        return int(d / 1000 | 0) + ' k';
      } else if (dd < 1e7) {
        return float1(d / 1e6) + ' m';
      } else if (dd < 1e9) {
        return int(d / 1e6 | 0) + ' m';
      } else {
        return float1(d / 1e9) + ' b';
      }
    };
  });

