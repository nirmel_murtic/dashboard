'use strict';

angular.module('common')
  .filter('metricDuration', function() {
    function twoDigits(n) {
      n = n.toString(10); 
      while(n.length < 2) {
        n = '0' + n;
      }
      return n;
    }
    return function(seconds) {
      // jshint bitwise: false
      seconds = seconds | 0;
      var mins =(seconds / 60) | 0; 
      var hours = (mins / 60) | 0;
      mins = mins % 60;
      var secs = seconds % 60;
      return [hours, mins, secs].map(twoDigits).join(':');
    };
  })
  .filter('metricDateAtTime', function() {

    var date = d3.time.format('%a, %m/%d/%y');
    var time = d3.time.format('%I:%M %p');
    return function(d) {
      return date(d) + ' at ' + time(d);
    };

  });
