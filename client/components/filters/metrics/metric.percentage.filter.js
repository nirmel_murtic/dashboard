'use strict';

angular.module('common')
  .filter('metricPercentage', function() {
    // positive integer, comma
    // var digits0 = d3.format(',.0%');
    var digits0 = d3.format(',.0%');
    var digits2 = d3.format(',.2%');
    var digits4 = d3.format(',.4%');

    var fdigits0 = d3.format(',.0f');
    var fdigits2 = d3.format(',.2f');
    var fdigits4 = d3.format(',.4f');


    return function percentage(d, sign) {
      if(!_.isNumber(d) || isNaN(d) || d >= Infinity || d <= -Infinity) {
        return 'N/A';
      }
      if(sign === 'remove%') {
        if(d === 0) {
          return '0';
        } else if(Math.abs(d) > 0.1) {  // 10% or greater
          return fdigits0(d * 100);
        } else if(Math.abs(d) > 0.0001) { // 0.01% or greater
          return fdigits2(d * 100);
        } else {
          return fdigits4(d * 100);
        }
      }
      if(d === 0) {
        return '0%';
      } else if(Math.abs(d) > 0.1) {  // 10% or greater
        if (Math.abs(d) % 1 === 0) {
          return digits0(d);
        }
        return digits2(d);
      } else if(Math.abs(d) > 0.0001) { // 0.01% or greater
        return digits2(d);
      } else {
        return digits4(d);
      }
    };
  });
