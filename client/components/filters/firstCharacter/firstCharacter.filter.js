'use strict';

angular.module('common')
  .filter('firstCharacters', function() {
    return function(input) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g,
      function(txt) {
        return txt.charAt(0).toUpperCase();
      }) : '';
    };
  });


/*
 Usage:
 {{team.name.example | firstCharacters:true}}
 Example:
 - samir hodzic : return SH
 - Promote your Page : return PYP
 */
