'use strict';

describe('Filter: firstCharacters', function () {

  // load the filter's module
  beforeEach(module('common'));

  // initialize a new instance of the filter before each test
  var firstCharacters;
  beforeEach(inject(function ($filter) {
    firstCharacters = $filter('firstCharacters');
  }));

  describe('firstCharacters', function () {
    it('Should return capitalized the first character of each word', function () {
      expect(firstCharacters('World wide web')).toBe('WWW');
    });
  });

});
