'use strict';

angular.module('common')
  .filter('attachmentTriming', function () {
    return function (input) {
      input = input.substring( input.indexOf('_') + 1, input.lastIndexOf('_'));
      return input;
    };
  });
