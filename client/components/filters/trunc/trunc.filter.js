'use strict';

angular.module('common')
  .filter('trunc', function() {
    return function(d, n) {
      n = n || 30;
      return _.trunc(d, n);
    };
  });
