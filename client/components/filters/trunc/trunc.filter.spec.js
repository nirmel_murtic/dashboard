/**
 * Created by Sinan on 11/9/15.
 */

'use strict';

describe('Filter: trunc', function () {

  // load the filter's module
  beforeEach(module('common'));

  // initialize a new instance of the filter before each test
  var trunc;
  beforeEach(inject(function ($filter) {
    trunc = $filter('trunc');
  }));

  describe('trunc with specified parameter', function () {
    it('Should return the sentence truncated at the given index -3 followed by ... ', function () {
      expect(trunc('Campaign Creation', 7)).toBe('Camp...');
    });
  });

  describe('trunc with default parameter (30)', function () {
    it('Should return the sentence truncated with the 30 first characters including "..." ', function () {
      expect(trunc('012345678901234567890123456-thisIsHidden')).toBe('012345678901234567890123456...');
    });
  });

});