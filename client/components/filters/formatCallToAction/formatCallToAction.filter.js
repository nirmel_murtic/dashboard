'use strict';

angular.module('common')
  .filter('formatCallToAction', function(fbCallsToAction, twCallsToAction) {
    return function(key, channel, invert) {
      if(!invert) {
        if (channel === 'twitter') {
          return twCallsToAction[key];
        } else {
          return fbCallsToAction[key];
        }
      } else { 
        // invert needed for bulk upload, to find the 
        // Java key for their funny input
        if (channel === 'twitter') {
          // could be just _.invert(fbCallsToAction)[key] but let's 
          // be nice and accept normal human variations of capitalization and spaces.
          return _(twCallsToAction)
                  .map(function (cta, javaCta) {
                    var normalCta = cta.toLowerCase().replace(/\s+/g, '');
                    return [normalCta, javaCta];
                  })
                  .object()
                  .value()[key.toLowerCase().replace(/\s+/g, '')];
        } else {
          return _(fbCallsToAction)
                  .map(function (cta, javaCta) {
                    var normalCta = cta.toLowerCase().replace(/\s+/g, '');
                    return [normalCta, javaCta];
                  })
                  .object()
                  .value()[key.toLowerCase().replace(/\s+/g, '')];
        }
      }
    };
  });