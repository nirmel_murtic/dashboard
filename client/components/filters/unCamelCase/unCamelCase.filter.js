'use strict';

angular.module('common')
  .filter('unCamelCase', function() {
    return function(text) {
      return (!!text) ? text.replace(/([A-Z])/g, ' $1')
                            .replace(/^./, function(str){
                              return str.toUpperCase();
                            })
                      : 'String does not exist!';
    };
  });
