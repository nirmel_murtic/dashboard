/**
 * Created by Sinan on 11/9/15.
 */
'use strict';

describe('Filter: unCamelCase', function () {

  // load the filter's module
  beforeEach(module('common'));

  // initialize a new instance of the filter before each test
  var unCamelCase;
  beforeEach(inject(function ($filter) {
    unCamelCase = $filter('unCamelCase');
  }));

  describe('unCamelCase', function () {
    it('Should return a setence with each word capitalized letters', function () {
      expect(unCamelCase('campaignCreation')).toBe('Campaign Creation');
    });
  });

});
