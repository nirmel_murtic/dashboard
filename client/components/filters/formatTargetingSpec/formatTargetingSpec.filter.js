'use strict';

angular.module('common')
  .filter('formatTargetingSpec', function(targetingSpecs) {
    return function(key) {
      return targetingSpecs[key];
    };
  });