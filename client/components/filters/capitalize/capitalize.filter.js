'use strict';

angular.module('common')
  .filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    };
  })
  .filter('capitalizeWords', function($filter) {
    var capitalize = $filter('capitalize');

    return function(input) {
      var words = input.split(/\s+/);
      return words.map(capitalize).join(' ');
    };
  });
