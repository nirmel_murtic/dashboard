'use strict';

describe('Service: position', function () {

  // load the service's module
  beforeEach(module('common'));

  // instantiate service
  var $position;
  beforeEach(inject(function (_$position_) {
    $position = _$position_;
  }));

  it('should do something', function () {
    expect(!!$position).toBe(true);
  });

});
