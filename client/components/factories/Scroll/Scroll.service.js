'use strict';

angular.module('common')
  .service('Scroll', function ($q) {
    var document = angular.element('body');

    this.bottom = function () {

      document.animate({ scrollTop: $(document).height() }, 1000);

    };

    this.top = function () {

      var deferred = $q.defer();
      document.animate({ scrollTop: 0 }, 1000, function() {
        // Animation complete.
        deferred.resolve();
      });

      return deferred.promise;
    };

    this.element = function (itemName, numPixels) {

      if(!itemName) {
        console.log('Unknown element.');
        return;
      }

      var element = angular.element(itemName);

      element.animate({scrollTop: numPixels}, 'slow');

    };
});
