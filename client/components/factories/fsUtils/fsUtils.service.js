'use strict';

angular.module('common')
  .factory('fsUtils', function ($q) {
    
    // node.js utils.inherit implementation for the browser
    var inherits; 
    if (typeof Object.create === 'function') {
      // implementation from standard node.js 'util' module
       inherits = function (ctor, superCtor) {
        ctor.super_ = superCtor;
        ctor.prototype = Object.create(superCtor.prototype, {
          constructor: {
            value: ctor,
            enumerable: false,
            writable: true,
            configurable: true
          }
        });
      };
    } else {
      // old school shim for old browsers
      inherits = function (ctor, superCtor) {
        ctor.super_ = superCtor;
        var TempCtor = function () {};
        TempCtor.prototype = superCtor.prototype;
        ctor.prototype = new TempCtor();
        ctor.prototype.constructor = ctor;
      };
    }

    // event emitter mixin
    function Emitter(obj) {
      if (obj) { return mixin(obj); }
    }

    /**
     * Mixin the emitter properties.
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */

    function mixin(obj) {
      for (var key in Emitter.prototype) {
        obj[key] = Emitter.prototype[key];
      }
      return obj;
    }

    /**
     * Listen on the given `event` with `fn`.
     *
     * @param {String} event
     * @param {Function} fn
     * @return {Emitter}
     * @api public
     */

    Emitter.prototype.on =
    Emitter.prototype.addEventListener = function(event, fn){
      this._callbacks = this._callbacks || {};
      (this._callbacks[event] = this._callbacks[event] || [])
        .push(fn);
      return this;
    };

    /**
     * Adds an `event` listener that will be invoked a single
     * time then automatically removed.
     *
     * @param {String} event
     * @param {Function} fn
     * @return {Emitter}
     * @api public
     */

    Emitter.prototype.once = function(event, fn){
      function on() {
        /*jshint validthis:true */
        this.off(event, on);
        fn.apply(this, arguments);
      }

      on.fn = fn;
      this.on(event, on);
      return this;
    };

    /**
     * Remove the given callback for `event` or all
     * registered callbacks.
     *
     * @param {String} event
     * @param {Function} fn
     * @return {Emitter}
     * @api public
     */

    Emitter.prototype.off =
    Emitter.prototype.removeListener =
    Emitter.prototype.removeAllListeners =
    Emitter.prototype.removeEventListener = function(event, fn){
      this._callbacks = this._callbacks || {};

      // all
      if (0 === arguments.length) {
        this._callbacks = {};
        return this;
      }

      // specific event
      var callbacks = this._callbacks[event];
      if (!callbacks) { return this; }

      // remove all handlers
      if (1 === arguments.length) {
        delete this._callbacks[event];
        return this;
      }

      // remove specific handler
      var cb;
      for (var i = 0; i < callbacks.length; i++) {
        cb = callbacks[i];
        if (cb === fn || cb.fn === fn) {
          callbacks.splice(i, 1);
          break;
        }
      }
      return this;
    };

    /**
     * Emit `event` with the given args.
     *
     * @param {String} event
     * @param {Mixed} ...
     * @return {Emitter}
     */

    Emitter.prototype.emit = function(event){
      this._callbacks = this._callbacks || {};
      var args = [].slice.call(arguments, 1), callbacks = this._callbacks[event];

      if (callbacks) {
        callbacks = callbacks.slice(0);
        for (var i = 0, len = callbacks.length; i < len; ++i) {
          callbacks[i].apply(this, args);
        }
      }

      return this;
    };

    /**
     * Return array of callbacks for `event`.
     *
     * @param {String} event
     * @return {Array}
     * @api public
     */

    Emitter.prototype.listeners = function(event){
      this._callbacks = this._callbacks || {};
      return this._callbacks[event] || [];
    };

    /**
     * Check if this emitter has `event` handlers.
     *
     * @param {String} event
     * @return {Boolean}
     * @api public
     */

    Emitter.prototype.hasListeners = function(event){
      return !! this.listeners(event).length;
    };


    // Public API here
    return {
      inherits : inherits,
      Emitter : Emitter,
      promise : {
        // [Promise] -> Promise
        settle : function(promises) {
          var deferred = $q.defer();
          var results = [];
          var nFinished = 0;
          _.forEach(promises, function(p, index) {
            p.then(function(value) {
              results[index] = {
                value : value,
                reason : null
              };
              nFinished++;
              if(nFinished === promises.length) {
                deferred.resolve(results);
              }
            }, function(err) {
              results[index] = {
                value : null,
                reason : err
              };
              nFinished++;
              if(nFinished === promises.length) {
                deferred.resolve(results);
              }
            });
          });
          return deferred.promise;
        }
      },
      zipWith : function() {
        var args = Array.prototype.slice.call(arguments);
        var zipFn = args.pop();
        var len, i, j, elements, ret = [];  
        if(args.length) {
          len = args[0].length;
          for(i=0; i<len; i++) {
            elements = [];
            for(j=0; j<args.length; j++) {
              elements.push(args[j][i]);
            }
            ret.push( zipFn.apply(null, elements) );
          } 
        }
        return ret;
      },
      sortObject : function(obj) {
        return _(obj).pairs().sortBy('0').zipObject().value();
      },
      keyMirror : function(obj) {
        var ret = {};
        var key;
        if (!(obj instanceof Object && !Array.isArray(obj))) {
          throw new Error('keyMirror(...): Argument must be an object.');
        }
        for (key in obj) {
          if (obj.hasOwnProperty(key)) {
            ret[key] = key;
          }
        }
        return ret;
      },
      object: {
        toJSON: function(input, callback){
          try{
            callback( JSON.parse(input) );
          }catch(e){
            callback( input );
          }
        },

        isEmpty: function(obj){
          var hasOwnProperty = Object.prototype.hasOwnProperty;
          // null and undefined are "empty"
          if (obj === null || obj === undefined){
            return true;
          } 

          // Assume if it has a length property with a non-zero value
          // that that property is correct.
          if (obj.length > 0){
            return false;
          } 

          if (obj.length === 0){
             return true;
          } 

          // Otherwise, does it have any properties of its own?
          // Note that this doesn't handle
          // toString and valueOf enumeration bugs in IE < 9
          for (var key in obj) {
              if (hasOwnProperty.call(obj, key)){
                return false;
              } 
          }

          return true;
        }
      },
      array: {

      },
      string: {

      },
      promisify : function(fn, context, resolveTransform, rejectTransform) {
        
        return function promisified() {
          var args = _.toArray(arguments); 
          var dfd = $q.defer();    
          args.push(function success(result) {
            if(resolveTransform) {
              result = resolveTransform.apply(null, arguments);
            }
            dfd.resolve(result); 
          });
          args.push(function error(reason) {
            if(rejectTransform) {
              reason = rejectTransform.apply(null, arguments);
            }
            dfd.reject(reason);
          });

          fn.apply(context || null, args);
          return dfd.promise;
        };
      }
    };
  });
