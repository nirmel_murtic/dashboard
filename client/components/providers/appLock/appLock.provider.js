'use strict';

angular.module('common')
  .provider('fsAppLock', function() {

    var idleOpts = {
        threshold : 30, // in minutes
        tickInterval : 2
    };

    var listeners = [function(monitor) {
      $(document).on('mousemove', monitor.reset);
    }, function(monitor) {
      $(document).on('keydown', monitor.reset);
    }];

    this.config = function(opts) {
      _.assign(idleOpts, opts || {});
    };

    this.addListener = function(fn) {
      listeners.push(fn);
    };

    this.$get = ['$q', 'fsUtils', function($q, fsUtils) {
      function IdleMonitor(opts) {
        opts = opts || idleOpts;
        this.threshold = opts.threshold;
        this.tickInterval = opts.tickInterval;

        this.idleTime = 0;
        this.lastAction = '';
        this.lastTimeStamp = null;

        fsUtils.Emitter(this);
        this.tick = this.tick.bind(this);
        this.reset = this.reset.bind(this);
      }
      IdleMonitor.prototype = {
        tick : function() {
          this.idleTime+=this.tickInterval;
          if(this.idleTime >= this.threshold) {
            this.emit('idle', {
              since : Date.now() - this.threshold * 60000,
              lastAction : this.lastAction,
              idleFor : this.idleTime
            });
            this.end();
          } else if (this.lastTimeStamp) {
            if(Date.now() - this.lastTimeStamp >= this.threshold * 60000) {
              this.emit('idle', {
                since : Date.now() - this.threshold * 60000,
                lastAction : this.lastAction,
                idleFor : this.idleTime
              });
              this.end();
            }
          }
          this.lastTimeStamp = Date.now();
        },
        reset : function(event) {
          if(event) {
            this.lastAction = event;
          }
          this.idleTime = 0;
          return this;
        },
        start : function() {
          this.timer = setInterval(this.tick, this.tickInterval * 60000);
          return this;
        },
        end : function() {
          if(this.timer) {
            clearInterval(this.timer);
          }
          this.timer = null;
          this.lastAction = null;
          return this;
        }
      };

      var monitor = new IdleMonitor();

      var fn;

      while(listeners.length) {
        fn = listeners.pop(); 
        fn(monitor);
      }

      monitor.start();

      return monitor;

    }];
  });
