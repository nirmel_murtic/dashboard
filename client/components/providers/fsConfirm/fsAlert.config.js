'use strict';

angular.module('common')
    .config(function (fsConfirmProvider){

        fsConfirmProvider.config('genericAlert', {
            templateUrl: 'components/providers/fsConfirm/generic.alert.html',
            dismiss : {
                click : false, // The user can't click away from the modal
                esc : true,
                enter : true, // user can dismiss alert on enter
                routing : true
            },
            dark : true,
            defaultParams : {
              title : '',
              body : '',
              yes : 'Ok'
            }
        });

        fsConfirmProvider.config('errors', {
            templateUrl: 'components/providers/fsConfirm/errors.confirm.html',
            dismiss : {
                click : false, // The user can't click away from the modal
                esc : true,
                enter : true, // user can dismiss alert on enter
                routing : true
            },
            dark : true,
            defaultParams : {
              title : '',
              body : '',
              yes : 'Ok'
            }
        });

        fsConfirmProvider.config('enterprisePlanAlert', {
            templateUrl: 'app/main/align/accountSettings/subscribeToPlan/enterprisePlan.alert.html',
            dismiss : {
                click : false, // The user can't click away from the modal
                esc : true,
                enter : true, // user can dismiss alert on enter
                routing : true
            }
        });

        fsConfirmProvider.config('confirmationWithInput', {
          templateUrl: 'components/providers/fsConfirm/confirmation.with.input.html',
          dismiss : {
            click : false,
            esc : true,
            routing : true
          }
        });

        fsConfirmProvider.config('genericWithLink', {
          templateUrl: 'components/providers/fsConfirm/generic.with.link.html',
          dismiss : {
            click : false,
            esc : true,
            routing : true
          }
        });

        fsConfirmProvider.config('genericMultipleLines', {
          templateUrl: 'components/providers/fsConfirm/generic.multiple.lines.html',
          dismiss : {
            click : false,
            esc : true,
            routing : true
          }
        });
});
