'use strict';

angular.module('common')
  .service('fsElementEnter', function($q) {
    return function(element, styles) {
      var deferred = $q.defer();
      try {
        var sel = d3.select(element[0] || element); 
        styles = _.assign({
          opacity: 1,
          top: 0
        }, styles || {});
        sel.transition()
          .duration(150)
          .style(styles)
          .each('end', function() {
            deferred.resolve(true); 
          });
      } catch(err) {
        deferred.reject(err);
      }
      return deferred.promise;
    };
  })
  .service('fsElementExit', function($q) {
    return function(element, styles) {
      var deferred = $q.defer();
      try {
        var sel = d3.select(element[0] || element); 
        var h = $(sel.node()).height();
        h = h > 0 ? h * 10 : 8000; 
        styles = _.assign({
          opacity: 0,
          top: h,
          width: 0,
          height: 15
        }, styles || {});
        sel.transition().style(styles)
          .each('end', function() {
            deferred.resolve(true); 
          });
      } catch(err) {
        deferred.reject(err);
      }
      return deferred.promise;
    };
  });
