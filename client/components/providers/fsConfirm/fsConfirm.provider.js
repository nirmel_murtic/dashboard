'use strict';

angular.module('common')
  .provider('fsConfirm', function() {

    //jshint loopfunc:true

    var presets = {};
    var masterContainer = $('<div class="fs-confirms">').appendTo('body');
    masterContainer.hide();

    var defaults = {
      templateUrl: 'components/providers/fsConfirm/generic.confirm.html',
      controller : ['$scope', function($scope) {
        $scope.body = 'Confirm action?';
        $scope.yes = 'Ok';
        $scope.no = 'Cancel';
      }],
      dark : true,
      dismiss : {
        click : false, // The user can't click away from the modal
        esc : true,
        enter : false,
        routing : true
      },
      defaultParams : {
        body : '',
        title : '',
        yes : 'Ok',
        no : 'Cancel'
      }
    };

    this.config = function(name, def) {
      if(!_.isObject(def) || !_.isString(name)) {
        throw 'Invalid fsConfirm config: ' + def;
      }
      presets[name] = _.assign({}, defaults, def);
      return this;
    };

    presets.generic = defaults;
    this.$get = ['$controller', '$rootScope', '$compile', '$templateCache', '$templateRequest', '$q', 'fsElementEnter', 'fsElementExit', 'withInput',
      function($controller, $rootScope, $compile, $templateCache, $templateRequest, $q, fsElementEnter, fsElementExit, withInput) {

      var views = {};
      var name, def, element, tpl;

      function fsConfirmIt(view) {
        if(view.deferred) {
          view.deferred.resolve(true);
          view.deferred = null;
        }
        if(view.scope.inputNameForm) {
          withInput.setInput(view.scope.inputName);
          //view.scope.inputNameForm.$setPristine(true);
          delete view.scope.inputName;
        }
      }
      function fsDenyIt(view) {
        if(view.deferred) {
          view.deferred.reject(false);
          view.deferred = null;
          withInput.destroyInput();
        }
        if(view.scope.inputNameForm) {
          view.scope.inputNameForm.$setPristine(true);
          delete view.scope.inputName;
        }
      }
      masterContainer.bind('click', function() {
        var viewPromises = _.map(_.toArray(views), function(v) {
          return $q.when(v);
        });
        $q.all(viewPromises).then(function(vs) {
          var view;
          for(var i=0; i<vs.length; i++) {
            view = vs[i];
            if(view.deferred && view.def.dismiss.click) {
              view.deferred.reject(false);
              view.deferred = null;
            }
          }
        });
      });

      for(name in presets) {
        def = presets[name];
        element = $('<div class="fs-confirm fs-confirm-' + name +'" tabindex="1">').appendTo(masterContainer);
        tpl = $templateCache.get(def.templateUrl);
        if(!tpl) {
          tpl = $templateRequest(def.templateUrl);
        }
        views[name] = $q.when(tpl).then(function(name, def, element, tplStr) {
          element.html(tplStr);

          var scope = $rootScope.$new(true);
          var ctrl = def.controller || angular.noop;
          var view = {
            deferred : null,
            def : def,
            element : element,
            scope : scope,
            ctrl : $controller(ctrl, { $scope : scope }),
            dark : !!def.dark,
            defaultParams : def.defaultParams || {}
          };
          scope.fsConfirmIt = fsConfirmIt.bind(null, view);
          scope.fsDenyIt = fsDenyIt.bind(null, view);

          views[name] = view;

          $compile(element.contents())(scope);
          element.bind('click', function(evt) {
            evt.stopPropagation();
          });
          if(def.dismiss.esc) {
            element.bind('keyup', function(evt) {
              if(evt.keyCode !== 27) {
                return;
              }
              evt.stopPropagation();
              if(view.deferred) {
                view.deferred.reject(false);
                view.deferred = null;
                withInput.destroyInput();
              }
              if(view.scope.inputName) {
                view.scope.inputNameForm.$setPristine(true);
                delete view.scope.inputName;
              }
            });
          }
          if(def.dismiss.enter) {
            element.bind('keyup', function(evt) {
              if(evt.keyCode !== 13) {
                return;
              }
              evt.stopPropagation();
              if(view.deferred) {
                view.deferred.resolve(true);
                view.deferred = null;
              }
            });
          }
          if(def.dismiss.enter) {
            element.bind('keyup', function(evt) {
              if(evt.keyCode !== 13) {
                return;
              }
              evt.stopPropagation();
              if(view.deferred) {
                view.deferred.resolve(true);
                view.deferred = null;
              }
            });
          }
          if(def.dismiss.routing) {
            $rootScope.$on('$stateChangeStart', function() {
              if(view.deferred) {
                view.deferred.reject(false);
                view.deferred = null;
              }
            });
          }

          views[name] = view;
          return view;

        }.bind(null, name, def, element));
      }

      function initEnter(element) {

        element.css({
          width: '',
          height: ''
        });
        element.show();
        d3.select(element[0]).style({
          top: -450,
          opacity: 0
        });
        masterContainer.show();
      }
      function finishExit(element) {
        if(element) {
          element.hide();
        }
        var viewPromises = _.map(_.toArray(views), function(v) {
          return $q.when(v);
        });
        $q.all(viewPromises).then(function(vs) {
          for(var i=0; i<vs.length; i++) {
            if(vs[i].deferred) {
              return;
            }
          }
          masterContainer.removeClass('dark').hide();
        });
      }

      return function(name, locals, zIndex) {
        return $q.when(views[name || 'generic']).then(function(view) {
          if(!view) {
            return true;
          }
          $('input').blur(function() {
            view.scope.focusInput = false;
          });

          setTimeout(function(){
            view.scope.focusInput = true;
            $('input').focus();
          }, 250);

          if(withInput.getInput()) {
            view.scope.inputName = withInput.getInput();
          }

          _.assign(view.scope, view.defaultParams, locals || {});
          view.deferred = $q.defer();
          initEnter(view.element);
          $('body').addClass('confirm-open');
          if(view.dark) {
            masterContainer.addClass('dark');
          } else {
            masterContainer.removeClass('dark');
          }
          if(zIndex) {
            view.element.css('z-index', zIndex);
          }
          return fsElementEnter(view.element)
            .then(function() {
              view.element.focus();
              var exit = function() {
                return fsElementExit(view.element);
              };
              var promise = view.deferred.promise;
              promise.then(exit, exit).then(function() {
                finishExit(view.element);
                $('body').removeClass('confirm-open');
                if(zIndex) {
                  view.element.css('z-index', '');
                }
                if(view.deferred) {
                  view.deferred = null;
                }
              });
              return promise;
            });
        });
      };
    }];

  })
  .run(function(fsConfirm) {
    return fsConfirm;
  })
  .directive('fsDeny', function() {
    return {
      restrict : 'A',
      scope : false,
      link : function(scope, element, attrs) {
        var evt = attrs.fsDeny || 'click';
        var handler = scope.fsDenyIt || angular.noop;
        element.bind(evt, handler);
        scope.$on('destroy', function() {
          element.off(evt, handler);
        });
      }
    };
  })
  .directive('fsConfirm', function() {
    return {
      restrict : 'A',
      scope : false,
      link : function(scope, element, attrs) {
        var evt = attrs.fsConfirm || 'click';
        var handler = scope.fsConfirmIt || angular.noop;
        element.bind(evt, handler);
        scope.$on('destroy', function() {
          element.off(evt, handler);
        });
      }
    };
  });
