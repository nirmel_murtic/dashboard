'use strict';

angular.module('common')

  .service('withInput', function(){
    var data = {
      input: ''
    };

    this.getInput = function () {
        return data.input;
      };

    this.setInput = function (input) {
        data.input = input;
    };

    this.destroyInput = function() {
      data.input = '';
    };

  });
