'use strict';

angular.module('common.providers')
  /* jshint camelcase: false, sub:true, expr: true */
  /**
   * A General Case Tree Data structure
   * 
   * and a heuristic diff algo (level-by-level)
   * this provider can be configured to used specialied TreeDS
   * for example, using custom compareFn for detecting node changes
   * @return {*} angular provider
   */
  .provider('TreeDS', function() {
    function TreeDS(node, children) {
      this.node = node;
      this.children = _.isArray(children) ? children : null;
    }
    TreeDS.prototype = {
      compareFn : _.isEqual,
      isLeaf : function() {
        return !this.children;
      },
      isDiffNode : function() {
        return (!!this.node['$$replaceNode']) ||
               (!!this.node['$$insertChild']) ||
               (!!this.node['$$removeChild']);
      },
      toJSON : function() {
        return _.isObject(this.node) ? _.extend({}, this.node) : this.node;
      },
      toIterator : function() {
        var stack = [this];
        var e = new ReferenceError('StopIteration');
        return function treeNext() {
          if(!stack.length) {
            return e;
          }
          var current = stack.pop();
          if(current.children) {
            stack = stack.concat(current.children.reverse());
          }
          return current.node;
        };
      }
    };
    function diffNode_(tree1, tree2) {
      if( tree1.compareFn(tree1.node, tree2.node) ) {
        return false;
      }
      return {
        '$$replaceNode' : {
          old_node : tree1.node,
          new_node : tree2.node
        }
      };
    }

    function diffChildren_(tree1, tree2) {
      var len1 = tree1.children.length, 
          len2 = tree2.children.length;
      var TreeClass = tree1.constructor;
      var enter, exit;
      if(len1 > len2) {
        exit = tree1.children.slice(len2).map(function(c) {
          return new TreeClass({ '$$removeChild' : c.node }, null); 
        });
        return tree1.children.slice(0, len2).concat(exit);
      } else if (len1 < len2) {
        enter= tree2.children.slice(len1).map(function(c) {
          return new TreeClass({ '$$insertChild' : c.node }, c.children); 
        });
        return tree1.children.concat(enter);
      } else {
        return false;
      }
    }

    function diffLeaves(tree1, tree2) {
      var TreeClass = tree1.constructor;
      var nodeChanged;
      if(tree1.isLeaf() && tree2.isLeaf()) {
        var changes = diffNode_(tree1, tree2);
        return new TreeClass( changes || tree1.node, null );
      }
      if(tree1.isLeaf()) {
        nodeChanged = diffNode_(tree1, tree2);
        var insertedChildren = tree2.children.map(function(c) {
          var TreeClass=tree2.constructor;
          return new TreeClass({ '$$insertChild' : c.node }, c.children);
        });
        return new TreeClass( nodeChanged || tree1.node, insertedChildren );
      }
      if(tree2.isLeaf()) {
        nodeChanged = diffNode_(tree1, tree2);
        var deletedChildren = tree1.children.map(function(c) {
          var TreeClass=tree1.constructor;
          return new TreeClass({ '$$removeChild' : c.node }, null);
        });
        return new TreeClass( nodeChanged || tree1.node, deletedChildren );
      }
    }

    function diffTree(tree1, tree2) {
      if(tree1.isLeaf() || tree2.isLeaf()) {
        return diffLeaves(tree1, tree2);
      }
      var TreeClass = tree1.constructor;
      var nodeDiff = diffNode_(tree1, tree2);
      var childrenDiff = diffChildren_(tree1, tree2);
      // var len = Math.min(tree1.children.length, tree2.children.length);
      childrenDiff = (childrenDiff || tree1.children).map(function(t, i) {
        if(t.node['$$insertChild'] || t.node['$$removeChild']) {
          return t;
        } 
        return diffTree(t, tree2.children[i]);
      });
      return new TreeClass(nodeDiff || tree1.node, childrenDiff);
    }

    // depth first
    function tranverseTree(tree, preFn, postFn) {
      preFn && preFn(tree.node, tree);
      if(tree.children) {
        tree.children.forEach(function(t) {
          tranverseTree(t, preFn, postFn);
        });
      }
      postFn && postFn(tree.node, tree);
    }
    function tranverseTreeWithPath(tree, fn) {
      var path_ = [];
      tranverseTree(tree, function(node, t) {
        fn(node, t, path_);
        path_.push(node);
      }, function() {
        path_.pop();
      });
    }
    function reduceTree(tree, fn, acc0, includePath) {
      var acc=acc0;
      var trans = includePath ? tranverseTreeWithPath : tranverseTree;
      trans(tree, function(node, t, path) {
        acc = fn(acc, node, t, path);
      });
      return acc;
    }
    function mapTree(NewTreeClass, tree, fn, path_) {
      NewTreeClass = NewTreeClass || TreeDS;
      if(path_ === true) {
        path_ = [];
      }
      var newTree = new NewTreeClass( fn(tree.node, tree, path_) );
      if(tree.children) {
        if(path_) {
          path_.push(tree.node);
        }
        newTree.children = tree.children.map(function(c) {
          return mapTree(NewTreeClass, c, fn, path_);
        });
        if(path_) {
          path_.pop();
        }
      }
      return newTree;
    }

    // Provider return values
    function createTreeClass(Ctor, options) {
      Ctor.prototype = _.extend(Ctor.prototype || {}, TreeDS.prototype, options || {});
      return Ctor;
    }

    createTreeClass.$get = function() {
      return function treeServiceFactory(TreeClass) {
        return {
          build : function(node, children) {
            return new TreeClass(node, children);
          },
          diff : diffTree,
          tranverse : tranverseTree,
          tranverseWithPath : tranverseTreeWithPath,
          map : function(tree, fn, includePath) {
            return mapTree(TreeClass, tree, fn, includePath);
          },
          reduce : reduceTree
        };
      };    
    };
    return createTreeClass;
  });
