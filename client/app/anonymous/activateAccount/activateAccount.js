'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
      .state('activateAccount', {
        url: '/activate?email&activationKey',
        templateUrl: 'app/anonymous/activateAccount/activateAccount.html', // Do not change it to login
        controller: 'ActivateaccountCtrl',
        resolve: {
          signOut : function($auth) {
            $auth.signOut(false);
          }
        }
      });
  });
