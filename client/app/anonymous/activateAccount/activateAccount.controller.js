'use strict';

angular.module('anonymous')
  .controller('ActivateaccountCtrl', function ($scope, $rootScope, $http, $stateParams, loginService, $state) {
    var activateData = {
        email : $stateParams.email.replace(' ', '+'),
        activationKey : $stateParams.activationKey
    };

    angular.element('body').addClass('login-picture');

    loginService.activate(activateData,
        function(){
            $state.go('anonymous.login', {'activateAccount': true});
        }, function(error){
            console.log(error.error.message);
            $scope.message = error.error.message;
        }
    );
  });
