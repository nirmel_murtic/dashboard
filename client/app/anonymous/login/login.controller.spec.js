'use strict';

describe('Controller: LoginCtrl', function () {

  // load the controller's module
  beforeEach(module('anonymous'));

  var LoginCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      redirect: {} //refer to login.js, redirect is a uiRouter resolve
    });
  }));

  it('should have errorCtr be false initially', function () {
    expect(scope.errorCtr).toBe(false);
  });

  it('should have errorCtr be true if missing password or email', function(){
    scope.email = '123@test.com';
    scope.password = '';
    scope.processForm();
    expect(scope.errorCtr).toBe(true);
    
    scope.email = '';
    scope.password = '123';
    scope.processForm();
    expect(scope.errorCtr).toBe(true);
  });

});
