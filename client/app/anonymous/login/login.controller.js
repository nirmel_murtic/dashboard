'use strict';

angular.module('anonymous')
  .controller('LoginCtrl', function ($scope, $rootScope, $http, $auth, $state, $stateParams, redirect, subscribePlan) {
    if(__DEV__) {
      console.log(redirect);
    }
    $scope.errorCtr = false;
    $scope.state = $state;
    $scope.closeError = function () {
      $scope.errorCtr = false;
    };
    if($stateParams.activateAccount){
        $scope.activateAccount = $stateParams.activateAccount;
    }

    angular.element('body').addClass('login-picture');

    $scope.processForm = function() {
        var loginData = {
            email: $scope.email,
            password: $scope.password
        };
        if (!$scope.email || !$scope.password){
          $scope.errorCtr = true;
        } else {
        	$auth.submitLogin($.param(loginData))
            	.then(function(result) {
                    $rootScope.sharedLoginToken = result.jti;
                    $scope.errorCtr = false;
                    if(result['terms_conditions'] === true){ // jshint ignore:line
                        $state.go('anonymous.termsAndConditions');
                    } else if(redirect.state !== null) {
                        $state.go(redirect.state, redirect.params);
                    } else {
                      if(subscribePlan) {
                        $state.go('main.align', { 'subscribePlan':'true' });
                      } else {
                        $state.go('main.align');
                      }
                    }
                })
                .catch(function() {
                    $scope.errorCtr = true;
                });
        }
    };
  });
