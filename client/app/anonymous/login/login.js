'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
		.state('anonymous.login', {
			url: '/login?activateAccount&subscribePlan',
			views: {
		    '@anonymous': {
		      templateUrl: 'app/anonymous/login/login.html',
					controller: 'LoginCtrl'
		    },
	      'footer@anonymous': {
	        templateUrl: 'app/anonymous/footer/footer.html',
					controller: 'AnonymousFooterCtrl'
	      }
		  },
      params : {
        redirectState : null,
        redirectParams : {}
      },
      resolve: {
        subscribePlan : function($stateParams) {
          return $stateParams.subscribePlan === 'true';
        },
        redirect : function($stateParams) {
          return {
            state : $stateParams.redirectState,
            params : $stateParams.redirectParams
          };
        },
        checkData: function($rootScope, $auth) {
          if($rootScope.fractalAccountInfo && !$auth.user.signedIn) {
            delete $rootScope.fractalAccountInfo;
          }
        }
      }
		});
  });
