'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
      .state('anonymous.termsAndConditions', {
        permissions: '<VTAC>',
        url: '/termsAndConditions',
        views: {
            '@anonymous': {
                templateUrl: 'app/anonymous/termsAndConditions/termsAndConditions.html',
                controller: 'TermsandconditionsCtrl'
            }
        }
      });
  });
