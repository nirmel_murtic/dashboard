'use strict';

describe('Controller: TermsandconditionsCtrl', function () {

  // load the controller's module
  beforeEach(module('anonymous'));

  var TermsandconditionsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TermsandconditionsCtrl = $controller('TermsandconditionsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
