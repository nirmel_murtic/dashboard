'use strict';

angular.module('anonymous')
  .controller('TermsandconditionsCtrl', function ($scope, loginService, $state, $auth, $location, $window) {

    angular.element('body').addClass('login-picture');

    if(!$auth.user.access_token){ // jshint ignore:line
      $auth.signOut();
    }

    $scope.cancelTerms = function() {
      $auth.signOut();
    };

    // This is useful when the user is redirected to the page where he/she needs to accept the terms and conditions. In that case, the Accept and Cancel buttons appear
    // If the Terms are just shown when the user clicks on the footer, then we don't need the buttons and they'll be hidden (because the controller won't be loaded)
    $scope.view = {
        showAcceptButton: true
    };

    $scope.acceptTerms = function(){
        loginService.termsAndConditions(
            function(){
                $auth.validateUser().then(function() {
                    angular.element('body').removeClass('login-picture');
                    $window.location.reload();
                }, function(reason) {
                    if(reason.errors) {
                        for(var i = 0; i<reason.errors.length; i++) {
                            console.log(reason.errors[i]);
                        }
                    }

                    $location.path('/login');
                    $state.go('anonymous.login');
                });
            }, function(error){
                console.log(error.error.message);
            }
        );
    };
  });
