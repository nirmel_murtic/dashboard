'use strict';

angular.module('anonymous')
  .controller('AnonymousFooterCtrl', function ($scope, ngDialog) {
  	$scope.openTermsAndConditions = function(){
      ngDialog.open({
            template: 'app/anonymous/termsAndConditions/termsAndConditionsModal.html',
            className: 'modal modal-large tos-modal',
            showClose: false,
            scope: $scope
        });
    };

    $scope.openPrivacyPolicy = function(){
        ngDialog.open({
            template: 'app/anonymous/privacyPolicy/privacyPolicy.html',
            className: 'modal modal-large privacy-modal',
            showClose: false,
            scope: $scope
        });
    };
});
