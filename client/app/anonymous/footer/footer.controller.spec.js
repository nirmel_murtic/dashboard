'use strict';

describe('Controller: AnonymousFooterCtrl', function () {

  // load the controller's module
  beforeEach(module('anonymous'));

  var AnonymousFooterCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AnonymousFooterCtrl = $controller('AnonymousFooterCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
