'use strict';

angular.module('anonymous')
  .factory('loginService', function ($http, identityConverter, apiUrl) {
    return {
        register: function (userData, successCallback, errorCallback) {
            $http({
              method: 'POST',
              url: apiUrl + '/api/v2/user/register?fields=username',
              data: $.param(userData)
            })
            .success(successCallback)
            .error(errorCallback);
        },
        resetPassword: function (resetData, successCallback, errorCallback){
            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/user/resetPassword',
                data: $.param(resetData)
            })
            .success(successCallback)
            .error(errorCallback);
        },
        resetPasswordAction: function (confirmData, successCallback, errorCallback){
            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/user/resetPasswordAction',
                data: $.param(confirmData)
            })
            .success(successCallback)
            .error(errorCallback);
        },
        termsAndConditions: function (successCallback, errorCallback){
            $http({
                method: 'GET',
                url: apiUrl + '/api/v2/user/acceptTermsAndConditions'
            })
                .success(successCallback)
                .error(errorCallback);
        },
        getCodeStates: function (successCallback, errorCallback){
          $http({
            method: 'GET',
            url: apiUrl + '/api/v2/user/getCodeStates'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        activate: function (activateData, successCallback, errorCallback){
            $http({
                method: 'GET',
                url: apiUrl + '/api/v2/user/activate',
                params: activateData
            })
            .success(successCallback)
            .error(errorCallback);
        }
    };
  });
