'use strict';

angular.module('anonymous', ['common'])
  .config(function ($stateProvider) {
    if(window.recurly && !recurly.configured) {
      recurly.configure({publicKey: window.configuration.recurlyPublicToken});
    }

    $stateProvider
		.state('anonymous', {
			abstract: true,
			templateUrl: 'app/anonymous/anonymous.html',
			controller: 'MainCtrl',
      resolve: {
        auth : function($auth, $rootScope, $location, $state, $translatePartialLoader, $translate) {
          return $auth.resolveAnonymous($rootScope, $location, $state, $translatePartialLoader, $translate);
        }
      }
		});
  });
