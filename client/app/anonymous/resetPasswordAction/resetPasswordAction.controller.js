'use strict';

angular.module('anonymous')
  .controller('ResetpasswordactionCtrl', function ($scope, $http, $state, $stateParams, loginService, fsConfirm, $filter) {
        var emailparam = $stateParams.email.replace(' ', '+');
        var keyparam = $stateParams.resetKey;

        angular.element('body').addClass('login-picture');
        $scope.processSubmitReset = function() {

            if((!$scope.newPassword || !$scope.confirmPassword) || ($scope.newPassword !== $scope.confirmPassword)){
                return;
            }
            var confirmData = {
                newPassword: $scope.newPassword,
                confirmPassword: $scope.confirmPassword,
                resetKey: keyparam,
                emailAddress: emailparam
            };

            loginService.resetPasswordAction(confirmData,
                function () {
                    fsConfirm('genericAlert', {
                      body: $filter('translate')('label.resetpasswordaction.PasswordChangedSuccessfully')
                    });
                    $state.go( 'anonymous.login' );
                    angular.element('body').removeClass('login-picture');
                }, function(error){
                    // error handler
                    console.log('NOT: ' + error.error.message);
                }
            );
        };
  });
