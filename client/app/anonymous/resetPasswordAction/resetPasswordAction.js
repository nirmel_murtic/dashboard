'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
      .state('anonymous.resetPasswordAction', {
        url: '/resetPasswordAction?email&resetKey',
        views: {
            '@anonymous': {
                templateUrl: 'app/anonymous/resetPasswordAction/resetPasswordAction.html',
                controller: 'ResetpasswordactionCtrl'
            }
        }
      });
  });