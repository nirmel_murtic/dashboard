'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
      .state('anonymous.resetPassword', {
        url: '/resetPassword',
        views: {
	        '@anonymous': {
	            templateUrl: 'app/anonymous/resetPassword/resetPassword.html',
        		controller: 'ResetPasswordCtrl'
	        }
	    }
      });
  });