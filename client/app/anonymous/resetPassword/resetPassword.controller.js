'use strict';

angular.module('anonymous')
  .controller('ResetPasswordCtrl', function ($scope, $http, vcRecaptchaService, loginService, fsConfirm) {
    $scope.publicKey = '';

    angular.element('body').addClass('login-picture');

    $scope.processReset = function() {
        var resetData = {
            emailAddress: $scope.resetPasswordemail.replace(' ', '+'),
            resetPasswordActionUrl: '#/resetPasswordAction'
        };
        if ($scope.publicKey !== ''){
            resetData.recaptchaChallenge = (vcRecaptchaService.data()).challenge;
            resetData.recaptchaResponse = (vcRecaptchaService.data()).response;
        }
        loginService.resetPassword(resetData,
            function (data) {
                if(!data.data.success && data.data.showCaptcha){
                    $scope.publicKey = data.data.recaptchaPublicKey;
                    if ($scope.publicKey !== '' && $scope.showCaptcha){
                        vcRecaptchaService.reload();
                    }
                    console.log(data.data.message);
                    fsConfirm('genericAlert', {
                      body: data.data.message
                    });
                } else {
                    $scope.publicKey = '';
                    $scope.resetPasswordForm.$setPristine();
                    $scope.resetPasswordemail = '';
                    console.log(data.data.message);
                    fsConfirm('genericAlert', {
                      body: data.data.message
                    });
                }
                $scope.showCaptcha = data.data.showCaptcha;
            }, function(error) {
                console.log(error.error.message);
                fsConfirm('genericAlert', {
                  body: error.error.message
                });
            }
        );
    };
});
