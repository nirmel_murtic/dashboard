'use strict';

describe('Controller: AnonymousCtrl', function () {

  // load the controller's module
  beforeEach(module('anonymous'));

  var AnonymousCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AnonymousCtrl = $controller('AnonymousCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
