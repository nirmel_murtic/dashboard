'use strict';

angular.module('anonymous')
  .controller('SuccessCtrl', function ($scope, $timeout, $rootScope, $state) {
  	angular.element('body').addClass('success-bg');
      $timeout(function(){
      	$state.go('main.align.projects');      	
      }, 5000);
  });