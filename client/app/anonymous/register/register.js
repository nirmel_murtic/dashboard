'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
      .state('anonymous.register', {
        url: '/signup',
        views: {
	        '@anonymous': {
	            templateUrl: 'app/anonymous/register/register.html',
    			    controller: 'RegisterCtrl'
	        },
          'footer@anonymous': {
              templateUrl: 'app/anonymous/footer/footer.html',
              controller: 'AnonymousFooterCtrl'
          }
        }
      })
      .state('anonymous.success', {
        url: '/signupSuccess',
        views: {
          '@anonymous': {
              templateUrl: 'app/anonymous/register/success.html',
              controller: 'SuccessCtrl'
          },
          'footer@anonymous': {
              templateUrl: 'app/anonymous/footer/footer.html',
              controller: 'AnonymousFooterCtrl'
          }
        }
      });
  });
