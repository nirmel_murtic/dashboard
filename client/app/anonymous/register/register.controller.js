'use strict';

angular.module('anonymous')
  .controller('RegisterCtrl', function ($scope, $rootScope, $http, vcRecaptchaService, $location, fsConfirm, loginService, billingService, $sce, $anchorScroll, $state, ngDialog, $window, $filter) {
      $scope.publicKey = '';

      $scope.selectedPlan = {
        planCode: 'f101',
        amount: 0
      };

      angular.element('body').addClass('login-picture');

      $scope.account = {
        billing: {}
      };

      loginService.getCodeStates(function(result) {
        $scope.account.codeStates = result.data.codeStates;
      }, function(error) {
        if(error.error && error.error.message){
          console.log(error.error.message);       
        } else {
          console.log(error);
        }
      });

      $scope.selectPlan = function(plan) {
        $scope.selectedPlan = plan;
        $scope.isFreePlan = plan.amount === 0;

        if ($location.hash() !== 'registerForm') {
          $location.hash('registerForm');
        } else {
          $anchorScroll();
        }
      };

      $scope.dataLayerSubmit = function() {
        try {
          $window.dataLayer.push({
            'firstName': $scope.account.accountFirstName,
            'lastName': $scope.account.accountLastName,
            'company': $scope.account.companyName,
            'email': $scope.account.emailAddress
          });
        } catch (e) {
          console.log(e);
        }
      };

      $scope.acceptTos = function(){
          $scope.tosAccepted = !!$scope.tosAccepted;
      };

      $scope.openTermsAndConditions = function(){
        ngDialog.open({
            template: 'app/anonymous/termsAndConditions/termsAndConditions.html',
            className: 'modal modal-large tos-modal',
            showClose: false,
            scope: $scope
        });
      };

      $scope.trustAsHtml = function(text) {
        return $sce.trustAsHtml(text);
      };

      billingService.getPlans(function(result) {
        $scope.availablePlans = result.data.plans;

        $scope.availablePlans.sort(function(a,b) {
          if(a.amount < b.amount) {
            return -1;
          } else if(a.amount > b.amount) {
            return 1;
          }

          if(b.planCode === 'b101') {
            return 1;
          } else {
            return 0;
          }
        });
      }, function(error) {
        $scope.processing = false;
        if(error.error && error.error.message){
          console.log(error.error.message);       
        } else {
          console.log(error);
        }
      });

      $scope.processRegisterUser = function(){
        console.log($scope);

        var continueProcessRegisterUser = function() {
          var registerData = {
            emailAddress: $scope.account.emailAddress,
            password: $scope.account.newPassword,
            confirmPassword: $scope.account.confirmPassword,
            companyName: $scope.account.companyName,
            phone: $scope.account.phone,
            address1: $scope.account.address1,
            address2: $scope.account.address2,
            city: $scope.account.city,
            state: $scope.account.state,
            zip: $scope.account.zip,
            planCode: $scope.selectedPlan.planCode,
            registerUserUrl: '#/activate'
          };

          if($scope.account.billing.token) {
            registerData.billingToken = $scope.account.billing.token;
          }

          if ($scope.publicKey !== ''){
            registerData.recaptchaChallenge = (vcRecaptchaService.data()).challenge;
            registerData.recaptchaResponse = (vcRecaptchaService.data()).response;
            if(!registerData.recaptchaResponse){
              fsConfirm('genericAlert', {
                body: $filter('translate')('label.register.PleaseEnterCaptcha')
              });
              return;
            }
          }
          if ($scope.account.accountFirstName || $scope.account.accountLastName){
            registerData.firstName = $scope.account.accountFirstName;
            registerData.lastName = $scope.account.accountLastName;
          }
          loginService.register(registerData,
            function(data) {

              if(!data.data.success && data.data.showCaptcha){
                $scope.publicKey = data.data.recaptchaPublicKey;
                if ($scope.publicKey !== '' && $scope.showCaptcha){
                  vcRecaptchaService.reload();
                }
                fsConfirm('genericAlert', {
                  body: data.data.message
                });
                console.log(data.data.message);
              } else {
                $scope.publicKey = '';

                $rootScope.$broadcast('auth:login-success', {
                  email: data.data.user.username,
                  /* jshint ignore:start */
                  refresh_token: data.data.refreshToken
                  /* jshint ignore:end */
                });

                $state.go('anonymous.success');
                angular.element('body').removeClass('login-picture');
              }

              $scope.showCaptcha = data.data.showCaptcha;
            }, function(error) {
              fsConfirm('genericAlert', {
                body: error.error.message
              });
              console.log(error.error.message);
            }
          );
        };

        if(!$scope.isFreePlan) {
          var billing = $scope.account.billing;

          recurly.token({
            number: billing.number,
            cvv: billing.cvv,
            month: billing.month,
            year: billing.year,
            first_name: billing.firstName, // jshint ignore:line
            last_name: billing.lastName, // jshint ignore:line
            address1: $scope.account.address1,
            city: $scope.account.city,
            state: $scope.account.state,
            country: 'US',
            postal_code: $scope.account.zip // jshint ignore:line
          }, function (err, token) {
            if (token) {
              $scope.$apply(function () {
                $scope.account.billing.token = token.id;
              });
            } else {
              delete $scope.account.billing.token;
            }

            continueProcessRegisterUser();
          });
        } else {
          continueProcessRegisterUser();
        }
     };
  });
