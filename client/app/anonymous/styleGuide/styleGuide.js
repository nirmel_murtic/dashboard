'use strict';

angular.module('anonymous')
  .config(function ($stateProvider) {
    $stateProvider
		.state('anonymous.styleGuide', {
			url: '/styleguide',
			views: {
		        '@anonymous': {
		            templateUrl: 'app/anonymous/styleGuide/styleGuide.html'
		        }
		    }
		});
});