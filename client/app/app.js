 'use strict';

angular.module('dashboardApp', [
	  'ngCookies',
	  'ngResource',
	  'ngSanitize',
    'ngAnimate',
    'anonymous',
    'main'
	])
	.config(function($httpProvider) {

	    $httpProvider.defaults.headers.post['Content-Type'] =
	        'application/x-www-form-urlencoded';
      $httpProvider.interceptors.push(function($q, $injector) {
        return {
          request : function(config) {
            var $auth;
            if(config.url && config.url.indexOf('/api/') > -1 &&
               config.url.indexOf('login') === -1 &&
               config.url.indexOf('validateToken') === -1 &&
               config.url.indexOf('refreshToken') === -1) {
              $auth = $injector.get('$auth');
              if($auth.refreshing) {
                return $auth.refreshing.promise.then(function() {
                  return config;
                });
              }
            }
            return config;
          },
          responseError : function(err) {
            try {
              if(__DEV__) {
                console.log('Http Request Error:', err);
              }
              var config = err.config;
              var url = config.url;

              var status = err.status;
              var $auth = $injector.get('$auth');
              var $state = $injector.get('$state');
              var fsConfirm = $injector.get('fsConfirm');

              var fsAppLock = $injector.get('fsAppLock');

              // token expired for api calls
              // 1. we are calling /api/*
              // 2. not refreshing token
              // 3. api call returns 401 error
              if(status === 401 &&
                 url.indexOf('login') === -1 &&
                 url.indexOf('/api/') !== -1) {

                fsAppLock.reset().end();

                if(url.indexOf('refreshToken') !== -1) {
                  if($auth.refreshing && $auth.refreshing.reject) {
                    $auth.refreshing.reject(err);
                  }
                  $auth.refreshing = null;
                  $state.go('anonymous.login');
                  return $q.reject(err);
                }
                if($auth.refreshing) {
                  return $auth.refreshing.promise.then(function() {
                    return $injector.get('$http')(config);
                  }, function() {
                    return $q.reject(err);
                  });
                }
                var email = $injector.get('$rootScope').fractalAccountInfo.username;
                $('#loading-bar').hide();
                $auth.signOut(false);
                var reloginPromise = fsConfirm('login', { email : email }).then(function() {
                  $auth.refreshing = null;
                  $('#loading-bar').show();
                  return $injector.get('$http')(config);
                }, function() {
                  $auth.refreshing = null;
                  $('#loading-bar').show();
                  $state.go('anonymous.login');
                  return $q.reject(err);
                });

                $auth.refreshing = $auth.refreshing || { promise : reloginPromise };

                return reloginPromise;
                /*
                console.log('Token expired, trying to refresh.');
                $auth.switchToRefreshUrl();
                // call to refresh token, if successful make the original failed call again
                return $auth.validateToken()
                  .then(function() {
                    console.log('Refresh token successful.');
                    $auth.switchToValidateUrl();
                    // use $injector to bypass circular dependency (which happens if we use $http)
                    return $injector.get('$http')(config);
                  }, function() {
                    $auth.switchToValidateUrl();
                    console.log('Refresh token failed');
                    var email = $injector.get('$rootScope').fractalAccountInfo.username;
                    return fsConfirm('login', { email : email }).then(function() {
                      return $injector.get('$http')(config);
                    }, function() {
                      $state.go('anonymous.login');
                      return $q.reject(err);
                    });
                  });
                */
              }
            } catch(e) {}
            return $q.reject(err);
          }
        };
      });
	})
	.config(function($authProvider) {
      window.configuration.labels = {
        'app.common': {
          'en': 'app/common.labels.en.json',
          'fr': 'app/common.labels.fr.json'
        },
        'app.main.main': {
          'en': 'app/main/main.labels.en.json'
        },
        'app.anonymous.anonymous': {
          'en': 'app/anonymous/anonymous.labels.en.json'
        },
        'app.main.align.align': {
          'en': 'app/main/align/align.labels.en.json',
          'fr': 'app/main/align/align.labels.fr.json'
        },
        'app.main.align.support.support': {
          'en': 'app/main/align/support/support.labels.en.json'
        },
        'app.main.act.act': {
          'en': 'app/main/act/act.labels.en.json'
        },
        'app.main.analyze.analyze': {
          'en': 'app/main/analyze/analyze.labels.en.json'
        },
        'app.main.library.library': {
          'en': 'app/main/library/library.labels.en.json',
          'fr': 'app/main/library/library.labels.fr.json'
        },
        'app.main.eventLog.eventLog': {
            'en': 'app/main/eventLog/eventLog.labels.en.json'
        }
      };

      var apiUrl = window.configuration.apiUrl;

      $authProvider.configure({
          apiUrl: apiUrl,
          tokenValidationPath: '/api/v2/user/validateToken',
          tokenRefreshPath: '/api/v2/user/refreshToken',
          emailSignInPath: '/api/v2/user/login',
	        storage: 'localStorage',
	        tokenFormat: {
	            'Authorization':'{{ Authorization }}'
	        },
	        handleLoginResponse: function(response) {
	            return this.parseResponse(response);
	        },
	        handleTokenValidationResponse: function(response) {
	            return this.parseResponse(response);
	        },
	        parseExpiry: function(headers) {
	            return (parseInt(headers.exp) * 1000) || null;
	        },
	        /* jshint ignore:start */
	        parseResponse: function(response) {
	            var jwtToken = jwt_decode(response.data.token.access_token);
	            var result = {
	                'access_token': response.data.token.access_token,
	                'token_type':   'Bearer',
	                'client_id':    jwtToken.client_id,
	                'exp':       	jwtToken.exp,
	                'jti':          jwtToken.jti,
	                'authorities':	jwtToken.authorities,
	                'email':		jwtToken.user_name,
                    'terms_conditions': response.data.showTermsAndConditions
	            };

	            if(response.data.token.refresh_token) {
	                result.refresh_token = response.data.token.refresh_token;
	            }

	            return result;
	        }
	        /* jshint ignore:end */
	    });
	})
    .config(function ($translateProvider) {
        $translateProvider.useLoader('$translatePartialLoader', {
            'urlTemplate': '{part}'
        });

        $translateProvider.preferredLanguage('en');
    })
	.config(function ($urlRouterProvider) {
		$urlRouterProvider.otherwise('/login');
	})
  .config(['ngClipProvider', function(ngClipProvider) {
    ngClipProvider.setPath(window.configuration.environment === 'local' ? 'bower_components/zeroclipboard/dist/ZeroClipboard.swf' : 'assets/ZeroClipboard.swf');
  }])
  .run(function(fsAppLock, $auth, $rootScope, fsConfirm, $state) {
    fsAppLock.on('idle', function() {
      if(!$state.includes('main')) {
        fsAppLock.end().start();
        return;
      }
      var email = $rootScope.fractalAccountInfo.username;
      $('#loading-bar').hide();
      $auth.signOut(false);
      var reloginPromise = fsConfirm('login', { email : email, originalEmail : email }).then(function() {
        $auth.refreshing = null;
        fsAppLock.end().start();
        $('#loading-bar').show();
      }, function() {
        $auth.refreshing = null;
        $('#loading-bar').show();
        $state.go('anonymous.login');
      });

      $auth.refreshing = $auth.refreshing || { promise : reloginPromise };
    });
  })
	.run(function($rootScope, $auth, $location, $state, $translatePartialLoader, $translate, $filter) {

        var buildTimestamp = '%%%BUILD_TIMESTAMP%%%';

        if(buildTimestamp.indexOf('BUILD_TIMESTAMP') === -1 && window.configuration.showBuildTimestamp) {
          $rootScope.buildTimestampString = 'Build update: ' + $filter('date')(buildTimestamp, 'MM/dd/yyyy \'at\' h:mma');
        }

        $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;

            if($state.current && $rootScope.toState.name.indexOf('main.') !== -1) {
              $auth.resolveMain($rootScope, $location, $state, $translatePartialLoader, $translate);
            }
        });

        $rootScope.$on('auth:logout-success', function() {
            console.log('Bye bye');
        });

        $rootScope.$on('auth:validation-success', function(ev, user) {
            console.log('User ' + user.email + ' validated successfully!');
        });

        $rootScope.$on('auth:login-success', function(ev, user) {
            console.log('Welcome: ' + user.email);
            /* jshint ignore:start */
            if(user.refresh_token) {
                $auth.persistData('refresh_token', user.refresh_token);
            }
            /* jshint ignore:end */
        });

        $rootScope.$on('auth:login-error', function(ev, reason) {
            console.log(reason.error.message);
        });

        $rootScope.postTranslationsRefresh = function() {
          $rootScope.refreshAgain = false;
          var newRefreshPromise = $translate.refresh(window.configuration.currentLang);

          $rootScope.refreshPromise = newRefreshPromise;

          return newRefreshPromise;
        };

        $rootScope.refreshTranslations = function() {
          if(!$rootScope.refreshPromise) {
            var refreshPromise = $translate.refresh(window.configuration.currentLang);

            $rootScope.refreshPromise = refreshPromise;

            return refreshPromise;
          } else if(!$rootScope.refreshAgain) {
            $rootScope.refreshAgain = true;

            $rootScope.newRefreshPromise = $rootScope.refreshPromise.then(function() {
              return $rootScope.postTranslationsRefresh();
            }, function(reason) {
              console.log('Error while loading labels: ' + reason);
              return $rootScope.postTranslationsRefresh();
            });
          }

          return $rootScope.newRefreshPromise;
        };

        $rootScope.$on('$translatePartialLoaderStructureChanged', function () {
          $rootScope.refreshTranslations();
        });

        if(window.configuration.walkMeUrl) {
          var walkme = document.createElement('script');
          walkme.type = 'text/javascript';
          walkme.async = true;
          walkme.src = window.configuration.walkMeUrl;

          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(walkme, s);
        }
    });
