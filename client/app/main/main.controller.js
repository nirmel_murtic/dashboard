'use strict';

angular.module('main')
  .controller('MainCtrl', function ($scope, $state,userService, $rootScope, $auth, $stateParams, library, $timeout) {
      $scope.state = $state;
      angular.element('body').removeClass('success-bg');
      angular.element('body').removeClass('login-picture');
      $rootScope.navState = $rootScope.navState || { open: false };

      var user = $stateParams.user;

      if(!user) {
      	return;
      }

      $rootScope.fadeLoggedTeam = function() {
        $timeout(function() {
          $scope.loggedInAsTeam = true;
          $timeout(function() {
            $scope.loggedInAsTeam = false;
          }, 1500);
        }, 0);
      };    

      $rootScope.screenWidth = function() {
        return window.innerWidth;
      };
  });
