'use strict';

angular.module('library', ['main'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.library', {
        url: '/library',
        views: {
        	'@main': {
        		templateUrl: 'app/main/library/landing.html',
        		controller: 'LibraryCtrl'
        	}
        },
        resolve: {
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/library/library');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
