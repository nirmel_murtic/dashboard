'use strict';

angular.module('library')
  .controller('ImageLibraryCtrl', function ($scope, $http, $rootScope, videoUploader, targetingSpecs, ngDialog, $window, fsConfirm, library, $upload, apiUrl, LibraryCardGenerator, $filter, withInput) {

    $scope.getImageEndpoint = apiUrl + '/api/v2/image/get?guid=';

    $scope.applyImages = function(response){
      $scope.loadingLibraryCards = false;
      for (var i in response.data) {
        $scope.imageCards.push(response.data[i]);
      }
    };

    $scope.queryIn = false;

    $window.onclick = function(event) {
      for(var i=0; i<event.target.classList.length; i++) {
        if(event.target.classList[i] === 'form-control') {
          return;
        }
      }

      $scope.queryIn = false;
      $scope.$apply();
    };

    $scope.searchMultiple = function (val) {
      return (angular.lowercase(val.originalName).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
              angular.lowercase(val.height.toString()).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
              angular.lowercase(val.width.toString()).indexOf(angular.lowercase($scope.query) || '') !== -1);
    };

    $scope.setQuery = function(name) {
      if(name) {
        $scope.query = name;
        $scope.queryIn = false;
      }
    };

    $scope.showResults = function() {
      if(!$scope.queryIn) {
        $scope.queryIn = true;
      }
    };

    $scope.containsObject = function (obj, list) {
      var i;
      for (i = 0; i < list.length; i++) {
        if (angular.equals(list[i], obj)) {
          return true;
        }
      }
      return false;
    };

    var showError = function(response){
      console.log(response);
    };

    $scope.openLibrary = function () {
      if(!$scope.ngDialogData){
        $scope.disableLoadSelected = true;
      }

      $scope.files = [];

      $scope.selectedCards = [];
      $scope.imageCards = [];
      $scope.loadingLibraryCards = true;
      library.getLibraryObjectsByType('image', $scope.applyImages, showError);
    };

    $scope.loadSelectedCards = function(selectedCards){
      var cancelCallBack = function () {
        return;
      };
      var useLibraryDataAndCloseModal = function () {

        $scope.ngDialogData.promise.resolve(selectedCards);
        $scope.closeThisDialog();
      };

      // Check that the options passed as parameters are correct - twitter website Cards - facebook multiproduct
      if ($scope.ngDialogData.minSize) {

        var message = '';
        var warningMessage = $filter('translate')('label.library.Theimageyouselecteddoesntsatisfythefollowingrequirements,areyousureyouwanttocontinue?');
        var title = $filter('translate')('label.library.Thereisaproblemwiththisimage');

        if (($scope.ngDialogData.aspectRatio) && $scope.ngDialogData.aspectRatio !== (selectedCards[0].width / selectedCards[0].height)) {
          var aspectRatioFormatted = $scope.ngDialogData.aspectRatio <= 1 ? 1 + ':' + 1 / $scope.ngDialogData.aspectRatio : $scope.ngDialogData.aspectRatio + ':' + 1;
          message += $filter('translate')('label.library.Theimageaspectratioshouldbe') + aspectRatioFormatted + '. ';
        }
        if ($scope.ngDialogData.minSize.height > selectedCards[0].height && $scope.ngDialogData.minSize.width > selectedCards[0].width) {
          message += $filter('translate')('label.library.Theimagesizeshouldbeatleast') + $scope.ngDialogData.minSize.width + 'x' + $scope.ngDialogData.minSize.height + $scope.ngDialogData.minSize.unit;
        }

        //instagram check
        if ($scope.ngDialogData.minSize.width > selectedCards[0].width) {
          message += $filter('translate')('label.library.Theimagesizeshouldbeatleast') + $scope.ngDialogData.minSize.width + 'px wide';
          selectedCards[0].imageErrorWidthOrHeight = true;
        }

        // If the image doesn't satisfy the requirements, warn the user (but do not block him)
        if (message) {
          fsConfirm('generic', {
            title: title,
            body: warningMessage + message
          }).then(useLibraryDataAndCloseModal, cancelCallBack);
        } else { // If the image satisfies the requirements, use it
          useLibraryDataAndCloseModal();
        }
      } else {
        useLibraryDataAndCloseModal();
      }
    };

    $scope.deleteObject = function (object, index) {
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var success = function (response) {
        // $scope.openLibrary();
        $scope.selectedCards = [];
        console.log(response);
      };

      function confirmCallback(){
        $scope.imageCards.splice(index, 1);
        if($scope.selectedCards){
          $scope.selectedCards.splice(index, 1);
        }
        library.deleteLibraryObjectById(teamId, object.id, 'image').then(success);
      }
      fsConfirm('generic', {
        body: $filter('translate')('label.library.Areyousureyouwanttodelete') + ' ' +  object.originalName + '?'
      }).then(confirmCallback);
    };

    $scope.loadOnDblClick = function(card, $event) {
      $scope.selectCard(card, $event, true);
      if($scope.selectedCards.length > 0 && !$scope.disableLoadSelected) {
        $scope.loadSelectedCards($scope.selectedCards);
      }
    };

    $scope.selectCard = function (card, $event, dblc) {
      $scope.selectOne = true;

      var i;
      if ($event.target.className === 'expand' || $event.target.className === 'fake-link') {
        return;
      }

      if(!dblc) {
        if ($scope.containsObject(card, $scope.selectedCards)) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        } else {
          if ($scope.selectOne && $scope.selectedCards.length >= 1) {
            i = $scope.selectedCards.indexOf(card);
            $scope.selectedCards.splice(i, 1);
          }
          $scope.selectedCards.push(card);
        }
      } else {
        if($scope.selectOne && $scope.selectedCards.length >= 1) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        }
        $scope.selectedCards.push(card);
      }
    };

    $scope.onFileSelect = function (files) {
      for (var i = 0; i < files.length; i++) {
        $scope.files.push(files[i]);
      }
      if($scope.files && $scope.files.length > 0) {
        $scope.uploadFiles($scope.files);
      }
    };

    $scope.checkIfFileChanged = function(file) {
      var checker = true;
      if(file.size > 0) {
        return checker;
      } else {
        return !checker;
      }
    };

    $scope.showInvalidFile = function() {
      fsConfirm('genericAlert', {
        body: 'Changes made to the selected file prevent system from uploading, please select the file again.'
      });
    };

    $scope.uploadFiles = function (files) {

      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var progressCallback = function (evt) {
        $scope.libraryUploadingScreen.videoProgress = parseInt(100 * evt.loaded / evt.total);
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      };
      $scope.uploadingFiles = true;

      $scope.libraryUploadingScreen = {
        message: 'Uploading to library ',
        status: 'show',
        uploading: true
      };

      var imageSuccessAndLoad = function (data) {
        // file is uploaded successfully
        console.log('Image upload success!' , data);
        $scope.openLibrary();
        $scope.uploadingFiles = false;
        $scope.libraryUploadingScreen.uploading = false;
        delete $scope.libraryUploadingScreen.message;
        $scope.files = [];
      };

      var imageSuccess = function (response) {
        $scope.files = [];
        console.log(response);
      };

      for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // This should be moved to service.

          if (files[i].type.toLowerCase().indexOf('image') >= 0) {
              if(!$scope.checkIfFileChanged(file)) {
                $scope.showInvalidFile();
                $scope.uploadingFiles = false;
                $scope.libraryUploadingScreen.uploading = false;
                delete $scope.libraryUploadingScreen.message;
                return;
              }
            if (i === files.length - 1) {
              library.uploadImage($scope, teamId, file, progressCallback, imageSuccessAndLoad);
            } else {
              library.uploadImage($scope, teamId, file, progressCallback, imageSuccess);
            }
          } else {
            $scope.uploadingFiles = false;
            $scope.libraryUploadingScreen.uploading = false;
            delete $scope.libraryUploadingScreen.message;
            $scope.files = [];
            fsConfirm('genericWithLink', {
              notSupported: $filter('translate')('label.library.ThisFileFormatIsNotSupportedForThisLibrary'),
              fileTypes: $filter('translate')('label.library.SupportedFormatsImage'),
              contact: $filter('translate')('label.library.IfYouBelieveThisMessageIsShownByError'),
              mailLink: $filter('translate')('label.library.Help@accompliceIo')
            });
            return;
          }
      }
    };

    $scope.editObjectName = function(object) {
      withInput.setInput(object.originalName);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.library.ChangeAssetName'),
        title: $filter('translate')('label.library.MyAssetName'),
        maxLength: 255
      }).then(function () {

        var newName = withInput.getInput();
        //get extension
        var ext = newName.match(/\.[0-9a-z]{1,5}$/, '');
        if(ext){
          newName = newName.replace(/\.[0-9a-z]{1,5}$/, '');
        }
        //remove special characters
        newName = newName.replace(/[\. ,:<>;+|*&%#]+/g, '');
        //add extension back
        if(ext){
          newName += ext[0];
        }
        library.updateObjectName(object.id, 'image', newName, function () {
          object.originalName = newName;
          object.name = newName;
        }, function (error) {
          console.log(error.error);
        });
      });
    };

    $scope.openLibrary();


  });
