'use strict';

angular.module('main')
  .service('videoUploader', function ($http, $rootScope, $timeout, apiUrl, $cacheFactory, $interval) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var cache = $cacheFactory('videoUploaderCache');

    var cacheValues = function(key,data){
      cache.put(key,data);
    };

    var getValue = function(key){
      return cache.get(key);
    };

    var videoUploader = function(){ 
      $rootScope.videoProgress = getValue('videoProgress');
      $rootScope.videoProgressId = getValue('videoProgressId');
    };

    videoUploader.prototype.getProgress = function($scope, videoProgressId){
      var videoProgressInterval = $interval(function(){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/library/video/upload/progress/?videoGuid=' + videoProgressId || getValue('videoProgressId')
        })
        .success(function(response){
          cacheValues('videoProgress', Math.ceil(response));
          cacheValues('videoProgressId', videoProgressId);
          $rootScope.videoProgress = getValue('videoProgress');
          $rootScope.videoProgressId = getValue('videoProgressId');
          $scope.libraryUploadingScreen.videoProgress = $rootScope.videoProgress;
          if(response >= 100){
            console.log('meh');
            $scope.openLibrary();
            $scope.uploadingFiles = false;
            $scope.showUploadView = false;
            $scope.files = [];
            cacheValues('videoProgress', null);
            cacheValues('videoProgressId', null);
            $rootScope.videoProgress = null;
            $scope.libraryUploadingScreen.uploading = false;
            delete $scope.libraryUploadingScreen.message;
            delete $scope.libraryUploadingScreen.videoProgress;
            $interval.cancel(videoProgressInterval);
          }
        });        
      }, 2000);
    };    

    return videoUploader;
});