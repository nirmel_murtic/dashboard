'use strict';

angular.module('main')
  .factory('LibraryCardGenerator', function ($rootScope) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    return{
    	getAudienceCard: function(data){
	    	var variants = angular.fromJson(data.json).audience;
			var card = {};
			card.id = data.id;
			card.channel = data.channelName;
			card.pills = [];
			card.modifiedOn = data.modifiedOn;
			card.modifiedBy = data.modifiedBy;
      		card.name = data.name ? data.name : angular.fromJson(data.json).name;
			for(var j in variants){
				for(var k = 0; k < variants[j].length; k++){
					for(var m = 0; m < variants[j][k].values.length; m++){
						card.pills.push(variants[j][k].values[m]);
					}
				}
			}
	        // card.audience = data;
			return card;
	    },

	    getCreativeCard: function(data){
	    	var creative = angular.fromJson(data.json).creative;
			var card = {};
			card.id = data.id;
			card.channel = data.channelName;
			card.text = creative.text;
			card.image = creative.image;
			card.adType = creative.adType;
			card.modifiedOn = data.modifiedOn;
			card.modifiedBy = data.modifiedBy;
			card.page = angular.fromJson(data.json);
      		card.name = data.name ? data.name : angular.fromJson(data.json).name;
	        // card.audience = data;
			return card;
	    },

      	getKeywordCard: function(data){
	    	var keyword = angular.fromJson(data.json).keyword;
			var card = {};
			card.id = data.id;
			card.channel = data.channelName;
			card.pills = keyword.slice(0, 10);
			card.numKeywords = keyword.length;
			card.modifiedOn = data.modifiedOn;
			card.modifiedBy = data.modifiedBy;
      		card.name = data.name ? data.name : angular.fromJson(data.json).name;
	        // card.audience = data;
			return card;
	    },


	    getComboCard:  function(data){
        var card = {};
        card.id = data.id;
        card.name = data.name;
        card.channel = data.channelName;
        card.modifiedOn = data.modifiedOn;
        card.modifiedBy = data.modifiedBy;
        card.numberAudiences = data.numberAudiences;
        card.numberCreatives = data.numberCreatives;
        card.projectGoalId = data.projectGoalId;

        var findGoal = [];

        for(var i=0;i<$rootScope.fractalAccountInfo.currentTeam.projects.length;i++) {
          findGoal = _.find($rootScope.fractalAccountInfo.currentTeam.projects[i].goals, {'id': data.projectGoalId});

          if(findGoal) {
            card.goalName = findGoal.formattedGoalName;
          }
        }

        return card;
		},

		getCampaignCard:  function(data){
			console.log('campaign data: ', data);
			var card = {};
			card.id = data.id;
			card.name = data.name;
			card.channels = data.channels;
			card.modifiedOn = data.modifiedOn;
			card.modifiedBy = data.modifiedBy;
			return card;
		}
	};

  });
