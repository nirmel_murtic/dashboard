'use strict';

angular.module('library')
  .controller('CampaignLibraryCtrl', function ($scope, $http, $rootScope, videoUploader, $window, targetingSpecs, ngDialog, fsConfirm, library, $upload, apiUrl, LibraryCardGenerator, $filter, withInput) {

    $scope.applyCampaigns = function(response){
      var card;
      $scope.loadingLibraryCards = false;
      var campaigns = response.data.CAMPAIGN;
      for (var i = 0; i < campaigns.length; i++) {
        card = LibraryCardGenerator.getCampaignCard(campaigns[i]);
        $scope.campaignCards.push(card);
      }
    };

    $scope.queryIn = false;

    $window.onclick = function(event) {
      for(var i=0; i<event.target.classList.length; i++) {
        if(event.target.classList[i] === 'form-control') {
          return;
        }
      }

      $scope.queryIn = false;
      $scope.$apply();
    };

    $scope.setQuery = function(name) {
      if(name) {
        $scope.query = name;
        $scope.queryIn = false;
      }
    };

    $scope.showResults = function() {
      if(!$scope.queryIn) {
        $scope.queryIn = true;
      }
    };

    $scope.containsObject = function (obj, list) {
      var i;
      for (i = 0; i < list.length; i++) {
        if (angular.equals(list[i], obj)) {
          return true;
        }
      }
      return false;
    };

    var showError = function(response){
      console.log(response);
    };

    $scope.openLibrary = function () {
      if(!$scope.ngDialogData){
        $scope.disableLoadSelected = true;
      }

      $scope.selectedCards = [];
      $scope.campaignCards = [];
      $scope.loadingLibraryCards = true;
      library.getLibraryObjectsByType('campaign', $scope.applyCampaigns, showError);
    };

    $scope.loadSelectedCards = function(selectedCards){

      library.loadTemplate('campaign', selectedCards[0].id, -1)
        .then(function (response) {
          var data = angular.fromJson(response.newObjectJSON);
          data.data.superCmpId = response.newObjectId;
          $scope.ngDialogData.promise.resolve(data);
        });

      $scope.closeThisDialog();
    };

    $scope.deleteObject = function (object, index) {
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var success = function (response) {
        // $scope.openLibrary();
        $scope.selectedCards = [];
        console.log(response);
      };

      function confirmCallback(){
        $scope.campaignCards.splice(index, 1);
        if($scope.selectedCards){
          $scope.selectedCards.splice(index, 1);
        }
        library.deleteLibraryObjectById(teamId, object.id, 'campaign').then(success);
      }
      fsConfirm('generic', {
        body: $filter('translate')('label.library.Areyousureyouwanttodelete') + ' ' +  object.name + '?'
      }).then(confirmCallback);
    };

    $scope.loadOnDblClick = function(card, $event) {
      $scope.selectCard(card, $event, true);
      if($scope.selectedCards.length > 0 && !$scope.disableLoadSelected) {
        $scope.loadSelectedCards($scope.selectedCards);
      }
    };

    $scope.selectCard = function (card, $event, dblc) {
      $scope.selectOne = true;

      var i;
      if ($event.target.className === 'expand' || $event.target.className === 'fake-link') {
        return;
      }

      if(!dblc) {
        if ($scope.containsObject(card, $scope.selectedCards)) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        } else {
          if ($scope.selectOne && $scope.selectedCards.length >= 1) {
            i = $scope.selectedCards.indexOf(card);
            $scope.selectedCards.splice(i, 1);
          }
          $scope.selectedCards.push(card);
        }
      } else {
        if($scope.selectOne && $scope.selectedCards.length >= 1) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        }
        $scope.selectedCards.push(card);
      }
    };


    $scope.editObjectName = function(object) {
      withInput.setInput(object.name);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.library.ChangeAssetName'),
        title: $filter('translate')('label.library.MyAssetName')
      }).then(function () {

        var newName = withInput.getInput();
        object.name = newName;

        library.updateObjectName(object.id, 'campaign', newName, function () {
          console.log('renamed success');
        }, function (error) {
          console.log(error.error);
        });
      });
    };

    $scope.openLibrary();


  });
