'use strict';

angular.module('library')
  .controller('ComboLibraryCtrl', function ($scope, $http, $rootScope, videoUploader, $window, targetingSpecs, ngDialog, fsConfirm, library, $upload, apiUrl, LibraryCardGenerator, $filter, withInput) {

    $scope.applyCombos = function(response){
      var card;
      $scope.loadingLibraryCards = false;
      var combos = response.data.ADCOMBO;
      for (var j = 0; j < combos.length; j++) {
        card = LibraryCardGenerator.getComboCard(combos[j]);
        $scope.comboCards.push(card);
      }
    };

    $scope.queryIn = false;

    $window.onclick = function(event) {
      for(var i=0; i<event.target.classList.length; i++) {
        if(event.target.classList[i] === 'form-control') {
          return;
        }
      }

      $scope.queryIn = false;
      $scope.$apply();
    };

    $scope.setQuery = function(name) {
      if(name) {
        $scope.query = name;
        $scope.queryIn = false;
      }
    };

    $scope.showResults = function() {
      if(!$scope.queryIn) {
        $scope.queryIn = true;
      }
    };

    $scope.containsObject = function (obj, list) {
      var i;
      for (i = 0; i < list.length; i++) {
        if (angular.equals(list[i], obj)) {
          return true;
        }
      }
      return false;
    };

    var showError = function(response){
      console.log(response);
    };

    $scope.openLibrary = function () {
      if(!$scope.ngDialogData){
        $scope.disableLoadSelected = true;
      }

      $scope.selectedCards = [];
      $scope.comboCards = [];
      $scope.loadingLibraryCards = true;
      library.getLibraryObjectsByType('adCombo', $scope.applyCombos, showError);
    };

    $scope.loadSelectedCards = function(selectedCards){
      var i, j = 0;
      for (i = 0, j = selectedCards.length; i < j; i++) {
        // Save then add to combo list in call back
        library.loadTemplate('adCombo', selectedCards[i].id, $scope.ngDialogData.parentCmpId)
          .then(function (combo) {
            var comboData = angular.fromJson(combo.newObjectJSON);
            comboData.id = combo.newObjectId;
            $scope.ngDialogData.promise.resolve(comboData);
          }); //jshint ignore:line
      }
      $scope.closeThisDialog();
    };

    $scope.deleteObject = function (object) {
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var success = function (response) {
        // $scope.openLibrary();
        $scope.selectedCards = [];
        console.log(response);
      };

      function removeObject(object){
        for(var i = 0; i < $scope.comboCards.length; i++){
          if(object.id === $scope.comboCards[i].id){
            $scope.comboCards.splice(i, 1);
          }
        }
        for(var j = 0; j < $scope.selectedCards.length; j++){
          if(object.id === $scope.selectedCards[j].id){
            $scope.selectedCards.splice(j, 1);
          }
        }
      }

      function confirmCallback(){
        removeObject(object);
        library.deleteLibraryObjectById(teamId, object.id, 'adCombo').then(success);
      }

      fsConfirm('generic', {
        body: $filter('translate')('label.library.Areyousureyouwanttodelete') + ' ' +  object.name + '?'
      }).then(confirmCallback);
    };

    $scope.loadOnDblClick = function(card, $event) {
      $scope.selectCard(card, $event, true);
      if($scope.selectedCards.length > 0 && !$scope.disableLoadSelected) {
        $scope.loadSelectedCards($scope.selectedCards);
      }
    };

    $scope.selectCard = function (card, $event, dblc) {
      $scope.selectOne = true;

      var i;
      if ($event.target.className === 'expand' || $event.target.className === 'fake-link') {
        return;
      }

      if(!dblc) {
        if ($scope.containsObject(card, $scope.selectedCards)) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        } else {
          if ($scope.selectOne && $scope.selectedCards.length >= 1) {
            i = $scope.selectedCards.indexOf(card);
            $scope.selectedCards.splice(i, 1);
          }
          $scope.selectedCards.push(card);
        }
      } else {
        if($scope.selectOne && $scope.selectedCards.length >= 1) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        }
        $scope.selectedCards.push(card);
      }
    };

    $scope.filterCombos = function (value) {
      if($scope.ngDialogData) {
        //filter combos by goal
        if (value.channel === $scope.ngDialogData.channel.toUpperCase() &&
          value.projectGoalId === $scope.ngDialogData.goalId) {
          return true;
        }
      } else {
        return value;
      }
    };


    $scope.editObjectName = function(object) {
      withInput.setInput(object.name);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.library.ChangeAssetName'),
        title: $filter('translate')('label.library.MyAssetName')
      }).then(function () {

        var newName = withInput.getInput();

        library.updateObjectName(object.id, 'adCombo', newName, function () {
          object.name = newName;
        }, function (error) {
          console.log(error.error);
        });
      });
    };

    $scope.openLibrary();


  });
