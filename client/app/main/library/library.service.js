'use strict';

angular.module('main')
  .factory('library', function ($http, $rootScope, ngDialog, apiUrl, $translatePartialLoader, $upload, identityConverter, $q) {
         // How it should be refactored eventually.  lots more files.
        var libraryMap = {
          'Campaigns': {
            controller: 'CampaignLibraryCtrl',
            template: 'app/main/library/campaign/campaignLibrary.html'
          },
          'Combos': {
            controller: 'ComboLibraryCtrl',
            template: 'app/main/library/combo/comboLibrary.html'
          },
          'Audiences': {
            controller: 'AudienceLibraryCtrl',
            template: 'app/main/library/audience/audienceLibrary.html'
          },
          'Images': {
            controller: 'ImageLibraryCtrl',
            template: 'app/main/library/image/imageLibrary.html'
          },
          'Videos': {
            controller: 'VideoLibraryCtrl',
            template: 'app/main/library/video/videoLibrary.html'
          },
          'Creatives': {
            controller: 'CreativeLibraryCtrl',
            template: 'app/main/library/creative/creativeLibrary.html'
          },
          'Keywords': {
            controller: 'KeywordLibraryCtrl',
            template: 'app/main/library/keyword/keywordLibrary.html'
          }
        };

    return {

        openLibrary: function(type, opts){
            var def = $q.defer();
            if(opts){
              opts.promise = def;              
            }

            $translatePartialLoader.addPart('app/main/library/library');
            $rootScope.refreshTranslations().then(function() {
              ngDialog.open({
                template: libraryMap[type].template,
                // template: 'app/main/library/library.html',
                controller: libraryMap[type].controller,
                // controller: 'LibraryCtrl',
                data: opts,
                className: 'modal modal-large'
                // scope: parentScope
              });
            });
            return def.promise;
        },

        loadTemplate: function(type, objectId, parentObjectId){ 
          var def = $q.defer();        
          $http({
                  method: 'GET',
                  url: apiUrl + '/api/v2/library/load/template?objectId=' + objectId + 
                    '&parentObjectId=' + parentObjectId + '&teamId=' + 
                    $rootScope.fractalAccountInfo.currentTeam.id + '&libraryObjectType=' + type
              })
          .success(function(data){
            var template = data.data;
            def.resolve(template);
          })
          .error(function(){
            def.reject('Failed to load template');
          });
          return def.promise;
        },

        getLibraryObjectsByType: function (type, successCallback, errorCallback){
          if(type === 'image'){
            return $http({
                method: 'GET',
                url: apiUrl + '/api/v2/library/get/all/images/' + $rootScope.fractalAccountInfo.currentTeam.id + '?isIcon=false&fields=accountId,createdAt,image,fullGuid,width,height,name,originalName'
            })
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
          } else if(type === 'video'){
            return $http({
                method: 'GET',
                url: apiUrl + '/api/v2/library/get/all/videos/' + $rootScope.fractalAccountInfo.currentTeam.id + '?fields=accountId,createdAt,fullGuid'
            })
            .success(successCallback)
            .error(errorCallback);
          } else{
            return $http({
                method: 'GET',
                url: apiUrl + '/api/v2/library/get/all/template/' + $rootScope.fractalAccountInfo.currentTeam.id + '?libraryObjectType=' + type
            })
            .success(successCallback)
            .error(errorCallback);
          }
        },

        uploadImage: function($scope, teamId, file, progressCallback, successCallback){
          $scope.upload = $upload.upload({
              url: apiUrl + '/api/v2/library/image/upload/' + teamId,
              file: file
          }).progress(progressCallback).success(successCallback);
        },

        uploadVideo: function($scope, teamId, file, progressCallback, successCallback){
          $scope.upload = $upload.upload({
            url: apiUrl + '/api/v2/library/video/upload/' + teamId + '?isIcon=false',
            file: file
          }).progress(progressCallback).success(successCallback);
        },
        deleteLibraryObjectById: function (teamId, libId, type){
          if(type === 'image'){
            return $http({
                method: 'DELETE',
                url: apiUrl + '/api/v2/library/image/' + teamId + '?imageId=' + libId
            });
            // .success(successCallback)
            // .error(errorCallback);
          } else if(type === 'video'){
             return $http({
                method: 'DELETE',
                url: apiUrl + '/api/v2/video/delete?id=' + libId
            });
          } else {
            return $http({
                method: 'DELETE',
                url: apiUrl + '/api/v2/library/delete/template/' + teamId + '?objectId=' + libId + '&libraryObjectType=' + type
            });
            // .success(function(){
            //   successCallback(successCallback(0));
            // })
            // .error(errorCallback);
          }
        },
        updateObjectName: function (libId, type, name, successCallback, errorCallback) {
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/library/updateName?libraryId=' + libId + '&libraryType=' + type + '&name=' + name
          })
            .success(successCallback)
            .error(errorCallback);
        },
        deleteImageFromTeam: function(imageId, teamId, successCallback, errorCallback) {
          $http({
            method: 'DELETE',
            url: apiUrl + '/api/v2/library/image/' + teamId + '?imageId=' + imageId
          })
            .success(successCallback)
            .error(errorCallback);
        }
    };
  });
