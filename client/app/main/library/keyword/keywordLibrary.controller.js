'use strict';

angular.module('library')
  .controller('KeywordLibraryCtrl', function ($scope, $http, $rootScope, targetingSpecs, fsConfirm, library, apiUrl, LibraryCardGenerator, $filter, withInput) {

  	$scope.applyKeywords = function(response){
  		$scope.loadingLibraryCards = false;
		  $scope.keywordCards = _.map(response.data.KEYWORD, function(keyword){
        return LibraryCardGenerator.getKeywordCard(keyword);
      });
    };

    $scope.apiUrl = apiUrl;

  	$scope.containsObject = function (obj, list) {
  	  var i;
  	  for (i = 0; i < list.length; i++) {
  	    if (angular.equals(list[i], obj)) {
  	      return true;
  	    }
  	  }
  	  return false;
  	};

  	var showError = function(response){
  		console.log(response);
  	};

  	$scope.openLibrary = function () {
      if(!$scope.ngDialogData){
        $scope.disableLoadSelected = true;
      }

  	  $scope.selectedCards = [];
  	  $scope.keywordCards = [];
  	  $scope.loadingLibraryCards = true;
  	  library.getLibraryObjectsByType('keyword', $scope.applyKeywords, showError);
  	};

    $scope.loadSelectedCards = function(selectedCards){
      var currentComboId = $scope.ngDialogData.currentComboId;
      library.loadTemplate('keyword', selectedCards[0].id, currentComboId)
        .then(function (keywords) { //jshint ignore:line
          var keywordData = angular.fromJson(keywords.newObjectJSON); //jshint ignore:line
          keywordData.keyword.keyword = angular.fromJson(keywordData.keyword.keyword);
          keywordData.keyword.keyword.id = keywords.newObjectId;
          $scope.ngDialogData.promise.resolve(keywordData.keyword.keyword);
          $scope.closeThisDialog();
        }); //jshint ignore:line
    };

    $scope.deleteObject = function (object) {
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var success = function (response) {
        // $scope.openLibrary();
        console.log(response);
      };

      function removeObject(object){
        for(var i = 0; i < $scope.keywordCards.length; i++){
          if(object.id === $scope.keywordCards[i].id){
            $scope.keywordCards.splice(i, 1);
          }
        }
        for(var j = 0; j < $scope.selectedCards.length; j++){
          if(object.id === $scope.selectedCards[j].id){
            $scope.selectedCards.splice(j, 1);
          }
        }
      }

      function confirmCallback(){
        removeObject(object);
        library.deleteLibraryObjectById(teamId, object.id, 'keyword').then(success);
      }
      fsConfirm('generic', {
        body: $filter('translate')('label.library.Areyousureyouwanttodelete') + ' ' +  object.name + '?'
      }).then(confirmCallback);
    };

    $scope.editObjectName = function(object) {
      withInput.setInput(object.name);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.library.ChangeAssetName'),
        title: $filter('translate')('label.library.MyAssetName')
      }).then(function () {

        var newName = withInput.getInput();

        library.updateObjectName(object.id, 'keyword', newName, function () {
          object.name = newName;
        }, function (error) {
          console.log(error.error);
        });
      });
    };

    //filter audiences by channel

    $scope.filterAudiences = function(value){
      if($scope.ngDialogData){
        if (value.channel === $scope.ngDialogData.channel.toUpperCase()) {
          return true;
        }
      } else {
        return true;
      }
    };

  	$scope.loadOnDblClick = function(card, $event) {
  	  $scope.selectCard(card, $event, true);
  	  if($scope.selectedCards.length > 0 && !$scope.disableLoadSelected) {
  	    $scope.loadSelectedCards($scope.selectedCards);
  	  }
  	};

  	$scope.selectCard = function (card, $event, dblc) {
  	  var i;
  	  if ($event.target.className === 'expand' || $event.target.className === 'fake-link') {
  	    return;
  	  }
  	  if(!dblc) {
  	    if ($scope.containsObject(card, $scope.selectedCards)) {
  	      i = $scope.selectedCards.indexOf(card);
  	      $scope.selectedCards.splice(i, 1);
  	    } else {
  	      $scope.selectedCards.push(card);
  	    }
  	  } else {
  	    $scope.selectedCards.push(card);
  	  }
  	};
  	$scope.openLibrary();
  });
