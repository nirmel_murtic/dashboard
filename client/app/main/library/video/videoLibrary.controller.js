'use strict';

angular.module('library')
  .controller('VideoLibraryCtrl', function ($scope, $http, $rootScope, videoUploader, $window, targetingSpecs, ngDialog, fsConfirm, library, $upload, apiUrl, LibraryCardGenerator, $filter, withInput) {

    $scope.getImageEndpoint = apiUrl + '/api/v2/image/get?guid=';

    $scope.applyVideo = function(response){
      $scope.loadingLibraryCards = false;
      for (var i in response.data) {
        $scope.videoCards.push(response.data[i]);
      }
    };

    $scope.queryIn = false;

    $window.onclick = function(event) {
      for(var i=0; i<event.target.classList.length; i++) {
        if(event.target.classList[i] === 'form-control') {
          return;
        }
      }

      $scope.queryIn = false;
      $scope.$apply();
    };

    $scope.setQuery = function(name) {
      if(name) {
        $scope.query = name;
        $scope.queryIn = false;
      }
    };

    $scope.showResults = function() {
      if(!$scope.queryIn) {
        $scope.queryIn = true;
      }
    };

    $scope.containsObject = function (obj, list) {
      var i;
      for (i = 0; i < list.length; i++) {
        if (angular.equals(list[i], obj)) {
          return true;
        }
      }
      return false;
    };

    var showError = function(response){
      console.log(response);
    };

    $scope.openLibrary = function () {
      if(!$scope.ngDialogData){
        $scope.disableLoadSelected = true;
      }

      $scope.files = [];

      $scope.selectedCards = [];
      $scope.videoCards = [];
      $scope.loadingLibraryCards = true;
      library.getLibraryObjectsByType('video', $scope.applyVideo, showError);
    };

    $scope.loadSelectedCards = function(selectedCards){
      $scope.ngDialogData.promise.resolve(selectedCards);
      $scope.closeThisDialog();
    };

    function checkIfVideoInCombos(video){
      if($scope.ngDialogData){
        return _.find($scope.ngDialogData.campaign.combos, function(combo){
          for(var i = 0; i < combo.creativeList.length; i++){
            if(combo.creativeList[i].creative.video === video.guid){
              return true;
            }
          }
        });
      }
    }

    $scope.filterVideos = function(){
      return function(video){
        if($scope.ngDialogData.isInstagram){
          return video.duration > 3 && video.duration < 30;            
        } else {
          return true;
        }       
      };
    };

    $scope.deleteObject = function (object, index) {
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var success = function (response) {
        // $scope.openLibrary();
        $scope.selectedCards = [];
        console.log(response);
      };

      if(checkIfVideoInCombos(object)){
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.library.Youcannotdeletethisvideoifitalreadyexistsinacombo')
        });
        return;
      }

      function confirmCallback(){
        $scope.videoCards.splice(index, 1);
        if($scope.selectedCards){
          $scope.selectedCards.splice(index, 1);
        }
        library.deleteLibraryObjectById(teamId, object.id, 'video').then(success);
      }
      fsConfirm('generic', {
        body: $filter('translate')('label.library.Areyousureyouwanttodelete') + ' ' +  object.name + '?'
      }).then(confirmCallback);
    };

    $scope.loadOnDblClick = function(card, $event) {
      $scope.selectCard(card, $event, true);
      if($scope.selectedCards.length > 0 && !$scope.disableLoadSelected) {
        $scope.loadSelectedCards($scope.selectedCards);
      }
    };

    $scope.selectCard = function (card, $event, dblc) {
      $scope.selectOne = true;

      var i;
      if ($event.target.className === 'expand' || $event.target.className === 'fake-link') {
        return;
      }

      if(!dblc) {
        if ($scope.containsObject(card, $scope.selectedCards)) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        } else {
          if ($scope.selectOne && $scope.selectedCards.length >= 1) {
            i = $scope.selectedCards.indexOf(card);
            $scope.selectedCards.splice(i, 1);
          }
          $scope.selectedCards.push(card);
        }
      } else {
        if($scope.selectOne && $scope.selectedCards.length >= 1) {
          i = $scope.selectedCards.indexOf(card);
          $scope.selectedCards.splice(i, 1);
        }
        $scope.selectedCards.push(card);
      }
    };

    $scope.onFileSelect = function (files) {
      for (var i = 0; i < files.length; i++) {
        $scope.files.push(files[i]);
      }
      if($scope.files && $scope.files.length > 0) {
        $scope.uploadFiles($scope.files);
      }
    };

    $scope.checkIfFileChanged = function(file) {
      var checker = true;
      if(file.size > 0) {
        return checker;
      } else {
        return !checker;
      }
    };

    $scope.showInvalidFile = function() {
      fsConfirm('genericAlert', {
        body: 'Changes made to the selected file prevent system from uploading, please select the file again.'
      });
    };

    $scope.uploadFiles = function (files) {

      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var progressCallback = function (evt) {
        $rootScope.videoProgress = parseInt(100.0 * evt.loaded / evt.total);
        $scope.libraryUploadingScreen.videoProgress = $rootScope.videoProgress;
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      };
      $scope.uploadingFiles = true;

      $scope.libraryUploadingScreen = {
        message: 'Uploading to library ',
        status: 'show',
        uploading: true
      };

      var videoSuccessAndLoad = function (response) {
        $scope.uploadingFiles = false;
        console.log(response);
        $scope.libraryUploadingScreen.uploading = false;
        delete $scope.libraryUploadingScreen.message;
        delete $rootScope.videoProgress;
        $scope.openLibrary();
      };

      var videoSuccess = function (response) {
        console.log(response);
        $scope.libraryUploadingScreen.uploading = false;
        delete $scope.libraryUploadingScreen.message;
      };


      for (var i = 0; i < files.length; i++) {
        var file = files[i];

        // This should be moved to service.

        if (files[i].type.toLowerCase().indexOf('video') >= 0) {
          if (i === files.length - 1) {
            library.uploadVideo($scope, teamId, file, progressCallback, videoSuccessAndLoad);
          } else {
            library.uploadVideo($scope, teamId, file, progressCallback, videoSuccess);
          }
        } else {
          $scope.uploadingFiles = false;
          $scope.libraryUploadingScreen.uploading = false;
          delete $scope.libraryUploadingScreen.message;
          $scope.files = [];
          fsConfirm('genericWithLink', {
            notSupported: $filter('translate')('label.library.ThisFileFormatIsNotSupportedForThisLibrary'),
            fileTypes: $filter('translate')('label.library.SupportedFormatsVideo'),
            contact: $filter('translate')('label.library.IfYouBelieveThisMessageIsShownByError'),
            mailLink: $filter('translate')('label.library.Help@accompliceIo')
          });
          return;
        }

      }
    };

    $scope.editObjectName = function(object) {
      withInput.setInput(object.originalName);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.library.ChangeAssetName'),
        title: $filter('translate')('label.library.MyAssetName'),
        maxLength: 255
      }).then(function () {

        var newName = withInput.getInput();
        //get extension
        var ext = newName.match(/\.[0-9a-z]{1,5}$/, '');
        if(ext){
          newName = newName.replace(/\.[0-9a-z]{1,5}$/, '');
        }
        //remove special characters
        newName = newName.replace(/[\. ,:<>;+|*&%#]+/g, '');
        //add extension back
        if(ext){
          newName += ext[0];
        }
        library.updateObjectName(object.id, 'video', newName, function () {
          object.originalName = newName;
          object.name = newName;
        }, function (error) {
          console.log(error.error);
        });
      });
    };

    $scope.openLibrary();


  });
