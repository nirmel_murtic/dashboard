'use strict';

describe('Controller: ActCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var ActCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActCtrl = $controller('ActCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
