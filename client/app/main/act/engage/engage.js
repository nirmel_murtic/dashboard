'use strict';

angular.module('act')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.act.engage', {
        url: '/engage',
        permissions: '<ACT> && <ENGAGE>',
        views: {
	        '@main': {
	          templateUrl: 'app/main/act/engage/engage.html',
    			  controller: 'EngageCtrl'
	        }
		    },
        data: {
          displayName: 'Engage'
        }
      });
  });
