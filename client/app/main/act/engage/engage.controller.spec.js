'use strict';

describe('Controller: EngageCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var EngageCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EngageCtrl = $controller('EngageCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
