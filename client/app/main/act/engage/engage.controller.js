'use strict';

angular.module('act')
  .controller('EngageCtrl', function ($scope, $timeout, $rootScope, $stateParams, $state, library, engage) {
  	$scope.activeChannels = ['facebook'];
  	$scope.showUpcomingPosts = true;
    $scope.search = '';

  	 $scope.applyUpcomingPosts = function(result){
  	 	// if(result.data.length > 0){
  	 		$scope.upcomingPosts = result;
  	 	// }
  	 };

  	$scope.toggleReplyView = function(post){
  		$scope.preventDoubleEventFiring();
  		if(!post.replyViewOpen){
  	 		post.replyViewOpen = true;  	 		
  	 	} else {
  	 		post.replyViewOpen = false;  	 		
  	 	}
  	};

    $scope.toggleShareView = function(post){
      $scope.preventDoubleEventFiring();
      if(!post.shareViewOpen){
        post.shareViewOpen = true;
        $scope.share = post;
      } else {
        post.shareViewOpen = false;
        $scope.share = '';
      }
    };

    $scope.applyScheduledPosts = function(result){
      $scope.scheduledPosts = result.data;
    };

    $scope.preventDoubleEventFiring = function(){
      //make sure pill click doesnt fire twice
        if (event.stopPropagation) {
          event.stopPropagation();
        }
        //just in case browser doesnt detect stopPropagation
        if (event.preventDefault) {
          event.preventDefault();
        }
        event.cancelBubble = true;
        event.returnValue = false;
    };

    $scope.toggleActionSummary = function(post, actionType){
      $scope.preventDoubleEventFiring();
      if(!post.showActionSummary){
        post.showActionSummary = actionType;        
      } else{        
        post.showActionSummary = false;
      }
    };


    $scope.formatDate = function(post){
      return moment([new Date(post.createdAt).getFullYear(), new Date(post.createdAt).getMonth(), new Date(post.createdAt).getDate(), new Date(post.createdAt).getHours(), new Date(post.createdAt).getMinutes(), new Date(post.createdAt).getSeconds()]).fromNow();
    };

    $scope.isConversationReplyOpen = function(conversation){    
      return conversation.replyViewOpen;
    };

    $scope.isRetweetOpen = function(conversation){    
      return conversation.retweetViewOpen;
    };

    $scope.isShareOpen = function(conversation){    
      return conversation.shareViewOpen;
    };


    $scope.replyToTweet = function(conversation, text){
      $scope.preventDoubleEventFiring();
      var success = function(result){
        console.log(result);
        engage.getAllPostsTwitter($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      };

      var error = function(result){
        console.log(result);
      };

      engage.replyToTweet(conversation.pageId, conversation.tweetId, $scope.activePages[0].name, text, success, error);
    };

    $scope.retweetTweet = function(conversation, text){
      $scope.preventDoubleEventFiring();
      var success = function(result){
        console.log(result);        
        engage.getAllPostsTwitter($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      };

      var error = function(result){
        console.log(result);
      };

      engage.retweetTweet(conversation.pageId, conversation.tweetId, $scope.activePages[0].name, text, success, error);
    };

    $scope.isFavorited = function(post){
      for(var i=0; i< post.favs.length; i++){
        if(post.favs[i].screenName === $scope.activePages[0].name){
          return true;   
        } 
      }      
    };

    $scope.isLiked = function(post){
      if(post.likes){
        for(var i=0; i< post.likes.length; i++){
          if(post.likes[i].screenName === $scope.activePages[0].name){
            return true;   
          } 
        }              
      }
    };

    $scope.favoriteTweet = function(tweet){
      $scope.preventDoubleEventFiring();
      var success = function(result){
        console.log(result);
        engage.getAllPostsTwitter($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      };

      var error = function(result){
        console.log(result);
      };

      if($scope.isFavorited(tweet)){
        engage.unfavoriteTweet(tweet.pageId, tweet.tweetId, $scope.activePages[0].name, success, error);
      } else {
        engage.favoriteTweet(tweet.pageId, tweet.tweetId, $scope.activePages[0].name, success, error);
      }

    };

    $scope.likePost = function(post){
      $scope.preventDoubleEventFiring();
      var success = function(result){
        console.log(result);
        engage.getAllPostsFacebook($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      };

      var error = function(result){
        console.log(result);
      };



      if($scope.isLiked(post)){
        engage.unlikePost(post.feedId, $scope.activePages[0].name, success, error);
      } else {
        engage.likePost(post.feedId, $scope.activePages[0].name, success, error);
      }
    };

    $scope.commentOnPost = function(post, message){
      $scope.preventDoubleEventFiring();
      var success = function(result){
        console.log(result);
        engage.getAllPostsFacebook($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      };

      var error = function(result){
        console.log(result);
      };
      post.comment = message;
      engage.commentOnPost(post, success, error);
    };

    $scope.likeComment = function(comment){
      $scope.preventDoubleEventFiring();
      var success = function(result){
        console.log(result);
        engage.getAllPostsFacebook($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      };

      var error = function(result){
        console.log(result);
      };



      if($scope.isLiked(comment)){
        engage.unlikeComment(comment.postId, comment.commentId, success, error);
      } else {        
        engage.likeComment(comment.postId, comment.commentId, success, error);
      }
    };

  	 $scope.applyPost = function(result){
  	 	$scope.pagePosts.push(result.data);
  	 };

  	 $scope.getPostTimeFromNow = function(time){
  	 	var date = new Date(time);
  	 	return moment([date.getFullYear(), date.getMonth(), date.getDay(), date.getMinutes()]).fromNow();
  	 };

     $scope.goToCalendar = function(){
        $timeout(function(){
          angular.element('#engage-scheduler a').trigger('click');
        });
     };

  	 $scope.applyMyPagePosts = function(result){
  	 	$scope.pagePosts = result.data.data;  	 	
  	 	console.log($scope.pagePosts);
  	 };

  	 $scope.deletePost = function(id){
	  	$scope.preventDoubleEventFiring();
  	 	engage.deletePost(id, $scope.applyMyPagePosts, $scope.errorCallback);

  	 };

  	 $scope.toggleConversation = function(index){
  	 	if(!$scope.pagePosts[index].conversationOpen){
  	 		$scope.pagePosts[index].conversationOpen = true;  	 		
  	 	} else {
  	 		$scope.pagePosts[index].conversationOpen = false;  	 		
  	 	}
  	 };

  	$scope.isPostConversationOpen = function(index){
  		return $scope.pagePosts[index].conversationOpen;
  	};

  	$scope.errorCallback = function(error){
  		console.log(error);
  	};

  	engage.getAllScheduledPosts($scope.applyUpcomingPosts, $scope.errorCallback);

  	engage.getAllSocialPages(function(result) {
      $scope.teamPages = result.data;
      $scope.assignChannelsToPages($scope.teamPages);
      $scope.activePages = [];
      $scope.activePages.push($scope.teamPages[4]);
      if($scope.activePages[0].pageName){

  	  	engage.getAllPostsFacebook($scope.activePages[0].pagePageId, $scope.applyMyPagePosts, $scope.errorCallback);
      	
      } else if($scope.activePages[0].name){
  	  	engage.getAllPostsTwitter($scope.activePages[0].name, $scope.applyMyPagePosts, $scope.errorCallback);
      	
      }
    }, function(error) {
      console.log(error);
    });

    $scope.assignChannelsToPages = function(accounts){
      for(var i = 0; i < accounts.length; i++){
        if(accounts[i].accountId){
          accounts[i].channel = 'twitter';
        } else if(accounts[i].pageId){
          accounts[i].channel = 'facebook';
        }
      }        
    };


    $scope.togglePage = function(page){
  		if($scope.isPageActive(page)){
  			$scope.activePages.splice($scope.activePages.indexOf(page), 1);
  		} else {
  			$scope.activePages = [];
  			$scope.activePages.push(page);
  			if(page.channel === 'twitter'){
  				engage.getAllPostsTwitter(page.name, $scope.applyMyPagePosts, $scope.errorCallback);
  			} else if(page.channel === 'facebook'){
  				engage.getAllPostsFacebook(page.pagePageId, $scope.applyMyPagePosts, $scope.errorCallback);				
  			}
  		}
  	};

    $scope.isPageActive = function(page){
    	if($scope.activePages.indexOf(page) > -1){
  			return true;
  		} else {
  			return false;
  		}
    };

  	$scope.upcomingPosts = [{
  		date: new Date().getMonth() + 1 + '/' + new Date().getDate() + '/' +  new Date().getFullYear()
  	},{
  		date: new Date().getMonth() + 1 + '/' + new Date().getDate() + '/' +  new Date().getFullYear()
  	},{
  		date: new Date().getMonth() + 1 + '/' + new Date().getDate() + '/' +  new Date().getFullYear()
  	}];

  	$scope.switchFeedView = function(view){
  		  $scope.feedView = view;
        $scope.pagePosts = [];
  	};

  	$scope.toggleChannel = function(channel){
  		if($scope.isChannelActive(channel)){
  			$scope.activeChannels.splice($scope.activeChannels.indexOf(channel), 1);
  		} else {
  			$scope.activeChannels.push(channel);
  		}
  	};

  	$scope.isChannelActive = function(channel){
  		if($scope.activeChannels.indexOf(channel) > -1){
  			return true;
  		} else {
  			return false;
  		}
  	};


    $scope.$watch('search', function() {
      $scope.hashtags = $scope.search.match(/[#]+[A-Za-z0-9_]+/g);
      $scope.mentions = $scope.search.match(/[@]+[A-Za-z0-9_]+/g);

      if($scope.feedView === 'hashtags'){

      } else if($scope.feedView === 'mentions'){

      } else {

      }


    });

  	$scope.newFeed = {};
  	$scope.feeds = [];

  	$scope.populateSocialPagesInDropdown = function(result){
  		$scope.pages = result;
  	};

  	$scope.selectEngageView = function(view){
  		$scope.currentEngageView = view;
  	};

  	$scope.clickToOpenLibrary = function(section, tab){
      $scope.initSection = section;
      if (section !== 'Home') {
          $scope.initTab = tab;
          $scope.showThisTab = tab;
      }
      library.openLibrary($scope);
  	};

	 $scope.currentEngageView = 'feed';
  });
