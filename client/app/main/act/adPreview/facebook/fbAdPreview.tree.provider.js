'use strict';

angular.module('act')
  .provider('PreviewTree', ['TreeDSProvider', function(TreeDSProvider) {
    /* jshint newcap: false */
    function PreviewTree(nodeInput, children) {
      this.node = nodeInput;
      this.children = children;
      this.indexAmongSiblings = 0;
      if(children) {
        children.forEach(function(c,i) {
          c.indexAmongSiblings = i;
        });
      }
    }
    TreeDSProvider(PreviewTree);
    this.$get = ['TreeDS', function(TreeDS) {
      var PreviewTreeService = TreeDS(PreviewTree);
      return PreviewTreeService;
    }];
  }]);
