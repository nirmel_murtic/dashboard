'use strict';

angular.module('act')
  .factory('FacebookAdPreview', ['$q', 'PreviewTree', 'FacebookAdPreviewWebApiUtils', function($q, PreviewTree, PreviewApi) {
    /* jshint camelcase: false, sub:true, unused: false */
    /**
     * getAdFormats
     *
     * @param creative
     *
     * ad_format should be one of the following:
     *
     * RIGHT_COLUMN_STANDARD
     * DESKTOP_FEED_STANDARD
     * MOBILE_FEED_STANDARD
     * MOBILE_BANNER: for ads served on the Audience network as a banner ad
     * MOBILE_INTERSTITIAL
     *
     * @return {object} - { ad_format: [ <list of one or more of the above formats> ] }
     */
    function getAdFormats(creative) {
      var adFormats;
      if(creative.placement === 'Right Column') {
        adFormats = ['RIGHT_COLUMN_STANDARD'];
      }
      if(creative.pageTypes === 'feed') {
        adFormats = creative.platform.map(function(platform) {
          return {
            'desktop' : 'DESKTOP_FEED_STANDARD',
            'mobile' : 'MOBILE_FEED_STANDARD'
          }[platform];
        });
      }
      if(creative.placement === 'Mobile App Installs' || creative.placement === 'Mobile App Engagement') {
        adFormats = ['MOBILE_FEED_STANDARD'];
      }
      // TO-DO: handle boosted post, waiting on finailized campaign workflow
      return { ad_format: adFormats };
    }
    /**
     * getSharedCreativeSpecs
     *
     * Convert our interal representation of creative into fb's creative spec json
     *
     * @param creative
     * @return {object} - { creative : { ...creative specs... } }
     */
    function getSharedCreativeSpecs(creative) {
      if(creative.placement === 'Right Column') {
        var propMap = {
          title : 'headline',
          body : 'text',
          object_url : 'url'
        };
        return {
          creative : _.mapValues(propMap, function(field) {
            return creative[field];
          })
        };
      }
      // the same for mobile and desktop
      if(creative.pageTypes === 'feed') {
        return {
          creative : {
            object_story_spec : {
              page_id : creative.facebookSponsoredPageId,
              link_data : {
                link : creative.url,
                message : creative.text,
                description : creative.siteDescription,
                name : creative.headline,
                // TO-DO: converte to image url
                // picture : '/api/v2/image/get?guid=' + creative.image
                picture : '//www.mycatspace.com/wp-content/uploads/2013/08/adopting-a-cat.jpg'
              }
            }
          }
        };
      }
      if('increase following') {
        return {
          creative : {
            body : creative.text,
            object_id : creative.facebookSponsoredPageId
          }
        };
      }
      if(creative.placement === 'Mobile App Installs') {
        return {
          objective: 'MOBILE_APP_INSTALLS',
          creative : {
            object_story_spec : {
              page_id : creative.facebookSponsoredPageId,
              link_data: {
                call_to_action: {
                  'type': 'INSTALL_MOBILE_APP',
                  'value': {
                    'link': creative.appLink
                  }
                },
                'link': creative.appLink,
                'message': 'message',
                'name': 'name',
                'caption': 'caption',
                'description': 'description'
              }
            }
          }
        };
      }
      // TO-DO: not fully tested yet
      if(creative.placement === 'Mobile App Engagement') {
        return {
          objective: 'MOBILE_APP_INSTALLS',
          creative : {
            object_story_spec : {
              page_id : creative.facebookSponsoredPageId,
              link_data: {
                call_to_action: {
                  'type': creative.callToAction,
                  'value': {
                    'app_link': creative.appLink
                  }
                },
                'link': creative.appLink,
                'message': 'message',
                'name': 'name',
                'caption': 'caption',
                'description': 'description'
              }
            }
          }
        };
      }
    }
    /**
     * getPermutedCreativeSpecs
     *
     * @param creative
     * @return {object} - eg. { creative : { image_url: ['1.png', '2.png'] }}
     */
    function getPermutedCreativeSpecs(creative) {
      // all props needs to be in sorted order
      // For now nothing to permute on, will have at least images in the future
      var ret = {
        creative : {}
      };
      return ret;
    }
    /**
     * buildTreeFromCreative
     *
     * Build a tree representation of previews from a single creative
     * 1. root node is shared specs
     * 2. firsr level nodes are ad formats permutations
     * 3. each level below, the nodes are permutations of one permutable field
     * 4. number of leaf nodes = number of previews to generate
     *
     * @param creative
     * @return { PreviewTree }
     */
    function buildTreeFromCreative(creative) {
      var sharedSpecs = getSharedCreativeSpecs(creative);
      var adFormats = getAdFormats(creative);
      var specs = getPermutedCreativeSpecs(creative);
      var branches = _.reduce(specs.creative, function(lastBranch, spec, prop) {
        var nextBranch;
        if(!lastBranch) {
          nextBranch = spec.map(function(s) {
            var nodeInput = { creative : {} };
            nodeInput.creative[prop] = s;
            return PreviewTree.build(nodeInput, null);
          });
        } else if(spec.length > 0) {
          nextBranch = spec.map(function(s) {
            var nodeInput = { creative : {} };
            nodeInput.creative[prop] = s;
            return PreviewTree.build(nodeInput, lastBranch);
          });
        } else {
          nextBranch = lastBranch;
        }
        return nextBranch;
      }, null);
      branches = adFormats.ad_format.map(function(f) {
        var nodeInput = {
          ad_format : f
        };
        return PreviewTree.build(nodeInput, branches);
      });
      return PreviewTree.build(sharedSpecs, branches);
    }
    /**
     * buildPreviewObjects
     *
     * Initial build of preview objects from a tree
     *
     * @param tree
     * @return { object }
     * - key in map encodes preview's hierachy in the tree,
     * - value is of the form {
     *     queryParams : <valide query params for fb graph ad preview api>.
     *     unsafeHtml : <empty string, later to be filled with ad preview iframe html>
     *   }
     */
    function buildPreviewObjects(tree) {
      var idLookup = {};
      var path = [];
      PreviewTree.tranverse(tree, function(node, branch) {
        var queryParams, id;
        if(branch.isLeaf()) {
          id = '';
          queryParams = _.reduce(path, function(q, b) {
            id = id + b.indexAmongSiblings + '.';
            return _.merge(q, b.node);
          }, _.extend({}, branch.node));
          id = id + branch.indexAmongSiblings;
          idLookup[id] = {
            queryParams : queryParams,
            unsafeHtml : ''
          };
        }
        path.push(branch);
      }, function(node, branch) {
        path.pop();
      });
      return idLookup;
    }
    /**
     * updatePreviewObjects
     *
     * Take a existing previewObjects hashmap and a tree diff results
     * modifies the hashmap *in place* and returns it based on diff.
     * Only new/changed nodes will trigger update of preview, unchanged
     * previews will reuse previous map's data (i.e. keeps the unsafeHtml)
     *
     * @param {obejct} - previewObjects
     * @param {PreviewTree} - diffTree
     * @return {object}
     */
    function updatePreviewObjects(idLookup, diffTree) {
      var path = [];
      PreviewTree.tranverse(diffTree, function(node, branch) {
        var queryParams, id, differentOrNew;
        var removed = !!node['$$removeChild'];
        if(branch.isLeaf()) {
          id = '';
          differentOrNew = false;
          var newNode = node;
          if(node['$$replaceNode']) {
            differentOrNew = true;
            newNode = node['$$replaceNode'].new_node;
          }
          if(node['$$insertChild']) {
            differentOrNew = true;
            newNode = node['$$insertChild'];
          }
          queryParams = _.reduce(path, function(q, b) {
            var node = b.node;
            id = id + b.indexAmongSiblings + '.';
            if(b.node['$$replaceNode']) {
              differentOrNew = true;
              node = b.node['$$replaceNode'].new_node;
            }
            if(b.node['$$insertChild']) {
              differentOrNew = true;
              node = b.node['$$insertChild'];
            }
            return removed ? q : _.merge(q, node);
          }, (removed ? null : _.extend({}, newNode)) );
          id = id + branch.indexAmongSiblings;
          if(removed) {
            var l = id.length;
            _.each(idLookup, function(v, key) {
              if(key === id || key.slice(0, l) === id) {
                delete idLookup[key];
              }
            });
          } else if (differentOrNew) {
            idLookup[id] = {
              queryParams : queryParams,
              unsafeHtml : ''
            };
          }
        }
        path.push(branch);
      }, function(node, branch) {
        path.pop();
      });
      return idLookup;
    }

    /**
     * constructPreviewApiQuery
     *
     * stringify creative and post field into json string
     *
     * @param queryParams
     * @return {object}
     */
    function constructPreviewApiQuery(queryParams) {
      var params = _.mapValues(queryParams, function(q, name) {
        if(name === 'creative' || name === 'post') {
          return JSON.stringify(q);
        }
        return q;
      });
      // due to legacy api, we call ad_format field adPlacement
      // in our backend web interface
      // params.post = params.post || '';
      // params.adPlacement = params.ad_format;
      // delete params.ad_format;
      return params;
    }
    /**
     * requestAPreview
     *
     * @param previewObject
     * @param apiUrl
     * @return {object} - a promise resolve to iframe preview html
     */
    function requestAPreview(previewObject) {
      var deferred;
      if(!previewObject.unsafeHtml) {
        deferred = $q.defer();
        PreviewApi.fromCreativeSpec({
          params : constructPreviewApiQuery(previewObject.queryParams)
        })
        .success(function(res) {
          try {
            var iframe = res.data[0].body;
            previewObject.unsafeHtml = iframe;
            deferred.resolve(iframe); // should be the iframe html
          } catch(e) {
            previewObject.unsafeHtml = '';
            deferred.reject(e);
          }
        })
        .error(function(err) {
          deferred.reject(err);
        });
        return deferred.promise;
      } else {
        return $q.when(previewObject.unsafeHtml);
      }
    }

    // Public Api
    return function adPreviewFactory() {
      var lastPreviewTree;
      var previewObjects;
      var self = {
        /**
         * getInitialPreviewsFromCreative
         *
         * @param creative
         * @return {array} - an array of promises resolve to preview iframe html
         */
        getInitialPreviewsFromCreative : function(creative) {
          lastPreviewTree = buildTreeFromCreative(creative);
          previewObjects = buildPreviewObjects(lastPreviewTree);
          window.pobj = previewObjects;
          return _.map(previewObjects, requestAPreview);
        },
        /**
         * updatePreviewsFromCreative
         *
         * @param creative
         * @return {array} - an array of promises resolve to preview iframe html
         */
        updatePreviewsFromCreative: function(creative) {
          var newTree = buildTreeFromCreative(creative);
          var diffTree = PreviewTree.diff(lastPreviewTree, newTree);
          window.dtree = diffTree;
          updatePreviewObjects(previewObjects, diffTree);
          lastPreviewTree = newTree;
          return _.map(previewObjects, requestAPreview);
        },
        getPreviewsFromCreative : function(creative) {
          return lastPreviewTree ? self.updatePreviewsFromCreative(creative) : self.getInitialPreviewsFromCreative(creative);
        }
      };
      return self;
    };
  }]);
