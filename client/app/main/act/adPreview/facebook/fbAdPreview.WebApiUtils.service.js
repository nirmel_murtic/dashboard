'use strict';

angular.module('act')
  .factory('FacebookAdPreviewWebApiUtils', ['$http','$q', '$rootScope', 'apiUrl', function($http, $q, $rootScope, apiUrl) {
    /* jshint unused:false */
    /*
    function fromPublishedAd () {
    
    }
    function fromPublishedCreative () {
    
    }*/
    function fromCreativeSpec() {
      var previewApiUrl = 'dummyapi/v2.2/act_10203017052734801/generatepreviews';
      // var previewApiUrl = apiUrl + '/api/v1/campaign/adPreview/' + $rootScope.fractalAccountInfo.currentTeam.id;
      return $http.get.bind($http, previewApiUrl); 
    }
    // Public Api
    return {
      fromCreativeSpec : fromCreativeSpec()
    };
  }]);
