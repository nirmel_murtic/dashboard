'use strict';

angular.module('act')
  .service('TwPost', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    	var TwPost;

        TwPost = (function() {
            function TwPost(twPost, selectedTwitterPage) {
                this.images = [];
                this.preview = {};
                this.text = null;                
                this.channel = 'TWITTER';
                this.startDate = [new Date(), new Date()];
                // Retrieve Twitter page of previous post if creating another, else take either the first page or assign empty object
                this.twitterPage = twPost ? twPost.twitterPage :  (selectedTwitterPage || null);
            }

            return TwPost;
        })();

        

        TwPost.prototype.isValid = function() {
            var isValid = true;
            //Check that the required fields are present
            if (this.text &&
                this.startDate) {
                isValid = true;
            } else {
                isValid = false;
            }

            return isValid;
        };

        return TwPost;

  });
