'use strict';

describe('Service: TwPost', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var TwPost;
  beforeEach(inject(function (_TwPost_) {
    TwPost = _TwPost_;
  }));

  it('should do something', function () {
    expect(!!TwPost).toBe(true);
  });

});
