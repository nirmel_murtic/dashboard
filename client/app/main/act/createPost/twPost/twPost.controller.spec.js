'use strict';

describe('Controller: TwPostCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var TwPostCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TwPostCtrl = $controller('TwPostCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
