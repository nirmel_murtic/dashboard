'use strict';

angular.module('act')
  .controller('TwPostCtrl', function ($scope, $filter) {
  	function initView() {
        return {
            errorApplyBestTime: false,
            showApplyBestTime: true,
            isBestTimeApplied: false,
            isUrlTracked: false,            
            daterangePickerOpts: {
                dateLimit: { days: 0 },
                singleDatePicker: true,
                showDropdowns: true,
                minDate: new Date(moment().format('YYYY-MM-DD HH:mm'))
            },
            fetchingImages: false
        };
    }

    $scope.view = initView();

    $scope.selectTwitterPage = function (twitterPage) {
        $scope.fbPost.twitterPage = twitterPage;
    };

    $scope.$watch('twPost.images', function (newVal, oldVal) {
        if (newVal && newVal !== oldVal) {
            for (var i=0; i<newVal.length; i++) {
                if (newVal[i].selected === true) {
                    $scope.twPost.preview.thumbnail = newVal[i].fullGuid;
                }
            }
        }
    }, true);


    $scope.changeDate = function (date) {
      //compare min timedate of datepicker and selected timedate
      if (date && $scope.view.daterangePickerOpts.minDate <= new Date(date[0])) {
        //today we can fetch apply best time
        if (moment(date[0]).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD')) {
          //if date is today and choosed hour is same as hour of now then show NOW in daterangepicker
          if (moment(date[0]).format('YYYY-MM-DD HH') === moment().format('YYYY-MM-DD HH')) {
            $scope.$parent.view.buttonTitle = $filter('translate')('label.act.post.PostNow');
          }
          $scope.view.showApplyBestTime = true;
        } else {
          //if date isn't today hide apply best time
          $scope.view.showApplyBestTime = false;
          $scope.$parent.view.buttonTitle = $filter('translate')('label.act.post.SchedulePost');
        }
      } else {
        alert('Choose another date or time');
      }
    };

  	
  });
