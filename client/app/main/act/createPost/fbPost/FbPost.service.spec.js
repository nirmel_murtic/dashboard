'use strict';

describe('Service: FbPost', function () {

  // load the service's module
  beforeEach(module('dashboardApp'));

  // instantiate service
  var FbPost;
  beforeEach(inject(function (_FbPost_) {
    FbPost = _FbPost_;
  }));

  it('should do something', function () {
    expect(!!FbPost).toBe(true);
  });

});
