'use strict';

describe('Controller: FbpostCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));

  var FbpostCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FbpostCtrl = $controller('FbpostCtrl', {
      $scope: scope
    });
  }));

});
