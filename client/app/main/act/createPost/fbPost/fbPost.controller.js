'use strict';

angular.module('act')
  .controller('FbpostCtrl', function ($scope, $rootScope, CreatePost, regex, $filter, fsConfirm) {

    $scope.shortUrlInTextRegex = regex.SHORTURLINTEXT;

    function initView() {
      return {
        errorApplyBestTime: false,
        isBestTimeApplied: false,
        displayNoMoreBestTimeAvailableMessage: false,
        isUrlTracked: false,
        isAudienceSelected: {
          myFans: {
            selected: true,
            display: 'My Fans'
          },
          fansFriends: {
            selected: false,
            display: 'Fans\' Friends'
          }
        },
        durations: [
          {name: '1 hour', value: 1},
          {name: '3 hours', value: 3},
          {name: '6 hours', value: 6},
          {name: '12 hours', value: 12},
          {name: '1 day', value: 24},
          {name: '3 days', value: 72},
          {name: '5 days', value: 120}
        ],
        daterangePickerOpts: {
          dateLimit: {days: 0},
          singleDatePicker: true,
          showDropdowns: true,
          minDate: new Date()
        },
        fetchingImages: false
      };
    }

    $scope.view = initView();

    $scope.$on('resetView', function () {
      $scope.view = initView();
      $scope.fbPost.duration = $scope.view.durations[4];
    });


    // Assign duration to 1 day
    $scope.fbPost.duration = $scope.view.durations[4];

    // Assign selected Facebook page to a scope variable to display the name and the picture in the FbCreative view. Assign the FB page id in the creative model
    $scope.selectFacebookPage = function (facebookPage) {
      $scope.fbPost.facebookSponsoredPageId = facebookPage.pagePageId;
      $scope.fbPost.facebookPage = facebookPage;

      // Disable best time
      $scope.view.isBestTimeApplied = false;
      $scope.view.displayNoMoreBestTimeAvailableMessage = false;
    };
    // Assign null to facebookPage in creative view so the dropdown to select a page is shown again
    $scope.unSelectFacebookPage = function () {
      $scope.fbPost.facebookSponsoredPageId = $scope.fbPost.facebookPage = null;
    };

    // We need to update the date here otherwise the dateRangePicker doesn't return to NOW
    $scope.fbPost.startDate = [new Date(), new Date()];

    $scope.changeDate = function (date) {
      //compare min timedate of datepicker and selected timedate
      if (date && $scope.view.daterangePickerOpts.minDate <= new Date(date[0])) {
        //today we can fetch apply best time
        if (moment(date[0]).format('YYYY-MM-DD') === moment().format('YYYY-MM-DD')) {
          //if date is today and choosed hour is same as hour of now then show NOW in daterangepicker
          if (moment(date[0]).format('YYYY-MM-DD HH') === moment().format('YYYY-MM-DD HH')) {
            $scope.$parent.view.buttonTitle = $filter('translate')('label.act.post.PostNow');
          }
        } else {
          $scope.$parent.view.buttonTitle = $filter('translate')('label.act.post.SchedulePost');
        }
        $scope.view.isBestTimeApplied = false;
        $scope.view.displayNoMoreBestTimeAvailableMessage = false;

      } else {
        fsConfirm('genericAlert', {body: $filter('translate')('label.act.post.Chooseanotherdateortime')});
      }
    };

    $scope.deleteImage = function () {
      $scope.fbPost.image = null;
    };

    // ------------------------------------- Checkboxes handling -----------------------------------------------
    $scope.togglePromotePost = function () {
      // Check that the user has at least one ad account
      if ($scope.api.facebookAdAccounts.length > 0) {
        // Auto select 1st ad account if the user clicks on Promote post for the 1st time
        if (!$scope.fbPost.adAccount) {
          $scope.fbPost.adAccount = $scope.api.facebookAdAccounts[0];
        }
      } else {
        // Display error message
        $scope.view.isMissingAdAccountWarning = true;
        $scope.fbPost.isPromoted = false;
      }
    };

    function applyBestTimeChanges(momentJsDate) {
      var jsDate = new Date(momentJsDate.toDate());

      $scope.fbPost.startDate = [jsDate, jsDate];
      $scope.$parent.view.buttonTitle = $filter('translate')('label.act.post.SchedulePost');
      $scope.view.isBestTimeApplied = true;
    }

    $scope.applyBestTime = function () {

      if ($scope.view.isBestTimeApplied) {
        fsConfirm('genericAlert', {body: $filter('translate')('label.act.post.Besttimealreadyapplied')});
      } else {
        if ($scope.fbPost.startDate && $scope.fbPost.startDate[0]) {
          CreatePost.getBestTimeFacebook($scope.fbPost.facebookPage.pagePageId, function (response) {
            var dataFromServer = response.data,
              startDate = moment($scope.fbPost.startDate[0]),

              displayNoMoreBestTimeAvailableMessage = false,  //jshint ignore:line
              todayDate = moment().toDate();

            // Check if it's for today or in the future
            var isPostToday = startDate.isSame(todayDate, 'd');

            if (dataFromServer && dataFromServer.first_hour !== null && dataFromServer.last_hour !== null && dataFromServer.minutes !== null) {  //jshint ignore:line
              // If today
              if (isPostToday) {
                // Current date is prior to beginning of best time, apply first hour of best time
                if (startDate.format('HH') <= dataFromServer.first_hour) {  //jshint ignore:line
                  startDate.hour(dataFromServer.first_hour);  //jshint ignore:line
                  startDate.minutes(0);
                  applyBestTimeChanges(startDate);
                } // Current time between start time and end time
                else if (startDate.format('HH') <= dataFromServer.last_hour) {  //jshint ignore:line
                  applyBestTimeChanges(startDate);
                  // Else, if the time is after the end of the best time window, show the message saying there are no more recommendations
                } else {
                  displayNoMoreBestTimeAvailableMessage = true;
                }
              } else { // Post date scheduled in the future
                startDate.hour(dataFromServer.first_hour);  //jshint ignore:line
                startDate.minutes(0);
                applyBestTimeChanges(startDate);
              }
            }

            $scope.view.displayNoMoreBestTimeAvailableMessage = displayNoMoreBestTimeAvailableMessage;
          });
        } else {
          fsConfirm('genericAlert', {body: $filter('translate')('label.act.post.Chooseanotherdateortime')});
        }
      }
    };

    $scope.toggleConnectionsSelection = function (category) {
      $scope.fbPost.audience.connections[category].selected = !$scope.fbPost.audience.connections[category].selected;
    };

    $scope.toggleGenderSelection = function (category) {
      $scope.fbPost.audience.gender[category].selected = !$scope.fbPost.audience.gender[category].selected;
    };
    // ------------------------------------- End Checkboxes handling -----------------------------------------------

    // This removes the preview and also removes the link attached to the post
    $scope.removePreview = function () {
      $scope.fbPost.preview = {};
      $scope.fbPost.images = []; // Reset images array
      delete $scope.fbPost.url;
    };

    $scope.checkForUrl = function (event) {
      // If there is no URL attached to the post so far AND the user didn't upload any image from the library
      if (!$scope.fbPost.url && !$scope.fbPost.image) {
        $scope.view.pastedContent = event.clipboardData.getData('text/plain');

        // Check that there are URLs in the pasted content and retrieve preview info for the 1st one
        var urlArray = $scope.view.pastedContent.match($scope.shortUrlInTextRegex);
        if (urlArray) {
          var pastedUrl = urlArray[0];
          $scope.view.fetchingImages = true;
          // Retrieve the metadata info for the URL
          CreatePost.loadMetadata(pastedUrl, function (result) {
            // Hide the loading animation
            $scope.view.fetchingImages = false;
            $scope.view.hasLoadMetadataFailed = false;

            var url = URI(result.data.url);

            $scope.fbPost.preview.headline = result.data.title;
            $scope.fbPost.preview.description = result.data.description;
            $scope.fbPost.preview.url = url._parts.hostname;

            for (var i = 0, j = result.data.images.length; i < j; i++) {
              var img = {fullGuid: result.data.images[i], selected: false};
              $scope.fbPost.images.push(img);
            }
            // thumbnail can be empty
            if (!result.data.thumbnail) {
              $scope.fbPost.preview.thumbnail = $scope.fbPost.images[0].fullGuid;
              $scope.fbPost.images[0].selected = true;
            } else {
              //insert thumbnail on begin of array
              $scope.fbPost.images.unshift({
                fullGuid: result.data.thumbnail,
                selected: true
              });
              $scope.fbPost.preview.thumbnail = $scope.fbPost.images[0].fullGuid;
            }

            // Validate the URL then shorten it
            $scope.validateLink(pastedUrl).then($scope.createShortenUrl).then(function (data) {
              $scope.fbPost.url = data.groupShortUrl;
              //return result.data.groupShortUrl;
            });

          }, function (error) { // If the URL is not valid
            // Hide the loading animation
            $scope.view.fetchingImages = false;
            // Used to display an error message to the user to let him know the call to retrieve info from the URL failed
            $scope.view.hasLoadMetadataFailed = true;
            console.log(error);
          });
        }
      }
    };

    $scope.$watch('fbPost.images', function (newVal, oldVal) {
      if (newVal && newVal !== oldVal && $scope.fbPost.url) {
        for (var i = 0; i < newVal.length; i++) {
          if (newVal[i].selected === true) {
            $scope.fbPost.preview.thumbnail = newVal[i].fullGuid;
          }
        }
      }
    }, true);
  });
