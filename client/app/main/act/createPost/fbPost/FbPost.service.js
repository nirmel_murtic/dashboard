'use strict';

angular.module('act')
    .service('FbPost', function ($http, apiUrl, $rootScope, Campaign) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        var FbPost;

        FbPost = (function() {
            function FbPost(fbPost, selectedFacebookPage) {
                this.images = [];
                this.isPromoted = false;
                this.preview = {};
                this.duration = null;
                this.text = null;
                this.audience = {
                    connections: {
                        myFans: {
                            selected: true,
                            display: 'My Fans'
                        },
                        fansFriends:{
                            selected: false,
                            display: 'Fans\' Friends'
                        }
                    },
                    gender:{
                        male: {
                            selected: false,
                            display: 'Male'
                        },
                        female: {
                            selected: false,
                            display: 'Female'
                        }
                    },
                    ageRange:{
                        min: null,
                        max: null
                    }
                };
                this.channel = 'FACEBOOK';
                this.startDate = [new Date(), new Date()];
                // Retrieve Facebook page of previous post if creating another, else take either the first page or assign empty object
                this.facebookPage = fbPost ? fbPost.facebookPage :  (selectedFacebookPage || null);
                this.facebookSponsoredPageId = fbPost ? fbPost.facebookPage.pagePageId :  (selectedFacebookPage ? selectedFacebookPage.pagePageId : null);
            }

            return FbPost;
        })();


        FbPost.prototype.isValid = function() {
            var isValid = true;
            //Check that the required fields are present
            if (this.text &&
                this.startDate &&
                this.facebookSponsoredPageId) {
                isValid = true;

                // Check conditional requirements
                if (this.isPromoted){
                    if (this.budget &&
                        this.adAccount &&
                        this.duration &&
                        // At least one connection setting needs to be selected
                        (this.audience.connections.myFans.selected || this.audience.connections.fansFriends.selected) &&
                        // Either both min and max are selected or neither one
                        ((this.audience.ageRange.min && this.audience.ageRange.max) || (!this.audience.ageRange.min && !this.audience.ageRange.max))) {
                        isValid = true;
                    } else {
                        isValid = false;
                    }
                }
            } else {
                isValid = false;
            }

            return isValid;
        };

        FbPost.prototype.convertToBackendFormat = function(callback) {
            // Convert the post to the backend format
            var formattedFbPost = angular.copy(this);

            delete formattedFbPost.audience;
            // The start date is an array (to work with the datepicker) but the backend expects a regular property
            formattedFbPost.startDate = formattedFbPost.startDate[0];
            // Format Audience Properties
            formattedFbPost.audience = {
                connections: [
                    {values: []}
                ],
                ageRange: [
                    {values: []}
                ],
                gender: [
                    {values: []}
                ],
                location:[
                    {values: []}
                ]
            };
            var newImage = {};
            var i,j;

            if (!formattedFbPost.url) {
                delete formattedFbPost.preview;
                //now images is object not array
                for (i = 0, j = formattedFbPost.images.length; i < j; i++) {
                    if (formattedFbPost.images[i].selected === true) {
                        //this is not bug. we send fullGuid with other name, in this case guid. Its for backend model.
                        newImage.guid = formattedFbPost.images[i].fullGuid;
                        //we have only one selected images so we can break this loop
                        break;
                    }
                }
                // Replace old array containing all the images by the new one containing only the selcted one
                if (newImage.guid) {
                    formattedFbPost.images = newImage;
                } else {
                    delete formattedFbPost.images;
                }
            } else { // Empty the images array for Link posts
                delete formattedFbPost.images;
            }

            // ---------------------------------- IF PROMOTED POST -----------------------------------------------
            if (formattedFbPost.isPromoted) {
                var value;
                // Connections
                for (var connectionSetting in this.audience.connections) {
                    if (this.audience.connections.hasOwnProperty(connectionSetting) && this.audience.connections[connectionSetting].selected){
                        switch (connectionSetting) {
                            case 'myFans':
                                value = {
                                    value: formattedFbPost.facebookPage.pageName,
                                    type: 'connections',
                                    id: formattedFbPost.facebookPage.pagePageId
                                };
                                formattedFbPost.audience.connections[0].values.push(value);
                                break;
                            case 'fansFriends':
                                value = {
                                    value: formattedFbPost.facebookPage.pageName,
                                    type: 'friendsOfConnections',
                                    id: formattedFbPost.facebookPage.pagePageId
                                };
                                formattedFbPost.audience.connections[0].values.push(value);
                                break;
                        }
                    }
                }
                // Gender
                for (var genderSetting in this.audience.gender) {
                    if (this.audience.gender.hasOwnProperty(genderSetting) && this.audience.gender[genderSetting].selected){
                        value = { value: this.audience.gender[genderSetting].display};
                        formattedFbPost.audience.gender[0].values.push(value);
                    }
                }
                // Age Range
                value = {value: this.audience.ageRange.min+' - '+this.audience.ageRange.max};
                formattedFbPost.audience.ageRange[0].values.push(value);


                // Assign the top country
                return fetchTopCountryForPage(formattedFbPost.facebookSponsoredPageId).then(retrieveInfoForCountry).then(function (response){
                    var country = response.data.data[0];
                    if (country) {
                        var value = {
                            value: country.name,
                            key: country.country_code, // jshint ignore:line
                            type: 'country'
                        };
                        formattedFbPost.audience.location[0].values.push(value);
                    }
                  callback(formattedFbPost);
                });
                // ---------------------------------- END IF PROMOTED POST -----------------------------------------------
            } else {
              if (!formattedFbPost.url) {
                delete formattedFbPost.preview;
                if (formattedFbPost.image) {
                  newImage.guid = formattedFbPost.image.fullGuid;
                  if (newImage.guid) {
                    formattedFbPost.image = newImage;
                  } else {
                    delete formattedFbPost.image;
                  }
                }
              } else {
                delete formattedFbPost.images;
              }

              //TODO we need to optimize this its duplicate code


              callback(formattedFbPost);
            }
        };

        function fetchTopCountryForPage (pageId) {
            return $http.get(apiUrl + 'api/v2/monitor/demographics/country/'+pageId).
                success(function (response) {
                    return response.data.topValue;
                }).error(function (response) {
                    console.log(response);
                });
        }

        function retrieveInfoForCountry (fetchTopCountryForPageResponse) {
            var country = fetchTopCountryForPageResponse.data.data.topValue;

            var fbAdAccounts = Campaign.getFacebookAdAccounts($rootScope.fractalAccountInfo.currentTeam);
            var fbAdAccountId = fbAdAccounts[0].accountId;

            return $http.get(apiUrl + '/api/v1/autocomplete/search/adcountry?&accountId='+fbAdAccountId+'&q='+country).
                success().error(function (response) {
                    console.log(response);
                });
        }

        return FbPost;

    });
