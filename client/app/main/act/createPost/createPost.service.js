'use strict';

angular.module('main')
    .factory('CreatePost', function ($http, ngDialog, identityConverter, apiUrl, $upload, $rootScope) {

        // Public API here
        return {

            openPostModal: function(parentScope){
                ngDialog.open({
                    template: 'app/main/act/createPost/createPost.html',
                    controller: 'CreatepostCtrl',
                    className: 'modal',
                    scope: parentScope
                });
            },

            publish: function(post, successCallback, errorCallback) {
                // Define the params of the endpoint
                var params = {
                    data: angular.toJson(post),
                    userId: $rootScope.fractalAccountInfo.userId,
                    teamId: $rootScope.fractalAccountInfo.currentTeam.id
                };

                console.log('multi channel post object in publish: ', angular.toJson(post));
                
                $http({
                    method: 'POST',
                    url: apiUrl + '/api/v2/post/publish',
                    data: $.param(params)
                })
                    .success(function(result) {
                        successCallback(identityConverter.convert(result, '@fsid'), result);
                    })
                    .error(errorCallback);
            },

            getImage: function(guid, successCallback, errorCallback){
                $http({
                    method: 'GET',
                    url: apiUrl + '/api/v2/image/get?guid=' + guid
                })
                    .success(function(result) {
                        successCallback(identityConverter.convert(result, '@fsid'), result);
                    })
                    .error(errorCallback);
            },

            uploadImageForPost: function(file, successCallback, errorCallback){
                $upload.upload({
                    url: apiUrl + '/api/v2/engage/upload?fields=id,fullGuid',
                    file: file
                })
                    .success(function(result) {
                        successCallback(identityConverter.convert(result, '@fsid'), result);
                    })
                    .error(errorCallback);
            },

            loadMetadata: function (url, successCallback, errorCallback){
                $http({
                    method: 'POST',
                    url: apiUrl + '/api/v2/post/loadMetadata',
                    data: $.param({url: url})
                })
                    .success(successCallback)
                    .error(errorCallback);
            },

            getBestTimeFacebook: function(pageId, successCallback, errorCallback){
                $http({
                    method: 'GET',
                    url: apiUrl + '/api/v2/post/page/bestTime/' + pageId + '?timestamp=' + new Date().getTime() + '&timezoneOffset=' + new Date().getTimezoneOffset()
                })
                    .success(successCallback)
                    .error(errorCallback);
            },

            getBestTimeTwitter: function(pageName, successCallback, errorCallback){
                $http({
                    method: 'GET',
                    url: apiUrl + '/v1/engage/twitter/page/bestTime/' + pageName + '?timestamp=' + new Date().getTime() + '&timezoneOffset=' + new Date().getTimezoneOffset() + '&takeSecond=true'
                })
                    .success(function(result) {
                        successCallback(identityConverter.convert(result, '@fsid'), result);
                    })
                    .error(errorCallback);
            }
        };
    });
