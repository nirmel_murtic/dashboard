'use strict';

angular.module('act')
    .controller('CreatepostCtrl', function ($scope, $rootScope, $state, ngDialog, CreatePost, library, links, $q, Campaign, FbPost, TwPost, $timeout, apiUrl, $translatePartialLoader) {

        $translatePartialLoader.addPart('app/main/act/act');

        // Everything related to the api calls and models gathered from rootScope
        $scope.api = {};
        // Everything related to the forms
        $scope.form = {};
        // Define currentProject if it has not been before
        $rootScope.fractalAccountInfo.currentTeam.currentProject = $rootScope.fractalAccountInfo.currentTeam.currentProject || $rootScope.fractalAccountInfo.currentTeam.projects[0];
        // Retrieve Facebook Pages for the current team
        $scope.api.facebookPages = Campaign.getFacebookPages($rootScope.fractalAccountInfo.currentTeam);
        // Retrieve Facebook Ad accounts for the current team
        $scope.api.facebookAdAccounts = Campaign.getFacebookAdAccounts($rootScope.fractalAccountInfo.currentTeam);

        $scope.selectingFromLibrary = false;

        $scope.apiUrl = apiUrl;

        $scope.view = {
            isPostAnother: false,
            buttonTitle: 'Post Now'
        };

        // Facebook is the first tab
        $scope.channel = {
            name: 'facebook'
        };



        $scope.getTwitterPages = function(){
            var twitterPages = [];
            var socialAccounts = $rootScope.fractalAccountInfo.currentTeam.socialAccounts;
            for(var i = 0; i < socialAccounts.length; i++){
                if(socialAccounts[i].social.socialType === 'Twitter'){
                    twitterPages.push(socialAccounts[i]);
                }
            }
            return twitterPages;
        };

        $scope.api.twitterPages = $scope.getTwitterPages();

        function initFbPost(fbPost) {
            return new FbPost(fbPost, $scope.api.facebookPages[0]);
        }

        function initTwPost(twPost) {
            return new TwPost(twPost, $scope.api.twitterPages[0]);
        }

        if (!$scope.fbPost) {
            $scope.fbPost = initFbPost();
        }

        if (!$scope.twPost) {
            $scope.twPost = initTwPost();
        }

        // ----------------------------------- URL Shortener methods -----------------------------------------------
        $scope.validateLink = function (url) {

            var defer = $q.defer();

            if(url){
                links.validateLink(url, function(result) {
                    if(result.data) {
                        defer.resolve(result.data);
                    }
                }, function(error) {
                    defer.reject(error);
                });
            } else {
                defer.reject('invalid url');
            }
            return defer.promise;
        };

        $scope.createShortenUrl = function(providedUrl){
            var channel = 'SOCIAL';

            $scope.urlDataModel = {
                description: providedUrl.description,
                shortUrlsCount: 0,
                projectId:  $rootScope.fractalAccountInfo.currentTeam.currentProject.id,
                budget: 0,
                shortened: true,
                urls:[{
                    originalUrl: providedUrl.urls[0].originalUrl,
                    enabled: providedUrl.urls[0].enabled,
                    description: providedUrl.urls[0].description,
                    paid: false,
                    channelType: channel
                }]
            };

            var defer = $q.defer();

            var urlCount = 1; // Hardcode to 1. Links can be created for several URLs but it's not relevant here

            links.createLink($scope.urlDataModel, urlCount, function(result) {
                defer.resolve(result.data);
            }, function(error) {
                defer.reject(error);
                console.log(error);
            });

            return defer.promise;

        };
        // ----------------------------------- End URL Shortener methods ---------------------------------------------

        // ----------------------------------- Library methods ---------------------------------------------
        $scope.loadImageFromLibrary = function(section) {
            $scope.initSection = section;

            $scope.disableLoadSelected = false;
            $scope.selectOne = true; //investigate

            library.openLibrary($scope);
        };

        $scope.useLibraryData = function(images){
            var i, j;
            if ($scope.channel.name === 'facebook') {
                // Link Post
                if ($scope.fbPost.url) {
//                    for (i = 0, j = images.length; i < j; i++) {
//                        // Build the full URL as this is used by the preview field
//                        var imageWithFullUrl = {
//                            fullGuid: apiUrl + '/api/v2/image/get?guid=' + images[i].fullGuid
//                        };
//                        $scope.fbPost.images.push(imageWithFullUrl);
//                    }
                    // The user can select only one image
                    var imageWithFullUrl = {
                        fullGuid: apiUrl + '/api/v2/image/get?guid=' + images[0].fullGuid,
                        selected: true
                    };
                    // Unselect all the images
                    for (i = 0, j = $scope.fbPost.images.length; i < j; i++){
                        $scope.fbPost.images[i].selected = false;
                    }
                    // Add the image to the slider array
                    $scope.fbPost.images.unshift(imageWithFullUrl);
                    // Select the last one
                    $scope.fbPost.preview.thumbnail = imageWithFullUrl;
                } else { // Image post
                  images[0].selected = true;
                  $scope.fbPost.image = images[0];
                }
            } else if($scope.channel.name === 'twitter'){            
                images[0].selected = true;
                $scope.twPost.image = images[0];
            }
        };
        // ----------------------------------- End Library methods ---------------------------------------------

        $scope.publishPostSuccess = function(response) {
            console.log('response creation post: ',response);
            // Close the view if the Post Another checkbox hasn't been checked
            if (!$scope.view.isPostAnother) {
                $scope.view.postLaunchedSuccessfully = true;
                $timeout( function(){
                    ngDialog.close();
                }, 3000);
            } else {
                $scope.view.postLaunchedSuccessfully = true;
                $timeout( function(){
                    $scope.view.postLaunchedSuccessfully = false;
                    // Reset the model
                    $scope.fbPost = initFbPost($scope.fbPost);
                    // Reset the view
                    $scope.$broadcast('resetView', 'data');
                    // Reset the form
                    $scope.form.fbPostSubmitted = false;
                    $scope.form.fbPost.$setPristine();
                }, 3000);

            }
        };

        $scope.publishPostError = function(error){
            console.log(error);
        };


        $scope.publish = function () {
            $scope.form.fbPostSubmitted = true;
            
            $scope.form.twPostSubmitted = true;
            console.log('fbPostIsValid: ', $scope.fbPost.isValid());


            if ($scope.fbPost.isValid()){
                // Convert the FbPost to the format expected by the backend
                $scope.fbPost.convertToBackendFormat(function (formattedFbPost) {
                    // Create the post object that contains all the channels
                    var post = {
                        channels: [formattedFbPost]
                    };
                    // Publish the post
                    CreatePost.publish(post,
                        function() {
                            // Close the view if the Post Another checkbox hasn't been checked
                            if (!$scope.view.isPostAnother) {
                                $scope.view.postLaunchedSuccessfully = true;
                                $timeout( function(){
                                    ngDialog.close();
                                }, 3000);
                            } else {
                                $scope.view.postLaunchedSuccessfully = true;
                                $timeout( function(){
                                    $scope.view.postLaunchedSuccessfully = false;
                                    // Reset the model
                                    $scope.fbPost = initFbPost($scope.fbPost);
                                    // Reset the view
                                    $scope.$broadcast('resetView', 'data');
                                    // Reset the form
                                    $scope.form.fbPostSubmitted = false;
                                    $scope.form.fbPost.$setPristine();
                                }, 3000);

                            }
                        }, function (error) {
                            console.log(error);
                        });

                });


            }

            if($scope.twPost.isValid()){
                CreatePost.publish($scope.twPost,
                    $scope.publishPostSuccess, $scope.publishPostError);
            }

        };

        $scope.redirectToAlign = function() {
            ngDialog.close();
            $state.go('main.align', { refresh : true });
        };
    });

