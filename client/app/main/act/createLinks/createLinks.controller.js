'use strict';

angular.module('act')
  .controller('CreatelinksCtrl', function (projectService, $scope, $state, $rootScope, $translatePartialLoader, $translate, regex, links, $timeout, ngDialog, $interval, $q, fsConfirm) {
    $translatePartialLoader.addPart('app/main/act/act');

    $scope.selectedChannel = [];

    $scope.updateChannel = [];

    $scope.availableChannels = [
      {
        name: 'Social',
        key: 'SOCIAL'
      },
      {
        name: 'Display',
        key: 'DISPLAY'
      },
      {
        name: 'Email',
        key: 'EMAIL'
      },
      {
        name: 'Search',
        key: 'SEARCH'
      },
      {
        name: 'Retargeting',
        key: 'RETARGETING'
      },
      {
        name: 'Affiliate',
        key: 'AFFILIATE'
      },
      {
        name: 'Publication/PR',
        key: 'PUBLICATION'
      },
      {
        name: 'Other',
        key: 'OTHER'
      }];

    if(!$rootScope.fractalAccountInfo.currentTeam.projects) {
      projectService.getProjects($rootScope.fractalAccountInfo.currentTeam.id, function (result) {
        $rootScope.fractalAccountInfo.currentTeam.projects = result.data.projects;

        if($rootScope.fractalAccountInfo.currentTeam.currentProject) {
          $scope.selectedProject = _.find($rootScope.fractalAccountInfo.currentTeam.projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProject.id});
        }
      }, function (error) {
        console.log(error.error.message);
      });
    } else if($rootScope.fractalAccountInfo.currentTeam.currentProject){
      $scope.selectedProject = _.find($rootScope.fractalAccountInfo.currentTeam.projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProject.id});
    }

    $scope.organicSwitcher = false;

    $scope.paidSwitcher = false;

    $scope.pasteUrlRegex = regex.SHORTURL;

    $scope.urlCountRegex = regex.NUMBERSWITHRANGE;

    $scope.urlBlocks = false;

    $scope.initialTable = false;

    $scope.appendId = true;

    $scope.selectChannel = function(channel) {
      $scope.selectedChannel = channel;
    };

    $scope.createShortenUrl = function(providedUrl){

      if(!$scope.numberOfUrls) {
        $scope.urlCount = 1;
      } else {
        $scope.urlCount = $scope.numberOfUrls;
      }

      var channel = $scope.selectedChannel.key;

      $scope.urlDataModel = {
        description: providedUrl.description,
        shortUrlsCount: 0,
        projectId: $scope.selectedProject.id,
        budget: 0,
        shortened: $scope.appendId,
        urls:[{
          originalUrl: encodeURIComponent(providedUrl.urls[0].originalUrl),
          enabled: providedUrl.urls[0].enabled,
          description: providedUrl.urls[0].description,
          paid: $scope.paidSwitcher,
          channelType: channel,
          utm: providedUrl.urls[0].utm
        }]
      };

      if(providedUrl) {
        $scope.urlCreated = true;
        links.createLink($scope.urlDataModel, $scope.urlCount, function(result) {
          $scope.shortenUrlPreview = result.data;
          if(!$rootScope.fractalAccountInfo.currentTeam.currentProject.links) {
            $rootScope.fractalAccountInfo.currentTeam.currentProject.links = [];
            $rootScope.fractalAccountInfo.currentTeam.currentProject.links.push($scope.shortenUrlPreview);
          } else {
            $rootScope.fractalAccountInfo.currentTeam.currentProject.links.push($scope.shortenUrlPreview);
            $rootScope.fractalAccountInfo.currentTeam.currentProject.links.sort(function (a, b) {
              if (a.createdAt > b.createdAt) {
                return -1;
              }
              if (a.createdAt < b.createdAt) {
                return 1;
              }
              return 0;
            });
          }
          if($scope.urlCount > 1) {
            $scope.initialTable = true;
            $scope.numberOfUrls = 0;
            $scope.gridOptions.data = $scope.shortenUrlPreview.urls;
            for(var i=0; i<$scope.gridOptions.data.length;i++) {
              $scope.gridOptions.data[i].countId = i+1;
              if($scope.gridOptions.data[i].paid === true) {
                $scope.gridOptions.data[i].paidId = 'PAID';

              } else if ($scope.gridOptions.data[i].paid === false) {
                $scope.gridOptions.data[i].paidId = 'ORGANIC';
              }
            }
            $scope.updateChannel = $scope.selectedChannel;
          } else {
            $scope.urlBlocks = true;
          }
        }, function(error) {
          console.log(error);
        });
      }
    };

    $scope.showMissingChannel = false;

    $scope.showMissingLink = false;

    $scope.urlsNumbersError = false;

    $scope.showMissingProject = false;

    $scope.$watch('urlForPreview', function (newValue, oldValue) {
      if(!newValue && !oldValue) {
        return false;
      } else if(newValue === oldValue) {
        return false;
      } else {
        var newUrl = $scope.checkURLRegex(newValue);

        if(newUrl !== newValue) {

          $timeout(function(){
            $scope.shortUrl.urlForPreview.$setValidity('valid', false);
          });
        } else {
          $scope.shortUrl.urlForPreview.$setValidity('valid', true);
        }
      }
    });

    $scope.checkURLRegex = function(url) {
      var urlArray;

      if(url) {
        urlArray = url.match($scope.pasteUrlRegex);

        if(urlArray[0]) {
          return urlArray[0];
        } else {
          return false;
        }
      }
    };

    $scope.shortenClicked = function(appendId){

      if(!$scope.selectedProject) {
        $timeout(function() {
          $scope.showMissingProject = true;
          $timeout(function() {
            $scope.showMissingProject = false;
          }, 3000);
        }, 0);
        return;
      }

      if($scope.getUrlShow && !$scope.numberOfUrls) {
        $timeout(function() {
          $scope.urlsNumbersError = true;
          $timeout(function() {
            $scope.urlsNumbersError = false;
          }, 3000);
        }, 0);
        return;
      }

      if(!$scope.urlForPreview) {
        $timeout(function() {
          $scope.showMissingLink = true;
          $timeout(function() {
            $scope.showMissingLink = false;
          }, 3000);
        }, 0);
        return;
      }

      if(appendId) {
        $scope.appendId = false;
      }

      if(!$scope.selectedChannel.key) {
        $timeout(function() {
          $scope.showMissingChannel = true;
          $timeout(function() {
            $scope.showMissingChannel = false;
          }, 3000);
        }, 0);
        return;
      }

      if($scope.urlForPreview && $scope.selectedChannel.key && $scope.selectedProject.name && $scope.shortUrl.$valid){
        links.validateLink($scope.urlForPreview, function(result) {
          if(result.data) {
            $scope.createShortenUrl(result.data);
          }
        }, function(error) {
          console.log(error.error.message);
          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
      }
    };

    $scope.switchTo = function(value) {
      if(value === 'paid') {
        $scope.paidSwitcher = true;
        $scope.organicSwitcher = false;
      } else if(value === 'organic') {
        $scope.paidSwitcher = false;
        $scope.organicSwitcher = true;
      }
    };

    $scope.updateLink = function() {
      $scope.updateUrlModel = {
        id: $scope.shortenUrlPreview.id,
        projectId: $scope.shortenUrlPreview.projectId,
        description: $scope.shortenUrlPreview.urls[0].description,
        urls:[{
          id: $scope.shortenUrlPreview.urls[0].id,
          campaignId: $scope.shortenUrlPreview.urls[0].campaignId,
          groupId: $scope.shortenUrlPreview.urls[0].groupId,
          description: $scope.shortenUrlPreview.urls[0].description
        }]
      };
      links.updateLink($scope.updateUrlModel, function() {
        if($rootScope.fractalAccountInfo.currentTeam.currentProject.links) {
          $scope.newDescription = _.find($rootScope.fractalAccountInfo.currentTeam.currentProject.links, {'id': $scope.shortenUrlPreview.id});
          $scope.newDescription.description = $scope.updateUrlModel.description;
        }
      }, function(error) {
        console.log(error);
      });
    };

    $scope.gotoLinksManager = function() {
      ngDialog.close();
      $timeout(function() {
        $state.go('main.act.linksManager');
      }, 500);
    };

    $scope.linkCopiedClipboard = false;

    $scope.copyToClipboard = function() {
      $timeout(function() {
        $scope.linkCopiedClipboard = true;
        $timeout(function() {
          $scope.linkCopiedClipboard = false;
        }, 2000);
      }, 0);
    };

    // Multi-channel urls table settings

    $scope.gridOptions = {
      enableGridMenu: false,
      exporterLinkLabel: 'shortenUrls.csv',
      enableRowSelection: true,
      enableSelectAll: true,
      selectionRowHeaderWidth: 30,
      rowHeight: 30,
      showGridFooter: false,
      exporterCsvLinkElement: angular.element(document.querySelectorAll('.custom-csv-link-location')),
      exporterFieldCallback: function( grid, row, col, input ) {
        if( col.name === 'paidId' ) {
          switch (input) {
            case 'ORGANIC':
              return 'Organic';
            case 'PAID':
              return 'Paid';
          }
        } else if(col.name === 'channelType') {
          switch (input) {
            case 'SOCIAL':
              return 'Social';
            case 'DISPLAY':
              return 'Display';
            case 'EMAIL':
              return 'Email';
            case 'SEARCH':
              return 'Search';
            case 'RETARGETING':
              return 'Retargeting';
            case 'AFFILIATE':
              return 'Affiliate';
            case 'PUBLICATION':
              return 'Publication/PR';
            case 'OTHER':
              return 'Other';
          }
        } else {
          return input;
        }
      }
    };

    $scope.exportShortUrls = function(rowType){

      var columnType = 'all';
      var myElement = angular.element(document.querySelectorAll('.custom-csv-link-location'));
      if(rowType === 'selected' && $scope.gridApi.selection.getSelectedRows().length < 1){
        return false;
      } else {
        $scope.gridApi.exporter.csvExport( rowType, columnType, myElement );
      }

    };

    $scope.saveRow = function( rowEntity ) {
      var promise = $q.defer();
      $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );

      if(rowEntity.paidId === 'PAID') {
        rowEntity.paid = true;
      } else if (rowEntity.paidId === 'ORGANIC') {
        rowEntity.paid = false;
      }

      links.updateMultiChannelUrl(rowEntity, function() {
        promise.resolve();
      }, function(error) {
        promise.reject();
        console.log(error);
      });
    };

    $scope.gridOptions.onRegisterApi = function(gridApi){
      $scope.gridApi = gridApi;
      gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
      gridApi.selection.on.rowSelectionChanged($scope,function(){
      });
    };

    $scope.setMultiLinkProperties = function() {
      var selectedRows = $scope.gridApi.selection.getSelectedRows();

      if(selectedRows.length > 0) {
        $scope.updateLink();
        links.updateMultiChannelUrls(selectedRows,
          $scope.updateChannel.key,
          $scope.paidSwitcher, function() {
            angular.forEach(selectedRows, function(value) {
              value.channelType = $scope.updateChannel.key;
              if($scope.paidSwitcher) {
                value.paidId = 'PAID';
              } else {
                value.paidId = 'ORGANIC';
              }
            });
            $scope.enableChannelUpdate = false;
          }, function(error) {
            console.log(error);
          });
      } else {
        $scope.updateLink();
      }

    };

    $scope.changedChannelUpdate = function(property) {
      $scope.updateChannel = property;
    };

    $scope.onProjectChange = function(project) {
      $rootScope.fractalAccountInfo.currentTeam.currentProject = project;
      $scope.selectedProject = project;
    };

    $scope.gridOptions.columnDefs = [
      {
        name: 'countId',
        displayName: '#',
        enableColumnMenu: false,
        width: '5%',
        enableCellEdit: false
      },
      {
        name: 'url',
        displayName: 'URL',
        enableColumnMenu: false,
        width: '30%',
        enableCellEdit: false,
        cellTooltip: true
      },
      {
        name: 'channelType',
        displayName: 'Channel',
        editableCellTemplate: 'ui-grid/dropdownEditor',
        editDropdownValueLabel: 'name',
        allowCellFocus : true,
        enableColumnMenu: false,
        width: '15%',
        cellFilter: 'channelMap',
        cellTooltip: true,
        editDropdownOptionsArray: [
          {
            id: 'SOCIAL',
            name: 'Social'
          },
          {
            id: 'DISPLAY',
            name: 'Display'
          },
          {
            id: 'EMAIL',
            name: 'Email'
          },
          {
            id: 'SEARCH',
            name: 'Search'
          },
          {
            id: 'RETARGETING',
            name: 'Retargeting'
          },
          {
            id: 'AFFILIATE',
            name: 'Affiliate'
          },
          {
            id: 'PUBLICATION',
            name: 'Publication/PR'
          },
          {
            id: 'OTHER',
            name: 'Other'
          }
      ]},
      {
        name: 'paidId',
        displayName: 'Type',
        enableColumnMenu: false,
        allowCellFocus : true,
        width: '13%',
        editableCellTemplate: 'ui-grid/dropdownEditor',
        editDropdownValueLabel: 'name',
        cellFilter: 'paidMap',
        editDropdownOptionsArray: [
          {
            id: 'PAID',
            name: 'Paid'
          },
          {
            id: 'ORGANIC',
            name: 'Organic'
          }
        ]
      },
      {
        name: 'channelText',
        displayName: 'Description',
        enableColumnMenu: false,
        width: '32%',
        cellTooltip: true
      }
    ];

    $scope.gridOptions.multiSelect = true;

    $scope.gridOptions.modifierKeysToMultiSelectCells = true;

    // Multi-channel urls table settings
  })
  .filter('channelMap', function() {
    var channelHash = {
      'SOCIAL': 'Social',
      'DISPLAY': 'Display',
      'EMAIL': 'Email',
      'SEARCH': 'Search',
      'RETARGETING': 'Retargeting',
      'AFFILIATE': 'Affiliate',
      'PUBLICATION': 'Publication/PR',
      'OTHER': 'Other'
    };

    return function(input) {
      if (!input){
        return '';
      } else {
        return channelHash[input];
      }
    };
  })
  .filter('paidMap', function() {
    var paidHash = {
      'PAID': 'Paid',
      'ORGANIC': 'Organic'
    };

    return function(input) {
      if (!input){
        return '';
      } else {
        return paidHash[input];
      }
    };
  });
