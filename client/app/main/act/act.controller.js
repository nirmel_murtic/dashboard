'use strict';

angular.module('act')
  .controller('ActCtrl', function ($state) {
    $state.go('main.act.campaignManager.home', { filter: 'active' });
  });
