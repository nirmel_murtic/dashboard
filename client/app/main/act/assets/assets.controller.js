'use strict';

angular.module('act')
  .controller('AssetsCtrl', function ($scope, library) {
    console.log('Assets Loaded');

    $scope.openLibrary = function(section) {
      library.openLibrary(section);
    };

  });
