'use strict';

angular.module('act')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.act.assets', {
        permissions: '<LOGGED_IN> && <ACT>',
        url: '/assets',
        views: {
          '@main': {
            templateUrl: 'app/main/act/assets/assets.html',
            controller: 'AssetsCtrl'
          }
        },
        data: {
          displayName: 'Assets',
          active: 'assets'
        },
        resolve: {
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/act/act');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
