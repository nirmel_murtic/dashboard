'use strict';

angular.module('act')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.act.linksManager', {
        permissions: '<LOGGED_IN> && <ACT>',
        url: '/links',
        views: {
          '@main': {
            templateUrl: 'app/main/act/linksManager/linksManager.html',
            controller: 'LinksmanagerCtrl'
          }
        },
        data: {
          displayName: 'Links',
          active: 'links'
        }
      })
      .state('main.act.linksManager.linkOverview', {
        permissions: '<LOGGED_IN> && <ACT>',
        url: '/linkOverview/:linkId',
        views: {
          '@main': {
            templateUrl: 'app/main/act/linksManager/linkOverview.html',
            controller: 'LinkoverviewCtrl'
          }
        },
        params: {
          project: null
        },
        data: {
          displayName: 'Link Overview',
          active: 'links'
        }
      });
  });
