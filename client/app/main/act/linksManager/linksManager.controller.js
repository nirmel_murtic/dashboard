'use strict';

angular.module('act')
  .controller('LinksmanagerCtrl', function ($scope, links, projectService, $state, $rootScope, fsConfirm, $filter) {

    $scope.selectedProject = [];

    $scope.timeFrame = [d3.time.day.offset(new Date(), -7), new Date()];

    $scope.updateProjectLinks = function(project) {
      if(project) {
        $rootScope.fractalAccountInfo.currentTeam.currentProject = project;
        $scope.selectedProject = _.find($rootScope.fractalAccountInfo.currentTeam.projects, {'id': project.id});
        $scope.selectedProject.links = [];
        var fromTime, toTime;
        if($scope.timeFrame) {
          fromTime = new Date($scope.timeFrame[0]).getTime();
          toTime = new Date($scope.timeFrame[1]).getTime();
        }
        links.getProjectURLs(project.id, fromTime, toTime, function(result) {
          $scope.gridOptions.data = result.data;
          $scope.selectedProject.links = $scope.gridOptions.data;
          $scope.gridOptions.data.sort(function (a, b) {
            if (a.createdAt > b.createdAt) {
              return -1;
            }
            if (a.createdAt < b.createdAt) {
              return 1;
            }
            return 0;
          });

          for(var j = 0;j < $scope.gridOptions.data.length; j++) {
            var obj = $scope.gridOptions.data[j];

            if(obj.enabled === false) {
              $scope.gridOptions.data.splice(j, 1);
              j--;
            }
          }

        }, function(error) {
          console.log(error);
        });
      }
    };

    $scope.updateTimeframe = function() {
      if($scope.selectedProject.id) {
        $scope.updateProjectLinks($scope.selectedProject);
      }
    };

    $scope.daterangePickerOpts = {
      maxDate : new Date(d3.time.day.ceil(new Date()) - 1),
      timePicker : false
    };

    if(!$rootScope.fractalAccountInfo.currentTeam.projects) {
      projectService.getProjects($rootScope.fractalAccountInfo.currentTeam.id, function (result) {
        $rootScope.fractalAccountInfo.currentTeam.projects = result.data.projects;

        if($rootScope.fractalAccountInfo.currentTeam.currentProject) {
          $scope.selectedProject = _.find($rootScope.fractalAccountInfo.currentTeam.projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProject.id});
        }
      }, function (error) {
        console.log(error.error.message);
      });
    } else if($rootScope.fractalAccountInfo.currentTeam.currentProject){
      $scope.selectedProject = _.find($rootScope.fractalAccountInfo.currentTeam.projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProject.id});
    }

    if($scope.selectedProject.name) {
      $scope.updateProjectLinks($scope.selectedProject);
    }

    $scope.createNewLink = function() {
      links.openCreateLink($scope);
    };

    $scope.openChildLink = function(row) {
      if(row.isSelected) {
        $state.go('main.act.linksManager.linkOverview', { linkId: row.entity.id, project: $scope.selectedProject});
      }
    };

    $scope.gridOptions = {
      enableGridMenu: false,
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      rowHeight: 70,
      showGridFooter:false,
      paginationPageSizes: [10, 25, 50],
      paginationPageSize: 10
    };

    $scope.gridOptions.modifierKeysToMultiSelect = false;

    $scope.gridOptions.multiSelect = false;

    $scope.gridOptions.onRegisterApi = function(gridApi){
      $scope.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,function(row){
        $scope.openChildLink(row);
      });
    };

    $scope.deactivateMultiLinkUrl = function(row) {
      fsConfirm('generic', {
        title: $filter('translate')('label.act.linksManager.AreYouSureYouWantToDeactivateThisLink'),
        body: row.description,
        yes: $filter('translate')('label.act.linksManager.DeactivateLink')
      }).then(function() {
        links.deactivateUrlGroup(row.id, function () {
          for (var j = 0; j < $scope.gridOptions.data.length; j++) {
            var obj = $scope.gridOptions.data[j];

            if (obj.id === row.id) {
              $scope.gridOptions.data.splice(j, 1);
              j--;
            }
          }
        }, function (error) {
          console.log(error);
        });
      });
    };

    $scope.gridOptions.columnDefs = [
      {
        name: 'description',
        displayName: 'Name',
        enableColumnMenu: false,
        width: '30%',
        cellTemplate: '<div class="url-description-mod">' +
        '{{row.entity[col.field]}}' +
        '</div>'
      },
      {
        name: 'clicks',
        displayName: 'Clickthroughs',
        enableColumnMenu: false,
        width: '13%',
        cellTemplate: '<div class="url-parent-mod">' +
        '{{row.entity[col.field]}}' +
        '</div>'
      },
      {
        name: 'groupShortUrl',
        displayName: 'URL',
        enableColumnMenu: false,
        width: '20%',
        cellTemplate: '<div class="url-parent-mod">' +
        '<span ng-if="row.entity.shortUrlsCount > 1">{{row.entity.shortUrlsCount}} Sub-Links</span>' +
        '<span ng-if="row.entity.shortUrlsCount === 1 && !row.entity.shortened">/{{row.entity.firstUrl.id}}</span>' +
        '<span ng-if="row.entity.shortUrlsCount === 1 && row.entity.shortened">{{row.entity.firstUrl.shortUrl}}</span>' +
        '</div>'
      },
      {
        name: 'createdAt',
        displayName: 'Creation Date',
        enableColumnMenu: false,
        width: '20%',
        cellTemplate: '<div class="url-parent-mod">' +
        '{{row.entity[col.field] | date:\'MMMM d, yyyy \'}}' +
        '</div>'
      },
      {
        name: 'shortened',
        displayName: 'Tracking',
        enableColumnMenu: false,
        width: '10%',
        cellTemplate: '<div class="url-parent-mod">' +
        '{{row.entity[col.field] | shortenedMap}}' +
        '</div>'
      },
      {
        name: 'deactivate',
        displayName: '',
        enableColumnMenu: false,
        width: '7%',
        cellTemplate: '<div class="url-parent-mod">' +
        '<button class="btn-primary btn-outline" style="float: left;" onclick="this.blur();" ng-click="grid.appScope.deactivateMultiLinkUrl(row.entity); $event.stopPropagation();">' +
        'Deactivate' +
        '</button>' +
        '</div>'
      }
    ];

  })
  .filter('shortenedMap', function() {
    var shortenHash = {
      true: 'Shorten'
    };

    return function(input) {
      if (!input){
        return 'Appended';
      } else {
        return shortenHash[input];
      }
    };
  });
