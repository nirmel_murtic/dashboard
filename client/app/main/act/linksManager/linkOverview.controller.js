'use strict';

angular.module('act')
  .controller('LinkoverviewCtrl', function ($scope, links, projectService, $stateParams, $state, $timeout, $q, fsConfirm, $filter) {

    $scope.detailedInfo = [];

    $scope.projectSelected = $stateParams.project;

    if($stateParams.linkId) {
      links.getGroupURLs($stateParams.linkId, function(result) {

        $scope.groupDetails = result.data;

        $scope.detailedInfo = result.data.urls;

        if($scope.detailedInfo.length > 1) {
          $scope.detailedInfo[0].multiLinkPrefix = '';
          if($scope.detailedInfo[0].shortUrl) {
            $scope.detailedInfo[0].multiLinkPrefix = $scope.detailedInfo[0].shortUrl.replace($scope.detailedInfo[0].numModifiedBase35,'');
          }
        }

        for(var i=0; i<$scope.detailedInfo.length;i++) {
          $scope.detailedInfo[i].countId = i+1;
          if($scope.detailedInfo[i].paid === true) {
            $scope.detailedInfo[i].paidId = 'Paid';
          } else if ($scope.detailedInfo[i].paid === false) {
            $scope.detailedInfo[i].paidId = 'Organic';
          }
        }

        $scope.detailedInfo.displayChannels = _.chain($scope.detailedInfo).map(function(item) {
          return item.channelType;
        }).uniq().value();

        $scope.detailedInfo.displayPaid = _.chain($scope.detailedInfo).map(function(item) {
          return item.paidId;
        }).uniq().value();

        if($scope.detailedInfo.length > 1) {
          $scope.gridOptions.data = $scope.detailedInfo;
          for(var j = 0;j < $scope.gridOptions.data.length; j++) {
            var obj = $scope.gridOptions.data[j];

            if(obj.enabled === false) {
              $scope.gridOptions.data.splice(j, 1);
              j--;
            }
          }
        }

      }, function(error) {
        console.log(error);
      });
    }

    $scope.$watch('groupDetails.description', function(newValue, oldValue) {
      if (newValue === oldValue) {
        return false;
      } else if($scope.firstDescription !== newValue){

        $scope.firstDescription = oldValue;

        $scope.updateUrlModel = {
          id: $scope.groupDetails.id,
          projectId: $scope.groupDetails.projectId,
          description: $scope.groupDetails.description,
          urls:[{
            id: $scope.groupDetails.urls[0].id,
            campaignId: $scope.groupDetails.urls[0].campaignId,
            groupId: $scope.groupDetails.urls[0].groupId,
            description: $scope.groupDetails.urls[0].description
          }]
        };

        links.updateLink($scope.updateUrlModel, function() {
        }, function(error) {
          $scope.groupDetails.description = $scope.firstDescription;
          console.log(error);
        });

      }
    }, true);

    $scope.backToLinks = function() {
      $state.go('main.act.linksManager');
    };

    $scope.createNewLink = function() {
      links.openCreateLink($scope);
    };

    $scope.gridOptions = {
      enableGridMenu: false,
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      rowHeight: 70,
      showGridFooter:false,
      paginationPageSizes: [10, 25, 50],
      paginationPageSize: 10,
      exporterLinkLabel: 'shortenUrls.csv',
      exporterCsvLinkElement: angular.element(document.querySelectorAll('.custom-csv-link-location')),
      exporterFieldCallback: function( grid, row, col, input ) {
        if(col.name === 'channelType') {
          switch (input) {
            case 'SOCIAL':
              return 'Social';
            case 'DISPLAY':
              return 'Display';
            case 'EMAIL':
              return 'Email';
            case 'SEARCH':
              return 'Search';
            case 'RETARGETING':
              return 'Retargeting';
            case 'AFFILIATE':
              return 'Affiliate';
            case 'PUBLICATION':
              return 'Publication/PR';
            case 'OTHER':
              return 'Other';
          }
        } else {
          return input;
        }
      }
    };

    $scope.gridOptions.modifierKeysToMultiSelect = false;

    $scope.gridOptions.multiSelect = false;

    $scope.saveRow = function( rowEntity ) {
      var promise = $q.defer();
      $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );

      if(rowEntity.paidId === 'PAID') {
        rowEntity.paid = true;
      } else if (rowEntity.paidId === 'ORGANIC') {
        rowEntity.paid = false;
      }

      links.updateMultiChannelUrl(rowEntity, function() {
        promise.resolve();
      }, function(error) {
        promise.reject();
        console.log(error);
      });
    };

    $scope.gridOptions.onRegisterApi = function(gridApi){
      $scope.gridApi = gridApi;
      gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
    };

    $scope.exportShortUrls = function(){
      var columnType = 'all';
      var rowType = 'all';
      var myElement = angular.element(document.querySelectorAll('.custom-csv-link-location'));
      $scope.gridApi.exporter.csvExport( rowType, columnType, myElement );
    };

    $scope.gridScope = function(row){

      var selectedRow = _.find($scope.gridApi.grid.rows, {'uid': row.uid});

      $timeout(function() {
        selectedRow.entity.showCopy = true;
        $timeout(function() {
          selectedRow.entity.showCopy = false;
        }, 2000);
      }, 0);

    };

    $scope.deactivateMultiLinkUrl = function(row) {
      fsConfirm('generic', {
        title: $filter('translate')('label.act.linksManager.AreYouSureYouWantToDeactivateThisLink'),
        body: row.channelText,
        yes: $filter('translate')('label.act.linksManager.DeactivateLink')
      }).then(function() {
        links.deactivateUrlMultiChannel(row.id, function() {
          for(var j = 0;j < $scope.gridOptions.data.length; j++) {
            var obj = $scope.gridOptions.data[j];

            if(obj.id === row.id) {
              $scope.gridOptions.data.splice(j, 1);
              j--;
            }
          }
        }, function(error) {
          console.log(error);
        });
      });
    };


    $scope.gridOptions.columnDefs = [
      {
        name: 'countId',
        displayName: '#',
        enableColumnMenu: false,
        width: '5%',
        enableCellEdit: false,
        headerCellClass: 'url-hash-header',
        cellTemplate: '<div class="url-hash-mod">' +
        '{{row.entity[col.field]}}' +
        '</div>'
      },
      {
        name: 'channelText',
        displayName: 'Description',
        enableColumnMenu: false,
        width: '30%',
        cellTemplate: '<div class="url-description-mod">' +
        '{{row.entity[col.field]}}' +
        '</div>'
      },
      {
        name: 'clicks',
        displayName: 'Clickthroughs',
        enableColumnMenu: false,
        enableCellEdit: false,
        width: '12%',
        cellTemplate: '<div class="url-copy-mod">' +
        '{{row.entity[col.field]}}' +
        '</div>'
      },
      {
        name: 'url',
        displayName: 'URL',
        enableColumnMenu: false,
        enableCellEdit: false,
        width: '23%',
        cellTemplate: '<div class="url-copy-mod" ng-mouseenter="hideLong = 100; fontSize = 9.5 + \'px\';" ng-mouseleave="hideLong = 30; fontSize = 16 + \'px\';">' +
        '<span ng-hide="row.entity.showCopy" clip-copy="row.entity[col.field]" class="copy-link" ng-init="row.entity.showCopy = false;" ng-click="grid.appScope.gridScope(row)">Copy</span>' +
        '<span ng-hide="row.entity.showCopy || row.entity.shortUrl" ng-style="{ \'font-size\' : fontSize }" ng-init="hideLong = 30; fontSize = 16 + \'px\';">{{row.entity[col.field] | stringLimiter:max:hideLong}}</span>' +
        '<span ng-hide="row.entity.showCopy || !row.entity.shortUrl">{{row.entity[col.field]}}</span>' +
        '<span class="copy-link fadein" style="font-weight: bold;" ng-show="row.entity.showCopy">Copied to clipboard</span>' +
        '</div>'
      },
      {
        name: 'channelType',
        displayName: 'Channel',
        enableColumnMenu: false,
        enableCellEdit: false,
        width: '13%',
        cellTemplate: '<div class="url-copy-mod">' +
        '{{row.entity[col.field] | channelMap}}' +
        '</div>'
      },
      {
        name: 'paidId',
        displayName: 'Type',
        enableColumnMenu: false,
        enableCellEdit: false,
        width: '10%',
        cellTemplate: '<div class="url-copy-mod">' +
        '{{row.entity[col.field]}}' +
        '</div>'
      },
      {
        name: 'deactivate',
        displayName: '',
        enableColumnMenu: false,
        enableCellEdit: false,
        width: '7%',
        cellTemplate: '<div class="url-copy-mod">' +
        '<button class="btn-primary btn-outline" style="float: left;" onclick="this.blur();" ng-click="grid.appScope.deactivateMultiLinkUrl(row.entity); $event.stopPropagation();">' +
        'Deactivate' +
        '</button>' +
        '</div>'
      }
    ];

  })
  .filter('channelMap', function() {
    var channelHash = {
      'SOCIAL': 'Social',
      'DISPLAY': 'Display',
      'EMAIL': 'Email',
      'SEARCH': 'Search',
      'RETARGETING': 'Retargeting',
      'AFFILIATE': 'Affiliate',
      'PUBLICATION': 'Publication/PR',
      'OTHER': 'Other'
    };

    return function(input) {
      if (!input){
        return '';
      } else {
        return channelHash[input];
      }
    };
  });
