'use strict';

angular.module('act', ['main', 'eventLog'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.act', {
        url: '/manage',
        permissions: '<LOGGED_IN> && <ACT>',
        views: {
          '@main': {
              templateUrl: 'app/main/act/act.html',
              controller: 'ActCtrl'
          }
        },
        data: {
          displayName: 'Manage'
        },
        resolve: {
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/act/act');
            $translatePartialLoader.addPart('app/main/eventLog/eventLog');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
