'use strict';

angular.module('act')
  .constant('CampaignManagerStatTypes', [
    {
      id : 'impressions',
      cumid : 'cumimpressions',
      label : 'label.act.Impressions',
      format : 'metricVolume',
      derived : false
    },
    {
      id : 'clicks',
      cumid : 'cumclicks',
      label : 'label.act.Clicks',
      format : 'metricVolume',
      derived : false
    },
    {
      id : 'conversions',
      cumid : 'cumconvs',
      label : 'label.act.Conversions',
      format : 'metricVolume',
      derived : false
    },
    {
      id : 'spent',
      cumid : 'cumspent',
      label : 'label.act.Spent',
      format : 'metricCurrency',
      derived : false
    },
    {
      id : 'ctr',
      cumid : 'cumctr',
      label : 'label.act.CTR',
      tooltip : 'label.act.clickthroughrate',
      format : 'metricPercentage',
      derived : true
    },
    {
      id : 'cvr',
      cumid : 'cumcvr',
      label : 'label.act.CVR',
      tooltip : 'label.act.conversionrate',
      format : 'metricPercentage',
      derived : true
    },
    {
      id : 'cpc',
      cumid : 'cumcpc',
      label : 'label.act.CPC',
      tooltip : 'label.act.costperclick',
      format : 'metricCurrency',
      derived : true
    },
    {
      id : 'cpm',
      cumid : 'cumcpm',
      label : 'label.act.CPM',
      tooltip : 'label.act.costper1000impressions',
      format : 'metricCurrency',
      derived : true
    },
    {
      id : 'cpa',
      cumid : 'cumcpa',
      label : 'label.act.CPA',
      tooltip : 'label.act.costperaction',
      format : 'metricCurrency',
      derived : true
    },
    {
      id : 'value',
      cumid : 'cumvalue',
      label : 'label.act.ConversionValueShort',
      tooltip : 'label.act.ConversionValue',
      format : 'metricCurrency',
      derived : false,
      onCondition : function(campaign) {
        return campaign.optimizeForRoas;
      }
    },
    {
      id : 'roas',
      cumid : 'cumroas',
      label : 'label.act.Roas',
      tooltip : 'label.act.ReturnOnAdSpend',
      format : 'metricPercentage',
      derived : true,
      onCondition : function(campaign) {
        return campaign.optimizeForRoas;
      }
    }
  ]);
