'user strict';

angular.module('act')
  .constant('CampaignmanagerTopLevelFilters', {
    ACTIVE : {
      filter : 'active',
      label : 'label.act.Live',
      key : 'ACTIVE',
      order : 0
    },
    PAST : {
      filter : 'completed',
      label : 'label.act.Past',
      key : 'PAST',
      order : 2
    },
    SCHEDULED : {
      filter : 'scheduled',
      label : 'label.act.Scheduled',
      key : 'SCHEDULED',
      order : 1
    },
    DRAFTS : {
      filter : 'drafted',
      label : 'label.act.Drafts',
      key : 'DRAFTS',
      order : 3
    }
  });
