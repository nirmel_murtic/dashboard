'use strict';

angular.module('act')
  .constant('SubCampaignChannelStatus', {
    adwords: {
      SUSPENDED : 'SUSPENDED',
      PENDING : 'PENDING',
      DELETED : 'DELETED',
      ENDED : 'ENDED',
      SERVING : 'SERVING',
      ELIGIBLE : 'ELIGIBLE',
      NONE : 'NONE',
      DISAPPROVED : 'DISAPPROVED'
    },
    facebook : {
      ACTIVE : 'ACTIVE',
      PAUSED : 'PAUSED',
      CAMPAIGN_PAUSED : 'CAMPAIGN_PAUSED',
      CAMPAIGN_GROUP_PAUSED : 'CAMPAIGN_GROUP_PAUSED',
      CREDIT_CARD_NEEDED : 'CREDIT_CARD_NEEDED',
      DISABLED : 'DISABLED',
      DISAPPROVED : 'DISAPPROVED',
      PENDING_REVIEW : 'PENDING_REVIEW',
      PREAPPROVED : 'PREAPPROVED',
      PENDING_BILLING_INFO : 'PENDING_BILLING_INFO',
      ARCHIVED : 'ARCHIVED',
      DELETED : 'DELETED'
    }
  });
