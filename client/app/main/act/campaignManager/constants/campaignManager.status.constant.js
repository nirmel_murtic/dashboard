'use strict';

angular.module('act')
  .constant('CampaignStatus', {
     'ACTIVE' : 'active',
     'SCHEDULED' : 'scheduled',
     'FAILED' : 'failed',
     'PENDING' : 'pending',
     'STOPPED': 'stopped',
     'PAUSED' : 'paused',
     'COMPLETED' : 'completed',
     'DRAFT' : 'draft',
     'UPDATING' : 'updating',
     'HIDDEN' : 'hidden'
   });
