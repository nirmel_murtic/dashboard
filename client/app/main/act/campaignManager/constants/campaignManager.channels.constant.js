'use strict';

angular.module('act')
  .constant('CampaignManagerChannels', {
    facebook : {
      id : 'facebook',
      label : 'label.act.Facebook',
      faClass: 'facebook',
      brandColor : '#3B5999' 
    },
    adwords : {
      id : 'adwords',
      label : 'label.act.Adwords',
      faClass : 'google',
      brandColor : 'rgba(66, 133, 244, 1)'
    },
    twitter : {
      id : 'twitter',
      label : 'label.act.Twitter',
      faClass : 'twitter',
      brandColor : '#55acee'
    }
  });
