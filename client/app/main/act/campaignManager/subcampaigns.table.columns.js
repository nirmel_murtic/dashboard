'use strict';

angular.module('act')
  .controller('SubCmpsTableColumns', function($scope) {
     
    $scope.columns = _($scope.columnDefs)
      .pairs()
      .filter(function(pair) {
        var def = pair[1];
        return !def.fixed && def.label; 
      })
      .map(function(pair) {
        var def = pair[1], key = pair[0];
        return [key, {
          key : key,
          label : def.label,
          active : key in $scope.customizedColumnDefs.subCampaigns
        }];
      })
      .zipObject()
      .value();

    // $scope.columns.unshift({ key: 'all', label: 'All KPIs' });
    $scope.allSelected = function() {
      return !_.any($scope.columns, function(d) {
        return !d.active;
      });
    };
    $scope.noneSelected = function() {
      return _.all($scope.columns, function(d) {
        return !d.active;
      });
    };
    $scope.selectAll = function() {
      _.each($scope.columns, function(d) {
        d.active = true;
      });
    };
    $scope.selectNone = function() {
      _.each($scope.columns, function(d) {
        d.active = false;
      });
    };


    $scope.isAll = $scope.allSelected();
    $scope.toggleAll = function() {
      if($scope.allSelected()) {
        $scope.selectNone();
      } else {
        $scope.selectAll();
      }
    };
    $scope.$watch('allSelected()', function(b) {
      $scope.isAll = b;
    });

    $scope.saveColumns = function() {
      var customDefs = _($scope.columnDefs)
        .pairs()
        .filter(function(pair) {
          var key = pair[0], def = pair[1];
          return def.fixed || ($scope.columns[key] && $scope.columns[key].active);
        })
        .zipObject()
        .value();
      var customParentDefs = _($scope.parentColumnDefs)
        .pairs()
        .filter(function(pair) {
          var key = pair[0], def = pair[1];
          return def.fixed || ($scope.columns[key] && $scope.columns[key].active);
        })
        .zipObject()
        .value();

      $scope.closeThisDialog({
        subCampaigns : customDefs,
        parentCampaigns : customParentDefs
      });
    };
      

  });
