'use strict';

angular.module('act')
  .controller('CampaignmanagerCtrl',
  function ($rootScope, $scope, $state, CampaignManagerStatTypes, $stateParams, 
            CampaignmanagerTopLevelFilters, projects, CampaignEntirety, fetchQueue, CampaignManagerActions, 
            campaignList, campaignsTopLevelFilter, CampaignStatus, CampaignmanagerWebApiUtils, Scroll) {
    /* jshint newcap: false */

    if(!$rootScope.fractalAccountInfo.currentTeam.projects) {
      $rootScope.fractalAccountInfo.currentTeam.projects = projects;
    }

    $scope.campaignProject = {};
    $scope.campaignProject.currentProject = $rootScope.fractalAccountInfo.currentTeam.currentProject;

    $scope.$watch('fractalAccountInfo.currentTeam.currentProject', function (newValue) {
      if(newValue) {
        $scope.campaignProject.currentProject = _.find(projects, {'id': newValue.id});
      }
    });

    $scope.onProjectChange = function() {
      $rootScope.fractalAccountInfo.currentTeam.currentProject = $scope.campaignProject.currentProject;
      $state.reload('main.act.campaignManager');
    };

    $scope.projects = _.sortBy(projects, 'createdAt');
    $scope.filterCampaignByProject = function() {
      return true;
      // return !$scope.campaignProject.currentProject || $scope.campaignProject.currentProject.id === cmp.projectId;
    };
    $scope.filterCampaignByTab = function(cmp) {
      return !cmp.tab || cmp.tab === campaignsTopLevelFilter.filterType; 
    };
    $scope.filterCampaignByNotHidden = function(cmp) {
      return cmp.status !== CampaignStatus.HIDDEN;
    };
    $scope.getCampaignsCount = function() {
      return _($scope.campaigns)
        .filter($scope.filterCampaignByTab)
        .filter($scope.filterCampaignByNotHidden)
        .value()
        .length;
    };
    $scope.CampaignEntirety = CampaignEntirety;
    $scope.statTypes = CampaignManagerStatTypes;

    $scope.categories = _(CampaignmanagerTopLevelFilters).values().sortBy('order').value();
    $scope.currentCategory = campaignsTopLevelFilter.filterType;
    $scope.changeCategory = function (categoryKey) {
      if(categoryKey !== $scope.currentCategory) {
        $state.go('main.act.campaignManager.home', { filter : categoryKey.toLowerCase() });
      }
    };
    $scope.campaigns = campaignList;
    $scope.sortingModes = {
      chronological : {
        value : 'chronological',
        label : campaignsTopLevelFilter.filterType === 'PAST' ? 'END TIME' : 'START TIME'
      }, 
      name : {
        value : 'name',
        label : 'A-Z'
      } 
    };
    $scope.sort = {
      mode : $stateParams.subset.sortColumn === 'superCampaignName' ? 'name': 'chronological'
    };

    function getSortOptions() {
      var sortColumn, sort='desc';
      if($scope.sort.mode === 'name') {
        sortColumn = 'superCampaignName';
        sort = 'asc';
      } else if (campaignsTopLevelFilter.filterType === 'PAST') {
        sortColumn = 'endTime';
        sort = 'desc';
      } else {
        sortColumn = 'startTime';
        sort = 'desc';
      }
      return {
        sort : sort,
        sortColumn : sortColumn
      };
    }

    $scope.updateSort = function() {
      var sortOpts = getSortOptions();
      var sort = sortOpts.sort;
      var sortColumn = sortOpts.sortColumn;
      var params = _.cloneDeep($stateParams);
      params.subset = {
        sort : sort,
        sortColumn : sortColumn
      };
      $state.go('.', params, { reloat: 'main.act.campaignManager' });
    };
    function onStateChangeStart(e, toState, toParams, fromState) {
      if(_.startsWith(toState.name, 'main.act.campaignManager.home') && _.startsWith(fromState.name, 'main.act.campaignManager.home')) {
        $scope.transitioning = true;
      }
    }
    $scope.$on('$stateChangeStart', onStateChangeStart);
    $scope.editCampaign = function(cmpId, type) {
      type = type || 'draft';
      $state.go('main.act.campaignCreation', { id: cmpId, type : type });
    };
    $scope.viewCampaign = function(cmpId) {
      $state.go('main.act.campaignManager.home.single', { superId: cmpId });
    };

    $scope.paging = {
      limit : 20,
      offset : 0,
      busy : false,
      hasNext : $scope.getCampaignsCount() > 20,
      hasPrev : function() {
        return this.offset > 0 && this.offsetStack.length && this.prevPages.length;
      },
      offsetStack : [],
      prevPages : []
    };

    /*
    $scope.checkNextPage = function() {
      if($scope.campaigns.length < $scope.paging.limit) {
        $scope.paging.hasNext = null;
        return;
      }
      var filter = CampaignmanagerTopLevelFilters[campaignsTopLevelFilter.filterType].filter; 
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var projectId = $scope.campaignProject.currentProject.id; 
      var promise = CampaignmanagerWebApiUtils.pokeCampaignSummaryList(teamId, filter, projectId, $scope.paging.offset+1);  
      promise.then(function() {
        $scope.paging.hasNext = true;
      }, function() {
        $scope.paging.hasNext = null;
      });
    };
    */
    $scope.loadPrevPage = function() {
      if(!$scope.paging.hasPrev()) {
        return;
      } 
      var cmps = $scope.paging.prevPages.pop();
      var prevOffset = $scope.paging.offsetStack.pop();
      $scope.paging.hasNext = true;
      var oldCampaigns = $scope.campaigns;
      $scope.campaigns = cmps;
      var scrollDepth = window.scrollY;
      if(cmps.length > oldCampaigns.length) {
        setTimeout(function() {
          if(window.scrollY === scrollDepth && window.scrollY > 150) {
            Scroll.top();
          }
        }, 100);
      }
      $scope.paging.offset = prevOffset;
    };
    function loadNextPage(offset) {
      if(!$scope.paging.hasNext) {
        return;
      } 
      var sortOpts = getSortOptions();
      var sort = sortOpts.sort;
      var sortColumn = sortOpts.sortColumn;
      offset = offset || $scope.paging.offset + $scope.paging.limit;
      var filter = CampaignmanagerTopLevelFilters[campaignsTopLevelFilter.filterType].filter; 
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var projectId = $scope.campaignProject.currentProject.id; 
      var promise = CampaignmanagerWebApiUtils.getCampaignSummaryList(teamId, filter, projectId, $scope.paging.limit+2, offset, sort, sortColumn);  
      $scope.paging.hasNext = false; // disable next until it loads
      $scope.paging.busy = true;
      promise.then(function(cmps) {
        CampaignEntirety.indexSuperCampaigns(cmps, campaignsTopLevelFilter.filterType);
        $scope.paging.prevPages.push($scope.campaigns);
        $scope.paging.offsetStack.push($scope.paging.offset);
        $scope.campaigns = cmps;
        $scope.paging.offset = offset;
        $scope.paging.hasNext = $scope.getCampaignsCount() > $scope.paging.limit;
        $scope.paging.busy = false;
      });
    }
    function fetchNMore(ndel) {
      if(ndel <= 0) {
        return;
      }
      // var cmps = $scope.campaigns;
      var ncmps = $scope.getCampaignsCount();
      var offset = $scope.paging.offset + ncmps - ndel;
      var filter = CampaignmanagerTopLevelFilters[campaignsTopLevelFilter.filterType].filter; 
      var teamId = $rootScope.fractalAccountInfo.currentTeam.id;
      var projectId = $scope.campaignProject.currentProject.id; 
      var sortOpts = getSortOptions();
      var sort = sortOpts.sort;
      var sortColumn = sortOpts.sortColumn;
      var promise = CampaignmanagerWebApiUtils.getCampaignSummaryList(teamId, filter, projectId, ndel, offset, sort, sortColumn);  
      promise.then(function(cmps) {
        var superIds = _($scope.campaigns)
          .map(function(c) { return [c.superId, true]; })
          .zipObject()
          .value();
        cmps = _.filter(cmps, function(c) { return !superIds[c.superId]; });
        CampaignEntirety.indexSuperCampaigns(cmps, campaignsTopLevelFilter.filterType);
        $scope.campaigns = $scope.campaigns.concat(cmps);
        $scope.paging.hasNext = $scope.getCampaignsCount() > $scope.paging.limit;
      });
    }
    var rQueue = fetchQueue.queue();
    window.rQueue = rQueue;
    function processQueue() {
      $scope.paging.busy = true;
      var promise = rQueue.collectAsync()
      .then(function(nDeleted) {
        $scope.paging.hasNext = false;
        console.log('fetching ' + nDeleted + ' more campaigns.');
        fetchNMore(nDeleted);    
      });
      promise['finally'](function() {
        $scope.paging.busy = false;
      });
      return promise;
    }
    $scope.loadNextPage = function(offset) {
      processQueue()['finally'](function() {
        loadNextPage(offset);
      });
    };

    if(CampaignEntirety.MANAGER_HOME) {
      CampaignEntirety.dispatcher.unregister(CampaignEntirety.MANAGER_HOME);
    }
    CampaignEntirety.MANAGER_HOME = CampaignEntirety.dispatcher.register(function(payload) {
      var action = payload.action;
      var superId = action.id;
      switch(action.type) {
        case CampaignManagerActions.STOP_SUPER:
          rQueue.pendingBuf.remove(superId);
          rQueue.readyBuf.add(superId);
          break;
        case CampaignManagerActions.STOPPING_SUPER:
          rQueue.pendingBuf.add(superId);
          processQueue();
          break;
        case CampaignManagerActions.HIDING_SUPER:
          rQueue.pendingBuf.add(superId);
          processQueue();
          console.log(rQueue.pendingBuf.size(), rQueue.readyBuf.size());
          break;
        case CampaignManagerActions.HIDE_DRAFT:
          rQueue.pendingBuf.remove(superId);
          rQueue.readyBuf.add(superId);
          console.log(rQueue.pendingBuf.size(), rQueue.readyBuf.size());
          break;
        case CampaignManagerActions.REVERT_SUPER_STATUS:
          rQueue.pendingBuf.remove(superId);
          console.log(rQueue.pendingBuf.size(), rQueue.readyBuf.size());
          break;
        default:
          return;
      }
    });

  });
