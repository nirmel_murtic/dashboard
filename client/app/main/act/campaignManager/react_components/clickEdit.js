'use strict';

angular.module('act')
  .factory('ClickEdit', function($state, TabularDispatcher, TabularConstants, DGColumnBorder, CampaignEntirety) {
    /* jshint quotmark:false, asi:true */

    function fmtBid(bitStr) {
      if(bitStr === '') {
        return 0;
      }
      var val = parseFloat(bitStr);
      if(_.isNumber(val) && val >=0) {
        return Math.floor(val * 100) / 100;
      }
      return null;
    }

    var ClickEdit = React.createClass({displayName: "ClickEdit",

      getInitialState : function() {
        return {
          active : false,
          draftValue : ''
        };
      },

      enable : function() {
        this.setState({ 
          draftValue : this.props.rowData.maxBid || 0,
          active: true 
        });
      },
      disable : function() {
        this.setState({ active: false });
      },

      keypress : function(e) {
        if(e.which === 13) {
          this.commit();
        } else if (e.which === 27) {
          this.discard();
        }
      },

      update : function() {
        var val;
        if(this.state.active) {
          val = this.refs.input.getDOMNode().value;
          this.setState({
            draftValue : val
          });
        }
      },
      commit : function() {
        if(!this.state.active) {
          return;
        }
        var prevBid = this.props.rowData.maxBid;
        var bid = fmtBid( this.state.draftValue );
        if(bid === prevBid) {
          this.disable();
          return;
        }
        if(null !== bid) {
          if(bid > 0) {
            this.props.rowData.maxBid = bid;
          } else {
            bid = null;
            this.props.rowData.maxBid = undefined;
          }
          CampaignEntirety.setMaxBid(this.props.rowData.parentId, bid).then(null, function() {
            this.props.rowData.maxBid = prevBid;
            this.forceUpdate();
          }.bind(this));
        }

        this.disable();
      },
      discard : function() {
        this.state.active = false;
        this.disable();
      },
      clear : function() {
        this.state.active = false;
        var prevBid = this.props.rowData.maxBid;
        this.props.rowData.maxBid = undefined;
        CampaignEntirety.setMaxBid(this.props.rowData.parentId, null).then(null, function() {
          this.props.rowData.maxBid = prevBid;
          this.forceUpdate();
        }.bind(this));
        this.disable();
      },

      componentDidUpdate : function() {
        var node;
        if(this.state.active) {
          node = this.refs.input.getDOMNode(); 
          node.focus();
        }
      },
    
      render : function() {

        var maxBidStr = (this.props.rowData.maxBid > 0) ? this.props.rowData.maxBid.toFixed(2) : 'None';

        return (
          React.createElement("div", {className:  "dg-"+this.props.column + " dg-table-cell"}, 
            React.createElement(DGColumnBorder, null), 
            React.createElement("div", {className:  "editable-field" + ((this.state.active && false) ? "" : " inactive"), 
                 onClick: (this.state.active || true) ? angular.noop: this.enable, 
                 title: "Click to edit"}, 
              React.createElement("span", {className: "editable-display"}, maxBidStr ), 
              React.createElement("span", {className: "editable-edit"}, React.createElement("i", {className: "fa fa-pencil"})), 
              React.createElement("input", {ref: "input", 
                     onChange: this.update, 
                     onKeyUp: this.keypress, 
                     onBlur: this.commit, 
                     className: "editable-input", 
                     value: this.state.draftValue}), 
              React.createElement("div", {className: "editable-buttons"}, 
                React.createElement("span", {onMouseDown: this.clear}, React.createElement("i", {className: "fa fa-trash"})), 
                React.createElement("span", {onMouseDown: this.discard}, React.createElement("i", {className: "fa fa-times"})), 
                React.createElement("span", null, React.createElement("i", {className: "fa fa-check"}))
              )
            )
          )
        );
      }
    });

    return ClickEdit;

  });
