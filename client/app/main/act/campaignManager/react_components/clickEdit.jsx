'use strict';

angular.module('act')
  .factory('ClickEdit', function($state, TabularDispatcher, TabularConstants, DGColumnBorder, CampaignEntirety) {
    /* jshint quotmark:false, asi:true */

    function fmtBid(bitStr) {
      if(bitStr === '') {
        return 0;
      }
      var val = parseFloat(bitStr);
      if(_.isNumber(val) && val >=0) {
        return Math.floor(val * 100) / 100;
      }
      return null;
    }

    var ClickEdit = React.createClass({

      getInitialState : function() {
        return {
          active : false,
          draftValue : ''
        };
      },

      enable : function() {
        this.setState({ 
          draftValue : this.props.rowData.maxBid || 0,
          active: true 
        });
      },
      disable : function() {
        this.setState({ active: false });
      },

      keypress : function(e) {
        if(e.which === 13) {
          this.commit();
        } else if (e.which === 27) {
          this.discard();
        }
      },

      update : function() {
        var val;
        if(this.state.active) {
          val = this.refs.input.getDOMNode().value;
          this.setState({
            draftValue : val
          });
        }
      },
      commit : function() {
        if(!this.state.active) {
          return;
        }
        var prevBid = this.props.rowData.maxBid;
        var bid = fmtBid( this.state.draftValue );
        if(bid === prevBid) {
          this.disable();
          return;
        }
        if(null !== bid) {
          if(bid > 0) {
            this.props.rowData.maxBid = bid;
          } else {
            bid = null;
            this.props.rowData.maxBid = undefined;
          }
          CampaignEntirety.setMaxBid(this.props.rowData.parentId, bid).then(null, function() {
            this.props.rowData.maxBid = prevBid;
            this.forceUpdate();
          }.bind(this));
        }

        this.disable();
      },
      discard : function() {
        this.state.active = false;
        this.disable();
      },
      clear : function() {
        this.state.active = false;
        var prevBid = this.props.rowData.maxBid;
        this.props.rowData.maxBid = undefined;
        CampaignEntirety.setMaxBid(this.props.rowData.parentId, null).then(null, function() {
          this.props.rowData.maxBid = prevBid;
          this.forceUpdate();
        }.bind(this));
        this.disable();
      },

      componentDidUpdate : function() {
        var node;
        if(this.state.active) {
          node = this.refs.input.getDOMNode(); 
          node.focus();
        }
      },
    
      render : function() {

        var maxBidStr = (this.props.rowData.maxBid > 0) ? this.props.rowData.maxBid.toFixed(2) : 'None';

        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <DGColumnBorder />
            <div className={ "editable-field" + ((this.state.active && false) ? "" : " inactive") } 
                 onClick={(this.state.active || true) ? angular.noop: this.enable}
                 title="Click to edit">
              <span className="editable-display">{ maxBidStr }</span>
              <span className="editable-edit"><i className="fa fa-pencil"></i></span>
              <input ref="input" 
                     onChange={this.update}
                     onKeyUp={this.keypress}
                     onBlur={this.commit}
                     className="editable-input" 
                     value={this.state.draftValue}/>
              <div className="editable-buttons">
                <span onMouseDown={this.clear}><i className="fa fa-trash"></i></span>
                <span onMouseDown={this.discard}><i className="fa fa-times"></i></span>
                <span><i className="fa fa-check"></i></span>
              </div>
            </div>
          </div>
        );
      }
    });

    return ClickEdit;

  });
