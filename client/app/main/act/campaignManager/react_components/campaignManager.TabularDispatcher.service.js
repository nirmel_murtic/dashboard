'use strict';

angular.module('act')
  /** Minimal Implementation for Flux Dispatcher, we do need anything more complex */
  .factory('TabularDispatcher', [function () {
    var _prefix = '';
    var _cid = 0;
    function Dispatcher() {
      this._callbacks = Object.create(null);
    }
    Dispatcher.prototype = {
      register : function(callback) {
        _cid++;
        var id = _prefix + _cid; 
        this._callbacks[id] = callback;
        return id;
      },
      unregisterAll : function() {
        this._callbacks = Object.create(null);
      },
      unregister : function(id) {
        if(!this._callbacks[id]) {
          console.warn('Callback in dispatcher does not exisit: ' + id);
        }
        delete this._callbacks[id];
      },
      dispatch : function (payload) {
        try {
          for (var id in this._callbacks) {
            if(_.isFunction(this._callbacks[id])) {
              this._callbacks[id](payload);
            }
          }
        } catch(err) {
          if(__DEV__) {
            console.log('Error dispatching payload', payload);
            console.error(err, err.stack);
          }
        }
      }
    };
    var tabularDispatcher = new Dispatcher();
    tabularDispatcher.handleViewAction = function(action) {
      this.dispatch({ source: 'view', action : action });
    };
    return tabularDispatcher;
  }]);
