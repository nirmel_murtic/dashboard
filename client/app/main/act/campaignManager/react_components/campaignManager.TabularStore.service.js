'use strict';

angular.module('act')
  .factory('TabularStore', ['fsUtils','TabularConstants', 'TabularDispatcher', function (utils, Constants, Dispatcher) {
    return function tabularStorefactory(columnDefs, items) {
      var t = transducers;
      var nrow = items.length;
      
      /** 
       *  Initialize State
       */
      
      var state = {
        sort : {
          column : '',
          order : ''
        },
        filterValues : _.mapValues(columnDefs, _.constant('')),
        rows : null
      };
      
      /** 
       * Consturct tablular data store
       * tabular is *NOT* directly visible to view (not part of state)
       * it's a mapping from index to relevant data entries
       * Since we do not need to support adding rows, we can bypass
       * using array data structure, as deleting rows will not affect 
       * sort order and filtering out deleted rows takes linear time
       * unlike arrays, delete row is O(1) fast
       * 
       */
      
      var columnIds = _.keys(columnDefs);
      var headers = columnIds.map(function(col) {
        var levels = null;
        if(columnDefs[col].type === 'atom') {
          levels = columnDefs[col].levels || _.uniq(items, col).map(function(level) {
            var format = columnDefs[col].format;
            return {
              name : level,
              formatted : format ? format(level) : level
            };
          });
        }
        return { 
          column: col, 
          label : columnDefs[col].label, 
          sort : columnDefs.sort !== false ? true : false, 
          columnType : columnDefs[col].type,
          levels : levels 
        };
      });
      var toArray = function (item) {
        return _.map(columnIds, function(c) {
          var format = columnDefs[c].format;
          return format ? format( item[c] ) : item[c].toString();  
        });
      };
      var tabular = _.reduce(items, function(store, item, index) {
        store[index] = {
          __checked__ : false,
          id : item.id,
          item : item.toJSON ? item.toJSON() : item, // if item is some model type with toJSON method, call it
          row : toArray(item),
          index : index
        };
        return store;
      }, Object.create(null));
      
      // alternative by-id lookup 
      var tabularById = _.reduce(tabular, function(byId, a) {
        byId[a.id] = a;
        return byId;
      }, Object.create(null));
      
      /** 
       * Sorting
       * cache ordering of item indices by column, all in asc order
       * use reduceRight in React's render for desc order
       * sorting on each column will ever happen once, so later sorting changes 
       * will be faster
       */

      var ordering = {
        nosort : _.range(nrow)
      };
      var sortbyColumn = function(col) {
        if(ordering[col]) {
          return ordering[col];
        }
        ordering[col] = _.sortBy(_.keys(tabular), function(i) {
          return tabular[i].item[col] ;
        }); 
        return ordering[col];
      };
      
      /** 
       * Filtering
       * Sorting happens before filtering in the pipeline ()
       * Filtering operations depend on column type, for atom, do a equality check, for numeric
       * parse expressions like '>=12' and apply associated arithmetic condition, for strings 
       * condition is contains substring specifed by filter value
       */
      
      
      var operators = {
        '<': function(x, y) { return x < y; },
        '<=': function(x, y) { return x <= y; },
        '>': function(x, y) { return x > y; },
        '>=': function(x, y) { return x >= y; },
        '=': function(x, y) { /* jshint eqeqeq:false */ return x == y; }
      };
      var operandRegex = /^((?:(?:[<>]=?)|=))s?([-]?\d+(?:\.\d+)?)$/;
      /** depend on column type, return different fitlerer, a filterer takes a datum and returns a bool */
      var constructFilterers = function(filterValues) {
        return columnIds.map(function(col) {
          var filterValue = filterValues[col];
          if(!filterValue) {
            return null;
          }
          if(columnDefs[col].type === 'atom') {
            return function(item) {
              return item[col] === filterValue;
            };
          } else if (columnDefs[col].type === 'string') {
            return function(item) {
              return item[col].toString().toLowerCase().indexOf( filterValue.toLowerCase() ) > -1;  
            };        
          } else if (columnDefs[col].type === 'numeric') {
            return function(item) {
              var opMatch = operandRegex.exec( filterValue ); 
              if(opMatch && opMatch.length === 3) {
                return operators[opMatch[1]](item[col], opMatch[2]);
              }
              return true;
            };
          }
        });
      };
      /** 
       * compse all filterers into a transducer, steps are:
       * 1. filter out non-functions
       * 2. transform filterer from taking datum to taking index (used in finding datum in store tabular)
       * 3. transform into composable transducers
       * 4. compose them!
       * returns false, if no filtering is applied, so we know later to skip filtering entirely
       */
      var composeFilter = function(filterers) {
        // transform a filter function taking an item to taking index
        function toIndexFilterer(f) {
          return function(index) {
            return index > -1 && index < nrow && tabular[index] && f(tabular[index].item);
          };
        }
        var transform = t.comp(
          t.filter(_.isFunction), 
          t.map(toIndexFilterer), 
          t.map(t.filter)
        );
        filterers = t.into([], transform, filterers);
        if(filterers.length === 1) {
          return filterers[0];
        }
        if(filterers.length > 1) {
          return t.comp.apply(t, filterers);
        }
        return false;
      };
      /**
       * Data Transformation Pipeline
       * Sorting (more expensive, cached) -> filtering 
       * updateSortedAndFiltered should be called whenever user interation mutates state.sort and state.filerValues
       */
      
      // filterTransducer will be re-constructed only if filterValues have changed
      var filterTransducer = null;
      var updateSortedAndFiltered = function(sort, filterValues, forceUpdate) {
        var sortChanged = !_.isEqual(sort, state.sort);
        var filterChanged = !_.isEqual(filterValues, state.filterValues);
        if(!forceUpdate && !sortChanged && !filterChanged) {
          return state.rows;
        }
        var sortedIndices = sortbyColumn(sort.column || 'nosort');
        if(filterChanged || filterTransducer === null) {
          filterTransducer = composeFilter(constructFilterers(filterValues));
        }
        var filteredIndices = filterTransducer ? t.into([], filterTransducer, sortedIndices) : sortedIndices;
        if(sort.order !== 'desc') {
          state.rows = _.reduce(filteredIndices, function(rows, index) {
            if(tabular[index]) {
              rows.push(tabular[index]);
            }
            return rows;
          }, []);
        } else {
          state.rows = _.reduceRight(filteredIndices, function(rows, index) {
            if(tabular[index]) {
              rows.push(tabular[index]);
            }
            return rows;
          }, []);
        }
        state.sort = sort;
        state.filterValues = filterValues;
        return state.rows;
      };
      /** 
       * Callbacks to be registered on data mutation
       * We can handle these type of mutations:
       * 1. Cell value change - invalidate the sorting cache for changed column, re-compute formatted values
       * 2. Rows deletetion - no need to invalidate sort, delete keys in tabular store, do a re-filtering
       * CANNOT hanlde rows addition
       */
      
      // cellMutaions is an array of objects in the form of { id: <item id>, <column> : <newValue> }
      var onCellsMutation = function(cellMutations) {
        cellMutations = _.isArray(cellMutations) ? cellMutations : [cellMutations];
        // all mutations
        var hasMutated = false;
        _.each(cellMutations, function(mut) {
          var a = tabularById[mut.id];
          if(!a) {
            return;
          }
          // all mutated columns
          _.each(mut, function(newVal, col) {
            if(col !== 'id' && _.has(a.item, col)) {
              hasMutated = hasMutated || a.item[col] !== newVal;
              a.item[col]  = newVal;
              // re compute formatted values
              a.row = toArray(a.item);
              // invalidate sorting cache
              if(ordering[col]) {
                delete ordering[col];
              }
            }
          });
        });
        // force update state
        if(hasMutated) {
          updateSortedAndFiltered(state.sort, state.filterValues, true);
        }
        return hasMutated;
      };
      var onRowsDeletion = function(rowIds) {
        rowIds = _.isArray(rowIds) ? rowIds : [rowIds];
        _.each(rowIds, function(id) {
          var a = tabularById[id];
          if(a) {
            delete tabularById[id];
            delete tabular[a.index];
          }
        });
        // force update state
        updateSortedAndFiltered(state.sort, state.filterValues, true);
      }; 
      
      var onRowChecked = function(rowIds, checked) {
        if(_.isUndefined(checked)) {
          checked = true;
        }
        rowIds = _.isArray(rowIds) ? rowIds : [rowIds];
        _.each(rowIds, function(id) {
          tabularById[id].__checked__ = checked;
        });
      };
      
      var setAllChecked = function() {
        _.each(state.rows, function(row) {
          row.__checked__ = true;
        });
      };
      var setNoneChecked = function() {
        _.each(state.rows, function(row) {
          row.__checked__ = false;
        });
      };
      
      /** Initialize state rows */
      state.rows = _.toArray(tabular);
      
      // transuder to get checked ids
      var checkedIds = t.comp(
        t.filter(function(a) { return a && a.__checked__; }),
        t.map(function(a) { return a.id; })
      );
      
      var TabularStore = utils.Emitter({
        getOriginalRows : function() {
          return _.toArray(tabular);
        },
        getRows : function() {
          return state.rows;
        },
        getSort : function() {
          return state.sort;
        },
        getFilters : function() {
          return state.filterValues;
        },
        getHeaders : function() {
          return headers;
        },
        getAllCheckedIds : function() {
          return t.into([], checkedIds, state.rows);
        },
        isAllChecked : function() {
          return _.every(state.rows, '__checked__');
        },
        isNoneChecked : function() {
          return !_.some(state.rows, '__checked__');
        },
        destory : function() {
          Dispatcher.unregister(this._storeId);
        }
      });
      TabularStore._storeId = Dispatcher.register(function(payload) {
        var action = payload.action;
        switch(action.actionType) {
          case Constants.CHANGE_SORT:
            updateSortedAndFiltered(action.sort, state.filterValues);
            break;
          case Constants.CHANGE_FILTERS:
            updateSortedAndFiltered(state.sort, _.extend({}, state.filterValues, action.filter));
            break;
          case Constants.CHECK:
            if(action.ids === 'all') {
              setAllChecked();
            } else {
              onRowChecked(action.ids, true);
            }
            break;
          case Constants.UNCHECK:
            if(action.ids === 'all') {
              setNoneChecked();
            } else {
              onRowChecked(action.ids, false);
            }
            break;
          case Constants.DELETE_ROWS:
            // only accept changes from angular
            // because angular managers communication with server
            if(payload.source === 'angular') {
              onRowsDeletion(action.ids);
            } else {
              return true;
            }
            break;
          case Constants.MUTATE_CELLS:
            if(payload.source === 'angular') {
              onCellsMutation(action.mutations);
            } else {
              return true;
            }
            break;
          default:
            return true;        
        }
        TabularStore.emit('change', payload.source);
      });
      
      return TabularStore;
    };
  }]);
