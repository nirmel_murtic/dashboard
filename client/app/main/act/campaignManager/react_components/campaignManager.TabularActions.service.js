'use strict';

angular.module('act')
  .factory('TabularActions', ['TabularConstants', 'TabularDispatcher', function(Constants, Dispatcher) {
    return {
      clickRow : function(id) {
        Dispatcher.handleViewAction({
          actionType : Constants.CLICK_ROW,
          id : id
        });
      },
      changeSort : function(column, order) {
        Dispatcher.handleViewAction({
          actionType: Constants.CHANGE_SORT,
          sort : {
            column : column,
            order : order
          }
        });
      },
      changeFilter : function(column, filterValue) {
        var filter;
        if(_.isObject(column)) {
          filter = column;
        } else {
          filter = {};
          filter[column] = filterValue || '';
        }
        Dispatcher.handleViewAction({
          actionType: Constants.CHANGE_FILTERS,
          filter : filter
        });
      }, 
      checkRows : function(ids, checked) {
        ids = _.isUndefined(ids) ? 'all' : ids;
        Dispatcher.handleViewAction({
          actionType: checked ? Constants.CHECK : Constants.UNCHECK,
          ids : ids
        });
      },
      deleteRows : function(ids) {
        Dispatcher.handleViewAction({
          actionType: Constants.DELETE_ROWS,
          ids : ids
        });
      },
      mutateCells : function(ids, mutation) {
        Dispatcher.handleViewAction({
          actionType: Constants.MUTATE_CELLS,
          mutations : _.map(ids, function(id) {
            return _.extend({ id: id }, mutation);
          })
        });
      }
    };
  }]);
