'use strict';

angular.module('act')
  .factory('SubCmpID', function($state, TabularDispatcher, TabularConstants) {
    /* jshint quotmark:false, asi:true */

    var DataCell = React.createClass({
      redirect : function() {
        var id = this.props.rowData.id;
        TabularDispatcher.handleViewAction({
          actionType : TabularConstants.CLICK_ROW,
          id : id
        });
      },
      render : function() {
        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <span onClick={ this.redirect } className="pointer">{ this.props.value }</span>
          </div>
        ); 
      }
    }); 

    return DataCell;
  });
