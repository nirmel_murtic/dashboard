'use strict';

angular.module('act')
  .constant('TabularConstants', {
    'CHANGE_SORT' : 'CHANGE_SORT',
    'CHANGE_FILTERS' : 'CHANGE_FILTERS',
    'CHECK' : 'CHECK',
    'UNCHECK' : 'UNCHECK',
    'DELETE_ROWS' : 'DELETE_ROWS',
    'CLICK_ROW' : 'CLICK_ROW',
    'MUTATE_CELLS' : 'MUTATE_CELLS'
  });
