'use strict';

angular.module('act')
  .factory('SubCmpWarning', function(SubCampaignChannelStatus) {
    /* jshint quotmark:false, asi:true */

    var WarningCell = React.createClass({
      redirect : function() {
        // var id = this.props.rowData.id;
      },

      hasProblem : function() {
        var row = this.props.rowData;
        if(!row.id) {
          return false;
        }
        switch (row.channel) {
          case 'adwords':
            return row.channelStatus === SubCampaignChannelStatus.adwords.SUSPENDED || 
                   row.channelStatus === SubCampaignChannelStatus.adwords.DELETED || 
                   row.channelStatus === SubCampaignChannelStatus.adwords.DISAPPROVED; 
          case 'facebook':
            return row.channelStatus === SubCampaignChannelStatus.facebook.CREDIT_CARD_NEEDED || 
                   row.channelStatus === SubCampaignChannelStatus.facebook.DISABLED || 
                   row.channelStatus === SubCampaignChannelStatus.facebook.DISAPPROVED || 
                   row.channelStatus === SubCampaignChannelStatus.facebook.PENDING_BILLING_INFO || 
                   row.channelStatus === SubCampaignChannelStatus.facebook.DELETED; 
          default:
            return false;
        }
      },

      render : function() {
        var hasProblem = this.hasProblem();
        return (
          <div className = { "dg-"+this.props.column + " dg-table-cell" }>
            <i className={ "pointer fa fa-warning text-error " + (hasProblem ? '' : 'hidden')}
               title={ this.props.rowData.channelStatus }>
            </i>
          </div>
        ); 
      }
    }); 

    return WarningCell;
  });
