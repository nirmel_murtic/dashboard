'use strict';

angular.module('act')
  .controller('CampaignmanagerCampaignSubcampaignsCtrl',
  function ($rootScope,
            $scope,
            $state,
            $filter,
            ngDialog,
            CampaignStatus,
            CampaignManagerStatTypes,
            CampaignmanagerTopLevelFilters,
            CampaignEntirety,
            campaignList,
            superCampaign,
            campaignsTopLevelFilter,
            subcampaigns,
            SubcampaignTableColumnDefs,
            SubcampaignTableRoasColumnDefs,
            ParentCampaignTableColumnDefs,
            RecentDateRange,
            TabularDispatcher,
            TabularConstants) {
    /* jshint newcap:false */


    $scope.campaign = superCampaign;

    $scope.subcampaigns = subcampaigns;
    $scope.columnDefs = $scope.campaign.optimizeForRoas ?
      _.assign({}, SubcampaignTableColumnDefs, SubcampaignTableRoasColumnDefs) :
      SubcampaignTableColumnDefs;

    $scope.parentColumnDefs = $scope.campaign.optimizeForRoas ?  
      _.assign({}, ParentCampaignTableColumnDefs, SubcampaignTableRoasColumnDefs) :
      ParentCampaignTableColumnDefs;

    $scope.customizedColumnDefs = {
      subCampaigns : _.clone($scope.columnDefs),
      parentCampaigns : _.clone($scope.parentColumnDefs),
    };

    var _i = 0;
    $scope.channelSummary = _.map(superCampaign.channels, function (c) {

      var status = 'active';

      if (_.all(subcampaigns, {status: 'paused'})) {
        status = 'paused';
      }
      if (_.all(subcampaigns, {status: 'ended'})) {
        status = 'ended';
      }

      var lowerBid = d3.min(_.filter(subcampaigns, {channel: c.id}), function (cmp) {
        return cmp.lowerBid > 0 ? cmp.lowerBid : 0;
      });
      var upperBid = d3.max(_.filter(subcampaigns, {channel: c.id}), function (cmp) {
        return cmp.upperBid > 0 ? cmp.upperBid : 0;
      });

      return {
        channel: c.id,
        parentId: superCampaign.parentCampaignIds[_i++],
        maxBid: superCampaign.maxBid[c.id],
        status: status,
        impressions: superCampaign.campaignStats.impressions[0][c.id],
        clicks: superCampaign.campaignStats.clicks[0][c.id],
        conversions: superCampaign.campaignStats.conversions[0][c.id],
        spend: superCampaign.campaignStats.spent[0][c.id],
        value: superCampaign.optimizeForRoas ? superCampaign.campaignStats.value[0][c.id] : 0,
        ctr: superCampaign.campaignStats.ctr[0][c.id],
        cpc: superCampaign.campaignStats.cpc[0][c.id],
        cpa: superCampaign.campaignStats.cpa[0][c.id],
        roas: superCampaign.optimizeForRoas ? superCampaign.campaignStats.roas[0][c.id] : NaN,
        lowerBid: lowerBid,
        upperBid: upperBid
      };
    });

    var dispatchToken = TabularDispatcher.register(function (payload) {
      var action = payload.action;
      var subCmp;
      if (action.actionType === TabularConstants.CLICK_ROW) {
        subCmp = _.find($scope.subcampaigns, {id: action.id});
        if (subCmp) {
          TabularDispatcher.unregister(dispatchToken);
          $state.go('main.act.campaignManager.home.single.subCampaign', {
            subCampaignId: subCmp.subCampaignId,
            channel: subCmp.channel
          });
        }
      }
    });

    $scope.openColumnSettings = function() {
      var surpress = function (e) {
        e.preventDefault();
      };
      var free = $rootScope.$on('$stateChangeStart', surpress);
      var modal = ngDialog.open({
        template: 'app/main/act/campaignManager/templates/subcampaigns.columnSelection.html',
        controller: 'SubCmpsTableColumns',
        scope: $scope,
        className: 'modal'
      });
      modal.closePromise.then(function(wrapper) {
        console.log(wrapper);
        if(!_.isObject(wrapper) || !wrapper.value || !wrapper.value.subCampaigns || !wrapper.value.parentCampaigns) {
          return; 
        }
        $scope.customizedColumnDefs = wrapper.value;
      })
      .then(free);
    
    };

  });
