'use strict';

angular.module('act')
  .filter('campaignStatus', function() {
    var labels = {
     'active' : 'label.act.Active',
     'scheduled' : 'label.act.Scheduled',
     'failed' : 'label.act.Failed',
     'pending' : 'label.act.Pending',
     'stopped': 'label.act.Stopped',
     'paused' : 'label.act.Paused',
     'completed' : 'label.act.Completed',
     'draft' : 'label.act.Draft', 
     'updating' : 'label.act.Updating' 
    };
    return function(status) {
      return labels[status] || status;
    };
  });
