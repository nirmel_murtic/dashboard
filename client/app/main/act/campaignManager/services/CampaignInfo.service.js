'use strict';

angular.module('act')
  .service('CampaignInfo', function(CampaignStatus, $filter, CampaignmanagerTopLevelFilters) {
  
    // analog to campaign settings on campaign creation, but applies to
    // campaigns at all levels (super, parent, sub)
    // also includes info about campaign status
    function CampaignInfo(info) {
      if(this.fromJSON) {
        this.fromJSON(info);
      }
    }
    CampaignInfo.prototype = {
      defaults : {
        id : null,
        name : '',
        channels: [{
          id : 'facebook',
          label : 'label.act.Facebook'
        }],
        budget: null,
        bid: null,
        status : null,
        interimStatus : null,
        runningStatus : null,
        startDate : null,
        endDate : null,
        goal : null,
        project : null
        // labels : null
      },
      getRunningStatus : function() {
        var now = Date.now();
        var runningStatus;
        if(!this.startDate || !this.endDate) {
          runningStatus = null;
        }
        else if(this.status === CampaignStatus.DRAFT) {
          runningStatus = CampaignStatus.DRAFT;
        } 
        else if(this.status === CampaignStatus.STOPPED) {
          runningStatus = CampaignStatus.STOPPED;
        } 
        else if(this.status === CampaignStatus.FAILED) {
          runningStatus = CampaignStatus.FAILED;
        } 
        else if(this.status === CampaignStatus.COMPLETED) {
          runningStatus = CampaignStatus.COMPLETED;
        } 
        else if(this.status === CampaignStatus.SCHEDULED) {
          runningStatus = CampaignStatus.SCHEDULED;
        }
        else if(this.status === CampaignStatus.PENDING) {
          runningStatus = CampaignStatus.PENDING;
        }
        else if(now < this.startDate.getTime()) {
          runningStatus = CampaignStatus.SCHEDULED;
        }
        else if(now >= this.startDate.getTime() && now <= this.endDate.getTime()) {
          runningStatus = CampaignStatus.ACTIVE;
        }
        else if(now > this.endDate.getTime()) {
          runningStatus = CampaignStatus.COMPLETED;
        }
        this.getRunningStatus = function() {
          return runningStatus;
        };
        return runningStatus;
      },
      getCategory : function() {
        switch(this.getRunningStatus()) {
          case CampaignStatus.ACTIVE:
            return CampaignmanagerTopLevelFilters.ACTIVE;
          case CampaignStatus.SCHEDULED:
            return CampaignmanagerTopLevelFilters.SCHEDULED;
          case CampaignStatus.COMPLETED:
            return CampaignmanagerTopLevelFilters.PAST;
          case CampaignStatus.DRAFT:
            return CampaignmanagerTopLevelFilters.DRAFTS;
          default:
            return CampaignmanagerTopLevelFilters.ACTIVE;
        }
      },
      isPausable : function() {
        return this.status !== CampaignStatus.STOPPED && 
          this.runningStatus && 
          this.runningStatus !== CampaignStatus.STOPPED  &&
          this.runningStatus !== CampaignStatus.FAILED &&
          this.runningStatus !== CampaignStatus.PENDING &&
          this.runningStatus !== CampaignStatus.DRAFT &&
          this.runningStatus !== CampaignStatus.COMPLETED;
      },
      isStoppable : function() {
        if(this.status === CampaignStatus.DRAFT || this.runningStatus === CampaignStatus.DRAFT) {
          return false;
        }
        if(this.status === CampaignStatus.FAILED || this.runningStatus === CampaignStatus.FAILED) {
          return false;
        }
        if(this.status === CampaignStatus.PENDING || this.runningStatus === CampaignStatus.PENDING) {
          return false;
        }
        return this.runningStatus && 
          this.runningStatus !== CampaignStatus.STOPPED  &&
          this.runningStatus !== CampaignStatus.COMPLETED;
      },
      hasReport : function() {
        return this.runningStatus !== CampaignStatus.DRAFT &&
          this.runningStatus !== CampaignStatus.PENDING && 
          this.runningStatus !== CampaignStatus.FAILED; 
      },
      fromJSON : function(json) {
        if(!json || _.size(json) === 0) {
          _.assign(this, this.defaults);
          return this;
        }
        this.projectId = json.projectId;
        this.name = json.campaignName ? json.campaignName : this.defaults.name;
        this.budget = json.budget ? parseFloat(json.budget) : this.defaults.budget;
        // this.bidType = json.bidType ? json.bidType : this.defaults.bidType;
        // this.bid = null;
        if(json.superCmpStatus && json.superCmpStatus.toLowerCase() === 'deleted') {
          this.status = 'stopped';
        } else { 
          this.status = json.superCmpStatus ? json.superCmpStatus.toLowerCase() : this.defaults.status;
        }
        this.startDate = new Date(json.startDate || json.startTime);
        this.endDate = new Date(json.endDate || json.endTime);
        this.campaignId = json.superCmpId;
        this.superId = json.superCmpId;

        try {
          this.goalObject = json.projectGoal;
          this.goal = $filter('fsGoalFmt')(json.projectGoal);
        } catch (err) {
          this.goal = '';
        }

        this.runningStatus = this.getRunningStatus();

        this.optimizeForRoas = json.optimizeForRoas || false;

        return this;
      }
    };

    return CampaignInfo;
  });
