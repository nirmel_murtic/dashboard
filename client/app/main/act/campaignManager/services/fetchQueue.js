'use strict';

angular.module('act')
  .service('fetchQueue', function($q, $timeout) {

    function CampaignRemovalBuffer() {
      this._buffer = Object.create(null);
    }
    CampaignRemovalBuffer.prototype = {
      empty : function() {
        for(var key in this._buffer) {
          delete this._buffer[key];
        }
      },
      size : function() {
        return _.size(this._buffer);
      },
      add : function(id) {
        this._buffer[id] = true; 
      },
      remove : function(id) {
        delete this._buffer[id];
      },
      isEmpty : function() {
        return _.size(this._buffer) === 0;
      }
    }; 

    function FetchQueue() {
      this.pendingBuf = new CampaignRemovalBuffer(); 
      this.readyBuf = new CampaignRemovalBuffer();
      this.dfd = $q.defer();
      this.nextFrame = null;
      this.collectAsync = this.collectAsync.bind(this);
    }

    FetchQueue.prototype = {
      interval : 500,
      collect : function() {
        var n = this.readyBuf.size();
        this.readyBuf.empty();
        return n;
      },
      collectAsync : function() {
        var dfd;
        console.log('collect async');
        if(this.pendingBuf.isEmpty() && this.readyBuf.isEmpty()) {
          console.log('Nothing to do.');
          dfd = this.dfd;
          // rejection indicate this no work to be done
          this.dfd.reject(); 
          this.dfd = $q.defer();
          this.nextFrame = null;
          return dfd.promise;
        }
        if(this.pendingBuf.isEmpty()) {
          console.log('Ready to commit to fetching.');
          dfd = this.dfd;
          // resolve to how many items have been deleted
          // fetching job count can be calculated based
          // on this number
          this.dfd.resolve(this.collect()); 
          this.dfd = $q.defer();
          this.nextFrame = null;
          return dfd.promise;
        }
        console.log('Will check again in 0.5s.');
        var self = this;
        this.nextFrame = this.nextFrame || $timeout(angular.noop, this.interval).then(function() {
          self.nextFrame = null;
          return self.collectAsync();
        }); 
        return this.nextFrame;
      }
    };

    return {
      queue : function() {
        return new FetchQueue();
      }
    };
  
  });

