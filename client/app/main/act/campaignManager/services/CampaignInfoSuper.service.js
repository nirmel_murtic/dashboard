'use strict';

angular.module('act')
  .service('CampaignInfoSuper', function(CampaignInfo, fsUtils, CampaignManagerChannels) {
    function CampaignInfoSuper(info) {
      this.fromJSON(info);    
    }
    fsUtils.inherits(CampaignInfoSuper, CampaignInfo);
    CampaignInfoSuper.prototype.defaults = _.assign({
      type : 'super',
      subCampaignCounts : null,
      timeSeriesStats : null,
      channels : [
        {
          id : 'facebook',
          label : 'label.act.Facebook'
        },
        {
          id : 'super'
        }
      ],
      parentCampaignIds : null,
      budgetPolicy: null,
      budgetPerChannel: null
    }, CampaignInfo.prototype.defaults);

    CampaignInfoSuper.prototype.fromJSON = function(json) {
      CampaignInfo.prototype.fromJSON.call(this, json);
      if(json.numberSubcampaigns > 0) {
        this.subCampaignCounts = json.numberSubcampaigns;
      }

      var orderedChannels = {
        'facebook' : 0,
        'twitter' : 1,
        'adwords' : 2
      };

      if (json.listChannels) {
        this.channels = _(json.listChannels)
          .map(function(channelString) {
            var id = channelString.toLowerCase();
            return CampaignManagerChannels[id];
          })
          .sortBy(function(channel){
            return orderedChannels[channel.id];
          })
          .value();
      } else {
        this.channels = this.defaults.channels;
      }

      this.parentCampaignIds = _.map(json.listChannelStats, function(s) {
        return s.campaignId;
      });

      this.budgetPolicy = json.budgetPolicy;
      this.budgetPerChannel = _.reduce(json.channelBudgetBreakdown || [], function(acc, d) {
        acc[d.channel.toLowerCase()] = d.budget;
        return acc;
      }, {});

      this.cpaLimits = _(json.channelBudgetBreakdown)
        .map(function(d) {
        // key, value pairs
          return [d.channel.toLowerCase(), d.cpaLimit || NaN];
        })
        .zipObject()
        .value();

      this.hasCpaLimits = _.any(this.cpaLimits, function(val) { return val && val>0; });

      return this;
    };

    return CampaignInfoSuper;

  });
