'use strict';

angular.module('act')
  .service('CampaignInfoSubCampaign', function(CampaignInfo, fsUtils) {
    var inheritedFields = ['name', 'goal', 'startDate', 'endDate', 'optimizeForRoas'];
    function CampaignInfoSubCampaign(superCmp, additional) {
      _.each(inheritedFields, function(key) {
        this[key] = superCmp[key];
      }, this);
      _.assign(this, additional || {});
      this.type = 'subcampaign';
    }
    fsUtils.inherits(CampaignInfoSubCampaign, CampaignInfo);
    CampaignInfoSubCampaign.prototype.defaults = _.assign({
      type : 'subcampaign',
    }, CampaignInfo.prototype.defaults);
    // Sub campaigns are managed by optimization, therefore a user cannot pause/stop them
    CampaignInfoSubCampaign.prototype.isPausable = function() {
      return false;
    };
    CampaignInfoSubCampaign.prototype.isStoppable = function() {
      return false;
    };

    return CampaignInfoSubCampaign;

  });
