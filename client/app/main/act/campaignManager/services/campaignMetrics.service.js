'use strict';

angular.module('act')
  .provider('campaignMetricsView', function() {
  
    function MetricsView(campaign, CampaignManagerStatTypes) {
      this.current = CampaignManagerStatTypes[0].id;   
      //  # main store for view stats in visulizations
      //  {
      //    impressions : {
      //      cumulative : true,
      //      normalize : false,
      //      cumulativeField : 'cumimpressions',
      //      isDerived : false
      //    },
      //    ...
      //  }
      //
      this.campaign = campaign;
      this.visModes = _(CampaignManagerStatTypes)
        // filter out optional stat types (ROAS and value)
        .filter(function(x) { return x.onCondition ? x.onCondition(campaign) : true; })
        // init view states values
        .map(function(x) { return [x.id, { cumulative: true, normalize: false, isDerived: x.derived, cumulativeField: x.cumid }]; })
        // convert array of [key, value] pairs into object
        .zipObject()
        // compute
        .value();
    }
    MetricsView.prototype = {
      // metricName :: String
      select : function(metricName) {
        // already selected
        if(this.current === metricName) {
          return false;
        }
        // weird metricName, not something we can support, do nothing
        if(!(metricName in this.visModes)) {
          return false;
        }
        this.current = metricName;
      },
      // return the string for data field modes (fallback to metricName)
      metricDataField : function(metricName) {
        if(this.visModes[metricName].cumulative) {
          return this.visModes[metricName].cumulativeField || metricName;
        } 
        return metricName;
      },
      getChartData : function(metricName) {
        if(!this.campaign.tsCampaignStats) {
          return [];
        }
        return this.campaign.tsCampaignStats[this.metricDataField(metricName)];
      },
      // actionCreator is fsVisulization service for chart-stacked
      adjustStackMode : function(metricName, actionCreator) {
        var mode = this.visModes[metricName];
        if(!mode) {
          return;
        }
        // derived metrics do not support stack modes
        if(mode.isDerived) {
          return; 
        }
        if(mode.cumulative) {
          if(mode.normalize) {
            // normalize stack mode
            actionCreator.normalizeStack();
          } else {
            // zero basis stack mode
            actionCreator.zeroStack();
          }
        } else {
          if(mode.normalize) {
            // normalize stack mode
            actionCreator.normalizeStack();
          } else {
            // clear stack or overlay mode
            actionCreator.clearStack();
          }
        }
      },
      isCumulative : function(metricName) {
        return this.visModes[metricName].cumulative;
      },
      isNormalized : function(metricName) {
        return this.visModes[metricName].normalize;
      },
      toggleCumulative : function(cumulative, actionCreator) {
        var metricName = this.current;
        if(cumulative === this.isCumulative(metricName)) {
          return;
        }
        this.visModes[metricName].cumulative = !!cumulative;
        this.adjustStackMode(metricName, actionCreator);
      },
      toggleNormalize : function(norm, actionCreator) {
        var metricName = this.current;
        if(this.isNormalizeDisabled(metricName)) {
          return;
        }
        if(norm === this.isNormalized(metricName)) {
          return;
        }
        this.visModes[metricName].normalize = !!norm;
        this.adjustStackMode(metricName, actionCreator);
      },
      isNormalizeDisabled : function(metricName) {
        // normalize is disabled for campaigns with only one channel
        if(this.campaign.channels.length < 2) {
          return true;
        }
        return this.visModes[metricName].isDerived;
      }
    };

    var metricsView = null;

    this.construct = ['campaign', 'CampaignManagerStatTypes', function(campaign, StatTypes) {
      if(__DEV__) {
        console.log('Init View with campaign: ', campaign);
      }
      metricsView = new MetricsView(campaign, StatTypes);
    }];

    this.$get = ['CampaignManagerStatTypes', function(StatTypes) {
      return {
        getView : function(cmp) {
          if(cmp && metricsView && metricsView.campaign !== cmp) {
            metricsView = new MetricsView(cmp, StatTypes);
            return metricsView;
          } 
          return metricsView;
        }
      };
    }];

  });
