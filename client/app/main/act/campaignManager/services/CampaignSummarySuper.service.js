'use strict';

angular.module('act')
  .service('CampaignSummarySuper', function(CampaignInfoSuper, CampaignStatsMixin, fsUtils) {
    function CampaignSummarySuper(infoJson) {
      this.fromJSON(infoJson); 
      this.extractSummaryStats(infoJson);
    }
    fsUtils.inherits(CampaignSummarySuper, CampaignInfoSuper);
    _.assign(CampaignSummarySuper.prototype, CampaignStatsMixin);

    return CampaignSummarySuper;
  });
