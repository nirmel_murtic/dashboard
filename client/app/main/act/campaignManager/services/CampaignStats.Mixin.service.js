'use strict';

angular.module('act')
  .factory('CampaignStatsMixin', function(fsUtils, CampaignManagerStatTypes) {
    // jshint latedef:false, camelcase: false
    return {
      // -- newtype ChannelDatum a = { facebook :: a, adwords :: a, date :: Date }
      // -- ChannelDatum is a functor
      // -- applyOnAllChannels :: (a -> a -> ... -> <Any>) -> (ChannelDatum a -> ChannelDatum a -> ... -> ChannelDatum <Any>)
      // -- applyOnAllChannels is `fmap` for functor, it could be considered as `mapChannelDatum`

      /**
       * applyOnAllChannels
       *
       * Transform a function into another that takes channel datums as input
       *
       * a channel datum is an object structured as:
       * {
       *   facebook : 10,
       *   adwords: 11
       * }
       *
       * @example
       * // we have function sum that takes two numbers and add them up
       * function sum(a, b) {
       *  return a + b;
       * }
       * // applyOnAllChannels lift this function to one that takes datums
       *
       * var sumByChannel = applyOnAllChannels(sum);
       *
       * sumByChannel({ 
       *  facebook: 12,
       *  adwords: 11
       * }, {
       *  facebook: 13,
       *  adwords: 14
       * })
       *
       * // { facebook: 25, adwords: 25}
       *
       *
       * @param fn
       * @return {function} 
       */

      applyOnAllChannels : function(fn) {
        var channels = [{ id: 'super' }].concat(this.channels);

        var self = this;
        return function() {
          var datums = _.toArray(arguments);
          var date = datums.length ? datums[0].date : null;
          var d;
          if(date) {
            d = { date: date };
          } else {
            d = {};
          }
          return _.reduce(channels, function(d, channel) {
            var id = channel.id; // is 'facebook' or 'adwords'
            var argValues = datums.map(function(d) {
              return d[id];
            });
            d[id] = 
              fn.apply(self, argValues);
            return d;
          }, d);
        };
      },
      /**
       * extractTsStatSubCmp
       * 
       * if campaign is subcampaign use this
       *
       * @example
       * // example output
       * [
       *   {
       *     date : <Date>,
       *     channel : 'facebook',
       *     parentCampaignId : 11,
       *     subCampaignId : 1,
       *     impressions : 10,
       *     ...
       *     cpa : 1
       *   }
       * ]
       *
       * @param type
       * @param statsJson
       * @return {[ChannelDatum]}
       */
      extractTsStatSubCmp : function(subCmpId, statsJson) {
        if(this.channels.length !== 1 || this.type !== 'subcampaign') {
          throw 'This is not a subcampaign (ad).';
        }
        this.__cache_aggregate__ = null;
        var channel = this.channels[0].id;
        var subCampaignTsStats = _.map(statsJson, function(valueAtTime, timestamp) {
          var parent = _.find(valueAtTime.parentCampaignStats, { channel : channel });
          var childStats = parent ? _.find(parent.subcampaignStats, { campaignId: subCmpId }) : null;
          var stats, deltaStats;
          if(childStats) {
            deltaStats = childStats.deltaStats;
            stats = childStats.stats;
          } else {
            stats = {
              spent : 0,
              impressions : 0,
              conversions : 0,
              clicks : 0,
              value : 0
            };
            deltaStats = {
              spent : 0,
              impressions : 0,
              conversions : 0,
              value : 0,
              clicks : 0
            };
          }
          _.assign(deltaStats, {
            date : new Date( +timestamp),
            channel : channel,
            parentCampaignId : parent ? parent.parentCampaignId : -1,
            subCampaignId : subCmpId
          });
          deltaStats.spent = deltaStats.spend;
          deltaStats.cumspent = stats.spend;
          deltaStats.cumconvs = stats.conversions;
          deltaStats.cumimpressions = stats.impressions;
          deltaStats.cumclicks = stats.clicks;

          deltaStats.value = deltaStats.totalActionValueDecimal || 0;
          deltaStats.cumvalue = stats.totalActionValueDecimal || 0;

          deltaStats.ctr = computeCtr(deltaStats.clicks, deltaStats.impressions);
          deltaStats.cvr = computeCtr(deltaStats.conversions, deltaStats.clicks);
          deltaStats.cpc = computeCtr(deltaStats.spent, deltaStats.clicks );
          deltaStats.cpm = computeCtr(deltaStats.spent, deltaStats.impressions);
          deltaStats.cpa = computeCtr(deltaStats.spent, deltaStats.conversions);


          deltaStats.cumctr = computeCtr(deltaStats.cumclicks, deltaStats.cumimpressions);
          deltaStats.cumcvr = computeCtr(deltaStats.cumconvs, deltaStats.cumclicks);
          deltaStats.cumcpc = computeCtr(deltaStats.cumspent, deltaStats.cumclicks );
          deltaStats.cumcpm = computeCtr(deltaStats.cumspent, deltaStats.cumimpressions);
          deltaStats.cumcpa = computeCtr(deltaStats.cumspent, deltaStats.cumconvs);
 
          deltaStats.roas = computeRoas(deltaStats.spent, deltaStats.value);
          deltaStats.cumroas = computeRoas(deltaStats.cumspent, deltaStats.cumvalue);

          return deltaStats;
        });
        subCampaignTsStats = _.sortBy(subCampaignTsStats, 'date');


        this.tsCampaignStats = this.tsCampaignStats || {};
        this.tsDateRange = d3.extent(subCampaignTsStats, function(d) {
          return d.date;
        });

        function mapMetric(metric) {
          return _.map(subCampaignTsStats, function(datum) {
            var a = {};
            a[channel] = datum[metric];
            a.date = datum.date;
            return a;
          });
        }

        var self = this;
        var metrics = _(CampaignManagerStatTypes)
          .filter(function(x) { return x.onCondition ? x.onCondition(self) : true; })
          .map(function(x) { return [x.id, x.cumid]; })
          .flatten()
          .value();

        _.each(metrics, function(m) {
          self.tsCampaignStats[m] = mapMetric(m);
        });

      },
      /**
       * extractStat
       *
       * extract one raw multi-channel stats (impressions, clicks, etc.)
       * from backend response json
       *
       * @example
       * // example output:
       * // [
       * //   {
       * //     facebook: 12,
       * //     adwords: 13,
       * //     super : 25,
       * //     date : <Date>
       * //   },
       * //   ...
       * // ]
       *
       * @param type
       * @param statsJson
       * @return {[ChannelDatum]}
       */
      extractTsStat : function(type, statsJson, mode) {
        var channels = this.channels.concat([{ id: 'super' }]);
        var statProp = mode === 'cumulative' ? 'stats' : 'deltaStats';
        return _.map(statsJson, function(valueAtTime, timestamp) {
          var d = {
            date : new Date( +timestamp )
          };
          return _.reduce(channels, function(d, channel) {
            var id = channel.id; // is 'facebook' or 'adwords' or 'super'
            var parentStatDatum;
            if(id === 'super') {
              d[id] = valueAtTime[statProp][type];
            } else {
              parentStatDatum = _.find(valueAtTime.parentCampaignStats, { 'channel' : id }); 
              if(parentStatDatum) {
                d[id] = parentStatDatum[statProp][type];
              } else {
                d[id] = 0;
              }
            }
            return d;
          }, d);
        });
      },
      /**
       * extractStats
       *
       * extract raw stats from backend json, and computed derived stats
       *
       * @param {object} - json
       * @return {undefined}
       */
      extractTsStats : function (json) {
        json = fsUtils.sortObject(json);

        var dateRange = d3.extent(_.keys(json), Number).map(function(d) {
          return new Date(d);
        }); 

        var impressions = this.extractTsStat('impressions', json);
        var conversions = this.extractTsStat('conversions', json);
        var spent = this.extractTsStat('spend', json);
        var clicks = this.extractTsStat('clicks', json);
        var values;
        if(this.optimizeForRoas) {
          values = this.extractTsStat('totalActionValueDecimal', json);
        }

        var cumConversions = fixMissing( this.extractTsStat('conversions', json, 'cumulative'), this );
        var cumSpent = fixMissing( this.extractTsStat('spend', json, 'cumulative'), this );
        var cumClicks = fixMissing( this.extractTsStat('clicks', json, 'cumulative'), this );
        var cumImpressions = fixMissing( this.extractTsStat('impressions', json, 'cumulative'), this );
        var cumValues;
        if(this.optimizeForRoas) {
          cumValues = fixMissing( this.extractTsStat('totalActionValueDecimal', json, 'cumulative'), this );
        }

        var ctr = fsUtils.zipWith(clicks, impressions, this.applyOnAllChannels(computeCtr));
        var cvr = fsUtils.zipWith(conversions, clicks, this.applyOnAllChannels(computeCvr));
        var cpc = fsUtils.zipWith(spent, clicks, this.applyOnAllChannels(computeCpc));
        var cpa = fsUtils.zipWith(spent, conversions, this.applyOnAllChannels(computeCpa));
        var cpm = fsUtils.zipWith(spent, impressions, this.applyOnAllChannels(computeCpm));
        // cumulative stats
        var cumctr = fsUtils.zipWith(cumClicks, cumImpressions, this.applyOnAllChannels(computeCtr));
        var cumcvr = fsUtils.zipWith(cumConversions, cumClicks, this.applyOnAllChannels(computeCvr));
        var cumcpc = fsUtils.zipWith(cumSpent, cumClicks, this.applyOnAllChannels(computeCpc));
        var cumcpa = fsUtils.zipWith(cumSpent, cumConversions, this.applyOnAllChannels(computeCpa));
        var cumcpm = fsUtils.zipWith(cumSpent, cumImpressions, this.applyOnAllChannels(computeCpm));
        // roas specific
        var roas, cumRoas;
        if(this.optimizeForRoas) {
          roas = fsUtils.zipWith(spent, values, this.applyOnAllChannels(computeRoas));
          cumRoas = fsUtils.zipWith(cumSpent, cumValues, this.applyOnAllChannels(computeRoas));
        }

        this.__cache_aggregate__ = null;
        this.tsDateRange = dateRange;
        this.tsCampaignStats = {
          impressions : impressions,
          cumimpressions : cumImpressions,
          clicks : clicks,
          cumclicks : cumClicks,
          cumconvs : cumConversions,
          conversions : conversions,
          spent : spent,
          cumspent : cumSpent,
          ctr : ctr,
          cumctr : cumctr,
          cvr : cvr,
          cumcvr : cumcvr,
          cpc : cpc,
          cumcpc : cumcpc,
          cpm : cpm,
          cumcpm : cumcpm,
          cpa : cpa,
          cumcpa : cumcpa
        };
        if(this.optimizeForRoas) {
          _.assign(this.tsCampaignStats, {
            value : values,
            cumvalue : cumValues,
            roas : roas,
            cumroas : cumRoas
          });
        }
      },
      extractSummaryStats : function(json) {
        this.__cache_aggregate__ = null;

        this.maxBid = _(this.channels)
          .map(function(chan) { return [chan.id, NaN]; })
          .zipObject()
          .value();

        if(this.status === 'draft' || this.status === 'failed' || this.status === 'pending') {
          this.campaignStats = this.getEmptyStats();
          return;
        }
        if(!json.listChannelStats || !json.listChannelStats.length) {
          return;
        }

        this.campaignStats = {};

        _.each(json.listChannelStats, function(channelStat) {

          if(channelStat.maxBid > 0) {
            this.maxBid[channelStat.channelName.toLowerCase()] = channelStat.maxBid;
          } else {
            this.maxBid[channelStat.channelName.toLowerCase()] = null;
          }

          this.campaignStats.impressions = this.campaignStats.impressions || [{}];
          this.campaignStats.impressions[0][channelStat.channelName.toLowerCase()] = channelStat.impressions;

          this.campaignStats.clicks = this.campaignStats.clicks || [{}];
          this.campaignStats.clicks[0][channelStat.channelName.toLowerCase()] = channelStat.clicks;

          this.campaignStats.spent = this.campaignStats.spent || [{}];
          this.campaignStats.spent[0][channelStat.channelName.toLowerCase()] = channelStat.spent;

          this.campaignStats.conversions = this.campaignStats.conversions || [{}];
          this.campaignStats.conversions[0][channelStat.channelName.toLowerCase()] = channelStat.conversions;

          if(this.optimizeForRoas) {
            this.campaignStats.value = this.campaignStats.value || [{}];
            this.campaignStats.value[0][channelStat.channelName.toLowerCase()] = channelStat.totalActionValue;
          }


        }, this);

        _.each(this.campaignStats, function(statArr) {
          var stat = statArr[0];
          stat.super = _.reduce(stat, function(acc, val) {
            return acc + val;
          }, 0);
        });

        this.campaignStats.ctr = fsUtils.zipWith(this.campaignStats.clicks, this.campaignStats.impressions, this.applyOnAllChannels(computeCtr));
        this.campaignStats.cvr = fsUtils.zipWith(this.campaignStats.conversions, this.campaignStats.clicks, this.applyOnAllChannels(computeCvr));
        this.campaignStats.cpc = fsUtils.zipWith(this.campaignStats.spent, this.campaignStats.clicks, this.applyOnAllChannels(computeCpc));
        this.campaignStats.cpa = fsUtils.zipWith(this.campaignStats.spent, this.campaignStats.conversions, this.applyOnAllChannels(computeCpa));
        this.campaignStats.cpm = fsUtils.zipWith(this.campaignStats.spent, this.campaignStats.impressions, this.applyOnAllChannels(computeCpm));
        if(this.optimizeForRoas) {
          this.campaignStats.roas = fsUtils.zipWith(this.campaignStats.spent, this.campaignStats.value, this.applyOnAllChannels(computeRoas));
        }

      },
      getLifetimeAggregateStats : function() {
        var stats = this.campaignStats;
        if(!stats) {
          return this.getEmptyAggregate();
        }
        var sumByChannel = this.applyOnAllChannels(sum);

        var impressions = _.reduce(stats.impressions, sumByChannel, {});
        var clicks = _.reduce(stats.clicks, sumByChannel, {});
        var conversions = _.reduce(stats.conversions, sumByChannel, {});
        var spent = _.reduce(stats.spent, sumByChannel, {});
        var values = _.reduce(stats.value, sumByChannel, {});

        var ctr = this.applyOnAllChannels(computeCtr)(clicks, impressions); 
        var cvr = this.applyOnAllChannels(computeCvr)(conversions, clicks); 
        var cpc = this.applyOnAllChannels(computeCpc)(spent, clicks); 
        var cpm = this.applyOnAllChannels(computeCpm)(spent, impressions); 
        var cpa = this.applyOnAllChannels(computeCpa)(spent, conversions); 
        var roas = this.applyOnAllChannels(computeRoas)(spent, values); 

        var aggr = {
          impressions : impressions,
          clicks : clicks,
          conversions : conversions,
          value : values,
          spent : spent,
          cvr : cvr,
          ctr : ctr,
          cpc : cpc,
          cpm : cpm,
          cpa : cpa,
          roas : roas
        };

        return aggr;
      },
      getAggregateStats : function () {
        var cached = this.__cache_aggregate__;
        var stats = this.tsCampaignStats || this.campaignStats;
        if(cached) {
          return cached;
        }
        if(!stats || !stats.impressions.length) {
          return this.getEmptyAggregate();
        }
        var sumByChannel = this.applyOnAllChannels(sum);
        var subtractByChannel = this.applyOnAllChannels(subtract);

        var impressions, clicks, conversions, spent, values;
        if(this.tsCampaignStats) {
          impressions = subtractByChannel(_.last(stats.cumimpressions), _.first(stats.cumimpressions));
          impressions = sumByChannel(impressions, _.first(stats.impressions));
          clicks = subtractByChannel(_.last(stats.cumclicks), _.first(stats.cumclicks));
          clicks = sumByChannel(clicks, _.first(stats.clicks));
          spent = subtractByChannel(_.last(stats.cumspent), _.first(stats.cumspent));
          spent = sumByChannel(spent, _.first(stats.spent));
          if(this.optimizeForRoas) {
            values = subtractByChannel(_.last(stats.cumvalue), _.first(stats.cumvalue));
            values = sumByChannel(values, _.first(stats.value));
          }
          conversions = subtractByChannel(_.last(stats.cumconvs), _.first(stats.cumconvs));
          conversions = sumByChannel(conversions, _.first(stats.conversions));
        } else {
          impressions = _.reduce(stats.impressions, sumByChannel, {});
          clicks = _.reduce(stats.clicks, sumByChannel, {});
          conversions = _.reduce(stats.conversions, sumByChannel, {});
          spent = _.reduce(stats.spent, sumByChannel, {});
          if(this.optimizeForRoas) {
            values = _.reduce(stats.value, sumByChannel, {});
          }
        }

        var ctr = this.applyOnAllChannels(computeCtr)(clicks, impressions); 
        var cvr = this.applyOnAllChannels(computeCvr)(conversions, clicks); 
        var cpc = this.applyOnAllChannels(computeCpc)(spent, clicks); 
        var cpm = this.applyOnAllChannels(computeCpm)(spent, impressions); 
        var cpa = this.applyOnAllChannels(computeCpa)(spent, conversions); 
        var roas;
        if(this.optimizeForRoas) {
          roas = this.applyOnAllChannels(computeRoas)(spent, values); 
        }

        var aggr = {
          impressions : impressions,
          clicks : clicks,
          conversions : conversions,
          spent : spent,
          value : values,
          cvr : cvr,
          ctr : ctr,
          cpc : cpc,
          cpm : cpm,
          roas : roas,
          cpa : cpa
        };

        this.__cache_aggregate__ = aggr;
        return aggr;
      },
      getEmptyStats : function() {
        var getNaN = this.applyOnAllChannels(justNaN);  
        var getZeros = this.applyOnAllChannels(justZero);  
        var NaNs = [getNaN()];
        var zeros = [getZeros()];
        return {
          impressions : zeros,
          clicks : zeros,
          conversions : zeros,
          spent : zeros,
          value : zeros,
          cvr : NaNs,
          ctr : NaNs,
          cpc : NaNs,
          cpm : NaNs,
          roas : NaNs,
          cpa : NaNs,
        };
      },
      getEmptyAggregate : function() {
        var getNaN = this.applyOnAllChannels(justNaN);  
        var getZeros = this.applyOnAllChannels(justZero);  
        var NaNs = getNaN();
        var zeros = getZeros();
        return {
          impressions : zeros,
          clicks : zeros,
          conversions : zeros,
          spent : zeros,
          value : zeros,
          cvr : NaNs,
          ctr : NaNs,
          cpc : NaNs,
          cpm : NaNs,
          roas : NaNs,
          cpa : NaNs,
        };
      }
    }; 
      
    // formulas to computed derived stats
    function computeCtr(clicks, impressions) {
      return clicks / impressions; // it can be NaN
    }
    function computeCvr(conversions, clicks) {
      return conversions / clicks;
    }
    function computeCpm(spent, impressions) {
      return spent / impressions * 1000;
    }
    function computeCpc(spent, clicks) {
      return spent / clicks;
    }
    function computeCpa(spent, conversions) {
      return spent / conversions;
    }
    function computeRoas(spent, value) {
      return value / spent;
    }


    function sum(acc, input) {
      return (acc || 0) + (input || 0);
    }

    function subtract(a, b) {
      if(a > 0) {
        return a - (b || 0);
      }
      return 0;
    }

    function justNaN() {
      return NaN;
    }
    function justZero() {
      return 0;
    }
    function fixMissing(series, cmpInst) {
      var state = {
        lastValid : null
      };    
      state.lastValid = _(cmpInst.channels).filter(function(c) {
        return c.id !== 'super';
      }).map(function(c) {
        return [c.id, 0];
      }).zipObject().value();
      state = _.reduce(series, function(s, d) {
        var x, total = 0; 
        for(var chan in cmpInst.channels) {
          x = d[chan.id];
          if(isNaN(x) || _.isUndefined(x)) {
            d[chan.id] = s.lastValid[chan.id];
          } else {
            s.lastValid[chan.id] = x;
          }
          total += d[chan.id];
        }
        // d.super = total;
        return s;
      }, state);
      return series;
    }

  });
