'use strict';

angular.module('act')
  .factory('CampaignmanagerWebApiUtils',
  function ($q, $http, $filter, $rootScope, $state, $cacheFactory, apiUrl, CampaignSummarySuper, CampaignInfoSuper, CampaignInfo, CampaignmanagerTopLevelFilters, fsDownloadFile, EventLog, identityConverter, CampaignStatus, $auth) {
    /* jshint unused:false, latedef:false */
    return {
      checkCampaignCategory: function (superId) {
        var url = apiUrl + '/api/v2/campaign/get/super/1234?superId=' + superId;
        return $http.get(url)
          .then(function (res) {
            try {
              var data = res.data.data;
              var status = data.campaignStatus.toLowerCase();
              if (status === 'deleted' || status === 'completed') {
                return CampaignmanagerTopLevelFilters.PAST.key;
              }
              var now = Date.now();
              var startDate = +data.startDate;
              var endDate = +data.endDate;
              if (startDate > now) {
                return CampaignmanagerTopLevelFilters.SCHEDULED.key;
              }
              if (endDate < now) {
                return CampaignmanagerTopLevelFilters.PAST.key;
              }
              return CampaignmanagerTopLevelFilters.ACTIVE.key;
            } catch (err) {
              return $q.reject({
                errorState: 'main.act.campaignManager.error',
                errorParams: {
                  msg: 'Invalid Campaign ID: ' + superId,
                  stack: err.stack
                }
              });
            }

          }, function () {
            return CampaignmanagerTopLevelFilters.ACTIVE.key;
          });
      },
      getCampaignSummaryById: function (teamId, superId) {
        // var url = 'app/main/act/campaignManager/dummyData/campaignList.json';
        var url = apiUrl + '/api/v2/campaign/stats/manager/get/listSuperCampaign';
        return $http.get(url, {
          params: {
            superCmpId: +superId,
            teamId: teamId,
            fields: 'name,description,goals,attachments,teamId,createdAt,page,pageId,pagePageId,image,socialAccount,socialAccountId,goal,goalId,pageName,appId,host,event,eventUrl,application,name,mobileGoalInfo,appName,applicationId,type,removable,tracker,thirdPartyPixel,name,displayName,placementTypes,name,fullGuid,width,height,size,extension,twitterPromotableUser,twitterPromotableUserId,userId,screenName'
          },
          responseType: 'json'
        }).then(function (res) {
          var ret;
          try {
            var campaignsJSON = res.data.data.data; // muhaha
            if (campaignsJSON && campaignsJSON.length) {
              campaignsJSON = identityConverter.convert(campaignsJSON, '@fsid');
            }
            ret = _.map(campaignsJSON, function (json) {
              var cmp = new CampaignSummarySuper(json);
              return cmp;
            });
          } catch (err) {
            ret = $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing campaigns data.',
                stack: err.stack
              }
            });
          }
          return ret;
        }, function () {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading your campaign:' + superId + ' from server.'
            }
          });
        });
      },
      pokeCampaignSummaryList: function (teamId, filter, projectId, offset) {
        // var url = 'app/main/act/campaignManager/dummyData/campaignList.json';
        var url = apiUrl + '/api/v2/campaign/stats/manager/get/listSuperCampaign';
        return $http.get(url, {
          params: {
            filter: filter || '',
            teamId: teamId,
            projectId : +projectId,
            limit : 1,
            offset : offset || 0,
          },
          cache: true,
          ignoreLoadingBar: true,
          responseType: 'json'
        }).then(function (res) {
          var ret;
          try {
            var campaignsJSON = res.data.data.data; // muhaha
            if(campaignsJSON.length) {
              return true;
            } else {
              return $q.reject(false);
            }
          } catch (err) {
            return $q.reject(false);
          }
          return ret;
        }, function () {
          return $q.reject(false);
        });
      },
      getCampaignSummaryList: function (teamId, filter, projectId, limit, offset, sort, sortColumn) {
        // var url = 'app/main/act/campaignManager/dummyData/campaignList.json';
        var url = apiUrl + '/api/v2/campaign/stats/manager/get/listSuperCampaign';
        return $http.get(url, {
          params: {
            filter: filter || '',
            teamId: teamId,
            projectId : +projectId,
            limit : limit || 22, // TO-DO: not hard code this,
            sort : sort || 'desc',
            sortColumn : sortColumn || 'startTime',
            offset : offset || 0,
            fields: 'name,description,goals,attachments,teamId,createdAt,page,pageId,pagePageId,image,socialAccount,socialAccountId,goal,goalId,pageName,appId,host,event,eventUrl,application,name,mobileGoalInfo,appName,applicationId,type,removable,tracker,thirdPartyPixel,name,displayName,placementTypes,name,fullGuid,width,height,size,extension,twitterPromotableUser,twitterPromotableUserId,userId,screenName'
          },
          responseType: 'json'
        }).then(function (res) {
          var ret;
          try {
            var campaignsJSON = res.data.data.data; // muhaha
            if (campaignsJSON && campaignsJSON.length) {
              campaignsJSON = identityConverter.convert(campaignsJSON, '@fsid');
            }
            ret = _.map(campaignsJSON, function (json) {
              return new CampaignSummarySuper(json);
            });
          } catch (err) {
            ret = $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing campaigns data.',
                stack: err.stack
              }
            });
          }
          return ret;
        }, function () {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading your campaigns from server.'
            }
          });
        });
      },
      performCampaignAction: function (channel, action, ids) {
        /* jshint camelcase:false */
        var superId = ids.superCampaignId;
        var parentId = ids.parentCampaignId;
        if (channel === 'adwords') {
          channel = 'google';
        }
        if (_.isUndefined(channel) || _.isUndefined(superId) || _.isUndefined(parentId)) {
          return $q.reject(null);
        }
        var actionToStatus = {
          'paused': 'PAUSE',
          'active': 'START',
          'stopped': 'DELETE',
          'delete': 'DELETE'
        };
        var url = apiUrl + '/api/v2/campaign/set/parent/status/' + channel + '/' + parentId;
        return $http.get(url, {
          params: {
            campaign_status: actionToStatus[action],
            superCmpId: superId
          }
        });
      },
      getGoalsForUser: function (userId) {
        return $http({
          url: apiUrl + '/api/v2/campaign/get/all/goals/' + userId,
          method: 'GET'
        }).then(function (res) {
          return res.data.data.fbGoals;
        }, function (err) {
          return new Error('Error Getting goals');
        });
      },
      getTimeseriesStats: function (superCampaignSummary, params) {
        if (!params) {
          var interval = 'hour', len;
          len = Math.min(Date.now(), superCampaignSummary.endDate) - superCampaignSummary.startDate;

          if (len > 24 * 3600000 * 15) {
            interval = 'day';
          }
          params = {
            interval: interval
          };
        }

        var url = apiUrl + '/api/v2/campaign/stats/get/' + superCampaignSummary.superId + '/' + superCampaignSummary.parentCampaignIds.join(',');
        // var url = 'app/main/act/campaignManager/dummyData/campaign.stats.v2.json';
        return $http.get(url, {
          responseType: 'json',
          params: params,
          cache: false
        }).then(function (res) {
          try {
            if (_.size(res.data.data)) {
              superCampaignSummary.$$rawStatsJSON = res.data.data;
              superCampaignSummary.extractTsStats(res.data.data);
            }
            return superCampaignSummary;
          } catch (e) {
            return $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing campaign timeseries data.',
                stack : e.stack
              }
            });
          }
        }, function (res) {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading Charts data'
            }
          });
        });
      },
      getSubcampaignSummary: function (superId) {
        var url = apiUrl + '/api/v2/campaign/stats/manager/get/listSubCampaign/' + superId;
        return $http({
          method: 'GET',
          // url : 'app/main/act/campaignManager/dummyData/subcampaignList.json',
          url: url
        }).then(function (res) {
          try {
            // TO-DO: make this a seperate service
            return _.map(res.data.data.data, function (d) {
              return {
                id : Number(d.hashId),
                subCampaignId : Number(d.subCampaignId),
                budget : Number(d.budget),
                channel : d.channel.toLowerCase(),
                status : (d.optStatus || d.subCampaignStatus || '').toLowerCase(),
                channelStatus : d.subCampaignStatus,
                impressions : d.impressions,
                clicks : d.clicks,
                conversions : d.conversions,
                spend : d.spent,
                value : d.totalActionValue,
                roas : d.totalActionValue / d.spent,
                bid : d.bidAmount,
                lowerBid : d.lowerBid,
                upperBid : d.upperBid,
                ctr : d.clicks / d.impressions || null,
                cvr : d.conversions / d.clicks || null,
                cpc : d.spent / d.clicks || null,
                cpm : d.spent / d.impressions / 1000 || null,
                cpa : d.spent / d.conversions || null
              };
            });
          } catch (e) {
            return $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing ad data, the backend api endpoint changed.'
              }
            });
          }
        }, function (res) {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading Ad data'
            }
          });
        });
      },
      getFbSubCampaignDetails: function (subCmpId) {
        var url = apiUrl + '/api/v2/campaign/stats/manager/get/fbSubCampaignDetail/' + subCmpId;
        return $http.get(url, {
          cache: true,
          responseType: 'json'
        }).then(function (res) {
          try {
            var adPreviews = res.data.data.data.listAdPreview;
            var audienceStr = res.data.data.data.audience;
            var audience = parseJSONDeep(audienceStr).audience;

            return {
              preview: adPreviews,
              audience: audience
            };
          } catch (err) {
            return $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing Ad\'s preview and audience.'
              }
            });
          }
        }, function () {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading Ad details.'
            }
          });
        });
      },
      getAwSubCampaignDetails: function (subCmpId) {
        var url = apiUrl + '/api/v2/campaign/stats/manager/get/awSubCampaignDetail/' + subCmpId;
        return $http.get(url, {
          cache: true,
          responseType: 'json'
        }).then(function (res) {
          try {
            var data = res.data.data.result;

            var creative = data.creatives;

            var audienceStr = data.targetingSpecs;
            var audience = parseJSONDeep(audienceStr).audience;

            var keywords = parseJSONDeep(data.keywordSpecs).KEYWORD;

            return {
              audience: audience,
              creative: creative,
              keywords: keywords
            };
          } catch (err) {
            return $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing ad preview and audience for adwords.'
              }
            });
          }
        }, function () {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading Ad Details.'
            }
          });
        });
      },
      getTwSubCampaignDetails: function (subCmpId) {
        var url = apiUrl + '/api/v2/campaign/get/twsubcmp/1234';
        return $http.get(url, {
          cache: true,
          params: {
            twsubcmpid: subCmpId,
            fields: 'id,acTargetingSpecs,post,tweetText,tweetIsCard,tweetHeadline,localImageGuid,tweetCallToAction,cardType,tweetCardName,tweetPreviewUrl'
          },
          responseType: 'json'
        }).then(function (res) {
          try {
            var data = res.data.data.data;

            var audience = parseJSONDeep(data.acTargetingSpecs).audience;
            var creative = {
              post: data.post,
              tweetText: data.tweetText,
              isCard: data.tweetIsCard,
              image: data.localImageGuid,
              tweetCardHeadline: data.tweetHeadline,
              tweetCallToAction: data.tweetCallToAction,
              tweetCardName: data.tweetCardName,
              tweetCardURL: null
            };

            return {
              audience: audience,
              creative: creative
            };
          } catch (err) {
            return $q.reject({
              errorState: 'main.act.campaignManager.error',
              errorParams: {
                msg: 'Error parsing ad\'s preview and audience for adwords.'
              }
            });
          }
        }, function () {
          return $q.reject({
            errorState: 'main.act.campaignManager.error',
            errorParams: {
              msg: 'Error Loading Ad Details.'
            }
          });
        });
      },
      setMaxBid: function (parentId, bid) {
        var url = apiUrl + '/api/v2/campaign/maxbid/' + parentId;
        var params;
        if (_.isNumber(bid) && bid > 0) {
          params = {
            isMaxBid: true,
            maxBid: bid
          };
        } else {
          params = {
            isMaxBid: false
          };
        }

        url = url + '?' + 'isMaxBid=' + params.isMaxBid +
          (params.isMaxBid ? '&maxBid=' + params.maxBid : '');

        return $http.post(url, {
          params: params
        }).then(_.constant(true), function () {
          return $q.reject(false);
        });
      },
      downloadCSV: function (superId, config, reportTypes) {
        reportTypes = _.isArray(reportTypes) ? reportTypes.map(function(t) { return t.toUpperCase(); }) : [reportTypes.toUpperCase()];
        var url = apiUrl + '/api/v2/campaignReport/csv/' + superId;
        var name = _.trim(config.reportName).replace(/(\s+|\s*-\s*)/g, '_');
        name = name.replace(/_+/g, '_');
        var params = {
          reportName: name,
          startTime: config.dateRange[0].getTime(),
          endTime: config.dateRange[1].getTime(),
          interval: config.interval,
          timeZoneOffset: new Date().getTimezoneOffset()
        };

        if (reportTypes.length > 1) {
          params.reportTypes = reportTypes.join(',');
        } else {
          params.reportType = reportTypes[0];
        }

        if (jstz) { // jshint ignore:line
          params.timeZoneName = jstz.determine().name(); // jshint ignore:line
        } // jshint ignore:line

        var token = $auth.retrieveData('auth_headers').Authorization.replace(/^Bearer\s+/, '');
        //jshint camelcase:false
        params.access_token = token;

        url = url + '?' + $.param(params);
        return $q.when(window.open(url, '_blank'));
        // return fsDownloadFile.downloadFromUrl(url, fileName);
      },
      hideDraft: function (superId) {
        var url = apiUrl + '/api/v2/campaign/hide/' + superId;
        return $http.get(url);
      },
      checkSuperCampaignEventLogEntries: function (superCmp, teamId) {
        var superId = superCmp.superId;
        var url = apiUrl + '/api/v2/eventlog/events/' + teamId;
        var params = {
          filterReferences: 'CAMPAIGN_ID:' + superId,
          filterPriorities: 'ALERT',
          filterEvents: 'CAMPAIGN_PARENT_STATUS_UPDATE,SUBCAMPAIGN_STATUS_UPDATE',
          size: 20,
          offset: 0
        };
        return $http.get(url, {
          params: params
        }).then(function (res) {
          try {
            var events;
            if (res.data.data && res.data.data.events && res.data.data.events.length) {
              events = EventLog.mapEventsForDisplay(res.data.data.events);
              superCmp.nativeChangeEvents = events;
            }
          } catch (err) {
          }
          return superCmp;
        }, function () {
          return superCmp;
        });
      },
      editCampaignBudgetAndDuration: function (superCmp, campaignInfo) {
        // Here we need to pass the json with the new budget (old budget + extension), not just the budget extension
        // We create a new object that will be passed to the backend. It only contains the information needed by the backend, nothing more.
        var campaignInfoFormatted = {};

        if (campaignInfo.newEndDate) {
          campaignInfoFormatted.newEndDate = campaignInfo.newEndDate;
        }

        if (superCmp.budgetPolicy === 'FLOATING') {
          campaignInfoFormatted.newBudget = campaignInfo.budgetIncrease + superCmp.budget;
        } else if (superCmp.budgetPolicy === 'FIXED') {
          campaignInfoFormatted.channels = {};
          for (var channel in campaignInfo.channels) {
            if (campaignInfo.channels.hasOwnProperty(channel)) {
              campaignInfoFormatted.channels[channel] = {
                newBudget: campaignInfo.channels[channel].budgetIncrease + superCmp.budgetPerChannel[channel]
              };
            }
          }
        }

        var campaignInfoJson = angular.toJson(campaignInfoFormatted);

        var params = {
          superCmpId: superCmp.superId,
          campaignJson: campaignInfoJson
        };

        return $http({
          url: apiUrl + '/api/v2/campaign/extendCampaign/',
          method: 'POST',
          data: $.param(params)
        });
      },
      getCampaignBudgetRecommendation: function (superId, campaignInfo) {

        var campaignInfoJson = angular.toJson(campaignInfo);
        return $http.get(apiUrl + '/api/v2/campaign/budgetRecommendation/' + '?superCmpId=' + superId + '&campaignJson=' + campaignInfoJson).
          then(function (response) {
            return response.data.data.result;
          });
      }
    };

    function parseJSONDeep(json) {
      if (_.isString(json)) {
        try {
          json = JSON.parse(json);
        } catch (err) {

        }
      }
      var key;
      if (_.isObject(json)) {
        for (key in json) {
          json[key] = parseJSONDeep(json[key]);
        }
      }
      return json;
    }

  });
