'use strict';

angular.module('act')
  .service('CampaignmanagerStatsUtils', function() {
 
    /**
     * cumsum  
     *
     * [Number] -> [Number]
     * cumulative sum
     *
     * @param {array} - xs
     * @return {array} - sums
     */
    function cumsum(xs) {
      var sums = []; 
      var sum = 0;
      for(var i=0, len=xs.length; i<len; i++) {
        sum = sum + (xs[i] || 0);
        sums.push(sum);
      }
      return sums;
    }

    function cumRatio(xs, ys) {
      var len = xs.length;
      var cumxs = cumsum(xs),
          cumys = cumsum(ys);

      cumxs['-1'] = 0;
      cumys['-1'] = 0;

      var output = [];

      for(var i=0; i<len; i++) {
        output.push(cumxs[i] / cumys[i]);
      }
      return output;
    
    }
    /**
     * expSmooth
     *
     * [Number] -> [Number] -> (Optional Number) -> [Number]
     *
     * Given two arrays of non-negative nubmers: xs and ys, we want to compute zs = xs / ys
     * Natively, this is done as: zs[i] = xs[i] / ys[i], for i = 0..n
     * Problem is if ys is sparse (a lot of 0s), we get lots of Infinitys, which looks
     * ugly on charts (eg. cpa, cpm, cpc)
     *
     * This function does the smoothing and degenerate to naive case when ys is dense (very few 0s)
     *
     * @example
     * TO-DO
     *
     * @param xs
     * @param ys
     *
     * @return {undefined}
     */
    function expSmooth(xs, ys, a, cumxs, cumys) {
      // jshint unused: false
      var len = xs.length;
      cumxs = cumxs || cumsum(xs);
      cumys = cumys || cumsum(ys);

      cumxs['-1'] = 0;
      cumys['-1'] = 0;


      var i0, ratio, dx, dy, output=[], xunit, satisfied;

      var r1, r2;
      var b, zeroCount;
      if(!(a >=0 && a <= 1)) {
        zeroCount = 0;
        for(var k=0; k<len; k++) {
          if(!ys[k]) {
            zeroCount++;
          }
        }
        b = zeroCount / len / 2; 
        a=1-b;
      } else {
        b=1-a;
      }

      for(var i=0; i<len; i++) {
        r1 = cumys[i-1] > 0 ? cumxs[i-1] / cumys[i-1] : NaN;
        r2 = cumys[i] > 0 ? cumxs[i] / cumys[i] : NaN;
        if(i===0) {
          output.push(r2);
        } else if (ys[i] > 0) {
          output.push(xs[i] / ys[i] * a + b * r1);
        } else if(r2 && r1) {
          output.push(r2 * a + r1 * b);
        } else {
          output.push(r1);
        } 
      }
      return output;

      /*
      xunit = cumxs[len-1] / cumys[len - 1] * k;
      for(var i=0; i<len; i++) {
        if(i === 0) {
          ratio = ys[0] > 0 ? xs[0] / ys[0] : NaN;
          output.push(ratio);
          continue;
        } 
        i0 = i - 1;
        dy = ys[i];
        satisfied = false;
        while(i0 >= 0) {
          dy = cumys[i] - cumys[i0]; 
          dx = cumxs[i] - cumxs[i0];
          if(dy >= k && dx >= xunit * dy) {
            satisfied = true;
            break;
          }
          i0--;
        }
        if(!satisfied) {
          dy = cumys[i] - cumys[i0];
          dx = cumxs[i] - cumxs[i0];
        }
        ratio = dy > 0 ? dx / dy : NaN;
        output.push(ratio);
      }
      return output; 
      */
    }

    function initDatum(chanIds) {
      var d = {}; 
      for(var i=0; i<chanIds.length; i++) {
        d[chanIds[i]] = null;
      }
      return d;
    }

    function extractSeries(data, kpi, chanId) {
      return _.map(data[kpi], function(d) {
        return d[chanId];
      });
    }

    function mergeChannelSeriesInto(chanId, xs, data) {
      for(var i=0; i<data.length; i++) {
        data[i][chanId] = xs[i];
      }
      return data;
    }
    
    // CampaignSummary -> Number -> [StatDatum]
    
    var lastIteration = { campaign : null, a : null, result : null };
    
    function smoothCpa(cmp, a) {
      if(!cmp.tsCampaignStats) {
        return [];
      }
      if(cmp === lastIteration.campaign && a === lastIteration.a && lastIteration.result) {
        return lastIteration.result;
      }
      var channels = cmp.channels.map(function(c) { return c.id; }); 
      var cpas = _.map(cmp.tsCampaignStats.cpa, function(d0) {
        var d = initDatum(channels); 
        d.date = d0.date;
        return d;
      });
      var chanId, spent, convs, cumspent, cumconvs, computedCpas;
      for(var i=0; i<channels.length; i++) {
        chanId = channels[i];
        spent = extractSeries(cmp.tsCampaignStats, 'spent', chanId);
        convs = extractSeries(cmp.tsCampaignStats, 'conversions', chanId);
        cumspent = extractSeries(cmp.tsCampaignStats, 'cumspent', chanId);
        cumconvs = extractSeries(cmp.tsCampaignStats, 'cumconvs', chanId);
        computedCpas = expSmooth(spent, convs, a, cumspent, cumconvs);
        mergeChannelSeriesInto(chanId, computedCpas, cpas);
      }
      lastIteration.campaign = cmp;
      lastIteration.a = a;
      lastIteration.result = cpas;
      return cpas;
    }

    return {
      cumsum : cumsum,
      cumRatio : cumRatio,
      expSmooth : expSmooth,
      smoothCpa : smoothCpa
    };

  });
