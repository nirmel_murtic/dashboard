'use strict';

angular.module('act')
  .service('CampaignSummarySubCampaign', function(CampaignInfoSubCampaign, CampaignStatsMixin, fsUtils) {
    function CampaignSummarySubCampaign() {
      CampaignInfoSubCampaign.apply(this, _.toArray(arguments));
    }
    fsUtils.inherits(CampaignSummarySubCampaign, CampaignInfoSubCampaign);
    _.assign(CampaignSummarySubCampaign.prototype, CampaignStatsMixin);

    return CampaignSummarySubCampaign;
  });
