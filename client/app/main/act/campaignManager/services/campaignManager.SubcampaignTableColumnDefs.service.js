'use strict';

angular.module('act')
  .factory('SubcampaignTableColumnDefs', function($filter, SubCmpID, SubCmpWarning) {
    var translate = $filter('translate');
    return {
      id : {
        type : 'string',
        label : 'ID',
        reactComponent : SubCmpID,
        fixed : true,
        filter : false,
        sort : false
      },
      status : {
        label : translate('label.act.OptimizationStatus'),
        type : 'atom',
        fixed : true,
        format : function(status) {
          switch (status) {
            case 'paused':
              return translate('label.act.Paused');
            case 'active':
              return translate('label.act.Active');
            case 'ended':
              return translate('label.act.Ended');
            case 'scheduled':
              return translate('label.act.Scheduled');
            case 'past':
              return translate('label.act.Past');
            default:
              return status;
          }
        },
        levels : [
          'paused',
          'active',
          'ended'
        ], 
        sort : true
      },
      channel : {
        label : translate('label.act.Channel'),
        fixed : true,
        type : 'atom',
        format : function(chan) {
          switch(chan) {
            case 'instagram':
              return translate('label.act.Instagram');
            case 'facebook':
              return translate('label.act.Facebook');
            case 'adwords':
              return translate('label.act.Adwords');
            case 'twitter':
              return translate('label.act.Twitter');
            default:
              return chan;
          } 
        },
        levels : [
          'facebook',
          'adwords',
          'twitter'
        ],
        sort : true
      },
      impressions : {
        type : 'numeric',
        label : translate('label.act.Impressions'),
        format : $filter('metricVolumeCompact'),
        sort : true
      },
      clicks : {
        type : 'numeric',
        label : translate('label.act.Clicks'),
        format : $filter('metricVolumeCompact'),
        sort : true
      },
      ctr : {
        type : 'numeric',
        label : translate('label.act.CTR'),
        format : $filter('metricPercentage'),
        sort : true
      },
      spend : {
        type : 'numeric',
        label : translate('label.act.Spent'),
        format : $filter('metricCurrency'),
        sort : true
      },
      cpc : {
        type : 'numeric',
        label : translate('label.act.CPC'),
        format : $filter('metricCurrency'),
        sort : true
      },
      lowerBid : {
        type : 'numeric',
        label : translate('label.act.LowerBid'),
        format : $filter('metricCurrency'),
        sort : true
      },
      upperBid : {
        type : 'numeric',
        label : translate('label.act.UpperBid'),
        format : $filter('metricCurrency'),
        sort : true
      },
      conversions : {
        type : 'numeric',
        label : translate('label.act.GoalConv.'),
        format : $filter('metricVolume'),
        sort : true
      },
      cpa : {
        type : 'numeric',
        label : translate('label.act.CPA'),
        format : $filter('metricCurrency'),
        sort : true
      },
      warning : {
        type : 'string',
        label : '',
        columnGrow : 0.2,
        reactComponent : SubCmpWarning,
        fixed : true,
        filter : false,
        sort : false
      }
    };
  })
  .factory('SubcampaignTableRoasColumnDefs', function($filter) {
    var translate = $filter('translate');
    return {
      value : {
        type : 'numeric',
        label : translate('label.act.ConversionValueShort'),
        format : $filter('metricCurrency'),
        sort : true
      },
      roas : {
        type : 'numeric',
        label : translate('label.act.Roas'),
        format : $filter('metricPercentage'),
        sort : true
      }
    };
  })
  .factory('ParentCampaignTableColumnDefs', function($filter, SubCmpWarning, ClickEdit) {
    var translate = $filter('translate');
    return {
      channel : {
        label : translate('label.act.Channel'),
        type : 'atom',
        format : function(chan) {
          switch(chan) {
            case 'facebook':
              return translate('label.act.Facebook');
            case 'adwords':
              return translate('label.act.Adwords');
            case 'twitter':
              return translate('label.act.Twitter');
            default:
              return chan;
          } 
        },
        levels : [
          'facebook',
          'adwords',
          'twitter'
        ],
        sort : false,
        fixed : true,
        filter: false
      },
      maxBid : {
        label : translate('label.act.MaxBid'),
        type : 'numeric',
        format : $filter('metricCurrency'),
        sort : false,
        fixed : true,
        filter: false,
        reactComponent : ClickEdit
      },
      status : {
        label : translate('label.act.OptimizationStatus'),
        type : 'atom',
        fixed : true,
        format : function(status) {
          switch (status) {
            case 'paused':
              return translate('label.act.Paused');
            case 'active':
              return translate('label.act.Active');
            case 'scheduled':
              return translate('label.act.Scheduled');
            case 'past':
              return translate('label.act.Past');
            case 'ended':
              return translate('label.act.Ended');
            default:
              return status;
          }
        },
        levels : [
          'paused',
          'active'
        ], 
        sort: false,
        filter: false
      },
      impressions : {
        type : 'numeric',
        label : translate('label.act.Impressions'),
        format : $filter('metricVolumeCompact'),
        sort : false,
        filter: false
      },
      clicks : {
        type : 'numeric',
        label : translate('label.act.Clicks'),
        format : $filter('metricVolumeCompact'),
        sort : false,
        filter: false
      },
      ctr : {
        type : 'numeric',
        label : translate('label.act.CTR'),
        format : $filter('metricPercentage'),
        sort : false,
        filter: false
      },
      spend : {
        type : 'numeric',
        label : translate('label.act.Spent'),
        format : $filter('metricCurrency'),
        sort : false,
        filter: false
      },
      cpc : {
        type : 'numeric',
        label : translate('label.act.CPC'),
        format : $filter('metricCurrency'),
        sort : false,
        filter: false
      },
      lowerBid : {
        type : 'numeric',
        label : translate('label.act.LowerBid'),
        format : $filter('metricCurrency'),
        sort : false,
        filter: false
      },
      upperBid : {
        type : 'numeric',
        label : translate('label.act.UpperBid'),
        format : $filter('metricCurrency'),
        sort : false,
        filter: false
      },
      conversions : {
        type : 'numeric',
        label : translate('label.act.GoalConv.'),
        format : $filter('metricVolume'),
        sort : false,
        filter: false
      },
      cpa : {
        type : 'numeric',
        label : translate('label.act.CPA'),
        format : $filter('metricCurrency'),
        sort : false,
        filter: false
      },
      warning : {
        type : 'string',
        label : '',
        fixed : true,
        columnGrow : 0.2,
        reactComponent : SubCmpWarning,
        filter : false,
        sort : false
      }
    };
  });
