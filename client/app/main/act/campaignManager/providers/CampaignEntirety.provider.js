'use strict';

angular.module('act')
  .provider('CampaignEntirety', function(CampaignStatus, CampaignmanagerTopLevelFilters) {

    // jshint -W030

    var superCampaignsIndex = {};  // index super campaigns by superId
    if(__DEV__) {
      window.allCmps = superCampaignsIndex;
    }

    this.requestCampaignList = {};

    this.requestCampaignList.ACTIVE = ['CampaignmanagerWebApiUtils', 'auth', function(WebApiUtils, auth) {
      var filter = CampaignmanagerTopLevelFilters.ACTIVE.filter;
      var teamId = auth.user.currentTeam.id;
      var currentProjectId = auth.user.currentTeam.currentProjectId;
      var sortAndPaging = {
        limit : 22,
        offset : 0,
        sort : 'desc',
        sortColumn : 'startTime'
      };
      return function(conf) {
        conf = _.assign({}, sortAndPaging, conf || {});
        return WebApiUtils.getCampaignSummaryList(teamId, filter, currentProjectId, conf.limit, conf.offset, conf.sort, conf.sortColumn);
      };
    }];
    this.requestCampaignList.PAST = ['CampaignmanagerWebApiUtils', 'auth', function(WebApiUtils, auth) {
      var filter = CampaignmanagerTopLevelFilters.PAST.filter;
      var teamId = auth.user.currentTeam.id;
      var currentProjectId = auth.user.currentTeam.currentProjectId;
      var sortAndPaging = {
        limit : 22,
        offset : 0,
        sort : 'desc',
        sortColumn : 'endTime'
      };
      return function(conf) {
        conf = _.assign({}, sortAndPaging, conf || {});
        return WebApiUtils.getCampaignSummaryList(teamId, filter, currentProjectId, conf.limit, conf.offset, conf.sort, conf.sortColumn);
      };
    }];
    this.requestCampaignList.SCHEDULED = ['CampaignmanagerWebApiUtils', 'auth', function(WebApiUtils, auth) {
      var filter = CampaignmanagerTopLevelFilters.SCHEDULED.filter;
      var teamId = auth.user.currentTeam.id;
      var currentProjectId = auth.user.currentTeam.currentProjectId;
      var sortAndPaging = {
        limit : 22,
        offset : 0,
        sort : 'desc',
        sortColumn : 'startTime'
      };
      return function(conf) {
        conf = _.assign({}, sortAndPaging, conf || {});
        return WebApiUtils.getCampaignSummaryList(teamId, filter, currentProjectId, conf.limit, conf.offset, conf.sort, conf.sortColumn);
      };
    }];
    this.requestCampaignList.DRAFTS = ['CampaignmanagerWebApiUtils', 'auth', function(WebApiUtils, auth) {
      var filter = CampaignmanagerTopLevelFilters.DRAFTS.filter;
      var teamId = auth.user.currentTeam.id;
      var currentProjectId = auth.user.currentTeam.currentProjectId;
      var sortAndPaging = {
        limit : 22,
        offset : 0,
        sort : 'desc',
        sortColumn : 'startTime'
      };
      return function(conf) {
        conf = _.assign({}, sortAndPaging, conf || {});
        return WebApiUtils.getCampaignSummaryList(teamId, filter, currentProjectId, conf.limit, conf.offset, conf.sort, conf.sortColumn);
      };
    }];

    this.indexSuperCampaigns = ['$q', 'dispatcher', 'CampaignManagerActions', function($q, dispatcher, CampaignManagerActions) {
      return function(cmps, filterType) {
        // campaigns can be a promise or a value
        $q.when(cmps).then(function(campaigns) {
          campaigns = _.isArray(campaigns) ? campaigns : [campaigns];
          _.each(campaigns,
            function(cmp) {
              if(filterType) {
                cmp.tab = filterType;
              }
              superCampaignsIndex[cmp.superId] = cmp;
            });
          dispatcher.dispatch({
            source : 'server',
            action : {
              type : CampaignManagerActions.INCOMING_SUPER_CAMPAIGNS,
              campaigns : campaigns
            }
          });
        }, null);
      };
    }];

    this.$get = ['fsConfirm', 'fsNotify', 'CampaignmanagerWebApiUtils', 'Dispatcher', '$injector', 'CampaignManagerActions', 'CampaignStatus', '$q', '$filter',
      function(fsConfirm, fsNotify, WebApiUtils, Dispatcher, $injector, CampaignManagerActions, CampaignStatus, $q, $filter) {
      var dispatcher = new Dispatcher();
      dispatcher.register(function(payload) {
        var action = payload.action;
        var superCmp;
        switch(action.type) {
          case CampaignManagerActions.PAUSE_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.status = CampaignStatus.PAUSED);
            superCmp && (superCmp.interimStatus = null);
            superCmp && fsNotify.push('Campaign Paused.', 3000);
            break;
          case CampaignManagerActions.STOP_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.status = CampaignStatus.STOPPED);
            superCmp && (superCmp.interimStatus = null);
            superCmp && (superCmp.tab = 'PAST');
            superCmp && fsNotify.push('Campaign Stopped.', 3000);
            break;
          case CampaignManagerActions.RESUME_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.status = superCmp.getRunningStatus());
            superCmp && (superCmp.interimStatus = null);
            superCmp && fsNotify.push('Campaign Resumed.', 3000);
            break;
          case CampaignManagerActions.PAUSING_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.interimStatus = CampaignStatus.UPDATING);
            break;
          case CampaignManagerActions.STOPPING_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.interimStatus = CampaignStatus.UPDATING);
            break;
          case CampaignManagerActions.RESUMING_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.interimStatus = CampaignStatus.UPDATING);
            break;
          case CampaignManagerActions.HIDING_SUPER:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.interimStatus = CampaignStatus.UPDATING);
            break;
          case CampaignManagerActions.REVERT_SUPER_STATUS:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.status = action.status);
            superCmp && (superCmp.interimStatus = null);
            break;
          case CampaignManagerActions.HIDE_DRAFT:
            superCmp = superCampaignsIndex[action.id];
            superCmp && (superCmp.status = CampaignStatus.HIDDEN);
            superCmp && (superCmp.interimStatus = null);
            superCmp && fsNotify.push('Campaign Removed.', 3000);
            break;
          default:
            return;
        }
      });
      return {
        dispatcher : dispatcher,
        indexSuperCampaigns : $injector.invoke(this.indexSuperCampaigns, null, { dispatcher : dispatcher }),
        getSuperCampaignById : function(superId) {
          return superCampaignsIndex[superId] || null;
        },
        getSuperCampaignByParentId : function(parentId) {
          parentId = +parentId;
          return _.find(superCampaignsIndex, function(cmp) {
            try {
              return _.contains(cmp.parentCampaignIds, parentId);
            } catch(err) {
              return null;
            }
          });
        },
        setMaxBid : function(parentId, bid) {
          var superCmp = this.getSuperCampaignByParentId(parentId);
          var channel;
          if(superCmp) {
            channel = _.findIndex(superCmp.parentCampaignIds, +parentId);
            channel = channel >= 0 ? superCmp.channels[channel].id : undefined;
          }
          var promise = WebApiUtils.setMaxBid(parentId, bid);
          if(superCmp && channel) {
            promise.then(function() {
              if(bid > 0) {
                superCmp.maxBid[channel] = bid;
              } else {
                superCmp.maxBid[channel] = undefined;
              }
            });
          }
          return promise;
        },
        pauseSuper : function(superCmp) {
          var currentStatus = superCmp.status;
          var promises = _.map(superCmp.channels, function(chan, index) {
            return WebApiUtils.performCampaignAction(chan.id, CampaignStatus.PAUSED, {
              parentCampaignId : superCmp.parentCampaignIds[index],
              superCampaignId : superCmp.superId
            });
          });
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CampaignManagerActions.PAUSING_SUPER,
              id : superCmp.superId
            }
          });
          $q.all(promises).then(function() {
            dispatcher.dispatch({
              source : 'server',
              action : {
                type : CampaignManagerActions.PAUSE_SUPER,
                id : superCmp.superId
              }
            });
          }, function() {
            dispatcher.dispatch({
              source : 'server',
              action : {
                type : CampaignManagerActions.REVERT_SUPER_STATUS,
                id : superCmp.superId,
                status : currentStatus
              }
            });
          });
        },
        stopSuper : function(superCmp) {
          return fsConfirm('stopCampaign').then(function() {
            var currentStatus = superCmp.status;
            var promises = _.map(superCmp.channels, function(chan, index) {
              return WebApiUtils.performCampaignAction(chan.id, CampaignStatus.STOPPED, {
                parentCampaignId : superCmp.parentCampaignIds[index],
                superCampaignId : superCmp.superId
              });
            });
            dispatcher.dispatch({
              source : 'view',
              action : {
                type : CampaignManagerActions.STOPPING_SUPER,
                id : superCmp.superId
              }
            });
            $q.all(promises).then(function() {
              dispatcher.dispatch({
                source : 'server',
                action : {
                  type : CampaignManagerActions.STOP_SUPER,
                  id : superCmp.superId
                }
              });
            }, function() {
              dispatcher.dispatch({
                source : 'server',
                action : {
                  type : CampaignManagerActions.REVERT_SUPER_STATUS,
                  id : superCmp.superId,
                  status : currentStatus
                }
              });
            });
          });
        },
        resumeSuper : function(superCmp) {
          var currentStatus = superCmp.status;
          var promises = _.map(superCmp.channels, function(chan, index) {
            return WebApiUtils.performCampaignAction(chan.id, CampaignStatus.ACTIVE, {
              parentCampaignId : superCmp.parentCampaignIds[index],
              superCampaignId : superCmp.superId
            });
          });
          dispatcher.dispatch({
            source : 'view',
            action : {
              type : CampaignManagerActions.RESUMING_SUPER,
              id : superCmp.superId
            }
          });
          $q.all(promises).then(function() {
            dispatcher.dispatch({
              source : 'server',
              action : {
                type : CampaignManagerActions.RESUME_SUPER,
                id : superCmp.superId
              }
            });
          }, function() {
            dispatcher.dispatch({
              source : 'server',
              action : {
                type : CampaignManagerActions.REVERT_SUPER_STATUS,
                id : superCmp.superId,
                status : currentStatus
              }
            });
          });
        },
        deleteDraftSuper : function(superCmp) {
          return fsConfirm('generic', {
            body : $filter('translate')('label.act.Areyousurethatyouwanttoremoveentiredraft'),
            yes : $filter('translate')('label.act.YesRemovecampaign')
          }).then(function() {
            var currentStatus = superCmp.status;
            if(currentStatus !== CampaignStatus.DRAFT) {
              return;
            }
            var promise = WebApiUtils.hideDraft(superCmp.superId);
            dispatcher.dispatch({
              source : 'view',
              action : {
                type : CampaignManagerActions.HIDING_SUPER,
                id : superCmp.superId
              }
            });
            promise.then(function() {
              dispatcher.dispatch({
                source : 'server',
                action : {
                  type : CampaignManagerActions.HIDE_DRAFT,
                  id : superCmp.superId
                }
              });
            }, function() {
              dispatcher.dispatch({
                source : 'server',
                action : {
                  type : CampaignManagerActions.REVERT_SUPER_STATUS,
                  id : superCmp.superId,
                  status : currentStatus
                }
              });
            });
          });
        },
        saveBudgetIncreaseAndDuration: function (superCmp, campaignInfo) {
          return WebApiUtils.editCampaignBudgetAndDuration(superCmp, campaignInfo).
            then(function (){
              // Here we update the campaign object in the front end
              if (campaignInfo.newEndDate) {
                superCmp.endDate = campaignInfo.newEndDate;
              }
              if (superCmp.budgetPolicy === 'FLOATING') {
                if (campaignInfo.budgetIncrease > 0) {
                  superCmp.budget += campaignInfo.budgetIncrease;
                }
              } else if (superCmp.budgetPolicy === 'FIXED') {
                for (var channel in campaignInfo.channels) {
                  if (campaignInfo.channels.hasOwnProperty(channel) && campaignInfo.channels[channel].budgetIncrease > 0) {
                    // Update budget at the channel level
                    superCmp.budgetPerChannel[channel] += campaignInfo.channels[channel].budgetIncrease;
                    // Updates the overall budget (so the UI gets updated too)
                    superCmp.budget += campaignInfo.channels[channel].budgetIncrease;
                  }
                }
              }
              return true;
            }, function(error) {
              return $q.reject(error);
            });
        }
      };
    }];
  });
