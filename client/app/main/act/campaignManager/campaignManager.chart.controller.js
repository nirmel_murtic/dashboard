'use strict';

angular.module('act')
  .controller('CampaignmanagerChartCtrl',
  function ($rootScope,
            $scope,
            $state,
            $filter,
            ngDialog,
            campaignMetricsView,
            CampaignStatus,
            CampaignManagerStatTypes,
            CampaignmanagerTopLevelFilters,
            CampaignEntirety,
            campaignList,
            superCampaign,
            campaignsTopLevelFilter,
            subcampaigns,
            SubcampaignTableColumnDefs,
            SubcampaignTableRoasColumnDefs,
            ParentCampaignTableColumnDefs,
            RecentDateRange,
            TabularDispatcher,
            TabularConstants,
            isPartial,
            colorPresets,
            campaignDetailLevel,
            CampaignmanagerStatsUtils,
            CampaignmanagerWebApiUtils) {
    /* jshint newcap:false */

    console.log('Campaign Detail Level: ' + campaignDetailLevel);
    if(campaignDetailLevel === 'subcampaign') {
      $scope.metricsView = campaignMetricsView.getView();
    } else {
      $scope.metricsView = campaignMetricsView.getView(superCampaign);
    }

    // $scope.reloadConfig = { reload : isPartial ? 'main.act.campaignManager.home' : false };

    var translate = $filter('translate');

    var channelsWithoutSuper = _($scope.metricsView.campaign.channels)
      .filter(function (c) {
        return c.id !== 'super';
      });

    var channelLabels =
      channelsWithoutSuper
        .map(function (c) {
          return translate(c.label);
        })
        .value();
    var channelColors =
       colorPresets.solids.map(function(d) { return d.toString(); });
    var channelIds =
      channelsWithoutSuper
        .map(function (c) {
          return c.id;
        })
        .value();

    $scope.channelIds = channelIds;
    $scope.channelColors = channelColors;
    $scope.channelLabels = channelLabels;

    $scope.campaign = superCampaign;
    $scope.statTypes = CampaignManagerStatTypes;
    $scope.CampaignEntirety = CampaignEntirety;

    $scope.formatVolume = $filter('metricVolumeCompact');
    $scope.formatRate = $filter('metricPercentage');
    $scope.formatCurrency = $filter('metricCurrency');

    $scope.currentMetric = 'impressions';
    var currentTime = Date.now();
    var cmpTsDateRange, cmpTsLifetimeDateRange;
    cmpTsDateRange = RecentDateRange.dateRangeIntersection(
      [$scope.campaign.startDate, moment($scope.campaign.endDate).add(30, 'days').toDate()],  // we collect data up to 30 days after campaign ends
      $scope.campaign.tsDateRange ? [$scope.campaign.tsDateRange[0], new Date()] : [$scope.campaign.startDate, new Date()]
    );
    $scope.currentRangeSetting = {
      range: null
    };
    function configureDaterangePicker() {
      console.log('Configure date picker');
      var now_ = new Date();
      cmpTsDateRange = RecentDateRange.dateRangeIntersection(
        [$scope.campaign.startDate, moment($scope.campaign.endDate).add(30, 'days').toDate()],  // we collect data up to 30 days after campaign ends
        $scope.campaign.tsDateRange ? [$scope.campaign.tsDateRange[0], now_] : [$scope.campaign.startDate, now_]
      );
      cmpTsLifetimeDateRange = RecentDateRange.dateRangeIntersection(
        [$scope.campaign.startDate, $scope.campaign.endDate],  // we collect data up to 30 days after campaign ends
        $scope.campaign.tsDateRange ? [$scope.campaign.tsDateRange[0], now_] : [$scope.campaign.startDate, now_]
      );
      $scope.recentTimeRanges =
        RecentDateRange.ranges(now_)
          .within(cmpTsDateRange)
          .addRecent('Today', 'day')
          .addRecent('Last Week', 1, 'weeks')
          .addRecent('Last Month', 1, 'months');

      $scope.currentRangeSetting.range = _.clone(cmpTsLifetimeDateRange);

      var ranges = {
        'Lifetime': cmpTsLifetimeDateRange,
        'All Time': $scope.recentTimeRanges.rangeBound
      };

      if ($scope.campaign.status !== CampaignStatus.COMPLETED) {
        delete ranges.Lifetime;
      }

      // use a iif, if we want dynamic recent ranges in the future,
      // make this variable a function instead
      var applicableRecentTimeranges = (function () {
        return _($scope.recentTimeRanges.getRangePresets()).filter(function (r) {
          return r.enabled;
        }).reduce(function (ranges, rangeSetting) {
          ranges[rangeSetting.label] = rangeSetting.range;
          return ranges;
        }, ranges);
      })();

      $scope.daterangePickerOptions = {
        ranges: applicableRecentTimeranges,
        minDate: $scope.recentTimeRanges.rangeBound[0],
        maxDate: $scope.recentTimeRanges.rangeBound[1]
      };

      currentTime = Date.now();
    }

    if ($scope.campaign.startDate <= Date.now() && $scope.campaign.tsCampaignStats && cmpTsDateRange) {

      // configureDaterangePicker();

      $scope.$watch(function () {
        return Date.now() - currentTime > 3600000; // refresh date picker after an hour 
      }, configureDaterangePicker);

      $scope.updateTimeRange = function (rangeSetting) {
        var interval = 'hour', len;
        if (rangeSetting.range) {
          while (rangeSetting.range.length > 2) {
            rangeSetting.range.pop();
          }
          len = RecentDateRange.dateRangeLength(rangeSetting.range);
        } else {
          return;
        }

        if (len > 24 * 3600000 * 15) {
          interval = 'day';
        }

        return CampaignmanagerWebApiUtils.getTimeseriesStats($scope.campaign, {
          startime: rangeSetting.range[0].getTime(),
          endtime: rangeSetting.range[1].getTime(),
          interval: interval
        }).then(function () {
          if ($scope.campaign.tsCampaignStats) {
            $scope.campaign.tsCampaignStats = _.mapValues($scope.campaign.tsCampaignStats, function (metricSeries) {
              return _.filter(metricSeries, function (d) {
                return d.date >= rangeSetting.range[0] &&
                  d.date <= rangeSetting.range[1];
              });
            });
          }
        });
      };
    }

  });
