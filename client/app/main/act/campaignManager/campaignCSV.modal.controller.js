'use strict';

angular.module('act')
  .controller('CampaignCSV', function($scope, $filter, CampaignmanagerWebApiUtils) {
    $scope.csv = {};

    $scope.csv.daterangerPickerOpts = {
    };

    $scope.csv.reportTypes = [
      {
        label : 'label.act.Overview',
        id : 'overview'
      },
      {
        label : 'label.act.ChannelComparison',
        id : 'channel_comparison'
      }
    ].concat($scope.campaign.channels);

    if(_.find($scope.campaign.channels, { id: 'adwords' })) {
      $scope.csv.reportTypes.push({
        id : 'adwords_keywords',
        label : 'label.act.AdWordsKeywords'
      }); 
    }

    $scope.csv.intervalOpts = [
      {
        key : 'hour',
        label : 'label.act.Hourly'
      },
      {
        key : 'day',
        label : 'label.act.Daily'
      },
      {
        key : 'week',
        label : 'label.act.Weekly'
      }
    ];

    var interval = 'hour';
    if($scope.campaign.endDate - $scope.campaign.startDate > 24 * 3600000) {
      interval = 'day';
    }


    $scope.csv.config = {
      reportName : $scope.campaign.name,
      dateRange : [$scope.campaign.startDate, $scope.campaign.endDate],
      interval : interval,
      reports : _.range($scope.csv.reportTypes.length).map(function(i) {
        return {
          index : i,
          checked : true
        };
      })
    };
    $scope.csv.noTypeChecked = function() {
      return _.all($scope.csv.config.reports, function(t) {
        return !t.checked;
      });
    };

    var getReportTypes = function() {
      return _($scope.csv.config.reports).filter(function(r) {
        return r.checked;
      }).map(function(r) {
        var type = $scope.csv.reportTypes[r.index].id.toUpperCase();
        if(type === 'ADWORD') {
          return 'ADWORDS';
        }
        return type;
      }).value();
    };


    $scope.download = function() {
      var types = getReportTypes(); 
      CampaignmanagerWebApiUtils.downloadCSV($scope.campaign.superId, $scope.csv.config, types)
        .then(function() {
          $scope.closeThisDialog();
        });
    };

  });
