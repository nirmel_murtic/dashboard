'use strict';

angular.module('act')
  .controller('CampaignmanagerCampaignCtrl',
  function ($rootScope,
            $scope,
            $state,
            $filter,
            ngDialog,
            campaignMetricsView,
            CampaignStatus,
            CampaignManagerStatTypes,
            CampaignmanagerTopLevelFilters,
            CampaignEntirety,
            campaignList,
            superCampaign,
            campaignsTopLevelFilter,
            subcampaigns,
            SubcampaignTableColumnDefs,
            SubcampaignTableRoasColumnDefs,
            ParentCampaignTableColumnDefs,
            RecentDateRange,
            TabularDispatcher,
            TabularConstants,
            isPartial) {
    /* jshint newcap:false */


    $scope.reloadConfig = { reload : isPartial ? 'main.act.campaignManager.home' : false };

    $scope.campaign = superCampaign;
    $scope.metricsView = campaignMetricsView.getView();
    $scope.statTypes = CampaignManagerStatTypes;
    $scope.CampaignEntirety = CampaignEntirety;

    // cpa alert
    $scope.hasCpaAlert = $scope.campaign && $scope.campaign.hasCpaLimits;
    $scope.cpaAlert = { show : false };

    // download csv
    var surpress = function (e) {
      e.preventDefault();
    };
    $scope.openCsvModal = function () {
      var free = $rootScope.$on('$stateChangeStart', surpress);
      var modal = ngDialog.open({
        template: 'app/main/act/campaignManager/templates/campaignCSV.modal.html',
        controller: 'CampaignCSV',
        scope: $scope,
        className: 'modal'
      });
      modal.closePromise.then(free);
    };

    $scope.openEditBudgetAndDurationModal = function () {
      // Open Modal
      var free = $rootScope.$on('$stateChangeStart', surpress);
      var modal = ngDialog.open({
        template: 'app/main/act/campaignManager/templates/editBudgetAndDuration.modal.html',
        controller: 'CampaignmanagereditbudgetanddurationCtrl',
        scope: $scope,
        className: 'modal'
      });
      modal.closePromise.then(free);

    };

    $scope.editCampaign = function (cmpId, type) {
      type = type || 'draft';
      $state.go('main.act.campaignCreation', {id: cmpId, type: type});
    };

  });
