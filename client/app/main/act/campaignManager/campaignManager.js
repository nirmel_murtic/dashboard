'use strict';

angular.module('act')
  .config(function ($stateProvider, CampaignEntiretyProvider, fsConfirmProvider) {

    fsConfirmProvider.config('stopCampaign', {
      templateUrl: 'app/main/act/campaignManager/templates/confirm.stopCampaign.html'
    });

    $stateProvider
      .state('main.act.campaignManager', {
        abstract: true,
        url : '/campaigns',
        permissions: '<LOGGED_IN> && <ACT> && <CAMPAIGN_MANAGER>',
        resolve : {
          // decorate CampaignEntirety provider with auth info (specifically, we need user's current team id)
          CampaignEntirety : function(CampaignEntirety, CampaignmanagerTopLevelFilters, $injector, auth) {
            CampaignEntirety.requestCampaignList = _.mapValues(CampaignmanagerTopLevelFilters, function(__, key) {
              return $injector.invoke(CampaignEntiretyProvider.requestCampaignList[key], null, { auth: auth });
            });
            return CampaignEntirety;
          }
        },
        views: {
          '@main': {
            templateUrl: 'app/main/act/campaignManager/campaignManager.html'
          }
        }
      })
      .state('main.act.campaignManager.error', {
        url: '/error',
        data : {
          displayName : 'Error',
          active: 'campaign'
        },
        views: {
          '@main.act.campaignManager': {
            templateUrl: 'app/main/act/campaignManager/templates/campaignManager.error.html',
            controller: 'CampaignmanagerErrorCtrl'
          }
        },
        params : {
          // generic error message when unknown error happens
          // usually this param will be set by $stateChangeError handlers
          msg : 'Oops, something went wrong.'
        }
      });
  })
  .run(function($rootScope, $state, TabularDispatcher) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
      if(fromState.name === 'main.act.campaignManager.home.single') {
        TabularDispatcher.unregisterAll();
      }
    });
    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
      if(__DEV__) {
        console.log('State Change Error', error);
        window.$$stateChangeError = error;
      }
      if(error && error.errorState && error.errorParams) {
        if(__DEV__) {
          console.log('Error Successfully caught.');
        }
        event.preventDefault();
        $state.go(error.errorState, error.errorParams);
      }
    });
  });
