'use strict';

angular.module('act')
  .controller('CampaignmanagerSubCampaignCtrl',
    function (
      $rootScope, 
      $scope, 
      $state, 
      $filter, 
      $sce,
      apiUrl,
      campaignMetricsView,
      CampaignStatus,
      CampaignManagerStatTypes, 
      CampaignmanagerStatsUtils,
      CampaignmanagerTopLevelFilters, 
      CampaignEntirety, 
      campaignList, 
      superCampaign, 
      campaignsTopLevelFilter, 
      subcampaigns, 
      SubcampaignTableColumnDefs, 
      RecentDateRange, 
      subCampaignDetails,
      subCampaignSummary) {
    /* jshint newcap:false */

    $scope.metricsView = campaignMetricsView.getView();
    $scope.getImageEndpoint = apiUrl + '/api/v2/image/get?guid=';

    
    $scope.campaign = subCampaignSummary;

    $scope.superCampaign = superCampaign;
    $scope.category = campaignsTopLevelFilter.filterType.toLowerCase();

    $scope.statTypes = CampaignManagerStatTypes;

    $scope.CampaignEntirety = CampaignEntirety;

  });
