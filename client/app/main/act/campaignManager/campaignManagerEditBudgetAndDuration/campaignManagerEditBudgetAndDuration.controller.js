'use strict';

angular.module('act')
  .controller('CampaignmanagereditbudgetanddurationCtrl', function ($scope, $filter, CampaignEntirety, CampaignmanagerWebApiUtils, ngDialog, fsConfirm, regex) {

    var todayDate = new Date();
    var minEndDate;
    // The new end date needs to be at least one hour after the start date
    if (todayDate < $scope.campaign.startDate) {
      minEndDate = angular.copy($scope.campaign.startDate);
      minEndDate.setHours(minEndDate.getHours() + 1);
    } else {
      minEndDate = todayDate;
    }

    $scope.view = {
      saveButtonDisabled: false,
      durationDateRangePickerModel: {},
      durationDateRangePickerOptions: {
        timePicker: true,
        dateLimit: 0,
        showDropdowns: true,
        minDate: minEndDate,
        singleDatePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 5,
        timePicker12Hour: true,
        timePickerSeconds: false
      },
      numberWithUpToTwoDecimals: regex.NUMBERUPTOTWODECIMALS
    };

    $scope.campaignInfo = {
      budgetIncrease: 0,
      channels: {},
      newEndDate: null
    };

    for (var i = 0, j = $scope.campaign.channels.length; i < j; i++) {
      $scope.campaignInfo.channels[$scope.campaign.channels[i].id] = {
        budgetIncrease: 0
      };
    }

    $scope.commitCampaignDateRange = function (range) {
      if (range) {
        $scope.campaignInfo.newEndDate = range[1];
      }
    };

    function saveBudgetIncreaseAndDuration () {
      $scope.CampaignEntirety.saveBudgetIncreaseAndDuration($scope.campaign, $scope.campaignInfo)
        .then(function(){ // Succss
          ngDialog.close();
        }, function() { // Error
          // Let the user know something went wrong
          fsConfirm('genericAlert', {body: $filter('translate')('label.act.campaignExtensionEndpointFailureMessage')});
        }

        );
    }

    $scope.saveNewBudgetAndDateButton = function(form) {

      if ((!$scope.campaignInfo.newEndDate || $scope.campaignInfo.newEndDate.getTime() === $scope.campaign.endDate.getTime()) &&
        ($scope.campaign.budgetPolicy === 'FLOATING' && (!$scope.campaignInfo.budgetIncrease || $scope.campaignInfo.budgetIncrease <= 0 || !form.fbBudget.$valid) ||
        $scope.campaign.budgetPolicy === 'FIXED' &&
        (!$scope.campaignInfo.channels.facebook || !$scope.campaignInfo.channels.facebook.budgetIncrease || $scope.campaignInfo.channels.facebook.budgetIncrease <= 0 || !form.fbBudget.$valid) &&
        (!$scope.campaignInfo.channels.adwords  || !$scope.campaignInfo.channels.adwords.budgetIncrease ||$scope.campaignInfo.channels.adwords.budgetIncrease <= 0 || !form.awBudget.$valid) &&
        (!$scope.campaignInfo.channels.twitter || !$scope.campaignInfo.channels.twitter.budgetIncrease || $scope.campaignInfo.channels.twitter.budgetIncrease <= 0 || !form.twBudget.$valid))) {
        // Alert the user of the need to either extend the duration or increase the budget
        fsConfirm('genericAlert', {body: $filter('translate')('label.act.campaignExtensionInvalidMessage')});
      } else {
        if (!$scope.campaignInfo.newEndDate || $scope.campaignInfo.newEndDate.getTime() === $scope.campaign.endDate.getTime()) {
          delete $scope.campaignInfo.newEndDate;
        }
        //Hide Budget recommendation
        $scope.budgetRecommendation = {};

        // Assign 0 to fields where the user deleted the budget increase without reassigning a new one
        if (!$scope.campaignInfo.budgetIncrease) {
          $scope.campaignInfo.budgetIncrease = 0;
        }
        _.forOwn($scope.campaignInfo.channels, function(channel){
          if (!channel.budgetIncrease) {
            channel.budgetIncrease = 0;
          }
        });

        CampaignmanagerWebApiUtils.getCampaignBudgetRecommendation($scope.campaign.superId, $scope.campaignInfo)
          .then(function (result) {
            $scope.budgetRecommendation = result;
            if (!$scope.budgetRecommendation.recommendationRequired) {
              saveBudgetIncreaseAndDuration();
            }
          });
      }
    };

    $scope.applyBudgetRecommendation = function (type) {
      switch (type) {
        case 'budget':
          // Set the budget of the campaign for Cross channel
          if ($scope.campaign.budgetPolicy === 'FLOATING') {
            $scope.campaignInfo.budgetIncrease += Math.round($scope.budgetRecommendation.raiseCrossChannelBudget * 100) / 100;
          } // Set the budget of each channel for multi
          else if ($scope.campaign.budgetPolicy === 'FIXED') {
            _.forOwn($scope.budgetRecommendation.mapRaiseMultiChannelBudget, function (budgetToAdd, channel){
              $scope.campaignInfo.channels[channel].budgetIncrease += Math.round(budgetToAdd * 100) / 100;
            });
          }
          break;
        case 'duration':
          // Decrease the number of days
          if (!$scope.campaignInfo.newEndDate) {
            var newDate = angular.copy($scope.campaign.endDate);
            $scope.metadata.durationDateRangePickerModel =  [newDate, newDate];
            $scope.campaignInfo.newEndDate = angular.copy($scope.campaign.endDate);
          }
          $scope.campaignInfo.newEndDate.setDate($scope.campaignInfo.newEndDate.getDate() - $scope.budgetRecommendation.decreaseNumberDays);
          break;
      }

      // Either way, mark budget recommendationNeeded as false to hide it from the view and save
      $scope.budgetRecommendation.recommendationRequired = false;
    };
  });
