'use strict';

describe('Controller: CampaignmanagereditbudgetanddurationCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var CampaignmanagereditbudgetanddurationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CampaignmanagereditbudgetanddurationCtrl = $controller('CampaignmanagereditbudgetanddurationCtrl', {
      $scope: scope
    });
  }));

});
