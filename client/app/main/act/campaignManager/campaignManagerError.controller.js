'use strict';

angular.module('act')
  .controller('CampaignmanagerErrorCtrl', ['$scope', '$stateParams', 
  function($scope, $stateParams) {
    $scope.errorMsg = $stateParams.msg;
  }]);
