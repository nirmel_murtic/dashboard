'use strict';

angular.module('act')
  .controller('CampaignmanagerAdCtrl',
    function (
      $rootScope, 
      $scope, 
      $state, 
      $filter, 
      $sce,
      apiUrl,
      campaignMetricsView,
      CampaignStatus,
      CampaignManagerStatTypes, 
      CampaignmanagerStatsUtils,
      CampaignmanagerTopLevelFilters, 
      CampaignEntirety, 
      campaignList, 
      superCampaign, 
      campaignsTopLevelFilter, 
      subcampaigns, 
      SubcampaignTableColumnDefs, 
      RecentDateRange, 
      subCampaignDetails,
      subCampaignSummary) {
    /* jshint newcap:false */

    $scope.getImageEndpoint = apiUrl + '/api/v2/image/get?guid=';

    $scope.campaign = subCampaignSummary;
    if(subCampaignSummary.channels[0].id === 'facebook') {
      if(!_.isArray(subCampaignDetails.preview)) {
        subCampaignDetails.preview = [subCampaignDetails.preview];
      }
      $scope.previews = _.map(subCampaignDetails.preview, function(preview) {
        return $sce.trustAsHtml(preview);
      });
      $scope.audiences = subCampaignDetails.audience;
    } else if(subCampaignSummary.channels[0].id === 'adwords') {
      $scope.audiences = subCampaignDetails.audience;
      $scope.creatives = subCampaignDetails.creative;
      $scope.keywords = subCampaignDetails.keywords;
    } else if(subCampaignSummary.channels[0].id === 'twitter') {
      $scope.getImageEndpoint = apiUrl + '/api/v2/image/get?guid=';
      $scope.audiences = subCampaignDetails.audience;
      $scope.creative = { creative : subCampaignDetails.creative };
    }

    $scope.superCampaign = superCampaign;
    $scope.category = campaignsTopLevelFilter.filterType.toLowerCase();

  });
