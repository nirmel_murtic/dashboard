'use strict';

angular.module('act')
  .run(function($rootScope, CampaignmanagerWebApiUtils, $state) {
    // if no category/filter is set, and only superId is provided, call backend to figure out
    // if if is active/past/etc..
    var supportedFilters = /ACTIVE|PAST|SCHEDULED|DRAFTS/i;
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams) {
      var superId = toParams.superId;
      var filter = toParams.filter;
      if(toState.name === 'main.act.campaignManager.home') {
        if(!supportedFilters.test(filter)) {
          e.preventDefault();
          $state.go('main.act.campaignManager.home', {
            filter : 'ACTIVE'
          });
        }
        return;
      }
      if(/^main\.act\.campaignManager\.home\.single/.test(toState.name)) {
        if(/\d+/.test(filter)) {
          superId = +filter;
          filter = '';
        }
        if(!superId) {
          e.preventDefault();
          $state.go('main.act.campaignManager.home', {
            filter : filter || 'active'
          });
        } else if(!filter || !supportedFilters.test(filter)) {
          e.preventDefault();
          CampaignmanagerWebApiUtils.checkCampaignCategory(superId)
            .then(function(filter) {
              $state.go(toState.name, _.assign(toParams, {
                // override filter using server response, while keeping all other state paramers
                filter : filter.toLowerCase(),
                superId : superId
              }));
            });
        }
      }
    });
  })
  .config(function ($stateProvider) {

    $stateProvider
      .state('main.act.campaignManager.home', {
        url: '/:filter',
        data : {
          displayName : 'Campaigns',
          active: 'campaign'
        },
        params : {
          subset : {}
        },
        resolve : {
          campaignList : function($stateParams, CampaignEntirety, CampaignStatus, $q, $state, CampaignmanagerWebApiUtils, auth) {
            var filterType = $stateParams.filter ? $stateParams.filter.toUpperCase(): 'ACTIVE';
            var promise;
            var campaignsThunk = CampaignEntirety.requestCampaignList[filterType];
            var skipWholeList = _.startsWith($state.next.name, this.name) && $state.next.name !== this.name;
            if(skipWholeList) {
              // loading only one super campaign
              promise = CampaignmanagerWebApiUtils.getCampaignSummaryById(auth.user.currentTeam.id, $state.toParams.superId); 
            } else {
              // loading many super campaigns
              // when campaigns are loaded from server, add them to index
              // so in all states/routes on campaignManager.home, we have easy access 
              // to them, and can perform campaign actions by superId
              promise = campaignsThunk($stateParams.subset);
            }
            return $q.when( promise )
              .then(function(cmps) {
                cmps = _.filter(cmps, function(cmp) {
                  return !cmp.tab || cmp.tab === filterType;
                });
                CampaignEntirety.indexSuperCampaigns(cmps, filterType);
                return cmps;
              }, null);
          },
          isPartial : function($state) {
            return _.startsWith($state.next.name, this.name) && $state.next.name !== this.name;
          },
          campaignsTopLevelFilter : function($stateParams) {
            var filterType = $stateParams.filter.toUpperCase() || 'ACTIVE';
            return {
              filterType : filterType
            };
          },
          projects : function(projectService, auth, $q) {
            if(auth.user.currentTeam.projects) {
              return _(auth.user.currentTeam.projects)
                .map(function(p) {
                  p.createdAt = new Date(p.createdAt);
                  return p;
                })
                .sortBy(function(d) {
                  return -d.createdAt;
                })
                .value();
            }
            var deferred = $q.defer();
            var teamId = auth.user.currentTeam.id;
            projectService.getProjects(teamId, function(result) {
              deferred.resolve(result.data.projects);
            }, function() {
              deferred.reject({
                errorState : 'main.act.campaignManager.error',
                errorParams : {
                  msg : 'Error loading projects for team ' + teamId
                }
              });
            });
            return deferred.promise;
          }
        },
        views : {
          '@main.act.campaignManager': {
            templateUrl: 'app/main/act/campaignManager/templates/campaignManager.home.html',
            controller: 'CampaignmanagerCtrl'
          }
        }
      });
  });

