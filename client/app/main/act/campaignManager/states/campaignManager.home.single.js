'use strict';

angular.module('act')
  .config(function ($stateProvider, campaignMetricsViewProvider) {

    $stateProvider
      .state('main.act.campaignManager.home.single', {
        url: '/:superId',
        data : {
          displayName : 'Campaign',
          active: 'campaign'
        },
        resolve : {
          // append time series stats to super campaign summary
          superCampaign : function($stateParams, CampaignEntirety, $q, CampaignmanagerWebApiUtils, campaignList, campaignsTopLevelFilter, auth, $resolve) {
            var superCmp = _.find(campaignList, { superId : Number($stateParams.superId) });
            if(!superCmp) {
              return $q.reject({
                errorState : 'main.act.campaignManager.error',
                errorParams : {
                  msg : 'Invalid super campaign category: \'' + $stateParams.filter + '\''
                }
              });
            }
            return $resolve.study({ init: campaignMetricsViewProvider.construct })({ campaign: superCmp }) // construct metrics view
              .then(function() {
                if(campaignsTopLevelFilter.filterType.toLowerCase() === 'scheduled') {
                  return superCmp;
                }
                if($stateParams.subCampaignId && $stateParams.channel) {
                  return CampaignmanagerWebApiUtils.getTimeseriesStats(superCmp, {
                    ids : Number($stateParams.subCampaignId)
                  });
                }
                return $q.all([
                  CampaignmanagerWebApiUtils.getTimeseriesStats(superCmp),
                  campaignsTopLevelFilter.filterType.toLowerCase() !== 'bactive' ? CampaignmanagerWebApiUtils.checkSuperCampaignEventLogEntries(superCmp, auth.user.currentTeam.id) : null
                ]).then(function() {
                  return superCmp;
                });
              });
          },
          subcampaigns : function($stateParams, CampaignmanagerWebApiUtils, campaignsTopLevelFilter) {
            return CampaignmanagerWebApiUtils.getSubcampaignSummary($stateParams.superId)
              .then(function(subcampaigns) {
                if (campaignsTopLevelFilter.filterType.toLowerCase() === 'past') {
                  _.each(subcampaigns, function (subCmp) {
                    subCmp.status = 'ended';
                  });
                }
                return subcampaigns;
              });
          },
          campaignDetailLevel : function() {
            return 'supercampaign';
          }
        },
        views : {
          '@main.act.campaignManager': {
            templateUrl: 'app/main/act/campaignManager/templates/campaignManager.SingleCampaign.html',
            controller: 'CampaignmanagerCampaignCtrl'
          },
          'charts@main.act.campaignManager.home.single' : {
            templateUrl: 'app/main/act/campaignManager/templates/campaignTsChart.html',
            controller : 'CampaignmanagerChartCtrl'
          },
          'subcampaigns@main.act.campaignManager.home.single' : {
            templateUrl: 'app/main/act/campaignManager/templates/subcampaigns.html',
            controller : 'CampaignmanagerCampaignSubcampaignsCtrl'
          }
        }
      });
  });

