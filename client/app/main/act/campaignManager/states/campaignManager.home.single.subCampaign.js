'use strict';

angular.module('act')
  .config(function ($stateProvider, campaignMetricsViewProvider) {

    $stateProvider
      .state('main.act.campaignManager.home.single.subCampaign', {
        url: '/{channel:facebook|adwords|twitter}/:subCampaignId',
        data : {
          displayName : 'Ad'
        },
        resolve : {
          superCampaign : function(superCampaign, CampaignmanagerWebApiUtils, $stateParams) {
            if($stateParams.subCampaignId && $stateParams.channel) {
              return CampaignmanagerWebApiUtils.getTimeseriesStats(superCampaign, {
                ids : Number($stateParams.subCampaignId)
              });
            }
            return superCampaign;
          },
          subCampaignDetails : function($stateParams, CampaignmanagerWebApiUtils) {
            var chan = $stateParams.channel.toLowerCase();
            var subCmpId = $stateParams.subCampaignId;
            if(chan === 'facebook') {
              return CampaignmanagerWebApiUtils.getFbSubCampaignDetails(subCmpId);
            } else if (chan === 'adwords') {
              return CampaignmanagerWebApiUtils.getAwSubCampaignDetails(subCmpId);
            } else if (chan === 'twitter') {
              return CampaignmanagerWebApiUtils.getTwSubCampaignDetails(subCmpId);
            }
          },
          subCampaignSummary : function($state, $stateParams, subcampaigns, superCampaign, CampaignSummarySubCampaign, CampaignManagerChannels, $q, campaignsTopLevelFilter, $resolve) {
            var chanId = $stateParams.channel.toLowerCase();
            var chan = CampaignManagerChannels[chanId]; 
            if(!chan) {
              return $q.reject({
                errorState : 'main.act.campaignManager.error', 
                errorParams : { 
                  msg : 'Unsupported channel ' + chanId
                } 
              });
            }
            var subId = +$stateParams.subCampaignId;
            var subCmpInfo = _.find(subcampaigns, { subCampaignId : subId });
            var subCmp;
            if(!subCmpInfo) {
              subCmp = new CampaignSummarySubCampaign(superCampaign, {
                subCampaignId : subId,
                channels : [chan]
              });
              return subCmp;
              // return $q.reject({
              //  errorState : 'main.act.campaignManager.error', 
              //  errorParams : { 
              //    msg : 'Invalid SubCampaign ID: ' + $stateParams.subCampaignId
              //  } 
              // });
            }
            subCmp = new CampaignSummarySubCampaign(superCampaign, {
              subCampaignId : subId,
              budget : subCmpInfo.budget,
              channels : [chan],
              status : campaignsTopLevelFilter.filterType.toLowerCase() === 'past' ? superCampaign.status : subCmpInfo.channelStatus 
            });
            if(campaignsTopLevelFilter.filterType.toLowerCase() !== 'scheduled') {
              subCmp.extractTsStatSubCmp(subId, superCampaign.$$rawStatsJSON);
            }
            return $resolve.study({ 
              init : campaignMetricsViewProvider.construct
            })({ campaign: subCmp })
              .then(function() {
                return subCmp;
              });
          },
          campaignDetailLevel : function() {
            return 'subcampaign';
          }
        },
        views : {
          '@main.act.campaignManager': {
            templateUrl: 'app/main/act/campaignManager/templates/campaignManager.SingleSubCampaign.html',
            controller: 'CampaignmanagerSubCampaignCtrl'
          },
          'charts@main.act.campaignManager.home.single.subCampaign' : {
            templateUrl: 'app/main/act/campaignManager/templates/campaignTsChart.html',
            controller : 'CampaignmanagerChartCtrl'
          },
          'ad@main.act.campaignManager.home.single.subCampaign' : {
            templateUrl: 'app/main/act/campaignManager/templates/singleAd.html',
            controller : 'CampaignmanagerAdCtrl'
          }
        }
      });
  });

