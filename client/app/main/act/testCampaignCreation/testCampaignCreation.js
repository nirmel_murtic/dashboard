'use strict';

angular.module('act')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.act.testCampaignCreation', {
        url: '/testCampaignCreation',
        views: {
                    '@main': {
                    templateUrl: 'app/main/act/testCampaignCreation/testCampaignCreation.html',
                    controller: 'TestCampaignCreationCtrl'
               }
        }
        
      });
  });