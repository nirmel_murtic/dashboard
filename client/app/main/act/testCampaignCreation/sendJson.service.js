'use strict';

angular.module('act')
    .factory('sendJson', function ($http, apiUrl) {
    	return {
    		executeEndpoint: function(endpoint, data, appendResponse, method){
    			$http({
                    url: apiUrl + endpoint,
                    method: method || 'POST',
                    data: $.param({data: angular.toJson(data)})
                }).
                success(appendResponse).
                error(appendResponse);
            }
    	};
    });
