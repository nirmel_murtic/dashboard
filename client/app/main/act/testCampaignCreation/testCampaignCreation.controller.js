'use strict';

angular.module('act')
  .controller('TestCampaignCreationCtrl', function ($scope, $rootScope, sendJson, projectService) {  	
  	$scope.rows = [];
  	$scope.accountInfo = {
  		currentTeamId: $rootScope.fractalAccountInfo.currentTeam.id,
  		currentTeamName: $rootScope.fractalAccountInfo.currentTeam.name,
  		accountId: $rootScope.fractalAccountInfo.fractalAccount.id
  	};
  	$scope.endpoints = ['/api/v2/campaign/create/settings/' + $scope.accountInfo.currentTeamId,                         
                        '/api/v2/campaign/update/settings/' + $scope.accountInfo.currentTeamId,
                        '/api/v2/campaign/delete/settings/' + $scope.accountInfo.accountId,
                        '/api/v2/campaign/create/adCombo/' + $scope.accountInfo.accountId,
                        '/api/v2/campaign/update/adCombo/' + $scope.accountInfo.accountId,
                        '/api/v2/campaign/delete/adCombo/' + $scope.accountInfo.accountId,
                        '/api/v2/campaign/get/all/adCombo/'+ $scope.accountInfo.accountId,
                        '/api/v2/campaign/create/adSetting/' + $scope.accountInfo.accountId,
                        '/api/v2/campaign/update/adSetting/' + $scope.accountInfo.accountId,
                        '/api/v2/campaign/publish/' + $scope.accountInfo.accountId];

  	$scope.addRow = function(){
  		var row = {};
  		row.endpoints = $scope.endpoints;
  		row.json = '';
      row.method = 'POST';
  		$scope.rows.push(row);
  	};

  	$scope.applyProjects = function(response){
  		$scope.accountInfo.projects = response.data.projects;
  	};

  	$scope.selectEndpoint = function(endpoint, row){
  		row.selectedEndpoint = endpoint;
  	};

  	$scope.removeRow = function(index){
  		$scope.rows.splice(index, 1);
  	};

     $scope.sendJson = function(json, row){
  		var appendResponse = function(response){
        	row.response = response;
    	};
        var parsedJson = JSON.parse(json);
        sendJson.executeEndpoint(row.selectedEndpoint, parsedJson,  appendResponse, row.method);
    };

    projectService.getProjects($rootScope.fractalAccountInfo.currentTeam.id, $scope.applyProjects);
    $scope.addRow();
  });
