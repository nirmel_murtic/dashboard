'use strict';

angular.module('main')
  .factory('targetingSpecs', function () {
    return {
      makePillObjects: function (card) {
        var pills = [];
        for(var type in card){
          if(card[type].length > 0){
            for(var i = 0; i < card[type].length; i++){
              for(var j = 0; j < card[type][i].pills.length; j++){
                if(type === 'gender' || type === 'relationship' || type === 'interestedIn' || type === 'ageRange'){
                    pills.push(this.makePillForBlankSegment(card[type][i].pills[j], type));
                } else if(type === 'location'){
                    pills.push(this.makePillForValueSegment(card[type][i].pills[j], type));
                } else if(type === 'workposition' || type === 'interest' || type === 'category' || type === 'language'){
                    pills.push(this.makePillForNameSegment(card[type][i].pills[j], type));
                }
              }
            }
          }
        }
        return pills;
      },
      makePillForValueSegment: function(segment, type){
        var pill = {};
        pill.type = type;
        pill.name = segment.value;
        return pill;
      },
      makePillForNameSegment: function(segment, type){
        var pill = {};
        pill.name = segment.name;
        pill.type = type;
        return pill;
      },
      getIconMapping: function(){
        return {
          'language': 'fa-language',
          'gender': 'fa-female',
          'ageRange': 'fa-male',
          'location': 'fa-map-marker',
          'interest': 'fa-gamepad',
          'relationship': 'fa-heart',
          'interestedIn': 'fa-heart-o',
          'workposition': 'fa-suitcase',
          'category': 'fa-beer'
        };
      },
      makePillForBlankSegment: function(segment, type){
        var pill = {};
        pill.type = type;
        pill.name = segment;
        return pill;
      }
    };
  });
