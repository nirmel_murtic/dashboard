'use strict';

describe('Service: targetingSpecs', function () {

  // load the service's module
  beforeEach(module('main'));

  // instantiate service
  var targetingSpecs;
  beforeEach(inject(function (_targetingSpecs_) {
    targetingSpecs = _targetingSpecs_;
  }));

  it('should do something', function () {
    expect(!!targetingSpecs).toBe(true);
  });

});
