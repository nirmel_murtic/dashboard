'use strict';

angular.module('act')
  .service('FbAudience', function (Audience, fsUtils, $http, apiUrl, $rootScope, fsConfirm, $filter, withInput, ngDialog, fsNotify) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var FbAudience;

    FbAudience = (function() {
        function FbAudience(audience) {
            // Call the super constructor Audience
            this.constructor.super_.call(this);

            if (audience) {
                for (var property in audience) {
                    if (audience.hasOwnProperty(property)) {
                        this[property] = audience[property];
                    }
                }
                return;
            }

            this.id = null;
            // All info related to the audience stored here
            this.audience = {};
        }

        // Implement inheritance from Audience abstract class
        fsUtils.inherits(FbAudience, Audience);

        return FbAudience;
    })();

    FbAudience.prototype.addVariant = function(variantType, variantId){
        var variants = this.audience[variantType];
        var emptyVariant = {
            variantId: variantId,
            values: []
        };
        if(!variants || variants.length === 0){
            this.audience[variantType] = [];
        } else {
            if(variants[variants.length - 1].values.length < 1 ){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddanothervariantifyouhaveablankone')});
                // $scope.focusInput(variantType);
                return;
            }
        }

        this.audience[variantType].push(emptyVariant);
    };


    FbAudience.prototype.addTargetingSpec = function(variantType, variantIndex, value){
        this.audience[variantType][variantIndex].values.push(value);
    };

    FbAudience.prototype.getValueFromVariantType = function(variantType, data){
        var value = {};
        if(variantType === 'location'){
            value.value = data.value.name;
            value.key = data.value.country_code; //jshint ignore:line
            value.type = data.model;
            value.displayValue = data.value.name;
            if(value.type === 'city' || value.type === 'zipcode' || value.type === 'region'){
                value.key = data.value.key;
                value.country_code = data.value.country_code; //jshint ignore:line
            }
            if(data.distance){
                value.displayValue = data.value.name + ' + ' + data.distance + ' ' + data.distance_unit + 's'; //jshint ignore:line
                value.distance = data.distance;
                value.distance_unit = data.distance_unit; //jshint ignore:line
            }
        } else if(variantType === 'language'){
            value.value = data.value.name;
            value.key = data.value.key;
        } else if(variantType === 'workEmployer'){
            value.id = data.value.id;
            value.value = data.value.name;
        } else if(variantType === 'workPosition'){
            value.id = data.value.id;
            value.value = data.value.name;
        } else if(variantType === 'preciseInterests'){
            value.value = data.value.name;
            value.id = data.value.id;
        } else if(variantType === 'advancedDemographics'){
            if(data.value){
                value.value = data.value.name;
                value.id = data.value.id;
                value.type = data.value.type;
            } else {
                value.value = data.name;
                value.id = data.id;
                value.type = data.type;
            }
        } else if(variantType === 'gender'){
            value.value = data;
        } else if(variantType === 'relationshipStatus'){
            value.value = data;
        } else if(variantType === 'interestedIn'){
            value.value = data;
        } else if(variantType === 'education'){
            if(data === 'High School' || data === 'High School Grad' ||
                data === 'Unspecified' || data === 'Some High School'){
                value.type = data;
                value.value = data;
            } else {

                if(data.fieldOfStudy){
                    value.fieldOfStudy = {};
                    value.fieldOfStudy.value = data.fieldOfStudy.name;
                    value.fieldOfStudy.id = data.fieldOfStudy.id;
                }

                if(data.school){
                    value.school = {};
                    value.school.value = data.school.name;
                    value.school.id = data.school.id;
                }
                if(data.minGradYear){
                    value.minGradYear = data.minGradYear;
                }
                if(data.maxGradYear){
                    value.maxGradYear = data.maxGradYear;
                }
            }
        } else if(variantType === 'ageRange'){
            value.value = data.min + ' - ' + data.max;
        } else if(variantType === 'behavior'){
            value.value = data.name;
            // value.path = data.path;
            value.id = data.id;
        } else if(variantType === 'connections'){
            value.value = data.page.pageName;
            value.type = data.type;
            value.id = data.page.pagePageId;
            value.displayType = data.displayType;
            value.displayValue = value.displayType + ': ' + value.value;
        } else if(variantType === 'mobileOs'){
            value.value = data;
        } else if(variantType === 'customAudiences' || variantType === 'excludedCustomAudiences'){
            value.id = data.fbAudienceId;
            value.name = data.audienceName;
            if(variantType === 'customAudiences'){
                value.displayValue = 'Include: ' +  data.audienceName;
            }
            if(variantType === 'excludedCustomAudiences'){
                value.displayValue = 'Exclude: ' +  data.audienceName;
            }
        }

        return value;
    };

    FbAudience.prototype.removeTargetingSpec = function(variantType, variantIndex, valueIndex){
        var variant = this.audience[variantType][variantIndex];
        variant.values.splice(valueIndex, 1);
    };


    FbAudience.prototype.removeVariant = function(variantType, index){
    	this.audience[variantType].splice(index, 1);
        if(this.audience[variantType].length === 0 ){
            delete this.audience[variantType];
        }
    };

    FbAudience.prototype.saveToLibrary = function(save, campaign){
        if(save){
            var newData = this;
            //set audience name to blank
            withInput.setInput('');
            fsConfirm('confirmationWithInput', {
              body: $filter('translate')('label.act.Nameyouraudience'),
              title: 'My Audience'
            }).then(function () {

              newData.name = withInput.getInput();

              var channel = campaign.selectedChannel.toUpperCase();

              var params = {
                channelName: channel,
                objectId: 0,
                libraryObjectType: 'audience',
                data: angular.toJson(newData)
              };

              $http({
                method: 'POST',
                url: apiUrl + '/api/v2/library/save/template/' + $rootScope.fractalAccountInfo.currentTeam.id,
                data: $.param(params)
              }).
                success(function (response) {

                  console.log('Audience response: ', response);
                  ngDialog.close();
                  fsNotify.push(
                    $filter('translate')('label.act.Audiencesavedtolibrary')+': '+ newData.name
                  );
                  withInput.destroyInput();
                }).
                error(function (response) {
                  withInput.destroyInput();
                  console.log(response);
                });
            });
        }
    };

    return FbAudience;
  });
