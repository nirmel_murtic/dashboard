'use strict';

describe('Service: FbAudience', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var FbAudience;
  var Audience;
  beforeEach(inject(function (_FbAudience_, _Audience_){
    FbAudience = _FbAudience_;
    Audience = _Audience_;
  }));

  it('should inherit from abstract Audience', function () {
    expect(new FbAudience() instanceof Audience).toBe(true);
  });

  it('should add variant with variantType', function(){
    var audience = new FbAudience();
    var variantType = 'location';
    audience.addVariant(variantType);
    expect(audience.audience[variantType].length).toBe(1);
  });

  it('should add targeting spec to variant with variantType and variant index', function(){
    var audience = new FbAudience();
    var variantType = 'location';
    var targetingSpec = {id: 0, value: 'CA'};
    audience.addVariant(variantType);
    audience.addTargetingSpec(variantType, 0, targetingSpec);
    expect(audience.audience[variantType][0].values.length).toBe(1);
  });

  it('should remove targeting spec with variantType, variantIndex, valueIndex', function(){
    var audience = new FbAudience();
    var variantType = 'location';
    var targetingSpec = {id: 0, value: 'CA'};
    audience.addVariant(variantType);
    audience.addTargetingSpec(variantType, 0, targetingSpec);
    audience.removeTargetingSpec(variantType, 0, 0);
    expect(audience.audience[variantType][0].values.length).toBe(0);
  });

  it('should remove variant with variantType, variantIndex', function(){
    var audience = new FbAudience();
    var variantType = 'location';
    audience.addVariant(variantType);
    audience.removeVariant(variantType, 0);
    expect(audience.audience[variantType]).toBe(undefined);
  });

});