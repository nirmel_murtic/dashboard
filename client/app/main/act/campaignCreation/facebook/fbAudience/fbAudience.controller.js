'use strict';

angular.module('act')
    .controller('FbAudienceCtrl', function ($scope, $parse, $http, FbAtpHelper, BehaviorHelper, customAudienceApi, $timeout, $rootScope, ngDialog, apiUrl, FbAudience, FbAudienceView, $filter, fsConfirm, $cacheFactory, $q, library) {
        $scope.locationType = 'country';
        $scope.connectionType = 'Include Users Connected To';
        $scope.fsAccountId = $rootScope.fractalAccountInfo.currentTeam.id;
        $scope.saveAudienceToLibraryCheckbox = false;
        $scope.newAudience = new FbAudience();
        $scope.view = new FbAudienceView();
        $scope.cityDistanceUnit = 'miles';
        $scope.cityDistance = {value: '', checked:false};
        //$scope.cityDistance.checked = false;
        $scope.educationFieldsUncommitted = {};
        $scope.loadingDemographics = false;
        $scope.showInterests = {};

        var atpHelper = new FbAtpHelper();
        $scope.behaviorHelper = new BehaviorHelper();

        $scope.facebookPages = $scope.campaign.api.facebook.pages;

        $scope.fbAdAccountId = $scope.campaign.settings.channels.facebook.adAccount.accountId;

        $scope.applyCustomAudiences = function(response){
            $scope.customAudiences = response.data;
        };

        $scope.applyLookalikeAudiences = function(response){
            $scope.lookalikeAudiences = response.data;
        };
        $scope.showError = function(response){
            console.log(response);
        };

        $scope.roundMiles = function(value) {
          if(value && value < 10) {
            $scope.cityDistance.value = 10;
          }
        };

        $scope.checkCityWithin = function() {
          if($scope.cityDistance.value) {
            if(!$scope.cityDistance.checked) {
              $scope.cityDistance.checked = !$scope.cityDistance.checked;
            }
          }
        };

        $scope.changeCityDistance = function(){
            $scope.checkCityWithin();
        };

        customAudienceApi.getAllAcceptedCustomAudiences($scope.fbAdAccountId, $scope.fsAccountId, $scope.applyCustomAudiences, $scope.showError);
        customAudienceApi.getAllAcceptedLookalikeAudiences($scope.fbAdAccountId, $scope.fsAccountId, $scope.applyLookalikeAudiences, $scope.showError);
        $scope.countryAtpConfig = atpHelper.getConfig('country', $scope.fbAdAccountId);
        $scope.regionAtpConfig = atpHelper.getConfig('region', $scope.fbAdAccountId);
        $scope.cityAtpConfig = atpHelper.getConfig('city', $scope.fbAdAccountId);
        $scope.zipcodeAtpConfig = atpHelper.getConfig('zipcode', $scope.fbAdAccountId);
        $scope.languageAtpConfig = atpHelper.getConfig('language', $scope.fbAdAccountId);
        $scope.schoolAtpConfig = atpHelper.getConfig('school', $scope.fbAdAccountId);
        $scope.fieldOfStudyAtpConfig = atpHelper.getConfig('fieldOfStudy', $scope.fbAdAccountId);
        $scope.workEmployerAtpConfig = atpHelper.getConfig('workEmployer', $scope.fbAdAccountId);
        $scope.workPositionAtpConfig = atpHelper.getConfig('workPosition', $scope.fbAdAccountId);
        $scope.preciseInterestsAtpConfig = atpHelper.getConfig('preciseInterests', $scope.fbAdAccountId);
        $scope.advancedDemographicsAtpConfig = atpHelper.getConfig('advancedDemographics', $scope.fbAdAccountId);


        //Select os if app install goal
        $scope.disableOsType = function(osType){
            if(osType === 'iOS'){
                $scope.isAndroidDisabled = true;
            } else {
                $scope.isIosDisabled = true;
            }
        };

        $scope.enableOsType = function(osType){
            if(osType === 'iOS'){
                $scope.isAndroidDisabled = false;
            } else {
                $scope.isIosDisabled = false;
            }
        };


        $scope.addVariant = function(variantType){
            //Validation if variant is blank
            var invalid = false;
            var variants = $scope.view[variantType].variants;
            if(variants.length >= 1){
                if(variants[variants.length - 1].pills.length < 1 ){
                    fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddanothervariantifyouhaveablankone')});
                    invalid = true; //jshint ignore:line
                    return;
                }
            }

            //logic specific to mobileOs
            if(variantType === 'mobileOs'){
                if($scope.supportedPlatforms.length === 1 && $scope.view[variantType].variants.length === 1){
                    fsConfirm('genericAlert', {body:$filter('translate')('label.act.Yourapponlysupportsoneplatformsoyoucannotaddmorevariants')});
                    invalid = true;
                    return;
                } else {
                    if($scope.view[variantType].variants.length >= 2){
                        fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddmorevariantsthanplatformsyourappsupports')});
                        invalid = true;
                        return;
                    }
                }
                $scope.enableOsType('iOS');
                $scope.enableOsType('Android');
            }

            if(!invalid){ //jshint ignore:line
                $scope.view.addVariant(variantType);
                $scope.newAudience.addVariant(variantType);
            }
        };

        $scope.selectEducationType = function(value){
            $scope.educationType = value;
            if(value === 'High School' || value === 'High School Grad' ||
                value === 'Unspecified' || value === 'Some High School'){
                $scope.educationFieldsUncommitted = {};
                $scope.addEducationValueToVariant('education');
            }

            if(value === 'Associate Degree' || value === 'In Grad School' ||
                value === 'Some Grad School' || value === 'Master Degree' ||
                value === 'Doctorate Degree' || value === 'Alumni'){
                delete $scope.educationFieldsUncommitted.maxGradYear;
                delete $scope.educationFieldsUncommitted.minGradYear;
            }
        };

        $scope.selectConnectionType = function(value){
            $scope.connectionType = value;
        };

        $scope.addEducationValueToVariant = function(variantType){
            $scope.addVariantIfNone(variantType);
            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
            var pillValue = $scope.view.getEducationPillValue($scope.educationType, $scope.educationFieldsUncommitted);
            var modelValue = $scope.newAudience.getValueFromVariantType(variantType, angular.copy($scope.educationFieldsUncommitted));
            modelValue.value = $scope.educationType;
            modelValue.type = $scope.educationType;
            modelValue.displayValue = pillValue; // In order to display the correct info in the combos page
            if($scope.checkForDuplicates(variantType, variantIndex, pillValue)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddtheduplicatevaluestothesamevariant')});
                return;
            }
            $scope.view.addTargetingSpec(variantType, pillValue);
            $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
            if($scope.educationFieldsUncommitted.minGradYear){
                delete $scope.educationFieldsUncommitted.minGradYear;
            }
            if($scope.educationFieldsUncommitted.maxGradYear){
                delete $scope.educationFieldsUncommitted.maxGradYear;
            }
        };

        $scope.clearAgeRange = function(ageRange){
            if(ageRange) {
              ageRange.min = '';
              ageRange.max = '';
            }
            angular.element('#age-range-1')[0].value = '';
            angular.element('#age-range-2')[0].value = '';
            angular.element('#age-range-1').focus();
        };

        $scope.addAgeRangeValueToVariant = function(ageRange){
            if(!ageRange.min || !ageRange.max) {
              fsConfirm('genericAlert', {body:$filter('translate')('label.act.Mustentervalidagerange')});
              return;
            }

            var variantType = 'ageRange';
            if(!ageRange.min || !ageRange.max){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Mustentervalidagerange')});
                return;
            }

            if(ageRange.min.match(/\D/) || ageRange.max.match(/\D/) || parseInt(ageRange.min) < 13 || parseInt(ageRange.min) > 65 ||  parseInt(ageRange.max) > 65 ||  parseInt(ageRange.min) < 13 || parseInt(ageRange.min) > parseInt(ageRange.max)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Mustentervalidagerange')});
                $scope.clearAgeRange(ageRange);
                return;
            }

            $scope.addVariantIfNone(variantType);

            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);

            var emptyVariantIndex = $scope.view.getNotSelectedEmptyVariantIndex(variantType);

            if($scope.view.ageRange.variants[variantIndex].pills.length >= 1 && !emptyVariantIndex){

                $scope.createNewVariantIfNotEmpty(ageRange, variantType);
                $scope.clearAgeRange(ageRange);

            } else if($scope.view.ageRange.variants[emptyVariantIndex].pills.length < 1 && !$scope.view.ageRange.variants[emptyVariantIndex].selected) {

              $scope.view.deselectVariants(variantType);
              $scope.view.selectVariant(variantType, emptyVariantIndex);
              $scope.createNewVariantIfNotEmpty(ageRange, variantType);
              $scope.clearAgeRange(ageRange);

            } else if($scope.view.ageRange.variants[emptyVariantIndex].pills.length < 1) {

              $scope.createNewVariantIfEmpty(ageRange, variantType, emptyVariantIndex);
              $scope.clearAgeRange(ageRange);

            }

        };

        $scope.createNewVariantIfEmpty = function(age, type, index) {
          var pillValue = $scope.view.getAgeRangePillValue(age);
          var modelValue = $scope.newAudience.getValueFromVariantType(type, age);
          $scope.view.addTargetingSpec(type, pillValue);
          $scope.newAudience.addTargetingSpec(type, index, modelValue);
        };

        $scope.createNewVariantIfNotEmpty = function(age, type) {
          var emptyIndex = $scope.view.getNotSelectedEmptyVariantIndex(type);

          if(!emptyIndex) {
            $scope.addVariant(type);
            emptyIndex = $scope.view.getNotSelectedEmptyVariantIndex(type);
          }

          var pillValue = $scope.view.getAgeRangePillValue(age);
          var modelValue = $scope.newAudience.getValueFromVariantType(type, age);
          $scope.view.addTargetingSpec(type, pillValue);
          $scope.newAudience.addTargetingSpec(type, emptyIndex, modelValue);
        };

        $scope.findValueInSelectedVariant = function(variantType, value){
            return this.view.findValueInSelectedVariant(variantType, value);
        };

        $scope.toggleTargetingSpec = function(variantType, value){

            $scope.addVariantIfNone(variantType);
            var pillValue = value;
            if(variantType === 'customAudiences' || variantType === 'excludedCustomAudiences'){
                pillValue = value.audienceName;
            }
            var pillIndex = $scope.view.getPillIndexInSelectedVariant(variantType, pillValue);
            var variant = $scope.view.getSelectedVariant(variantType);
            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
            if($scope.view.isValueInSelectedVariant(variantType, pillValue)){
                $scope.removeTargetingSpecFromVariant(variantType, variant, pillIndex);
                if(variantType === 'mobileOs' && $scope.supportedPlatforms.length > 1){
                    $scope.enableOsType(value);
                }
            } else {
                if(variantType === 'mobileOs'){
                    var modelValue = {};
                    modelValue.appId = $scope.appId;
                    modelValue.value = value;
                    if(value === 'Android'){

                        if(!$scope.androidAppInfo){
                            fsConfirm('genericAlert', {body:$filter('translate')('label.act.Notabletogetappinfomustbeadminordevofthisapp')});
                            $scope.disableOsType('Android');
                            $scope.disableOsType('iOS');
                            return;
                        }

                        modelValue.appStore = $scope.androidAppInfo.app_store_url; //jshint ignore:line
                    } else {


                        if(!$scope.iosAppInfo){
                            fsConfirm('genericAlert', {body:$filter('translate')('label.act.Notabletogetappinfomustbeadminordevofthisapp')});
                            $scope.disableOsType('Android');
                            $scope.disableOsType('iOS');
                            return;
                        }

                        modelValue.appStore = $scope.iosAppInfo.app_store_url; //jshint ignore:line
                    }
                    $scope.disableOsType(value);
                    $scope.view.addTargetingSpec('mobileOs', pillValue);
                    $scope.newAudience.addTargetingSpec('mobileOs', variantIndex, modelValue);
                    return;
                }
                $scope.addTargetingSpec(variantType, value);
            }
        };

        $scope.addVariantIfNone = function(variantType){
            if($scope.view[variantType].variants.length === 0){
                $scope.addVariant(variantType);
            }
        };

        $scope.addTargetingSpec = function(variantType, value){
            $scope.addVariantIfNone(variantType);
            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
            var modelValue = $scope.newAudience.getValueFromVariantType(variantType, value);
            var pillValue = value;
            if($scope.checkForDuplicates(variantType, variantIndex, value)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddtheduplicatevaluestothesamevariant')});
                return;
            }
            if(variantType === 'customAudiences' || variantType === 'excludedCustomAudiences'){
                pillValue = value.audienceName;
            }
            $scope.view.addTargetingSpec(variantType, pillValue);
            $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
        };

        $scope.generateViewFromAudienceData = function(data){
            var view = new FbAudienceView();
            if($scope.campaign.settings.goal.goal.name === 'MOBILE_APP_INSTALLS' ||
                $scope.campaign.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT'){
                view.mobileOs = {};
                view.mobileOs.variants = [];
            }
            for(var variantType in data){
                view[variantType].variants = data[variantType];
                for(var i = 0; i < data[variantType].length; i++){
                    view[variantType].variants[i].pills = [];
                    for(var j = 0; j < data[variantType][i].values.length; j++){
                        var pill = {};
                        // eventually we can use displayValue for location too (|| variantType === 'location'),
                        //but since it was just added for location (in fbAudience.service.js) wait for a while
                        //for customers who don't have this to cycle out.
                        if(variantType === 'education'){
                            pill.value = data[variantType][i].values[j].displayValue;
                        //in the mean time, repeat the logic here...
                        } else if(variantType === 'location' && data[variantType][i].values[j].distance){
                            //pill.value += ' + ' + data[variantType][i].values[j].distance + ' ' + data[variantType][i].values[j].distance_unit + 's'; //jshint ignore:line
                            pill.value = data[variantType][i].values[j].displayValue;
                        } else if(variantType  === 'connections'){
                            pill.value = data[variantType][i].values[j].displayType + ': ' + data[variantType][i].values[j].value;
                        } else {
                            pill.value = data[variantType][i].values[j].value;
                        }

                        if(variantType === 'customAudiences' || variantType === 'excludedCustomAudiences'){
                            pill.value = data[variantType][i].values[j].name;
                        }

                        view[variantType].variants[i].pills.push(pill);
                    }
                    delete view[variantType].variants[i].values;
                }
            }
            return view;
        };

        $scope.connectionMapping = {
            'connections': 'connections',
            'Include Users Connected To': 'connections',
            'Include Users Whose Friends are Connected To': 'friendsOfConnections',
            'Exclude Users Connected To': 'excludedConnections'
        };

        $scope.connectionDisplayMapping = {
            'connections': 'Include Users Connected To',
            'friendsOfConnections': 'Include Users Whose Friends are Connected To',
            'excludedConnections': 'Exclude Users Connected To'
        };

        $scope.addConnection = function(type, page){
            var connectionType = $scope.connectionMapping[type];
            $scope.addVariantIfNone('connections');
            var pv = {type: type, page: page};
            var mv = {type: connectionType, page: page, displayType: type};
            var modelValue = $scope.newAudience.getValueFromVariantType('connections', mv);
            var pillValue = $scope.view.getConnectionPillValue(pv);
            var variantIndex = $scope.view.getSelectedVariantIndex('connections');
            if($scope.checkForDuplicates('connections', variantIndex, pillValue)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddtheduplicatevaluestothesamevariant')});
                return;
            }

            if($scope.differentConnectionTypes(modelValue, variantIndex)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotadddifferentconnectiontypestothesamevariant')});
                return;
            }

            $scope.view.addTargetingSpec('connections', pillValue);
            $scope.newAudience.addTargetingSpec('connections', variantIndex, modelValue);
        };

        $scope.differentConnectionTypes = function(modelValue, variantIndex){
            if($scope.newAudience.audience.connections){
                var variant = $scope.newAudience.audience.connections[variantIndex];
                for(var i = 0; i < variant.values.length; i++){
                    if(variant.values[i].type !== modelValue.type){
                        return true;
                    }
                }
            }
        };

        $scope.checkForDuplicates = function(variantType, variantIndex, pillValue){
            var variant = $scope.view[variantType].variants[variantIndex];
            for(var i = 0; i < variant.pills.length; i++){
                if(variant.pills[i].value === pillValue){
                    return true;
                }
            }
            return false;
        };

        $scope.addDemographicToVariant = function(ev, value){
            $scope.addVariantIfNone('advancedDemographics');
            $scope.selectedDemographicValue = value.name;
            var variantType = 'advancedDemographics';
            var pillValue = $scope.demographicType + ': ' + value.name;
            var modelValue = $scope.newAudience.getValueFromVariantType(variantType, value);
            modelValue.displayValue = pillValue;
            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);

            if($scope.checkForDuplicates(variantType, variantIndex, pillValue)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddtheduplicatevaluestothesamevariant')});
                return;
            }

            $scope.view.addTargetingSpec(variantType, pillValue);
            $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
        };

        $scope.addAtpSuggestionToVariant = function(e, data){

            // if(data.triggeredBy === 'MANUAL'){
            //    return;
            // }
            var variantType = data.model;
            var pillValue = data.value.name;


            if(variantType === 'country' || variantType === 'region' || variantType === 'city' || variantType === 'zipcode'){
                if(variantType === 'city' && $scope.cityDistance.checked){
                    if(!$scope.cityDistance.value){
                        fsConfirm('genericAlert', {body:$filter('translate')('label.act.Mustentervalidradius')});
                        return;
                    }
                    data.distance = $scope.cityDistance.value;
                    //Change 'miles' or 'kilometers' to 'mile' or 'kilometer' because facebook is picky
                    var tempDistanceUnit = $scope.cityDistanceUnit;
                    if(tempDistanceUnit.substr(tempDistanceUnit.length - 1) === 's'){
                        tempDistanceUnit = $scope.cityDistanceUnit.slice(0,-1);
                    }
                    data.distance_unit = tempDistanceUnit; //jshint ignore:line
                    //for backwards compatibility, eventually the pillValue will just be the modelValue.displayValue
                    pillValue += ' + ' + data.distance + ' ' + $scope.cityDistanceUnit; //jshint ignore:line
                } else if (variantType === 'city' && !$scope.cityDistance.checked) {
                    delete data.distance;
                    delete data.distance_unit; //jshint ignore:line
                }

                variantType = 'location';
            }
            // If variant is education, don't add to variant, education is handled separately in controller
            if(data.model === 'educationFieldsUncommitted.school' || data.model === 'educationFieldsUncommitted.fieldOfStudy'){
                return;
            }

            $scope.addVariantIfNone(variantType);
            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
            var modelValue = $scope.newAudience.getValueFromVariantType(variantType, data);

            if(variantType === 'location'){
                //for backwards compatibility. Eventually, modelValue will always have a displayValue
                if (!modelValue.displayValue) {
                    modelValue.displayValue = pillValue;
                }

                var validLocation = $scope.validateLocation(variantIndex, data.model);
                if(!validLocation){
                    fsConfirm('generic', {
                        body: $filter('translate')('label.act.Youcannotadddifferentlocationtypesinthesamevariant'),
                        yes: $filter('translate')('label.act.Addtoanewvariant', {value:modelValue.value}),
                        no: $filter('translate')('label.act.Cancel')
                    }).then(function() {
                        // Add a variant if the user desires
                        $scope.addVariant(variantType);
                        // Retrieve updated variantIndex and modelValues because we added a new variant
                        variantIndex = $scope.view.getSelectedVariantIndex(variantType);
                        modelValue = $scope.newAudience.getValueFromVariantType(variantType, data);
                        // Save the model and the view
                        $scope.view.addTargetingSpec(variantType, pillValue);
                        $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
                        $timeout(function(){
                            angular.element('#' + data.model + '_value')[0].value = '';
                        }, 50);
                    });
                    return;
                }
            }
            if($scope.checkForDuplicates(variantType, variantIndex, pillValue)){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddtheduplicatevaluestothesamevariant')});
                $timeout(function(){
                    angular.element('#' + data.model + '_value')[0].value = '';
                }, 50);
                return;
            }

            $scope.view.addTargetingSpec(variantType, pillValue);
            $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);

            $timeout(function(){
                angular.element('#' + data.model + '_value')[0].value = '';
            }, 50);
        };

        $scope.validateLocation = function(variantIndex, type){
            var values = $scope.newAudience.audience.location[variantIndex].values;
            for(var i = 0; i < values.length; i++){
                if(values[i].type !== type){
                    return false;
                }
            }
            return true;
        };

        $scope.selectVariant = function(variantType, index){
            $scope.view.deselectVariants(variantType);
            $scope.view.selectVariant(variantType, index);
        };

        $scope.removeVariant = function(variantType, index){
            $scope.view.removeVariant(variantType, index);
            $scope.newAudience.removeVariant(variantType, index);
            if($scope.view[variantType].variants.length > 0){
                $scope.selectVariant(variantType, $scope.view[variantType].variants.length - 1);
            }
            if(variantType === 'preciseInterests') {
              $scope.getSuggestions(false);
            }
        };

        $scope.removeTargetingSpecFromVariant = function(variantType, variant, pillIndex){
            var variantIndex = _.findIndex($scope.view[variantType].variants, function(v){
                return angular.equals(v, variant);
            });
            $scope.view.removePillFromVariant(variant, pillIndex);
            $scope.newAudience.removeTargetingSpec(variantType, variantIndex, pillIndex);
        };

        //listen for autocomplete

        $scope.$on('ngAtp:autocomplete', $scope.addAtpSuggestionToVariant);

        $scope.stopEventProp = function(event){
            if (event.stopPropagation) {
                event.stopPropagation();
              }
              //just in case browser doesnt detect stopPropagation
              if (event.preventDefault) {
                event.preventDefault();
              }
        };

        $scope.focusInput = function(inputId){
            angular.element('#' + inputId + '_value').focus();
        };

        $scope.selectExistingVariants = function(){
          for(var variantType in $scope.view){
            if(typeof $scope.view[variantType] === 'object'){
                if($scope.view[variantType].variants.length > 0){
                  $scope.selectVariant(variantType, $scope.view[variantType].variants.length - 1);
                }
            }
          }
        };

        $scope.initMobileOsTargeting = function(){

            //find number of supported platforms
            var app =  $scope.campaign.settings.goal.application;
            var osType, modelValue, variantIndex;
            $scope.supportedPlatforms = [];
            $scope.appId = app.appId;
            for(var i = 0; i < app.channelApplications.length; i++){
                if (app.channelApplications[i].type === 'Facebook') {
                    for (var k = 0, l = app.channelApplications[i].nativeApplications.length; k < l; k++) {
                        if(app.channelApplications[i].nativeApplications[k].platform === 'ANDROID'){
                            $scope.supportedPlatforms.push('Android');
                            $scope.androidAppInfo = app.channelApplications[i].nativeApplications[k];
                        } else if (app.channelApplications[i].nativeApplications[k].platform === 'IOS'){
                            $scope.supportedPlatforms.push('iOS');
                            $scope.iosAppInfo = app.channelApplications[i].nativeApplications[k];
                        }
                    }
                }
            }
            if(!$scope.view.mobileOs){
                $scope.view.mobileOs = {};
                $scope.view.mobileOs.variants = [];
            }

            //if only one platform, add that platform as variant and disable other radio button, disable add variant
            if($scope.supportedPlatforms.length === 1){
                osType = $scope.supportedPlatforms[0];
                modelValue = {};
                modelValue.appId = app.appId;
                modelValue.value = osType;
                if(osType === 'Android'){
                    modelValue.appStore = $scope.androidAppInfo.app_store_url; //jshint ignore:line
                } else {
                    modelValue.appStore = $scope.iosAppInfo.app_store_url; //jshint ignore:line
                }

                $scope.addVariantIfNone('mobileOs');
                variantIndex = $scope.view.getSelectedVariantIndex('mobileOs');

                if(!$scope.view.isValueInSelectedVariant('mobileOs', osType)){
                    $scope.newAudience.addTargetingSpec('mobileOs', variantIndex, modelValue);
                    $scope.view.addTargetingSpec('mobileOs', osType);
                    $scope.disableOsType(osType);
                }
            }  else if($scope.supportedPlatforms.length === 2) {

                //add both to different variants
                for (var j = 0; j < $scope.supportedPlatforms.length; j++) {
                    osType = $scope.supportedPlatforms[j];
                    modelValue = {};
                    modelValue.appId = app.appId;
                    modelValue.value = osType;
                    if(osType === 'Android'){
                        modelValue.appStore = $scope.androidAppInfo.app_store_url; //jshint ignore:line
                    } else {
                        modelValue.appStore = $scope.iosAppInfo.app_store_url; //jshint ignore:line
                    }

                    $scope.addVariant('mobileOs');
                    variantIndex = $scope.view.getSelectedVariantIndex('mobileOs');
                    if(!$scope.view.isValueInSelectedVariant('mobileOs', osType)){
                        $scope.newAudience.addTargetingSpec('mobileOs', variantIndex, modelValue);
                        $scope.view.addTargetingSpec('mobileOs', osType);
                    }
                }

                // disable last os type
                $scope.disableOsType(osType);
            }
        };

        $scope.excludeConnectionsForSelectedPage = function(){
            if(!$scope.newAudience.audience.connections){
                $scope.addConnection('Exclude Users Connected To', $scope.campaign.settings.goal.page);
            }
        };

        // Manage - Save - Load Audience
        // Open FbAudience Segments modal to manage audience segments
        $scope.manageAudience = function (combo) {

            $scope.metadata.currentCombo = combo;
            // Retrieve current audience

            $scope.showInterests.check = false;

            $scope.newAudience = angular.copy(combo.audience);
            $scope.view = new FbAudienceView();
            $scope.ageRange = {value: '18;65'};
            $scope.fbAdAccountId = $scope.campaign.settings.channels.facebook.adAccount.accountId;
            customAudienceApi.getAllAcceptedCustomAudiences($scope.fbAdAccountId, $scope.fsAccountId, $scope.applyCustomAudiences, $scope.showError);
            customAudienceApi.getAllAcceptedLookalikeAudiences($scope.fbAdAccountId, $scope.fsAccountId, $scope.applyLookalikeAudiences, $scope.showError);



            // If there is no audience, instantiate it.
            if (!$scope.newAudience) {
                $scope.newAudience = new FbAudience();
                $scope.newAudience.audience = {};
                $scope.cityDistance = {value: '', checked:false};
            } else {
                $scope.view = $scope.generateViewFromAudienceData(angular.copy(combo.audience.audience));
                $scope.selectExistingVariants();
            }

            //if mobile app goal, init mobile os targeting
            if(($scope.campaign.settings.goal.goal.name === 'MOBILE_APP_INSTALLS' ||
                $scope.campaign.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT') &&
                !combo.audience){
                $scope.initMobileOsTargeting();
            }

            if($scope.campaign.settings.goal.goal.name === 'PAGE_LIKES'){
                $scope.excludeConnectionsForSelectedPage();
            }

            ngDialog.open({
                template: 'app/main/act/campaignCreation/facebook/fbAudience/templates/fbAudienceModal.html',
                className: 'modal modal-large',
                scope: $scope,
                closeByDocument: false
            });
        };

        $scope.saveAudience = function (saveBox) {
            var duplicateVariants = [];
            var isAudienceValid = true;

            var i = 0;
            var j = 0;

            // Verify at lease one location has been specified
            if (!$scope.newAudience.audience.location || $scope.newAudience.audience.location[0].values.length === 0){
                isAudienceValid = false;
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youneedtospecifyatleastonelocation')});
            }

            var category;


            // Remove empty variants
            if (isAudienceValid) {
                for (category in $scope.newAudience.audience) {
                    if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
                        for ( i = 0, j = $scope.newAudience.audience[category].length; i < j; i++) {
                            if ($scope.newAudience.audience[category][i].values.length === 0) {
                                $scope.newAudience.audience[category].splice(i, 1);
                                $scope.view[category].variants.splice(i,1);
                            }
                        }
                    }
                }
            }

            // Remove empty categories
            if (isAudienceValid) {
                for (category in $scope.newAudience.audience) {
                    if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
                       if($scope.newAudience.audience[category].length === 0){
                           delete $scope.newAudience.audience[category];
                        }
                    }
                }
            }

            if (isAudienceValid){
                // Check that there are no identical variants
                for (category in $scope.newAudience.audience) {
                    if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
                        for ( i = 0, j = $scope.newAudience.audience[category].length; i < j; i++) {
                            for (var k = i + 1; k < j; k++) {
                                if (angular.equals(_.sortBy($scope.newAudience.audience[category][i].values, 'value'), _.sortBy($scope.newAudience.audience[category][k].values, 'value'))) {
                                    duplicateVariants.push(category);
                                    // Exit the loop
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // Remove variants that are present more than once in the array
            duplicateVariants = _.uniq(duplicateVariants);

            var messages = [];

            // Verify that mobile OS info is present if necessary
            if (isAudienceValid && ($scope.campaign.settings.goal.goal.name === 'MOBILE_APP_INSTALLS' ||
                                    $scope.campaign.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT') &&
                                    !$scope.newAudience.audience.mobileOs) {
                isAudienceValid = false;
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youneedtoselectamobileoperatingsystem')});
            }
            if (duplicateVariants.length > 0) {
                for ( i = 0, j = duplicateVariants.length; i < j; i++) {
                    var translatedVariant = $filter('formatTargetingSpec')(duplicateVariants[i]);
                    messages.push(translatedVariant);
                }
                fsConfirm('multiLineAlert', {body:$filter('translate')('label.act.Youshouldnthavetwoidenticalvariantsin:'), messages:messages});
            } else if(isAudienceValid) {
                $scope.metadata.currentCombo.saveAudience($scope.newAudience);
                if(!saveBox) {
                  ngDialog.close();
                }
            }
        };

        $scope.radioBtn = {
          config: {
            valueField: 'name'
          },
          data: []
        };

        $scope.interests = [];

        $scope.preciseInterests = {};
        $scope.usernameDataType = 'searchBar';

        $scope.applyCopyPasteInterests = function(val){
          var untrimmedData;
          if (val){
            untrimmedData = val.split('\n');
          }

          // clean it up, remove white spaces
          $scope.interests = _.map(untrimmedData, function(datum){
            return datum.trim();
          });
          $scope.interests = $scope.interests.filter(function(q){ return q;});

          $scope.showInterests.check = false;

          // now let's only ask about variants they've added to their list, don't bother asking
          // about the ones we alread know about.
          var uniqueInterests = [];

          var activeVariant = $scope.view.getSelectedVariant('preciseInterests');

          _.forEach($scope.interests, function(value){
            var found = false;
            if(activeVariant && activeVariant.pills.length > 0){
              found = _.find(activeVariant.pills, function(name){
                return name.value === value;
              });
            }
            if (!found) {
              uniqueInterests.push(value);
            }
          });

          if(uniqueInterests.length){
            $scope.checkAndSubmitInterests(uniqueInterests);
          }

        };

        $scope.checkAndSubmitInterests = function(uniqueInterests){

          $scope.failedInterests = [];

          var submittedInterests = uniqueInterests.join(',');

          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/autocomplete/facebook/validateInterests?&interests=' + submittedInterests + '&accountId=' + $scope.fbAdAccountId
          })
            .success(function (response) {
              console.log(response);

              var returnedInterests = [];
              var invalidInterests = [];

              for(var i=0 ; i<response.data.length ; i++) {
                if(response.data[i].valid) {
                  returnedInterests.push(response.data[i]);
                } else {
                  invalidInterests.push(response.data[i].name);
                }

              }

              if(returnedInterests.length > 0) {
                $scope.addValidInterests(returnedInterests);
              }

              $scope.failedInterests = invalidInterests;

            })
            .error(function (error) {
              console.log(error);
            });
        };

        $scope.addValidInterests = function(interests) {
          var variantType = 'preciseInterests';

          _.forEach(interests, function(value){
            var pathName;
            if(value.path) {
              for(var i=0; i< value.path.length; i++) {
                if(!pathName) {
                  pathName = value.path[i];
                } else {
                  pathName = pathName + ' > ' + value.path[i];
                }
              }
            } else {
              pathName = value.name;
            }

            value.model = variantType;
            value.value = {
              name: pathName,
              audience_size: value.audience_size, //jshint ignore:line
              id: value.id
            };

            $scope.addAtpSuggestionToVariant('', value);
          });
        };

        $scope.getSuggestions = function(checked){
            $scope.showRadioList = false;
            $scope.loadingInterestRecs = true;

            if(checked) {
                if($scope.newAudience.audience.preciseInterests && $scope.newAudience.audience.preciseInterests[0].values.length !== 0){
                    $scope.noInterestsToSuggest = false;
                    var interests = _.map($scope.view.getSelectedVariant('preciseInterests').pills, function(interest){
                        return interest.value;
                    });

                    var params = {
                        accountId: $scope.fbAdAccountId,
                        suggestions: angular.toJson(interests)
                    };
                    $http({
                        method: 'GET',
                        url: apiUrl + '/api/v2/autocomplete/facebook/suggestions/adinterestsuggestion',
                        params: params
                    }).success(function(response){
                        $scope.loadingInterestRecs = false;
                        $scope.showRadioList = true;
                        $scope.radioBtn.data = response.data;
                    }).error(function() {
                        $scope.loadingInterestRecs = false;
                    });
                } else {
                    //no interests in variant
                    $scope.loadingInterestRecs = false;
                    $timeout(function(){
                      $scope.loadingInterestRecs = false;
                      $scope.noInterestsToSuggest = true;
                      $timeout(function(){
                        $scope.loadingInterestRecs = false;
                        $scope.noInterestsToSuggest = false;
                      }, 3000);
                    }, 0);
                    $scope.showInterests.check = false;
                }
            } else {
                $scope.showRadioList = false;
                $scope.loadingInterestRecs = false;
                $scope.showInterests.check = false;
            }
        };

        $scope.addSuggestionsToVariant = function(allValues){
            $scope.radioBtn.list = [];
            var variantType = 'preciseInterests';
            _.forEach(allValues, function(value){
                if (value.selected === true) {
                  if(!$scope.view.isValueInSelectedVariant(variantType, value.name)){
                    value.model = variantType;

                    value.value = {
                      name: value.name,
                      audience_size: value.audience_size, //jshint ignore:line
                      id: value.id
                    };

                    $scope.addAtpSuggestionToVariant('', value);
                  }
                }
            });
            $scope.showRadioList = false;
            $scope.getSuggestions(true);
        };



        $scope.loadAudienceFromLibrary = function () {

            // Open Library - Load audience modal view

            var opts = {
              channel: $scope.campaign.selectedChannel,
              currentComboId: $scope.metadata.currentCombo.id
            };

            library.openLibrary('Audiences', opts).then(function(template){
                if (template) {
                    // Now the backend persists the loaded audience in the database on load. So we simply reflect the change in the UI and close the modal to avoid having the user making edits then clicking on cancel.
                    // Doing that would lose the synchronization between the front end and the backend
                    $scope.metadata.currentCombo.audience = new FbAudience(template);
                    ngDialog.close();
                }
            });
        };

        // End Manage - Save - Load Audience

        $scope.selectDistanceUnit = function(unit){
            $scope.cityDistanceUnit = unit;
        };

// Behavior Tree Methods

        $scope.applyParentBehaviors = function(values){
            $timeout(function(){
                $scope.behaviorTree = values;
                $scope.$apply();
            });
        };

        $scope.expandNode = function(behavior){
            $scope.searchBehaviors(behavior);
        };

        $scope.collapseNode = function(behavior){
            behavior.nodes = null;
        };

        $scope.toggleNode = function(behavior){
            if(!behavior.nodes && behavior.guId){
                $scope.expandNode(behavior);
            }
            else{
                $scope.collapseNode(behavior);
            }
        };

        $scope.selectNode = function(variantType, value){
            $scope.addVariantIfNone(variantType);
            var variantIndex  = $scope.view.getSelectedVariantIndex(variantType);
            var pillIndex = $scope.view.getPillIndexInSelectedVariant(variantType, value.name);
            var variant = $scope.view.getSelectedVariant(variantType);
            var pillValue = $scope.view.getBehaviorPillValue(value);
            var modelValue = $scope.newAudience.getValueFromVariantType(variantType, value);

            if($scope.view.isValueInSelectedVariant(variantType, value.name)){
                $scope.removeTargetingSpecFromVariant(variantType, variant, pillIndex);
            } else {
                $scope.view.addTargetingSpec(variantType, pillValue);
                $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
            }

            if(value.guId){
                $scope.expandNodeAndSelectAll(value);
            }

        };

        $scope.searchBehaviors = function(behavior){
            $scope.behaviorHelper.getBehaviors($scope.fbAdAccountId, behavior, $scope);
        };

        // TODO: Check why this line triggers a failed call to broad categories
        $scope.behaviorHelper.getParentBehaviors($scope.fbAdAccountId, $scope.applyParentBehaviors);


        // Filter to sort audience variants
        $scope.audienceSort = function (variant) {
            var order = null;

            switch (variant.$key) {
                case 'location':
                    order = 0;
                    break;
                case 'language':
                    order = 10;
                    break;
                case 'ageRange':
                    order = 20;
                    break;
                case 'gender':
                    order = 30;
                    break;
                case 'audiencesToInclude':
                    order = 31;
                    break;
                case 'audiencesToExclude':
                    order = 32;
                    break;
                case 'interestedIn':
                    order = 40;
                    break;
                case 'relationshipStatus':
                    order = 50;
                    break;
                case 'connections':
                    order = 60;
                    break;
                case 'education':
                    order = 70;
                    break;
                case 'workEmployer':
                    order = 80;
                    break;
                case 'jobDescription':
                    order = 90;
                    break;
                case 'preciseInterests':
                    order = 100;
                    break;
                case 'advancedDemographics':
                    order = 110;
                    break;
                case 'behavior':
                    order = 120;
                    break;
            }

            return order;
        };

        // Caching the advanced demographics dropdown
        var cache;
        if (!$cacheFactory.info().advancedDemoCache){
         cache = $cacheFactory('advancedDemoCache');
        } else {
         cache = $cacheFactory.get('advancedDemoCache');
        }

        var cacheValues = function(key,data){
          cache.put(key,data);
        };

        var getValue = function(key){
          return cache.get(key);
        };

        $scope.selectDemographicType = function(value, label){
            $scope.demographicValues = [];
            $scope.demographicType = label;
            delete $scope.selectedDemographicValue;
            // retrieve the cache, if there is one
            var values = getValue(value);
            if(values){
                $scope.demographicValues = values;
            } else {
                $scope.loadingDemographics = true;
                // $scope.advancedDemographicsAtpConfig = atpHelper.getConfig('advancedDemographics', $scope.fbAdAccountId, type);
                $http({
                    method: 'GET',
                    url: apiUrl + '/api/v2/autocomplete/facebook/adTargetingCategory/'+ value + '?accountId=' + $scope.fbAdAccountId
                })
                .success(function(response){
                    $scope.demographicValues = response.data;
                    console.log($scope.demographicValues);
                    cacheValues(value, response.data);
                    $scope.loadingDemographics = false;
                });
            }
        };

        $scope.filterCustomAudiences = function(value){

            if($scope.view.isValueInSelectedVariant('customAudiences', value.audienceName)){
                return false;
            } else if($scope.view.isValueInSelectedVariant('excludedCustomAudiences', value.audienceName)){
                return false;
            } else {
                return value;
            }
        };

    });

