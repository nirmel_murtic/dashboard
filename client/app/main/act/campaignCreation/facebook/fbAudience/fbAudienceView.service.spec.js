'use strict';

describe('Service: FbAudienceView', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var FbAudienceView;
  beforeEach(inject(function (_FbAudienceView_){
    FbAudienceView = _FbAudienceView_;
  }));

  it('should have all variant types', function(){
    var view = new FbAudienceView();
    expect(view.location).toBeDefined();
    expect(view.language).toBeDefined();
    expect(view.gender).toBeDefined();
    expect(view.ageRange).toBeDefined();
    expect(view.customAudiences).toBeDefined();
    expect(view.excludedCustomAudiences).toBeDefined();
    expect(view.interestedIn).toBeDefined();
    expect(view.relationshipStatus).toBeDefined();
    expect(view.connections).toBeDefined();
    expect(view.education).toBeDefined();
    expect(view.workEmployer).toBeDefined();
    expect(view.workPosition).toBeDefined();
    expect(view.preciseInterests).toBeDefined();
    expect(view.advancedDemographics).toBeDefined();
    expect(view.behavior).toBeDefined();
  });

  it('should add variant from variantType', function(){
    var view = new FbAudienceView();
    var variantType = 'location';
    view.addVariant(variantType);
    expect(view[variantType].variants.length).toBe(1);
  });

  it('should add targeting spec pill to variant from value string', function(){
    var view = new FbAudienceView();
    var variantType = 'location';
    var targetingSpec = 'CA';
    view.addVariant(variantType);
    view.addTargetingSpec(variantType, targetingSpec);
    expect(view[variantType].variants[0].pills.length).toBe(1);
  });

  it('should remove pill from variant from variant, pillIndex', function(){
    var view = new FbAudienceView();
    var variantType = 'location';
    var targetingSpec = 'CA';
    view.addVariant(variantType);
    view.addTargetingSpec(variantType, targetingSpec);
    view.removePillFromVariant(view[variantType].variants[0], 0);
    expect(view[variantType].variants[0].pills.length).toBe(0);
  });

  it('should remove variant with variantType, variantIndex', function(){
    var view = new FbAudienceView();
    var variantType = 'location';
    view.addVariant(variantType);
    view.removeVariant(variantType, 0);
    expect(view[variantType].variants.length).toBe(0);
  });

});