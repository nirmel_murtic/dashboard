'use strict';

angular.module('act')
    .factory('FbAudienceView', function () {
    	var FbAudienceView = function(){
            //init variantTypes
            this.location = {};
            this.language = {};
            this.gender = {};
            this.ageRange = {};
            this.customAudiences = {};
            this.excludedCustomAudiences = {};
            this.interestedIn = {};
            this.relationshipStatus = {};
            this.connections = {};
            this.education = {};
            this.workEmployer = {};
            this.workPosition = {};
            this.preciseInterests = {};
            this.advancedDemographics = {};
            this.behavior = {};

            //init variants
            this.location.variants = [];
            this.language.variants = [];
            this.gender.variants = [];
            this.ageRange.variants = [];
            this.customAudiences.variants = [];
            this.excludedCustomAudiences.variants = [];
            this.interestedIn.variants = [];
            this.relationshipStatus.variants = [];
            this.connections.variants = [];
            this.education.variants = [];
            this.workEmployer.variants = [];
            this.workPosition.variants = [];
            this.preciseInterests.variants = [];
            this.advancedDemographics.variants = [];
            this.behavior.variants = [];
    	};

    	FbAudienceView.prototype = {
            addVariant: function(variantType){
                var variant = {};
                variant.pills = [];
                variant.selected = true;
                this.deselectVariants(variantType);
                this[variantType].variants.push(variant);
            },

            removeVariant: function(variantType, index){
                this[variantType].variants.splice(index, 1);
            },

            getSelectedVariant: function(variantType){
                var variants = this[variantType].variants;
                for(var i =0; i < variants.length; i++){
                    if(variants[i].selected){
                        return variants[i];
                    }
                }
            },
            getPillIndexInSelectedVariant: function(variantType, value){
                var selectedVariant = this.getSelectedVariant(variantType);
                if(selectedVariant){
                    for(var i = 0; i < selectedVariant.pills.length; i++ ){
                        if(selectedVariant.pills[i].value === value){
                            return i;
                        }
                    }
                }
            },
            getSelectedVariantIndex: function(variantType){
                var variants = this[variantType].variants;
                for(var i =0; i < variants.length; i++){
                    if(variants[i].selected){
                        return i;
                    }
                }
            },
            getNotSelectedEmptyVariantIndex: function(variantType){
              var variants = this[variantType].variants;
              for(var i =0; i < variants.length; i++){
                if(variants[i].pills.length < 1){
                  return i;
                }
              }
            },
            isValueInSelectedVariant: function(variantType, value){
                var selectedVariant = this.getSelectedVariant(variantType);
                if(selectedVariant){
                    for(var i = 0; i < selectedVariant.pills.length; i++ ){
                        if(selectedVariant.pills[i].value === value){
                            return true;
                        }
                    }
                }
            },
            addTargetingSpec: function(variantType, value){
                var variants = this[variantType].variants;
                var pill = {};
                pill.value = value;
                for(var i = 0; i < variants.length; i++){
                    if(variants[i].selected){
                        variants[i].pills.push(pill);
                    }
                }
            },

            selectVariant: function(variantType, index){
                this[variantType].variants[index].selected = true;
            },

            deselectVariants: function(variantType){
                var variants = this[variantType].variants;
                if(variants){
                    for(var i = 0; i < variants.length; i++){
                        variants[i].selected = false;
                    }
                }
            },
            removePillFromVariant: function(variant, pillIndex){
                variant.pills.splice(pillIndex, 1);
            },
            getEducationPillValue: function(type, data){
                var pillValue = type;
                if(data.school){
                    pillValue += ' | ' + data.school.name || data.school.value;
                }
                if(data.fieldOfStudy){
                    pillValue += ' | ' + data.fieldOfStudy.name || data.fieldOfStudy.value;
                }

                if(data.minGradYear){
                    pillValue += ' | Min Grad Year: ' + data.minGradYear;
                }

                if(data.maxGradYear){
                    pillValue += ' | Max Grad Year: ' + data.maxGradYear;
                }
                return pillValue;
            },
            getAgeRangePillValue: function(ageRange){
                return ageRange.min + ' - ' + ageRange.max;
            },
            getConnectionPillValue: function(value){
                return value.type + ': ' + value.page.pageName;
            },
            getBehaviorPillValue: function(behavior){
                // return behavior.path
                return behavior.name;
            }
    	};

    	return FbAudienceView;

    });
