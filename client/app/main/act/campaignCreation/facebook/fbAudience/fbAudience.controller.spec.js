'use strict';

describe('Controller: FbAudienceCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var FbAudienceCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    
    $rootScope.fractalAccountInfo = {};
    $rootScope.fractalAccountInfo.currentTeam = {};
    $rootScope.fractalAccountInfo.currentTeam.id = 0;
    // $scope.api = {};
    // $scope.api.facebookPages = [];
    scope = $rootScope.$new();
    FbAudienceCtrl = $controller('FbAudienceCtrl', {
      $scope: scope = {
        campaign: {
          settings: {
            channels:{
              facebook:{
                adAccount: {
                  accountId: 0
                }
              }
            }
          },
          api: {
            facebook: {
              pages: []
            }
          }
        }, 
        $on: function(){}
      }
    });
  }));

  it('should add variant to model and view from variant type', function(){
    var variantType = 'location';
    scope.addVariant(variantType);
    expect(scope.view[variantType].variants[0]).toBeDefined();
    expect(scope.newAudience.audience[variantType][0]).toBeDefined();
  });

  it('should add autocomplete value to model and view from variant type and value', function(){
    var variantType = 'location';
    var countryAtpValue = {
                  model: 'country', 
                  value: {
                    name: 'United States', 
                    country_code: 'US' //jshint ignore:line
                  }
                }; 
    scope.addVariant(variantType);
    scope.addAtpSuggestionToVariant(variantType, countryAtpValue);
    expect(scope.view[variantType].variants[0].pills[0].value).toBe('United States');
    expect(scope.newAudience.audience[variantType][0].values[0].value).toBe('United States');
  });

  it('should add targeting spec to model and view from string value', function(){
    var variantType = 'gender';
    var value = 'Men';
    scope.addTargetingSpec(variantType, value);
    expect(scope.view[variantType].variants[0].pills[0].value).toBe('Men');
    expect(scope.newAudience.audience[variantType][0].values[0].value).toBe('Men');
  });

  it('should remove variant from model and view from variantType and index', function(){
    var variantType = 'location';
    scope.addVariant(variantType);
    scope.removeVariant(variantType, 0);
    expect(scope.view[variantType].variants.length).toBe(0);
    expect(scope.newAudience.audience[variantType]).toBeUndefined();  
  });

  it('should remove targeting spec from model and view from variantType, variant, pillIndex', function(){
    //after reviewing code, this method should be refactored and then test should be written again. 
    //We can do everything with variantIndex. We dont need to pass the whole variant
    var variantType = 'gender';
    var value = 'Men';
    scope.addTargetingSpec(variantType, value);
    scope.removeTargetingSpecFromVariant(variantType, scope.view[variantType].variants[0], 0);
    expect(scope.view[variantType].variants[0].pills.length).toBe(0);
    expect(scope.newAudience.audience[variantType][0].values.length).toBe(0);

  });

});
