'use strict';

angular.module('main')
  .service('BehaviorHelper', function ($http, $timeout, apiUrl, $cacheFactory) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var cache = $cacheFactory('fbBehaviorCache');

    var cacheValues = function(key,data){
      cache.put(key,data);
    };

    var getValue = function(key){
      return cache.get(key);
    };

    var behaviorHelper = function(){ 

    };

    behaviorHelper.prototype.getParentBehaviors = function(accountId, callback){
      var values = getValue('parentBehaviors');
      if(values){
        callback(values);
      } else {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/broadCategories?accountId=' + accountId
        }).success(function(result){
          callback(result.data);        
          cacheValues('parentBehaviors', result.data);
        });        
      }
    };

    behaviorHelper.prototype.getBehaviors = function(accountId, behavior, scope){
      var value = getValue(behavior.guId);
      if(value){
        $timeout(function(){
            behavior.nodes = value;                        
            scope.$apply();                 
         });
      } else {        
      	$http({
    			method: 'GET',
    			url: apiUrl + '/api/v2/autocomplete/broadCategories?accountId=' + accountId + '&parent=' + behavior.guId
    		}).success(function(result){
          cacheValues(behavior.guId, result.data);
          $timeout(function(){
              behavior.nodes = result.data;                        
              scope.$apply();        
           });
        });
      }

	  };

    return behaviorHelper;
  });