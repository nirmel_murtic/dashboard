'use strict';

angular.module('main')
  .service('FbAtpHelper', function (apiUrl) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    
    var FbAtpHelper = function(){};

    FbAtpHelper.prototype.getConfig = function(variantType, accountId){
        var defaultConfig =  {
          remote: {                     
            filter : function (res) {
              return res.data;
            }
          },
          verify : function(datum) {
            return datum && _.has(datum, 'name');
          },
          format : function(datum) {
            return datum ? datum.name : '';   
          },
          limit: 25,
          idAttribute : 'key'
        };

        if(variantType === 'country'){                  
            // defaultConfig.remote.url = 'https://graph.facebook.com/search?type=adgeolocation&location_types=country&q=%QUERY';
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adcountry?&accountId=' + accountId + '&q=%QUERY';
            defaultConfig.idAttribute = 'name';
            //defaultConfig.local = some Angular constant with the 20 most popular countries for instant responsiveness for 80% of users.
            // very careful to keep this up to date with Facebook's structure...
        } else if(variantType === 'region'){                            
            // defaultConfig.remote.url = 'https://graph.facebook.com/search?type=adgeolocation&location_types=region&q=%QUERY';
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adregion?&accountId=' + accountId + '&q=%QUERY';
        } else if(variantType === 'city'){                            
            // defaultConfig.remote.url = 'https://graph.facebook.com/search?type=adgeolocation&location_types=city&q=%QUERY';
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adcity?&accountId=' + accountId + '&q=%QUERY';
        } else if(variantType === 'zipcode'){  
         // defaultConfig.remote.url = 'https://graph.facebook.com/search?type=adgeolocation&location_types=zip&q=%QUERY';                          
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adzipcode?&accountId=' + accountId + '&q=%QUERY';
            defaultConfig.idAttribute = 'name';
        } else if(variantType === 'language'){
            // defaultConfig.remote.url = 'https://graph.facebook.com/search?type=adlocale&q=%QUERY';
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adlocale?&accountId=' + accountId + '&q=%QUERY';
        } else if(variantType === 'school'){
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adeducationschool?&accountId=' + accountId + '&q=%QUERY';
            defaultConfig.idAttribute = 'id';
        } else if(variantType === 'fieldOfStudy'){
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adeducationmajor?&accountId=' + accountId + '&q=%QUERY';
            defaultConfig.idAttribute = 'id';
        } else if(variantType === 'workEmployer') {
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adworkemployer?&q=%QUERY&accountId=' + accountId;
            defaultConfig.idAttribute = 'id';
        } else if(variantType === 'workPosition') {
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adworkposition?&q=%QUERY&accountId=' + accountId;
            defaultConfig.idAttribute = 'id';
        } else if(variantType === 'preciseInterests') {
            defaultConfig.remote.url = '/api/v2/autocomplete/facebook/adinterest?&q=%QUERY&accountId=' + accountId;
            defaultConfig.idAttribute = 'id';
        } else if(variantType === 'advancedDemographics') {
            defaultConfig.prefetch = {
                url: apiUrl + '/api/v2/autocomplete/facebook/adTargetingCategory/demographics?&accountId=' + accountId,
                transform: function(x){
                    return x.data || [];
                }
            };
            defaultConfig.remote = null;
            defaultConfig.idAttribute = 'id';
        }

        if (defaultConfig.remote) {
            defaultConfig.remote.url = apiUrl + defaultConfig.remote.url;           
        }

        return defaultConfig;
    };


    FbAtpHelper.prototype.returnPillConfig = function(variantType, fbAutocompleteData){
        var pillConfig = {};
        pillConfig.value = fbAutocompleteData.value.name;
        pillConfig.type = variantType;
        return pillConfig;
    };

    return FbAtpHelper;

  });
