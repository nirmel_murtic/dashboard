'use strict';

describe('Service: FbCreative', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var FbCreative;
  beforeEach(inject(function (_FbCreative_) {
    FbCreative = _FbCreative_;
  }));

  it('should do something', function () {
    expect(!!FbCreative).toBe(true);
  });

});
