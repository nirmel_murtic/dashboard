'use strict';

describe('Controller: FbCreativeCtrl', function () {

  // load the controller's module
  beforeEach(module('dashboardApp'));

  var FbCreativeCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FbCreativeCtrl = $controller('FbCreativeCtrl', {
      $scope: scope
    });
  }));
});
