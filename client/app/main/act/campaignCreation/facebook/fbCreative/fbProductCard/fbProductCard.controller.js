'use strict';

angular.module('act')
  .controller('FbproductcardCtrl', function ($scope, library) {

    $scope.loadImageFromLibrary = function (productCard) {
        //Remember which product card needs an image
        $scope.metadata.productCard = productCard;
        var opts = {
            currentCombo: $scope.metadata.currentCombo,
            aspectRatio: 1,
            minSize: {width:458, height:458, unit:'px'}
        };

        // Open Library - Load Image modal view
        library.openLibrary('Images', opts).then(function(content){
            if (content) {
                $scope.metadata.productCard.image  = content[0].fullGuid;
                $scope.metadata.productCard = null;
            }
            delete $scope.metadata.productCard;
        });
    };

    $scope.removeImage = function (productCard)  {
        productCard.image = null;
    };

  });
