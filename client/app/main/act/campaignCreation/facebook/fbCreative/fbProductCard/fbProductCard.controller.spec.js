'use strict';

describe('Controller: FbproductcardCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var FbproductcardCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FbproductcardCtrl = $controller('FbproductcardCtrl', {
      $scope: scope
    });
  }));

});
