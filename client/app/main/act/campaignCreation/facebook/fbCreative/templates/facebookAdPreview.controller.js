'use strict';

angular.module('act')
.controller('FacebookAdPreviewCtrl', ['$scope', 'FacebookAdPreview', 
  function ($scope, FacebookAdPreview) {
    // var loadingHtml = '<span>Now Loading...</span>'; //TO-DO: move this to seperate template file
    // var errorHtml = '<span>Error</span>';

    var adPreviewGenerator = FacebookAdPreview.preview($scope.creative, {
      width: 450,
      height: 540
    });
    $scope.adPreviewGenerator = adPreviewGenerator;

    function updatePreview() {
      /* initialing loading html for previews
      var previewPromises = adPreviewGenerator.requestAdPreviews();
      $scope.previewHtmls = previewPromises.map(function() {
        return { status: 'loading', html: loadingHtml }; 
      });
      previewPromises.forEach(function(promise, index) {
        promise.then(function(res) {
          $scope.previewHtmls[promise.__index__] = { status: 'done', html: res }; 
        }, function (err) {
          $scope.previewHtmls[promise.__index__] = { status: 'error', html: errorHtml }; 
        });
      });
      */
    }

    updatePreview();
    $scope.$watch('creative', updatePreview, true);

  }]);
