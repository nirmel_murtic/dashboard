'use strict';

angular.module('act')
  .controller('FbCreativeCtrl', function ($scope, FbCreative, ngDialog, regex, urlValidator, fsConfirm, $filter, microService, modelOptions, $timeout, library) {

    $scope.textRegex = regex.TEXTWITHOUTDOUBLEPUNCTUATION;
    $scope.shortUrlRegex = regex.DIEGOSHORTURL.source;
    $scope.adTypeFilter = {type: '!video'};

    // Needs to be declared at the root of the file because it's being accessed in multiple methods
    var creativesByAdTypeCollection = {}, instagramCreativesByAdTypeCollection = {}, fbPostsFilterType;

    // Reset the last selected Facebook Page if necessary
    FbCreative.lastSelectedFacebookPage = $scope.campaign.api.facebook.pages.indexOf(FbCreative.lastSelectedFacebookPage) > -1 ? FbCreative.lastSelectedFacebookPage : null;
    // Reset the last selected Instagram Account if necessary
    try {
      FbCreative.lastSelectedInstagramAccount = $scope.campaign.settings.channels.facebook.adAccount.instagramAccounts.indexOf(FbCreative.lastSelectedInstagramAccount) > -1 ? FbCreative.lastSelectedInstagramAccount : null;
    } catch (e){
      FbCreative.lastSelectedInstagramAccount = null;
    }

    $scope.newOrExistingPostRadioToggle = {
      option1: {
        label: 'New Post',
        value: 'new'
      },
      option2: {
        label: 'Existing Post',
        value: 'existing'
      }
    };

    $scope.placementRadioToggle = {
      option1: {
        label: 'Newsfeed',
        value: 'newsfeed'
      },
      option2: {
        label: 'Right Column',
        value: 'rightColumn'
      }
    };

    $scope.creativeChannelsRadioToggle = {
      option1: {
        label: 'Facebook',
        value: 'facebook'
      },
      option2: {
        label: 'Instagram',
        value: 'instagram'
      }
    };

    $scope.callsToActionList =[];

    $scope.metadata.displayWaitMessage = 0;

    var goal = $scope.campaign.settings.goal;
    var goalType = $scope.campaign.settings.goal.goal.name;
    var goalUrl = $scope.campaign.settings.goal.eventUrl ? $scope.campaign.settings.goal.eventUrl : $scope.campaign.settings.goal.host;

    function setRequiredFields() {
      $scope.isRequired = {
        text: ($scope.campaign.settings.goal.goal.name !== 'MOBILE_APP_INSTALLS' &&
               $scope.campaign.settings.goal.goal.name !== 'MOBILE_APP_ENGAGEMENT' &&
               $scope.campaign.settings.goal.goal.name !== 'VIDEO_VIEWS' &&
               $scope.newCreative.creative.adType !== 'videoAd'),
        appUrl: (true),
        headline: ($scope.campaign.settings.goal.goal.name !== 'MOBILE_APP_INSTALLS' &&
                   $scope.campaign.settings.goal.goal.name !== 'MOBILE_APP_ENGAGEMENT' &&
                   $scope.newCreative.creative.adType !== 'productCard' &&
                   $scope.newCreative.creative.adType !== 'videoAd'),
        url: ($scope.newCreative.creative.adType !== 'videoAd' || $scope.campaign.settings.goal.goal.name === 'WEBSITE_CLICKS'),
        callToActionURL: ($scope.newCreative.creative.callToAction)//put a watcher on
      };
    }

    $scope.$watch('newCreative.creative.callToAction', function (newValue) {
      if (!!newValue) {
        $scope.isRequired.callToActionURL = true;
      }
    });

    // Open FbCreative Modal to add creatives
    $scope.addCreative = function (combo) {
      $scope.urlModelOptions = modelOptions.newInstance().creativeUrl;
      // Declare the new creative object that will be used in the creative modal view
      $scope.newCreative = new FbCreative();
      $scope.stopLoading = false;

      setRequiredFields();
      $scope.currentCreative = null;
      $scope.metadata.currentCombo = combo;
      $scope.newCreative.adAccountId = $scope.campaign.settings.channels.facebook.adAccount.accountId;
      $scope.newCreative.$$facebookPosts = [];
      $scope.metadata.creativeChannelTargeted = 'facebook';

      fetchCallsToAction();
      if (FbCreative.lastSelectedFacebookPage) {
        $scope.selectFacebookPageForCreative(FbCreative.lastSelectedFacebookPage);
      }
      configureNewCreativeForGoal($scope.newCreative, goal);

      // Initialize the object
      creativesByAdTypeCollection = {};
      instagramCreativesByAdTypeCollection = {};
      // Start with Facebook by default
      creativesByAdTypeCollection[$scope.newCreative.creative.adType] = $scope.newCreative;

      ngDialog.open({
        template: 'app/main/act/campaignCreation/facebook/fbCreative/templates/fbCreativeModal.html',
        className: 'modal',
        scope: $scope
      });
    };

    function configureNewCreativeForGoal (newCreative, goal) {
      switch (goal.goal.name) {
        case 'MOBILE_APP_ENGAGEMENT':
        case 'MOBILE_APP_INSTALLS':
          newCreative.platform = [
            'mobile'
          ];
          newCreative.creative.facebookAppId = goal.appId;
          break;
        case 'POST_ENGAGEMENT':
        case 'PAGE_LIKES':
          newCreative.platform = [
            'desktop', 'mobile'
          ];
          $scope.selectFacebookPageForCreative(goal.page);
          break;
        case 'WEBSITE_CLICKS':
        case 'WEBSITE_CONVERSIONS':
          newCreative.platform = [
            'desktop', 'mobile'
          ];
          break;
        case 'VIDEO_VIEWS':
          newCreative.platform = [
            'desktop', 'mobile'
          ];
          $scope.adTypeFilter = {type: 'video'};
          $scope.selectFacebookPageForCreative(goal.page);
          break;
      }
    }

    function fetchCallsToAction() {
      $scope.callsToActionList = [];
      switch (goalType) {
        case 'MOBILE_APP_ENGAGEMENT':
          addToCallsToActionList([
            '',
            'BUY_NOW', 
            'GET_OFFER',
            'OPEN_LINK']); 
          /* falls through */
        case 'MOBILE_APP_INSTALLS':
          addToCallsToActionList([
            '',
            'PLAY_GAME', 
            'USE_APP', 
            'SHOP_NOW',
            'LISTEN_MUSIC',
            'WATCH_VIDEO',
            'WATCH_MORE',
            'DOWNLOAD',
            'LEARN_MORE',
            'SIGN_UP'
            ]);
          break;
        case 'WEBSITE_CLICKS':
          addToCallsToActionList(['', 'CONTACT_US']);
          /* falls through */
        case 'WEBSITE_CONVERSIONS':
          addToCallsToActionList([
            '',
            'SHOP_NOW',
            'LEARN_MORE',
            'SIGN_UP',
            'DOWNLOAD',
            'WATCH_MORE',
            'APPLY_NOW',
            'DONATE_NOW',
            'BOOK_TRAVEL',
            //not in the documentation, but available.
            'OPEN_LINK',
            'LISTEN_MUSIC',
            'WATCH_VIDEO',
            'MESSAGE_PAGE',
            'SUBSCRIBE',
            'GET_QUOTE',
            'BUY_NOW',
            'GET_OFFER',
            'PLAY_GAME',
            'USE_APP',
            //Only in one error message from fb
            // 'INSTALL_MOBILE_APP',
            // 'USE_MOBILE_APP',
            // 'FRIENDS_PHOTO',
            // 'MOBILE_DOWNLOAD',
            // 'BUY_TICKETS',
            // 'UPDATE_APP',
            // 'BET_NOW',
            // 'ADD_TO_CART',
            // 'ORDER_NOW',
            // 'SELL_NOW',
            // 'CALL',
            // 'MISSED_CALL',
            // 'CALL_ME',
            // 'BUY',
            // 'VIDEO_ANNOTATION',
          ]);
          if ($scope.newCreative.creative.adType !== 'videoAd') {
            addToCallsToActionList(['']);
          }
          break;
        case 'POST_ENGAGEMENT':
        case 'VIDEO_VIEWS':
          addToCallsToActionList([
            '',
            'SHOP_NOW',
            'LEARN_MORE',
            'SIGN_UP',
            'DOWNLOAD',
            'WATCH_MORE',
            'BOOK_TRAVEL'
          ]);
          break;
      }
    }

    // Open FbCreative Modal to add creatives
    $scope.editCreative = function (combo, creative) {
      $scope.urlModelOptions = modelOptions.newInstance().creativeUrl;
      $scope.metadata.currentCombo = combo;
      $scope.currentCreative = creative;
      $scope.newCreative = angular.copy(creative);
      $scope.stopLoading = false;
      fetchCallsToAction();
      setRequiredFields();

      // Initialize the object
      creativesByAdTypeCollection = {};
      instagramCreativesByAdTypeCollection = {};
      if ($scope.newCreative.creative.pageTypes === 'instagramstream') {
        // No need for backward compatibility as we don't have any instagram creative without ad type
        instagramCreativesByAdTypeCollection[$scope.newCreative.creative.adType] = $scope.newCreative;
      } else {
        // Added for backward compatibility for old creative created before multi product ads were implemented
        if (!$scope.newCreative.creative.adType) {
          $scope.newCreative.creative.adType = 'pagePost';
          creativesByAdTypeCollection.pagePost = $scope.newCreative;
        } else {
          creativesByAdTypeCollection[$scope.newCreative.creative.adType] = $scope.newCreative;
        }
      }


      // Assign index to each product card if they don't have one (backward compatibility)
      if ($scope.newCreative.creative.adType === 'productCard'){
        if (!$scope.newCreative.creative.multiProductCards[0].index) {
          for (var i = 0, j = $scope.newCreative.creative.multiProductCards.length; i < j; i++) {
            $scope.newCreative.creative.multiProductCards[i].index = i;
          }
        }
      }

      if ($scope.newCreative.facebookPage) {
        $scope.selectFacebookPageForCreative($scope.newCreative.facebookPage);
      }

      //if user has existing post, find existing post
      if ($scope.newCreative.creative.facebookSponsoredObjectId) {
        var post = findFacebookPost();
        $scope.selectPostForCreative(post);
      }

      $scope.metadata.creativeChannelTargeted = $scope.newCreative.creative.pageTypes === 'instagramstream' ? 'instagram' : 'facebook';

      ngDialog.open({
        template: 'app/main/act/campaignCreation/facebook/fbCreative/templates/fbCreativeModal.html',
        className: 'modal',
        scope: $scope
      });

    };

    function findFacebookPost() {
      return _.find($scope.newCreative.$$facebookPosts, function (post) {
        if (post.feedId === $scope.newCreative.creative.facebookSponsoredObjectId) {
          return post;
        }
      });
    }

    $scope.saveCreative = function (creativeForm, saveBox) {

      $scope.newCreative.isValid = true;
      creativeForm.submitted = true;

      // Validate creative
      switch ($scope.newCreative.creative.adType) {
        case 'pagePost':
          validatePagePostAd(creativeForm);
          break;
        case 'productCard':
          validateProductCardAd(creativeForm);
          break;
        case 'videoAd':
          validateVideoAd(creativeForm);
          break;
        default:
          console.log('Unexpected Ad Type: ', $scope.newCreative.creative.adType);
      }
      if(saveBox) {
        $scope.newCreative.saveToLibrary($scope.campaign.selectedChannel.toUpperCase());
      }

      if ($scope.newCreative.isValid) {
        assignPageTypeToNewCreative();
        saveCreative();
        creativeForm.submitted = false;
        if(!saveBox){
          ngDialog.close();            
        }
      } else { // If the creative is not valid, ask the user if he's sure about saving it
        fsConfirm('generic', {
          body: $filter('translate')('label.act.creativeInvalidMessage')
        }).then(function () {
          assignPageTypeToNewCreative();
          creativeForm.submitted = false;
          saveCreative();
          if(!saveBox){
            ngDialog.close();            
          }
        });
      }
    };

    function saveCreative() {
      if ($scope.currentCreative) { // If we are editing a current creative, assign the content of newCreative to currentCreative
        _.assign($scope.currentCreative, $scope.newCreative);
        $scope.metadata.currentCombo.saveCreative($scope.currentCreative);
      } else {
        $scope.metadata.currentCombo.saveCreative($scope.newCreative);
      }
    }

    function assignPageTypeToNewCreative() {
      switch ($scope.newCreative.placement) {
        case 'rightColumn':
          $scope.newCreative.creative.pageTypes = 'rightcolumn';
          break;
        case 'mobile':
          $scope.newCreative.creative.pageTypes = 'mobile';
          break;
        case 'newsfeed':
          // If Instagram, do nothing
          if ($scope.newCreative.creative.pageTypes !== 'instagramstream') {
            // For both mobile and desktop
            if ($scope.newCreative.platform.length === 2) {
              $scope.newCreative.creative.pageTypes = 'feed';
            } else if (_.find($scope.newCreative.platform, function (x) {
                return x === 'mobile';
              })) {
              $scope.newCreative.creative.pageTypes = 'mobile';
            } else if (_.find($scope.newCreative.platform, function (x) {
                return x === 'desktop';
              })) {
              $scope.newCreative.creative.pageTypes = 'desktopfeed';
            }
          }
          break;
      }
    }

    function validatePagePostAd(creativeForm) {
      // Image is required for most goals
      $scope.newCreative.isImageRequired = (goalType === 'MOBILE_APP_INSTALLS' ||
        goalType === 'MOBILE_APP_ENGAGEMENT' ||
        goalType === 'PAGE_LIKES' ||
        ((goalType === 'WEBSITE_CLICKS' || goalType === 'WEBSITE_CONVERSIONS') && $scope.newCreative.creative.adType === 'pagePost' && $scope.newCreative.creative.post !== 'existing')
      );

      if ( ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') && (goalType === 'WEBSITE_CONVERSIONS' || goalType === 'WEBSITE_CLICKS') && $scope.newCreative.creative.post !== 'existing' ) {
        $scope.newCreative.isValid = !$scope.validateUrlForPagePostCreative();

        $scope.newCreative.isValid = $scope.newCreative.isValid && ($scope.newCreative.trackingScriptStateInUrl === 'pass' || $scope.newCreative.trackingScriptStateInUrl === 'unknown');
      }
      // When boosting an existing post, make sure one facebook page and one facebook post are selected
      if ((goalType === 'POST_ENGAGEMENT' || goalType === 'WEBSITE_CLICKS' || goalType === 'WEBSITE_CONVERSIONS' || goalType === 'PAGE_LIKES' || goalType === 'VIDEO_VIEWS') && $scope.newCreative.creative.post === 'existing' && $scope.newCreative.isValid) {
        if ($scope.newCreative.creative.facebookSponsoredPageId === null || $scope.newCreative.creative.facebookSponsoredObjectId === null || !$scope.newCreative.creative.facebookSponsoredObjectId) {
          $scope.newCreative.isValid = false;
        }
      }
      if ($scope.newCreative.placement === 'newsfeed') {
        if ($scope.newCreative.creative.facebookSponsoredPageId === null) { // For newsfeed, make sure a facebook page is selected
          $scope.newCreative.isValid = false;
        }
        if ($scope.newCreative.platform && $scope.newCreative.platform.length === 0) { // Check for platform
          $scope.newCreative.isValid = false;
        }

        // Remove post object reference for new posts
        if (goalType === 'POST_ENGAGEMENT' && $scope.newCreative.creative.post === 'new' && $scope.newCreative.isValid) {
          delete $scope.newCreative.creative.facebookSponsoredObjectId;
        } else if ($scope.newCreative.creative.post === 'existing') {
          delete $scope.newCreative.creative.image;
          delete $scope.newCreative.creative.text;
          delete $scope.newCreative.creative.multiProductCards;
        }
      }

      if ($scope.newCreative.isImageRequired && !$scope.newCreative.creative.image && ($scope.newCreative.creative.post !== 'existing')) { // Check for image, if required
        $scope.newCreative.isValid = false;
      }
      // Mandotory video for
      if (goalType === 'VIDEO_VIEWS' && !$scope.newCreative.creative.video && $scope.newCreative.creative.post !== 'existing') { // Check for video, if required
        $scope.newCreative.isValid = false;
      }

      if (creativeForm.$invalid && $scope.newCreative.isValid) {
        $scope.newCreative.isValid = false;
      }
      // Instagram specific
      if ($scope.newCreative.isValid && $scope.newCreative.creative.pageTypes === 'instagramstream') {
        $scope.newCreative.isValid = !!$scope.newCreative.creative.instagramActorId;
      }
    }

    function validateVideoAd(creativeForm) {
      // Validate URL is same as goalType url if tracked by Accomplice
      if ( ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') && (goalType === 'WEBSITE_CONVERSIONS' || goalType === 'WEBSITE_CLICKS') && $scope.newCreative.creative.post !== 'existing' ) {
        $scope.newCreative.isValid = !$scope.validateUrlForPagePostCreative();

        $scope.newCreative.isValid = $scope.newCreative.isValid && ($scope.newCreative.trackingScriptStateInUrl === 'pass' || $scope.newCreative.trackingScriptStateInUrl === 'unknown');
      }
      // make sure a FB page is selected
      if ($scope.newCreative.creative.facebookSponsoredPageId === null) { // For newsfeed, make sure a facebook page is selected
        $scope.newCreative.isValid = false;
      }
      // make sure there's at least one platform
      if ($scope.newCreative.platform && $scope.newCreative.platform.length === 0) {
        $scope.newCreative.isValid = false;
      }
      // They better have a video
      if (!$scope.newCreative.creative.video && ($scope.newCreative.creative.post !== 'existing')) {
        $scope.newCreative.isValid = false;
      }
      if ((goalType === 'WEBSITE_CONVERSIONS' || goalType === 'WEBSITE_CLICKS') && $scope.newCreative.creative.post !== 'existing' && (!$scope.newCreative.creative.callToAction || $scope.newCreative.creative.callToAction === '')) {
        $scope.newCreative.isValid = false;
      }

      //if something in the form is invalid, set the isValid flag
      if (creativeForm.$invalid && $scope.newCreative.isValid) {
        $scope.newCreative.isValid = false;
      }
    }

    function validateProductCardAd(creativeForm) {
      if ((goalType === 'WEBSITE_CONVERSIONS' || goalType === 'WEBSITE_CLICKS')) {
        // Validate the URLs for each product card
        for (var i = 0, j = $scope.newCreative.creative.multiProductCards.length; i < j; i++) {
          // Validate URL
          if ($scope.newCreative.isValid) {
            $scope.newCreative.isValid = !$scope.validateUrlForMultiProductCard($scope.newCreative.creative.multiProductCards[i]);
          } else {
            break;
          }

          //Validate image
          if ($scope.newCreative.isValid) {
            if (!$scope.newCreative.creative.multiProductCards[i].image) {
              $scope.newCreative.isValid = false;
            }
          } else {
            break;
          }

          // Remove empty fields
          if ($scope.newCreative.isValid) {
            // Delete null fields if applicable
            if (!$scope.newCreative.creative.multiProductCards[i].description) {
              delete $scope.newCreative.creative.multiProductCards[i].description;
            }
            if (!$scope.newCreative.creative.multiProductCards[i].name) {
              delete $scope.newCreative.creative.multiProductCards[i].name;
            }
          }
        }
        // Validate the seeMoreURL (mandatory)
        if ($scope.newCreative.isValid) {
          $scope.newCreative.isValid = !$scope.validateProductSeeMoreUrl();

          if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE') {
            $scope.newCreative.isValid = $scope.newCreative.isValid && ($scope.newCreative.trackingScriptStateInUrl === 'pass' || $scope.newCreative.trackingScriptStateInUrl === 'unknown');
          }
        }

        if ($scope.newCreative.placement === 'newsfeed') {
          if ($scope.newCreative.creative.facebookSponsoredPageId === null) { // For newsfeed, make sure a facebook page is selected
            $scope.newCreative.isValid = false;
          }
          if ($scope.newCreative.platform && $scope.newCreative.platform.length === 0) { // Check for platform
            $scope.newCreative.isValid = false;
          }
        }
      }

      // Nothing for right column for now
      if (creativeForm.$invalid && $scope.newCreative.isValid) {
        $scope.newCreative.isValid = false;
      }
    }

    // ------------------- URL / Goal domain URL validation methods ---------------------------------------------
    $scope.validateUrlForPagePostCreative = function () {
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        // Reset the validation
        $scope.newCreative.isDestinationDomainTypedDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain($scope.newCreative.creative.url, goalUrl);
        return $scope.newCreative.isDestinationDomainTypedDifferentFromGoalDomain;
      } else {
        return false;
      }
    };

    $scope.validateUrlForMultiProductCard = function (card) {
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        card.isDomainTypedDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain(card.url, goalUrl);
        return card.isDomainTypedDifferentFromGoalDomain;
      } else {
        return false;
      }
    };

    $scope.validateProductSeeMoreUrl = function () {
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        $scope.newCreative.isSeeMoreUrlDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain($scope.newCreative.creative.seeMoreUrl, goalUrl);
        return $scope.newCreative.isSeeMoreUrlDifferentFromGoalDomain;
      } else {
        return false;
      }
    };

    $scope.checkIfUrlContainsTrackingScriptPagePostCreative = function () {
      var url = $scope.newCreative.creative.url;
      checkIfUrlContainsTrackingScript(url, 700);
    };

    $scope.checkIfUrlContainsTrackingScriptSeeMoreUrl = function () {
      var url = $scope.newCreative.creative.seeMoreUrl;
      checkIfUrlContainsTrackingScript(url, 800);
    };

    // Function used in the scope methods above
    function checkIfUrlContainsTrackingScript(url, index) {
      // Only if we are using Accomplice tracking script
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        $scope.newCreative.trackingScriptStateInUrl = 'processing';
        microService.urlValidation(
          {
            host: 'na',
            testUrl: url,
            index: index,
            nowait: true,
            nocache: true,
            badurlonly: false
          },
          function (response) {
            // If the webservice is not available
            if (response && response.status === 'ERROR') {
              // If the webservice is down, we define a new state (unknown) that will mark the creative as valid
              $scope.newCreative.trackingScriptStateInUrl = 'unknown';
            } else {
              var url = response.api || {};
              $scope.newCreative.trackingScriptStateInUrl = url.found ? 'pass' : 'fail';
            }
          },
          function (error) {
            console.log('validate tracking script in url server ERROR: ', error);
            $scope.newCreative.trackingScriptStateInUrl = 'unknown';
          });
      }
    }

    $scope.checkIfCardUrlContainsTrackingScript = function (card) {

      // Only if we are using Accomplice tracking script
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {

        // Count number of requests in order to know if we display the wait message next to save creative or not
        $scope.metadata.displayWaitMessage++;
        card.trackingScriptStateInUrl = 'processing';
        microService.urlValidation(
          {
            host: 'na',
            testUrl: card.url,
            index: card.index,
            nowait: true,
            nocache: true,
            badurlonly: false
          },
          function (response) {
            // If the webservice is not available
            if (response && response.status === 'ERROR') {
              // If the webservice is down, we define a new state (unknwown) that will mark the creative as valid
              card.trackingScriptStateInUrl = 'unknown';
            } else {
              var url = response.api || {};
              card.trackingScriptStateInUrl = url.found ? 'pass' : 'fail';
            }

            $scope.metadata.displayWaitMessage--;
          },
          function (error) {
            console.log('validate tracking script in url server ERROR: ', error);
            card.trackingScriptStateInUrl = 'unknown';
            $scope.metadata.displayWaitMessage--;
          });
      }
    };

    // ------------------- End URL / Goal domain URL validation methods ---------------------------------------------
    function applyFacebookPosts(response) {

      // If the post doesn't have a name, show the post id in lieu of the name
      $scope.loadingPosts = false;
      for (var i = 0, j = response.data.data.length; i < j; i++) {
        if (!response.data.data[i].name) {
          response.data.data[i].name = $filter('translate')('label.act.PostID:') + response.data.data[i].feedId;
        }
      }
      $scope.newCreative.$$facebookPosts = response.data.data;

      if(response.data.data.length < 20) {
        $scope.stopLoading = true;
      } else {
        $scope.stopLoading = false;
      }
    }

    $scope.filterNonBoostablePosts = function(){
      return function(post){
        if(post.fromName === $scope.newCreative.facebookPage.pageName && !post.name.includes('cover photo')){
          return post;
        }
      };
    };


// Assign selected Facebook page to a scope variable to display the name and the picture in the FbCreative view. Assign the FB page id in the creative model
    $scope.selectFacebookPageForCreative = function (facebookPage) {
      $scope.newCreative.creative.facebookSponsoredPageId = facebookPage.pagePageId;
      $scope.newCreative.facebookPage = facebookPage;

      limitPosts = 40;
      $scope.stopLoading = false;

      if($scope.campaign.settings.goal.goal.name === 'VIDEO_VIEWS' || 
        $scope.newCreative.creative.adType === 'videoAd'){
        fbPostsFilterType = 'video';
      } else if($scope.newCreative.creative.adType !== 'videoAd' &&
                ($scope.campaign.settings.goal.goal.name === 'WEBSITE_CLICKS' ||
                $scope.campaign.settings.goal.goal.name === 'WEBSITE_CONVERSIONS')) {
        fbPostsFilterType = 'link';
      } else {
        fbPostsFilterType = '';
      }

      if($scope.newCreative.creative.adType === 'videoAd' && (
                $scope.campaign.settings.goal.goal.name === 'WEBSITE_CLICKS' ||
                $scope.campaign.settings.goal.goal.name === 'WEBSITE_CONVERSIONS')){
        fbPostsFilterType = 'video&hasCallToAction=true';
      }

      FbCreative.lastSelectedFacebookPage = facebookPage;
      // For post engagement, load the posts
      if (($scope.campaign.settings.goal.goal.name === 'POST_ENGAGEMENT') ||
          ($scope.campaign.settings.goal.goal.name === 'WEBSITE_CLICKS') ||
          ($scope.campaign.settings.goal.goal.name === 'WEBSITE_CONVERSIONS') ||
          ($scope.campaign.settings.goal.goal.name === 'PAGE_LIKES') ||
          ($scope.campaign.settings.goal.goal.name === 'VIDEO_VIEWS')) {
        $scope.loadingPosts = true;
        FbCreative.getPosts(facebookPage.pagePageId, fbPostsFilterType, limitPosts).success(applyFacebookPosts);
      }

    };

    $scope.stopLoading = false;

    var limitPosts = 40;


    function newFacebookPosts(response) {
      if(response.data.data.length < limitPosts) {
        $scope.stopLoading = true;
      } else {
        limitPosts = limitPosts + 20;
      }

      for (var i = 0, j = response.data.data.length; i < j; i++) {
        if (!response.data.data[i].name) {
          response.data.data[i].name = 'Post ID: ' + response.data.data[i].feedId;
        }
      }

      $scope.loadingPosts = false;

      $scope.newCreative.$$facebookPosts = response.data.data;
    }


    $scope.loadingPosts = false;

    //Load more posts for infinite scroll
    $scope.loadMorePostsFacebook = function () {
      if($scope.stopLoading || $scope.loadingPosts) {
        return;
      }

      $scope.loadingPosts = true;
      FbCreative.getPosts($scope.newCreative.creative.facebookSponsoredPageId, fbPostsFilterType, limitPosts).success(newFacebookPosts);
    };

// Assign null to facebookPage in creative view so the dropdown to select a page is shown again
    $scope.unSelectFacebookPageForCreative = function () {
      $scope.newCreative.creative.facebookSponsoredPageId = $scope.newCreative.facebookPage = null;
    };

    $scope.selectPostForCreative = function (post) {
      if (post) {
        $scope.newCreative.creative.facebookSponsoredObjectId = post.feedId;
        $scope.newCreative.selectedPost = post;
      } else {
        FbCreative.getPosts($scope.newCreative.facebookPage.pagePageId, fbPostsFilterType, limitPosts).success(function (response) {
          applyFacebookPosts(response);
          var post = findFacebookPost();
          $scope.selectPostForCreative(post);
        });
      }
    };

    $scope.switchCreativeAdType = function (type) {
      if (creativesByAdTypeCollection[type]){
        $scope.newCreative = creativesByAdTypeCollection[type];
      } else {
        var creative = new FbCreative(null, type);
        creative.id = $scope.newCreative && $scope.newCreative.id ? $scope.newCreative.id : null;
        $scope.newCreative = creativesByAdTypeCollection[type] = creative;
        if (FbCreative.lastSelectedFacebookPage) {
          $scope.selectFacebookPageForCreative(FbCreative.lastSelectedFacebookPage);
        }
        configureNewCreativeForGoal(creative, goal);
      }

      setRequiredFields();
      fetchCallsToAction();

      $scope.newCreative.adAccountId = $scope.campaign.settings.channels.facebook.adAccount.accountId;

      // For backward compatibility for creatives created before the implementation of multi product cards
      if (!$scope.newCreative.creative.multiProductCards) {
        $scope.newCreative.creative.multiProductCards = [];
      }

      // This method will initialize 3 product cards if the array is empty
      if (type === 'productCard') {
        if (!$scope.newCreative.creative.multiProductCards.length) {
          $scope.newCreative.initializeProductCards();
        }
        $scope.newCreative.placement = 'newsfeed';
      }
      if (type === 'videoAd') {
        $scope.adTypeFilter = {type: 'video'};
      } else {
        $scope.adTypeFilter = {type: '!video'};
      }
    };

    $scope.switchInstagramCreativeAdType = function (type) {
      if (instagramCreativesByAdTypeCollection[type]){
        $scope.newCreative = instagramCreativesByAdTypeCollection[type];
      } else {
        var creative = new FbCreative(null, type);
        creative.id = $scope.newCreative && $scope.newCreative.id ? $scope.newCreative.id : null;
        creative.creative.pageTypes = 'instagramstream';
        $scope.newCreative = instagramCreativesByAdTypeCollection[type] = creative;
        if (FbCreative.lastSelectedFacebookPage) {
          $scope.selectFacebookPageForCreative(FbCreative.lastSelectedFacebookPage);
        }
        if (FbCreative.lastSelectedInstagramAccount) {
          $scope.selectInstagramAccountForCreative(FbCreative.lastSelectedInstagramAccount);
        }
        configureNewCreativeForGoal(creative, goal);
      }

      setRequiredFields();
      fetchCallsToAction();

      $scope.newCreative.adAccountId = $scope.campaign.settings.channels.facebook.adAccount.accountId;

      // For backward compatibility for creatives created before the implementation of multi product cards
      if (!$scope.newCreative.creative.multiProductCards) {
        $scope.newCreative.creative.multiProductCards = [];
      }

      // This method will initialize 3 product cards if the array is empty
      if (type === 'productCard') {
        if (!$scope.newCreative.creative.multiProductCards.length) {
          $scope.newCreative.initializeProductCards();
        }
        $scope.newCreative.placement = 'newsfeed';
      }
      if (type === 'videoAd') {
        $scope.adTypeFilter = {type: 'video'};
      } else {
        $scope.adTypeFilter = {type: '!video'};
      }
    };

    $scope.toggleChannelInstagramFacebook = function () {
      switch ($scope.metadata.creativeChannelTargeted) {
        case 'facebook':
          $scope.switchCreativeAdType('pagePost');
          break;
        case 'instagram':
          $scope.switchInstagramCreativeAdType('pagePost');
          break;
      }
    };

    $scope.togglePlatformUnavailablityForAdTypeMessageDisplay = function (value) {
      $scope.metadata.desktopCheckboxClicked = !value;
    };

    $scope.selectCallToAction = function (callToAction) {
      $scope.newCreative.creative.callToAction = callToAction;
    };

    function addToCallsToActionList (callsToAction) {
      //only add the call to action to the list if it isn't already there.
      _.map(callsToAction, function (CTA) {
        if ($scope.callsToActionList.indexOf(CTA) === -1) {
          $scope.callsToActionList.push(CTA);
        }
      });
    }

    // -------------------------- Library ------------------------------------

    $scope.loadImageFromLibrary = function () {
      // Open Library - Load Image modal view


      var opts = {
        currentCombo: $scope.metadata.currentCombo
      };

      if($scope.newCreative.creative.pageTypes === 'instagramstream'){
        opts.minSize = {};
        opts.minSize.width = 600;
      }

      library.openLibrary('Images', opts).then(function(content){
        if(content){
          if(content[0].imageErrorWidthOrHeight){
            $scope.imageErrorWidthOrHeight = true;
            $scope.picWidth = content[0].width;
          }
          $scope.newCreative.creative.image = content[0].fullGuid;
        }
      });
    };

    $scope.loadCreativeFromLibrary = function () {
      // Open Library - Load Image modal view
      var opts = {
        currentComboId: $scope.metadata.currentCombo.id,
        channel: $scope.campaign.selectedChannel
      };
      library.openLibrary('Creatives', opts).then(function(content){        
        $scope.newCreative.facebookPage = content.facebookPage;
        $scope.newCreative.creative = content.creative;
        $scope.newCreative.platform = content.platform;
        $scope.newCreative.placement = content.placement;
        $scope.newCreative.isValid = content.isValid;
        $scope.newCreative.trackingScriptStateInUrl = content.trackingScriptStateInUrl;
        // $scope.saveCreative($scope.newCreative.creative);
      });
    };

    $scope.loadVideoFromLibrary = function () {
      // Open Library - Load Video modal view
      var opts = {
        campaign: $scope.campaign,
        currentCombo: $scope.metadata.currentCombo,
        isInstagram: $scope.newCreative.creative.pageTypes === 'instagramstream'
      };
      library.openLibrary('Videos', opts).then(function(content){
        if (content) {
          $scope.newCreative.creative.video = content[0].guid;
          $scope.newCreative.creative.image = content[0].imageUrl;
        }
      });
    };

    // -------------------------- End Library ------------------------------------

    $scope.setForm = function (form, isCreativeValid) {
      form.submitted = !isCreativeValid;
    };

    $scope.clickOnPlatformOption = function () {
      $scope.metadata.hasClickedOnPlatformOption = true;
      $timeout(function() {
        $scope.metadata.hasClickedOnPlatformOption = false;
      }, 3000);
    };

    // To determine whether the text has double punctuation
    // TODO: could be turned into a function called on ng-change in text.html to reduce the watchers
    $scope.$watch('newCreative.creative.text', function (newValue) {
      if (newValue) {
        $scope.metadata.isCreativeTextInvalid = $scope.textRegex.test($scope.newCreative.creative.text);
        // Replace newline characters by empty space when user copy / pastes text with spaces
        $scope.newCreative.creative.text = $scope.newCreative.creative.text.replace(/\s+/g, ' ');
      }
    });

    $scope.preventDefaultEnterKeyPressed = function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
      }
    };

    $scope.selectInstagramAccountForCreative = function (instagramAccount) {
      $scope.newCreative.creative.instagramActorId = instagramAccount.accountId;
      $scope.newCreative.instagramAccount = instagramAccount;

      FbCreative.lastSelectedInstagramAccount = instagramAccount;
    };

  });
