'use strict';

angular.module('act')
    .service('FbCreative', function ($http, fsUtils, $sce, Creative, apiUrl) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        var FbCreative;

        FbCreative = (function () {
            function FbCreative(creative, adType) {

                // Call the super constructor Creative
                this.constructor.super_.call(this);

                if (creative) {
                    for (var property in creative) {
                        if (creative.hasOwnProperty(property)) {
                            this[property] = creative[property];
                        }
                    }
                    return;
                }

                this.id = null;
                // All info related to the creative stored here.
                this.creative = {
                    facebookSponsoredPageId: null,
                    image: null,
                    text: '',
                    // Multi product
                    adType: adType || 'pagePost', // other option is "productCard"
                    post: 'new', // other option is "existing"
                    multiProductCards: []
                };
                // Used in front end only
                // Platform is Desktop, mobile etc in Facebook.
                this.platform = [];
                this.previewList = [];
                // Placement is used by front end to determine pageTypes using platform
                this.placement = 'newsfeed'; // newsfeed by default
            }

            // Implement inheritance from Creative abstract class
            fsUtils.inherits(FbCreative, Creative);

            return FbCreative;
        })();

        // To generate fb card indexes (necessary when calling webservice to validate tracking script is present in URL)
        var counter = 1;
        FbCreative.lastFacebookPageSelected = FbCreative.lastFacebookPageSelected || null;

        FbCreative.prototype.removeImage = function () {
            this.creative.image = null;
        };


        FbCreative.prototype.getPreview = function(){
            var preview = {};
            var self = this;
            preview.iframeList = [];
            // self.previewList = [];

            if (this.isValid) {
              $http({
                method: 'GET',
                url: apiUrl + '/api/v2/campaign/adPreview/' + this.adAccountId + '?creativeIds=' + this.id
              }).success(function(response){
                preview.error = {};
                self.previewList = [];
                self.previewLoading = true;
                if(response.error){
                  preview.error.label = 'Error in Generating Ad Preview';
                  preview.error.value = response.error;
                  self.previewList.push(preview);
                } else{
                  for(var i = 0; i < response.length; i++){
                    //attach loading callback to iframe string
                    // response[i] = response[i].splice(7,0, ' onLoad="uploadDone()"');
                    preview.iframeList.push($sce.trustAsHtml(response[i]));
                  }
                    //show preview when done loading
                    // window.uploadDone = function(){
                    //     self.previewLoading = false;
                    // };
                  self.previewList.push(preview);
                }
                self.previewsLoaded = true;
              })
                .error(function(response){
                  preview.error = {};
                  preview.error.label = 'Error in Generating Ad Preview';
                  preview.error.value = response.data;
                  self.previewList.push(preview);
                });
            }
        };

        FbCreative.prototype.loadPostToBoost = function(facebookPage, post, adAccountId){
            this.adAccountId = adAccountId;
            this.creative.post = 'existing';
            this.placement = 'newsfeed';
            this.platform = [
                'desktop', 'mobile'
            ];
            this.facebookPage = facebookPage;
            this.creative.image = null;
            this.creative.facebookSponsoredPageId = this.facebookPage.pagePageId;
            this.creative.facebookSponsoredObjectId = post.feedId;
            this.creative.pageTypes = 'feed';
            delete this.creative.text;
            delete this.creative.image;
            delete this.creative.multiProductCards;
        };

        FbCreative.prototype.removeVideo = function () {
            this.creative.video = null;
            this.creative.image = null;
        };
        // ------------------ Multi Product methods -------------------
        FbCreative.prototype.initializeProductCards = function() {
            for (var i = 0, j = 3; i < j; i++){
                this.addProductCard();
            }
        };

        FbCreative.prototype.addProductCard = function() {
            var productCard = {
                // Mandatory fields
                image: null,
                destinationUrl: null,
                // Optional fields
                name: null,
                description: null,
                index: counter++
            };
            this.creative.multiProductCards.push(productCard);
        };

        FbCreative.prototype.deleteProductCard = function(card) {
            var index = this.creative.multiProductCards.indexOf(card);
            this.creative.multiProductCards.splice(index, 1);
        };

        // ------------------ End Multi Product methods -------------------

        FbCreative.getPosts = function (pageId, filterType, limit) {

            // var from = moment().subtract(30, 'days').unix();
            // var to = moment().unix();
            if(filterType){
                return $http({
                    method: 'GET',
                    url: apiUrl + '/api/v1/monitor/posts?limit=' + limit + '&pageIds=' + pageId + '&filterType=' + filterType 
                });
            } else {
                return $http({
                    method: 'GET',
                    url: apiUrl + '/api/v1/monitor/posts?limit=' + limit + '&pageIds=' + pageId
                });
            }
        };

        return FbCreative;
    });
