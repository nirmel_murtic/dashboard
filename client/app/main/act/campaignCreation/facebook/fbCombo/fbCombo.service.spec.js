'use strict';

describe('Service: FbCombo', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var FbCombo;
  beforeEach(inject(function (_FbCombo_) {
    FbCombo = _FbCombo_;
  }));

  it('should do something', function () {
    expect(!!FbCombo).toBe(true);
  });

});
