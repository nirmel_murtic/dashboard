'use strict';

angular.module('act')
  .service('FbCombo', function ($http, FbCreative, $rootScope, $sce, FacebookAdPreview, $timeout, fsUtils, FbAudience, Combo, apiUrl, fsNotify, $filter) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var FbCombo;

    FbCombo = (function () {
      function FbCombo() {

        // Call the super constructor Combo
        this.constructor.super_.call(this);
//                if(comboId) {
//                    this.load(comboId);
//                    return;
//                }

        this.name = 'FB Combo ' + (counter++);
        this.channel = 'FACEBOOK';
      }

      // Implement inheritance from Combo abstract class
      fsUtils.inherits(FbCombo, Combo);

      return FbCombo;
    })();

    // Counter to generate combo default names
    var counter = 1;

    // Save and Update methods have been moved to Combo.service.js

    FbCombo.prototype.saveCreative = function (newCreative) {

      removeEmptyFieldsInCreative(newCreative);

      if (newCreative.id) {
        return updateCreative(this, newCreative);
      } else {
        return createCreative(this, newCreative);
      }
    };

    FbCombo.prototype.saveAudience = function (newAudience) {

      if (newAudience.id) {
        updateAudience(this, newAudience);
      } else {
        createAudience(this, newAudience);
      }
    };

    FbCombo.prototype.calculateNumberOfAudienceVariants = function () {
      var numberOfVariants = 0;

      if (this.audience) {
        numberOfVariants = 1;

        for (var property in this.audience.audience) {
          if (this.audience.audience.hasOwnProperty(property) && this.audience.audience[property] !== undefined && this.audience.audience[property].length !== 0) {
            numberOfVariants *= this.audience.audience[property].length;
          }
        }
      }
      // Update the count of audience variants
      this.numberOfAudienceVariants = numberOfVariants;
    };

    // Check if the combo has audience (at least location) + at least one creative
    FbCombo.prototype.checkValidity = function () {
      var isComboValid = true;

      this.checkForInvalidCreatives();

      if (!this.audience || !this.audience.audience || !this.audience.audience.location || this.audience.audience.location.length === 0 ||
        this.creativeList.length === 0 || this.invalidCreativeIndexList.length > 0 || (this.audience.lowReachAdsList && this.audience.lowReachAdsList.length > 0 )) {
        isComboValid = false;
      }
      this.isValid = isComboValid;
      return isComboValid;
    };

    // Load from Library and return a combo object
    FbCombo.load = function (comboData, superCmpId, parentCmpId) {
      var combo = new FbCombo();
      var unsortedCreativesList = [];

      // For each creative and audience, create the relevant objects
      for (var property in comboData) {
        if (comboData.hasOwnProperty(property)) {
          if (property === 'audience' && comboData[property] !== '') { // If it's an audience, associate to the audience property of the combo object
            combo.audience = new FbAudience(angular.fromJson(comboData[property].audience));
            combo.audience.id = comboData[property].id;

          } else if (property === 'creativeList' && comboData[property] !== null) { // If it's a creative, create creative objects
            for (var i = 0, j = comboData[property].length; i < j; i++) {
              var creative = new FbCreative(angular.fromJson(comboData[property][i].creative));
              creative.id = comboData[property][i].id;
              // Handle legacy issue when post objects were at the root of the creative object
              if (creative.post) {
                creative.creative.post = creative.post;
                delete creative.post;
              }
              creative.getPreview();
              unsortedCreativesList.push(creative);
            }
          } else { // Other properties like comboId or channel
            combo[property] = comboData[property];
          }
        }
      }

      // When loading from library, use ids passed as a parameter
      combo.superCmpId = superCmpId || combo.superCmpId;
      combo.parentCmpId = parentCmpId || combo.parentCmpId;

      // Sort creatives order: First created is first displayed
      combo.creativeList = _.sortBy(unsortedCreativesList, function (creativeInList) {
        return creativeInList.id;
      });

      // Generate number of creative and variant for each combo
      combo.calculateNumberOfCreativeVariants();
      combo.calculateNumberOfAudienceVariants();
      combo.checkForInvalidCreatives();
      return combo;
    };

    // Internal method to create an audience
    function createAudience(self, newAudience) {
      var params = {
        typeReq: 'AUDIENCE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        data: angular.toJson(newAudience)
      };

      $http({
        url: apiUrl + '/api/v2/campaign/create/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          // Assign id returned by backend
          newAudience.id = response.data.cmpAdSettingId;
          // Assign audience to the newly created audience
          self.audience = newAudience;
          // Retrieve list of ads that have a low reach, if any
          self.audience.lowReachAdsList = FbCombo.parseLowReachAds(response.data.lowReachAdsList);

          self.calculateNumberOfAudienceVariants();
          self.checkValidity();

          fsNotify.push(
            $filter('translate')('label.act.Audienceaddedtocombo') + ' ' + self.name
          );
        }).
        error(
        function (response) { // optional
          console.log(response);
        });
    }

    // Internal method to update an audience
    function updateAudience(self, newAudience) {
      var params = {
        typeReq: 'AUDIENCE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        cmpAdSettingId: newAudience.id,
        data: angular.toJson(newAudience)
      };

      $http({
        url: apiUrl + '/api/v2/campaign/update/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          // Assign audience to the newly created audience
          self.audience = newAudience;
          // Retrieve list of ads that have a low reach, if any
          self.audience.lowReachAdsList = FbCombo.parseLowReachAds(response.data.lowReachAdsList);

          self.calculateNumberOfAudienceVariants();
          self.checkValidity();

          fsNotify.push(
            $filter('translate')('label.act.Audienceupdatedincombo') + ' ' + self.name
          );
        }).
        error(
        function (response) { // optional
          console.log(response);
        });
    }


    //TODO: need to refactor... very hacky
    String.prototype.splice = function (idx, rem, s) {
      return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
    };

    // This is to delete the fields that might have been edited then emptied, in order to avoid issues with the backend parsing
    // The backend can't process null results
    function removeEmptyFieldsInCreative(newCreative) {

      for (var field in newCreative.creative) {
        if (newCreative.creative.hasOwnProperty(field) &&
          (newCreative.creative.hasOwnProperty(field) === '' || newCreative.creative.hasOwnProperty(field) === null || newCreative.creative.hasOwnProperty(field) === undefined)) {
          delete newCreative.creative[field];
        }
      }

      //TODO: Ask Palak if he wants this removed
      //if (newCreative.creative.adType === 'productCard') {
      //    for (var i = 0, j = multiProductCards.length; i < j; i++) {
      //        delete newCreative.creative.isDomainTypedDifferentFromGoalDomain;
      //    }
      //}
    }

    // Internal method to create a creative
    function createCreative(self, newCreative) {
      var params = {
        typeReq: 'CREATIVE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        data: angular.toJson(newCreative)
      };

      return $http({
        url: apiUrl + '/api/v2/campaign/create/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          // Assign returned id to the creative
          newCreative.id = response.data.cmpAdSettingId;
          self.creativeList.push(newCreative);
          self.calculateNumberOfCreativeVariants();
          self.checkForInvalidCreatives();
          self.checkValidity();
          newCreative.getPreview();

          fsNotify.push(
            $filter('translate')('label.act.Creativeaddedtocombo') + ' ' + self.name
          );
        }).
        error(
        function (response) { // optional
          console.log(response);
        });
    }

    // Internal method to update a creative
    function updateCreative(self, newCreative) {
      var params = {
        typeReq: 'CREATIVE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        cmpAdSettingId: newCreative.id,
        data: angular.toJson(newCreative)
      };

      return $http({
        url: apiUrl + '/api/v2/campaign/update/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function () {
          self.calculateNumberOfCreativeVariants();
          self.checkForInvalidCreatives();
          self.checkValidity();

          newCreative.getPreview();

          fsNotify.push(
            $filter('translate')('label.act.Creativeupdatedincombo') + ' ' + self.name
          );
        }).
        error(
        function (response) { // optional
          console.log(response);
        });
    }

    return FbCombo;
  });


