'use strict';

angular.module('act')
    .config(function ($stateProvider) {
        $stateProvider
            .state('main.act.campaignCreation', {
                url: '/campaignCreation',
                params : {
                    id : null,
                    type : null,
                    param: null
                },
                resolve: {
                    campaignInstantiation: function (auth, Campaign, $stateParams) {
                        // Load draft or duplicate campaign if id is passed as a parameter. If not, create a new campaign
                        if ($stateParams.id && ($stateParams.type === 'draft' || $stateParams.type === 'duplicate')) {
                          return new Campaign(auth.user, $stateParams.id, $stateParams.type);
                        } else if($stateParams.type === 'post') {
                            return new Campaign(auth.user, null, 'post', $stateParams.param);
                        } else {
                          return new Campaign(auth.user);
                        }
                    }
                },
                views: {
                    '@main': {
                        templateUrl: 'app/main/act/campaignCreation/core/campaignCreation.html',
                        controller: 'CampaigncreationCtrl'
                    }
                },
                data: {
                    displayName: 'Campaign Creation',
                    active: 'campaign'
                }
            })
        ;
    });
