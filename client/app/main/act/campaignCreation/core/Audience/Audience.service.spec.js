'use strict';

describe('Service: Audience', function () {

  // load the service's module
  beforeEach(module('dashboardApp'));

  // instantiate service
  var Audience;
  beforeEach(inject(function (_Audience_) {
    Audience = _Audience_;
  }));

  it('should do something', function () {
    expect(!!Audience).toBe(true);
  });

});
