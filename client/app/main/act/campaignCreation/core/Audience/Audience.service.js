'use strict';

angular.module('act')
  .service('Audience', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function

        var Audience;
        Audience = (function(){
            function Audience(audience) {

                if(audience) {
                    for (var property in audience){
                        if(audience.hasOwnProperty(property)){
                            this[property] = audience[property];
                        }
                    }
                    return;
                }

                this.id = null;
                // All info related to the audience stored here
                this.audience = {};

            }
            return Audience;
        })();


        Audience.prototype.addVariant = function () {

        };

        Audience.prototype.removeVariant = function () {

        };

        Audience.prototype.addTargetingSpec = function () {

        };

        Audience.prototype.removeTargetingSpec = function () {

        };

        return Audience;
  });
