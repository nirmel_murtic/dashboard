'use strict';

angular.module('act')
  .service('Combo', function (apiUrl, $rootScope, $http, fsNotify, $filter, fsConfirm, withInput) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var Combo;

    Combo = (function () {
      function Combo() {

        this.id = null;
        this.audience = null;
        this.creativeList = [];
        this.needsUpdate = false;
        this.isValid = false;

        this.numberOfAudienceVariants = 0;
        this.numberOfCreativeVariants = 0;
      }

      return Combo;
    })();


    Combo.prototype.save = function (parentCmpId) {
      var self = this;

      var params = {
        parentCmpId: parentCmpId,
        data: angular.toJson(self)
      };

      var promise = $http({
        url: apiUrl + '/api/v2/campaign/create/adCombo/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      });
      // Success & Error handled in campaign.service.js in the addCombo method // or in library
      return promise;
    };

    Combo.prototype.bulkSave = function (parentCmpId) {
      var self = this;

      var params = {
        parentCmpId: parentCmpId,
        data: angular.toJson(self)
      };

      var promise = $http({
        url: apiUrl + '/api/v2/campaign/create/bulk/adCombo/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      });
      return promise;
    };

    // This basically only update the combo name for now even though the endpoint allows us to update everything
    Combo.prototype.update = function () {
      // IF the combo name has changed
      if (this.needsUpdate) {
        var self = this;
        var params = {
          adComboId: self.id,
          data: angular.toJson(self)
        };

        return $http({
          url: apiUrl + '/api/v2/campaign/update/adCombo/' + $rootScope.fractalAccountInfo.currentTeam.id,
          method: 'POST',
          data: $.param(params)
        }).
          success(function (response) {
            console.log('combo updated: ', response);
            self.needsUpdate = false;
          }).
          error(function (response) { // optional
            console.log(response);
          });
      }
    };

    Combo.prototype.saveToLibrary = function () {

      var newData = this;

      var oldName = angular.copy(newData.name);

      withInput.setInput(oldName);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.act.NameYourCombo'),
        title: 'My Combo'
      }).then(function () {
        var newName = withInput.getInput();

        var params = {
          channelName: newData.channel,
          name: newName,
          objectId: newData.id,
          data: angular.toJson(newData),
          libraryObjectType: 'adCombo'
        };

        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/library/save/template/' + $rootScope.fractalAccountInfo.currentTeam.id,
          data: $.param(params)
        }).
          success(function () {
            fsNotify.push(
              $filter('translate')('label.act.Combosavedtolibrary', {comboName:newName})
            );
          }).
          error(function (response) { // optional
            console.log(response);
            newData.name = oldName;
          });
      });

    };

    Combo.prototype.calculateNumberOfCreativeVariants = function() {
      this.numberOfCreativeVariants = this.creativeList.length;
    };

    Combo.prototype.checkForInvalidCreatives = function () {
      var invalidCreativeIndexList = [];

      for (var i = 0, j = this.creativeList.length; i < j; i++) {
        if (!this.creativeList[i].isValid) {
          invalidCreativeIndexList.push(i);
        }
      }

      this.invalidCreativeIndexList = invalidCreativeIndexList;
    };

    Combo.prototype.deleteCreative = function (creative) {
      var self = this;

      var confirmCallBack = function() {
        $http({
          method: 'DELETE',
          url: apiUrl + '/api/v2/campaign/delete/adCreative/?adCreativeId='+creative.id
        }).
          success(function () {
            // Remove creative from creativeList
            var creativeIndex = _.findIndex(self.creativeList, {'id': creative.id});
            self.creativeList.splice(creativeIndex, 1);
            // Update the number of creative in the
            self.calculateNumberOfCreativeVariants();
            self.checkForInvalidCreatives();
            // Notify  the user
            fsNotify.push(
              $filter('translate')('label.act.Creativedeleted')
            );
          }).
          error(function (response) { // optional
            console.log(response);
          });
      };

      fsConfirm('generic', {
        body : $filter('translate')('label.act.Areyousureyouwanttodeletethiscreative?')
      }).then(confirmCallBack);
    };

    Combo.parseLowReachAds = function (lowReachAds) {
      var lowReachAdsList = [];
      _.forEach(lowReachAds, function (lowReachAd) {
        var ad = {
          adInfo: parseJSONDeep(lowReachAd.adInfo),
          adReach: lowReachAd.adReach
        };
        lowReachAdsList.push(ad);
      });
      return lowReachAdsList;
    };

    function parseJSONDeep(json) {
      if (_.isString(json)) {
        try {
          json = JSON.parse(json);
        } catch (err) {
        }
      }
      var key;
      if (_.isObject(json)) {
        for (key in json) {
          json[key] = parseJSONDeep(json[key]);
        }
      }
      return json;
    }

    return Combo;
  });
