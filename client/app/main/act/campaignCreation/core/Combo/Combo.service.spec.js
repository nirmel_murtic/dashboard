'use strict';

describe('Service: Combo', function () {

  // load the service's module
  beforeEach(module('dashboardApp'));

  // instantiate service
  var Combo;
  beforeEach(inject(function (_Combo_) {
    Combo = _Combo_;
  }));

  it('should do something', function () {
    expect(!!Combo).toBe(true);
  });

});
