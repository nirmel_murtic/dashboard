'use strict';

describe('Controller: CampaigncreationCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var CampaigncreationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CampaigncreationCtrl = $controller('CampaigncreationCtrl', {
      $scope: scope
    });
  }));
});
