'use strict';

angular.module('act')
  .service('Campaign', function ($http, $filter, FbCombo, AwCombo, TwCombo, projectService, FbCreative, apiUrl, ngDialog, $q, $state, Scroll, fsNotify, fsConfirm, withInput, $rootScope) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var Campaign;

    Campaign = (function () {
      function Campaign(user, campaignId, type, param) {
        this.$$user = user;

        // Retrieve info from APIs
        this.api = {
          facebook :{
            adAccounts: Campaign.getFacebookAdAccounts(this.$$user.currentTeam),
            pages: Campaign.getFacebookPages(this.$$user.currentTeam)
          }, twitter: {
            pages: Campaign.getTwitterPages(this.$$user.currentTeam),
            adAccounts: Campaign.getTwitterAdAccounts(this.$$user.currentTeam),
          }, adwords: {
            adAccounts: Campaign.getAdwordsAdAccounts(this.$$user.currentTeam)
          },
          projects: $rootScope.fractalAccountInfo.currentTeam.projects
        };

        if (campaignId && type === 'draft') {
          return this.loadDraft(campaignId, user.currentTeam.id);
        } else if (campaignId && type === 'duplicate') {
          return this.loadDuplicate(campaignId, user.currentTeam.id);
        } else if (type === 'post') {
          return this.loadPostToBoost(param);
        }

        this.settings = {
          goal: null,
          channels: {},
          budgetPolicy: 'FLOATING',
        };
        // All channels are disabled until the user selects a goal
        this.isChannelAvailableForGoal = {
          adwords: false,
          facebook: false,
          twitter: false
        };
        // Check that at least one channel has a valid ad account
        verifyAtLeastOneAdAccountIsPresent(this);
        // Setup the dates
        initializeCampaignDuration(this);
        this.combos = [];
        this.estimatedReach = {};

      }

      // Check there is at least one channel that has at least one valid ad account
      // If there is no channel with at least one valid ad account, prompt the user to go to Align
      function verifyAtLeastOneAdAccountIsPresent(self) {
        var i, j, adAccountExists = false;

        var fbAdAccounts = Campaign.getFacebookAdAccounts(self.$$user.currentTeam);
        var awAdAccounts = Campaign.getAdwordsAdAccounts(self.$$user.currentTeam);
        var twAdAccounts = Campaign.getTwitterAdAccounts(self.$$user.currentTeam);

        for (i = 0, j = fbAdAccounts.length; i < j; i++) {
          if (fbAdAccounts[i].flagged === false) {
            adAccountExists = true;
            break;
          }
        }
        if (!adAccountExists) {
          for (i = 0, j = awAdAccounts.length; i < j; i++) {
            if (awAdAccounts[i].flagged === false) {
              adAccountExists = true;
              break;
            }
          }
        }
        if (!adAccountExists) {
          for (i = 0, j = twAdAccounts.length; i < j; i++) {
            if (twAdAccounts[i].flagged === false) {
              adAccountExists = true;
              break;
            }
          }
        }

        if (!adAccountExists) {
          var confirmCallback = function () {
            ngDialog.close();
            $state.go('main.align', {refresh: true});
          };

          fsConfirm('genericAlert', {
            body: $filter('translate')('label.act.missingAdAccountMessage'),
            yes: $filter('translate')('label.act.GoToSetup')
          }).then(confirmCallback);
        }
      }

      Campaign.prototype.publish = function () {

        var self = this;
        self.isLaunchButtonDisabled = true;

        // Check if startDate is before "Now" time
        if (self.settings.startDate > new Date()) {
          return publishCampaign(self);
        } else {
          return fsConfirm('generic', {
            body: $filter('translate')('label.act.campaignStartDateInPastMessage', {endDate: $filter('date')(self.settings.endDate, 'EEEE, MM/dd/yy hh:mm a')}),
            yes:$filter('translate')('label.act.LaunchNow'),
            no:$filter('translate')('label.act.Cancel')
          }).then(function() {
            // User clicks on Launch Now. We need to update the date, save the settings and launch the campaign
            self.settings.startDate = new Date();
            return self.saveSettings().then(publishCampaign(self));

          }, function () { // User clicks on cancel
            // Send a rejected promise in order to avoid calling redirect to campaign manager
            return $q.reject(null);
          });

        }
      };

      function publishCampaign (self) {
        var params = {
          superCmpId: self.settings.id
        };

        return $http({
          method: 'POST',
          url: apiUrl + '/api/v2/campaign/publish/' + self.$$user.currentTeam.id,
          data: $.param(params)
        });
      }

      function handleCampaignLoading(self, campaignData, campaignId, campaignType) {

        var settingsData = angular.fromJson(campaignData.settings.settings),
          comboListData = campaignData.combos;

        // Bind values to our model
        self.settings = settingsData;
        // Assign id of the loaded campaign
        self.settings.id = campaignId;
        self.settings.budget =  Math.round(Number(self.settings.budget)* 100) / 100;
        // Select the same goal that is present in the project goals (Otherwise goal doesn't load properly in settings page)
        self.settings.goal = _.find(self.settings.project.goals, {'id': self.settings.goal.id});
        self.newGoal = _.find(self.settings.project.goals, {'id': self.settings.goal.id});
        // Convert channels array to object (easier to handle in front end)
        self.settings.channels = convertChannelArrayToObject(self.settings.channels);
        // Setup the dates
        self.settings.startDate = new Date(settingsData.startDate);
        self.settings.endDate = new Date(settingsData.endDate);
        var newDate = new Date();
        if (self.settings.startDate < newDate) {
          initializeCampaignDuration(self);
          fsNotify.push(
            $filter('translate')('label.act.cmpResetDateMessage')
          );
        }
        var adAccounts, channel, channelsWhereInitiallyUsedAdAccountsAreNoLongerAvailable = [];

        // Assign the channel id from settings channels object returned by backend containing the mapping channel / id
        // Assign the previously attached ad account. If it's not available, warn the user. If no ad account is available for the channel, prompt the user to go to Align
        function handleChannelSettings(channel, adAccounts) {
          // Retrieve the cmp parent id from the channels array
          self.settings.channels[channel].id = _.find(campaignData.settings.channels, {'channel': channel.toUpperCase()}).id;
          // Round the budget to up to 2 decimals
          self.settings.channels[channel].budget =  Math.round(Number(self.settings.channels[channel].budget)* 100) / 100;

          var previouslySelectedAdAccount = _.find(adAccounts, {'id': self.settings.channels[channel].adAccount.id});
          // Alert that the previously used ad account is no currently available
          if (!previouslySelectedAdAccount || previouslySelectedAdAccount.flagged) {
            self.settings.channels[channel].adAccount = null;
            channelsWhereInitiallyUsedAdAccountsAreNoLongerAvailable.push(channel);
          } else {
            self.settings.channels[channel].adAccount = previouslySelectedAdAccount;
          }
        }

        // Select the same ad account that is present in the api
        for (channel in self.settings.channels) {
          if (self.settings.channels.hasOwnProperty(channel) && channel === 'facebook') {
            adAccounts = Campaign.getFacebookAdAccounts(self.$$user.currentTeam);
            handleChannelSettings(channel, adAccounts);
          } else if (self.settings.channels.hasOwnProperty(channel) && channel === 'adwords') {
            adAccounts = Campaign.getAdwordsAdAccounts(self.$$user.currentTeam);
            handleChannelSettings(channel, adAccounts);
          } else if (self.settings.channels.hasOwnProperty(channel) && channel === 'twitter') {
            adAccounts = Campaign.getTwitterAdAccounts(self.$$user.currentTeam);
            handleChannelSettings(channel, adAccounts);
          }
        }

        // If we have ad accounts that were used before and are no longer available when loading drafts and duplicates
        // We do not check for templates because the user might not want to use the same ad account and seeing popups every time the template is used can be frustrating
        if (channelsWhereInitiallyUsedAdAccountsAreNoLongerAvailable.length > 0 && campaignType === 'draft') {

          var messageDisplay = [];
          for (var i = 0, j = channelsWhereInitiallyUsedAdAccountsAreNoLongerAvailable.length; i < j; i++) {
            messageDisplay.push($filter('translate')('label.act.adAccountCurrentlyNotAvailableMessage', {channelName: channelsWhereInitiallyUsedAdAccountsAreNoLongerAvailable[i]}));
          }
          messageDisplay.push(channelsWhereInitiallyUsedAdAccountsAreNoLongerAvailable > 1 ?
              $filter('translate')('label.act.adAccountsNotAvailableMessage') :
              $filter('translate')('label.act.adAccountNotAvailableMessage')
          );

          fsConfirm('multiLineAlert', {
            messages: messageDisplay,
            yes: $filter('translate')('label.act.ChooseAnotherOne'),
            no: $filter('translate')('label.act.GoToAlign')
          })
            .then(function () {
              // No action required here, the user is simply closing the popup
            }, function () {
              $state.go('main.align', {refresh: true});
            });
        }

        self.combos = [];
        var unsortedCombosList = [];
        // Load Combos
        function handleCombos(comboData) {
          var combo;

          if (comboData.channel === 'FACEBOOK') {
            combo = FbCombo.load(comboData);
          } else if (comboData.channel === 'ADWORDS') {
            combo = AwCombo.load(comboData);
          } else if (comboData.channel === 'TWITTER') {
            combo = TwCombo.load(comboData);
          }
          combo.parentCmpId = comboListData[comboData.channel].parentCmpId;
          combo.superCmpId = self.settings.id;

          unsortedCombosList.push(combo);
        }

        for (channel in comboListData) {
          if (comboListData.hasOwnProperty(channel)) {
            for (var a = 0, b = comboListData[channel].listCombos.length; a < b; a++) {
              handleCombos(comboListData[channel].listCombos[a]);
            }
          }
        }
        // Sort combos in the order they were created
        self.combos = _.sortBy(unsortedCombosList, function (comboInList) {
          return -comboInList.id;
        });

        // Check that at least one channel has an ad account
        verifyAtLeastOneAdAccountIsPresent(self);

        // Determine compatible channels with the current goal
        self.isChannelAvailableForGoal = {
          adwords: self.determineIfChannelIsAvailableForGoal('adwords'),
          facebook: self.determineIfChannelIsAvailableForGoal('facebook'),
          twitter: self.determineIfChannelIsAvailableForGoal('twitter')
        };

        self.isReadyForCombosPage = true;
        self.settingsPageHasChanged = false;
      }

      // Load campaign settings when user loads a draft
      Campaign.prototype.loadDraft = function (campaignId, teamId) {
        var self = this;

        // Retrieve code from the backend
        return $http.get(apiUrl + '/api/v2/campaign/get/draft/' + teamId + '?superCmpId=' + campaignId).//jshint ignore:line
          success(function (response) {

            var campaignData = angular.fromJson(response.data.draftData).data;

            handleCampaignLoading(self, campaignData, campaignId, 'draft');

          }).
          error(function () {
            console.log('Couldn\'t retrieve campaign data');
          })
          .then(function () {
            return self;
          });
      };

// Duplicate campaign
// This loads the duplicate of the campaign passed in parameter (superCmpId). A new campaign is created in the backend then returned as the response
      Campaign.prototype.loadDuplicate = function (campaignDuplicateId, teamId) {
        var self = this;

        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/duplicate/all/' + teamId + '?superCmpId=' + campaignDuplicateId //jshint ignore:line
        }).
          success(function (response) {
            var campaignId = response.data.superCmpId,
              campaignData = angular.fromJson(response.data.campaignData).data;

            handleCampaignLoading(self, campaignData, campaignId, 'duplicate');

          }).
          error(function (response) { // optional
            console.log('Error while loading duplicate campaign:', response);
          }).
          then(function () {
            return self;
          });
      };

      //if user clicks on post to boost from current status, init boost a post
      Campaign.prototype.loadPostToBoost = function (post) {
        this.settings = {
          goal: null,
          channels: {},
          budgetPolicy: 'FLOATING',
        };

        // All channels are disabled until the user selects a goal
        this.isChannelAvailableForGoal = {
          adwords: false,
          facebook: false,
          twitter: false
        };
        // Check that at least one channel has a valid ad account
        verifyAtLeastOneAdAccountIsPresent(this);
        // Setup the dates
        initializeCampaignDuration(this);
        this.combos = [];

        this.settings.project = this.$$user.currentTeam.currentProject || this.$$user.currentTeam.projects[0];
        this.settings.goal = _.find(this.settings.project.goals, function (goal) {
          if (goal.goal.name === 'POST_ENGAGEMENT' && post.pageId === goal.page.pagePageId) {
            return goal;
          }
        });

        this.settings.name = 'Boost Post Campaign';
        this.settings.budget = 20;
        this.newGoal = this.settings.goal;
        this.isChannelAvailableForGoal.facebook = true;
        this.activateChannel('facebook');
        this.isReadyForCombosPage = true;

        var self = this;
        createCampaignSettings(this).then(function(){
          var newCreative = new FbCreative();
          var accountId = self.settings.channels.facebook.adAccount.accountId;
          var facebookPage = self.settings.goal.page;
          newCreative.loadPostToBoost(facebookPage, post, accountId);
          self.combos[0].saveCreative(newCreative);
        });
      };

      Campaign.prototype.loadFromLibrary = function (response) {
        var self = this;

        var campaignId = response.data.superCmpId,
          campaignData = angular.fromJson(response.data);

        handleCampaignLoading(self, campaignData, campaignId, 'template');

      };


// This method saves the settings of the campaign. If the campaign has been assigned an ID already, we call the update endpoint instead of create.
      Campaign.prototype.saveSettings = function () {

        var channel;

        // If it's a multi-channel campaign, calculate the lifetime budget for the backend
        if (this.settings.budgetPolicy === 'FIXED') {
          // Reset lifetime budget
          this.settings.budget = 0;
          // Add each channel budget into lifetime budget
          for (channel in this.settings.channels) {
            if (this.settings.channels.hasOwnProperty(channel)) {
              this.settings.budget += this.settings.channels[channel].budget;
            }
          }

          this.settings.budget = Math.round(this.settings.budget * 100) / 100;
        }

        // Count the number of parent campaigns that have an ID. At least one parent campaign needs to have an ID in order to call updateCampaignSettings
        // If no parent campaign has an id, (like when we change a goal and wipe out all parent campaign info), then we need to call createCampaignSettings
        var campaignNeedsToBeUpdated = false;

        for (channel in this.settings.channels) {
          if (this.settings.channels.hasOwnProperty(channel)) {
            if (this.settings.channels[channel].id) {
              campaignNeedsToBeUpdated = true;
            }
          }
        }

        if (campaignNeedsToBeUpdated) {
          // Update campaign settings
          return updateCampaignSettings(this);
        } else {
          // Create campaign settings
          return createCampaignSettings(this);
        }
      };


// Call this method to create an empty combo and get the id returned
      Campaign.prototype.addCombo = function (channel) {

        var self = this;
        var newCombo;

        channel = channel ? channel.toUpperCase() : angular.copy(self.selectedChannel).toUpperCase();

        switch (channel) {
          case 'FACEBOOK':
            newCombo = new FbCombo();
            break;
          case 'ADWORDS':
            newCombo = new AwCombo();
            break;
          case 'TWITTER':
            newCombo = new TwCombo();
            break;

        }
        var parentCmpId = self.settings.channels[channel.toLowerCase()].id;

        newCombo.channel = channel;

        return newCombo.save(parentCmpId).
          success(function (response) {
            // Assign ids returned
            newCombo.superCmpId = self.settings.id;
            newCombo.parentCmpId = parentCmpId;
            newCombo.id = response.data.adComboId;
            // Add combo in the list of combos of the campaign
            self.combos.unshift(newCombo);
          }).
          error(function (response) { // optional
            console.log(angular.toJson(response));
          });
      };

// Load one combo from libraries.
      Campaign.prototype.loadComboFromLibrary = function (comboData) {

        var self = this, combo;
        switch (comboData.channel) {
          case 'FACEBOOK':
            combo = FbCombo.load(comboData, this.settings.id, this.settings.channels.facebook.id);
            self.combos.unshift(combo);
            break;
          case 'ADWORDS':
            combo = AwCombo.load(comboData, this.settings.id, this.settings.channels.adwords.id);
            self.combos.unshift(combo);
            break;
          case 'TWITTER':
            combo = TwCombo.load(comboData, this.settings.id, this.settings.channels.twitter.id);
            self.combos.unshift(combo);
            break;
        }
      };

      Campaign.prototype.duplicateCombo = function (comboToDuplicate) {
        var self = this;

        // Copy the combo passed in parameter because we're going to remove ids
        var comboData = angular.copy(comboToDuplicate);

        // Create a new combo
        var combo;

        switch (comboData.channel) {
          case 'FACEBOOK':
            combo = new FbCombo();
            break;
          case 'ADWORDS':
            combo = new AwCombo();
            break;
          case 'TWITTER':
            combo = new TwCombo();
            break;
        }

        combo.parentCmpId = comboData.parentCmpId;
        combo.channel = comboData.channel;

        var timestamp = (new Date()).getTime();

        if (_.includes(comboData.name, '_copy')) {
          combo.name = comboData.name = comboData.name.slice(0, comboData.name.indexOf('_copy')) + '_copy ' + timestamp;
        } else {
          // Add copy in front of the combo name
          combo.name = comboData.name = comboData.name + '_copy ' + timestamp;
        }


        // Remove id from combo source so a new one can be assigned
        delete comboData.id;
        delete comboData.adComboId;

        combo.save(combo.parentCmpId).
          success(function (response) {

            // Assign ids returned
            combo.superCmpId = self.settings.id;
            combo.id = response.data.adComboId;

            for (var property in comboData) {
              if (comboData.hasOwnProperty(property)) {
                switch (property) {
                  case 'audience':
                    // Copy audience if there was one
                    if (comboData.audience) {
                      delete comboData.audience.id;
                      combo.saveAudience(comboData.audience);
                    }
                    break;
                  case 'creativeList':
                    for (var k = 0, l = comboData.creativeList.length; k < l; k++) {
                      // Delete id so a new one can be assigned
                      delete comboData.creativeList[k].id;
                      combo.saveCreative(comboData.creativeList[k]);
                    }
                    break;
                  case 'keyword':
                    delete comboData.keyword.id;
                    combo.saveKeyword(comboData.keyword);
                    break;
                  default:
                    combo[property] = comboData[property];
                    break;

                }
              }
            }
            // Add combo in the list of combos of the campaign
            self.combos.unshift(combo);
          }).
          error(function (response) { // optional
            console.log(angular.toJson(response));
          });
      };

      // Delete all combos
      Campaign.prototype.deleteAllCombos = function () {
        var self = this;

        var params = {
          superCmpId: this.settings.id
        };

        return $http({
          method: 'POST',
          url: apiUrl + '/api/v2/campaign/delete/adCombo/' + this.$$user.currentTeam.id,//+'?superCmpId='+self.id+'&channels='+channelsList.toString()
          data: $.param(params)

        }).
          success(function () {
            // Remove all combos
            self.combos = [];
          }).
          error(function (response) { // optional
            console.log('Error while deleting combo:', response);
          });
      };

// Delete all combos for a channel
      Campaign.prototype.deleteAllCombosForChannel = function (channel) {
        channel = channel.toUpperCase();
        var self = this;

        var params = {
          superCmpId: this.settings.id,
          channels: channel
        };

        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/campaign/delete/adCombo/' + this.$$user.currentTeam.id,
          data: $.param(params)

        }).
          success(function () {
            // Delete all the combos for that channel
            var i = self.combos.length;
            while(i--) {
              if (self.combos[i].channel === channel) {
                self.combos.splice(i, 1);
              }
            }
          }).
          error(function (response) { // optional
            console.log('Error while deleting combo:', channel, response);
          });
      };

// Delete a single combo
      Campaign.prototype.deleteCombo = function (combo, confirmFlag) {
        //showConfirm is a flag that should evaluate to "true" if you
        //want the popup confirmation to show, false if you want to
        //skip it.  Default is true if not included.
        var showConfirm = (confirmFlag !== undefined) ? confirmFlag : true;
        var self = this;
        var params = {
          adComboId: combo.id
        };

        var confirmCallback = function () {
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/campaign/delete/adCombo/' + self.$$user.currentTeam.id,
            data: $.param(params)
          }).
            success(function () {
              // Remove combo from combo list
              for (var i = 0; i < self.combos.length; i++) {
                if (self.combos[i].id && self.combos[i].id === combo.id) {
                  self.combos.splice(i, 1);
                  break;
                }
              }

              // Add Combo if there are no more combos for that channel
              if (_.findIndex(self.combos, {'channel': combo.channel}) === -1) {
                self.addCombo(combo.channel);
              }
            }).
            error(function (response) { // optional
              console.log('error: ', response);
            });
        };

        if ((!showConfirm && combo.channel === 'ADWORDS' && !combo.audience &&
          combo.creativeList.length === 0 && combo.keyword && combo.keyword.keyword.length === 0) ||
          (!showConfirm && combo.channel === 'FACEBOOK' && combo.audience === null &&
          combo.creativeList.length === 0) ||
          (!showConfirm && combo.channel === 'TWITTER' && combo.audience === null &&
          combo.creativeList.length === 0)) {
          confirmCallback();
          // Otherwise, ask the user permission
        } else {
          fsConfirm('generic', {
            body: $filter('translate')('label.act.Areyousureyouwanttodeletethecombo', {comboName: combo.name})
          }).then(confirmCallback);
        }

      };

// Calculate the number of subcampaigns for a given campaign
      Campaign.prototype.getNumberOfSubcampaigns = function () {
        // Determine the current channels for the campaign
        this.numberOfSubcampaigns = {};
        for (var channel in this.settings.channels) {
          if (this.settings.channels.hasOwnProperty(channel)) {
            this.numberOfSubcampaigns[channel] = 0;
          }
        }
        //var numberOfSubcampaigns = 0;
        for (var i = 0, j = this.combos.length; i < j; i++) {
          if (this.combos[i].numberOfAudienceVariants && this.combos[i].numberOfCreativeVariants) {
            this.numberOfSubcampaigns[(this.combos[i].channel).toLowerCase()] += this.combos[i].numberOfAudienceVariants * this.combos[i].numberOfCreativeVariants;
          }
        }
      };

      Campaign.prototype.getNumberOfCombosForChannel = function (channel) {
        var numberOfCombos = 0;
        if (channel) {
          for (var i = 0, j = this.combos.length; i < j; i++) {
            if ((this.combos[i].channel).toLowerCase() === channel) {
              numberOfCombos++;
            }
          }
        }

        return numberOfCombos;
      };


      Campaign.prototype.getFacebookEstimatedReach = function () {

        var self = this;

        self.isLaunchButtonDisabled = true;

        // Make the call only if there are fb Combos
        if (self.settings.channels.facebook) {
          var fbCombosId = '';
          // Prepare comma separated string containing combos ids
          for (var i = 0, j = this.combos.length; i < j; i++) {
            if (this.combos[i].channel === 'FACEBOOK') {
              fbCombosId += ',' + this.combos[i].id;
            }
          }
          // Remove the first comma
          fbCombosId = fbCombosId.substring(1);
          var facebookAdAccountId = this.settings.channels.facebook.adAccount.accountId;

          return $http.get(apiUrl + '/api/v2/campaign/estimate/reach/' + facebookAdAccountId + '/?comboIds=' + fbCombosId).
            success(function (response) {
              self.estimatedReach.facebook = response.data;

            }).error(function (response) {
              console.log(response);
              fsConfirm('genericAlert', {body: $filter('translate')('label.act.fbEstReachFailedMsg')});
            });
        } else {
          self.estimatedReach.facebook = null;
          return $q.when(null);
        }

      };

      Campaign.prototype.getTwitterEstimatedReach = function () {

        var self = this;

        self.isLaunchButtonDisabled = true;

        // Make the call only if there are fb Combos
        if (self.settings.channels.twitter) {

          //make a comma-separated list of all the twitter combo ids.
          var twCombosId = [];
          _.map(this.combos, function (combo) {
            if(combo.channel === 'TWITTER') {
              twCombosId.push(combo.id);
            }
          });
          twCombosId = twCombosId.join();

          var twitterAdAccountId = this.settings.channels.twitter.adAccount.accountId;

          return $http.get(apiUrl + '/api/v2/campaign/estimate/reach/' + twitterAdAccountId + '/?type=twitter&comboIds=' + twCombosId).
            success(function (response) {
              self.estimatedReach.twitter = response.data;

            }).error(function (response) {
              console.log(response);
              fsConfirm('genericAlert', {body: $filter('translate')('label.act.twEstReachFailedMsg')});
            });
        } else {
          self.estimatedReach.twitter = null;
          return $q.when(null);
        }

      };

      Campaign.prototype.getAdwordsBids = function () {
        var self = this;

        // Make the call only if there are fb Combos
        if (self.settings.channels.adwords) {
          var awComboIds = '';
          // Prepare comma separated string containing combos ids
          for (var i = 0, j = this.combos.length; i < j; i++) {
            if (this.combos[i].channel === 'ADWORDS') {
              awComboIds += ',' + this.combos[i].id;
            }
          }
          // Remove the first comma
          awComboIds = awComboIds.substring(1);

          // Todo: return the call promise when adwords bids become necessary for Budget recommendation, when adwords channel is activated (instead of returning $q.when(null)
          $http.get(apiUrl + '/api/v2/campaign/adwords/bids?parentCmpId=' + self.settings.channels.adwords.id + '&comboIds=' + awComboIds).
            success(function () {
              return $q.when(null);
            }).error(function (response) {
              console.log(response);
              return $q.when(null);
            });
        } else { // Return enmpty promise to allow the next method to be called
          return $q.when(null);
        }

      };

      Campaign.prototype.getBudgetRecommendation = function () {

        var self = this;

        // Budget recommendation is working for Facebook only at the time. When we support more channels, we simply need to add an OR clause in the if and add that channel too.
        // The backend will return an unified recommendation for all channels. The applyBudgetRecommendation method won't need to be edited as it already is channel agnostic
        // Disable the Launch Campaign button until we get the response
        self.isLaunchButtonDisabled = true;

        return $http.get(apiUrl + '/api/v2/campaign/budgetRecommendation/' + '?superCmpId=' + this.settings.id).
          success(function (response) {
            self.budgetRecommendation = response.data.result;
            // If a change is required, then the button will still be disabled.
            // IF no change is required, then the button will be enabled and the user can launch the campaign
            self.isLaunchButtonDisabled = self.budgetRecommendation.recommendationRequired;
          }).
          error(function (response) { // optional
            console.log('Error while getting campaign recommendation:', response);
          });

      };

      Campaign.prototype.applyBudgetRecommendation = function (type) {
        var self = this;

        switch (type) {
          case 'budget':
            // Set the budget of the campaign for Cross channel
            if (self.budgetRecommendation.budgetPolicy === 'FLOATING') {
              self.settings.budget += self.budgetRecommendation.raiseCrossChannelBudget;
              self.settings.budget = Math.round(self.settings.budget * 100) / 100;

            } // Set the budget of each channel for multi
            else if (self.budgetRecommendation.budgetPolicy === 'FIXED') {
              _.forOwn(self.budgetRecommendation.mapRaiseMultiChannelBudget, function (budgetToAdd, channel){
                self.settings.channels[channel].budget += budgetToAdd;
                self.settings.channels[channel].budget = Math.round(self.settings.channels[channel].budget * 100) / 100;
              });
            }
            this.saveSettings().then(function() {
              // Either way, mark budget recommendationNeeded as false to hide it from the view
              // And we mark isLaunchButton disabled so the user is able to launch the campaign
              self.budgetRecommendation.recommendationRequired = self.isLaunchButtonDisabled = false;
            });
            break;
          case 'duration':
            // Decrease the number of days
            self.settings.endDate.setDate(self.settings.endDate.getDate() - self.budgetRecommendation.decreaseNumberDays);
            self.saveSettings().then(function() {
              // Either way, mark budget recommendationNeeded as false to hide it from the view
              // And we mark isLaunchButtonDisabled as false too so the user is able to launch the campaign
              self.budgetRecommendation.recommendationRequired = self.isLaunchButtonDisabled = false;
            });
            break;
        }
      };

      Campaign.prototype.saveToLibrary = function () {
        var newData = this;
        var oldName = newData.settings.name;
        withInput.setInput(oldName);

        fsConfirm('confirmationWithInput', {
          body: $filter('translate')('label.act.NameYourCampaign'),
          title: 'My Campaign'
        }).then(function () {

          var newName = withInput.getInput();

          var params = {
            data: angular.toJson(newData),
            objectId: newData.settings.id,
            name: newName,
            libraryObjectType: 'campaign'
          };

          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/library/save/template/' + newData.$$user.currentTeam.id,
            data: $.param(params)
          }).
            success(function () {
              fsNotify.push(
                $filter('translate')('label.act.Campaign') + ' ' + newName + ' ' + $filter('translate')('label.act.savedtolibrary')
              );
            }).
            error(function (response) { // optional
              console.log(response);
            });

        });

      };

// Check that all the combos for this campaign are valid
      Campaign.prototype.areAllCombosValid = function () {
        var areAllCombosValid = true;

        if (!this.combos || this.combos && this.combos.length === 0) {
          return false;
        }
        for (var i = 0, j = this.combos.length; i < j; i++) {
          if (this.combos[i].checkValidity() === false) {
            areAllCombosValid = false;
            break;
          }
        }
        return areAllCombosValid;
      };


      function determineChannelOfVideoGoal(self, goal) {
        var i;

        try {
          for (i = 0; i < self.api.facebook.pages.length; i++) {
            if (self.api.facebook.pages[i].pageId === goal.pageId) {
              return 'FACEBOOK';
            }
          }
        } catch (e) {
          console.log(e);
        }

        try {
          for (var j = 0; j < self.api.twitter.pages.length; j++) {
            if (self.api.twitter.pages[j].socialId === goal.socialAccountId) {
              return 'TWITTER';
            }
          }
        } catch (e) {
          console.log(e);
        }

      }

      function determineIfApplicationExistsForChannel(self, channel) {
        var isAppExisting = false;
        for (var i = 0, j = self.settings.goal.application.channelApplications.length; i < j; i++) {
          if (self.settings.goal.application.channelApplications[i].type.toUpperCase() === channel) {
            isAppExisting = true;
            break;
          }
        }
        return isAppExisting;
      }

      Campaign.prototype.determineIfChannelIsAvailableForGoal = function (channel) {

        switch (channel) {
          case 'adwords':
            if (this.settings.goal && !this.settings.goal.thirdPartyPixel &&
              (this.settings.goal.goal.name === 'WEBSITE_CLICKS' ||
              (this.settings.goal.goal.name === 'WEBSITE_CONVERSIONS' && !this.settings.goal.thirdPartyPixel))) {

              return true;

            } else {
              return false;
            }
            break;

          case 'facebook':
            if (this.settings.goal &&
              (this.settings.goal.goal.name === 'WEBSITE_CLICKS' ||
              this.settings.goal.goal.name === 'WEBSITE_CONVERSIONS' ||
              this.settings.goal.goal.name === 'POST_ENGAGEMENT' ||
              this.settings.goal.goal.name === 'PAGE_LIKES' ||
              ((this.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT' || this.settings.goal.goal.name === 'MOBILE_APP_INSTALLS') && determineIfApplicationExistsForChannel(this, 'FACEBOOK')) ||
              (this.settings.goal.goal.name === 'VIDEO_VIEWS' && determineChannelOfVideoGoal(this, this.settings.goal) === 'FACEBOOK') )) {
              return true;

            } else {
              return false;
            }
            break;

          case 'twitter':
            if (this.settings.goal && !this.settings.goal.thirdPartyPixel &&
              (this.settings.goal.goal.name === 'WEBSITE_CLICKS' ||
              (this.settings.goal.goal.name === 'WEBSITE_CONVERSIONS' && !this.settings.goal.thirdPartyPixel) ||
              this.settings.goal.goal.name === 'TWEET_ENGAGEMENT' ||
              this.settings.goal.goal.name === 'GROW_FOLLOWERS' ||
              ((this.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT' || this.settings.goal.goal.name === 'MOBILE_APP_INSTALLS') && determineIfApplicationExistsForChannel(this, 'TWITTER')) ||
              (this.settings.goal.goal.name === 'VIDEO_VIEWS' && determineChannelOfVideoGoal(this, this.settings.goal) === 'TWITTER') )) {

              return true;

            } else {
              return false;
            }
        }

      };

      Campaign.prototype.activateChannel = function (channel) {
        var self = this;

        if (!this.settings.goal) {
          fsConfirm('genericAlert', {body:$filter('translate')('label.act.selectAGoalFirstMessage')});
          return;
        }

        var confirmCallback = function () {
          $state.go('main.align', {refresh: true});
        };

        //Retrieve adAccounts based on the channel to activate
        var adAccounts = this.api[channel].adAccounts;
        // Retrieve the first valid ad account (which is not flagged)
        var firstValidAdAccountIndex = _.findIndex(adAccounts, {'flagged': false});

        var adAccount;
        if (channel === 'facebook' && this.settings.goal.thirdPartyPixel) {
          adAccount = retrieveFacebookAdAccountWithPixel(this);
        } else if (channel === 'twitter' && this.settings.goal.goal.name === 'TWEET_ENGAGEMENT' || this.settings.goal.goal.name === 'GROW_FOLLOWERS') {
          adAccount = _.find(this.api.twitter.adAccounts, function(adAccount){
            return adAccount.id === self.settings.goal.twitterPromotableUser.twitterAccount.id;
          });
        } else if (this.settings.channels[channel] && this.settings.channels[channel].adAccount) {
          adAccount = this.settings.channels[channel].adAccount;
        } else if (firstValidAdAccountIndex >= 0) {
          adAccount = adAccounts[firstValidAdAccountIndex];
        } else {
          adAccount = null;
        }

        var isChannelAvailableForGoal = this.isChannelAvailableForGoal[channel];
        // adAccount needs to be valid otherwise it means the user doesn't have any account for that channel
        if (this.settings && adAccount && isChannelAvailableForGoal) {
          // Turn channel ON
          this.settings.channels[channel] = {
            channel: channel.toUpperCase(),
            adAccount: adAccount,
            bidType: 'clicks',
            isAutoBid: false
          };

          if (!this.currency || this.currency !== '') {
            this.currency = adAccount.currency;
          }

          // Channel specific settings
          switch (channel) {
            case 'adwords':
              this.settings.channels.adwords.deliveryType = 'SEARCH';
            break;
            case 'facebook':
              // TODO: This will need to be replaced when we switch to tracker = FACEBOOK_CONVERSION_PIXEL
              if (this.settings.goal.goal.name === 'VIDEO_VIEWS' || this.settings.goal.tracker === 'AD_CHANNELS' && this.settings.goal.thirdPartyPixel) {
                this.settings.channels.facebook.bidType = 'absolute_ocpm';
                this.settings.channels.facebook.isAutoBid = true;
              } else {
                //if bidtype exists, use that, otherwise default to clicks
                this.settings.channels.facebook.bidType = this.settings.channels.facebook.bidType || 'clicks';
              }
          }

        } else if (isChannelAvailableForGoal && this.settings.goal !== null && !adAccount) {
          fsConfirm('generic', {
            body: $filter('translate')('label.act.missingAdAccountInOneChannelInCampaignCreationMessage', {channel: $filter('capitalize')(channel)}),
            yes: $filter('translate')('label.act.GoToSetup') // Cancel button is set by default
          }).then(confirmCallback);
        } else if (!isChannelAvailableForGoal) {
          fsConfirm('genericAlert', {body: $filter('capitalize')(channel) + $filter('translate')('label.act.isnotavailableforthisgoal')});
        }
      };

      Campaign.prototype.deactivateChannel = function (channel) {
        // Turn channel OFF
        delete this.settings.channels[channel];

        // Remove all Combos for the channel
        if (_.findIndex(this.combos, {'channel': channel.toUpperCase()}) > -1) {
          this.deleteAllCombosForChannel(channel);
        }
        
        if (this.selectedChannel === channel) { // if selected channel on combos page, deactivate selection
          try {
            this.selectedChannel =  Object.keys(this.settings.channels)[0];
          } catch (e) {
            console.log('User removed all channels. Can\'t set a selected channel now');
          }
        }

        // If there is only one channel, remove the budget at the channel level and set budget policy to FLOATING (one budget)
        if (Object.keys(this.settings.channels).length <= 1) {
          this.settings.budgetPolicy = 'FLOATING';
          for (var lastChannel in this.settings.channels) {
            if (this.settings.channels.hasOwnProperty(lastChannel)) {
              delete this.settings.channels[lastChannel].budget;
            }
            break; // Stop after the first iteration as there is max one channel
          }

          // Remove currency property if all ad accounts are unselected
          if (Object.keys(this.settings.channels).length === 0) {
            this.currency = '';
            this.selectedChannel = null;
          }
        }
      };

      // All ad accounts need to have the same currency
      Campaign.prototype.checkAllSelectedAdAccountsHaveTheSameCurrency = function (channelSettings) {
        // if no campaignSettings is specified, assume it is campaign.setttings
        // In channel settigns controller, we need to check if the "newSettings" are valid before assigning them back to campaign.settings
        if (!channelSettings) {
          channelSettings = this.settings.channels;
        }
        // Define currency as the currency of the first ad account as the reference
        var isCurrencyTheSame = true, currency = null;
        for (var channel in channelSettings) {
          if (channelSettings.hasOwnProperty(channel)) {
            if (!currency) {
              currency = channelSettings[channel].adAccount.currency;

            } else if (currency !== channelSettings[channel].adAccount.currency) {
              isCurrencyTheSame = false;
              fsConfirm('genericAlert', {body: $filter('translate')('label.act.currencyCheckAdvancedSettings')});
              break;
            }
          }
        }
        // Update the campaign currency if necessary
        this.currency = isCurrencyTheSame ? currency : this.currency;

        return isCurrencyTheSame;
      };

      Campaign.prototype.onProjectChange = function () {
        if (this.settings.project) {
          $rootScope.fractalAccountInfo.currentTeam.currentProject = this.settings.project;
        }
      };

      Campaign.prototype.onGoalChange = function () {
        var self = this;
        // When we load a campaign, onGoalChange is triggered unnecessarily. By comparing new goal and goal (which are the same at that point) we avoid showing the popup
        if (!self.settings.goal || (self.settings.goal.id !== self.newGoal.id)) {
          // If the user changes the goal and has already added combo data
          if (self.combos && self.combos.length > 0) {
            // If user confirms, delete all combo data
            var confirmCallBack = function () {
              self.deleteAllCombos().then(function() {
                _.assign(self.settings.goal = self.newGoal);
                activateAllChannelsAvailableForThisGoal(self);
              });
            };
            // If user says no, restore previous goal type
            var cancelCallBack = function () {
              // Revert to previously selected goal
              _.assign(self.newGoal = self.settings.goal);
            };
            fsConfirm('generic', {
              body: $filter('translate')('label.act.Areyousureyouwanttoselectadifferentgoal?Allthecombosdatawillbelost')
            }).then(confirmCallBack, cancelCallBack);
          } else { // If the user changes the goal and doesn't have combo data yet
            // Assign new goal to current goal
            _.assign(self.settings.goal = self.newGoal);
            //manage conversion pixel for that goal
            activateAllChannelsAvailableForThisGoal(self);
          }
        }
      };

      Campaign.getFacebookAdAccounts = function (currentTeam) {
        // Loop through ad accounts in current team's social accounts
        var adAccounts = [];

        if (currentTeam.socialAccounts !== undefined) {
          for (var j = 0, socialLength = currentTeam.socialAccounts.length; j < socialLength; j++) { // Loop through social accounts
            if (currentTeam.socialAccounts[j].fbAccounts !== undefined) {
              for (var k = 0, fbLength = currentTeam.socialAccounts[j].fbAccounts.length; k < fbLength; k++) {
                // If the ad account doesn't have a name, display the accountId

                var adAccount = currentTeam.socialAccounts[j].fbAccounts[k];

                // Make sure the account has not been flagged
                if (!adAccount.flagged) {
                  if (adAccount.name === '') {
                    adAccount.name = 'ID: ' + adAccount.accountId;
                  }
                  adAccounts.push(adAccount);
                }
              }
            }
          }
        }

        return adAccounts;
      };

      Campaign.getFacebookPages = function (currentTeam) {
        // Loop through facebook accounts in current team's social accounts
        var facebookPages = [];

        if (currentTeam.socialAccounts !== undefined) {
          for (var j = 0, socialLength = currentTeam.socialAccounts.length; j < socialLength; j++) { // Loop through social accounts
            if (currentTeam.socialAccounts[j].pages !== undefined) {
              for (var k = 0, fbLength = currentTeam.socialAccounts[j].pages.length; k < fbLength; k++) {
                var page = currentTeam.socialAccounts[j].pages[k];

                // Make sure the account has not been flagged
                if (!page.flagged) {
                  facebookPages.push(page);
                }
              }
            }
          }
        }

        return facebookPages;
      };

      Campaign.getAdwordsAdAccounts = function (currentTeam) {
        // Loop through ad accounts in current team's social accounts
        var adAccounts = [];

        for (var j = 0, socialLength = currentTeam.socialAccounts.length; j < socialLength; j++) { // Loop through social accounts
          if (currentTeam.socialAccounts !== undefined) {
            for (var k = 0, awLength = currentTeam.socialAccounts[j].adwordsAccounts.length; k < awLength; k++) {
              if (currentTeam.socialAccounts[j].adwordsAccounts !== undefined) {

                var adAccount = currentTeam.socialAccounts[j].adwordsAccounts[k];

                // Make sure the account has not been flagged
                if (!adAccount.flagged) {
                  // If the ad account doesn't have a name, display the accountId
                  if (adAccount.name === '') {
                    adAccount.name = 'ID: ' + adAccount.accountId;
                  }
                  adAccounts.push(adAccount);
                }
              }
            }
          }
        }

        return adAccounts;
      };

      Campaign.getTwitterPages = function (currentTeam) {
        var twitterPages = [];
        if (currentTeam.socialAccounts !== undefined) {
          for (var j = 0, socialLength = currentTeam.socialAccounts.length; j < socialLength; j++) { // Loop through social accounts
            if (currentTeam.socialAccounts[j].social.socialType === 'Twitter') {
              var page = currentTeam.socialAccounts[j].social;
              twitterPages.push(page);
            }
          }
        }
        return twitterPages;
      };

      Campaign.getTwitterAdAccounts = function (currentTeam) {
        // Loop through ad accounts in current team's social accounts
        var adAccounts = [];

        for (var j = 0, socialLength = currentTeam.socialAccounts.length; j < socialLength; j++) { // Loop through social accounts
          if (currentTeam.socialAccounts && currentTeam.socialAccounts[j].twAccounts) {
            for (var k = 0, twLength = currentTeam.socialAccounts[j].twAccounts.length; k < twLength; k++) {
              if (currentTeam.socialAccounts[j].twAccounts !== undefined) {

                // If the ad account doesn't have a name, display the accountId
                if (currentTeam.socialAccounts[j].twAccounts[k].name === '') {
                  currentTeam.socialAccounts[j].twAccounts[k].name = 'ID: ' + currentTeam.socialAccounts[j].twAccounts[k].accountId;
                }
                adAccounts.push(currentTeam.socialAccounts[j].twAccounts[k]);
              }
            }

          }
        }
        return adAccounts;
      };

// Internal method to create a campaign settings
      function createCampaignSettings(self) {

        // Convert settings object to settings channel
        var settings = angular.copy(self.settings);
        var channelSettings = $.map(self.settings.channels, function (value) {
          return [value];
        });
        settings.channels = channelSettings;

        var params = {
          data: angular.toJson(settings)
        };

        return $http({
          url: apiUrl + '/api/v2/campaign/create/settings/' + self.$$user.currentTeam.id,
          method: 'POST',
          data: $.param(params)
        }).
          then(function (res) {
            var response = res.data;

            var channelComboPromises = [];

            self.settings.id = response.data.superCmpId;
            // Retrieve parentCmpId and create one combo default for each platform
            // If we are loading a campaign template, then load all combos related to the campaign
            for (var i = 0, j = response.data.parentCmpId.length; i < j; i++) {
              // Retrieve channel object
              var channel = response.data.parentCmpId[i].channel;
              // Retrieve parentCmpId associated to the channel
              var parentCmpId = _.find(response.data.parentCmpId, {'channel': channel}).id;
              // Assign parentCmpId to the relevant channel. Need Lowercase because backend returns channel in CAPS
              self.settings.channels[channel.toLowerCase()].id = parentCmpId;
              // Add combo for the channel
              channelComboPromises.push(self.addCombo(channel));
            }

            var addComboPromise = $q.all(channelComboPromises);
            var notifyName = $filter('stringLimiter')(self.settings.name, 'max', 42);

            addComboPromise.then(function () {
              fsNotify.push(
                $filter('translate')('label.act.Campaign') + ' ' + notifyName + ' ' + $filter('translate')('label.act.settingssaved')
              );
            });

            return addComboPromise;
          },
          function (response) { // optional
            console.log(response);
          });
      }

// Internal method to update a campaign settings
      function updateCampaignSettings(self) {
        // Convert settings object to settings channel
        var settings = angular.copy(self.settings);
        var channelSettings = $.map(self.settings.channels, function (value) {
          return [value];
        });
        settings.channels = channelSettings;

        var params = {
          data: angular.toJson(settings)
        };

        // Disable the Launch Campaign button until we get the response from the budget recommendation
        self.isLaunchButtonDisabled = true;

        return $http({
          url: apiUrl + '/api/v2/campaign/update/settings/' + self.$$user.currentTeam.id,
          method: 'POST',
          data: $.param(params)
        }).
          then(function (res) {
            var response = res.data;

            // Update ids if necessary. For example if a channel has been added in the meantime
            for (var i = 0, j = response.data.parentCmpId.length; i < j; i++) {
              // Retrieve channel object
              var channel = response.data.parentCmpId[i].channel;
              // Retrieve parentCmpId associated to the channel
              var parentCmpId = _.find(response.data.parentCmpId, {'channel': channel}).id;
              // If the channel is new, assign an id and create empty combo
              if (!self.settings.channels[channel.toLowerCase()].id) {
                self.settings.channels[channel.toLowerCase()].id = parentCmpId;
                self.addCombo(channel);

              } // If the user has changed a goal after having saved settings, all combos have been delete. Recreate one here for each channel
              else if (!_.find(self.combos, {channel: channel})) {
                self.addCombo(channel);
              }
            }

            var notifyName = $filter('stringLimiter')(self.settings.name, 'max', 42);

            fsNotify.push(
              $filter('translate')('label.act.Campaign') + ' ' + notifyName + ' ' + $filter('translate')('label.act.settingssaved')
            );
          },
          function (response) { // optional
            console.log(response);
          });
      }

// Function used to convert channels array to an object (when loading campaign data from the backend)
      function convertChannelArrayToObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i) {
          if (arr[i] !== undefined) {
            rv[arr[i].channel.toLowerCase()] = arr[i];
          }
        }
        return rv;
      }

      function initializeCampaignDuration(self) {
        // Setup the dates
        self.settings.startDate = new Date();
        // Set the start date to tomorrow midnight local time
        self.settings.startDate.setDate(self.settings.startDate.getDate() + 1);
        self.settings.startDate.setHours(0, 0, 0, 0);
        // Set end date 24h later
        self.settings.endDate = new Date(self.settings.startDate);
        self.settings.endDate.setDate(self.settings.endDate.getDate() + 1);
      }

      function retrieveFacebookAdAccountWithPixel (self) {
        var pixel = self.settings.goal.thirdPartyPixel;
        var adAccounts = self.api.facebook.adAccounts;

        for (var i = 0, l = adAccounts.length; i < l; i++) {
          if (adAccounts[i].thirdPartyPixels && adAccounts[i].flagged === false) {
            for (var j = 0; j < adAccounts[i].thirdPartyPixels.length; j++) {
              if (pixel.id === adAccounts[i].thirdPartyPixels[j].id) {
                return adAccounts[i];
              }
            }
          }
        }
      }
      function activateAllChannelsAvailableForThisGoal(self) {
        self.isChannelAvailableForGoal = {
          adwords: false,
          facebook: false,
          twitter: false
        };

        // ---------------------------- ADWORDS -------------------------------
        if (self.determineIfChannelIsAvailableForGoal('adwords')) {
          self.isChannelAvailableForGoal.adwords = true;
          if (self.api.adwords.adAccounts && self.api.adwords.adAccounts.length > 0) {
            self.activateChannel('adwords');
          }
        } else if (self.settings.channels.adwords) {
          self.deactivateChannel('adwords');
        }
        // ---------------------------- END ADWORDS -------------------------------
        // ---------------------------- FACEBOOK -------------------------------
        if (self.determineIfChannelIsAvailableForGoal('facebook')) {
          self.isChannelAvailableForGoal.facebook = true;
          if (self.api.facebook.adAccounts && self.api.facebook.adAccounts.length > 0) {
            self.activateChannel('facebook');
          }
        } else if (self.settings.channels.facebook) {
          self.deactivateChannel('facebook');
        }
        // ---------------------------- FACEBOOK -------------------------------
        // ---------------------------- TWITTER -------------------------------
        //Also check the Twitter on/off switch here, so they don't select an existing Twitter goal
        //and it automatically moves the button that's not there
        if (self.determineIfChannelIsAvailableForGoal('twitter')) {
          self.isChannelAvailableForGoal.twitter = true;
          if (self.api.twitter.adAccounts && self.api.twitter.adAccounts.length > 0) {
            self.activateChannel('twitter');
          }
        } else if (self.settings.channels.twitter) {
          self.deactivateChannel('twitter');
        }
        // ---------------------------- END TWITTER -------------------------------
      }

      return Campaign;
    })();

    return Campaign;
  })
;
