'use strict';

describe('Service: Campaign', function () {

  // load the service's module
  beforeEach(module('dashboardApp'));

  // instantiate service
  var Campaign;
  beforeEach(inject(function (_Campaign_) {
    Campaign = _Campaign_;
  }));

  it('should do something', function () {
    expect(!!Campaign).toBe(true);
  });

});
