'use strict';

angular.module('act')
  .controller('CampaigncreationCtrl', function ($scope, Campaign, $auth, ngDialog, $stateParams, $state, library, $rootScope, apiUrl, projectService, $timeout, $filter, campaignInstantiation, PaginatedArray, regex, fsConfirm, experimentalFeatures) {
    $scope.apiUrl = apiUrl;
    // Load or create backend
    $scope.token = $auth.user.access_token;  // jshint ignore:line
    $scope.showInstagram = !!experimentalFeatures.instagram; // || configuration.environment === 'beta' || configuration.environment === 'local';

    // Everything related to the forms
    $scope.form = {};
    // Everything else, usually data kept in the front end.
    $scope.metadata = {
      page: 1,
      numberWithUpToTwoDecimals: regex.NUMBERUPTOTWODECIMALS
    };

    $scope.budgetPolicyRadioToggle = {
      option1: {
        label: 'Cross',
        value: 'FLOATING'
      },
      option2: {
        label: 'Multi',
        value: 'FIXED'
      }
    };

    // The campaign is being created in the resolve method in campaignCreation.js
    $scope.campaign = campaignInstantiation;

//initiate combo pagination
    $scope.fbPaginatedCombos = new PaginatedArray([], {numPerPage: 10});
    $scope.awPaginatedCombos = new PaginatedArray([], {numPerPage: 10});
    $scope.twPaginatedCombos = new PaginatedArray([], {numPerPage: 10});

    $scope.$watchCollection('campaign.combos', function (newValue) {
      var oldLength;
      oldLength = angular.copy($scope.fbPaginatedCombos.array.length);
      $scope.fbPaginatedCombos.array = _.filter(newValue, function (combo) {
        return combo.channel === 'FACEBOOK';
      });
      $scope.fbPaginatedCombos.paginate($scope.fbPaginatedCombos.array.length, oldLength);

      oldLength = angular.copy($scope.twPaginatedCombos.array.length);
      $scope.twPaginatedCombos.array = _.filter(newValue, function (combo) {
        return combo.channel === 'TWITTER';
      });
      $scope.twPaginatedCombos.paginate($scope.twPaginatedCombos.array.length, oldLength);

      oldLength = angular.copy($scope.awPaginatedCombos.array.length);
      $scope.awPaginatedCombos.array = _.filter(newValue, function (combo) {
        return combo.channel === 'ADWORDS';
      });
      $scope.awPaginatedCombos.paginate($scope.awPaginatedCombos.array.length, oldLength);
    });

    $scope.showBulkUpload = ($scope.campaign.selectedChannel === 'adwords' ||
    ($scope.campaign.selectedChannel === 'facebook' &&
    ($scope.campaign.settings.goal.goalId === 4 ||
    $scope.campaign.settings.goal.goalId === 3)));

    $scope.$watch('campaign.selectedChannel', function (newValue) {
      if (newValue === 'adwords') {
        $scope.showBulkUpload = true;
      } else if (newValue === 'facebook' && ($scope.campaign.settings.goal.goalId === 4 ||
        $scope.campaign.settings.goal.goalId === 3)) {
        $scope.showBulkUpload = true;
      } else {
        $scope.showBulkUpload = false;
      }
    });

// End activateChannelIfGoalIsCompatible

    if ($scope.campaign.settings.id) {
      // Refresh the project and the goals inside the saved campaign (template, draft, duplicate
      $scope.campaign.settings.project = _.find($scope.campaign.api.projects, {'id': $scope.campaign.settings.project.id});
      $scope.campaign.settings.goal = _.find($scope.campaign.settings.project.goals, {'id': $scope.campaign.settings.goal.id});
      $scope.campaign.newGoal = _.find($scope.campaign.settings.project.goals, {'id': $scope.campaign.newGoal.id});
    } // else grab the data from current project for new campaigns
    else if ($scope.campaign.settings && !$scope.campaign.settings.id && !$scope.campaign.settings.project && $rootScope.fractalAccountInfo.currentTeam.currentProject) {
      $scope.campaign.settings.project = _.find($scope.campaign.api.projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProject.id});
    }

    $scope.$watch('fractalAccountInfo.currentTeam.currentProject', function (newValue) {
      if (newValue && !$scope.campaign.settings.id) {
        $scope.campaign.settings.project = _.find($scope.campaign.api.projects, {'id': newValue.id});

        if ($scope.campaign.settings.goal) {
          $scope.campaign.settings.goal = _.find($scope.campaign.settings.project.goals, {'id': $scope.campaign.settings.goal.id});
        }
      }
    });

    $scope.metadata.durationDateRangePickerModel = [
      $scope.campaign.settings.startDate,
      $scope.campaign.settings.endDate
    ];

    $scope.metadata.durationDateRangePickerOptions = {
      timePicker: true,
      dateLimit: 0,
      format: 'MM/DD/YYYY h:mm A',
      timePickerIncrement: 5,
      timePicker12Hour: true,
      timePickerSeconds: false,
      minDate: new Date(Date.now() - 100000) // min date is 100 sec before the now date
    };

    $scope.metadata.facebookBidSettingsOptions = {
      option1: {
        label: 'Newsfeed',
        value: 'newsfeed'
      },
      option2: {
        label: 'Right Column',
        value: 'rightColumn'
      }
    };

    $scope.commitCampaignDateRange = function (range) {
      $scope.campaign.settings.startDate = moment(range[0]).seconds(0).toDate();
      $scope.campaign.settings.endDate = moment(range[1]).seconds(0).toDate();

      var todayDate = new Date();
      var startDateCopy = angular.copy(range[0]);
      $scope.metadata.isCampaignStartDateToday = (startDateCopy.setHours(0, 0, 0, 0) === todayDate.setHours(0, 0, 0, 0));
    };

    $scope.loadComboFromLibrary = function () {

      var parentCmpId;
      parentCmpId = $scope.campaign.settings.channels[$scope.campaign.selectedChannel].id;

      var opts = {
        channel: $scope.campaign.selectedChannel,
        parentCmpId: parentCmpId,
        goalId: $scope.campaign.settings.goal.id
      };

      library.openLibrary('Combos', opts).then(function (comboData) {
        $scope.campaign.loadComboFromLibrary(comboData);
      });
    };

    
    $scope.loadCampaignFromLibrary = function () {
      var opts = {};

      library.openLibrary('Campaigns', opts).then(function (campaignData) {

        $scope.campaign.settings.project = _.find($rootScope.fractalAccountInfo.currentTeam.projects, function (project) {
          return $scope.campaign.settings.project.id === project.id;
        });

        for (var channel in $scope.campaign.settings.channels) {
          if ($scope.campaign.settings.channels.hasOwnProperty(channel)) {
            $scope.campaign.selectedChannel = channel;
            break;
          }
        }
        $scope.campaign.loadFromLibrary(campaignData);
      });
    };

    $scope.finishSaveToLibrary = function () {
      ngDialog.close();
    };

// This method is called when the user wants to switch between the 3 pages of campaign creation
// It goes through some checks and if they are successful, redirects to the desire page. If not, show the error to the user
    $scope.validateAndGoToPage = function (pageNumber) {
      // Validate settings page
      $scope.metadata.isEndDateBeforeStartDate = false;
      $scope.metadata.isStartDateBeforeToday = false;

      function validateSettingsPage() {
        var todayDate = new Date();
        todayDate.setHours(0, 0, 0, 0);

        var isSettingsValid = true;

        if ($scope.form.settings.$invalid) { // Stay on the same page and correct errors
          $scope.form.settingsSubmitted = true;
          isSettingsValid = false;
        }
        if (isSettingsValid && $scope.campaign.settings.startDate >= $scope.campaign.settings.endDate) {
          $scope.metadata.isEndDateBeforeStartDate = true;
          isSettingsValid = false;
        }
        if (isSettingsValid && $scope.campaign.settings.startDate < todayDate) {
          $scope.metadata.isStartDateBeforeToday = true;
          isSettingsValid = false;
        }
        if (isSettingsValid && _.size($scope.campaign.settings.channels) > 0) { // The campaign needs to have at least one channel
          $scope.metadata.areChannelsValid = true;
          $scope.form.settingsSubmitted = true;
          // Check if channel info is set properly
          for (var channel in $scope.campaign.settings.channels) {
            if ($scope.metadata.areChannelsValid &&
              $scope.campaign.settings.channels.hasOwnProperty(channel) &&
              $scope.campaign.settings.channels[channel].adAccount && $scope.campaign.settings.channels[channel].adAccount.flagged === false &&
              $scope.campaign.settings.channels[channel].bidType && $scope.campaign.settings.channels[channel].bidType !== null) {
            } else {
              isSettingsValid = $scope.metadata.areChannelsValid = false;
              break;
            }
          }
        }
        if (isSettingsValid) { // We need to check here although we checked in channel settings because the user might start a campaign and not open channel settings at all
          isSettingsValid = $scope.metadata.areAllCurrenciesTheSame = $scope.campaign.checkAllSelectedAdAccountsHaveTheSameCurrency();
        }

        return isSettingsValid;
      }


      function validateCombosPage() {
        var isPageValid = true;

        var i, j = 0;

        if ($scope.campaign.combos.length > 0) {
          for (i = 0, j = $scope.campaign.combos.length; i < j; i++) {
            if (!$scope.campaign.combos[i].name) {
              isPageValid = false;

              fsConfirm('genericAlert', {body: $filter('translate')('label.act.Pleasemakesureeverycombohasaname')});
              break;
            }
          }
        } else { // If the user has never left the settings page and wants to go to the review page
          isPageValid = false;
          fsConfirm('genericAlert', {body: $filter('translate')('label.act.Youneedtocreatecombosfirst')});
          return;
        }

        if (isPageValid) { // Check that every combo has at least one audience and one creative
          var comboInvalidMessages = [];

          for (i = 0, j = $scope.campaign.combos.length; i < j; i++) {
            if (!$scope.campaign.combos[i].checkValidity()) {

              switch ($scope.campaign.combos[i].channel) {
                case 'ADWORDS':
                  comboInvalidMessages.push($filter('translate')('label.act.Thecombo') + ' ' +
                    $scope.campaign.combos[i].name + ' ' +
                    $filter('translate')('label.act.needstohaveoneaudience(includinglocation)andatleastonecreativeandonekeyword')
                  );
                  break;
                case 'FACEBOOK':
                case 'TWITTER':
                  comboInvalidMessages.push($filter('translate')('label.act.Thecombo') + ' ' +
                    $scope.campaign.combos[i].name + ' ' +
                    $filter('translate')('label.act.needstohaveoneaudience(includinglocation)andatleastonecreative')
                  );
                  break;
              }
              isPageValid = false;
            }
          }
          if (comboInvalidMessages.length > 0) {
            fsConfirm('multiLineAlert', {messages: comboInvalidMessages});
          }
        }

        if (isPageValid) {
          return true;
        }
      }

      if ($scope.metadata.page === 1 && pageNumber === 2) {
        if (validateSettingsPage() && $scope.campaign.isReadyForCombosPage) {
          // Disabled buttons to go to Combos page until the endpoint is executed
          $scope.metadata.isButtonToGoToCombosPageDisabled = true;

          // Save settings and move to combos page
          $scope.campaign.saveSettings().then(function () {
            $scope.metadata.isButtonToGoToCombosPageDisabled = false;
            $scope.campaign.settingsPageHasChanged = false;
            $scope.metadata.page = pageNumber;

            if (!$scope.campaign.selectedChannel) { // If the user has never been to the combos or summary page yet
              if ($scope.campaign.settings.channels.facebook) {                        // Determine which tab is open by default in the combos page
                $scope.campaign.selectedChannel = 'facebook';
              } else if ($scope.campaign.settings.channels.adwords) {
                $scope.campaign.selectedChannel = 'adwords';
              } else if ($scope.campaign.settings.channels.twitter) {
                $scope.campaign.selectedChannel = 'twitter';
              }
            }
            // Assign the currency at the campaign level in case the user has not opened channel settings modal
            var channelsObject = $scope.campaign.settings.channels;
            $scope.campaign.currency = $scope.campaign.currency || channelsObject[Object.keys(channelsObject)[0]].adAccount.currency;
          });
        }
      }

      // If the user wants to go to the review page
      else if ($scope.metadata.page === 1 && pageNumber === 3) {
        if (validateSettingsPage() && validateCombosPage()) {
          // Disabled buttons to go to Summary page until the endpoint is executed
          $scope.metadata.isButtonToGoToSummaryPageDisabled = true;

          // Save settings and move to combos page
          $scope.campaign.saveSettings().then(function () {
            $scope.metadata.isButtonToGoToSummaryPageDisabled = false;

            if (!$scope.campaign.selectedChannel) { // If the user has never been to the combos or summary page yet
              if ($scope.campaign.settings.channels.facebook) {                        // Determine which tab is open by default in the combos page
                $scope.campaign.selectedChannel = 'facebook';
              } else if ($scope.campaign.settings.channels.adwords) {
                $scope.campaign.selectedChannel = 'adwords';
              } else if ($scope.campaign.settings.channels.twitter) {
                $scope.campaign.selectedChannel = 'twitter';
              }
            }
            // Assign the currency at the campaign level in case the user has not opened channel settings modal
            var channelsObject = $scope.campaign.settings.channels;
            $scope.campaign.currency = $scope.campaign.currency || channelsObject[Object.keys(channelsObject)[0]].adAccount.currency;

            if (validateCombosPage()) {
              $scope.metadata.page = pageNumber;
              $scope.campaign.getNumberOfSubcampaigns();
              $scope.campaign.getFacebookEstimatedReach()
                .then($scope.campaign.getTwitterEstimatedReach())
                .then($scope.campaign.getAdwordsBids.bind($scope.campaign))
                .then($scope.campaign.getBudgetRecommendation.bind($scope.campaign));
            }
          });
        }
      }

      else if ($scope.metadata.page === 2 && pageNumber === 3) {
        // Will show invalid combos after the user has clicked to go to summary page only
        $scope.metadata.hasClickedToGoToSummaryPage = true;

        // Disabled buttons to go to Summary page until the endpoint is executed
        $scope.metadata.isButtonToGoToSummaryPageDisabled = true;

        if (validateCombosPage()) {
          $scope.metadata.isButtonToGoToSummaryPageDisabled = false;

          $scope.metadata.page = pageNumber;
          $scope.campaign.getNumberOfSubcampaigns();


          //subcampaign limiter for twitter
          if($scope.campaign.numberOfSubcampaigns.twitter > 50){
            fsConfirm('genericAlert', {body: $filter('translate')('label.act.Youcannotpublishmorethan50Twitterads')});
            $scope.campaign.overSubcampaignLimit = true;
          } else {
            $scope.campaign.overSubcampaignLimit = false;
          }

          $scope.campaign.getFacebookEstimatedReach()
            .then($scope.campaign.getTwitterEstimatedReach())
            .then($scope.campaign.getAdwordsBids.bind($scope.campaign))
            .then($scope.campaign.getBudgetRecommendation.bind($scope.campaign));
        }
      }

      else {
        $scope.metadata.page = pageNumber;
      }
    };

    $scope.$watch('campaign.combos', function (newValue) {
      if (newValue) {
        $scope.campaign.getNumberOfSubcampaigns();
      }
    }, true);


// Watch changes in campaign settings. If any of these fields is changed when the user visits the settings page, we call the backend when the user leaves the page
    $scope.$watch('campaign.settings', function (newValue, oldValue) {
      // Compare ids
      if (oldValue && oldValue.id !== newValue.id) {
        // If ids have changed, that means we loaded a new campaign.
        // Therefore it shouldn't be considered as a change and the user can go to the next page without calling the save campaign settings
        $scope.campaign.settingsPageHasChanged = false;
      }

      else if (oldValue && oldValue !== newValue) { // If there is a setting that is changed, other than id, then the settings change will be saved before moving to the next page
        $scope.campaign.settingsPageHasChanged = true;
        // Determine whether the user can move from the settings page or not
        $scope.metadata.isEndDateBeforeStartDate = false;
        $scope.metadata.isStartDateBeforeToday = false;

        // If start date has changed, set end date at start date + 1
        // We divide by 60000 to get the number of minutes
        var minutesBetweenStartDateAndEndDate = (newValue.endDate.getTime() - newValue.startDate.getTime()) / 60000;
        if (oldValue.startDate !== newValue.startDate && minutesBetweenStartDateAndEndDate < 60) {
          // If end date doesn't exist yet, created it and make it one day after start date
          $scope.campaign.settings.endDate = new Date($scope.campaign.settings.startDate);
          $scope.campaign.settings.endDate.setDate($scope.campaign.settings.startDate.getDate());
          $scope.campaign.settings.endDate.setHours($scope.campaign.settings.startDate.getHours() + 1);


          // Update the dates displayed in the dropdown
          $scope.metadata.durationDateRangePickerModel = [
            $scope.campaign.settings.startDate,
            $scope.campaign.settings.endDate
          ];
        }

        if ($scope.campaign.settings.goal &&
          $scope.campaign.settings.channels &&
          $scope.campaign.settings.project &&
          $scope.campaign.settings.name &&
          $scope.campaign.settings.startDate &&
          $scope.campaign.settings.endDate
        ) {
          var todayDate = new Date();
          todayDate.setHours(0, 0, 0, 0);

          $scope.campaign.isReadyForCombosPage = true;

          if ($scope.campaign.isReadyForCombosPage && $scope.campaign.settings.startDate >= $scope.campaign.settings.endDate) {
            $scope.metadata.isEndDateBeforeStartDate = true;
            $scope.campaign.isReadyForCombosPage = false;
          }
          if ($scope.campaign.isReadyForCombosPage && $scope.campaign.settings.startDate < todayDate) {
            $scope.metadata.isStartDateBeforeToday = true;
            $scope.campaign.isReadyForCombosPage = false;
          }

          // The campaign needs to have a budget if budgetPolicy is Floating
          if ($scope.campaign.isReadyForCombosPage && $scope.campaign.settings.budgetPolicy === 'FLOATING' && $scope.campaign.settings.budget <= 0) {
            $scope.campaign.isReadyForCombosPage = false;
          }
          // The campaign needs to have at least one channel
          if ($scope.campaign.isReadyForCombosPage && _.size($scope.campaign.settings.channels) > 0) {
            // Check if channel info is set properly
            for (var channel in $scope.campaign.settings.channels) {
              if ($scope.campaign.settings.channels.hasOwnProperty(channel) &&
                $scope.campaign.settings.channels[channel].adAccount !== null &&
                $scope.campaign.settings.channels[channel].bidType && $scope.campaign.settings.channels[channel].bidType !== null ) {
                  $scope.metadata.areChannelsValid = true;
                  $scope.campaign.isReadyForCombosPage = true;
              } else {
                $scope.metadata.areChannelsValid = false;
                $scope.campaign.isReadyForCombosPage = false;
                break;
              }
            }
          } else {
            $scope.campaign.isReadyForCombosPage = false;
          }

        } else { // If all the fields aren't fulfilled yet
          $scope.campaign.isReadyForCombosPage = false;
        }
      }

    }, true); // Deep watch

// When the user selects a time in the advanced options, assign the time to the start or end date
    $scope.onTimeChange = function (date) {
      if (date === 'start') {
        $scope.campaign.settings.startDate.setHours($scope.metadata.startTime.getHours());
        $scope.campaign.settings.startDate.setMinutes($scope.metadata.startTime.getMinutes());
      } else if (date === 'end') {
        $scope.campaign.settings.endDate.setHours($scope.metadata.endTime.getHours());
        $scope.campaign.settings.endDate.setMinutes($scope.metadata.endTime.getMinutes());
      }
    };

    $scope.editDates = function () {
      // Reset the date range picker with the settings date
      $scope.metadata.durationDateRangePickerModel = [
        $scope.campaign.settings.startDate,
        $scope.campaign.settings.endDate
      ];

      // Storing start date and end date in case user ends up canceling the chances in the modal view
      $scope.metadata.startDateBeforeModal = new Date($scope.campaign.settings.startDate);
      $scope.metadata.endDateBeforeModal = new Date($scope.campaign.settings.endDate);


      ngDialog.open({
        plain: false,
        template: 'app/main/act/campaignCreation/core/templates/changeDateModal.html',
        scope: $scope,
        showClose: false
      });
    };
// Save settings in the summary page and trigger budget recommendation
    $scope.closeDateModal = function (saveSettings) {
      // If the user decides to save the changes, assign the newly selected dates to the settings dates and save the changes
      if (saveSettings) {
        $scope.campaign.saveSettings().then($scope.campaign.getBudgetRecommendation.bind($scope.campaign));
      } else {
        $scope.campaign.settings.startDate = $scope.metadata.startDateBeforeModal;
        $scope.campaign.settings.endDate = $scope.metadata.endDateBeforeModal;
      }
      ngDialog.close();
    };
// End Save settings in the summary page and trigger budget recommendation

    $scope.manageChannelSettings = function () {
      // Create a variable that will be used in channel settings modal to hold on the changes until the user closes the modal
      // If the user saves, the changes are assigned back to campaign.settings.channels If the user cancels (hit on the X), then the original campaign.settings.channels will not be altered
      $scope.newChannelSettings = angular.copy($scope.campaign.settings.channels);
      if ($scope.campaign.settings.goal) {
        ngDialog.open({
          plain: false,
          template: 'app/main/act/campaignCreation/core/channelSettings/channelSettingsModal.html',
          scope: $scope,
          controller: 'ChannelsettingsCtrl',
          className: 'modal'
        });
      } else {
        fsConfirm('genericAlert', {body: $filter('translate')('label.act.Pleaseselectagoalfirst')});
      }
    };

    $scope.publishCampaign = function () {
      // Check the date & time
      //if ($scope.campaign.settings.startTime > )
      $scope.campaign.publish().then(handleCampaignSubmission);
    };

    function handleCampaignSubmission() {
      displayCampaignSubmittedModal();
      redirectToManager();
    }

    function redirectToManager() {
      $state.go('main.act.campaignManager.home', {refresh: true});
    }

    function displayCampaignSubmittedModal() {
      ngDialog.open({
        plain: false,
        template: 'app/main/act/campaignCreation/core/templates/campaignSubmittedModal.html',
        scope: $scope,
        className: 'modal',
        showClose: false
      });

      return $timeout(function () {
        ngDialog.close();
      }, 3000);
    }

    $scope.getSizeOfObject = _.size; // To be able to use it in ngShow

    $scope.openBulkUploadModal = function (channel) {
      //based on selected channel and goal id.
      var template = '';
      switch (channel) {
        case 'adwords':
          template = 'app/main/act/campaignCreation/core/bulkUpload/bulkUploadModalAw.html';
          break;
        case 'facebook':
          template = 'app/main/act/campaignCreation/core/bulkUpload/bulkUploadModalFb.html';
          break;
      }

      ngDialog.open({
        plain: false,
        template: template,
        scope: $scope,
        controller: 'BulkUploadCtrl',
        className: 'modal'
      });
    };

// Animation when adding a combo
    $rootScope.$on('paginateArrayChange', function () {
      // We take the first combo of the list because now when we add a combo, we add it on top of the list
      var combo = $scope.campaign.combos ? $scope.campaign.combos[0] : null;
      if (combo) {
        // Show the animation
        combo.hasJustBeenAdded = true;
        // Hide the animation after 1 sec
        $timeout(function () {
          combo.hasJustBeenAdded = false;
        }, 1000);
      }
    });


  })
;
