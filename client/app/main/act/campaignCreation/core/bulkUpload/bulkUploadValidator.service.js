'use strict';

angular.module('act')
  .service('bulkUploadValidator', function (urlValidator, $filter, bulkUploadAw, bulkUploadFb, CallToAction) {
    //jshint sub: true

    //Should not change the input (fullCSV) in any way, merely check.
    //This may result in some redundancy to unpack the data once here to make sure it's valid, 
    //then unpack it again later and modify it to meet the syntax expected by the backend.
    //This redundancy is preferable to modifying the 'single source of truth'.

    var keywordRegex = new RegExp(/[!@%^*={};~`<>\?¦]/);

    var typeCheck = function (metadata, bulkType, channel) {
      var requiredFields = [];
      var errors = [];
      if (channel === 'adwords') {
        if (bulkType === 'creative') {
          requiredFields = ['Headline', 'Description Line 1', 'Description Line 2', 'Display URL', 'Final URL', 'Combo Name'];
        } else if (bulkType === 'keyword') {
          requiredFields = ['Combo Name', 'Keyword', 'Match Type'];
        }
      } else if (channel === 'facebook') {
        if (bulkType === 'creative') {
          requiredFields = ['Headline', 'Facebook Page', 'Placement Type', 'Display URL', 'Text', 'Combo Name', 'Platform', 'Image'];
        }
      }
      _.forEach(requiredFields, function(field){
        // a rather strict comparison, but we can swing it. 
        if (metadata.fields.indexOf(field) === -1) {
          var errorMessage = $filter('translate')('label.act.bulkUpload.Itlookslikethereisno') + field + 
                             $filter('translate')('label.act.bulkUpload.columnAllheadersfromthetemplate');
          errors.push({'column': field, 'message': errorMessage, 'errorType': 'missingHeader'});
        }
      });
      return errors;
    };

    // Delete this when we accept Right Column again.
    var rightColumnCheck = function (row, index) {
      if (row['Placement Type'].toLowerCase().replace(/\s+/g, '') === 'rightcolumn') {
        var errorMessage = 'On row ' + (index + 2) + ', "Right Column" Placement Types are not supported at the moment.';
        return {'row': index, 'column': 'Placement Type', 'message': errorMessage, 'errorType': 'rightColumn'};
      }
    };

    var keywordCharCheck = function (row, index, bulkType) {
      if (bulkType === 'keyword') {
        if (row.Keyword.match(keywordRegex)) {
          var errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                             $filter('translate')('label.act.bulkUpload.theKeywordcontainsoneofthefollowing');
          return {'row': index, 'column': 'Keyword', 'message': errorMessage, 'errorType': 'badChars'};
        }
      }
    };

    var keywordDupeCheck = function (data, bulkType) {

      var errors = [];
      if(bulkType === 'keyword'){
        //fancy _.groupBy lost the indexes (rows).
        //sometimes just an oldSkool for loop will do it.
        for(var i = 0; i < data.length; i++){
          var rawWord = data[i].Keyword;
          var keyword1 = bulkUploadAw.applyMatchType(data[i].Keyword, data[i]['Match Type']);
          var matchType1 = data[i]['Match Type'];
          var combo1 = data[i]['Combo Name'];
          for(var j = i+1; j < data.length; j++){ //start at i+1, so you don't check or repeat yourself
            var keyword2 = bulkUploadAw.applyMatchType(data[j].Keyword, data[j]['Match Type']);
            var matchType2 = data[j]['Match Type'];
            var combo2 = data[j]['Combo Name'];

            if((combo1 === combo2) && 
               ((keyword2.indexOf('-') === 0 &&
                keyword2.substring(1).trim() === keyword1) ||
               (keyword1.indexOf('-') === 0 &&
                keyword1.substring(1).trim() === keyword2))){

              var errorMessage = $filter('translate')('label.act.bulkUpload.Incombo') + combo1 + 
                                 $filter('translate')('label.act.bulkUpload.thereareconflictingkeywords') + rawWord + 
                                 $filter('translate')('label.act.bulkUpload.parenrows') + (i + 2) + 
                                 $filter('translate')('label.act.bulkUpload.and') + (j + 2) + 
                                 $filter('translate')('label.act.bulkUpload.isusedwithbotha') + matchType1 + 
                                 $filter('translate')('label.act.bulkUpload.anda') + matchType2 + 
                                 $filter('translate')('label.act.bulkUpload.MatchType');
              errors.push({'row': [i,j] , 'column': 'Match Type', 'message': errorMessage, 'errorType': 'contradictingDuplicates'});
            }
          }
        }
      }
      return errors;
    };

    var blankCheck = function (row, index) {
      var errors = [];
      _.map(row, function(value, key, fullRow) {
        var skip = false;
        
        // skip optional fields ( that start with a "(" ), and if the 
        // placement is "Right Column" then the platform can only 
        // be "newsfeed" so we'll be nice and accept a blank platform.
        if (key[0] === '(' ||
            (key === 'Platform' && fullRow['Placement Type'].toLowerCase().replace(/\s+/g, '') === 'rightcolumn')){
          skip = true;
        }
        if (value === '' && !skip) {
          var errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                             $filter('translate')('label.act.bulkUpload.thereisno') + key + 
                             $filter('translate')('label.act.bulkUpload.Allfieldsaremandatory');
          errors.push({'row': index, 'column': key, 'message': errorMessage, 'errorType': 'missingField'});
        }
      });
      return errors;
    };

    var lengthCheck = function (row, index, channel) {
      var lengthArray;
      var errors = [];
      if (channel === 'adwords') {
        lengthArray = [
          {'header': 'Headline',
          'maxLength': 25},
          {'header': 'Description Line 1',
          'maxLength': 35},
          {'header': 'Description Line 2',
          'maxLength': 35},
          {'header': 'Display URL',
          'maxLength': 35}
        ];
        var errorMessage = '';
        var enforced;
        _.map(row, function(value, key) {
          enforced = _.find(lengthArray, {'header': key});
          if (enforced && key === enforced.header && value.length > enforced.maxLength) {
            errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                           $filter('translate')('label.act.bulkUpload.commathe') + key + 
                           $filter('translate')('label.act.bulkUpload.istoolongMaximumlengthis') + enforced.maxLength + 
                           $filter('translate')('label.act.bulkUpload.charactersbutthefieldis') + value.length + 
                           $filter('translate')('label.act.bulkUpload.characterslong');
            errors.push({'row': index, 'column': key, 'length': value.length, 'message': errorMessage, 'errorType': 'tooLong'});
          }
        });
      }
      return errors;
    };


    var urlCheck = function (row, index, bulkType, goalUrl, tracker, channel) {
      if (bulkType === 'creative') {
        if (tracker === 'ACCOMPLICE' || tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
          var checkUrl;
          if (channel === 'adwords') {
            checkUrl = row['Final URL'];
          } else if (channel === 'facebook') {
            checkUrl = row['Display URL'];
          }
          var badUrl = urlValidator.checkUrlDomainMatchesGoalDomain(checkUrl, goalUrl);
          if(badUrl) {
            var errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                               $filter('translate')('label.act.bulkUpload.theFinalURL') + checkUrl + 
                               $filter('translate')('label.act.bulkUpload.doesnotmatchthegoalURL') + goalUrl + ')';
            return {'row': index, 'column': 'Final URL', 'message': errorMessage, 'errorType': 'mismatchedURL'};
          }
        }
      }
    };

    var imageCheck = function (row, index, imageLibrary) {
      var imageName = row['Image'];
      var imageFound = bulkUploadFb.getImageFromName(imageName, imageLibrary);
      if (!imageFound) {
        var errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                           $filter('translate')('label.act.bulkUpload.theimage') + imageName + 
                           $filter('translate')('label.act.bulkUpload.couldnotbefoundpleasemakesurethenames');
        return {'row': index, 'column': 'Image', 'message': errorMessage, 'errorType': 'imageNotFound'};
      }
    };

    var pageCheck = function (row, index, facebookPages) {
      var pageName = row['Facebook Page'];
      var availablePages = facebookPages.map(function(page){
        return '"' + page.pageName + '"';
      });
      if (!_.find(facebookPages, {'pageName': pageName})) {
        var errorMessage;
        if (availablePages.length === 1) {
          errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                         $filter('translate')('label.act.bulkUpload.theFacebookPage') + pageName + 
                         $filter('translate')('label.act.bulkUpload.couldnotbefoundTheonlyFacebook') + availablePages[0];
        } else if (availablePages.length > 1) {
          errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                         $filter('translate')('label.act.bulkUpload.theFacebookPage') + pageName + 
                         $filter('translate')('label.act.bulkUpload.couldnotbefoundTheFacebookPages') + availablePages.join(', ');
        }
        return {'row': index, 'column': 'Facebook Page', 'message': errorMessage, 'errorType': 'wrongPage'};
      }
    };

    var callToActionCheck = function (row, index, goal) {
      var adType = 'pagePost';
      var cta = row['(Call to Action)'];
      if (!cta) {
        return;
      }
      var acceptableCtas = CallToAction.fbCallsToAction(goal, adType);
      var acceptableCtasDisplay = _.map(acceptableCtas, function (cta) {
        return $filter('formatCallToAction')(cta, 'facebook');
      });
      var match = $filter('formatCallToAction')(cta, 'facebook', 'inverse');
      var errorMessage;
      console.log(cta, match, acceptableCtas);
      if (cta !== '' && match && !_.includes(acceptableCtas, match)) {
        errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                       ' the Call to Action "' + cta + '" is not available for this goal. Available options are ' + 
                       acceptableCtasDisplay.join(', ') + ', or you may leave the field blank.';
        //NOTE: if we allow bulk upload for video ad type for website goals, they may not leave the field blank.
        return {'row': index, 'column': '(Call to Action)', 'message': errorMessage, 'errorType': 'badCta'};
      } else if (cta !== '' && !match) {
        errorMessage = $filter('translate')('label.act.bulkUpload.Onrow') + (index + 2) + 
                       ' the Call to Action "' + cta + '" is not a valid facebook Call to Action for any goal. Available options are ' + 
                       acceptableCtasDisplay.join(', ') + ', or you may leave the field blank.';
        //NOTE: if we allow bulk upload for video ad type for website goals, they may not leave the field blank.
        return {'row': index, 'column': '(Call to Action)', 'message': errorMessage, 'errorType': 'badCta'};
      }
    };

    var validate = function (data, bulkType, goalUrl, tracker, channel, metadata, imageLibrary, facebookPages, goal) {
      var validationErrors = [];
      if (channel === 'adwords') {
        validationErrors = typeCheck(metadata, bulkType, channel);
        if (validationErrors.length > 0) {
          // If you don't even have the right columns, everything is
          // going to be messed up, just quit now.
          return validationErrors;
        }
        validationErrors = keywordDupeCheck(data, bulkType);
        //The Main Loop
        _.map(data, function(row, index){
          validationErrors.push(urlCheck(row, index, bulkType, goalUrl, tracker, channel),
                                keywordCharCheck(row, index, bulkType)
                                );
          validationErrors = validationErrors.concat(blankCheck(row, index),
                                                     lengthCheck(row, index, channel));
          //filter for when there is no error found and the function returns 'undefined'
          validationErrors = validationErrors.filter(Boolean);
        });
      } else if (channel === 'facebook') {
        validationErrors = typeCheck(metadata, bulkType, channel);
        if (validationErrors.length > 0) {
          // If you don't even have the correct columns, everything is
          // going to be messed up, just quit now.
          return validationErrors;
        }
        _.map(data, function(row, index){
          //for checks that only return one error (per row):
          validationErrors.push(urlCheck(row, index, bulkType, goalUrl, tracker, channel),
                                pageCheck(row, index, facebookPages),
                                imageCheck(row, index, imageLibrary),
                                callToActionCheck(row, index, goal),
                                //Temporary check for "Right Column"
                                rightColumnCheck(row, index)
                                );
          //for checks that return an array of errors (per row):
          validationErrors = validationErrors.concat(blankCheck(row, index),
                                                     lengthCheck(row, index, channel));
          //filter for when there is no error found and the function returns 'undefined'
          validationErrors = validationErrors.filter(Boolean);
        });
      }
      return validationErrors;
    };

    return {
      typeCheck: typeCheck,
      rightColumnCheck: rightColumnCheck,
      keywordCharCheck: keywordCharCheck,
      keywordDupeCheck: keywordDupeCheck,
      blankCheck: blankCheck,
      lengthCheck: lengthCheck,
      urlCheck: urlCheck,
      imageCheck: imageCheck,
      pageCheck: pageCheck,
      callToActionCheck: callToActionCheck,
      validate: validate
    };
  });
