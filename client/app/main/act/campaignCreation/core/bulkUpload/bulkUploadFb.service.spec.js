'use strict';

describe('Service: bulkUploadFb', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var bulkUploadFb;
  beforeEach(inject(function (_bulkUploadFb_){
    bulkUploadFb = _bulkUploadFb_;
  }));

  it('should have bulkUploadFb be defined', function () {
    expect(bulkUploadFb).toBeDefined();
  });
  describe('translatePlacement', function(){
    it('should turn variations of "Right Column" into "rightColumn"', function(){
      var rawPlacement = 'Right Column';
      var translatedPlacement = bulkUploadFb.translatePlacement(rawPlacement);
      expect(translatedPlacement).toBe('rightColumn');
    });
  });

});