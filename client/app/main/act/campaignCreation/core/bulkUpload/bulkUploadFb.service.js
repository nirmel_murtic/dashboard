'use strict';

angular.module('act')
  .factory('bulkUploadFb', function (FbCombo, FbCreative, $filter) {
    //jshint sub: true
    // allows me to use creative['Headline'] instead of 
    // creative.Headline for stylistic consistency with 
    // the other fields which contain spaces
    return {
      /*---------------------------------------------------------------------------*/
                        /* Facebook Main Function Declaration */
      /*---------------------------------------------------------------------------*/

      setupFbJSON : function (parsedCSVData, imageLibrary, campaign) {
        var deepThis = this;
        var channel = campaign.selectedChannel;
        var parentCmpId = campaign.settings.channels[channel].id;
        var facebookPages = campaign.api.facebook.pages;
        var emptyCombos = _(parsedCSVData)
          .groupBy('Combo Name')
          .map(function(comboGroup, comboName) {
            // Setup the combo object
            // check if that combo already exists and use that if it does
            // otherwise make a new Combo and assign the appropriate values
            var newCombo;
            var oldCombo = _.find(campaign.combos, {'name':comboName});
            if (!!oldCombo) {
              newCombo = oldCombo;
            } else {
              newCombo = new FbCombo();
              newCombo.name = comboName;
              newCombo.superCmpId = campaign.settings.id;
              newCombo.parentCmpId = parentCmpId;  
            }
            // --Creatives--
            var fbCreatives = comboGroup.map(function(creative) {
              var fbCreative = new FbCreative();
              fbCreative.creative.text = creative['Text'];
              fbCreative.creative.headline = creative['Headline'];
              fbCreative.creative.url = creative['Display URL'];
              fbCreative.creative.siteDescription = creative['(Newsfeed Link Description)'] || creative['(News Feed Link Description)'];
              fbCreative.creative.callToAction = creative['(Call to Action)'] ? $filter('formatCallToAction')(creative['(Call to Action)'], 'facebook', 'inverse') : '';
              fbCreative.creative.facebookSponsoredPageId = deepThis.getPageFromName(creative['Facebook Page'], 'id', facebookPages);
              fbCreative.creative.image = deepThis.getImageFromName(creative['Image'], imageLibrary);
              fbCreative.creative.pageTypes = deepThis.getPageTypes(creative['Placement Type'], creative['Platform']);
              fbCreative.creative.post = 'new';
              fbCreative.placement = deepThis.translatePlacement(creative['Placement Type']);
              fbCreative.platform = deepThis.translatePlatform(creative['Platform'], creative['Placement Type']);
              fbCreative.adAccountId = campaign.settings.channels.facebook.adAccount.accountId;
              fbCreative.facebookPage = deepThis.getPageFromName(creative['Facebook Page'], 'page', facebookPages);
              fbCreative.isImageRequired = true;
              //fbCreative.creative.adType = 'pagePost';
              fbCreative.isDestinationDomainTypedDifferentFromGoalDomain = false;
              fbCreative.isValid = true;

              return fbCreative;
            });

            return {
              combo : newCombo,
              creatives : fbCreatives
            };
          })
          .value();
        return emptyCombos;
      },

      /*---------------------------------------------------------------------------*/
                        /* Facebook Helper Function Declarations */
      /*---------------------------------------------------------------------------*/

      translatePlacement : function (rawPlacement) {
        //lowercase it and remove spaces so 'News Feed' and 'Right column' are recognized
        var placement = rawPlacement.toLowerCase().replace(/\s+/g, '');
        if (placement === 'newsfeed') {
          return 'newsfeed';
        } else if (placement === 'rightcolumn') {
          return 'rightColumn';
        } // else (error handling)
      },

      translatePlatform : function (rawPlatform, rawPlacement) {
        var placement = this.translatePlacement(rawPlacement);
        var platform = rawPlatform.toLowerCase().replace(/\s+/g, '');
        var platformList = [];
        if (placement === 'rightColumn') {
          platformList.push('mobile');
        } else if (placement === 'newsfeed') {
          if (platform.indexOf('mobile') !== -1) {
            platformList.push('mobile');
          }
          if (platform.indexOf('desktop') !== -1) {
            platformList.push('desktop');
          }
        }
        return platformList;
      },

      getPageFromName : function (pageName, detailRequested, facebookPages) {
        var page = _.find(facebookPages, {'pageName': pageName});
        if (detailRequested === 'page') {
          return page;
        } else if (detailRequested === 'id') {
          return page.pagePageId;
        } // else (error handling)
      },

      getImageFromName : function (imageName, imageLibrary) {
        return _.result(_.find(imageLibrary, function (image){
          // remove all underscores (which may have been spaces or underscores, yay encoding!)
          var possibleName = image.originalName.replace(/_+|\s+/g, '');
          //remove both underscores and spaces
          //Note: case is preserved. So you can have two different images apple.jpg and Apple.jpg
          var targetName = imageName.replace(/_+|\s+/g, '');
          // remove the extension (eg .jpg, .png) if only one of them has it
          if(targetName.lastIndexOf('.') === -1 && possibleName.lastIndexOf('.') > -1) {
            possibleName = possibleName.substring(0,possibleName.lastIndexOf('.'));
          } else if (targetName.lastIndexOf('.') > -1 && possibleName.lastIndexOf('.') === -1) {
            targetName = targetName.substring(0,targetName.lastIndexOf('.'));
          }
          return (possibleName === targetName);
        }), 'fullGuid');
      },

      getPageTypes : function (rawPlacement, rawPlatform) {
        //  both? feed
        var pageType;
        var placement = this.translatePlacement(rawPlacement);
        var platform = this.translatePlatform(rawPlatform, rawPlacement);
        if (placement === 'rightColumn') {
          pageType = 'rightcolumn';
        } else if (placement === 'newsfeed') {
          if (platform.indexOf('mobile') !== -1 && platform.indexOf('desktop') !== -1) {
            pageType = 'feed';
          } else if (platform.indexOf('mobile') !== -1) {
            pageType = 'mobile';
          } else if (platform.indexOf('desktop') !== -1) {
            pageType = 'desktopfeed';
          }
        }
        return pageType;
      }
    };
  });