'use strict';

describe('Service: bulkUploadValidator', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var bulkUploadValidator;
  beforeEach(inject(function (_bulkUploadValidator_){
    bulkUploadValidator = _bulkUploadValidator_;
  }));

  var validationErrors;
  var error;
  beforeEach(function(){
    validationErrors = [];
    error = undefined;
  });

  it('should have bulkUploadValidator be defined', function () {
    expect(bulkUploadValidator).toBeDefined();
  });
  it('should have validationErrors be empty array', function() {
    expect(validationErrors.length).toBe(0);
  });

  describe('typeCheck', function(){
    it('should not let you submit a csv with the wrong headers', function(){
      var badHeadersAdWords = {fields:['Facebook Page', 'Footline', 'Placement Type', 'Very Descriptive line 1', 'Beginning URL', 'Combo Name', 'Something else']};
      error = bulkUploadValidator.typeCheck(badHeadersAdWords, 'creative', 'adwords');
      expect(error.length).toBe(5);
    });
  });

  describe('rightColumnCheck', function(){
    it('should not let you use the placement type of "Right Column"', function(){
      var row = {'Placement Type': 'Right Column'};
      expect(error).not.toBeDefined();
      error = bulkUploadValidator.rightColumnCheck(row, 0);
      expect(error).toBeDefined();
      expect(error.errorType).toBe('rightColumn');
    });
  });

  describe('keywordCharCheck', function(){
    it('should not let you use keywords with forbidden characters', function(){
      var badKeyword = {Keyword:'C@'};
      error = bulkUploadValidator.keywordCharCheck(badKeyword, 0, 'keyword');
      expect(error).toBeDefined();
      expect(error.errorType).toBe('badChars');
    });
  });

  describe('keywordDupeCheck', function(){
    it('should not let you use the same keyword as a positive and negative keyword of the same match type, in the same combo', function(){
      var badData = [
        {
          'Keyword': 'fruit', 
          'Match Type': 'exact',
          'Combo Name': '1'
        },
        {
          'Keyword': 'fruit', 
          'Match Type': 'negative exact',
          'Combo Name': '1'
        },
        {
          'Keyword': 'fruit', 
          'Match Type': 'negative exact',
          'Combo Name': '2'
        }
      ];
      error = bulkUploadValidator.keywordDupeCheck(badData, 'keyword');
      expect(error).toBeDefined();
      expect(error[0].errorType).toBe('contradictingDuplicates');
      expect(error[0].row).toEqual([0,1]);
      expect(error.length).toBe(1);
    });
    it('should let you have the same keyword as a positive and negative keyword of different match types, in the same combo', function(){
      var okData = [
        {
          'Keyword': 'fruit', 
          'Match Type': 'exact',
          'Combo Name': '1'
        },
        {
          'Keyword': 'fruit', 
          'Match Type': 'negative phrase',
          'Combo Name': '1'
        },
        {
          'Keyword': 'fruit', 
          'Match Type': 'negative exact',
          'Combo Name': '2'
        }
      ];
      error = bulkUploadValidator.keywordDupeCheck(okData, 'keyword');
      expect(error).toBeDefined();
      expect(error.length).toBe(0);
    });
  });

  describe('blankCheck', function(){
    it('should check every mandatory field and ensure it\'s not blank', function(){
      var blankRow = {
        'Combo Name': '',
        'Description Line 1': '',
        'Description Line 2': '',
        'Display URL': '',
        'Final URL': '',
        'Headline': ''
      };
      error = bulkUploadValidator.blankCheck(blankRow, 0);
      expect(error).toBeDefined();
      expect(error.length).toBe(6);
    });
    it('should not have any errors if all fields are filled in', function(){
      var goodRow = {
        'Combo Name': 'Group 1-Fruits',
        'Description Line 1': 'For a limited time, get fruit up to',
        'Description Line 2': '25% off. Free Delivery!',
        'Display URL': 'www.EnjoyFruitsAndVeggies.com',
        'Final URL': 'http://www.EnjoyFruitsAndVeggies.com/fruits/discount25',
        'Headline': 'Try Fresh Fruit'
      };
      error = bulkUploadValidator.blankCheck(goodRow, 0);
      expect(error).toBeDefined();
      expect(error.length).toBe(0);
    });
    it('should allow you to have a blank optional field', function(){
      var optionalRow = {
        '(Newsfeed Link Description)': '',
        'Combo Name': 'Group-1',
        'Display URL': 'www.EnjoyFruitsAndVeggies.com'
      };
      error = bulkUploadValidator.blankCheck(optionalRow, 0);
      expect(error).toBeDefined();
      expect(error.length).toBe(0);
    });
    it('should allow you to have a blank platform if the placement type is "Right Column"', function(){
      var rightColumnRow = {
        'Image': 'HappyApple.jpg',
        'Placement Type': 'Right Column',
        'Platform': ''
      };
      error = bulkUploadValidator.blankCheck(rightColumnRow, 0);
      expect(error).toBeDefined();
      expect(error.length).toBe(0);
    });
  });

  describe('lengthCheck', function(){
    it('should reject fields that are too long for adwords', function(){
      var longRow = {
        'Combo Name': '1',
        'Description Line 1': 'For a limited time, get fruit up to 25%',
        'Description Line 2': 'off. Free Delivery! (if you grow it yourself)',
        'Display URL': 'www.EnjoyFruitsAndVeggiesAndDairy.com',
        'Final URL': 'http://www.EnjoyFruitsAndVeggies.com/fruits/discount25',
        'Headline': 'Try fresh fruit or else!!!!'
      };
      error = bulkUploadValidator.lengthCheck(longRow, 0, 'adwords');
      expect(error).toBeDefined();
      expect(error.length).toBe(4);
    });
    it('should accept fields that are less than the maximum length', function(){
      var goodRow = {
        'Combo Name': 'Group 1-Fruits',
        'Description Line 1': 'For a limited time, get fruit up to',
        'Description Line 2': '25% off. Free Delivery!',
        'Display URL': 'www.EnjoyFruitsAndVeggies.com',
        'Final URL': 'http://www.EnjoyFruitsAndVeggies.com/fruits/discount25',
        'Headline': 'Try Fresh Fruit'
      };
      error = bulkUploadValidator.lengthCheck(goodRow, 0);
      expect(error).toBeDefined();
      expect(error.length).toBe(0);
    });
  });

  describe('urlCheck', function(){
    it('should make sure the domain of the URL matches the domain in the goal if the tracker is Accomplice', function(){
      var bulkType = 'creative',
          tracker = 'ACCOMPLICE',
          channel = 'adwords',
          row = {'Final URL':'http://www.xyz.com'},
          index = 0,
          goalUrl = 'http://www.abc.com'; 

      error = bulkUploadValidator.urlCheck(row, index, bulkType, goalUrl, tracker, channel);
      expect(error).toBeDefined();
      expect(error.errorType).toBe('mismatchedURL');
    });
  });

  describe('pageCheck', function(){
    it('should make sure you specified a facebook page that you have access to', function(){
      var goodPage = {'Facebook Page': 'Banana'};
      var facebookPages = [
        {
          'pageId':'123',
          'pageName': 'BananaRama'
        },
        {
          'pageId':'456',
          'pageName': 'Banana'
        },
        {
          'pageId':'789',
          'pageName': 'Orange'
        }
      ];
      error = bulkUploadValidator.pageCheck(goodPage, 0, facebookPages);
      expect(error).not.toBeDefined();

      var badPage = {'Facebook Page': 'Kumquat'};
      error = bulkUploadValidator.pageCheck(badPage, 0, facebookPages);
      expect(error).toBeDefined();
      expect(error.errorType).toBe('wrongPage');
    });
  });

});