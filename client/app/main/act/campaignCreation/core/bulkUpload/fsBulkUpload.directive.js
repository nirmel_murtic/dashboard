'use strict';

angular.module('act')
  .directive('fsBulkUpload', function ($parse, fsConfirm) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, element, attrs) {
                var fn = $parse(attrs.fsBulkUpload);
                
          element.on('change', function(onChangeEvent) {
            var reader = new FileReader();
                    
            reader.onload = function(onLoadEvent) {
              scope.$apply(function() {
                fn(scope, {$fileContent:onLoadEvent.target.result});
              });
            };

            //check to make sure they uploaded a CSV (or txt should work too)
            var files = (onChangeEvent.srcElement || onChangeEvent.target).files;
            var fileName = files.length ? files[0].name : '';
            var fileExtension = fileName.substr(fileName.lastIndexOf('.')); 
            if (!(fileExtension === '.csv' || fileExtension === '.txt')){
              fsConfirm('genericAlert',{body:'You cannot upload a file of type \'' + fileExtension + '\', only .csv or .txt'});
              console.log('You cannot upload a file of type \'' + fileExtension + '\', only .csv or .txt');
              return;
            }

            var target = onChangeEvent.target || onChangeEvent.srcElement;
            reader.readAsText(target.files[0]);
            target.value = '';

          });
        }
      };
  });