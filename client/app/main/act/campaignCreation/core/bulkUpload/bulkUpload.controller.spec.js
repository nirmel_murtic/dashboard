'use strict';

describe('Controller: BulkUploadCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var BulkUploadCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BulkUploadCtrl = $controller('BulkUploadCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
