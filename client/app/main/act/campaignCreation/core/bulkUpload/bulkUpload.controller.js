'use strict';

angular.module('act')
  .controller('BulkUploadCtrl', function ($q, $scope, fsUtils, Campaign, apiUrl, bulkUploadValidator, 
                                          Combo, ngDialog, Scroll, library, bulkUploadAw, bulkUploadFb, microService){



    $scope.onCsvImport = function ($fileContent, bulkType) { //add error
      /*---------------------------------------------------------------------------*/
                                /* Synchronous Code */
      /*---------------------------------------------------------------------------*/
      // ---- Setup ----
      $scope.processing = true;
      $scope.uploadErrors = null;
      $scope.validateErrors = null;
      var channel = $scope.campaign.selectedChannel;
      var goal = $scope.campaign.settings.goal.goal.name;
      var parentCmpId = $scope.campaign.settings.channels[channel].id;
      var goalUrl = angular.copy($scope.campaign.settings.goal.eventUrl ? 
                                 $scope.campaign.settings.goal.eventUrl : 
                                 $scope.campaign.settings.goal.host);
      var tracker = $scope.campaign.settings.goal.tracker;
      var facebookPages = $scope.campaign.api.facebook.pages;

      //the config for PapaParse
      var config = {
        header: true,
        skipEmptyLines: true,
        complete: function (results) {
          console.log('Parsing complete:', results);
        },
        error: function (error) {
          console.log('Errors encountered:', error);
        }
      };

      // ---- Parse the uploaded file ----
      var parsedContent = Papa.parse($fileContent, config);
      var parsedCSVData = parsedContent.data;
      var parsedCSVMeta = parsedContent.meta;
      $scope.uploadErrors = parsedContent.errors;
      // If there were any errors parsing the csv ("MissingQuotes", 
      // "UndetectableDelimiter", "TooFewFields", or "TooManyFields"),
      // stop everything and tell the user.
      if (!!$scope.uploadErrors.length) {
        console.log('Parsing errors:', $scope.uploadErrors);
        $scope.processing = false;
        return;
      } else if (parsedCSVData.length < 1) {
        $scope.uploadErrors = [{'row':1, 'message': 'We couldn\'t find any data in that file.'}];
        console.log('Parsing errors:', $scope.uploadErrors);
        $scope.processing = false;
        return;
      }

      // ---- Trim everything ----
      parsedCSVData = _.map(parsedCSVData, function(row){
        return _.object(_.map(row, function(value, key){
          return [key, value.trim()];
        }));
      });

      /*---------------------------------------------------------------------------*/
                          /* Main Asynchronous Function Declarations */
      /*---------------------------------------------------------------------------*/

      // ---- Get the images ----
      var fetchImages = function () {
        var imageLibrary = [];
        var deferred = $q.defer();
        library.getLibraryObjectsByType('image', 
          function (data) { // success
            for (var k in data.data) {
              imageLibrary.push(data.data[k]);
            }
            //console.log('view data', imageLibrary);
            deferred.resolve(imageLibrary);
          },
          function(err){
            console.log(err);
          }
        );
        return deferred.promise;
      };

      // ---- Save all the unique Combos ----
      var saveCombos = function (emptyCombos) {
        var saveCombosPromises = _.map(emptyCombos, function(comboWrapper) {
          if (!comboWrapper.combo.id) {
            return comboWrapper.combo.save(parentCmpId);
          } 
          else {
            // could be just $q.when(); because the result of this promise is only 
            // used if the combo doesn't already have an id.  Being in this else
            // statement means this combo does in fact already have an id but 
            // it does nicely show what the response of this promise is comprised of
            return $q.when({
              data : {
                data : {
                  adComboId : comboWrapper.combo.id
                }
              }
            });
          }
        });
        return saveCombosPromises;
      };

      // ---- Save the Creatives to the Combos ----
      // (Ok, Creatives or Keywords)
      var saveCreatives = function (emptyCombos, results) {
        var promises = [];
        results.forEach(function(response, index) {
          var combo, creatives, keywords;

          if(response.reason === null) {
            combo = emptyCombos[index].combo;
            // if the combo doesn't already exist so there is no id,
            // use the response from saveCombosPromises.
            if (!combo.id){
              combo.id = response.value.data.data.adComboId; // lolwut
            }
            // --Creative--
            if (bulkType === 'creative') {
              creatives = emptyCombos[index].creatives;
              for(var i=0; i<creatives.length; i++) {
                promises.push( combo.saveCreative(creatives[i]) );                  
              }
            // --Keyword--
            } else if (bulkType === 'keyword') {
              keywords = emptyCombos[index].keywords;
              promises.push( combo.saveKeyword(keywords) );                      
            } 
          } else {
            emptyCombos[index] = null;
          } 
        });
        return fsUtils.promise.settle(promises);
      };

      // ---- Once everything has saved, do the visual magic to show it on the page ----
      var updateScope = function(emptyCombos) {
        for(var i=0; i<emptyCombos.length; i++) {
          var comboExists = _.find($scope.campaign.combos, {'id':emptyCombos[i].combo.id});
          if(emptyCombos[i] !== null && !comboExists) {
            $scope.campaign.combos.push(emptyCombos[i].combo);
          }
        }
        // Delete that annoying first empty combo.  If user has edited it, 
        // it will ask for confirmation, otherwise it goes silently
        var firstCombo;
        if (channel === 'adwords') {
          firstCombo = _.find($scope.campaign.combos, function (combo) {
            return (combo.channel === 'ADWORDS' && combo.name.substring(0,8) === 'AW Combo' &&
                    combo.audience === null && combo.creativeList.length === 0 &&
                    combo.keyword && combo.keyword.keyword.length === 0);
          });
        } else if (channel === 'facebook') {
          firstCombo = _.find($scope.campaign.combos, function (combo) {
            return (combo.channel === 'FACEBOOK' && combo.name.substring(0,8) === 'FB Combo' &&
                    combo.audience === null && combo.creativeList.length === 0);
          });
        }
        if (!!firstCombo) {
          var showConfirm = false;
          $scope.campaign.deleteCombo(firstCombo, showConfirm);
        }

        $scope.processing = false;
        // Auto-close the dialog and show the amazing results
        ngDialog.close();
        Scroll.bottom();
      };

      /*---------------------------------------------------------------------------*/
                              /* MAIN PROMISE CHAIN */
      /*---------------------------------------------------------------------------*/
      fetchImages().then(function(imageLibrary) {
        var validateErrors = bulkUploadValidator.validate(parsedCSVData, bulkType, goalUrl, 
                                                          tracker, channel, parsedCSVMeta, 
                                                          imageLibrary, facebookPages, goal);
        if(!validateErrors || !validateErrors.length) {
          if (channel === 'adwords') {
            return bulkUploadAw.setupAwJSON(parsedCSVData, $scope.campaign, bulkType);
          } else if (channel === 'facebook') {
            return bulkUploadFb.setupFbJSON(parsedCSVData, imageLibrary, $scope.campaign);
          }
        } else {
          return $q.reject(validateErrors);
        }
      }).then(function(emptyCombos){
          function success(apiResponse) {
            if (apiResponse && apiResponse.status === 'ERROR') {
              // If the webservice is down, we define a new state (unknown) that will mark the creative as valid
              return 'unknown';
            } else {
              var url = apiResponse.api || {};
              return url.found ? 'pass' : 'fail';
            }
          }

          // All of this is to call the microService and check the urls for the tracking script.
          if (tracker === 'ACCOMPLICE' || tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
            var flattenedCreatives = _(emptyCombos).map(function(combo){return combo.creatives;}).flatten().value();
            var error = _.constant('unknown');
            var extractedUrls = _.map(flattenedCreatives, function (creative) {
              if (channel === 'facebook') {
                return creative.creative.url;
              } else if (channel === 'adwords') {
                return creative.creative.destinationUrl;
              }
            });

            return microService.bulkUrlValidation(extractedUrls, success, error).then( function(results) {
              // match the results with the correct 'creatives' objects
              // (note that this changes emptyCombos because these are the 
              // same 'creatives' objects)
              _(flattenedCreatives).zip(results).map(function(pair) {
                pair[0].trackingScriptStateInUrl = pair[1]; //pair[0] is creatives[x], pair[1] is results
                pair[0].isValid = (pair[1] === 'fail') ? false : true;
              }).value();
              var promises = saveCombos(emptyCombos); 
              return fsUtils.promise.settle(promises).then(function(comboId) {
                return saveCreatives(emptyCombos, comboId).then(function(){
                  return updateScope(emptyCombos);
                });
              });
              
            });
          // If they're not tracked by Accomplice, it's much easier.
          } else {
            var promises = saveCombos(emptyCombos); 
            return fsUtils.promise.settle(promises).then(function(comboId) {
              return saveCreatives(emptyCombos, comboId).then(function(){
                return updateScope(emptyCombos);
              });
            });
          }

          //var emptyCombos = assignUrlStatesToCombos(urlStates);
      }).catch(function(validateErrors){
        $scope.validateErrors = validateErrors;
        console.log('Validation errors:', $scope.validateErrors);
        $scope.processing = false;
      });
    };
  });

