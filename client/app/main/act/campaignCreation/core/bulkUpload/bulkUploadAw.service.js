'use strict';

angular.module('act')
  .factory('bulkUploadAw', function (AwCombo, AwCreative, AwKeyword) {
    //jshint sub: true
    // allows me to use creative['Headline'] instead of 
    // creative.Headline for stylistic consistency with 
    // the other fields which contain spaces
    return {

      /*---------------------------------------------------------------------------*/
                        /* AdWords Main Function Declaration */
      /*---------------------------------------------------------------------------*/
      //Create a complicated nested object that has following structure:
      //
      // [
      //   {
      //     combo: {AwCombo Object}, // 1
      //     creatives: [             // or keywords
      //       {AwCreative Object},   // creatives with Combo Name 1
      //       {AwCreative Object},
      //       {AwCreative Object}
      //     ]
      //   },
      //   {
      //     combo: {AwCombo Object}, // 2
      //     creatives: [             // or keywords
      //       {AwCreative Object}    // creatives with Combo Name 2
      //     ]
      //   }
      // ]
      setupAwJSON : function(parsedCSVData, campaign, bulkType) {
        var deepThis = this;
        var emptyCombos = _(parsedCSVData)
          .groupBy('Combo Name')
          .map(function(bulkTypeGroup, comboName) {
            // Setup the combo object
            // check if that combo already exists and use that if it does
            // otherwise make a new Combo and assign the appropriate values
            var newCombo;
            var channel = campaign.selectedChannel;
            var parentCmpId = campaign.settings.channels[channel].id;
            var oldCombo = _.find(campaign.combos, {'name':comboName});
            if (!!oldCombo) {
              newCombo = oldCombo;
            } else {
              newCombo = new AwCombo();
              newCombo.name = comboName;
              newCombo.superCmpId = campaign.settings.id;
              newCombo.parentCmpId = parentCmpId;  
            }
            // --Creatives--
            if (bulkType === 'creative') {
              var awCreatives = bulkTypeGroup.map(function(creative) {
                var awCreative = new AwCreative();
                awCreative.creative.headline = creative['Headline'];
                awCreative.creative.descriptionLine1 = creative['Description Line 1'];
                awCreative.creative.descriptionLine2 = creative['Description Line 2'];
                awCreative.creative.displayUrl = creative['Display URL'];
                awCreative.creative.destinationUrl = creative['Final URL'];
                // isDestinationDomainTypedDifferentFromGoalDomain: false
                // isDisplayDomainTypedDifferentFromGoalDomain: false
                // isDisplayUrlInvalid: false
                // isDisplayUrlWronglyFormatted: false
                // isValid: false
                // trackingScriptStateInUrl: "fail"
                return awCreative;
              });
              return {
                combo : newCombo,
                creatives : awCreatives
              };  
            // --Keywords--
            } else if (bulkType === 'keyword') {
              var awKeyword = new AwKeyword();
              _(bulkTypeGroup)
              // silently eliminate duplicate keywords in the same combo with the same match type
              .uniq(function(keyword){
                return deepThis.applyMatchType(keyword['Keyword'], keyword['Match Type']);
              })
              .map(function(keyword) {
                // deal with Match Type
                var keywordCorrected = deepThis.applyMatchType(keyword['Keyword'], keyword['Match Type']);
                awKeyword.keyword.push({'value':keywordCorrected});
              }).value();
              return {
                combo : newCombo,
                keywords : awKeyword
              };
            }
          })
          .value(); 
        return emptyCombos;
      },

      /*---------------------------------------------------------------------------*/
                        /* AdWords Helper Function Declaration */
      /*---------------------------------------------------------------------------*/

      // function to map the Match Types to the keywords before 
      // we send them.  
      // Turns out Excel does not play well with quotes and 
      // square brackets and minus signs, so the workaround
      // for marketers is to keep two columns of Keywords 
      // and Match Types (It's also more explicit and sortable
      // which is nice too).  We send just the strings full of 
      // symbols attached to AdWords and that seems to work well, 
      // so here we convert from marketer-version to API version
      // e.g.  'yoga' | 'exact'  -> '[yoga]'
      applyMatchType : function (keyword, matchType) {
        //clean up the "Keyword"
        keyword = keyword.trim();
        //clean up the "Match Type"
        matchType = matchType.toLowerCase().trim();
        //hope it's one of these options
        if (matchType === 'negative' || 
            matchType === 'negative match' ||
            matchType === 'negative broad match') {
          keyword = '-' + keyword;
          return keyword;
        } else if (matchType === 'exact' || 
                   matchType === 'exact match') {
          keyword = '[' + keyword + ']';
          return keyword;
        } else if (matchType === 'phrase' || 
                   matchType === 'phrase match') {
          keyword = '\"' + keyword + '\"';
          return keyword;
        } else if (matchType === 'broad match modifier' || 
                   matchType === 'broad match modified') {
          keyword = '+' + keyword;
          return keyword;
        } else if (matchType === 'negative exact' || 
                   matchType === 'negative exact match') {
          keyword = '-[' + keyword + ']';
          return keyword;
        } else if (matchType === 'negative phrase' || 
                   matchType === 'negative phrase match') {
          keyword = '-\"' + keyword + '\"';
          return keyword;
        } else { // 'broad' or none of the above
          return keyword;
        }
      }
    };
  });