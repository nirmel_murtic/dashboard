'use strict';

describe('Controller: ChannelsettingsCtrl', function () {

  // load the controller's module
  beforeEach(module('dashboardApp'));

  var ChannelsettingsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ChannelsettingsCtrl = $controller('ChannelsettingsCtrl', {
      $scope: scope
    });
  }));

});
