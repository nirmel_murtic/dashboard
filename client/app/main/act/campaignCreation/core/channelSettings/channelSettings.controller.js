'use strict';

angular.module('act')
    .controller('ChannelsettingsCtrl', function ($scope, ngDialog, fsConfirm, $filter) {

        $scope.twitterSpecificGoal = false;

        $scope.metadata.facebookBidTypeOptions = {
            option1: {
                label: 'CPC',
                value: 'clicks'
            },
            option2: {
                label: 'CPM',
                value: 'impressions'
            },
            option3: {
                label: 'oCPM',
                value: 'absolute_ocpm'
            }
        };

        if($scope.newChannelSettings.facebook){
            if ($scope.newChannelSettings.facebook.bidType === 'clicks'){
                $scope.metadata.facebookBidTypeViewValue = 'CPC';
            } else if ($scope.newChannelSettings.facebook.bidType === 'impressions') {
                $scope.metadata.facebookBidTypeViewValue = 'CPM';
            } else if ($scope.newChannelSettings.facebook.bidType === 'absolute_ocpm') {
                $scope.metadata.facebookBidTypeViewValue = 'oCPM';
            }            
        }
        
        if($scope.campaign.settings.goal.tracker !== 'AD_CHANNELS' && 
            $scope.campaign.settings.goal.goal.name !== 'MOBILE_APP_INSTALLS' &&
            $scope.campaign.settings.goal.goal.name !== 'MOBILE_APP_ENGAGEMENT' ){
            delete $scope.metadata.facebookBidTypeOptions.option3;
        }

        if ($scope.campaign.settings.channels.twitter && ($scope.campaign.settings.goal.goal.name === 'TWEET_ENGAGEMENT' || 
            $scope.campaign.settings.goal.goal.name === 'GROW_FOLLOWERS')) {

            $scope.twitterSpecificGoal = true;

            $scope.newChannelSettings.twitter.adAccount = _.find($scope.campaign.api.twitter.adAccounts, function(adAccount){
                return adAccount.id === $scope.campaign.settings.goal.twitterPromotableUser.twitterAccount.id;
            });
            _.assign($scope.campaign.settings.channels = $scope.newChannelSettings);
        } 

        $scope.form = {};

        // ---------------------- Utility functions ---------------------------
        function verifyMaxBidAndCpaLimitForAllChannels() {
            for (var channel in $scope.newChannelSettings) {
                if ($scope.newChannelSettings.hasOwnProperty(channel)) {
                    clearMaxBidIfNeeded(channel);
                    clearCpaLimitIfNeeded(channel);
                }
            }
            // Facebook only
            if ($scope.newChannelSettings.facebook) {
                clearConversionValueIfNeeded();
                determineAutoBidStrategy();
            }
        }
        // For the following methods, we clear the fields either if the checkbox is ticked but there is no value or user typed some value (auto-ticked) then user unticked
        // Facebook only
        function clearConversionValueIfNeeded() {
            if ($scope.newChannelSettings.facebook.isConversionValue && !$scope.newChannelSettings.facebook.conversionValue || !$scope.newChannelSettings.facebook.isConversionValue) {
                $scope.newChannelSettings.facebook.isConversionValue = false;
                delete $scope.newChannelSettings.facebook.conversionValue;
            }
        }
        function clearMaxBidIfNeeded (channel) {
            if ($scope.newChannelSettings[channel].isMaxBid && !$scope.newChannelSettings[channel].maxBid || !$scope.newChannelSettings[channel].isMaxBid) {
                $scope.newChannelSettings[channel].isMaxBid = false;
                delete $scope.newChannelSettings[channel].maxBid;
            }
        }
        function clearCpaLimitIfNeeded (channel) {
            if ($scope.newChannelSettings[channel].isCpaLimit && !$scope.newChannelSettings[channel].cpaLimit || !$scope.newChannelSettings[channel].isCpaLimit) {
                $scope.newChannelSettings[channel].isCpaLimit = false;
                delete $scope.newChannelSettings[channel].cpaLimit;
            }
        }

        // Facebook only for now
        function determineAutoBidStrategy () {
            if ($scope.newChannelSettings.facebook.bidType === 'absolute_ocpm'  && !$scope.newChannelSettings.facebook.isConversionValue) {
                $scope.newChannelSettings.facebook.isAutoBid = true;
            } else {
                $scope.newChannelSettings.facebook.isAutoBid = false;
            }
            // Here we will handle isAutoBid for CPC & CPM eventually
        }
        // ---------------------- End Utility functions ---------------------------

        $scope.closeChannelSettingsModal = function(isSaved) {

            if (isSaved) {
                $scope.form.channelSettingsSubmitted = true;

                if ($scope.form.channelSettings.$valid && $scope.campaign.checkAllSelectedAdAccountsHaveTheSameCurrency($scope.newChannelSettings)) {

                  // Uncheck CPC max bid and CPA limit checkboxes if the user left the input field empty for all channels
                  verifyMaxBidAndCpaLimitForAllChannels();

                  // Assign new channel settings to channel settings
                  _.assign($scope.campaign.settings.channels = $scope.newChannelSettings);

                  ngDialog.close();
                }
            }
        };

        $scope.setFocusOnElementIfCheckboxIsChecked = function (element, isChecked){
            if (isChecked) {
                angular.element('#'+element).focus();
            }
        };

        $scope.onFbAdAccountChange = function () {
            if(!$scope.campaign.combos || 
                $scope.newChannelSettings.facebook.adAccount.id === 
                $scope.campaign.settings.channels.facebook.adAccount.id){
                return;
            }

            function findOrDeleteCustomAudiences(del){
                var customAudienceVariants = [];
                var audiences = _.chain($scope.campaign.combos).pluck('audience').flatten().value();
                var variants = _.chain(audiences).pluck('audience').flatten().value();    
                for(var i = 0; i < variants.length; i++){
                    for(var variantType in variants[i]){
                        if(variantType === 'customAudiences' || variantType === 'excludedCustomAudiences'){
                            if(del){
                                delete variants[i][variantType];
                            } else {                                
                                customAudienceVariants.push(variants[i][variantType]);
                            }
                        }
                    }                                    
                }
                return customAudienceVariants;
            }

            var customAudienceVariants = findOrDeleteCustomAudiences();
            if(customAudienceVariants){
                fsConfirm('generic',{
                    body: $filter('translate')('label.act.Areyousureyouwanttochangeadaccounts?Yourcustomaudienceswillbeinvalidandwipedfromyouraudience')
                }).then(function(){
                    findOrDeleteCustomAudiences(true);
                }, function(){
                    return;
                });
            }
        };

        $scope.onTwAdAccountChange = function () {

            var combosWithCreativesWithDifferentHandles = _.filter($scope.campaign.combos, function (combo) {
              var creativeWithDifferentHandle;
              _.forEach(combo.creativeList, function (creative) {
                if (creative.twitterHandle) {
                  creativeWithDifferentHandle = (creative.twitterHandle.screenName !== $scope.newChannelSettings.twitter.adAccount.promotableUsers[0].screenName);
                  return creativeWithDifferentHandle;
                }
              });
              return (creativeWithDifferentHandle);
            });

            if ($scope.campaign.settings.channels.twitter && 
                $scope.campaign.combos && 
                $scope.campaign.combos.length > 0 &&
                combosWithCreativesWithDifferentHandles.length > 0) {


              //If user says yes, delete all the twitter combos.
              var confirmCallBack = function () {
                $scope.campaign.deleteAllCombosForChannel('twitter');
              };
              // If user says no, restore previous Ad Account
              var cancelCallBack = function () {
                // Revert to previously selected adAccount
                _.assign($scope.newChannelSettings.twitter.adAccount = $scope.campaign.settings.channels.twitter.adAccount);
              };
              fsConfirm('generic', {
                body: $filter('translate')('label.act.AreyousureyouwanttoselectadifferentTwitterAdAccount?AllthecombosdatawillbelostforTwitter')
              }).then(confirmCallBack, cancelCallBack);
            }
        };

// ---------------------------- Watchers ------------------------

        //If they have entered a valid number in MaxBid or CpaLimitt, then automatically check the box for them
        $scope.$watch('newChannelSettings.facebook.maxBid', function (newValue) {
            if ($scope.newChannelSettings.facebook && $scope.form.channelSettings) {
                $scope.newChannelSettings.facebook.isMaxBid = (newValue && $scope.form.channelSettings.fbMaxBid.$valid && $scope.form.channelSettings.fbMaxBid.$viewValue.length > 0);
            }
        });

      $scope.$watch('newChannelSettings.facebook.conversionValue', function (newValue) {
          if ($scope.newChannelSettings.facebook && $scope.form.channelSettings) {
              $scope.newChannelSettings.facebook.isConversionValue = (newValue && $scope.form.channelSettings.fbConversionValue.$valid && $scope.form.channelSettings.fbConversionValue.$viewValue.length > 0);
          }
      });

        $scope.$watch('newChannelSettings.facebook.cpaLimit', function (newValue) {
            if ($scope.newChannelSettings.facebook && $scope.form.channelSettings) {
                $scope.newChannelSettings.facebook.isCpaLimit = (newValue && $scope.form.channelSettings.fbCpaLimit.$valid && $scope.form.channelSettings.fbCpaLimit.$viewValue.length > 0);
            }
        });

        $scope.$watch('newChannelSettings.adwords.maxBid', function (newValue) {
            if ($scope.newChannelSettings.adwords && $scope.form.channelSettings) {
                $scope.newChannelSettings.adwords.isMaxBid = (newValue && $scope.form.channelSettings.awMaxBid.$valid && $scope.form.channelSettings.awMaxBid.$viewValue.length > 0);
            }
        });

        $scope.$watch('newChannelSettings.adwords.cpaLimit', function (newValue) {
            if ($scope.newChannelSettings.adwords && $scope.form.channelSettings) {
                $scope.newChannelSettings.adwords.isCpaLimit = (newValue && $scope.form.channelSettings.awCpaLimit.$valid && $scope.form.channelSettings.awCpaLimit.$viewValue.length > 0);
            }
        });

        $scope.$watch('newChannelSettings.twitter.maxBid', function (newValue) {
            if ($scope.newChannelSettings.twitter && $scope.form.channelSettings) {
                $scope.newChannelSettings.twitter.isMaxBid = (newValue && $scope.form.channelSettings.twMaxBid.$valid && $scope.form.channelSettings.twMaxBid.$viewValue.length > 0);
            }
        });

        $scope.$watch('newChannelSettings.twitter.cpaLimit', function (newValue) {
            if ($scope.newChannelSettings.twitter && $scope.form.channelSettings) {
                $scope.newChannelSettings.twitter.isCpaLimit = (newValue && $scope.form.channelSettings.twCpaLimit.$valid && $scope.form.channelSettings.twCpaLimit.$viewValue.length > 0);
            }
        });
// ---------------------------- Watchers ------------------------
    });
