'use strict';

angular.module('act')
  .service('Creative', function (fsConfirm, $filter, withInput, ngDialog, $http, apiUrl, $rootScope, fsNotify) {
    // AngularJS will instantiate a singleton by calling "new" on this function

        var Creative;

        Creative = (function () {
            function Creative(creative) {

                if (creative) {
                    for (var property in creative) {
                        if (creative.hasOwnProperty(property)) {
                            this[property] = creative[property];
                        }
                    }
                    return;
                }

                this.id = null;
                this.isValid = true;
                // All info related to the creative stored here
            }

            return Creative;
        })();

        Creative.prototype.saveToLibrary = function(channel){
            var newData = this;

            fsConfirm('confirmationWithInput', {
              body: $filter('translate')('label.act.Nameyourcreative'),
              title: 'My Creative'
            }).then(function () {

              newData.name = withInput.getInput();
              
              var params = {
                channelName: channel,
                objectId: 0,
                libraryObjectType: 'creative',
                data: angular.toJson(newData)
              };

              $http({
                method: 'POST',
                url: apiUrl + '/api/v2/library/save/template/' + $rootScope.fractalAccountInfo.currentTeam.id,
                data: $.param(params)
              }).
                success(function (response) {

                  console.log('Audience response: ', response);
                  ngDialog.close();
                  fsNotify.push(
                    $filter('translate')('label.act.Creativesavedtolibrary')+': '+ newData.name
                  );
                  withInput.destroyInput();
                }).
                error(function (response) {
                  withInput.destroyInput();
                  console.log(response);
                });
            });
        };

        return Creative;
  });
