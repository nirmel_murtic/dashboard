'use strict';

angular.module('act')
    .service('TwCreative', function ($http, apiUrl, fsUtils, $sce, Creative) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        var TwCreative;

        TwCreative = (function () {
            function TwCreative(creative) {

                // Call the super constructor Creative
                this.constructor.super_.call(this);

                if (creative) {
                    for (var property in creative) {
                        if (creative.hasOwnProperty(property)) {
                            this[property] = creative[property];
                        }
                    }
                    return;
                }

                this.id = null;
                // All info related to the creative stored here.
                this.creative = {
                    tweetText: null,
                    tweetURL: null,
                    image: null,
                    tweetCardHeadline: null,
                    tweetCardCallToAction: null,
                    tweetCardType: null,
                    tweetCardName: null,
                    tweetCardURL: null,
                    isCard: false
                };
                this.twitterHandle = {};
            }

            // Implement inheritance from Creative abstract class
            fsUtils.inherits(TwCreative, Creative);

            return TwCreative;
        })();

        TwCreative.prototype.getPreview = function(accountId){
            var self = this;
            // if(self.creative.twitterSponsoredObjectId){
                $http({
                  url: apiUrl + '/api/v2/campaign/tweet/preview/' + accountId + '?creativeIds=' + self.id,
                  method: 'GET'
                }).success(function(response){
                    self.previews = response.data;
                });
            // }
        };

        TwCreative.prototype.removeImage = function () {
            this.creative.image = null;
        };

        TwCreative.prototype.getAllPostsTwitter = function (accountId, goalId, cursor) {
            var params = {
                goalId: goalId,
                cursor: cursor,
                count: 20
            };
            return $http({
                method: 'GET',
                url: apiUrl + '/api/v2/campaign/twitter/existingTweets/' + accountId,
                params: params
            });
        };

      return TwCreative;
    });
