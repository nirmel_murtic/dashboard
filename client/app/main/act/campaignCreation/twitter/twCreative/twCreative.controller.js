'use strict';

angular.module('act')
  .controller('TwCreativeCtrl', function (apiUrl, $scope, TwCreative, ngDialog, $http, fsConfirm, $filter, regex, microService, urlValidator, modelOptions, library, CallToAction) {

    var goal = $scope.campaign.settings.goal.goal.name;
    var goalUrl = $scope.campaign.settings.goal.eventUrl ? $scope.campaign.settings.goal.eventUrl : $scope.campaign.settings.goal.host;

    $scope.shortUrlRegex = regex.DIEGOSHORTURL.source;
    $scope.fullUrlRegex = regex.FULLURL.source;
    $scope.tweetUrlRegex = regex.TWEETSHORTURL.source;



    $scope.getImageEndpoint = apiUrl + '/api/v2/image/get?guid=';

    $scope.selectedApp = $scope.campaign.newGoal.application ? $scope.campaign.newGoal.application.name : '';

    $scope.newOrExistingPostRadioToggle = {
      option1: {
        label: 'New',
        value: 'new'
      },
      option2: {
        label: 'Existing',
        value: 'existing'
      }
    };

    $scope.websiteCallsToAction = CallToAction.twCallsToAction('website');

    $scope.appCallsToAction = CallToAction.twCallsToAction('app');

    function formatTwitterDate(startDate) {
      var shortMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
        'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      var date = shortMonthNames[startDate.getMonth()] + ' ' + startDate.getDate();
      return date;
    }

    if (!!$scope.campaign.settings.channels.twitter) {
      //Snag the info needed for the twitter ad preview (Twitter profile pic, handle, user name, and campaign launch date)
      //The html templates for the previews suck these values in
      if ($scope.campaign.settings.channels.twitter.adAccount.promotableUsers[0].screenName ===
          $scope.campaign.settings.channels.twitter.adAccount.social.socialUsername) {
        $scope.tweetPreviewImage = $scope.campaign.settings.channels.twitter.adAccount.social.image || 'http://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png';
      } else {
        $scope.tweetPreviewImage = 'http://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png';
      }
      $scope.tweetPreviewHandle = $scope.campaign.settings.channels.twitter.adAccount.promotableUsers[0].screenName;
      $scope.tweetPreviewName = $scope.campaign.settings.channels.twitter.adAccount.name;
      $scope.tweetPreviewDate = formatTwitterDate($scope.campaign.settings.startDate);
    }

    // Assign selected twitter page to a scope variable to display the name and the picture in the FbCreative view. Assign the FB page id in the creative model
    function getExistingTweets () {
      // var limit = 20;
      // limitPosts = 40;
      // For post engagement, load the posts
      delete $scope.existingTweetCursor;
      $scope.newCreative.getAllPostsTwitter($scope.campaign.settings.channels.twitter.adAccount.accountId,
                                            $scope.campaign.settings.goal.goalId,
                                            $scope.existingTweetCursor)
        .success(function (response) {
          // If the post doesn't have a name, show the post id in lieu of the name
          // for (var i = 0, j = response.data.listExistingTweets.length; i < j; i++) {
          //     if (! response.data.listExistingTweets[i].name) {
          //         response.data.listExistingTweets[i].name = 'Tweet ID: '+response.data.listExistingTweets[i].feedId;
          //     }
          // }

          $scope.existingTweetCursor = response.data.cursor;
          $scope.campaign.api.tweets = response.data.listExistingTweets;
          $scope.campaign.api.currentTwitterHandle = $scope.newCreative.twitterHandle;
          if(!$scope.existingTweetCursor) {
            $scope.stopLoading = true;
          }
        });
    }

    //Load more tweets for infinite scroll
    $scope.loadMoreTweets = function() {
      if($scope.stopLoading) {
        return;
      }
      if($scope.loadingPosts === true) {
        return;
      }

      $scope.loadingPosts = true;

      $scope.newCreative.getAllPostsTwitter($scope.campaign.settings.channels.twitter.adAccount.accountId, $scope.campaign.settings.goal.goalId, $scope.existingTweetCursor).success(function (response) {

        $scope.loadingPosts = false;

        $scope.existingTweetCursor = response.data.cursor;

        if(!$scope.existingTweetCursor) {
          $scope.stopLoading = true;
        }        
        $scope.campaign.api.tweets = $scope.campaign.api.tweets.concat(response.data.listExistingTweets);
      });
    };


    $scope.selectTweetForCreative = function (tweet) {
      if (tweet) {
        $scope.newCreative.creative.twitterSponsoredObjectId = tweet.tweetId;
        $scope.newCreative.selectedTweet = tweet;
      }
    };

    function noTweetsOrDifferentHandle(){
      if(!$scope.campaign.api.tweets){
        return true;
      }
      else if($scope.campaign.api.currentTwitterHandle.id !== $scope.newCreative.twitterHandle.id){
        return true;
      }
      return false;
    }


    var openCreativeModal = function () {
      $scope.loadingPosts = false;
      $scope.stopLoading = false;
      $scope.newCreative.twitterHandle = $scope.campaign.settings.channels.twitter.adAccount.promotableUsers[0];

      //only get existing tweets if not accomplice trackers goal
      if($scope.campaign.settings.goal.tracker !== 'ACCOMPLICE' && noTweetsOrDifferentHandle()){
        getExistingTweets();
      }

      ngDialog.open({
        template: 'app/main/act/campaignCreation/twitter/twCreative/templates/twCreativeModal.html',
        className: 'modal modal-small',
        scope: $scope,
        closeByDocument: false
      });
    };

    // Open TwCreative Modal to add creatives
    $scope.addCreative = function (combo) {
      $scope.urlModelOptions = modelOptions.newInstance().creativeUrl;
      // Declare the new creative object that will be used in the creative modal view
      $scope.newCreative = new TwCreative();
      $scope.currentCreative = null;
      // if it's a mobile goal, set isCard to true, otherwise initialize to false and they have a checkbox to choose.
      $scope.isCard = $scope.newCreative.creative.isCard = !!($scope.campaign.settings.goal.goal.name === 'MOBILE_APP_INSTALLS' ||
      $scope.campaign.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT');
      $scope.metadata.currentCombo = combo;
      $scope.newCreative.creative.post = 'new';
      openCreativeModal();
    };

    // Open TwCreative Modal to add creatives
    $scope.editCreative = function (combo, creative) {      
      $scope.urlModelOptions = modelOptions.newInstance().creativeUrl;
      $scope.metadata.currentCombo = combo;
      $scope.currentCreative = creative;
      $scope.newCreative = angular.copy(creative);
      $scope.isCard = $scope.newCreative.creative.isCard;
      openCreativeModal();
    };

    $scope.saveCreative = function (creativeForm, saveBox) {

      $scope.newCreative.isValid = true;
      creativeForm.submitted = true;

      //Validity checks for elements that are tough to check (<image> does not have 'required' option)
      if ($scope.newCreative.creative.isCard && !$scope.newCreative.creative.image) {
        //If it is a card and there is no image, the form is invalid
        creativeForm.$setValidity('creativeForm.twImage', false);
      } else if ($scope.imageErrorWidthOrHeight) {
        //If the image is too small in either dimension, invalid
        creativeForm.$setValidity('creativeForm.twImage', false);
      } else {
        creativeForm.$setValidity('creativeForm.twImage', true);
      }

      if ($scope.newCreative.creative.isCard && !$scope.newCreative.creative.tweetCardCallToAction) {
        //If it is a card and there is no callToAction, the form is invalid
        $scope.newCreative.isValid = false;
      }
      if(!$scope.newCreative.creative.isCard && $scope.newCreative.creative.tweetURL === -1){
        // It's not a card and there is no URL in the tweet.
        $scope.newCreative.isValid = false;
      }

      //check to see if tweet text length over 140 or over 116 (if image/website card)
      if($scope.determineTweetTextLength() < 0){
        $scope.newCreative.isValid = false; 
      }
    
      $scope.newCreative.isValid = $scope.newCreative.isValid && !creativeForm.$invalid;
      // Validate URL and make sure it has a tracking script if tracked by accomplice
      if ((goal === 'WEBSITE_CONVERSIONS' || goal === 'WEBSITE_CLICKS')&& $scope.newCreative.isValid && $scope.newCreative.creative.post === 'new') {
        if ($scope.newCreative.creative.isCard) {
          $scope.newCreative.isValid = !$scope.validateCardUrl();
          if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE') {
            $scope.newCreative.isValid = $scope.newCreative.isValid && ($scope.newCreative.trackingScriptStateInCardUrl === 'pass' || $scope.newCreative.trackingScriptStateInCardUrl === 'unknown');
          }
        }

        if (!$scope.newCreative.creative.isCard) {
          $scope.newCreative.isValid = !validateTweetUrl();
          if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE') {
            $scope.newCreative.isValid = $scope.newCreative.isValid && ($scope.newCreative.trackingScriptStateInTweetUrl === 'pass' || $scope.newCreative.trackingScriptStateInTweetUrl === 'unknown');
          }
        }
      } else if((goal === 'WEBSITE_CONVERSIONS' || goal === 'WEBSITE_CLICKS')&& $scope.newCreative.isValid && $scope.newCreative.creative.post === 'existing') {
        $scope.newCreative.isValid = !!$scope.newCreative.creative.twitterSponsoredObjectId;
      }

      //remove unnecessary elements

      //if new tweet is selected on radio toggle but existing tweet is selected, delete existing tweet
      if($scope.newCreative.creative.twitterSponsoredObjectId && $scope.newCreative.creative.post === 'new'){
        delete $scope.newCreative.creative.twitterSponsoredObjectId;
        delete $scope.newCreative.selectedTweet;
      }

      if($scope.newCreative.creative.tweetText && $scope.newCreative.creative.post === 'existing'){
        delete  $scope.newCreative.creative.tweetText;
        delete  $scope.newCreative.creative.image;
        delete  $scope.newCreative.creative.tweetURL;
        delete  $scope.newCreative.creative.tweetCardHeadline;
        delete  $scope.newCreative.creative.tweetCardURL;
        delete  $scope.newCreative.creative.tweetCardCallToAction;
      }
      if(saveBox){
        $scope.newCreative.saveToLibrary($scope.campaign.selectedChannel.toUpperCase());
      }

      if ($scope.newCreative.isValid) {
        saveCreative();
        creativeForm.submitted = false;
        if(!saveBox){
          ngDialog.close();
        }
      } else  { // If the creative is not valid, ask the user if he's sure about saving it
        fsConfirm('generic', {
          body: $filter('translate')('label.act.creativeInvalidMessage')
        }).then(function () {
          saveCreative();
          creativeForm.submitted = false;
          if(!saveBox){
            ngDialog.close();
          }
        });
      }
    };

    function saveCreative() {
      // Save the creative in the backend then add it to the combo / update the creative
      if ($scope.currentCreative) { // If we are editing a current creative, assign the content of newCreative to currentCreative
        _.assign($scope.currentCreative, $scope.newCreative);
        $scope.metadata.currentCombo.saveCreative($scope.newCreative, $scope.campaign.settings.channels.twitter.adAccount.accountId);
      } else {
        $scope.metadata.currentCombo.saveCreative($scope.newCreative, $scope.campaign.settings.channels.twitter.adAccount.accountId);
      }
    }

    $scope.urlInTweetCheck = _.debounce(function(){
      if($scope.newCreative.creative.tweetText){
        $scope.newCreative.creative.tweetURL = null;

        if (($scope.campaign.settings.goal.goal.name === 'WEBSITE_CONVERSIONS' ||
          $scope.campaign.settings.goal.goal.name === 'WEBSITE_CLICKS') &&
          !$scope.newCreative.creative.isCard) {
          if(!$scope.newCreative.creative.tweetText.match($scope.fullUrlRegex)){
            $scope.newCreative.creative.tweetURL = -1;
          } else {
            $scope.newCreative.creative.tweetURL = $scope.newCreative.creative.tweetText.match($scope.fullUrlRegex)[0];
            validateTweetUrl();
            checkIfTweetUrlContainsTrackingScript();
          }
        }
      }
    }, 1000);

    $scope.selectCallToAction = function (callToAction) {
      $scope.newCreative.creative.tweetCardCallToAction = callToAction;
    };

    $scope.changeIsWebsiteCard = function () {
      $scope.isCard = !$scope.isCard;
      $scope.newCreative.creative.isCard = $scope.isCard;
      $scope.validateImage();
    };

    // -------------------------- Library ------------------------------------

    $scope.loadImageFromLibrary = function () {
      // Open Library - Load Image modal view
      var opts = {
        currentCombo: $scope.metadata.currentCombo
      };

      //if website card, set aspect ratio/min width/height
      if($scope.newCreative.creative.isCard){
        opts.minSize = {width: 800, height: 320, unit: 'px'};
      }


      library.openLibrary('Images', opts).then(function(content){
        if(content){
          $scope.newCreative.creative.image = content[0].fullGuid;
          $scope.picWidth = content[0].width;
          $scope.picHeight = content[0].height;
          $scope.aspectRatio = $scope.picWidth / $scope.picHeight;
          $scope.validateImage();
        }
      });
    };

    $scope.loadVideoFromLibrary = function () {
      // Open Library - Load Video modal view
      var opts = {
        currentCombo: $scope.metadata.currentCombo
      };
      library.openLibrary('Videos', opts).then(function(content){
        if (content) {
          $scope.newCreative.creative.video = content[0].guid;
          $scope.newCreative.creative.image = content[0].imageUrl;
        }
      });
    };

    $scope.loadCreativeFromLibrary = function () {
      // Open Library - Load Image modal view
      var opts = {
        currentComboId: $scope.metadata.currentCombo.id,
        channel: $scope.campaign.selectedChannel
      };
      library.openLibrary('Creatives', opts).then(function(content){

        $scope.newCreative.creative = content.creative;
        // $scope.saveCreative($scope.newCreative.creative);
      });
    };

    // -------------------------- End Library ------------------------------------

    $scope.validateImage = function () {
      $scope.imageErrorWidthOrHeight = false;

      if ($scope.newCreative.creative.isCard && $scope.picWidth && $scope.picHeight &&
         (($scope.picWidth < 800 || $scope.picWidth > 816) ||
          ($scope.picHeight < 320 || $scope.picHeight > 325)) ) {
        $scope.imageErrorWidthOrHeight = true;
      }
    };    

    // Makes sur the URL typed by the user inside the destination url property of the card has the same domain as the URL registered in the goal
    $scope.validateCardUrl = function () {

      // Add protocol if not present already
      if (!_.contains($scope.newCreative.creative.tweetCardURL, 'http')) {
        $scope.newCreative.creative.tweetCardURL = 'http://' + $scope.newCreative.creative.tweetCardURL;
      }

      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        $scope.newCreative.isCardUrlDomainTypedDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain($scope.newCreative.creative.tweetCardURL, goalUrl);
        return $scope.newCreative.isCardUrlDomainTypedDifferentFromGoalDomain;
      } else {
        return false;
      }
    };

    // Makes sur the URL typed by the user inside the tweet has the same domain as the URL registered in the goal
    function validateTweetUrl () {
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        $scope.newCreative.isTweetUrlDomainTypedDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain($scope.newCreative.creative.tweetURL, goalUrl);
        return $scope.newCreative.isTweetUrlDomainTypedDifferentFromGoalDomain;
      } else {
        return false;
      }
    }

    $scope.checkIfCardUrlContainsTrackingScript = function () {
      // Only if we are using Accomplice tracking script
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {

        $scope.newCreative.trackingScriptStateInCardUrl = 'processing';
        var url = $scope.newCreative.creative.tweetCardURL;
        microService.urlValidation(
          {
            host: 'na',
            testUrl: url,
            index: 100,
            nowait: true,
            nocache: true,
            badurlonly: false
          },
          function (response) {
            // If the webservice is not available
            if (response && response.status === 'ERROR') {
              // If the webservice is down, we define a new state (unknwown) that will mark the creative as valid
              $scope.newCreative.trackingScriptStateInCardUrl = 'unknown';

            } else {
              var url = response.api || {};
              $scope.newCreative.trackingScriptStateCardInUrl = url.found ? 'pass' : 'fail';
            }
          },
          function (error) {
            console.log('validate tracking script in url server ERROR: ', error);
            $scope.newCreative.trackingScriptStateInCardUrl = 'unknown';
          });
      }
    };

    function checkIfTweetUrlContainsTrackingScript() {

      // Only if we are using Accomplice tracking script
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {

        $scope.newCreative.trackingScriptStateInTweetUrl = 'processing';
        var url = $scope.newCreative.creative.tweetURL;
        microService.urlValidation(
          {
            host: 'na',
            testUrl: url,
            index: 100,
            nowait: true,
            nocache: true,
            badurlonly: false
          },
          function (response) {

            // If the webservice is not available
            if (response && response.status === 'ERROR') {
              // If the webservice is down, we define a new state (unknwown) that will mark the creative as valid
              $scope.newCreative.trackingScriptStateInTweetUrl = 'unknown';

            } else {
              var url = response.api || {};
              $scope.newCreative.trackingScriptStateInTweetUrl = url.found ? 'pass' : 'fail';
            }
          },
          function (error) {
            console.log('validate tracking script in url server ERROR: ', error);
            $scope.newCreative.trackingScriptStateInTweetUrl = 'unknown';
          });
      }
    }

    $scope.determineTweetTextLength = function(){
      var tweetURLLength = 0;
      var urlLength = 0;
      var tweetLength = 0;
      if($scope.newCreative.creative.tweetText){
        //if url, length of the url will be 22 characters, replace url length with 22 characters
        tweetLength = angular.copy($scope.newCreative.creative.tweetText.length);
        tweetURLLength = 22;
        if($scope.newCreative.creative.tweetText.match($scope.fullUrlRegex)){
          urlLength = $scope.newCreative.creative.tweetText.match($scope.fullUrlRegex)[0].length;
          tweetLength = tweetURLLength + (tweetLength - urlLength);
        }
      }

      return $scope.newCreative.creative.isCard || $scope.newCreative.creative.image ? (116 - tweetLength) : (140 - tweetLength);

    };

    $scope.setForm = function (form, isCreativeValid) {
      form.submitted = !isCreativeValid;
    };

  });
