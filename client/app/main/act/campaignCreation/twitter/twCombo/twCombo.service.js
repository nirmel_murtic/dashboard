'use strict';

angular.module('act')
  .service('TwCombo', function ($http, apiUrl, $rootScope, $sce, $timeout, fsUtils, TwCreative, TwAudience, Combo) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var TwCombo;

    TwCombo = (function () {
      function TwCombo() {

        // Call the super constructor Combo
        this.constructor.super_.call(this);
//                if(comboId) {
//                    this.load(comboId);
//                    return;
//                }

        this.name = 'TW Combo ' + (counter++);
        this.channel = 'TWITTER';

        this.interest = {
          id: null,
          interest: []
        };
        // creative and audience counts are defined in the parent Combo class
        this.numberOfInterestVariants = 0;
      }

      // Implement inheritance from Combo abstract class
      fsUtils.inherits(TwCombo, Combo);

      return TwCombo;
    })();

    // Counter to generate combo default names
    var counter = 1;

    TwCombo.prototype.saveAudience = function (newAudience) {
      if (newAudience.id) {
        updateAudience(this, newAudience);
      } else {
        createAudience(this, newAudience);
      }
    };

    TwCombo.prototype.saveCreative = function (newCreative, accountId) {

      removeEmptyFieldsInCreative(newCreative);

      if (newCreative.id) {
        updateCreative(this, newCreative, accountId);
      } else {
        createCreative(this, newCreative, accountId);
      }
    };

    TwCombo.prototype.checkValidity = function () {
      var isComboValid = true;
      try {

        this.checkForInvalidCreatives();

        if (!this.audience || !this.audience.audience || !this.audience.audience.location ||
          this.audience.audience.location.length === 0 || this.creativeList.length === 0 || this.invalidCreativeIndexList.length > 0 ||
          (!this.audience.audience.interest && !this.audience.audience.keyword && !this.audience.audience.follower && !this.audience.audience.behavior)
        ) {
          isComboValid = false;
        }
        this.isValid = isComboValid;
        return isComboValid;
      } catch (e) {
        console.log('error', e);
        return false;
      }
    };

    TwCombo.load = function (comboData, superCmpId, parentCmpId) {
      var combo = new TwCombo();
      var unsortedCreativesList = [];

      // For each creative and audience, create the relevant objects
      for (var property in comboData) {
        if (comboData.hasOwnProperty(property)) {
          if (property === 'audience' && comboData[property] !== '') { // If it's an audience, associate to the audience property of the combo object
            combo.audience = new TwAudience(angular.fromJson(comboData[property].audience));
            combo.audience.id = comboData[property].id;

          } else if (property === 'creativeList' && comboData[property] !== null) { // If it's a creative, create creative objects
            for (var i = 0, j = comboData[property].length; i < j; i++) {
              var creative = new TwCreative(angular.fromJson(comboData[property][i].creative));
              creative.id = comboData[property][i].id;
              // Handle legacy issue when post objects were at the root of the creative object
              if (creative.post) {
                creative.creative.post = creative.post;
                delete creative.post;
              }
              unsortedCreativesList.push(creative);
            }
          } else if (property === 'interest' && comboData[property] !== null) { // If it's a interest, create interest objects
            // when it exists
            //combo.interest = new TwInterest(angular.fromJson(comboData[property].interest));
            //combo.interest.id = comboData[property].id;

          } else { // Other properties like comboId or channel
            combo[property] = comboData[property];
          }
        }
      }
      // When loading from library, use ids passed as a parameter
      combo.superCmpId = superCmpId || combo.superCmpId;
      combo.parentCmpId = parentCmpId || combo.parentCmpId;

      // Sort creatives order: First created is first displayed
      combo.creativeList = _.sortBy(unsortedCreativesList, function (creativeInList){
        return creativeInList.id;
      });

      // Generate number of audience, creative, interest variants for each combo
      combo.calculateNumberOfCreativeVariants();
      combo.calculateNumberOfAudienceVariants();
      combo.calculateNumberOfInterestVariants();

      // Return combo
      return combo;
    };

    TwCombo.prototype.calculateNumberOfAudienceVariants = function () {
      var numberOfVariants = 0;

      if (this.audience) {

        //first add up the primary targeting specs (there's always at least one)
        for (var property in this.audience.audience) {
          if ((property === 'keyword' && property.length > 0) ||
              (property === 'follower' && property.length > 0) ||
              (property === 'interest' && property.length > 0) ||
              (property === 'behavior' && property.length > 0)) {
            numberOfVariants += 1;
          }
        }
        //then multiply them by the number of variants in each non-primary targeting spec
        //(actually it multiplies by every targeting spec, but there's only ever 1 variant
        // for the primaries so multiplying by 1 works out)
        for (property in this.audience.audience) {
          if (this.audience.audience.hasOwnProperty(property) && this.audience.audience[property] !== undefined && this.audience.audience[property].length !== 0) {
            numberOfVariants *= this.audience.audience[property].length;
          }
        }
      }
      // Update the count of audience variants
      this.numberOfAudienceVariants = numberOfVariants;
    };

    TwCombo.prototype.calculateNumberOfCreativeVariants = function () {
      this.numberOfCreativeVariants = this.creativeList.length;
    };

    TwCombo.prototype.calculateNumberOfInterestVariants = function () {
      this.numberOfInterestVariants = this.interest.interest.length;
    };

    // This is to delete the fields that might have been edited then emptied, in order to avoid issues with the backend parsing
    // The backend can't process null results
    function removeEmptyFieldsInCreative(newCreative) {
      if (newCreative.creative.siteDescription === '') {
        delete newCreative.creative.siteDescription;
      }
      if (newCreative.creative.headline === '') {
        delete newCreative.creative.headline;
      }
      if (newCreative.creative.tweetText === '') {
        delete newCreative.creative.text;
      }
      if (newCreative.creative.deepLink === '') {
        delete newCreative.creative.deepLink;
      }
    }

    // Internal method to create an audience
    function createAudience(self, newAudience) {
      console.log('creating audience');

      var params = {
        typeReq: 'AUDIENCE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        data: angular.toJson(newAudience)
      };

      $http({
        url: apiUrl + '/api/v2/campaign/create/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          console.log('create audience response', response);
          // Assign id returned by backend
          newAudience.id = response.data.cmpAdSettingId;
          // Assign audience to the newly created audience
          self.audience = newAudience;

          // Retrieve list of ads that have a low reach, if any
          // self.audience.lowReachAdsList = TwCombo.parseLowReachAds(response.data.lowReachAdsList);

          self.checkValidity();
          self.calculateNumberOfAudienceVariants();
        }).
        error(
        function (response) { // optional

          console.log(response);
        });
    }

    // Internal method to update an audience
    function updateAudience(self, newAudience) {
      console.log('updating audience');

      var params = {
        typeReq: 'AUDIENCE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        cmpAdSettingId: newAudience.id,
        data: angular.toJson(newAudience)
      };

      $http({
        url: apiUrl + '/api/v2/campaign/update/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          console.log('update audience response', response);

          // Assign audience to the newly created audience
          self.audience = newAudience;
          // Retrieve list of ads that have a low reach, if any
          // self.audience.lowReachAdsList = TwCombo.parseLowReachAds(response.data.lowReachAdsList);

          self.checkValidity();
          self.calculateNumberOfAudienceVariants();
        }).
        error(
        function (response) { // optional

          console.log(response);
        });
    }

    // Internal method to create a creative
    function createCreative(self, newCreative) {
      // Note: to fix a jshint error, "accountId" was removed from the list of variables included in this function
      // If you need it, add it back in.

      console.log('creating creative', newCreative);

      var params = {
        typeReq: 'CREATIVE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        data: angular.toJson(newCreative)
      };

      $http({
        url: apiUrl + '/api/v2/campaign/create/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          console.log('creative response', response);
          // Assign returned id to the creative
          newCreative.id = response.data.cmpAdSettingId;
          self.creativeList.push(newCreative);
          self.calculateNumberOfCreativeVariants();
          self.checkForInvalidCreatives();
          self.checkValidity();
          // newCreative.getPreview(accountId);
        }).
        error(
        function (response) { // optional
          console.log(response);
        });
    }

    // Internal method to update a creative
    function updateCreative(self, newCreative) {

      console.log('updating creative');

      var params = {
        typeReq: 'CREATIVE_REQ',
        superCmpId: self.superCmpId,
        parentCmpId: self.parentCmpId,
        adComboId: self.id,
        cmpAdSettingId: newCreative.id,
        data: angular.toJson(newCreative)
      };

      //update backend
      $http({
        url: apiUrl + '/api/v2/campaign/update/adSetting/' + $rootScope.fractalAccountInfo.currentTeam.id,
        method: 'POST',
        data: $.param(params)
      }).
        success(function (response) {
          console.log('creative response', response);
          self.calculateNumberOfCreativeVariants();
          self.checkForInvalidCreatives();
          self.checkValidity();
          // newCreative.getPreview(accountId);
        }).
        error(
        function (response) { // optional
          console.log(response);
        });
    }

    return TwCombo;
  });
