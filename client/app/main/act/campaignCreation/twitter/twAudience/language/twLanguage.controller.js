'use strict';

angular.module('act')
  .controller('TwLanguageCtrl', function ($scope, $cacheFactory) {    

    var cache;
    if (!$cacheFactory.info().twLanguageCache){
      cache = $cacheFactory('twLanguageCache');
    } else {
      cache = $cacheFactory.info().twLanguageCache;
    }

    $scope.twLanguageAtpConfig = $scope.atpHelper.getConfig('language');

    // to easily see if those fools still only support 21 languages. 
    // ( include apiUrl and $http ) 
    // $http({
    //   method: 'GET',
    //   url: apiUrl + '/api/v2/autocomplete/twitter/languages?count=30'
    // }).then(function(response){
    //   console.log('langs', response)
    // })

  	$scope.addLanguageSuggestionToVariant = function(e, data){

      var variantType = data.model;
      console.log('e:', e, 'data:', data);

      if(data.triggeredBy === 'MANUAL'){
        return;
      }
      
      $scope.addVariantIfNone(variantType);
      var modelValue = {};
      modelValue.id = data.value.targeting_value; //jshint ignore:line  
      modelValue.value =  data.value.name;
      var pillValue = modelValue.value;
      var variantIndex = $scope.view.getSelectedVariantIndex(variantType);

      $scope.view.addTargetingSpec(variantType, pillValue);
      $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);

      $scope.focusInput(data.model);
    };


    $scope.$on('ngAtp:autocomplete', $scope.addLanguageSuggestionToVariant);
  });