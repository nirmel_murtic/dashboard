'use strict';

angular.module('act')
    .service('TwAudience', function (Audience, apiUrl, $rootScope, fsUtils, $http, withInput, ngDialog, fsNotify, fsConfirm, $filter) {

    	var TwAudience;

        TwAudience = (function() {
            function TwAudience(audience) {
                // Call the super constructor Creative
                this.constructor.super_.call(this);

                if (audience) {
                    for (var property in audience) {
                        if (audience.hasOwnProperty(property)) {
                            this[property] = audience[property];
                        }
                    }
                    return;
                }

                this.id = null;
                // All info related to the audience stored here
                this.audience = {};
            }

            // Implement inheritance from Creative abstract class
            fsUtils.inherits(TwAudience, Audience);

            return TwAudience;
        })();

        TwAudience.prototype.isAlreadyAdded = function (variantType, variantIndex, value) {
          if (variantIndex > -1 && variantIndex <  this.audience[variantType].length) {
            for (var i=0; i<this.audience[variantType][variantIndex].values.length; i++) {
              if (_.isEqual(this.audience[variantType][variantIndex].values[i], value)) {
                return true;
              }
            }
          }
          return false;
        };

        TwAudience.prototype.addVariant = function(variantType, variantId){
            var variants = this.audience[variantType];
            var emptyVariant = {
                variantId: variantId,
                values: []
            };
            if(!variants || variants.length === 0){
                this.audience[variantType] = [];
            } else {
                if(variants[variants.length - 1].values.length < 1 ){
                    alert('you cannot add another variant if you have a blank one');
                    // $scope.focusInput(variantType);
                    return;
                }
            }
            this.audience[variantType].push(emptyVariant);
        };

        TwAudience.prototype.addTargetingSpec = function(variantType, variantIndex, value){
          if (!this.isAlreadyAdded(variantType, variantIndex, value)) {
            this.audience[variantType][variantIndex].values.push(value);
          }
        };

        TwAudience.prototype.saveToLibrary = function(save, campaign){
            if(save){
                var newData = this;
                //set audience name to blank
                withInput.setInput('');
                fsConfirm('confirmationWithInput', {
                  body: $filter('translate')('label.act.Nameyouraudience'),
                  title: 'My Audience'
                }).then(function () {

                  newData.name = withInput.getInput();

                  var channel = campaign.selectedChannel.toUpperCase();
                  var params = {
                    channelName: channel,
                    objectId: 0,
                    libraryObjectType: 'audience',
                    data: angular.toJson(newData)
                  };

                  $http({
                    method: 'POST',
                    url: apiUrl + '/api/v2/library/save/template/' + $rootScope.fractalAccountInfo.currentTeam.id,
                    data: $.param(params)
                  }).
                    success(function (response) {

                      console.log('Audience response: ', response);
                      ngDialog.close();
                      fsNotify.push(
                        $filter('translate')('label.act.Audiencesavedtolibrary')+': '+ newData.name
                      );
                      withInput.destroyInput();
                    }).
                    error(function (response) { // optional
                      withInput.destroyInput();
                      console.log(response);
                    });

                });
            }
        };

        var platformMatrix = [
            {'platform': 'IOS', 'key': 0},
            {'platform': 'Android', 'key': 1},
            {'platform': 'Blackberry', 'key': 2},
            {'platform': 'Other', 'key': 3},
            {'platform': 'Desktop', 'key': 4}
            // ,{'platform': 'Wifi', 'key': 'Wifi'}
        ];

        TwAudience.prototype.getValueFromVariantType = function(variantType, data){
            var value = {};
            if(variantType === 'location'){
                value.value = data.value.name;
                value.type = data.model;
                value.key = data.value.targeting_value; //jshint ignore:line
                //value.twTargetingValue = data.value.targeting_value;

            } else if(variantType === 'language'){
                value.value = data.value.name;
                value.type = data.model;
                value.key = data.value.targeting_value; //jshint ignore:line
            } else if(variantType === 'gender'){
                value.value = data;
            } else if(variantType === 'ageRange'){
                value.value = data;
            } else if(variantType === 'platform'){
                value.key = _.result(_.find(platformMatrix, {'platform': data}), 'key');
                value.value = data;
            } else if(variantType === 'carrier'){
                value.value = data;
            } else if(variantType === 'newDevice'){
                value.value = data;
            } else if(variantType === 'keyword'){
                value.value = data;
            } else if(variantType === 'follower'){
                value.value = data.screen_name; // jshint ignore:line
                value.key = data.id;
            } else if(variantType === 'tv'){
                value.value = data;
            } else if(variantType === 'behavior'){
                value.value = data;
            }

            return value;
        };

        TwAudience.prototype.removeTargetingSpec = function(variantType, variantIndex, valueIndex){
            var variant = this.audience[variantType][variantIndex];
            variant.values.splice(valueIndex, 1);
        };

        TwAudience.prototype.removeVariant = function(variantType, index){
            this.audience[variantType].splice(index, 1);
            if(this.audience[variantType].length === 0 ){
                delete this.audience[variantType];
            }
        };


        return TwAudience;
    });
