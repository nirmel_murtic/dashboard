'use strict';

angular.module('act')
  .controller('TwCarrierCtrl', function ($scope, apiUrl, $parse, $http, $filter, $timeout, TwAtpHelper, countryISO, TwInterestCategories, $cacheFactory) {

  	var atpHelper = new TwAtpHelper();
  	$scope.twCarrierAtpConfig = atpHelper.getConfig('carrier');

  	var cache = $cacheFactory('carrierCache');

  	var cacheValues = function(key,data){
  	  cache.put(key,data);
  	};

  	var getValue = function(key){
  	  return cache.get(key);
  	};

  	$scope.allCarriers = _.map(countryISO.twitterJune, 'Name');

    var countryCode;

    $scope.browseCountry = function(country){
      $scope.country = country;
    	var values = getValue(country);
      countryCode = _.result(_.find(countryISO.twitterJune, {'Name': country}), 'Code');
    	if(values){
    	  $scope.carriers = values;
    	} else {
    	  $scope.browseCarrierLoading = true;
    	  $http({
    	    method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/network?country_code=' + countryCode + '&additional_type=operators'
    	  }).success(function(response){
    	    $scope.browseCarrierLoading = false;
    	    $scope.carriers = response.data;
    	    cacheValues(country, response.data);
    	  }).error(function(response){
          console.log(response);
        });    
    	}

    };
    console.log(countryISO.twitterJune[0]);
    $scope.browseCountry($scope.allCarriers[0]);

    $scope.makeCarrierPill = function(carrier){
      carrier.selected = !carrier.selected;
      var pillValue = $scope.country + ' | ' + carrier.name;
      return pillValue;
    };

  });