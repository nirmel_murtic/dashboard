'use strict';

angular.module('act')
  .controller('TwNewDevicesCtrl', function ($scope, $filter) {
  	
  	$scope.addNewDeviceTargeting = function(n, method) {

        var variantType = 'newDevices';
        $scope.newDevice.howLong = n;
        $scope.newDevice.method = method;
        console.log('new device for ' + $scope.newDevice.howLong + ' months using ' + $scope.newDevice.method + ' method');

        //pluralize pills if it's more than 1 month
        if($scope.newDevice.howLong === 1){
            $scope.newDevice.timePeriod = ' ' + $filter('translate')('label.act.twitter.audiences.month');
        } else {
            $scope.newDevice.timePeriod = ' ' + $filter('translate')('label.act.twitter.audiences.months');
        }

        var value = $scope.newDevice.method + ' | ' + $scope.newDevice.howLong + $scope.newDevice.timePeriod;

        //Can only have 1 variant for newDevices, so if there is already one, remove it.
        if($scope.view[variantType].variants.length && $scope.view[variantType].variants[0].pills.length){

            var variantIndex = $scope.view.getSelectedVariantIndex(variantType);

            $scope.view.removePillFromVariant($scope.view[variantType].variants[0], 0);
            $scope.newAudience.removeTargetingSpec(variantType, variantIndex, 0);
        }

        $scope.addVariant(variantType);
        $scope.addTargetingSpec(variantType, value);

        $scope.newDevice.howLong = null;
        $scope.newDevice.method = null;
    };

  });