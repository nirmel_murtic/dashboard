'use strict';

angular.module('act')
  .controller('TwFollowerCtrl', function ($scope, $timeout, twAudienceWebApiUtils, $http, apiUrl) {

    $scope.radioBtn = {
      config: {
        valueField: 'screen_name'
      },
      data: []
    };

  	$scope.twFollowers = {};
  	var variantType = 'follower';

    $scope.showFollowerRecs = {};


    $scope.twFollowerAtpConfig = $scope.atpHelper.getConfig('follower');

  	//$scope.onCsvFollowerImport = function(){
      //
      //$scope.followers = _.map(this.result,'followerCsv');
      //$scope.showFollowerRecs.check = false;
      //
      //_.forEach($scope.followers, function(value){
      	//$scope.addVariantAndSpec(variantType, value);
      //});
  	//};

    // $scope.toggleTargetMyFollowers = function(){
    //   $scope.newAudience.audience.follower.targetMyFollowers = !!$scope.newAudience.audience.follower.targetMyFollowers;
    // };

    //$scope.campaign.settings.channels.twitter.adAccount.social.socialUsername

    $scope.toggleTargetUsersLikeMyFollowers = function(checked){
      // $scope.newAudience.audience.follower.targetUsersLikeMyFollowers = !!$scope.newAudience.audience.follower.targetUsersLikeMyFollowers;
      var userUsername = $scope.campaign.settings.channels.twitter.adAccount.social.socialUsername;
      if (checked === true) {
        $scope.checkAndSubmitScreenName([userUsername]);
      } else {
        if($scope.view.isValueInSelectedVariant('follower', '@' + userUsername)){
          var variantIndex = $scope.view.getSelectedVariantIndex('follower');
          var variant = $scope.view.follower.variants[variantIndex];
          var pillIndex = $scope.view.getPillIndexInSelectedVariant('follower', '@' + userUsername);
          $scope.removeTargetingSpecFromVariant('follower', variant, pillIndex);
        }
      }

    };

  	$scope.addVariantAndSpec = function(variantType, value){
  		var variants = $scope.view[variantType].variants;
      $scope.moreThan100 = false;
      if(variants.length === 0){
        $scope.view.addVariant(variantType);
        $scope.newAudience.addVariant(variantType);
      }
      if (variants[0].pills.length > 99) {
        $scope.moreThan100 = true;
      } else {
    		if ($scope.newAudience.isAlreadyAdded(variantType, 0, value, 'value') === false) {
          var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
          var modelValue = $scope.newAudience.getValueFromVariantType(variantType, value);
          var displayValue = '@' + modelValue.value;
          $scope.view.addTargetingSpec(variantType, displayValue);
          $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
        }
      }
  	};

    $scope.applyCopyPaste = function(){
      var untrimmedData;
      $scope.clearErrors();
      if ($scope.twFollowers.copyPaste){
        untrimmedData = $scope.twFollowers.copyPaste.split('\n');
      }
      //trim leading and trailing whitespace and lowercase it
      $scope.followers = _.map(untrimmedData, function(datum){
        return datum.toLowerCase().trim();
      });
      //clear empty strings
      $scope.followers = $scope.followers.filter(function(q){ return q;});

      //hide and uncheck the "recommended followers" checkbox because now it
      //will be different recommendations.
      $scope.showFollowerRecs.check = false;

      //clear the list of successful and failed screen names for error reporting
      $scope.successfulScreenNames = [];
      $scope.failedScreenNames = [];
      var uniqueFollowers = [];

      _.forEach($scope.followers, function(value){
        
        //Check to see if the user has prepended the follower with an '@' because
        //the search is much more accurate with the '@'
        //ES6: !typedFollower.startsWith('@')
        if (value.slice(0,1) === '@'){
          value = value.slice(1);
        }

        //If it's already a variant, don't bother asking Twitter again
        var found = false;
        if($scope.view.follower.variants[0]){
          found = _.find($scope.view.follower.variants[0].pills, function(name){
            //remove the '@' we added to the view and compare
            return name.value.slice(1).toLowerCase() === value.toLowerCase();
          });
        }
        if (!found) {
          uniqueFollowers.push(value);
        }
        
      });

      if(uniqueFollowers.length){
        $scope.checkAndSubmitScreenName(uniqueFollowers);
      }
    };

    $scope.checkAndSubmitScreenName = function(uniqueFollowers){

      var submittedFollowers = uniqueFollowers.join(','); 
      
      //send the comma-separated list of followers to the validate endpoint.
      //Only exact matches are returned
      $http({
        method: 'POST',
        url: apiUrl + '/api/v2/autocomplete/twitter/user/validate?listUsers=' + submittedFollowers
      })
      .success(function (response) {
        // console.log('Follower response: ', response);
        var returnedFollowers = [];
        for(var i=0 ; i<response.data.length ; i++) {
          returnedFollowers.push(response.data[i].screen_name.toLowerCase()); //jshint ignore:line
          //just send the whole user object and addVariantAndSpec will find the screen_name and id and add them to the pill
          $scope.addVariantAndSpec(variantType, response.data[i]);
        }
        $scope.failedScreenNames = _.difference(uniqueFollowers, returnedFollowers);
        $scope.successfulScreenNames = _.intersection(uniqueFollowers, returnedFollowers);

      })
      .error(function (error) {
        uniqueFollowers.push('or there may have been an error');
        $scope.failedScreenNames = uniqueFollowers;
        console.log(error);
      });
    };

    $scope.addFollowerSuggestionToVariant = function (e, data) {
      $scope.clearErrors();
      $scope.showFollowerRecs.check = false;
      $scope.addVariantAndSpec(variantType, data.value);
      $timeout(function () {
        angular.element('#' + 'follower' + '_value')[0].value = '';
      }, 50);
    };
    $scope.$on('ngAtp:autocomplete', $scope.addFollowerSuggestionToVariant);

    $scope.addFn = function(allValues){
      $scope.clearErrors();
      _.forEach(allValues, function(value){
        if (value.selected === true) {
          if(!$scope.view.isValueInSelectedVariant(variantType, value.screen_name)){ //jshint ignore:line
            $scope.addVariantAndSpec(variantType, value);
          }
        }
      });
      $scope.showRadioList = false;
      $scope.showFollowersClick(true);
    };

    $scope.showFollowersClick = function (checked) {
      $scope.clearErrors();
      $scope.showRadioList = false;
      $scope.loadingFollowerRecs = true;
      if (checked) {
        if($scope.newAudience.audience.follower && $scope.newAudience.audience.follower[0].values.length !== 0){
          var query = _.map($scope.newAudience.audience.follower[0].values, 'key').join();
          var queryEncoded = encodeURI(query);
          twAudienceWebApiUtils.getRecommendedFollowers(queryEncoded, function (result) {
            $scope.loadingFollowerRecs = false;
            $scope.showRadioList = true;
            $scope.radioBtn.data = result.data.data;
          }, function (error) {
            console.log('Twitter Error: ', error.data.error.message);
            $scope.tooFewToRecommend = true;
            $scope.loadingFollowerRecs = false;
          });
        } else {
          $scope.noFollowersToRecommend = true;
          $scope.loadingFollowerRecs = false;
        }
      } else {
        $scope.showRadioList = false;
        $scope.loadingFollowerRecs = false;
      }
    };

    $scope.clearErrors = function () {
      $scope.noFollowersToRecommend = false;
      $scope.tooFewToRecommend = false;
    };

  });
