'use strict';

angular.module('act')
  .service('twAudienceWebApiUtils', function ( $http, apiUrl) {
    return {
      getAllBehaviors: function (successCallback, errorCallback) {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/behaviors/?parent_behavior_taxonomy_ids=null&count=10&additional_type=taxonomies'
        }).then(successCallback, errorCallback);
      },
      getBehaviorChilds: function (parrent_id, successCallback, errorCallback) { //jshint ignore:line
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/behaviors/?parent_behavior_taxonomy_ids=' + parrent_id + '&count=10&additional_type=taxonomies' //jshint ignore:line
        }).then(successCallback, errorCallback);
      },
      getBehaviorChildsofChild: function (child_id, successCallback, errorCallback) { //jshint ignore:line
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/behaviors/?parent_behavior_taxonomy_ids='+ child_id + '&count=10&additional_type=taxonomies' //jshint ignore:line
        }).then(successCallback, errorCallback);
      },
      getRecommendedKeywords: function (keywords, successCallback, errorCallback) {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/suggestions/targeting?account_id=18ce53vjuh5&suggestion_type=KEYWORD&targeting_values=' + keywords +'&count=40'
        }).then(successCallback, errorCallback);
      },
      getRecommendedFollowers: function (followers, successCallback, errorCallback) {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/suggestions/targeting?account_id=18ce53vjuh5&suggestion_type=USER_ID&targeting_values=' + followers +'&count=40'
        }).then(successCallback, errorCallback);
      }
    };
  });
