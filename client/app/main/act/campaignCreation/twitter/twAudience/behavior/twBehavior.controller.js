'use strict';

angular.module('act')
  .controller('TwBehaviorCtrl', function ($scope, $cacheFactory, twAudienceWebApiUtils) {
    $scope.behaviors = {
      data: [],
      isLoading: false
    };

    var cache;
    if (!$cacheFactory.info().twBehaviorCache){
      cache = $cacheFactory('twBehaviorCache');
    } else {
      cache = $cacheFactory.info().twBehaviorCache;
    }

    $scope.twBehaviorAtpConfig = $scope.atpHelper.getConfig('behavior');

    $scope.addBehaviorSuggestionToVariant = function(e, data){
      if(data.triggeredBy === 'MANUAL'){
        return;
      }
      $scope.addVariantIfNone('behavior');
      var modelValue = {};
      modelValue.key = data.value.id; //jshint ignore:line
      modelValue.value =  data.value.name;
      var pillValue = modelValue.value;
      $scope.view.addTargetingSpec('behavior', pillValue);

      //can only one variant for this targeting spec
      $scope.newAudience.addTargetingSpec('behavior', 0, modelValue);
      $scope.focusInput(data.model);
    };

    $scope.toggleBehavior = function(behavior){
      var modelValue = {};
      $scope.addVariantIfNone('behavior');
      behavior.selected = true;
      modelValue.key = behavior.targeting_value; //jshint ignore:line
      modelValue.value =  behavior.name;
      modelValue.id =  behavior.id;
      var variantIndex = $scope.view.getSelectedVariantIndex('behavior');
      var variant = $scope.view.behavior.variants[variantIndex];
      var pillValue = modelValue.value;
      var pillIndex = $scope.view.getPillIndexInSelectedVariant('behavior', pillValue);
      if($scope.view.isValueInSelectedVariant('behavior', pillValue)){
        behavior.selected = false;
        $scope.removeTargetingSpecFromVariant('behavior', variant, pillIndex);
      } else {
        $scope.view.addTargetingSpec('behavior', pillValue);
        $scope.newAudience.addTargetingSpec('behavior', variantIndex, modelValue);
      }
    };

    twAudienceWebApiUtils.getAllBehaviors(function (result) {
      $scope.behaviors = {
        data: result.data.data,
        isLoading: false
      };
      if ($scope.behaviors.data.length !== 0) {
        $scope.getBehaviorChilds($scope.behaviors.data[0]);
      }
    }, function (error) {
      console.log(error);
    });


    $scope.getBehaviorChilds = function (behavior) {
      $scope.childsOfChild = [];
      console.log(behavior);
      twAudienceWebApiUtils.getBehaviorChilds(behavior.id, function (result) {
        $scope.behaviorChilds = result.data.data;
        /*if ($scope.behaviorChilds.length !== 0) {
          $scope.getBehaviorChildsOfChild($scope.behaviorChilds[0]);
        }*/
      }, function (error) {
        console.log('error', error);
      });
    };


    /*$scope.getBehaviorChildsOfChild = function (behaviorChild) {
      $scope.childsOfChild = [];
      console.log(behaviorChild);
      twAudienceWebApiUtils.getBehaviorChildsofChild(behaviorChild.id, function (result) {
        $scope.childsOfChild = result.data.data;
      }, function (error) {
        console.log('error', error);
      });
    };*/

    $scope.$on('ngAtp:autocomplete', $scope.addBehaviorSuggestionToVariant);

  });
