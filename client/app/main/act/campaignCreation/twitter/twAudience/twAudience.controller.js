'use strict';

angular.module('act')
  .controller('TwAudienceCtrl', function (apiUrl, $scope, $parse, $http, $filter, $timeout, TwAtpHelper, ngDialog, TwAudience, TwAudienceView, fsConfirm, library) {
    $scope.atpHelper = new TwAtpHelper();
    $scope.usernameDataType = 'searchBar';
    $scope.keywordDataType = 'copyPaste';
    $scope.view = new TwAudienceView();
    $scope.newAudience = new TwAudience();


    $scope.newDeviceLengths = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    $scope.newDevice = {};
    $scope.newDevice.howLong = 0;

    //  TODO: Include Mobile Apps
    ////Select os if app install goal
    //$scope.disableOsType = function(osType){
    //  switch (osType) {
    //    case 'Iphone':
    //      $scope.isIphoneDisabled = true;
    //      break;
    //    case 'Ipad':
    //      $scope.isIpadDisabled = true;
    //      break;
    //    case 'Android':
    //      $scope.isAndroidDisabled = true;
    //      break;
    //  }
    //};
    //
    //$scope.enableOsType = function(osType){
    //  switch (osType) {
    //    case 'Iphone':
    //      $scope.isIphoneDisabled = false;
    //      break;
    //    case 'Ipad':
    //      $scope.isIpadDisabled = false;
    //      break;
    //    case 'Android':
    //      $scope.isAndroidDisabled = false;
    //      break;
    //  }
    //};

    //$scope.accordion = {
    //  platformOpenIphone: false,
    //  platformOpenIpad: false,
    //  platformOpenAndroid: false,
    //  platformDisabled: false
    //};

    $scope.accordion = {
      platformOpenIos: false,
      platformOpenAndroid: false,
      platformDisabled: false
    };

    $scope.addTargetingSpec = function (variantType, value) {
      $scope.addVariantIfNone(variantType);
      var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
      var modelValue = $scope.newAudience.getValueFromVariantType(variantType, value);
      $scope.view.addTargetingSpec(variantType, value);
      $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
    };

    $scope.countries = ['United States', 'Brazil', 'United Kingdom', 'Bosnia', 'India'];

    //unchecking a checkbox eliminates the variant in that section. Used by newDevices, but general.
    $scope.toggleVariant = function (variantType, value) {
      if (value === false) {
        $scope.removeVariant(variantType, 0);
        //$scope.stopEventProp($event);
      }
    };

    $scope.toggleTargetingSpec = function (variantType, value) {
      var pillValue = value;
      var pillIndex = $scope.view.getPillIndexInSelectedVariant(variantType, pillValue);
      var variant = $scope.view.getSelectedVariant(variantType);
      //var variantIndex = $scope.view.getSelectedVariantIndex(variantType);

      if ($scope.view.isValueInSelectedVariant(variantType, pillValue)) {
        $scope.removeTargetingSpecFromVariant(variantType, variant, pillIndex);
      } else {
        $scope.addVariantIfNone(variantType);
        $scope.addTargetingSpec(variantType, value);
      }

    //  TODO: Include Mobile Apps
    //  if(variantType === 'platform'){
    //    var modelValue = {};
    //    modelValue.appId = $scope.appId;
    //    modelValue.value = value;
    //    if(value === 'Android'){
    //      modelValue.appStore = $scope.androidAppInfo.app_store_url; //jshint ignore:line
    //    } else if (value === 'Iphone') {
    //      modelValue.appStore = $scope.iphoneAppInfo.app_store_url; //jshint ignore:line
    //    } else if (value === 'Ipad') {
    //        modelValue.appStore = $scope.ipadAppInfo.app_store_url; //jshint ignore:line
    //      }
    //    $scope.disableOsType(value);
    //    $scope.view.addTargetingSpec('platform', pillValue);
    //    $scope.newAudience.addTargetingSpec('platform', variantIndex, modelValue);
    //    return;
    //  }
    //  $scope.addTargetingSpec(variantType, value);

    };

    $scope.removeTargetingSpecFromVariant = function (variantType, variant, pillIndex) {
      var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
      $scope.view.removePillFromVariant(variant, pillIndex);
      $scope.newAudience.removeTargetingSpec(variantType, variantIndex, pillIndex);
    };

    $scope.addVariant = function (variantType) {
      //Validation if variant is blank

      var variants = $scope.view[variantType].variants;

      if (variants.length > 1) {
        console.log(variants[variants.length - 1].pills.length);
        if (variants[variants.length - 1].pills.length < 1) {
          fsConfirm('genericAlert', {body: $filter('translate')('label.act.Youcannotaddanothervariantifyouhaveablankone')});
          return;
        }
      }
      $scope.view.addVariant(variantType);
      $scope.newAudience.addVariant(variantType);
    };

    $scope.removeVariant = function (variantType, index) {
      $scope.view.removeVariant(variantType, index);
      $scope.newAudience.removeVariant(variantType, index);
      if ($scope.view[variantType].variants.length > 0) {
        $scope.selectVariant(variantType, $scope.view[variantType].variants.length - 1);
      }
    };

    // Manage - Save - Load Audience
    // Open TwAudience Segments modal to manage audience segments
    $scope.manageAudience = function (combo) {
      $scope.metadata.currentCombo = combo;
      //$scope.newAudience = new TwAudience();
      console.log('combo.audience: ', combo.audience);
      // // Retrieve current audience
      $scope.newAudience = angular.copy(combo.audience);
      $scope.view = new TwAudienceView();
      // $scope.twAdAccountId = $scope.campaign.settings.channels.twitter.adAccount.accountId;

      // If there is no audience, instantiate it.
      if (!$scope.newAudience) {
        $scope.newAudience = new TwAudience();
        $scope.newAudience.audience = {};
      } else {
        $scope.view = $scope.generateViewFromAudienceData(angular.copy(combo.audience.audience));
        $scope.selectExistingVariants();
      }

       // if mobile app goal, init mobile os targeting
       if($scope.campaign.settings.goal.goal.name === 'MOBILE_APP_INSTALLS' ||
           $scope.campaign.settings.goal.goal.name === 'MOBILE_APP_ENGAGEMENT'){
           $scope.initMobileOsTargeting();
       }

      ngDialog.open({
        template: 'app/main/act/campaignCreation/twitter/twAudience/twAudienceModal.html',
        className: 'modal modal-large',
        scope: $scope,
        closeByDocument: false
      });
    };

    $scope.generateViewFromAudienceData = function (data) {
      var view = new TwAudienceView();
      for (var variantType in data) {
        view[variantType].variants = data[variantType];
        for (var i = 0; i < data[variantType].length; i++) {
          view[variantType].variants[i].pills = [];
          for (var j = 0; j < data[variantType][i].values.length; j++) {
            var pill = {};

            if (variantType === 'carriers') {
              pill.value = data[variantType][i].values[j].country + ' | ' + data[variantType][i].values[j].value;
            } else if (variantType === 'follower') {
              pill.value = '@' + data[variantType][i].values[j].value;
            } else {
              pill.value = data[variantType][i].values[j].value;
            }

            view[variantType].variants[i].pills.push(pill);
          }
          delete view[variantType].variants[i].values;
        }
      }
      return view;
    };

    $scope.initMobileOsTargeting = function () {
      //find number of supported platforms
      var app = $scope.campaign.settings.goal.application;
      $scope.supportedPlatforms = [];
      $scope.appId = app.appId;
      for (var i = 0; i < app.channelApplications.length; i++) {
        if (app.channelApplications[i].type === 'Twitter') {
          for (var k = 0, l = app.channelApplications[i].nativeApplications.length; k < l; k++) {
            if (app.channelApplications[i].nativeApplications[k].platform === 'ANDROID') {
              $scope.supportedPlatforms.push('Android');
              $scope.androidAppInfo = app.channelApplications[i].nativeApplications[k];
            } else if (app.channelApplications[i].nativeApplications[k].platform === 'IPHONE') {
              $scope.supportedPlatforms.push('Iphone');
              $scope.iphoneAppInfo = app.channelApplications[i].nativeApplications[k];
            } else if (app.channelApplications[i].nativeApplications[k].platform === 'IPAD') {
              $scope.supportedPlatforms.push('Ipad');
              $scope.ipadAppInfo = app.channelApplications[i].nativeApplications[k];
            }
          }
        }
      }
      if (!$scope.view.platform) {
        $scope.view.platform = {};
        $scope.view.platform.variants = [];
      }

      //  TODO: Include Mobile Apps
      //if only one platform, add that platform as variant and disable other radio button, disable add variant
      //if ($scope.supportedPlatforms.length === 1) {
      //  var osType = $scope.supportedPlatforms[0];
      //  var modelValue = {};
      //  modelValue.appId = app.appId;
      //  modelValue.value = osType;
      //  if (osType === 'Android') {
      //    modelValue.appStore = $scope.androidAppInfo.app_store_url; //jshint ignore:line
      //  } else if (osType === 'Iphone') {
      //    modelValue.appStore = $scope.iphoneAppInfo.app_store_url; //jshint ignore:line
      //  } else if (osType === 'Ipad') {
      //    modelValue.appStore = $scope.ipadAppInfo.app_store_url; //jshint ignore:line
      //  }
      //
      //  $scope.addVariantIfNone('platform');
      //  var variantIndex = $scope.view.getSelectedVariantIndex('platform');
      //
      //  if (!$scope.view.isValueInSelectedVariant('platform', osType)) {
      //    $scope.newAudience.addTargetingSpec('platform', variantIndex, modelValue);
      //    $scope.view.addTargetingSpec('platform', osType);
      //    $scope.disableOsType(osType);
      //  }
      //}
    };

    $scope.selectExistingVariants = function () {
      for (var variantType in $scope.view) {
        if (typeof $scope.view[variantType] === 'object') {
          if ($scope.view[variantType].variants.length > 0) {
            $scope.selectVariant(variantType, $scope.view[variantType].variants.length - 1);
          }
        }
      }
    };

    $scope.validateLocation = function (variantIndex, type) {
      var values = $scope.newAudience.audience.location[variantIndex].values;
      for (var i = 0; i < values.length; i++) {
        if (values[i].type !== type) {
          return false;
        }
      }
      return true;
    };

    $scope.selectVariant = function (variantType, index) {
      $scope.view.deselectVariants(variantType);
      $scope.view.selectVariant(variantType, index);
    };

    $scope.focusInput = function (inputId) {
      angular.element('#' + inputId + '_value').focus();
    };

    $scope.addVariantIfNone = function (variantType) {
      if ($scope.view[variantType].variants.length === 0) {
        $scope.addVariant(variantType);
      }
    };

    $scope.focusInput = function (inputId) {
      $timeout(function () {
        angular.element('#' + inputId + '_value')[0].value = '';
      }, 50);
    };

    $scope.loadAudienceFromLibrary = function () {
        // // Close Manage FbAudience Modal view
        // ngDialog.close();
        // Open Library - Load audience modal view

        var opts = {
          channel: $scope.campaign.selectedChannel, 
          currentComboId: $scope.metadata.currentCombo.id
        };

        library.openLibrary('Audiences', opts).then(function(template){
            if (template) {
              // Now the backend persists the loaded audience in the database on load. So we simply reflect the change in the UI and close the modal to avoid having the user making edits then clicking on cancel.
              // Doing that would lose the synchronization between the front end and the backend
              $scope.metadata.currentCombo.audience = new TwAudience(template);
              ngDialog.close();
            }
        });
      };

    $scope.saveAudience = function (saveAudienceToLibraryCheckbox) {
      var duplicateVariants = [];
      var isAudienceValid = true;

      var i = 0;
      var j = 0;

      var category;

      // Verify at lease one location has been specified
      if (!$scope.newAudience.audience.location || $scope.newAudience.audience.location[0].values.length === 0) {
        isAudienceValid = false;
        fsConfirm('genericAlert', {body: $filter('translate')('label.act.Youneedtospecifyatleastonelocation')});
      }

      // Verify at least one primary targeting spec has been specified
      if ((!$scope.newAudience.audience.keyword || $scope.newAudience.audience.keyword[0].values.length === 0) && 
          (!$scope.newAudience.audience.follower || $scope.newAudience.audience.follower[0].values.length === 0) && 
          (!$scope.newAudience.audience.interest || $scope.newAudience.audience.interest[0].values.length === 0) && 
          (!$scope.newAudience.audience.behavior || $scope.newAudience.audience.behavior[0].values.length === 0)) {
        isAudienceValid = false;
        fsConfirm('genericAlert', {body: $filter('translate')('label.act.YouneedtospecifyatleastonePrimary')});
      }

      // Remove empty variants
      if (isAudienceValid) {
        for (category in $scope.newAudience.audience) {
          if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
            i = $scope.newAudience.audience[category].length;
            while (i--) {
              // if they have no pills in the variant, remove that variant
              if ($scope.newAudience.audience[category][i].values.length === 0) {
                $scope.newAudience.audience[category].splice(i, 1);
              }
            }
            // if they have no variants in the category, but the category is still there, just delete the category.
            if ($scope.newAudience.audience[category].length === 0) {
              delete $scope.newAudience.audience[category];
            }
          }
        }
      }

      if (isAudienceValid) {
        // Check that there are no identical variants
        for (category in $scope.newAudience.audience) {
          if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
            for (i = 0, j = $scope.newAudience.audience[category].length; i < j; i++) {
              for (var k = i + 1; k < j; k++) {
                if (angular.equals($scope.newAudience.audience[category][i].values, $scope.newAudience.audience[category][k].values)) {
                  duplicateVariants.push(category);
                  // Exit the loop
                  break;
                }
              }
            }
          }
        }
      }

      // Remove variants that are present more than once in the array
      duplicateVariants = _.uniq(duplicateVariants);

      var messages = [];

      if (duplicateVariants.length > 0) {
        for (i = 0, j = duplicateVariants.length; i < j; i++) {
          messages.push(duplicateVariants[i]);
        }
        fsConfirm('multiLineAlert', {body: $filter('translate')('label.act.Youshouldnthavetwoidenticalvariantsin:'), messages:messages});
      } else if (isAudienceValid) {
        $scope.newAudience.saveToLibrary(saveAudienceToLibraryCheckbox, $scope.campaign);
        $scope.metadata.currentCombo.saveAudience($scope.newAudience);
        if (!saveAudienceToLibraryCheckbox) {
          ngDialog.close();
        }
      }
    };

    //ghetto function for getting Reach working
    $scope.callReach = function () {

      //make a comma-separated list of all the twitter combo ids.
      var twCombosId = [];
      _.map($scope.campaign.combos, function (combo) {
        if(combo.channel === 'TWITTER') {
          twCombosId.push(combo.id);
        }
      });
      twCombosId = twCombosId.join();

      //just the one combo id that you are on. you can use this to test instead if you want.
      //var justOneTwComboId = $scope.combo.id;

      $http.get(apiUrl + '/api/v2/campaign/estimate/reach/' + $scope.campaign.settings.channels.twitter.adAccount.accountId + '/?comboIds=' + twCombosId + '&type=twitter').
        success(function (response) {
          console.log('success! woohoo! reach! ', response);
        }).error(function (error) {
          console.log('error getting Twitter reach: ', error);
        });
    };


    //UTIL FUNCTIONS

    $scope.stopEventProp = function (event) {
      if (event.stopPropagation) {
        event.stopPropagation();
      }
      //just in case browser doesnt detect stopPropagation
      if (event.preventDefault) {
        event.preventDefault();
      }
    };


  });
