'use strict';

angular.module('act')
  .controller('TwPlatformAndDevicesCtrl', function ($scope, $http, apiUrl, AndroidDeviceCategories, $cacheFactory) {

  	$scope.androidDeviceCategories = AndroidDeviceCategories.list;

  	$scope.platforms = {
  		iosDevices: {selected: 0, total: 0},
  		androidDevices: {selected: 0, total: 0}
  	};

  	var cache = $cacheFactory('androidCache');

    var cacheValues = function(key,data){
      cache.put(key,data);
    };

    var getValue = function(key){
      return cache.get(key);
    };

    $scope.getDevicePillIndex = function(deviceType){
    	var variant = $scope.view.platforms.variants[0];
    	if(variant.pills.length){
    		for(var i = 0; i < variant.pills.length; i++){
    			if(variant.pills[i].value.includes(deviceType)){
    				return i;
    			}
    		}
    	}
    };

    $scope.toggleIosDevice = function(device){
      $scope.addVariantIfNone('platforms');
      var modelValue = {};
      modelValue.id = device.targeting_value; //jshint ignore:line  
      modelValue.value =  device.name;
      var variant = $scope.view.getSelectedVariant('platforms');
      var pillValue = 'IOS Devices ( ' + $scope.platforms.iosDevices.selected + ' selected)';
      var pillIndex = $scope.getDevicePillIndex('IOS');
      if(device.selected){
      	$scope.platforms.iosDevices.selected--;
      	pillValue = 'IOS Devices ( ' + $scope.platforms.iosDevices.selected + ' selected)';
        device.selected = false;
        $scope.removeTargetingSpecFromVariant('platforms', variant, pillIndex);
        $scope.view.addTargetingSpec('platforms', pillValue);        	        	
      } else {
      	$scope.platforms.iosDevices.selected++;
      	pillValue = 'IOS Devices ( ' + $scope.platforms.iosDevices.selected + ' selected)';
      	device.selected = true;
      	if(pillIndex || pillIndex === 0){
        	$scope.removeTargetingSpecFromVariant('platforms', variant, pillIndex);      		
      	}
        $scope.view.addTargetingSpec('platforms', pillValue);        	        	
        $scope.newAudience.addTargetingSpec('platforms', 0, modelValue);        
      }      
    };

    $scope.toggleAndroidDevice = function(device){
      $scope.addVariantIfNone('platforms');
      var modelValue = {};
      modelValue.id = device.targeting_value; //jshint ignore:line  
      modelValue.value =  device.name;
      var variant = $scope.view.getSelectedVariant('platforms');
      var pillValue = 'Android ( ' + $scope.platforms.androidDevices.selected + ' selected)';
      var pillIndex = $scope.getDevicePillIndex('Android');
      if(device.selected){
      	$scope.platforms.androidDevices.selected--;
      	pillValue = 'Android ( ' + $scope.platforms.androidDevices.selected + ' selected)';
        device.selected = false;
        $scope.removeTargetingSpecFromVariant('platforms', variant, pillIndex);        	
        $scope.view.addTargetingSpec('platforms', pillValue);        	        	
      } else {
      	$scope.platforms.androidDevices.selected++;
      	pillValue = 'Android ( ' + $scope.platforms.androidDevices.selected + ' selected)';
      	device.selected = true;
      	if(pillIndex || pillIndex === 0){
        	$scope.removeTargetingSpecFromVariant('platforms', variant, pillIndex);      		      		      		
      	}
        $scope.view.addTargetingSpec('platforms', pillValue);        	        	
        $scope.newAudience.addTargetingSpec('platforms', 0, modelValue);        
      }      
    };

    $scope.togglePlatform = function(platform){
    	var pillValue = platform;
    	var modelValue = platform;
    	var pillIndex = $scope.view.getPillIndexInSelectedVariant('platforms', platform);
    	var variant = $scope.view.getSelectedVariant('platforms');
    	$scope.addVariantIfNone('platforms');
    	if(pillIndex || pillIndex === 0){
    		$scope.removeTargetingSpecFromVariant('platforms', variant, pillIndex);        		
    	} else {
    		$scope.view.addTargetingSpec('platforms', pillValue);    		
    	}
        $scope.newAudience.addTargetingSpec('platforms', 0, modelValue);        
    };

  	//TODO maybe this function can be prototype for view.platforms
    $scope.clickOnAccordionRadioBtn = function (obj) {
      var selected = true;
      if ($scope.view.platforms[obj] && $scope.view.platforms[obj].selected === $scope.view.platforms[obj].total) {
        selected = false;
        $scope.view.platforms[obj].selected = 0;
      } else {
        $scope.view.platforms[obj].selected = $scope.view.platforms[obj].total;
      }

      for (var i=0; i<$scope.view.platforms[obj].devices.length; i++) {
        $scope.view.platforms[obj].devices[i].selected = selected;
      }
    };

    $scope.initIosDevices = function(){
    	$scope.iosDevices = [];
    	$http({
  			method: 'GET',
  			url: apiUrl + '/api/v2/autocomplete/twitter/devices?q=ipod'
  		}).success(function(response){
  			console.log(response);
  			$scope.iosDevices = $scope.iosDevices.concat(response.data);
  		});

  		$http({
  			method: 'GET',
  			url: apiUrl + '/api/v2/autocomplete/twitter/devices?q=iphone'
  		}).success(function(response){
  			$scope.iosDevices = $scope.iosDevices.concat(response.data);
  		});

  		$http({
  			method: 'GET',
  			url: apiUrl + '/api/v2/autocomplete/twitter/devices?q=ipad'
  		}).success(function(response){
  			$scope.iosDevices = $scope.iosDevices.concat(response.data);
  		});

    };

    $scope.browseDeviceCategory = function(category){
    	var values = getValue(category);
      	if(values){
        	$scope.androidDevices = values;
      	} else {
    		$scope.browseAndroidLoading = true;
	    	$http({
		  		method: 'GET',
		  		url: apiUrl + '/api/v2/autocomplete/twitter/devices?q=' + category
		  	}).success(function(response){
		  		$scope.browseAndroidLoading = false;
		  		$scope.androidDevices = response.data;
		  		cacheValues(category, response.data);
		  	});
		}
    };

    $scope.browseDeviceCategory($scope.androidDeviceCategories[0]);
    $scope.initIosDevices();





  });