'use strict';

angular.module('act')
  .constant('AndroidDeviceCategories', {
  	list: ['Samsung',
          'HTC',
          'Google',
          'Amazon',
          'Sony',
          'LG',
          'Fujitsu',
          'Sharp',
          'Huawei']
  });