'use strict';

angular.module('act')
  .controller('TwLocationCtrl', function ($scope, apiUrl, $parse, $http, $filter, $timeout) {
  	$scope.locationType = 'country';
  	$scope.twCountryAtpConfig = $scope.atpHelper.getConfig('country');
    $scope.twRegionAtpConfig = $scope.atpHelper.getConfig('region');
    $scope.twCityAtpConfig = $scope.atpHelper.getConfig('city');
    $scope.twZipcodeAtpConfig = $scope.atpHelper.getConfig('zipcode');

  	$scope.addLocationSuggestionToVariant = function(e, data){
        
        var variantType = data.model;
        var pillValue = data.value.name;

        console.log('e:', e, 'data:', data);

        if(data.triggeredBy === 'MANUAL'){
          return;
        }

        if(variantType === 'country' || variantType === 'region' || variantType === 'city' || variantType === 'zipcode'){
            variantType = 'location';
        }

        var modelValue = $scope.newAudience.getValueFromVariantType(variantType, data);
        $scope.addVariantIfNone(variantType);
        var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
        if(variantType === 'location'){
            var validLocation = $scope.validateLocation(variantIndex, data.model);
            if(!validLocation){
                alert($filter('translate')('label.act.Youcannotadddifferentlocationtypesinthesamevariant'));
                $timeout(function(){
                  angular.element('#' + data.model + '_value')[0].value = '';
                }, 50);
                return;
            }
        }
        $scope.view.addTargetingSpec(variantType, pillValue);
        $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
        $scope.focusInput(data.model);
    };

  	$scope.$on('ngAtp:autocomplete', $scope.addLocationSuggestionToVariant);
  });