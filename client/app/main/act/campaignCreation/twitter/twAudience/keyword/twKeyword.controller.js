'use strict';

angular.module('act')

  .controller('TwKeywordCtrl', function ($scope, $timeout, twAudienceWebApiUtils, library) {

    $scope.radioBtn = {
      config: {
        valueField: 'suggestion_value'
      },
      data: []
    };

  	$scope.twKeywords = {};
  	var variantType = 'keyword';

    $scope.kwSpecType = 'Broad Match';

    $scope.selectSpecType = function(specType){
      $scope.kwSpecType = specType;
    };

    $scope.showRecommendations = {};

    $scope.twKeywordAtpConfig = $scope.atpHelper.getConfig('keyword');
    $scope.twKeywordAtpConfig.filter = function (res) {
      return res.data;
    };
    $scope.twKeywordAtpConfig.format = function (itemOfArray) {
      return itemOfArray ? itemOfArray.suggestion_value : ''; //jshint ignore:line
    };
    $scope.twKeywordAtpConfig.verify = function (itemOfArray) {
      return itemOfArray && _.has(itemOfArray, 'suggestion_value');
    };

    // Killing CSV upload :(
  	// $scope.onCsvKeywordImport = function(){

   //    $scope.keywords = _.map(this.result,'keywordCsv');
   //    $scope.showRecommendations.check = false;

   //    _.forEach($scope.keywords, function(value){
   //    	$scope.addVariantAndSpec(variantType, value);
   //    });
  	// };

    $scope.convertToMatchType = function(value){
      if($scope.kwSpecType === 'Exact Match'){
        value =  '\"' + value + '\"';
      } else if($scope.kwSpecType === 'Exclude Broad Match'){
        value = '-' + value;
      } else if($scope.kwSpecType === 'Exclude Exact Match'){
        value = '-\"' + value + '\"';
      }
      return value;
    };

  	$scope.addVariantAndSpec = function(variantType, value){

  		var variants = $scope.view[variantType].variants;
      $scope.moreThan1000 = false;
  		if(variants.length === 0){
	  		$scope.view.addVariant(variantType);
	  		$scope.newAudience.addVariant(variantType); //missing variantID ?
	  	}
      if (variants[0].pills.length > 999) {
        $scope.moreThan1000 = true;
      } else {
        if ($scope.newAudience.isAlreadyAdded(variantType, 0, value, 'value') === false) {
          var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
          var modelValue = $scope.newAudience.getValueFromVariantType(variantType, value);
          $scope.view.addTargetingSpec(variantType, value); //jshint ignore:line
          $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
        }
      }
  	};

  	$scope.changeCopyPaste = function(keyEvent){
  		if (keyEvent.which === 13){
        $scope.applyCopyPaste();
      }
  	};

    $scope.loadKeywordsForTwitter = function() {
      var opts = {
        currentCombo: $scope.metadata.currentCombo
      };

      library.openLibrary('Keywords', opts).then(function(content){
        if(content){
          _.forEach(content.keyword, function(value){
            if($scope.twKeywords.copyPaste) {
              $scope.twKeywords.copyPaste = $scope.twKeywords.copyPaste + '\n' + value.value;
            } else {
              $scope.twKeywords.copyPaste = value.value;
            }
          });
        }
      });
    };

    $scope.applyCopyPaste = function(){
      var untrimmedData;
      if ($scope.twKeywords.copyPaste){
        untrimmedData = $scope.twKeywords.copyPaste.split('\n');
      }
      //trim leading and trailing whitespace
      $scope.keywords = _.map(untrimmedData, function(datum){
        return datum.trim();
      });
      //clear empty strings
      $scope.keywords = $scope.keywords.filter(function(q){ return q;});

      $scope.showRecommendations.check = false;
      _.forEach($scope.keywords, function(value){
        $scope.addVariantAndSpec(variantType, value);
      });

    };

    $scope.addKeywordSuggestionToVariant = function (e, data) {
      $scope.showRecommendations.check = false;
      var value = data.value.suggestion_value; //jshint ignore:line
      if($scope.kwSpecType !== 'Broad Match' ){
        value = $scope.convertToMatchType(value);
      }

      $scope.addVariantAndSpec(variantType, value);
      $timeout(function () {
        angular.element('#' + data.model + '_value')[0].value = '';
      }, 50);
    };
    $scope.$on('ngAtp:autocomplete', $scope.addKeywordSuggestionToVariant);

    $scope.addFn = function(allValues){
      _.forEach(allValues, function(value){
        if (value.selected === true) {
          if(!$scope.view.isValueInSelectedVariant(variantType, value.suggestion_value)){ //jshint ignore:line
            $scope.addVariantAndSpec(variantType, value.suggestion_value); //jshint ignore:line
          }
        }

      });
      $scope.showKeywordList = false;
      $scope.showRecommendationsClick(true);
    };

    $scope.convertToBroadMatch = function(keyword){
      return _.map(keyword, function(kw){

        var temp = {};
        temp.value = kw.value;
        var dashIndex = temp.value.indexOf('-');
        if(dashIndex !== -1){
          temp.value = temp.value.slice(dashIndex + 1, temp.value.length);
        }

        var quoteIndex = temp.value.indexOf('\"');
        if(quoteIndex !== -1){
          temp.value = temp.value.slice(quoteIndex + 1, temp.value.length);
          var quoteIndex2 = temp.value.indexOf('\"');
          temp.value = temp.value.slice(0, quoteIndex2);
        }
        return temp;

      });
    };

    $scope.showRecommendationsClick = function (checked) {
      $scope.twitterOverloaded = false;
      $scope.noKeywordsToRecommend = false;
      var query;
      if (checked === true) {
        var convertedKeywords;
        if($scope.newAudience.audience.keyword && $scope.newAudience.audience.keyword[0].values.length !== 0){
          convertedKeywords = $scope.convertToBroadMatch($scope.newAudience.audience.keyword[0].values);
          query = _.map(convertedKeywords, 'value').join();
          var queryEncoded = encodeURI(query);
          twAudienceWebApiUtils.getRecommendedKeywords(queryEncoded, function (result) {
            $scope.showKeywordList = true;
            $scope.radioBtn.data = result.data.data;
          }, function (error) {
            console.log('Twitter Error: ', error.data.error.message);
            if (error.status === 503 || error.data.error.code === 503){
              $scope.twitterOverloaded = true;
              $scope.showKeywordList = false;
              $scope.showRecommendations.check = false;
            }
            // if (error.status === 500 || error.data.error.code === 500){
            //   $scope.twitterUnhappy = true;
            //   $scope.showKeywordList = false;
            //   $scope.showRecommendations.check = false;
            // }
          });
        } else {
          $scope.noKeywordsToRecommend = true;
        }

      } else {
        $scope.showKeywordList = false;
      }
    };
  });
