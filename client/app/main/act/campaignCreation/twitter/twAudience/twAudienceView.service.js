'use strict';

angular.module('act')
    .factory('TwAudienceView', function () {
    	var TwAudienceView = function(){

            //init variantTypes
            this.location = {};
            this.language = {};
            this.gender = {};
            this.ageRange = {};
            this.platform = {};
            // this.carrier = {};
            // this.newDevices = {};
            this.keyword = {};
            this.follower = {};
            this.interest = {};
            // this.tv = {};
            this.behavior = {};

            //init variants
            this.location.variants = [];
            this.language.variants = [];
            this.gender.variants = [];
            this.ageRange.variants = [];
            this.platform.variants = [];
            // this.carrier.variants = [];
            // this.newDevices.variants = [];
            this.keyword.variants = [];
            this.follower.variants = [];
            this.interest.variants = [];
            this.behavior.variants = [];
            // this.tv.variants = [{
            //             selected: true,
            //             pills: []
            //           }];

    	};

    	TwAudienceView.prototype = {
            addVariant: function(variantType){
                var variant = {};
                variant.pills = [];
                variant.selected = true;
                this.deselectVariants(variantType);
                this[variantType].variants.push(variant);
            },
            addTargetingSpec: function(variantType, value){
              if (!this.isValueInSelectedVariant(variantType, value)) {
                var variants = this[variantType].variants;
                var pill = {};
                pill.value = value;
                for (var i = 0; i < variants.length; i++) {
                  if (variants[i].selected) {
                    variants[i].pills.push(pill);
                  }
                }
              }
            },
            selectVariant: function(variantType, index){
                this[variantType].variants[index].selected = true;
            },

            deselectVariants: function(variantType){
                var variants = this[variantType].variants;
                if(variants){
                    for(var i = 0; i < variants.length; i++){
                        variants[i].selected = false;
                    }
                }
            },
            getSelectedVariant: function(variantType){
                var variants = this[variantType].variants;
                for(var i =0; i < variants.length; i++){
                    if(variants[i].selected){
                        return variants[i];
                    }
                }
            },
            getSelectedVariantIndex: function(variantType){
                var variants = this[variantType].variants;
                for(var i =0; i < variants.length; i++){
                    if(variants[i].selected){
                        return i;
                    }
                }
            },
            getPillIndexInSelectedVariant: function(variantType, value){
                var selectedVariant = this.getSelectedVariant(variantType);
                if(selectedVariant){
                    for(var i = 0; i < selectedVariant.pills.length; i++ ){
                        if(selectedVariant.pills[i].value === value){
                            return i;
                        }
                    }
                }
            },
            isValueInSelectedVariant: function(variantType, value){
                var selectedVariant = this.getSelectedVariant(variantType);
                if(selectedVariant){
                    for(var i = 0; i < selectedVariant.pills.length; i++ ){
                        if(selectedVariant.pills[i].value === value){
                            return true;
                        }
                    }
                }
            },

            removeVariant: function(variantType, index){
                this[variantType].variants.splice(index, 1);
            },
            removePillFromVariant: function(variant, pillIndex){
                variant.pills.splice(pillIndex, 1);
            }
        };

        return TwAudienceView;

    });
