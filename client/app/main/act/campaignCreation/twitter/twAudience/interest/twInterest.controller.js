'use strict';

angular.module('act')
  .controller('TwInterestCtrl', function ($scope, apiUrl, $parse, $http, $filter, $timeout, TwInterestCategories, TwAtpHelper, $cacheFactory) {
      //can only one variant for this targeting spec

    $scope.showBrowseInterest = true;

    var variantType = 'interest';

    var atpHelper = new TwAtpHelper();
    $scope.twInterestAtpConfig = atpHelper.getConfig(variantType);
    
    var cache;
    if (!$cacheFactory.info().interestCache){
     cache = $cacheFactory('interestCache');
    } else {
     cache = $cacheFactory.get('interestCache');
    }

    var cacheValues = function(key,data){
      cache.put(key,data);
    };

    var getValue = function(key){
      return cache.get(key);
    };

    $scope.interestCategories = TwInterestCategories.list;
    $scope.browseCategory = function(category){
      var values = getValue(category);
      if(values){
        $scope.interests = values;
        _.forEach($scope.interests, function (tempInterest) {
          tempInterest.selected = !!$scope.view.isValueInSelectedVariant(variantType, tempInterest.name);
        });
      } else {
        $scope.browseInterestLoading = true;
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/autocomplete/twitter/interests?q=' + category
        }).success(function(response){
          $scope.browseInterestLoading = false;
          $scope.interests = response.data;
          cacheValues(category, response.data);
        });    
      }
    };

    $scope.toggleInterest = function(interest){
      interest.selected = !interest.selected;
    };

    $scope.selectAllInterest = function(interests){
      _.forEach(interests, function(interest){
        if(!$scope.selectAllInterest.checked){
          interest.selected = true;
        } else {
          interest.selected = false;
        }
      });
    };

    $scope.applyInterests = function(interests) {
      _.forEach(interests, function(interest){
        var modelValue = {};
        modelValue.key = interest.targeting_value; //jshint ignore:line  
        modelValue.value =  interest.name;
        $scope.addVariantIfNone(variantType);
        var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
        var pillValue = modelValue.value;
        var variants = $scope.view[variantType].variants;
        var variant = variants[variantIndex];
        var pillIndex = $scope.view.getPillIndexInSelectedVariant(variantType, pillValue);
        $scope.moreThan100 = false;
        if (variants.length > 0 && variants[variantIndex].pills.length > 99) {
          $scope.moreThan100 = true;
        } else if (interest.selected && interest.selected === true && !$scope.view.isValueInSelectedVariant(variantType, interest.name)) {
          $scope.view.addTargetingSpec(variantType, pillValue);
          $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
        } else if ((!interest.selected || interest.selected === false) && $scope.view.isValueInSelectedVariant(variantType, interest.name)) {
          $scope.removeTargetingSpecFromVariant(variantType, variant, pillIndex);
        }
      });
    };

    $scope.parseInterestLabel = function(label){
      var slash = label.indexOf('/');
      return label.slice(slash + 1, label.length);
    };


    $scope.addInterestSuggestionToVariant = function(e, data){
      if(data.triggeredBy === 'MANUAL'){
        return;
      }
      $scope.moreThan100 = false;
      var variants = $scope.view[variantType].variants;
      var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
      if (variants.length > 0 && variants[variantIndex].pills.length > 99) {
        $scope.moreThan100 = true;
      } else {
        $scope.addVariantIfNone(variantType);
        var modelValue = {};
        modelValue.id = data.value.targeting_value; //jshint ignore:line  
        modelValue.value =  data.value.name;
        var pillValue = modelValue.value;
        $scope.view.addTargetingSpec(variantType, pillValue);

    	  //can only have one variant for this targeting spec, hence index '0'
        $scope.newAudience.addTargetingSpec(variantType, 0, modelValue);
        $scope.focusInput(data.model);
      }
    };


    $scope.browseCategory($scope.interestCategories[0]);
    $scope.$on('ngAtp:autocomplete', $scope.addInterestSuggestionToVariant);

  });