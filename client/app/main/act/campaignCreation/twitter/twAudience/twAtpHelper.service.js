'use strict';

angular.module('main')
  .service('TwAtpHelper', function (apiUrl, NonEligibleCountries) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var TwAtpHelper = function(){};

    TwAtpHelper.prototype.getConfig = function(variantType){
      var defaultConfig =  {
        remote: {
          filter : function (res) {
            return res.data;
          }
        },
        verify : function(datum) {
          return datum && _.has(datum, 'name');
        },
        format : function(datum) {
          return datum ? datum.name : '';
        },
        limit: 10,
        idAttribute : 'key'
      };

      //comma-separated string of behavior IDs by which to filter the response
      //var behaviorIds = '';

      if(variantType === 'country'){
          defaultConfig.remote.url = '/api/v2/autocomplete/twitter/locations?location_type=' + 'COUNTRY' + '&q=%QUERY&count=10';
          defaultConfig.idAttribute = 'name';
          defaultConfig.remote.filter = function (res) {
            return _.filter(res.data, function (datum) {
              return NonEligibleCountries.indexOf(datum.name) === -1;
            });
          };
      } else if(variantType === 'region'){
          defaultConfig.remote.url = '/api/v2/autocomplete/twitter/locations?location_type=' + 'REGION' + '&q=%QUERY&count=10';
          defaultConfig.idAttribute = 'name';
      } else if(variantType === 'city'){
          defaultConfig.remote.url = '/api/v2/autocomplete/twitter/locations?location_type=' + 'CITY' + '&q=%QUERY&count=10';
          defaultConfig.idAttribute = 'name';
      } else if(variantType === 'zipcode'){
          defaultConfig.remote.url = '/api/v2/autocomplete/twitter/locations?location_type=' + 'POSTAL_CODE' + '&q=%QUERY&count=10';
          defaultConfig.idAttribute = 'name';
      } else if(variantType === 'language'){
          delete defaultConfig.remote;
          defaultConfig.prefetch = {
            url: apiUrl + '/api/v2/autocomplete/twitter/languages?count=30',
            transform: function(data){
              return data.data;
            },
            cache: true
          };
          defaultConfig.idAttribute = 'name';
      } else if(variantType === 'interest'){
          defaultConfig.remote.url = '/api/v2/autocomplete/twitter/interests?q=%QUERY&count=10';
          defaultConfig.idAttribute = 'name';
      } else if(variantType === 'behavior'){
          delete defaultConfig.remote;
          //defaultConfig.idAttribute = 'id';
          defaultConfig.prefetch = {
            url: apiUrl + '/api/v2/autocomplete/twitter/behaviors/?count=999',
            transform: function(data){
              return data.data;
            },
            cache: true
          };
          defaultConfig.idAttribute = 'name';
      } else if(variantType === 'keyword'){
        defaultConfig.remote.url = '/api/v2/autocomplete/twitter/suggestions/targeting?account_id=18ce53vjuh5&suggestion_type=KEYWORD&targeting_values=' + '%QUERY' + '&count=10';
        defaultConfig.idAttribute = 'suggestion_value';
      } else if(variantType === 'follower'){
        defaultConfig.remote.url = '/api/v2/autocomplete/twitter/user/search?q=' + '%QUERY' + '&count=10';
        defaultConfig.idAttribute = 'screen_name';
        defaultConfig.format = function (datum) {
            return datum ? ('@' + datum.screen_name) : ''; //jshint ignore:line
        };
      }

      if (defaultConfig.remote){
        defaultConfig.remote.url = apiUrl + defaultConfig.remote.url;
      }
      return defaultConfig;
    };

    return TwAtpHelper;

  });
