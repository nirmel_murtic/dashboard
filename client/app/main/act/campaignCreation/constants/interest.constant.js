'use strict';

angular.module('act')
  .constant('TwInterestCategories', {
  	list: ['Automotive',
          'Beauty',
          'Books and literature',
          'Business',
          'Careers',
          'Education',
          'Events',
          'Family and parenting',
          'Food and drink',
          'Gaming',
          'Health',
          'Hobbies and interests',
          'Home and garden',
          'Law, government, and politics',
          'Life stages',
          'Movies and television',
          'Music and radio',
          'Personal finance',
          'Pets',
          'Science',
          'Society',
          'Sports',
          'Style and fashion',
          'Technology and computing',
          'Travel']
  });