'use strict';

angular.module('act')
  .controller('AwKeywordCtrl', function ($scope, awKeywordApi, AwKeyword, ngDialog, $filter, fsConfirm, withInput, $http, fsNotify, $rootScope, apiUrl, library) {
  	$scope.api = awKeywordApi;
    $scope.addKeyword = function(newKeyword){

      if($scope.selectedKeywords.length){
        if($scope.selectedKeywords[$scope.selectedKeywords.length - 1].length > 0){
          $scope.selectedKeywords += '\n' + newKeyword ;
        } else {
          $scope.selectedKeywords += newKeyword + '\n';
        }
      } else {
        $scope.selectedKeywords += newKeyword;
      }
    };

    $scope.removePlaceholder = function(){
      if($scope.placeholder){
        $scope.placeholder = false;
        $scope.selectedKeywords = '';
      }
    };

    $scope.clearSuggestion = function(index){
      $scope.suggestions.splice(index, 1);
    };

    $scope.addSuggestion = function(suggestion, index){
      $scope.removePlaceholder();
      $scope.addKeyword(suggestion.keyword);
      $scope.clearSuggestion(index);
    };

    $scope.addAllSuggestions = function(){
      $scope.removePlaceholder();
      for(var i = 0; i < $scope.suggestions.length; i++){
        $scope.addKeyword($scope.suggestions[i].keyword);
      }
      $scope.suggestions = [];
    };

    $scope.badSuggestion = function(response){
      $scope.noSuggestionsFound = true;
      $scope.suggestionsLoading = false;
      if(response.error.message.includes('auth')){
        $scope.authError = $filter('translate')('label.act.Youarenotloggedintoadwordsmakesuretologintoadwordsinalign');
      }
    },

    $scope.applyKeywordSuggestions = function(result){
      $scope.suggestionsLoading = false;
      $scope.noSuggestionsFound = false;
      $scope.suggestions = result.data;
    }; //jshint ignore:line

    $scope.getKeywordSuggestions = function(keys){
      var accountId = $scope.campaign.settings.channels.adwords.adAccount.accountId;
      $scope.suggestionsLoading = true;
      $scope.suggestions = [];
      awKeywordApi.getKeywordSuggestions(accountId, keys, $scope.applyKeywordSuggestions, $scope.badSuggestion);
    };

  	$scope.onCsvImport = function(){
      $scope.removePlaceholder();
  	  angular.forEach(this.result, function(item) {
	      $scope.addKeyword(item.name);
      });
  	};

    $scope.contradictingMatchTypes = function(keyword){
      // If you remove the '-' sign, (and trim) are they the exact same?
      // I believe this is the simplest way to achieve the AdWords behavior.
      // apple and -apple are not allowed, but
      // apple and -"apple" are.
      for(var i = 0; i < keyword.keyword.length; i++){
        var keyword1 = keyword.keyword[i].value;
        for(var j = i + 1; j < keyword.keyword.length; j++){
          // Starting at i+1 takes it from O(n^2) to O(n*log(n)) (I think?)
          // but then we have to compare both ways (2<->1, 1<->2)
          // Still great savings
          var keyword2 = keyword.keyword[j].value;
          if((keyword2.indexOf('-') === 0 &&
              keyword2.substring(1).trim() === keyword1) ||
             (keyword1.indexOf('-') === 0 &&
              keyword1.substring(1).trim() === keyword2) ){
            return true;
          }
        }
      }
      return false;
    };

    String.prototype.replaceAt = function(index, character) {
      return this.substr(0, index) + character + this.substr(index+character.length);
    };

    $scope.getKeywordsfromSelected = function(selected){
      var regex = new RegExp(/[!@%^*={};~`<>\?¦]/);
      var kws = selected.split(/\n|,/);
      var keyword = new AwKeyword();
      for(var i = 0; i < kws.length; i++){
        var value = {};
        if(kws[i].match(regex)){
          return false;
        }

        //replace left/right double quotes with straight quotes
        if(kws[i].charCodeAt(0) === 8220){
          kws[i] = kws[i].replaceAt(0, '\"');
          kws[i] = kws[i].replaceAt(kws[i].length - 1, '\"');
        }
        value.value = kws[i].trim();

        if(value.value !== '' && value.value !== '\n'){
          keyword.addKeyword(value);
        }


      }
      keyword.keyword = _.uniq(keyword.keyword, function(keyword){
        return angular.toJson(keyword);
      });

      return keyword;
    };

    $scope.saveKeyword = function (saveBox){
      var keyword = $scope.getKeywordsfromSelected($scope.selectedKeywords);
      if(!keyword){
        fsConfirm('genericAlert', {body:$filter('translate')('label.act.Keywordscontainoneormoreunsupportedcharacters')});
        return;
      }

      if(keyword.keyword.length === 0){
        fsConfirm('genericAlert', {body:$filter('translate')('label.act.Nokeywordsselectedorkeywordsnotproperlyformatted')});
        return;
      }

      if($scope.contradictingMatchTypes(keyword)){
        fsConfirm('genericAlert', {body:$filter('translate')('label.act.Nokeywordsselectedorkeywordsnotproperlyformatted')});
        return;
      }

      if(saveBox){
        $scope.saveKeywordToLibrary();
      }
      $scope.metadata.currentCombo.saveKeyword(keyword);
      if(!saveBox){
        ngDialog.close();        
      }
    };

    $scope.saveKeywordToLibrary = function(){
      var keyword = $scope.getKeywordsfromSelected($scope.selectedKeywords);

      fsConfirm('confirmationWithInput', {
        body: $filter('translate')('label.act.Nameyourkeywords'),
        title: 'My Keywords'
      }).then(function () {

        keyword.name = withInput.getInput();

        var params = {
          channelName: 'ADWORDS',
          objectId: 0,
          libraryObjectType: 'keyword',
          data: angular.toJson(keyword)
        };

        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/library/save/template/' + $rootScope.fractalAccountInfo.currentTeam.id,
          data: $.param(params)
        }).
          success(function (response) {

            console.log('Audience response: ', response);
            ngDialog.close();
            fsNotify.push(
              $filter('translate')('label.act.Keywordssavedtolibrary')+': '+ keyword.name
            );
            withInput.destroyInput();
          }).
          error(function (response) {
            withInput.destroyInput();
            console.log(response);
          });
      });
    };

    $scope.loadKeywordFromLibrary = function () {
      // Open Library - Load Image modal view
      var opts = {
        currentComboId: $scope.metadata.currentCombo.parentCmpId
      };
      library.openLibrary('Keywords', opts).then(function(content){
        // $scope.metadata.currentCombo.saveKeyword(content.keyword);
        $scope.removePlaceholder();
        for(var i = 0; i < content.keyword.length; i++){
          $scope.addKeyword(content.keyword[i].value);
        }        
      });
    };

    $scope.manageKeyword = function (combo) {
        $scope.metadata.currentCombo = combo;
        // Retrieve current keyword
        $scope.keyword = angular.copy(combo.keyword);
        // If there is no keyword, instantiate it.
        if ($scope.keyword.keyword.length === 0) {
          $scope.keyword = new AwKeyword();
          $scope.placeholder = true;
          $scope.selectedKeywords = 'Example: \n accomplice \n "accomplice blog" \n [accomplice blog] \n +accomplice \n -accomplice';
        } else {
          $scope.removePlaceholder();
          $scope.selectedKeywords = '';
          $scope.placeholder = false;
          for(var i = 0; i < $scope.keyword.keyword.length; i++){
            $scope.addKeyword($scope.keyword.keyword[i].value);
          }
        }

        ngDialog.open({
            template: 'app/main/act/campaignCreation/adwords/awKeyword/awKeyword.html',
            className: 'modal modal-large',
            controller: 'AwKeywordCtrl',
            scope: $scope
        });
    };
  });
