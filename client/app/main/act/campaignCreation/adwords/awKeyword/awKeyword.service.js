'use strict';

angular.module('act')
    .factory('AwKeyword', function () {
    	var AwKeyword = function(keyword){

            if (keyword) {
                for (var property in keyword) {
                    if (keyword.hasOwnProperty(property)) {
                        this[property] = keyword[property];
                    }
                }
                return;
            }

            this.id = null;
            this.keyword = [];


    	};

    	AwKeyword.prototype = {
    		addKeyword: function(keyword){
                this.keyword.push(keyword);
    		},
    		removeKeyword: function(index){
    			this.keyword.splice(index, 1);
    		}
    	};

    	return AwKeyword;

    });