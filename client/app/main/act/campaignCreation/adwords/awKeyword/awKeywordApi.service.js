'use strict';

angular.module('act')
    .factory('awKeywordApi', function ($http, apiUrl) {
    	return {
    		getKeywordSuggestions: function(accountId, keys, successCallback, error){
    			$http({
                	method: 'GET',
                	url: apiUrl + '/api/v2/autocomplete/google/keywordSuggestion/' + accountId + '?q=' + keys
            	})
            	.success(successCallback)
            	.error(error);
    		},
    		saveKeywordsToCombo: function(successCallback, errorCallback){
    			$http({
                	method: 'POST',
                	url: apiUrl + '/api/v2/',
            	})
            	.success(successCallback)
            	.error(errorCallback);
    		}
    	};
    });