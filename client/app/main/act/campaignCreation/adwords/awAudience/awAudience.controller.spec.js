'use strict';

describe('Controller: AwAudienceCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var AwAudienceCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    
    $rootScope.fractalAccountInfo = {};
    $rootScope.fractalAccountInfo.currentTeam = {};
    $rootScope.fractalAccountInfo.currentTeam.id = 0;
    // $scope.api = {};
    // $scope.api.facebookPages = [];
    scope = $rootScope.$new();
    AwAudienceCtrl = $controller('AwAudienceCtrl', {
    $scope: scope = {
                      api: {
                        facebookPages: []
                      }, 
                      campaign: {
                        settings: {
                          channels:{
                            facebook:{
                              adAccount: {
                                accountId: 0
                              }
                            }
                          }
                        }
                      }, 
                      $on: function(){}
                    }
    });



  }));

  it('should add variant to model and view from variant type', function(){
    var variantType = 'location';
    scope.addVariant(variantType);
    expect(scope.view[variantType].variants[0]).toBeDefined();
    expect(scope.newAudience.audience[variantType][0]).toBeDefined();
  });

  it('should add autocomplete value to model and view from variant type and value', function(){
    var variantType = 'location';
    var countryAtpValue = {
                  model: 'country', 
                  value: {
                    name: 'United States', 
                    country_code: 'US' //jshint ignore:line
                  }
                }; 
    scope.addVariant(variantType);
    scope.addAtpSuggestionToVariant(variantType, countryAtpValue);
    expect(scope.view[variantType].variants[0].pills[0].value).toBe('United States');
    expect(scope.newAudience.audience[variantType][0].values[0].value).toBe('United States');
  });

  it('should remove variant from model and view from variantType and index', function(){
    var variantType = 'location';
    scope.addVariant(variantType);
    scope.removeVariant(variantType, 0);
    expect(scope.view[variantType].variants.length).toBe(0);
    expect(scope.newAudience.audience[variantType]).toBeUndefined();  
  });

  it('should remove targeting spec from model and view from variantType, variant, pillIndex', function(){
    //after reviewing code, this method should be refactored and then test should be written again. 
    //We can do everything with variantIndex. We dont need to pass the whole variant
    var variantType = 'location';
    var value = {
              model: 'country', 
              value: {
                name: 'United States', 
                country_code: 'US' //jshint ignore:line
              }
            }; 
    scope.addVariant(variantType);
    scope.addAtpSuggestionToVariant(variantType, value);
    scope.removeTargetingSpecFromVariant(variantType, scope.view[variantType].variants[0], 0);
    expect(scope.view[variantType].variants[0].pills.length).toBe(0);
    expect(scope.newAudience.audience[variantType][0].values.length).toBe(0);

  });

  //Extension testing
  it('should add extension to model and view', function(){

    var extensionType = 'call';
    var call = {phoneNumber: '5555555555'};
    scope.addExtension(extensionType, call);

    expect(scope.view.extensions[extensionType][0]).toBe(call);
    expect(scope.newExtension.extension[extensionType][0]).toBe(call);

  });

  it('should remove extension from model and view', function(){
    var extensionType = 'call';
    var call = {phoneNumber: '5555555555'};
    scope.addExtension(extensionType, call);

    scope.removeExtension(extensionType, 0);
    expect(scope.view.extensions[extensionType].length).toBe(0);
    expect(scope.newExtension.extension[extensionType].length).toBe(0);

  });
});
