'use strict';

angular.module('act')
    .service('AwAudience', function (Audience, apiUrl, $rootScope, fsUtils, $http, fsConfirm, $filter, withInput, ngDialog, fsNotify) {

    	var AwAudience;

        AwAudience = (function() {
            function AwAudience(audience) {
                // Call the super constructor Creative
                this.constructor.super_.call(this);

                if (audience) {
                    for (var property in audience) {
                        if (audience.hasOwnProperty(property)) {
                            this[property] = audience[property];
                        }
                    }
                    return;
                }

                this.id = null;
                // All info related to the audience stored here
                this.audience = {};
            }

            // Implement inheritance from Creative abstract class
            fsUtils.inherits(AwAudience, Audience);

            return AwAudience;
        })();

    	AwAudience.prototype.addVariant = function(variantType, variantId){
            var variants = this.audience[variantType];
            var emptyVariant = {
                variantId: variantId,
                values: []
            };

            if(!variants || variants.length === 0){
                this.audience[variantType] = [];
            } else {
                if(variants[variants.length - 1].values.length < 1 ){
                    fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddanothervariantifyouhaveablankone')});
                    // $scope.focusInput(variantType);
                    return;
                }
            }

            this.audience[variantType].push(emptyVariant);
        };


        AwAudience.prototype.addTargetingSpec = function(variantType, variantIndex, value){
            this.audience[variantType][variantIndex].values.push(value);
        };

        AwAudience.prototype.getValueFromVariantType = function(variantType, data){
            var value = {};
            if(variantType === 'location'){
                if(data.model === 'city'){
                    var i = data.value.canonicalName.indexOf(',');
                    var j = data.value.canonicalName.indexOf(',', i + 1);
                    value.value = data.value.name + ', ' +  data.value.canonicalName.slice(i + 1, j);
                } else {
                    value.value = data.value.name;
                }
                value.key = data.value.countryCode; //jshint ignore:line
                value.type = data.model;
                value.criteriaId = data.value.criteriaId;

            } else if(variantType === 'language'){
                value.value = data.value.languageName;
                value.key = data.value.languageCode;
                value.criteriaId = data.value.criteriaId;
            } else if(variantType === 'gender'){
                value.value = data;
            } else if(variantType === 'devices'){
                value.value = data;
            }
            return value;
        };

        AwAudience.prototype.removeTargetingSpec = function(variantType, variantIndex, valueIndex){
            var variant = this.audience[variantType][variantIndex];
            variant.values.splice(valueIndex, 1);
        };

        AwAudience.prototype.removeVariant = function(variantType, index){
            this.audience[variantType].splice(index, 1);
            if(this.audience[variantType].length === 0 ){
                delete this.audience[variantType];
            }
        };

        AwAudience.prototype.saveToLibrary = function(save, campaign){
            if(save){
              var newData = this;
              //set audience name to blank
              withInput.setInput('');
              fsConfirm('confirmationWithInput', {
                body: $filter('translate')('label.act.Nameyouraudience'),
                title: 'My Audience'
              }).then(function () {

                newData.name = withInput.getInput();

                var channel = campaign.selectedChannel.toUpperCase();

                var params = {
                    channelName: channel,
                    objectId: 0,
                    libraryObjectType: 'audience',
                    data: angular.toJson(newData)
                };

                $http({
                  method: 'POST',
                  url: apiUrl + '/api/v2/library/save/template/' + $rootScope.fractalAccountInfo.currentTeam.id,
                  data: $.param(params)
                }).
                  success(function (response) {

                    console.log('Audience response: ', response);
                    ngDialog.close();
                    fsNotify.push(
                      $filter('translate')('label.act.Audiencesavedtolibrary')+': '+ newData.name
                    );
                    withInput.destroyInput();
                  }).
                  error(function (response) { // optional
                    withInput.destroyInput();
                    console.log(response);
                  });

              });
            }
        };

    	return AwAudience;

    });
