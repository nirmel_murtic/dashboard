'use strict';

angular.module('act')
  .controller('AwAudienceCtrl', function ($scope, $parse, $http, $filter, $timeout, awAtpHelper, ngDialog, AwAudience, AwExtension, AwAudienceView, fsConfirm, library, regex) {
  	$scope.locationType = 'country';
    $scope.view = new AwAudienceView();
    $scope.newAudience = new AwAudience();
    $scope.newExtension = {};
    $scope.newExtension.extension = {};
    $scope.address = {};
    $scope.call = {};
    $scope.siteLink = {};
  	$scope.awCountryAtpConfig = awAtpHelper.getConfig('country');
  	$scope.awRegionAtpConfig = awAtpHelper.getConfig('region');
  	$scope.awCityAtpConfig = awAtpHelper.getConfig('city');
  	$scope.awZipcodeAtpConfig = awAtpHelper.getConfig('zipcode');
  	$scope.awLanguageAtpConfig = awAtpHelper.getConfig('language');
    $scope.shortUrlRegex = regex.DIEGOSHORTURL.source;
    $scope.getSizeOfObject = _.size; // To be able to use it in ngShow
    var siteLinkForm;

    $scope.addVariant = function(variantType){
      //Validation if variant is blank
      var variants = $scope.view[variantType].variants;
      if(variants.length > 1){
          if(variants[variants.length - 1].pills.length < 1 ){
              fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddanothervariantifyouhaveablankone')});
              // $scope.focusInput(variantType);
              return;
          }
      }
    	$scope.view.addVariant(variantType);
    	$scope.newAudience.addVariant(variantType);
    };

    $scope.validateExtension = function(extensionType, value){
      if($scope.view.extensions[extensionType]){
        if(extensionType === 'siteLink' ){
            if($scope.view.extensions[extensionType].length >= 3){
              fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddmorethan4SiteLinks')});
              return false;
            }

            // Add http if it's not there already
            if (value.linkUrl && ! _.contains(value.linkUrl, 'http')) {
              value.linkUrl = 'http://'+value.linkUrl;
            }

          siteLinkForm.$setPristine();

        } else if (extensionType === 'call') {
            if (_.isEmpty(value)) {
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Pleaseenteravalidphonenumber')});
                return false;
            } 
            if ($scope.view.extensions[extensionType].length >= 1) {
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddmorethan1PhoneNumber')});
                return false;
            } else if(value.phoneNumber.length < 7){
                fsConfirm('genericAlert', {body:$filter('translate')('label.act.Pleaseenteravalidphonenumber')});
            } else {
                return true;
            }
        }
        else {
          return true;
        }

      }
      return true;
    };

    $scope.addExtension = function(extensionType, value){
      var valid = $scope.validateExtension(extensionType, value);
      if(!valid){
        return;
      }

    	$scope.view.addExtension(extensionType, value);
      if(!$scope.newExtension.extension[extensionType]){
        $scope.newExtension.extension[extensionType] = [];
      }
      $scope.newExtension.extension[extensionType].push(value);
      $scope.clearExtension(extensionType);
    };

    $scope.editExtension = function(extensionType, index){
      $scope[extensionType] = $scope.view.extensions[extensionType][index];
      $scope.removeExtension(extensionType, index);
    };

    $scope.removeExtension = function(extensionType, index){
      $scope.view.removeExtension(extensionType, index);
      $scope.newExtension.extension[extensionType].splice(index, 1);
    };

    $scope.clearExtension = function(extensionType){
    	$scope[extensionType] = {};
    };

    $scope.findValueInSelectedVariant = function(variantType, value){
    	return this.view.findValueInSelectedVariant(variantType, value);
    };

  	$scope.addVariantIfNone = function(variantType){
  		if($scope.view[variantType].variants.length === 0){
    		$scope.addVariant(variantType);
    	}
  	};

    $scope.checkForDuplicates = function(variantType, variantIndex, pillValue){
      var variant = $scope.view[variantType].variants[variantIndex];
      for(var i = 0; i < variant.pills.length; i++){
          if(variant.pills[i].value === pillValue){
              return true;
          }
      }
      return false;
    };

    $scope.addAtpSuggestionToVariant = function(e, data){
    	var variantType = data.model;

      // if(data.triggeredBy === 'MANUAL'){
      //  return;
      // }

      var pillValue;
      if(variantType === 'country' || variantType === 'region' || variantType === 'city' || variantType === 'zipcode'){
        if(variantType === 'city'){
            var i = data.value.canonicalName.indexOf(',');
            var j = data.value.canonicalName.indexOf(',', i + 1);
            if (data.value.countryCode === 'US') {
              pillValue = data.value.name + ', ' +  data.value.canonicalName.slice(i + 1, j);
            } else {
              pillValue = data.value.name + ', ' +  data.value.canonicalName.slice(j + 1);
            }
        } else {
            pillValue = data.value.name;
        }
        variantType = 'location';
      } else if(variantType === 'language'){
        pillValue = data.value.languageName;
      }

      var modelValue = $scope.newAudience.getValueFromVariantType(variantType, data);
      $scope.addVariantIfNone(variantType);
      var variantIndex = $scope.view.getSelectedVariantIndex(variantType);
       if(variantType === 'location'){
          var validLocation = $scope.validateLocation(variantIndex, data.model);
          if(!validLocation){
            fsConfirm('generic', {
              body: $filter('translate')('label.act.Youcannotadddifferentlocationtypesinthesamevariant'),
              yes: $filter('translate')('label.act.Addtoanewvariant', {value:modelValue.value}),
              no: $filter('translate')('label.act.Cancel')
            }).then(function() {
              // Add a variant if the user desires
              $scope.addVariant(variantType);
              // Retrieve updated variantIndex and modelValues because we added a new variant
              variantIndex = $scope.view.getSelectedVariantIndex(variantType);
              modelValue = $scope.newAudience.getValueFromVariantType(variantType, data);
              // Save the model and the view
              $scope.view.addTargetingSpec(variantType, pillValue);
              $scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
              $timeout(function(){
                angular.element('#' + data.model + '_value')[0].value = '';
              }, 50);
            });
            return;
            }
        }
      if($scope.checkForDuplicates(variantType, variantIndex, pillValue)){
          fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youcannotaddtheduplicatevaluestothesamevariant')});
          $timeout(function(){
              angular.element('#' + data.model + '_value')[0].value = '';
          }, 50);
          return;
      }
      $scope.view.addTargetingSpec(variantType, pillValue);
    	$scope.newAudience.addTargetingSpec(variantType, variantIndex, modelValue);
      $timeout(function(){
        angular.element('#' + data.model + '_value')[0].value = '';
      }, 50);
    };

    $scope.selectVariant = function(variantType, index){
    	$scope.view.deselectVariants(variantType);
    	$scope.view.selectVariant(variantType, index);
    };

    $scope.removeVariant = function(variantType, index){
    	$scope.view.removeVariant(variantType, index);
    	$scope.newAudience.removeVariant(variantType, index);
        if($scope.view[variantType].variants.length > 0){
            $scope.selectVariant(variantType, $scope.view[variantType].variants.length - 1);
        }
    };

    $scope.validateLocation = function(variantIndex, type){
        var values = $scope.newAudience.audience.location[variantIndex].values;
        for(var i = 0; i < values.length; i++){
            if(values[i].type !== type){
                return false;
            }
        }
        return true;
    };

    $scope.generateViewFromAudienceData = function(data){
        var view = new AwAudienceView();
        for(var variantType in data){
            view[variantType].variants = data[variantType];
            for(var i = 0; i < data[variantType].length; i++){
                view[variantType].variants[i].pills = [];
                for(var j = 0; j < data[variantType][i].values.length; j++){
                    var pill = {};
                    pill.value = data[variantType][i].values[j].value;
                    view[variantType].variants[i].pills.push(pill);
                }
                delete view[variantType].variants[i].values;
            }
        }
        return view;
    };

    $scope.removeTargetingSpecFromVariant = function(variantType, variant, pillIndex){
      var variantIndex = _.findIndex($scope.view[variantType].variants, function(v){
        return angular.equals(v, variant);
      });
      $scope.view.removePillFromVariant(variant, pillIndex);
      $scope.newAudience.removeTargetingSpec(variantType, variantIndex, pillIndex);
    };

    //listen for autocomplete
    $scope.$on('ngAtp:autocomplete', $scope.addAtpSuggestionToVariant);

    //UTIL FUNCTIONS

    $scope.stopEventProp = function(event){
	 	    if (event.stopPropagation) {
	        event.stopPropagation();
	      }
	      //just in case browser doesnt detect stopPropagation
	      if (event.preventDefault) {
	        event.preventDefault();
	      }
    };

    $scope.focusInput = function(inputId){
        angular.element('#' + inputId + '_value').focus();
    };

    $scope.saveAudience = function (saveBox) {
        var duplicateVariants = [];
        var isAudienceValid = true;
        var duplicateValuesInVariant = [];

        var i = 0;
        var j = 0;

        var category;

        // Verify at lease one location has been specified
        if (!$scope.newAudience.audience.location || $scope.newAudience.audience.location[0].values.length === 0){
            isAudienceValid = false;
            fsConfirm('genericAlert', {body:$filter('translate')('label.act.Youneedtospecifyatleastonelocation')});
        }

        // Remove empty variants
        if (isAudienceValid) {
            for (category in $scope.newAudience.audience) {
                if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
                    for ( i = 0, j = $scope.newAudience.audience[category].length; i < j; i++) {
                        if ($scope.newAudience.audience[category][i].values.length === 0) {
                          $scope.newAudience.audience[category].splice(i, 1);
                        }
                    }
                }
            }
        }

        if (isAudienceValid){
            // Check that there are no identical variants
            for (category in $scope.newAudience.audience) {
                if ($scope.newAudience.audience.hasOwnProperty(category) && $scope.newAudience.audience[category] !== undefined) {
                    for ( i = 0, j = $scope.newAudience.audience[category].length; i < j; i++) {
                        for (var k = i + 1; k < j; k++) {
                            if (angular.equals($scope.newAudience.audience[category][i].values, $scope.newAudience.audience[category][k].values)) {
                                duplicateVariants.push(category);
                                // Exit the loop
                                break;
                            }
                        }
                    }
                }
            }
        }


        // Remove variants that are present more than once in the array
        duplicateVariants = _.uniq(duplicateVariants);

        var messages = [];

        if (duplicateVariants.length > 0) {
            for ( i = 0, j = duplicateVariants.length; i < j; i++) {
                messages.push(duplicateVariants[i]);
            }
            fsConfirm('multiLineAlert', {body:$filter('translate')('label.act.Youshouldnthavetwoidenticalvariantsin:'), messages:messages});
        } else if (duplicateValuesInVariant.length > 0) {
            for ( i = 0, j = duplicateValuesInVariant.length; i < j; i++) {
                messages.push(duplicateValuesInVariant[i].value + ' in variant '+ duplicateValuesInVariant[i].variant + ' of ' + duplicateValuesInVariant[i].category);
            }
            fsConfirm('multiLineAlert', {body:$filter('translate')('label.act.Youshouldnthaveidenticalvaluesinvariants:'), messages:messages});
        }
        else if(isAudienceValid) {
            $scope.metadata.currentCombo.saveAudience($scope.newAudience);
            $scope.metadata.currentCombo.saveExtension($scope.newExtension);
            if(!saveBox) {
              ngDialog.close();
            }
        }
    };

    $scope.selectExistingVariants = function(){
      for(var variantType in $scope.view){
        if(variantType !== 'extensions' && typeof $scope.view[variantType] === 'object'){
          if($scope.view[variantType].variants.length > 0){
            $scope.selectVariant(variantType, $scope.view[variantType].variants.length - 1);
          }
        }
      }
    };

    $scope.loadAudienceFromLibrary = function () {
        // // Close Manage FbAudience Modal view
        // ngDialog.close();
        // Open Library - Load audience modal view

        var opts = {
          channel: $scope.campaign.selectedChannel, 
          currentComboId: $scope.metadata.currentCombo.id
        };

        library.openLibrary('Audiences', opts).then(function(template){
            if (template) {
              // Now the backend persists the loaded audience in the database on load. So we simply reflect the change in the UI and close the modal to avoid having the user making edits then clicking on cancel.
              // Doing that would lose the synchronization between the front end and the backend
              $scope.metadata.currentCombo.audience = new AwAudience(template);
              ngDialog.close();
            }
        });
    };


    $scope.manageAudience = function (combo) {
        $scope.metadata.currentCombo = combo;
        console.log('combo.audience: ', combo);
        // Retrieve current audience
        $scope.newAudience = angular.copy(combo.audience);
        $scope.newExtension = angular.copy(combo.extension);
        $scope.view = new AwAudienceView();

        // If there is no audience, instantiate it, otherwise, generate the view
        if (!$scope.newAudience) {
            $scope.newAudience = new AwAudience();
            $scope.newAudience.audience = {};
        } else {
          $scope.view = $scope.generateViewFromAudienceData(angular.copy(combo.audience.audience));
          $scope.selectExistingVariants();
        }


        //generate extension model/view
        if(!$scope.newExtension){
          $scope.newExtension = {};
          $scope.newExtension.extension = {};
        } else {
          $scope.view.extensions = angular.copy($scope.newExtension.extension);
          if(typeof $scope.view.extensions === 'string'){
            $scope.view.extensions = angular.fromJson($scope.view.extensions).extension;
          }
          if(!$scope.view.extensions.address){
            $scope.view.extensions.address = [];
            $scope.address = {};
          } else if(!$scope.view.extensions.siteLink){
            $scope.view.extensions.siteLink = [];
            $scope.siteLink = {};
          } else if(!$scope.view.extensions.call){
            $scope.view.extensions.call = [];
            $scope.call = {};
          }
        }

        ngDialog.open({
            template: 'app/main/act/campaignCreation/adwords/awAudience/templates/awAudience.html',
            className: 'modal modal-large',
            scope: $scope
        });
    };

    $scope.setForm = function(form) {
      siteLinkForm = form;
    };

  });
