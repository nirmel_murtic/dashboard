'use strict';

angular.module('act')
    .factory('AwAudienceView', function () {
    	var AwAudienceView = function(){
            //init variantTypes
            this.location = {};
            this.language = {};
            this.gender = {};
            this.devices = {};

            //init variants
            this.location.variants = [];
            this.language.variants = [];
            this.gender.variants = [];
            this.devices.variants = [];

            //init extensions
            this.extensions = {};
            this.extensions.call = [];
            this.extensions.address = [];
            this.extensions.siteLink = [];
    	};

    	AwAudienceView.prototype = {
            addVariant: function(variantType){
                var variant = {};
                variant.pills = [];
                variant.selected = true;
                this.deselectVariants(variantType);
                this[variantType].variants.push(variant);
            },

            removeVariant: function(variantType, index){
                this[variantType].variants.splice(index, 1);
            },

            getSelectedVariant: function(variantType){
                var variants = this[variantType].variants;
                for(var i =0; i < variants.length; i++){
                    if(variants[i].selected){
                        return variants[i];
                    }
                }
            },
            getSelectedVariantIndex: function(variantType){
                var variants = this[variantType].variants;
                for(var i =0; i < variants.length; i++){
                    if(variants[i].selected){
                        return i;
                    }
                }
            },
            getPillIndexInSelectedVariant: function(variantType, value){
                var selectedVariant = this.getSelectedVariant(variantType);
                if(selectedVariant){
                    for(var i = 0; i < selectedVariant.pills.length; i++ ){
                        if(selectedVariant.pills[i].value === value){
                            return i;
                        }
                    }
                }
            },
            isValueInSelectedVariant: function(variantType, value){
                var selectedVariant = this.getSelectedVariant(variantType);
                if(selectedVariant){
                    for(var i = 0; i < selectedVariant.pills.length; i++ ){
                        if(selectedVariant.pills[i].value === value){
                            return true;
                        }
                    }
                }
            },
            addTargetingSpec: function(variantType, value){
                var variants = this[variantType].variants;
                var pill = {};
                pill.value = value;
                for(var i = 0; i < variants.length; i++){
                    if(variants[i].selected){
                        variants[i].pills.push(pill);
                    }
                }
            },
            selectVariant: function(variantType, index){
                this[variantType].variants[index].selected = true;
            },
            deselectVariants: function(variantType){
                var variants = this[variantType].variants;
                if(variants){
                    for(var i = 0; i < variants.length; i++){
                        variants[i].selected = false;
                    }
                }
            },
            removePillFromVariant: function(variant, pillIndex){
                variant.pills.splice(pillIndex, 1);
            },

    		addExtension: function(extensionType, value){
                this.extensions[extensionType].push(value);
    		},

            removeExtension: function(extensionType, index){
                this.extensions[extensionType].splice(index, 1);
            }
    	};

    	return AwAudienceView;

    });
