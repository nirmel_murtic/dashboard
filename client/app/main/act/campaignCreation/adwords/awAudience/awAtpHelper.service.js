'use strict';

angular.module('act')
  .factory('awAtpHelper', function (apiUrl) {
    return {
      getConfig: function (variantType) {

        var defaultConfig = {
          remote: {
            filter: function (res) {
              return res.data;
            }
          },
          verify: function (datum) {
            return datum && _.has(datum, 'name');
          },
          format: function (datum) {
            return datum ? datum.name : '';
          },
          limit: 25,
          idAttribute: 'key'
        };

        if (variantType === 'country') {
          defaultConfig.remote.url = '/api/v2/autocomplete/google/location?&type=' + variantType + '&q=%QUERY';
          defaultConfig.idAttribute = 'criteriaId';
        } else if (variantType === 'region') {
          defaultConfig.remote.url = '/api/v2/autocomplete/google/location?&type=' + 'state' + '&q=%QUERY';
          defaultConfig.idAttribute = 'criteriaId';
        } else if (variantType === 'city') {
          var i, j;
          defaultConfig.remote.url = '/api/v2/autocomplete/google/location?&type=' + variantType + '&q=%QUERY';
          defaultConfig.idAttribute = 'criteriaId';
          defaultConfig.format = function (datum) {
            if (datum) {
              i = datum.canonicalName.indexOf(',');
              j = datum.canonicalName.indexOf(',', i + 1);
            }
            return datum ? datum.name + ', ' + datum.canonicalName.slice(i + 1, j) : '';
          };
        } else if (variantType === 'zipcode') {
          defaultConfig.remote.url = '/api/v2/autocomplete/google/location?&type=' + variantType + '&q=%QUERY';
          defaultConfig.idAttribute = 'criteriaId';
        } else if (variantType === 'language') {
          defaultConfig.remote.url = '/api/v2/autocomplete/google/language?q=%QUERY';
          defaultConfig.idAttribute = 'criteriaId';
          defaultConfig.format = function (datum) {
            return datum ? datum.languageName : '';
          };
          defaultConfig.verify = function (datum) {
            return datum && _.has(datum, 'languageName');
          };
        } else if (variantType === 'iosStoresCountries') {
          defaultConfig.remote.url = '/api/v2/autocomplete/country/?query=%QUERY';
          defaultConfig.idAttribute = 'iso2';
          defaultConfig.remote.filter = function (res) {
            return res.data.data;
          };
          defaultConfig.format = function (itemOfArray) {
            return itemOfArray ? itemOfArray.country : '';
          };
          defaultConfig.verify = function (itemOfArray) {
            return itemOfArray && _.has(itemOfArray, 'country');
          };
        }
        defaultConfig.remote.url = apiUrl + defaultConfig.remote.url;
        return defaultConfig;
      }
    };
  });
