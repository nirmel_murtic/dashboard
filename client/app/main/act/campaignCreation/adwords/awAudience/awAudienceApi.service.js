'use strict';

angular.module('act')
    .factory('awAudienceApi', function ($http, apiUrl) {
    	return {
    		saveAudienceToCombo: function(successCallback, errorCallback){
    			$http({
                	method: 'POST',
                	url: apiUrl + '/api/v2/',
            	})
            	.success(successCallback)
            	.error(errorCallback);

    		}
    	};
    });