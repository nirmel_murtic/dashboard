'use strict';

angular.module('act')
  .service('AwCreative', function ($http, fsUtils, Creative) {
    // AngularJS will instantiate a singleton by calling "new" on this function
        var AwCreative;

        AwCreative = (function () {
            function AwCreative(creative) {

                // Call the super constructor Creative
                this.constructor.super_.call(this);

                if (creative) {
                    for (var property in creative) {
                        if (creative.hasOwnProperty(property)) {
                            this[property] = creative[property];
                        }
                    }
                    return;
                }
                // All info related to the creative stored here.
                this.creative = {
                    headline: null,
                    descriptionLine1: null,
                    descriptionLine2: null,
                    displayUrl: null,
                    destinationUrl: null
                };
            }

            // Implement inheritance from Creative abstract class
            fsUtils.inherits(AwCreative, Creative);

            return AwCreative;
        })();

        return AwCreative;

    });
