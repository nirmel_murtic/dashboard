'use strict';

describe('Service: AwCreative', function () {

  // load the service's module
  beforeEach(module('act'));

  // instantiate service
  var AwCreative;
  beforeEach(inject(function (_AwCreative_) {
      AwCreative = _AwCreative_;
  }));

  it('should do something', function () {
    expect(!!AwCreative).toBe(true);
  });

});
