'use strict';

describe('Controller: AwCreativeCtrl', function () {

  // load the controller's module
  beforeEach(module('act'));

  var scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
  }));
});
