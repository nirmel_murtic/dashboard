'use strict';

angular.module('act')
  .controller('AwCreativeCtrl', function ($scope, AwCreative, ngDialog, regex, urlValidator, $filter, fsConfirm, microService, modelOptions, library) {

    $scope.shortUrlRegex = regex.DIEGOSHORTURL.source; // HTTP not required
    $scope.fullUrlRegex = regex.DIEGOFULLURL.source; // HTTP required

    var goalUrl = angular.copy($scope.campaign.settings.goal.eventUrl ? $scope.campaign.settings.goal.eventUrl : $scope.campaign.settings.goal.host);

    // Open AwCreative Modal to add creatives
    $scope.addCreative = function (combo) {
      $scope.urlModelOptions = modelOptions.newInstance().creativeUrl;
      // Declare the new creative object that will be used in the creative modal view
      $scope.newCreative = new AwCreative();
      $scope.currentCreative = null;
      $scope.metadata.currentCombo = combo;

      ngDialog.open({
        template: 'app/main/act/campaignCreation/adwords/awCreative/awCreativeModal.html',
        className: 'modal',
        scope: $scope
      });
    };

    // Open AwCreative Modal to add creatives
    $scope.editCreative = function (combo, creative) {
      $scope.urlModelOptions = modelOptions.newInstance().creativeUrl;
      $scope.metadata.currentCombo = combo;
      $scope.currentCreative = creative;
      $scope.newCreative = angular.copy(creative);
      // Added for legacy creative, if a customer has a draft that was created before the implementation of the url validation
      $scope.validateDestinationUrl();

      ngDialog.open({
        template: 'app/main/act/campaignCreation/adwords/awCreative/awCreativeModal.html',
        className: 'modal',
        scope: $scope
      });
    };

    $scope.saveCreative = function (creativeForm, saveBox) {
      // Validate the form
      creativeForm.submitted = true;
      // Check that all the fields in the form are valid
      $scope.newCreative.isValid = !creativeForm.$invalid;
      // Check that the destination URL is valid
      if ($scope.newCreative.isValid) {
        $scope.newCreative.isValid = !$scope.validateDestinationUrl() && !$scope.validateDisplayUrl();
      }
      // Check that there is a tracking script on the landing page
      // If our webservice is unavailable, mark it as unknown and allow the user to proceed
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {
        $scope.newCreative.isValid = $scope.newCreative.isValid && ($scope.newCreative.trackingScriptStateInUrl === 'pass' || $scope.newCreative.trackingScriptStateInUrl === 'unknown');
      }

      if(saveBox){
        $scope.newCreative.saveToLibrary($scope.campaign.selectedChannel.toUpperCase());
      }

      if ($scope.newCreative.isValid) {
        // Save the creative in the backend then add it to the combo / update the creative
        saveCreative();
        creativeForm.submitted = false;
        if(!saveBox){
          ngDialog.close();        
        }
        // Close the modal
      } else { // If the creative is not valid, ask the user if he's sure about saving it
        fsConfirm('generic', {
          body: $filter('translate')('label.act.creativeInvalidMessage')
        }).then(function () {
          saveCreative();
          creativeForm.submitted = false;
          if(!saveBox){
            ngDialog.close();        
          }
        });
      }
     

    };

    function saveCreative() {
      if ($scope.currentCreative) { // If we are editing a current creative, assign the content of newCreative to currentCreative
        _.assign($scope.currentCreative, $scope.newCreative);
        $scope.metadata.currentCombo.saveCreative($scope.currentCreative);
      } else {
        $scope.metadata.currentCombo.saveCreative($scope.newCreative);
      }
    }

    $scope.setForm = function (creativeForm, isCreativeValid) {
      //$scope.form = form;
      creativeForm.submitted = !isCreativeValid;
    };

    // Make sure the display URL doesn't not contain http or https
    $scope.checkDisplayUrlFormat = function() {
      if ($scope.newCreative.creative.displayUrl) {
        $scope.newCreative.isDisplayUrlWronglyFormatted = ( _.contains($scope.newCreative.creative.displayUrl, 'http://') || _.contains($scope.newCreative.creative.displayUrl, 'https://') );

        var shortUrlRegex = new RegExp($scope.shortUrlRegex);
        $scope.newCreative.isDisplayUrlInvalid = !shortUrlRegex.test($scope.newCreative.creative.displayUrl);
      } else {
        $scope.newCreative.isDisplayUrlWronglyFormatted = false;
      }
      return ($scope.newCreative.isDisplayUrlWronglyFormatted && $scope.newCreative.isDisplayUrlInvalid);
    };


    // Makes sur the URL typed by the user has the same domain as the URL registered in the goal
    $scope.validateDisplayUrl = function () {

      $scope.newCreative.isDisplayDomainTypedDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain($scope.newCreative.creative.displayUrl, goalUrl);
      // They all need to be valid. Return true if one of them is not valid
      return $scope.newCreative.isDisplayDomainTypedDifferentFromGoalDomain;
    };

    // Makes sur the URL typed by the user has the same domain as the URL registered in the goal
    $scope.validateDestinationUrl = function () {

      // Add protocol if not present already
      if (!_.contains($scope.newCreative.creative.destinationUrl, 'http')) {
        $scope.newCreative.creative.destinationUrl = 'http://' + $scope.newCreative.creative.destinationUrl;
      }

      $scope.newCreative.isDestinationDomainTypedDifferentFromGoalDomain = urlValidator.checkUrlDomainMatchesGoalDomain($scope.newCreative.creative.destinationUrl, goalUrl);
      // They all need to be valid. Return true if one of them is not valid
      return $scope.newCreative.isDestinationDomainTypedDifferentFromGoalDomain;
    };

    $scope.loadCreativeFromLibrary = function () {
      // Open Library - Load Image modal view
      var opts = {
        currentComboId: $scope.metadata.currentCombo.id,
        channel: $scope.campaign.selectedChannel
      };
      library.openLibrary('Creatives', opts).then(function(content){

        $scope.newCreative.creative = content.creative;
        // $scope.saveCreative($scope.newCreative.creative);
      });
    };

    $scope.checkIfUrlContainsTrackingScript = function () {

      // Only if we are using Accomplice tracking script
      if ($scope.campaign.settings.goal.tracker === 'ACCOMPLICE' || $scope.campaign.settings.goal.tracker === 'ACCOMPLICE_CONVERSION_PIXEL') {

        $scope.newCreative.trackingScriptStateInUrl = 'processing';

        var url = $scope.newCreative.creative.destinationUrl;

        microService.urlValidation(
          {
            host: 'na',
            testUrl: url,
            index: 100,
            nowait: true,
            nocache: true,
            badurlonly: false
          },
          function (response) {

            // If the webservice is not available
            if (response && response.status === 'ERROR') {
              // If the webservice is down, we define a new state (unknwown) that will mark the creative as valid
              $scope.newCreative.trackingScriptStateInUrl = 'unknown';

            } else {
              var url = response.api || {};
              $scope.newCreative.trackingScriptStateInUrl = url.found ? 'pass' : 'fail';
            }
          },
          function (error) {
            console.log('validate tracking script in url server ERROR: ', error);
            $scope.newCreative.trackingScriptStateInUrl = 'unknown';
          });
      }
    };

    // This watcher allows us to know how to format the description.
    $scope.$watch('newCreative.creative.descriptionLine1', function (newValue) {
      $scope.displayDescriptionNextToHeadline = (newValue !== null && newValue !== undefined && (newValue.slice(-1) === '!' || newValue.slice(-1) === '.' || newValue.slice(-1) === '?'));
    });

  });
