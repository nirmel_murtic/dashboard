'use strict';

describe('Service: AwCombo', function () {

  // load the service's module
  beforeEach(module('dashboardApp'));

  // instantiate service
  var AwCombo;
  beforeEach(inject(function (_AwCombo_) {
    AwCombo = _AwCombo_;
  }));

  it('should do something', function () {
    expect(!!AwCombo).toBe(true);
  });

});
