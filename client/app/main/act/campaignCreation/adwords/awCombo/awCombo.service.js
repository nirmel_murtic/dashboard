'use strict';

angular.module('act')
  .service('AwCombo', function ($http, apiUrl, $rootScope, fsUtils, AwAudience, AwCreative, AwKeyword, Combo, fsNotify, $filter) {
    // AngularJS will instantiate a singleton by calling "new" on this function

        var AwCombo;

        AwCombo = (function() {
            function AwCombo() {

                // Call the super constructor Combo
                this.constructor.super_.call(this);
//                if(comboId) {
//                    this.load(comboId);
//                    return;
//                }

                this.name = 'AW Combo '+(counter++);
                this.channel = 'ADWORDS';

                this.keyword = {
                    id: null,
                    keyword: []
                };
                // creative and audience counts are defined in the parent Combo class
                this.numberOfKeywordVariants = 0;

            }

            // Implement inheritance from Combo abstract class
            fsUtils.inherits(AwCombo, Combo);

            return AwCombo;
        })();

        // Counter to generate combo default names
        var counter = 1;

        // Save and Update methods have been moved to Combo.service.js

        // Check if the combo has audience (at least location) + at least one creative + at lease one keyword
        AwCombo.prototype.checkValidity = function() {
            var isComboValid = true;
            this.checkForInvalidCreatives();

            if (! this.audience || ! this.audience.audience || ! this.audience.audience.location || this.audience.audience.location.length === 0 ||
              this.creativeList.length === 0 || this.invalidCreativeIndexList.length > 0 || this.keyword.keyword.length === 0) {
                isComboValid = false;
            }

            this.isValid = isComboValid;
            return isComboValid;
        };

        AwCombo.prototype.saveAudience = function(newAudience) {
            if(newAudience.id) {
                updateAudience(this, newAudience);
            } else {
                createAudience(this, newAudience);
            }
        };

        AwCombo.prototype.saveExtension = function(newExtension) {
            if(newExtension.id) {
                updateExtension(this, newExtension);
            } else {
                createExtension(this, newExtension);
            }
        };

        AwCombo.prototype.saveCreative = function(newCreative) {
            if(newCreative.id) {
                return updateCreative(this, newCreative);
            } else {
                return createCreative(this, newCreative);
            }
        };

        AwCombo.prototype.saveKeyword = function(keyword) {
            if(this.keyword.id) {
                return updateKeyword(this, keyword);
            } else {
                return createKeyword(this, keyword);
            }
        };

        // Load from Library and return a combo object
        AwCombo.load = function(comboData, superCmpId, parentCmpId) {
            var combo = new AwCombo() ;
            var unsortedCreativesList = [];

            // For each creative and audience, create the relevant objects
            for (var property in comboData) {
                if (comboData.hasOwnProperty(property)) {
                    if (property === 'audience' && comboData[property] !== '') { // If it's an audience, associate to the audience property of the combo object
                        combo.audience = new AwAudience(angular.fromJson(comboData[property].audience));
                        combo.audience.id = comboData[property].id;

                    } else if (property === 'creativeList' && comboData[property] !== null) { // If it's a creative, create creative objects
                        for (var i = 0, j = comboData[property].length; i < j; i++) {
                            var creative = new AwCreative(angular.fromJson(comboData[property][i].creative));
                            creative.id = comboData[property][i].id;
                            unsortedCreativesList.push(creative);
                        }
                    } else if (property === 'keyword' && comboData[property] !== null) { // If it's a keyword, create keyword objects
                        combo.keyword = new AwKeyword(angular.fromJson(comboData[property].keyword));
                        combo.keyword.id = comboData[property].id;

                    } else { // Other properties like comboId or channel
                        combo[property] = comboData[property];
                    }
                }
            }

            // When loading from library, use ids passed as a parameter
            combo.superCmpId = superCmpId || combo.superCmpId;
            combo.parentCmpId = parentCmpId || combo.parentCmpId;

            // Sort creatives order: First created is first displayed
            combo.creativeList = _.sortBy(unsortedCreativesList, function (creativeInList){
                return creativeInList.id;
            });

            // Generate number of audience, creative, keyword variants for each combo
            combo.calculateNumberOfCreativeVariants();
            combo.calculateNumberOfAudienceVariants();
            combo.calculateNumberOfKeywordVariants();

            // Return combo
            return combo;
        };

        AwCombo.prototype.calculateNumberOfAudienceVariants = function() {
            var numberOfVariants = 0;

            if (this.audience) {
                numberOfVariants = 1;

                for (var property in this.audience.audience) {
                    if (this.audience.audience.hasOwnProperty(property) && this.audience.audience[property] !== undefined && this.audience.audience[property].length !== 0) {
                        numberOfVariants *= this.audience.audience[property].length;
                    }
                }
            }
            // Update the count of audience variants
            this.numberOfAudienceVariants = numberOfVariants;
        };

        AwCombo.prototype.calculateNumberOfKeywordVariants = function() {
            this.numberOfKeywordVariants = this.keyword.keyword.length;
        };


        // Internal method to create an audience
        function createAudience (self, newAudience) {
            var params = {
                typeReq: 'AUDIENCE_REQ',
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                data: angular.toJson(newAudience)
            };

            $http({
                url: apiUrl + '/api/v2/campaign/create/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function(response) {
                    // Assign id returned by backend
                    newAudience.id = response.data.cmpAdSettingId;
                    // Assign audience to the newly created audience
                    self.audience = newAudience;
                    self.calculateNumberOfAudienceVariants();
                    self.checkValidity();

                    fsNotify.push(
                            $filter('translate')('label.act.Audienceaddedtocombo')+' '+self.name
                    );
                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }
        // Internal method to update an audience
        function updateAudience (self, newAudience) {
            var params = {
                typeReq: 'AUDIENCE_REQ',
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                cmpAdSettingId: newAudience.id,
                data: angular.toJson(newAudience)
            };

            $http({
                url: apiUrl + '/api/v2/campaign/update/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function() {
                    // Assign audience to the newly created audience
                    self.audience = newAudience;
                    self.calculateNumberOfAudienceVariants();
                    self.checkValidity();

                    fsNotify.push(
                            $filter('translate')('label.act.Audienceupdatedincombo')+' '+self.name
                    );
                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }

        // Internal method to create a creative
        function createExtension(self, newExtension) {
            var params = {
                typeReq: 'EXTENSION_REQ',
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                data: angular.toJson(newExtension)
            };

            $http({
                url: apiUrl + '/api/v2/campaign/create/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function(response) {
                    // Assign returned id to the Extension
                    newExtension.id = response.data.cmpAdSettingId;
                    self.extension = newExtension;

                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }

        // Internal method to update a Extension
        function updateExtension(self, newExtension) {
            var params = {
                typeReq: 'Extension_REQ',
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                cmpAdSettingId: newExtension.id,
                data: angular.toJson(newExtension)
            };

            $http({
                url: apiUrl + '/api/v2/campaign/update/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function() {
                    self.extension = newExtension;
                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }



        // Internal method to create a creative
        function createCreative(self, newCreative) {
            var params = {
                typeReq: 'CREATIVE_REQ',
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                data: angular.toJson(newCreative)
            };

            return $http({
                url: apiUrl + '/api/v2/campaign/create/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function(response) {
                    // Assign returned id to the creative
                    newCreative.id = response.data.cmpAdSettingId;
                    self.creativeList.push(newCreative);
                    self.calculateNumberOfCreativeVariants();
                    self.checkForInvalidCreatives();
                    self.checkValidity();

                    fsNotify.push(
                            $filter('translate')('label.act.Creativeaddedtocombo')+' '+self.name
                    );
                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }

        // Internal method to update a creative
        function updateCreative(self, newCreative) {
            var params = {
                typeReq: 'CREATIVE_REQ',
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                cmpAdSettingId: newCreative.id,
                data: angular.toJson(newCreative)
            };

            return $http({
                url: apiUrl + '/api/v2/campaign/update/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function() {
                    self.calculateNumberOfCreativeVariants();
                    self.checkForInvalidCreatives();
                    self.checkValidity();

                    fsNotify.push(
                            $filter('translate')('label.act.Creativeupdatedincombo')+' '+self.name
                    );
                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }

        function createKeyword(self, keyword){
            keyword.id = 0;
            var params = {
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                data: angular.toJson(keyword)
            };

            return $http({
                url: apiUrl + '/api/v2/campaign/create/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function(response) {
                    // Assign audience to the newly created audience
                    self.keyword = keyword;
                    keyword.id = response.data.cmpAdSettingId;
                    self.keyword.id = keyword.id;
                    self.calculateNumberOfKeywordVariants();
                    self.checkValidity();

                    fsNotify.push(
                            $filter('translate')('label.act.Keywordaddedtocombo')+' '+self.name
                    );

                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }

        function updateKeyword(self, keyword){
            var params = {
                superCmpId: self.superCmpId,
                parentCmpId: self.parentCmpId,
                adComboId: self.id,
                cmpAdSettingId: self.keyword.id,
                data: angular.toJson(keyword)
            };
            return $http({
                url: apiUrl + '/api/v2/campaign/update/adSetting/'+$rootScope.fractalAccountInfo.currentTeam.id,
                method: 'POST',
                data: $.param(params)
            }).
                success(function(response) {
                    // Assign audience to the newly created audience
                    keyword.id = response.data.cmpAdSettingId;
                    self.keyword = keyword;
                    self.calculateNumberOfKeywordVariants();
                    self.checkValidity();

                    fsNotify.push(
                            $filter('translate')('label.act.Keywordupdatedincombo')+' '+self.name
                    );
                }).
                error(
                function(response) { // optional
                    console.log(response);
                });
        }

        return AwCombo;
    });
