'use strict';

angular.module('act')
    .service('AwExtension', function () {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var AwExtension;

        AwExtension = (function () {
            function AwExtension(extension) {

                if (extension) {
                    for (var property in extension) {
                        if (extension.hasOwnProperty(property)) {
                            this[property] = extension[property];
                        }
                    }
                    return;
                }
                // All info related to the extension stored here.
                this.id = null;
                this.extension = {};
            }

            return AwExtension;
        })();

        AwExtension.prototype.addExtension = function(extensionType, value){
            if(!this.extension[extensionType]){
                this.extension[extensionType] = [];
            }
            this.extension[extensionType].push(value);
        };

        AwExtension.prototype.removeExtension = function(extensionType, index){
            this.extension[extensionType].splice(index, 1);
        };


        return AwExtension;

    });