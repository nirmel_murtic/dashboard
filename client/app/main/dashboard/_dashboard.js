'use strict';

angular.module('dashboard', ['main'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.dashboard', {
      	permissions: '<LOGGED_IN>',
        url: '/dashboard',
        views: {
		        '@main': {
		            templateUrl: 'app/main/dashboard/dashboard.html',
        			  controller: 'DashboardCtrl'
		        }
		    },
        data: {
          active: 'dashboard'
        }
      });
  });
