'use strict';

angular.module('analyze')
  .factory('ProjectReportWebApiUtils', function($http, $q, apiUrl) {
    // jshint unused:false

    function dateParam(date) {
      if(_.isDate(date)) {
        return d3.time.format('%Y-%m-%d')(date);
      } 
      return date.toString();
    }


    var errorDataEmpty = {
      error : {
        message: 'There\'s no activity for the project and date range selected.'
      } 
    };

    function checkProjectStats(projectId, from, to, interval) {
      var url = apiUrl + '/api/v2/projectReport/get/activities/' + projectId;
      var params = {
        startTime : from.getTime(),
        endTime : to.getTime(),
        interval: interval || 'day'
      };

      return $http.get(url, {
        responseType : 'json',
        params : params
      }).then(function(res) {
        if(res.data.data.isData) {
          return true;
        } else {
          return $q.reject(errorDataEmpty);
        } 
      }, function() {
        return true;
        // return $q.reject(errorDataEmpty);
      });
    }
 
    function getProjectStats(projectId, goalId, from, to, inverval) {
      // var url = '/custom/stats/';
      var url;
      if(goalId === null) {
        url = apiUrl + '/api/v2/projectReport/get/' + projectId;
      } else {
        url = apiUrl + '/api/v2/projectReport/goalReport/' + projectId;
      }
      var params = {
        startTime : from.getTime(),
        endTime : to.getTime(),
        interval: 'day'
      };
      if(goalId !== null) {
        params.projectGoalId = goalId;
      }
      return $http.get(url, {
        responseType : 'json',
        params : params
      }).then(function(res) {
        var data = res.data.data.data;
        return data;
      }, function(err) {
        var unknownError = {
          message : 'Data missing or currently unavailable.'
        };
        try {
          if(err.data.error.message === 'At least one campaignId/campaign hashId has to be provided') {
            err.data.error.message = 'There\'s no activity for the project and date range selected.';
          }
          return {
            error : err.data.error || unknownError
          };
        } catch (e) {
          return {
            error : unknownError
          };
        }
      });
    }

    // Public Apis
    return {
      getProjectStats : getProjectStats, 
      checkProjectStats : checkProjectStats 
    };

  });
