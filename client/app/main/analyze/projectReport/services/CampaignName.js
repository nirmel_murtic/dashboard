'use strict';

angular.module('analyze')
  .factory('CampaignNameCell', function(DGColumnBorder, $state) {
    /* jshint quotmark:double */
    var DataCell = React.createClass({displayName: "DataCell",
      redirect : function() {
        var id = this.props.rowData.id;
        if(this.props.rowData.channel === "campaign" && id) {
          $state.go("main.act.campaignManager.home.single", {
            filter : "active",
            superId : id
          }).then(null, function() {
            $state.go("main.act.campaignManager.home.single", {
              filter : "past",
              superId : id
            });
          });
        } else if(this.props.rowData.channel === "link" && id) {
          $state.go("main.act.linksManager.linkOverview", {
            linkId : id
          }); 
        }
      },
      render : function() {
        return (
          React.createElement("div", {className:  "dg-"+this.props.column + " dg-table-cell"}, 
            React.createElement(DGColumnBorder, null), 
            React.createElement("span", {onClick:  this.redirect, className: "pointer"},  this.props.value)
          )
        ); 
      }
    }); 

    return DataCell;
  });
