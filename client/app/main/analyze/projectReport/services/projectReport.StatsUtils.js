'use strict';

angular.module('analyze')
  .service('ProjectReportStatsUtils', function() {

    // var channels = ['facebook', 'adwords'];

    function mapGoalConvs(raw) {
      var series = _.map(raw, function(value, timestamp) {
        return {
          conv : value,
          time : new Date( +timestamp )
        };
      });
      return _.sortBy(series, 'time');
    }

    // kip -> channelBreakdown -> [ { time: <time>, FACEBOOK: <kpi value>, ADWORDS: <kpi value> }]
    function mapChannelKpi(kpi, raw, channels) {
      var series = _.map(raw, function(datum, timestamp) {
        var kpiDatum = _.mapValues(datum, function(channelDatum) {
          return channelDatum[kpi];
        });
        if(channels && channels.length) {
          kpiDatum = _.reduce(channels, function(d, c) {
            d[c] = d[c] || 0;
            return d;
          }, kpiDatum);
        }
        kpiDatum.time = new Date( +timestamp );
        return kpiDatum;
      }); 
      return _.sortBy(series, 'time');
    } 
    // kpi -> channelAggregate -> Number / <kpi value>
    function aggregateKpi(kpi, raw, useNa) {
      return _.reduce(raw, function(acc, chan, chanKey) {
        if(chanKey.toLowerCase() === 'all') {
          return acc;
        }
        var kpiVal = useNa && (!_.isNumber(chan[kpi]) || isNaN(chan[kpi])) ? NaN : (chan[kpi] || 0);
        return acc + kpiVal;
      }, 0);
    }
    function aggregateKpiDerived(kpiDef, raw) {
      var aggr = _.mapValues(kpiDef.derivedFrom, function(__, kpiKey) {
        return aggregateKpi(kpiKey, raw, false);
      });
      return kpiDef.formula(aggr);
    }
    // by channel data -> flattend array of activity table data
    function flattenActivities(raw) {
      var activities = _.map(raw, function(channelActivities, channel) {
        return _.map(channelActivities, function(activity, actId) {
          activity.id = Number(actId);
          activity.channel = channel;
          return activity;
        });
      });
      return _.flatten(activities);
    }

    return {
      mapGoalConvs : mapGoalConvs,
      mapChannelKpi : mapChannelKpi,
      aggregateKpi : aggregateKpi,
      aggregateKpiDerived : aggregateKpiDerived,
      flattenActivities : flattenActivities
    };
  
  });
