'use strict';

angular.module('analyze')
  .constant('ProjectReportDateRanges', function() { 
    var now = d3.time.day.floor(new Date());
    var nowPrecise = new Date();
    return {
      // 'Now' : [d3.time.day.floor(new Date(Date.now() - 86400000)), new Date()],   
      'Last 7 Days' : [d3.time.day.offset(now, -7), nowPrecise],   
      'Last 30 Days' : [d3.time.day.offset(now, -30), nowPrecise],   
    };
  });
