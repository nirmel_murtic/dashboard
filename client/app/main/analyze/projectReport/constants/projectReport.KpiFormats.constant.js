'use strict';

angular.module('analyze')
  .constant('KpiFormats', {
    impressions : 'metricVolumeCompact',
    clicks : 'metricVolumeCompact',
    conversions : 'metricVolumeCompact',
    spent : 'metricCurrency'
  });
