'use strict';

angular.module('analyze')
  .factory('ProjectKpis', function($filter) {
    return {
      impressions : {
        label : 'Impressions',
        displayOrder : 1, 
        format : 'metricVolumeCompact',
        type : 'volume',
        category: 'engagement'
      },
      clicks : {
        label : 'Clicks',
        displayOrder : 2, 
        format : 'metricVolumeCompact',
        category: 'engagement',
        type : 'volume'
      },
      conversions : {
        label : 'Conversions',
        displayOrder : 0, 
        format : 'metricVolumeCompact',
        category: 'engagement',
        type : 'volume'
      },
      conversionRate : {
        label : 'Conversion Rate',
        displayOrder : 3,
        format : 'metricPercentage',
        category : 'engagement',
        type : 'derived',
        derivedFrom : {
          conversions : true,
          clicks : true
        },
        formula : function(datum) {
          return datum.conversions / datum.clicks;
        }
      },
      ctr : {
        label : 'CTR',
        tooltip : $filter('translate')('label.kpi.clickthroughrate'),
        displayOrder : 3.5,
        format : 'metricPercentage',
        category : 'engagement',
        type : 'derived',
        derivedFrom : {
          impressions : true,
          clicks : true
        },
        formula : function(datum) {
          return datum.clicks / datum.impressions;
        }
      },
      spent : {
        label : 'Spend',
        format : 'metricCurrency',
        displayOrder : 10,
        category: 'cost',
        type : 'volume'
      },
      cpc : {
        label : 'CPC',
        tooltip : $filter('translate')('label.kpi.costperclick'),
        format : 'metricCurrency',
        displayOrder : 11,
        category: 'cost',
        type : 'derived',
        derivedFrom : {
          spent : true,
          clicks : true
        },
        formula : function(datum) {
          return datum.spent / datum.clicks;
        }
      },
      cpm : {
        label : 'CPM',
        tooltip : $filter('translate')('label.kpi.costper1000impressions'),
        format : 'metricCurrency',
        category: 'cost',
        displayOrder : 12,
        type : 'derived',
        derivedFrom : {
          spent : true,
          impressions : true
        },
        formula : function(datum) {
          return datum.spent / datum.impressions * 1000;
        }
      },
      cpa : {
        label : 'CPA',
        tooltip : $filter('translate')('label.kpi.costperaction'),
        category: 'cost',
        format : 'metricCurrency',
        displayOrder : 13,
        type : 'derived',
        derivedFrom : {
          spent : true,
          conversions : true
        },
        formula : function(datum) {
          return datum.spent / datum.conversions;
        }
      },
      pageViewsPerVisitCount : {
        label : 'Page View per Visit',
        displayOrder : 5, 
        format : 'metricVolume',
        category : 'engagement',
        type : 'avg'
      },
      dailyAvgReturningVisitorRate: {
        label : 'Avg. Return Rate',
        displayOrder : 7, 
        format : 'metricPercentage',
        category : 'engagement',
        type : 'avg'
      },
      newVisitorCount: {
        label : 'New Visitors',
        displayOrder : 4, 
        format : 'metricVolumeCompact',
        category : 'engagement',
        type : 'volume'
      },
      avgVisitTime : {
        label : 'Avg. Visit Time',
        displayOrder : 6, 
        format : 'metricDuration',
        type : 'avg',
        category : 'engagement'
      }
    };
  });
