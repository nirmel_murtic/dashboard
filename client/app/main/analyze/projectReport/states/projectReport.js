'use strict';

angular.module('analyze')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.analyze.projects', {
        abstract : true,
        url : '/project',
        resolve : {
          timeFrames : function(ProjectReportDateRanges) {
            // jshint newcap : false
            return ProjectReportDateRanges();
          }
        }
      })
      .state('main.analyze.projects.all', {
        url: '/:dateRangePreset',
        data : {
          displayName : false,
          active: 'project'
        },
        params : {
          // timeFrame is an array of two dates
          timeFrame : null
        },
        views: {
          '@main': {
            templateUrl: 'app/main/analyze/projectReport/templates/projectReport.html',
            controller: 'ProjectReportCtrl'
          }
        },
        resolve : {
          timeFrame : function(timeFrames, $stateParams, $state) {
            if($stateParams.timeFrame && $stateParams.dateRangePreset === 'custom-daterange') {
              return $stateParams.timeFrame;
            } else if($stateParams.dateRangePreset === 'now') {
              return timeFrames.Now;
            } else if ($stateParams.dateRangePreset === 'last7'){
              return timeFrames['Last 7 Days'];
            } else if ($stateParams.dateRangePreset === 'last30'){
              return timeFrames['Last 30 Days'];
            } else {
              // if no date range is set, choose now
              $stateParams.dateRangePreset = 'last7';
              $state.go(this.name, $stateParams);
              return timeFrames.Now;
            }
          },
          projects : function(auth, projectService, $q) {
            var teamId = auth.user.currentTeam.id;
            if(auth.user.currentTeam.projects) {
              return _(auth.user.currentTeam.projects)
                .map(function(p) {
                  p.createdAt = new Date(p.createdAt);
                  return p;
                })
                .sortBy(function(d) {
                  return -d.createdAt;
                })
                .value();
            }
            var deferred = $q.defer();
            projectService
              .getProjects(teamId, function(res) {
                var projects = res.data.projects;

                auth.user.currentTeam.projects = projects;

                projects = _(projects)
                  .map(function(p) {
                    p.createdAt = new Date(p.createdAt);
                    return p;
                  })
                  .sortBy(function(d) {
                    return -d.createdAt;
                  })
                  .value();
                if(__DEV__) {
                  console.log('Projects at Project Report, for team ' + teamId + ' ', projects);
                }
                deferred.resolve(projects);
              }, function() {
                deferred.reject({
                  errorState : 'main.analyze.error',
                  errorParams : {
                    msg : 'Error loading your team\'s projects from server.'
                  }
                });
              });
            return deferred.promise;
          }
        }
      });
  });
