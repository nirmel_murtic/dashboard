'use strict';

angular.module('analyze')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.analyze.projects.all.oneProject', {
        url: '/:projectId',
        views: {
          '@main.analyze.projects.all': {
            templateUrl: 'app/main/analyze/projectReport/templates/oneProject.html',
            controller: 'OneProjectReportCtrl'
          }
        },
        resolve : {
          project : function(auth, $stateParams, projects, $state) {
            auth.user.currentTeam.projects = projects;

            var project = _.find(projects, { id: +$stateParams.projectId });
            if(!project) {
              $state.go('main.analyze.projects.all');
            }
            return project;
          },
          pageStats : function($stateParams, ProjectReportWebApiUtils, timeFrame) {
            console.log('eh?');
            return ProjectReportWebApiUtils.checkProjectStats($stateParams.projectId, timeFrame[0], timeFrame[1])
              .then(function() {
                return ProjectReportWebApiUtils.getProjectStats($stateParams.projectId, null, timeFrame[0], timeFrame[1]);
              }, function(err) {
                return err;
              });
          },
          goal : function() {
            return null;
          }
        },
        data: {
          displayName: 'Project Report'
        }
      })
      .state('main.analyze.projects.all.oneProject.oneGoal', {
        url: '/:goalId',
        views: {
          '@main.analyze.projects.all': {
            templateUrl: 'app/main/analyze/projectReport/templates/oneProject.html',
            controller: 'OneProjectReportCtrl'
          }
        },
        resolve : {
          goal : function(project, $stateParams, $state) {
            var goal = _.find(project.goals, function(g) {
              return g.id === Number($stateParams.goalId);
            });
            if(!goal) {
              $state.go('main.analyze.projects.all.oneProject', $stateParams);
            }
            return goal;
          },
          pageStats : function($stateParams, ProjectReportWebApiUtils, timeFrame, pageStats) {
            return ProjectReportWebApiUtils.getProjectStats($stateParams.projectId, $stateParams.goalId, timeFrame[0], timeFrame[1])
              .then(function(goalPageStats) {
                goalPageStats.conversions = pageStats.conversions;
                return goalPageStats;
              });
          }
        },
        data: {
          displayName: 'Goal Report'
        }
      });
  });
