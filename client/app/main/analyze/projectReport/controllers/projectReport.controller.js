'use strict';

angular.module('analyze')
  .controller('ProjectReportCtrl', function ($scope, $rootScope, $state, ProjectReportDateRanges, projects, timeFrames, timeFrame) {
    $scope.projects = projects;

    var currentProject = $rootScope.fractalAccountInfo.currentTeam.currentProject ? _.find($scope.projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProject.id}) : null;
    if(!currentProject && projects.length) {
      currentProject = projects[0];
    }

    $scope.activeEntities = {
      project : null,
      goal : null,
      timeFrame : timeFrame
    };

    $scope.updateProject = function(project) {
      if(project) {
        $rootScope.fractalAccountInfo.currentTeam.currentProject = project;

        $state.go('main.analyze.projects.all.oneProject', { projectId : project.id });
      }
    };

    $scope.$watch('fractalAccountInfo.currentTeam.currentProject', function (newValue) {
      if(newValue) {
        $scope.activeEntities.project = _.find($scope.projects, {'id': newValue.id});
        $scope.updateProject($scope.activeEntities.project);
      }
    });

    $scope.updateTimeframe = function(timeFrame) {
      var params = {
        projectId : $scope.activeEntities.project.id
      };
      if(_.isEqual(timeFrame, timeFrames.Now)) {
        params.dateRangePreset = 'now';
        params.timeFrame = null;
      } else if (_.isEqual(timeFrame, timeFrames['Last 7 Days'])) {
        params.dateRangePreset = 'last7';
      } else if (_.isEqual(timeFrame, timeFrames['Last 30 Days'])) {
        params.dateRangePreset = 'last30';
      } else {
        params.dateRangePreset = 'custom-daterange';
        params.timeFrame = timeFrame;
      }
      // actual current state, can be at project level or goal level
      var toState = $scope.activeEntities.goal ? 'main.analyze.projects.all.oneProject.oneGoal' : 'main.analyze.projects.all.oneProject'; 
      if($scope.activeEntities.goal) {
        params.goalId = $scope.activeEntities.goal.id;
      }
      $state.go(toState, params, {
        location : 'replace'
      });
      /*
      .then(function() {
        console.log('done');
      }, function(err) {
        console.log('failed', err);
      });
      */
    };

    $scope.daterangePickerOpts = {
      ranges : timeFrames,
      maxDate : new Date(d3.time.day.ceil(new Date()) - 1),
      timePicker : false
    };

    var cleanup = $rootScope.$on('$stateChangeSuccess', function(fromState, toState) {
      if(_.startsWith(toState.name, 'main.analyze.projects.all')) {
        if($scope.activeEntities.project && toState.name === 'main.analyze.projects.all') {
          $scope.updateProject($scope.activeEntities.project);
        } 
      } else {
        cleanup();
      }
    });

    $scope.initProject = function() {
      if(currentProject && !$scope.activeEntities.project) {
        $scope.updateProject(currentProject);
      }
    };

  });


