'use strict';

angular.module('analyze')
  .controller('OneProjectReportCtrl', function ($scope, $state, $stateParams, $filter,
               CampaignNameCell, 
               goal, project, projects, timeFrame,
               ProjectKpis,
               pageStats,
               fsUtils, ProjectReportStatsUtils) {
    // jshint bitwise : false
    console.log('ctrl:', pageStats);

    $scope.transitioning = function() {
      return !!$state.transition;
    };
    $scope.activeEntities.transitionToGoal = -1;

    $scope.activeEntities.project = project;
    $scope.fractalAccountInfo.currentTeam.currentProject = project;

    $scope.noData = [];

    $scope.showGoals = false;

    $scope.toggleAllGoals = function() {
      if(goal) {
        return true;
      }
      $scope.showGoals = !$scope.showGoals;
    };

    $scope.activeEntities.timeFrame = timeFrame;
    if(!pageStats.error) {
      $scope.goalConvsAggregate = _.mapValues(pageStats.conversions, function(convs) {
        convs = ProjectReportStatsUtils.mapGoalConvs(convs); 
        if(convs && convs.length) {
          return _.reduce(convs, function(acc, conv) {
            return acc + (conv.conv || 0);
          }, 0);
        }
        return 0;
      });
      $scope.convsTotal = _.reduce($scope.goalConvsAggregate, function(val, d) {
        return val + d;
      }, 0);
    } else {
      $scope.goalConvsAggregate = {};
      $scope.convsTotal = 0;
    }
    if(goal) {
      $scope.activeEntities.goal = goal;
      $scope.goalConvs = pageStats.error ? $scope.noData : ProjectReportStatsUtils.mapGoalConvs(pageStats.conversions[goal.id]);
    } else {
      $scope.activeEntities.goal = null;
      try {
        $scope.activeEntities.goalHasReport = _.reduce(pageStats.activityBreakdown.campaign || {}, function(_map, activity) {
          if(activity.projectGoalId) {
            _map[activity.projectGoalId] = true;
          }
          return _map;
        }, {});
      } catch(err) {
        $scope.activeEntities.goalHasReport = {};
      }
    }
   
    $scope.focusGoal = function(goalId) {
      goalId = Number(goalId);
      var params = _.assign({}, $stateParams);
      if(goal && goal.id === goalId) {
        delete params.goalId;
        $state.go('main.analyze.projects.all.oneProject', params);
        $scope.activeEntities.transitionToGoal = -1;
        return;
      }
      if(!$scope.activeEntities.goalHasReport[goalId]) {
        $scope.activeEntities.transitionToGoal = -1;
        return;
      }
      params.goalId = goalId;
      $scope.activeEntities.transitionToGoal = goalId;
      $state.go('main.analyze.projects.all.oneProject.oneGoal', params);
    };

    if(pageStats.error) {
      $scope.error = pageStats.error; 
      return;
    } else {
      $scope.error = null;
    }

    var orderedChannels = {
      'facebook' : 0,
      'twitter' : 1,
      'adwords' : 2
    };

    $scope.participatingChannels = _.keys(pageStats.channelAggregate).filter(function(x) {
      return x.toLowerCase() !== 'all';
    });
    $scope.participatingChannels = _.sortBy($scope.participatingChannels, function(channel){
      return orderedChannels[channel.toLowerCase()];
    });
    $scope.availableKpis = _.map(ProjectKpis, function(kpiDef, kpiKey) {
      return _.assign({ key: kpiKey }, kpiDef);
    });
    $scope.availableKpis = _.sortBy($scope.availableKpis, function(kpi) {
      return kpi.displayOrder;
    });
    $scope.activeKpi = 'conversions';
    $scope.selectKpi = function(kpiKey) {
      $scope.activeKpi = kpiKey;
    };
    $scope.kpiToDate = _.reduce($scope.availableKpis, function(data, kpi) {
      if(kpi.type === 'derived') {
        data[kpi.key] = ProjectReportStatsUtils.aggregateKpiDerived(kpi, pageStats.channelAggregate);
      } else {
        data[kpi.key] = pageStats.channelAggregate.all[kpi.key];
      }
      // } else {
      //  data[kpi.key] = ProjectReportStatsUtils.aggregateKpi(kpi.key, pageStats.channelAggregate, ProjectKpis[kpi.key].type !== 'volume');
      // }
      return data;
    }, {});
    $scope.kpiOvertime = _.reduce($scope.availableKpis, function(data, kpi) {
      data[kpi.key] =
        ProjectReportStatsUtils.mapChannelKpi(kpi.key, 
          pageStats.channelBreakdown, 
          kpi.type === 'volume' ? $scope.participatingChannels : false);
      return data;
    }, {});
    $scope.getChannelDataFormat = function(kpiKey) {
      var filter = ProjectKpis[kpiKey].format;
      if(!filter) {
        return _.identity;
      }
      return $filter(filter);
    };
    $scope.channelLabels = _.map($scope.participatingChannels, function(c) {
      c = c.toLowerCase();
      if (c === 'facebook') {
        return 'Facebook';
      } else if (c === 'twitter') {
        return 'Twitter';
      } else if(c === 'adwords') {
        return 'AdWords';
      }
    });
    $scope.getChannelData = function() {
      return $scope.kpiOvertime[$scope.activeKpi];
    };
    $scope.activeKpiIsVolume = function() {
      return ProjectKpis[$scope.activeKpi].type === 'volume';
    };
    $scope.activeKpiLabel = function() {
      return ProjectKpis[$scope.activeKpi].label;
    };

    $scope.engageKpis = _.filter($scope.availableKpis, function(kpi) {
      return kpi.category === 'engagement';
    });
    $scope.costKpis = _.filter($scope.availableKpis, function(kpi) {
      return kpi.category === 'cost';
    });
    $scope.activityData = ProjectReportStatsUtils.flattenActivities(pageStats.activityBreakdown);
    $scope.engageMode = {
      label : 'Engagements',
      value : 'ENGAGE'
    };
    $scope.costMode = {
      label : 'Costs',
      value : 'COST'
    };
    $scope.gridMode = {
      mode : $scope.engageMode.value
    };

    $scope.engageOpts = {
      data : 'activityData',
      columnDefs : _.reduce($scope.engageKpis, function(defs, a) {
        defs[a.key] = {
          label : a.label, 
          type : 'numeric',
          tooltip : a.tooltip || '',
          format : $filter(a.format || 'metricVolumeCompact')
        };
        return defs;
      }, {
        activityName : {
          label : 'Activity',
          columnGrow : 2,
          reactComponent : CampaignNameCell,
          type : 'string'
        }
      }) 
      /*
      columnDefs : _.map($scope.engageKpis, function(a) {
        return {
          field : a.key,
          displayName : a.label,
          cellFilter : 'metricVolumeCompact'
        };
      })
      */
    };
    $scope.costOpts = {
      data : 'activityData',
      columnDefs : _.reduce($scope.costKpis, function(defs, a) {
        defs[a.key] = {
          label : a.label, 
          type : 'numeric',
          tooltip : a.tooltip || '',
          format : $filter('metricCurrency')
        };
        return defs;
      }, {
        activityName : {
          label : 'Activity',
          type : 'string',
          columnGrow : 2,
          reactComponent : CampaignNameCell 
        }
      }) 
      /*
      columnDefs : _.map($scope.costKpis, function(a) {
        return {
          field : a.key,
          displayName : a.label,
          cellFilter : 'metricCurrency'
        };
      })
      */
    };

    $scope.cohortColors = ['#3B5999', '#DD4B39'];

    $scope.formatDate = d3.time.format('%m/%d/%y');

  });
