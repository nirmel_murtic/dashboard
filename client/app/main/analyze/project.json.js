/*

TEAM:
At the top of the project hierarchy is the Team. The team should be created before the project is created.

PROJECT:
When the project is created, it will be associated with the current team. 

A project will have one or more goals. Projects and goals are created in the Align section.

GOALS:
There are a number of types of goals, including POST_ENGAGEMENT, PAGE_LIKE, WEBSITE_CLICK and others.
These will be initially finalized the week of February 23-27. It is expected that these will grow as
additional channels are added. These are determined at the backend by the type of goal that is set.

ACTIVITIES:
Hierarchically beneath the Goal are activities. There are three types of activities: they can be
organic, with no direct cash investment associated, or purchased. The three types are:

POST  (organic)
   This is not associated with expenditure, and its success is measured in EPI (engagement per impression)
LINK  (organic*)
   This can be paid for, as in the case of a blogger posting a link to a campaign destination
CAMPAIGN (paid-for)
   This can also include a paid-for post or re-post, often associated with an organic post

The calculations in the chart are determined as follows:   
   * Cost/Conversions = CPA
   * Clicks/Paid Impressions = CTR
   * Conversions/Paid Impression = CVR
   * Engagements/Free Impressions = EPI

Each activity is associated with one or more channels. Their values will either be the single channel in which the activity takes place or multiple:

channel: 'FACEBOOK' or
channel: 'TWITTER'  or
channel: 'MULTIPLE'

*** Sorted by Impact and Value  will be revisited in conjunction with how to order organic and non-organic goals.

There will be two properties for each activity associated with time measurement: timePeriod and timeSeries. 
Initial valid values will be:

	timePeriod
		d: day (24 hours);
		w: week (7 days);
		m: month (28-31 days);
		q: quarter (3 months);
		h: half year (6 months);
		y: year (12 months)

	timeSeries will have the number of elements required by the timePeriod in an associative array with the following properties:
		date:TIMESTAMP in milliseconds since 1/1/1970, 
		actid:the activity ID, 
		imp:number of impressions,
		clk:number of click-thrus,
		cnv:number of conversions

	EXAMPLE:
		timePeriod:'w',
		timeSeries: [
			{date:1417717823600, actid:100, imp:25,clk:1,cnv:0,cost:2.25},
			{date:1417804223600, actid:100, imp:205,clk:7,cnv:5,cost:17.25},
			{date:1417890623600, actid:100, imp:125,clk:3,cnv:1,cost:12.5},
			{date:1417977023600, actid:100, imp:151,clk:10,cnv:2,cost:14.5},
			{date:1418063423600, actid:100, imp:525,clk:15,cnv:10,cost:32.5},
			{date:1418149823600, actid:100, imp:25,clk:1,cnv:0,cost:2.75},
			{date:1418236223600, actid:100, imp:125,clk:3,cnv:3,cost:11.25}
		]

*/

