'use strict';

angular.module('analyze')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.analyze.chartTest', {
        url: '/charts',
        views: {
          '@main': {
              templateUrl: 'app/main/analyze/chartTest/chartTest.html',
              controller: 'chartTestCtrl'
          }
        },
        data: {
          displayName: 'Charts'
        },
        rightColumn : {
          present : true,
          columns : 5
        }
      });
  });
