'use strict';

angular.module('analyze')
  .controller('chartTestCtrl',
    function ($scope) {
    /* jshint newcap:false, bitwise:false */

    //
    $scope.radius = 100;

    $scope.ratio = 0.5;

    $scope.dateFormat = d3.time.format('[%Y][%m][%d]');
    $scope.dates = [];
    var ranges = {
      Now : [d3.time.day.floor( new Date() ), new Date()],
      Month : [d3.time.month.floor( new Date() ), new Date()]
    };
    $scope.dateOpts = {
      timePicker : false,
      ranges : ranges
    };
    $scope.datesChangeLog = [];
    var i = 0;
    $scope.changedDates = function(ds) {
      $scope.datesChangeLog.push({
        id : ++i,
        dates : ds
      });
      $scope.datesString = ds.map(function(d) {
        return moment(d).format('MM/DD/YYYY');
      }).join('-');
    };

    $scope.parseDates = function(str) {
      var dates = str.split(/\s*-\s*/g);
      if(dates.length === 2) {
        $scope.dates = dates.map(function(d) {
          return moment(d).toDate();
        });
      } else {
        $scope.dates = null;
      }
    };

    $scope.formatter = {
      string : '[%Y][%m][%d]'
    };

    $scope.changeFormatter = function(str) {
      try {
        $scope.dateFormat = d3.time.format(str);
      } catch (err) {
        return;
      }
    };

    $scope.cohort = _.map(_.range(25), function(i) {
      var time = new Date(2015, 2, i);
      if(i === 0) {
        time = new Date(time - ((Math.random() * 36000000) | 0));
      }
      if(i === 24) {
        time = new Date(time + ((Math.random() * 36000000) | 0));
      }
      return {
        fb : ~~(Math.random() * 40 + 10),
        aw : ~~(Math.random() * 40 + 10),
        ad : ~~(Math.random() * 40 + 10),
        tw : ~~(Math.random() * 40 + 10),
        ig : ~~(Math.random() * 40 + 10),
        time : time
      };
    });

    $scope.cohortLabels = ['FB', 'AW', 'AD', 'TW', 'IG'];

    $scope.getMetrics = function() {
      var n = (Math.random() * 100 + 2) | 0;
      console.log(n + ' data points.');
      return _.map(_.range(n), function(i) {
        return {
          clicks : ~~(Math.random() * 40 + 10),
          impressions : ~~(Math.random() * 400 + Math.random() * 200 + Math.random() * 100 + 100),
          time : new Date(2015, 2, i)
        };
      });
    };

    $scope.metrics = $scope.getMetrics();


    $scope.metricLabels = ['Clicks', 'Impressions'];
    $scope.metricKeys = ['clicks'];

    $scope.formatInt = d3.format('0d');
    // for NAs
    $scope.naData = _.range(50).map(function(i, __, r) {
      var d = (i - r.length / 2) / r.length; 
      if(d > 0) {
        d = -d;
      }
      d*=8; // [-4, 0]
      var naProb = Math.exp(d) / 10; // [0.0018315638888734183, 0.1]
      var datum = {
        time : new Date(2015, 2, i),
        clicks : ~~(Math.random() * 4 + 1),
        impressions : ~~(Math.random() * 4 + 2)
      };
      if(Math.random() < naProb) {
        datum.impressions = null;
        datum.clicks = null;
      } else if(Math.random() < naProb) {
        datum.clicks = null;
      }
      return datum;
    });
    $scope.fields = ['clicks', 'impressions'];
    $scope.timeField = 'time';


    // test table
    $scope.defs= {
      id : {
        type : 'numeric',
        label: 'ID',
        filter : false,
        sort : false
      },
      text : {
        type : 'string',
        label : 'String Column'
      },
      col1 : {
        type : 'numeric',
        label : 'First Column'
      },
      col2 : {
        type : 'numeric',
        format : function(a) {
          return '$' + a;
        },
        label : 'Second Column'
      },
      col3 : {
        type : 'atom',
        levels : ['A', 'B', 'C'],
        format : function(a) {
          return a.toLowerCase() + ' ::';
        },
        label : 'Atom Column'
      }
    };
    $scope.tableData = _.range(50).map(function(i) {
      return {
        id : i,
        col1 : ~~(Math.random() * 100),
        col2 : ~~(Math.random() * 100 * Math.sin(i) + 100),
        col3 : Math.random() > 0.5 ? 'A' : 'B',
        text : Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '') + 
               Math.random().toString(32).replace(/[\d\.]/g, '')  

      };
    });
  });
