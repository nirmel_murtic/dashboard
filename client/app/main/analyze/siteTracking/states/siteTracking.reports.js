'use strict';

angular.module('analyze')
  .config(function ($stateProvider) {
    $stateProvider.state('main.analyze.sitetracking.reports', {
      url: '/:metrics',
      data: {
        displayName: 'Website',
        active: 'website'
      },
      reloadOnSearch : true,
      params : {
        metrics : null,
        charts : null,
        reloadChart : true,
        metricValues : null
      },
      views : {
        'metrics@main.analyze.sitetracking' : {
          templateUrl : 'app/main/analyze/siteTracking/templates/metrics.html',
          controller : 'TrafficMetricsController'
        },
        'chart@main.analyze.sitetracking' : {
          templateUrl : 'app/main/analyze/siteTracking/templates/chart.html',
          controller : 'TrafficChartController'
        },
        'platform@main.analyze.sitetracking' : {
          templateUrl : 'app/main/analyze/siteTracking/templates/platform.html',
          controller : 'TrafficPlatformController'
        },
        'sources@main.analyze.sitetracking' : {
          templateUrl : 'app/main/analyze/siteTracking/templates/sources.html',
          controller : 'TrafficSourcesController'
        },
        'geographical@main.analyze.sitetracking' : {
          templateUrl : 'app/main/analyze/siteTracking/templates/geographical.html',
          controller : 'TrafficGeographicalController'
        },
        'events@main.analyze.sitetracking' : {
          templateUrl : 'app/main/analyze/siteTracking/templates/events.html',
          controller : 'TrafficEventTableController'
        }
      },
      resolve: {
        config : function(config, $stateParams, disableMetrics, SitetrackingStateErrors, SitetrackingMetricsDef, $q, $state) {
          if(config.sites.length < 1) {
            return;
          }
          config.filterAndDrilldown = $stateParams.filterAndDrilldown;
          config.eventsOrPages = $stateParams.eventsOrPages;
          config.charts = $stateParams.charts;
          config.metricValues = $stateParams.metricValues;
          config.reloadChart = $stateParams.reloadChart || (!$stateParams.charts);
          var disabledMetrics = disableMetrics(config);
          var selectedMetricsInvalid = _.any(config.activeMetrics, function(metric) {
            return !!disabledMetrics[metric];
          });
          if(selectedMetricsInvalid) {
            var nextState = $state.next;
            var error = {};

            var newMetric = _.find(SitetrackingMetricsDef, function(__, metric) {
              return !disabledMetrics[metric];
            });

            var correctedParams = _.assign({}, $stateParams, {
              metrics : newMetric.value
            });
            error[SitetrackingStateErrors.INVALID_PARAMS] = {
              correction : correctedParams
            };
            $state.transition.finally(function() {
              $state.go(nextState.name, correctedParams);
            });
            return $q.reject(error);
          } else {
            config.disabledMetrics = disabledMetrics;
          }
          return config;
        }
      }
    });
  });
