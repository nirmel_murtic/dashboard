'use strict';

angular.module('analyze')
  .config(function($provide) {
    $provide.decorator('$state', function($delegate, $rootScope) {
      $rootScope.$on('$stateChangeStart', function(event, state, params) {
        $delegate.next = state;
        $delegate.toParams = params;
      });
      return $delegate;
    });
  })
  .config(function ($stateProvider) {
    $stateProvider.state('main.analyze.sitetracking', {
      url: '/website?host&daterange',
      permissions: '<LOGGED_IN> && <ANALYZE>',
      data: {
        displayName: false
      },
      views : {
        '@main' : {
          templateUrl: 'app/main/analyze/siteTracking/templates/sitetracking-main.html',
          controller : 'SiteTrackingMaster'
        }
      },
      params : {
        host : null,
        sites : null,
        daterange : null,
        customDates : null,
        metrics : null,
        filterAndDrilldown : {
          hasFilter: false,
          filters: [],
          hasDrilldown: false,
          drilldown: {
            type: '',
            value: ''
          }
        },
        eventsOrPages : 'events'
      },
      resolve: {
        config : function($stateParams, reportsConfig, auth, $state, SitetrackingStateErrors, $q) {
          var self = this;
          if($state.next.name === this.name) {
            var nextParams = _.assign({}, $stateParams, { metrics : 'pageViewCount' });
            $state.transition.finally(function() {
              $state.go(self.name + '.reports', nextParams);
            });
            return $q.reject(null);
          }
          return reportsConfig.resolve(auth.user, $stateParams)
            .then(function(conf) {
                conf.filterAndDrilldown = $stateParams.filterAndDrilldown;
                conf.eventsOrPages = $stateParams.eventsOrPages;
                return conf;
            }, function(error) {
              // recover from NO_SITES and INVALID_PARAMS errors
              if(error[SitetrackingStateErrors.NO_SITES]) {
                $state.transition.finally(function() {
                  nextParams.NO_SITES = true;
                  $state.go($state.next, nextParams);
                });
               return { sites : [] };
              }
              var paramsError = error[SitetrackingStateErrors.INVALID_PARAMS];
              var nextParams = paramsError ? paramsError.correction : null;
              var nextState = $state.next;
              if(nextParams) {
                $state.transition.finally(function() {
                  $state.go(nextState.name, nextParams);
                });
              }
              return $q.reject(error);
            });
        }
      }
    });
  });
