'use strict';

angular.module('analyze')
  .controller('SiteTrackingMaster', function($scope, $state, $stateParams, config, SitetrackingMetricsDef, $auth) {

    if(__DEV__) {
      console.log('Load site tracking master controller with config: ', config);
    }

    $scope.sites = config.sites;
    if(config.sites.length === 0) {
      return;
    }

    function saveSettings(settings) {
      var savedSettings = $auth.retrieveData('sitetracking-settings') || {};
      savedSettings = settings ? _.merge(savedSettings, settings) : savedSettings;
      $auth.persistData('sitetracking-settings', savedSettings);
    }

    $scope.updateReports = function(newParams, forceUpdate) {
      var nextParams = angular.copy($stateParams);
      nextParams.charts = $scope.charts;
      _.merge(nextParams, newParams);
      if(__DEV__) {
        console.log('nextParams', nextParams);
      }
      if($state.transtion) {
        try {
          window.stop();
        }
        catch(e)
        {
          // for IE
          document.execCommand('Stop');
        }
      }
      console.log(forceUpdate, $state.current.name);
      $state.transitionTo($state.current, nextParams, { reload : forceUpdate ? $state.current.name : false });
    };

    // bind sites and date ranges data to scope
    $scope.selectedSite = config.selectedSite;

    $scope.selectedPeriod = config.selectedDates;
    $scope.dateranges = config.dateranges;

    $scope.daterangePickerOpts = {
      timePicker: false,
      ranges: config.dateranges,
      maxDate: moment().endOf('day')
    };
    $scope.changeSite = function(site) {
      $scope.selectedSite = site;
      var nextParams = _.assign({}, 
        $stateParams, { host: site.host, charts : $scope.charts,
          filterAndDrilldown : { 
            hasFilter: false,
            filters: [],
            hasDrilldown: false,
            drilldown: {
              type: '',
              value: ''
            }
          }});
      saveSettings({ site : site.host });
      $state.transitionTo($state.current, nextParams, { reload: false });
    };
    $scope.changeDateRange = function (dateRange) {

      var label = 'custom';
      for(var labelPreset in config.dateranges) {
        if(_.isEqual(config.dateranges[labelPreset], dateRange)) {
          label = labelPreset;
          break;
        }
      }
      if(label === 'Now') {
        label = 'now';
      } else if(label === 'Yesterday') {
        label = 'yesterday';
      } else if(label === 'Last 7 Days') {
        label = 'last7';
      } else if(label === 'Last 30 Days') {
        label = 'last30';
      }

      if(label !== 'custom') {
        saveSettings({ daterange : label });
      }

    
      var nextParams = _.assign({}, 
        $stateParams, { daterange: label, customDates: dateRange, charts : $scope.charts, 
          filterAndDrilldown : { 
            hasFilter: false,
            filters: [],
            hasDrilldown: false,
            drilldown: {
              type: '',
              value: ''
            }
          }});
      $state.transitionTo($state.current, nextParams, { reload: false });
    };
    // end of sites and date ranges

    // setup metrics
    $scope.availableMetrics = config.metricValues || angular.copy(_.toArray(SitetrackingMetricsDef));


    $scope.activeMetrics = _.map(config.activeMetrics, function(metricName) {
      return _.find($scope.availableMetrics, { value : metricName });
    }); 

    _.each($scope.availableMetrics, function(metric) {
       metric.second = false;
       metric.first = false;
       metric.checked = false;
       metric.disabled = !!config.disabledMetrics[metric.value];
       if(metric.disabled) {
         metric.amount = 'NA'; 
       } else if (metric.value === config.activeMetrics[0]) {
         if(!metric.disabled) {
           metric.first = true;
           metric.checked = true;
         }
       } else if (config.activeMetrics.length > 1 && metric.value === config.activeMetrics[1]) {
         if(!metric.disabled) {
           metric.second = true;
           metric.checked = true;
         }
       }
    });

    // end of metrics setup

    // init chart bindings

    $scope.charts = config.charts || {
      traffic: {
        data: []
      },
      platform: {
        PersonalComputer: [],
        Smartphone: [],
        Tablet: []
      },
      sources: {},
      geographical: {},
      events: {}
    };
    _.assign($scope.charts.traffic, {
      metrics: $scope.activeMetrics,
      config: {
        valueFields: _.map($scope.activeMetrics, function(m) {
          return m.value.toLowerCase();
        }),
        labels: _.map($scope.activeMetrics, function(m) {
          return m.label;
        })
      }
    });

    // setup filters and drilldowns if any
    $scope.hasFilterOrDrilldown = function() {
      return config.filterAndDrilldown.hasFilter || config.filterAndDrilldown.hasDrilldown;
    };

    $scope.clearAllFiltersAndDrilldown = function() {
      delete $stateParams.filterAndDrilldown;
      $scope.updateReports({
        filterAndDrilldown : { 
          hasFilter: false,
          filters: [],
          hasDrilldown: false,
          drilldown: {
            type: '',
            value: ''
          }
        }
      });
    };
    

    // utility functions to be used for child controllers
    $scope.getQueryFilter = function (query, config, defaultFilter, includes) {

      var includePlatforms = includes.includePlatforms; 
      var includeSources = includes.includeSources; 
      var includeDrilldown= includes.includeDrilldown; 

      query.filter = defaultFilter || '';
      if (config.filterAndDrilldown.hasFilter === true) {
        var filters = {
          platforms: [],
          sources: []
        };
        for (var i = 0; i < config.filterAndDrilldown.filters.length; i++) {
          if (config.filterAndDrilldown.filters[i].type === 'deviceType=' && includePlatforms === true) {
            filters.platforms.push('deviceType=' + config.filterAndDrilldown.filters[i].value);
          } else if (config.filterAndDrilldown.filters[i].type === 'referal=' && includeSources === true) {
            filters.sources.push('referal=' + config.filterAndDrilldown.filters[i].value);
          }
        }
        var filterString = '';
        if (filters.platforms.length !== 0) {
          filterString = filters.platforms.join(';');
        }
        if (filters.sources.length !== 0) {
          filterString = (filterString !== '' ? filterString + ',' : '') + filters.sources.join(';');
        }
        query.filter = (query.filter ? query.filter + ',' : '') + filterString;
      }
      if (config.filterAndDrilldown.hasDrilldown === true && includeDrilldown === true) {
        query.filter = (query.filter && query.filter !== '' ? query.filter + ',' : '') + 
          config.filterAndDrilldown.drilldown.type + config.filterAndDrilldown.drilldown.value;
      }
      if (!query.filter || query.filter === '') {
        delete query.filter;
      }
      return query;
    };

    $scope.calculatePercentage = function (value, total) {
      var precisely = ((value || 0) / (total || 1));
      return precisely;
    };

  });
