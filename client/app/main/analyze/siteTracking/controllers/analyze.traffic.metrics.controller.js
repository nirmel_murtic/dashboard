'use strict';

angular.module('analyze')
  .controller('TrafficMetricsController', ['$rootScope', '$scope', '$q', '$timeout', 'pixelService', 'Webanalytics.WebApiUtils', 'SitetrackingMetricsDef', 'config', function ($rootScope, $scope, $q, $timeout, pixelService, WebApiUtils, SiterackingMetricsDef, config) {

    // split metrics in two rows for responsive purposes
    var n = $scope.availableMetrics.length;
    $scope.availableMetricsRow1 = $scope.availableMetrics.slice(0, Math.floor(n/2));
    $scope.availableMetricsRow2 = $scope.availableMetrics.slice(Math.floor(n/2));

    $scope.isLoading = false;


    // called by ng-init
    $scope.loadMetricsData = function() {
      // set loading indicator to show
      if(config.metricValues) {
        return;
      }

      angular.forEach($scope.availableMetrics, function (metric) {
        if (metric.disabled === false) {
          metric.loading = true;
        }
      });
      // init query with dates
      var query = {
        startDate: config.selectedDates[0],
        endDate: config.selectedDates[1],
        dimensions: 'propertyId'
      };
      // add filters and drilldown to query
      query = $scope.getQueryFilter(query, config, undefined, {
        includePlatforms : true, 
        includeSources : true,
        includeDrilldown : true
      });
      // use minute aggregates for now or yesterday
      if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
          _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
        query.minutesWidth = 1440;
      }
      
      var activeMetrics = _($scope.availableMetrics)
          .filter(function(m) { return !m.disabled; });

      var qMetrics = activeMetrics
            .map(function(m) { return m.value; })
            .reduce(function(s, v) { return (s ? s+',': '') + v; }, '');

      query.metrics = qMetrics;

      var loadingMetrics = activeMetrics.map(function(m) { m.loading=true; return m; });
      $q.all([WebApiUtils.executeQuery(config.selectedSite.propertyId, query), $q.when(loadingMetrics.value())])
        .then(function(pair) {
          var readyMetrics = pair[1];
          var result = pair[0];
          _.each(readyMetrics, function(metric) {
            var value = 0;
            if (result && result.data && result.data.data && result.data.data.length !== 0 && result.data.data[0]) {
              value = result.data.data[0][metric.value.toLowerCase()] || 0;
            }
            if (metric.convert) {
              switch (metric.convert) {
                case 'time':
                  metric.amount = calculateTimePeriod(value);
                  break;
                case 'percentage':
                  metric.amount = getPercentage(value);
                  break;
              }
            } else {
              metric.amount = value;
            }
            metric.loading = false;
          });
        }, function(pair) {
          var readyMetrics = pair[1];
          _.each(readyMetrics, function(metric) {
            metric.loading = false;
            metric.amount = 'N/A'; 
          });
        });


      /*
      // make calls, once done assign amount and set loading to false 
      angular.forEach($scope.availableMetrics, function (metric) {
        var _query = angular.copy(query);
        if (!metric.disabled) {
          _query.metrics = metric.value + ',dailyAvgReturningVisitorRate';
          WebApiUtils.executeQuery(config.selectedSite.propertyId, _query, function (result) {
            var value = 0;
            if (result && result.data && result.data.data && result.data.data.length !== 0 && result.data.data[0]) {
              value = result.data.data[0][metric.value.toLowerCase()] || 0;
            }
            if (metric.convert) {
              switch (metric.convert) {
                case 'time':
                  metric.amount = calculateTimePeriod(value);
                  break;
                case 'percentage':
                  metric.amount = getPercentage(value);
                  break;
              }
            } else {
              metric.amount = value;
            }
            metric.loading = false;
          }, undefined, function(err) {
            if(err.status !== 0) {
              metric.amount = 'error';
              metric.loading = false;
            }
          });
        }
      });
      */
    };

    $scope.clickOnMetric = function (metric) {
      if(_.any($scope.availableMetrics, function(m) { return m.loading; })) {
        return;
      }
      //console.log('metric', metric);
      var reloadChart = true;
      if (metric.disabled === false) {
        if (metric.checked) {
          if ($scope.charts.traffic.metrics.indexOf(metric) === 0) {
            if ($scope.charts.traffic.metrics.length === 2) {
              $scope.charts.traffic.metrics[0].first = false;
              $scope.charts.traffic.metrics[1].second = false;
              $scope.charts.traffic.metrics[0] = $scope.charts.traffic.metrics[1];
              $scope.charts.traffic.metrics[0].color = '#279CB7';
              $scope.charts.traffic.metrics[0].first = true;
              $scope.charts.traffic.metrics.pop();
            } else {
              return;
            }
          } else if ($scope.charts.traffic.metrics.indexOf(metric) === 1) {
            $scope.charts.traffic.metrics[1].second = false;
            $scope.charts.traffic.metrics.pop();
          }
          reloadChart = false;
          metric.color = '#909d90';
          metric.checked = false;
        } else {
          if ($scope.charts.traffic.metrics.length === 2) {
            $scope.charts.traffic.metrics[1].color = '#909d90';
            $scope.charts.traffic.metrics[1].checked = false;
            $scope.charts.traffic.metrics[1].second = false;
            $scope.charts.traffic.metrics.pop();
            metric.color = '#6CBF6B';
            metric.second = true;
            $scope.charts.traffic.metrics.push(metric);
          } else {
            if ($scope.charts.traffic.metrics.length === 0) {
              metric.color = '#279CB7';
              metric.first = true;
            } else {
              metric.color = '#6CBF6B';
              metric.second = true;
            }
            $scope.charts.traffic.metrics.push(metric);
          }
          metric.checked = true;
        }

        if($scope.charts.traffic.data.length) {
          _.each($scope.charts.traffic.data, function(d) {
            var key;
            for(var i=0; i<$scope.availableMetrics.length; i++) {
              key = $scope.availableMetrics[i].value.toLowerCase();
              if(!(key in d)) {
                d[key] = 0;
              }
            }
          }); 
        }

        var updates = {
          metrics : _.map($scope.charts.traffic.metrics, function(metric) {
            return metric.value;
          }).join(','),
          metricValues : $scope.availableMetrics,
          reloadChart : reloadChart
        };
        $scope.updateReports(updates);
      }
    };


    // formatting helpers
    function getPercentage(x) {
      return x.toFixed(2) + '%';
    }

    /**
     * calculateTimePeriod
     *  
     * Convert number of seconds into a string 
     * of properly formated durations
     *
     * @param {Number} - seconds
     * @return {String} - HH:mm:ss
     */
    function calculateTimePeriod(seconds) {
      var outputString;
      var numDays = Math.floor(seconds / 86400);
      var numWeeks = Math.floor(numDays / 7);
      if (numWeeks > 0) {
        numDays = numDays % 7;
      }
      if (numWeeks > 0) {
        outputString = numWeeks + 'wk';
        if (numDays > 0) {
          outputString = outputString + '/' + numDays + 'd';
        }
        return outputString;
      } else {
        var numHours = Math.floor((seconds % 86400) / 3600);
        if (numDays > 0) {
          outputString = numDays + 'd';
          if (numHours > 0) {
            outputString = outputString + '/' + numHours + 'h';
          }
        } else {
          var numMinutes = Math.floor(((seconds % 86400) % 3600) / 60);
          var numSeconds = ((seconds % 86400) % 3600) % 60;
          var date = moment();
          date.set('hour', numHours);
          date.set('minute', numMinutes);
          date.set('second', numSeconds);
          outputString = date.format('HH:mm:ss');
          return outputString;
        }
      }
    }

  }]);
