'use strict';

angular.module('analyze')
  .controller('TrafficPlatformController', ['$rootScope', '$scope', '$timeout', 'TrafficChartDashboard', 'Webanalytics.WebApiUtils', 'config', 
  function ($rootScope, $scope, $timeout, TrafficChartDashboard, WebApiUtils, config) {

    $scope.Math = Math;
    $scope.currentFilters = _.filter(config.filterAndDrilldown.filters, function(filter) {
      return filter.type === 'deviceType='; 
    });

    $scope.isFilteredBy = function (deviceType) {
      for (var i = 0; i < $scope.currentFilters.length; i++) {
        if ($scope.currentFilters[i].value === deviceType) {
          return true;
        }
      }
      return false;
    };


    function deleteFromArray(array, deviceType) {
      var index = -1;
      for (var i = 0; i < array.length; i++) {
        if (array[i].value === deviceType) {
          index = i;
        }
      }
      if (index !== -1) {
        array.splice(index, 1);
      }
      return array;
    }

    $scope.filterPage = function (deviceType) {
      if ($scope.isFilteredBy(deviceType) === true) {
        $scope.currentFilters = deleteFromArray($scope.currentFilters, deviceType);
        config.filterAndDrilldown.filters = deleteFromArray(config.filterAndDrilldown.filters, deviceType);
      } else {
        if ($scope.currentFilters.length === 2) {
          config.filterAndDrilldown.filters = _.difference(config.filterAndDrilldown.filters, $scope.currentFilters);
          $scope.currentFilters.length = 0;
        } else {
          var filter = {
            type: 'deviceType=',
            value: deviceType
          };
          $scope.currentFilters.push(filter);
          config.filterAndDrilldown.filters.push(filter);
        }
      }
      config.filterAndDrilldown.hasFilter = config.filterAndDrilldown.filters.length > 0;
      $scope.updateReports({
        filterAndDrilldown : angular.copy(config.filterAndDrilldown)
      });
    };


    /*
    TrafficChartDashboard.formPlatformData({data: {data: []}}, function (platformData) {
      $scope.charts.platform = platformData;
    });
    */


    $scope.loadPlatformData = function() {
      $scope.isLoading = true;
      var query = {
        startDate: config.selectedDates[0],
        endDate: config.selectedDates[1],
        metrics: 'eventCount',
        dimensions: 'propertyId,os,deviceType'
      };

      var defaultFilter = 'eventName=Page load';
      if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type === 'eventName=') {
        defaultFilter = undefined;
      }
      query = $scope.getQueryFilter(query, config, defaultFilter, {
        includePlatforms : false, 
        includeSources : true,
        includeDrilldown : true
      });
      // use minute aggregates for now or yesterday
      if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
          _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
        query.minutesWidth = 1440;
      }
      getPlatformsData(config.selectedSite.propertyId, query);
    };

    function getPlatformsData(property, query) {
      WebApiUtils.executeQuery(property, query, function (result) {
        TrafficChartDashboard.formPlatformData(result, function (platformData) {
          $scope.charts.platform = platformData;
          $scope.isLoading = false;
        });
      });
    }

  }]);




