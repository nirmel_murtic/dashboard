'use strict';

angular.module('analyze')
  .controller('TrafficEventTableController', ['$rootScope', '$scope', '$q', '$timeout', 'Webanalytics.WebApiUtils', 'config', 
  function ($rootScope, $scope, $q, $timeout, WebApiUtils, config) {
    if(config.filterAndDrilldown.hasDrilldown) {
      $scope.currentDrilldown= config.filterAndDrilldown.drilldown.value;
    } else {
      $scope.currentDrilldown= '';
    }
    $scope.eventTab = config.eventsOrPages === 'events';
    console.log($scope.eventTab, config);
    $scope.Math = Math;


    $scope.topPageTabOpened = function () {
      $scope.eventTab = false;
      getTopPagesTabData();
    };

    $scope.EventTabOpened = function () {
      $scope.eventTab = true;
      getEventTabData();
    };


    function getEventTabData () {
      $scope.isLoadingFirst = true;
      $scope.isLoadingSecond = true;
      var query = formQueryObject('customEventCount', 'propertyId,eventName');
      var defaultFilter;
      if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type === 'eventName=') {
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : false
        });
      } else {
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : config.filterAndDrilldown.hasDrilldown
        });
      }
      WebApiUtils.executeQuery(config.selectedSite.propertyId, query, function (result) {
        $scope.charts.events.eventTable = result.data.data;
        $scope.isLoadingFirst = false;
      });
      query = formQueryObject('customEventCount', 'propertyId,referalUrl');
      query = $scope.getQueryFilter(query, config, defaultFilter, {
        includePlatforms : true, 
        includeSources : true,
        includeDrilldown : config.filterAndDrilldown.hasDrilldown
      });
      WebApiUtils.executeQuery(config.selectedSite.propertyId, query, function (result) {
        $scope.charts.events.referalTable = result.data.data;
        $scope.isLoadingSecond = false;
      });
    }

    function getTopPagesTabData () {
      $scope.isLoadingFirst = true;
      $scope.isLoadingSecond = true;
      var query = formQueryObject('pageViewCount', 'propertyId,domain,page');
      var defaultFilter;
      if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type === 'page=') {
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : false
        });
      } else {
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : config.filterAndDrilldown.hasDrilldown
        });
      }
      WebApiUtils.executeQuery(config.selectedSite.propertyId, query, function (result) {
        $scope.charts.events.topPages = result.data.data;
        $scope.isLoadingFirst = false;
      });
      query.dimensions = 'propertyId,referalUrl';
      query = $scope.getQueryFilter(query, config, defaultFilter, {
        includePlatforms : true, 
        includeSources : true,
        includeDrilldown : config.filterAndDrilldown.hasDrilldown
      });
      WebApiUtils.executeQuery(config.selectedSite.propertyId, query, function (result) {
        $scope.charts.events.referalTable = result.data.data;
        $scope.isLoadingSecond = false;
      });
    }

    function formQueryObject(metric, dimensions) {
      $scope.currentDrilldown ='';
      if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type ==='eventName=') {
        $scope.currentDrilldown = config.filterAndDrilldown.drilldown.value;
      }
      if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type ==='page=') {
        $scope.currentDrilldown = config.filterAndDrilldown.drilldown.value;
      }
      var query = {
        startDate: config.selectedDates[0],
        endDate: config.selectedDates[1],
        metrics: metric,
        dimensions: dimensions,
        sort: metric+'-DESC',
        maxResult: 200
      };
      // use minute aggregates for now or yesterday
      if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
          _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
        query.minutesWidth = 1440;
      }
      return query;
    }

    $scope.loadEventsData = function() {
      if ($scope.eventTab === true) {
        getEventTabData();
      } else {
        getTopPagesTabData();
      }
      /*
      var query={}, defaultFilter = undefined;
      $scope.isLoadingSecond = true;
      if ($scope.eventTab) {
        query = formQueryObject('customEventCount', 'domain,referalUrl');
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : true
        });
        WebApiUtils.executeQuery(config.selectedSite.propertyId, query, function (result) {
          $scope.charts.events.referalTable = result.data.data;
          $scope.isLoadingSecond = false;
        });
      } else {
        query = formQueryObject('pageViewCount', 'domain,referalUrl');
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : true
        });
        WebApiUtils.executeQuery(config.selectedSite.propertyId, query, function (result) {
          $scope.charts.events.referalTable = result.data.data;
          $scope.isLoadingSecond = false;
        });
      }
      */
    };

    $scope.eventClickFilter = function (event) {
      if ($scope.currentDrilldown !== event.eventname) {
        config.filterAndDrilldown.hasDrilldown = true;
        config.filterAndDrilldown.drilldown = {
          type: 'eventName=',

          // TODO: REGEXs should be stored in REGEX.constant.js and not hardcoded here
          value: event.eventname.replace(/[=|:|,]/g,'\\$&')
        };
        $scope.currentDrilldown=event.eventname;
      } else {
        config.filterAndDrilldown.hasDrilldown = false;
        config.filterAndDrilldown.drilldown = {
          type: 'eventName=',
          value: ''
        };
        $scope.currentDrilldown='';
      }
      $scope.updateReports({
        filterAndDrilldown : angular.copy(config.filterAndDrilldown),
        eventsOrPages : $scope.eventTab ? 'events' : 'pages'
      }, true);
    };

    $scope.topPageClickFilter = function (topPage) {
      if ($scope.currentDrilldown !== topPage.page) {
        config.filterAndDrilldown.hasDrilldown = true;
        config.filterAndDrilldown.drilldown = {
          type: 'page=',
          value: topPage.page
        };
        $scope.currentDrilldown=topPage.page;
      } else {
        config.filterAndDrilldown.hasDrilldown = false;
        config.filterAndDrilldown.drilldown = {
          type: 'page=',
          value: ''
        };
        $scope.currentDrilldown='';
      }
      $scope.updateReports({
        filterAndDrilldown : angular.copy(config.filterAndDrilldown),
        eventsOrPages : $scope.eventTab ? 'events' : 'pages'
      }, true);
    };

  }]);



