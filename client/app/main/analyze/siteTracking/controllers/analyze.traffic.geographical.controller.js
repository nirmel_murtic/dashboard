'use strict';

angular.module('analyze')
    .controller('TrafficGeographicalController', ['$rootScope', '$scope', '$q', '$timeout','Webanalytics.WebApiUtils', 'TrafficChartDashboard', 'fsConfirm', 'config', 
    function ($rootScope, $scope, $q, $timeout,WebApiUtils, TrafficChartDashboard, fsConfirm, config) {

      $scope.currentDrilldown = '';
      $scope.selectedCountry = '';
      $scope.countryAccoirdionOpened = true;
      $scope.isLoading = true;

      $scope.geoMetrics = [
        {label:'Sessions',value:'trafficSourceCount'},
        {label:'New Visitors',value:'newVisitorCount'},
        {label:'Total Page Views',value:'eventCount'}
      ];
      $scope.selectedGeoMetric = $scope.geoMetrics[2];


      function filterPage (geoLocation, filterBy, countryCode) {
        if (geoLocation.indexOf('undetermined') === -1 && geoLocation.indexOf('No ') === -1) {
          if ($scope.currentDrilldown !== geoLocation) {
              $scope.currentDrilldown = geoLocation;
              $scope.selectedCountry = countryCode;
              config.filterAndDrilldown.hasDrilldown = true;
              config.filterAndDrilldown.drilldown = {
                  type: filterBy + '=',
                  value: geoLocation
              };
          } else {
              config.filterAndDrilldown.hasDrilldown = false;
              config.filterAndDrilldown.drilldown = {
                  type: filterBy + '=',
                  value: ''
              };
              $scope.currentDrilldown = '';
              $scope.selectedCountry = '';
          }
          $scope.updateReports({
            filterAndDrilldown : angular.copy(config.filterAndDrilldown)
          });
        } else {
          fsConfirm('genericAlert', {
            body: 'can\'t drilldown with ' + filterBy +' "'+geoLocation+'"'
          });
        }
      }

      $scope.filterPageByCity = function (city, countryCode) {
        filterPage(city, 'city', countryCode);
      };

      $scope.filterPageByCountry = function (country, countryCode) {
        filterPage(country, 'country', countryCode);
      };

      $scope.getPageviewsForOthers = function () {
        return $scope.availableMetrics[0].amount - $scope.geographicalTable.totalPageViews;
      };

      $scope.getNewVisotrForOthers = function () {
        return $scope.availableMetrics[0].amount - $scope.geographicalTable.totalPageViews;
      };

      $scope.loadGeoData = function() {
        $scope.isLoading = true;
        var query = {
          startDate: config.selectedDates[0],
          endDate: config.selectedDates[1],
          metrics: 'pageViewVsNewVisitorCount',
          dimensions: 'country,countryCode,city,latitude,longitude',
          maxResult: 100
        };
        var defaultFilter;
        if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type === 'eventName=') {
          defaultFilter = undefined;
        }
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : true
        });
        // use minute aggregates for now or yesterday
        if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
            _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
          query.minutesWidth = 1440;
        }
        $scope.geographicalTable = {};
        var promises = [];
        promises.push(WebApiUtils.executeQueryReturnPromise(config.selectedSite.propertyId, query));
        delete  query.maxResult;
        query.dimensions = 'propertyId';
        promises.push(WebApiUtils.executeQueryReturnPromise(config.selectedSite.propertyId, query));
        $q.all(promises).then(function (arrayOfResults) {
          TrafficChartDashboard.formGeographicalData(arrayOfResults, function (geographicalData) {
            $scope.isLoading = false;
            $scope.geographicalTable = geographicalData;
            $scope.geographicalTable.countries = _.sortBy($scope.geographicalTable.countries, function(d) {
              return -Number(d.eventcount);
            });
            $scope.geographicalTable.cities = _.sortBy($scope.geographicalTable.cities, function(d) {
              return -Number(d.eventcount);
            });
            $scope.charts.geographical.data = {
              countries: geographicalData.countries,
              cities: TrafficChartDashboard.formBubblesArayForCities(geographicalData.cities, geographicalData.totalPageViews)
            };
          });
        });
      };
    }]);
