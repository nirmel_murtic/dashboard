'use strict';

angular.module('analyze')
  .controller('TrafficSourcesController', ['$rootScope', '$scope', '$timeout', 'Webanalytics.WebApiUtils', 'TrafficChartDashboard', 'config',  
  function ($rootScope, $scope, $timeout, WebApiUtils, TrafficChartDashboard, config) {

    $scope.currentFilters = _.filter(config.filterAndDrilldown.filters, function(filter) {
      return filter.type === 'referal='; 
    });

    $scope.isFilteredBy = function (referal) {
      referal = referal === 'Direct' ? '{empty}' : referal;
      for (var i = 0; i < $scope.currentFilters.length; i++) {
        if ($scope.currentFilters[i].value === referal) {
          return true;
        }
      }
      return false;
    };

    $scope.getSourceIcon = function (referal) {
      if (angular.isDefined(referal)) {
        if (referal.indexOf('Direct') !== -1) {
          return 'fa-male source-direct-icon-padding';
        } else if (referal.indexOf('facebook') !== -1) {
          return 'fa-facebook-square';
        } else if (referal.indexOf('twitter') !== -1) {
          return 'fa-twitter-square';
        } else if (referal.indexOf('linkedin') !== -1) {
          return 'fa-linkedin-square';
        } else if (referal.indexOf('link') !== -1) {
          return 'fa-envelope-square';
        } else {
          return 'fa-question-circle';
        }
      } else {
        return 'fa-question-circle';
      }
    };

    function deleteFromArray(array, deviceType) {
      var index = -1;
      for (var i = 0; i < array.length; i++) {
        if (array[i].value === deviceType) {
          index = i;
        }
      }
      if (index !== -1) {
        array.splice(index, 1);
      }
      return array;
    }

    $scope.filterPage = function (referal) {
      if (referal !== 'other') {
        referal = referal === 'Direct' ? '{empty}' : referal;
        if ($scope.isFilteredBy(referal) === true) {
          $scope.currentFilters = deleteFromArray($scope.currentFilters, referal);
          config.filterAndDrilldown.filters = deleteFromArray(config.filterAndDrilldown.filters, referal);
        } else {
          if ($scope.currentFilters.length >= 4) {
            config.filterAndDrilldown.filters = _.difference(config.filterAndDrilldown.filters, $scope.currentFilters);
            $scope.currentFilters.length = 0;
          } else {
            var filter = {
              type: 'referal=',
              value: referal
            };
            $scope.currentFilters.push(filter);
            config.filterAndDrilldown.filters.push(filter);
          }
        }
        config.filterAndDrilldown.hasFilter = config.filterAndDrilldown.filters.length !== 0;
        $scope.updateReports({
          filterAndDrilldown : angular.copy(config.filterAndDrilldown)
        });
      }
    };

    function getSourcesData(property, query) {
      WebApiUtils.executeQuery(property, query, function (result) {
        TrafficChartDashboard.formSourcesData(result, function (sourceData) {
          $scope.charts.sources = sourceData;
          $scope.Math = Math;
          $scope.isLoading = false;
        });
      });
    }

    $scope.loadReferalData = function() {
      $scope.isLoading = true;
      var query = {
        startDate: config.selectedDates[0],
        endDate: config.selectedDates[1],
        metrics: 'eventCount',
        dimensions: 'propertyId,referal'
      };
      var defaultFilter = 'eventName=Page load';
      if (config.filterAndDrilldown.hasDrilldown && config.filterAndDrilldown.drilldown.type === 'eventName=') {
        defaultFilter = undefined;
      }
      query = $scope.getQueryFilter(query, config, defaultFilter, {
        includePlatforms : true, 
        includeSources : false,
        includeDrilldown : true
      });
      // use minute aggregates for now or yesterday
      if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
          _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
        query.minutesWidth = 1440;
      }
      getSourcesData(config.selectedSite.propertyId, query);
    };

  }]);




