'use strict';

angular.module('analyze')
  .controller('TrafficChartController', ['$rootScope', '$scope', '$q', '$timeout', '$filter', 'Webanalytics.WebApiUtils', 'TrafficChartDashboard', 'config', 
  function ($rootScope, $scope, $q, $timeout, $filter, WebApiUtils, TrafficChartDashboard, config) {

    $scope.isLoading = false;
    $scope.currentFilter = '';
    // init chart bindings 


    $scope.loadChartData = function() {

      if(!config.reloadChart) {
        return;
      }

      $scope.isLoading = true;
      var query = {
        startDate: config.selectedDates[0],
        endDate: config.selectedDates[1],
        dimensions: 'monthOfYear,day,year,propertyId'
      };
      var datesLabel = '';
      if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
          _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
        query.minutesWidth = 1440;
        datesLabel = 'OneDayDifference';
        query.dimensions = 'hour,monthOfYear,day,year,propertyId';
      } 
      var count = $scope.charts.traffic.metrics.length;
      var promises = [];
      var defaultFilter;
      /*$scope.charts.traffic.data = [];*/
      for (var i = 0; i < count; i++) {
        query.metrics = $scope.charts.traffic.metrics[i].value;
        defaultFilter = $scope.charts.traffic.metrics[i].filter;
        query = $scope.getQueryFilter(query, config, defaultFilter, {
          includePlatforms : true, 
          includeSources : true,
          includeDrilldown : true
        });
        promises.push(WebApiUtils.executeQueryReturnPromise(config.selectedSite.propertyId, query));
      }
      $q.all(promises).then(function (arrayOfResults) {
        $scope.charts.traffic.data = null;
        for (var i = 0; i < count; i++) {
          var chartData = TrafficChartDashboard.applyMulti(
            arrayOfResults[i], 
            $scope.charts.traffic.metrics[i].value.toLowerCase(), 
            config.selectedDates[0], 
            config.selectedDates[1], 
            datesLabel
          );
          fillChartData(chartData, $scope.charts.traffic.metrics[i].value.toLowerCase());
        }
        setChartConfiguration(datesLabel !== '');
        $scope.isLoading = false;
      });
    };

    function fillChartData(arrayToAdd, metric) {
      if (!$scope.charts.traffic.data || $scope.charts.traffic.data.length === 0) {
        $scope.charts.traffic.data = arrayToAdd;
      } else {
        for (var i = 0; i < $scope.charts.traffic.data.length; i++) {
          $scope.charts.traffic.data[i][metric] = arrayToAdd[i][metric];
        }
      }
    }

    function setChartConfiguration(useMinuteAggregate) {
      var valueFields = [],
        colors = [],
        labels = [];
      for (var i = 0; i < $scope.charts.traffic.metrics.length; i++) {
        valueFields.push($scope.charts.traffic.metrics[i].value.toLowerCase());
        labels.push($scope.charts.traffic.metrics[i].label);
        colors.push($scope.charts.traffic.metrics[i].color);
      }
      $scope.charts.traffic.config = {
        valueFields: valueFields,
        labels: labels,
        dateFormat: useMinuteAggregate ? $filter('metricDatetime') : $filter('metricDate')
      };
      if ($scope.charts.traffic.metrics[0]) {
        $scope.charts.traffic.config.leftAxis = d3.format($scope.charts.traffic.metrics[0].format);
      }
      if ($scope.charts.traffic.metrics[1]) {
        $scope.charts.traffic.config.leftAxis = d3.format($scope.charts.traffic.metrics[1].format);
      }
    }
  }]);
