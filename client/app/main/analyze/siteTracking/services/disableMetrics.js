'use strict';

angular.module('analyze')
  .service('disableMetrics', function() {
    var metricsForPlatformsSourcesTopPages = [
      'dailyAvgVisitCount', 
      'dailyAvgReturningVisitorCount', 
      'dailyAvgVisitorCount', 
      'dailyAvgPageViewsPerVisitCount'
    ];
    var metricsForEvents = [
      'pageViewCount', 
      'dailyAvgNewVisitorCount', 
      'dailyAvgVisitCount', 
      'dailyAvgReturningVisitorCount', 
      'dailyAvgVisitorCount', 
      'dailyAvgReturningVisitorRate',
      'avgVisitTime', 
      'avgTimeBetweenVisit', 
      'dailyAvgBouncedCount', 
      'bounceRate', 
      'dailyAvgPageViewsPerVisitCount'
    ];
    var metricsForCitiesCountries = [
      'dailyAvgVisitCount', 
      'dailyAvgReturningVisitorCount', 
      'dailyAvgVisitorCount', 
      'dailyAvgPageViewsPerVisitCount'
    ];

    var disabledMetricsForToady = [
      'dailyAvgVisitCount', 
      'dailyAvgReturningVisitorCount', 
      'dailyAvgVisitorCount', 
      'dailyAvgReturningVisitorRate', 
      'avgVisitTime',
      'avgTimeBetweenVisit', 
      'dailyAvgBouncedCount', 
      'bounceRate', 
      'dailyAvgPageViewsPerVisitCount'
    ];

    var metrics = {
      'deviceType=': metricsForPlatformsSourcesTopPages,
      'referal=': metricsForPlatformsSourcesTopPages,
      'eventName=': metricsForEvents,
      'page=': metricsForPlatformsSourcesTopPages,
      'city=': metricsForCitiesCountries,
      'country=': metricsForCitiesCountries
    };

    function disableForConfig(config) {
      if(!config.filterAndDrilldown) {
        return {};
      }
      var output = [], uniqueFilters = [];
      if (config.filterAndDrilldown.hasFilter) {
        uniqueFilters = _.uniq(config.filterAndDrilldown.filters, 'type');
      }
      if (config.filterAndDrilldown.hasDrilldown) {
        uniqueFilters.push(config.filterAndDrilldown.drilldown);
      }
      for (var i = 0; i < uniqueFilters.length; i++) {
        output = _.union(output, metrics[uniqueFilters[i].type]);
      }
      if (_.isEqual(config.selectedDates, config.dateranges.Now) || 
          _.isEqual(config.selectedDates, config.dateranges.Yesterday)) {
        output = _.union(output, disabledMetricsForToady);
      }
      return _.reduce(output, function(memo, metric) {
        memo[metric] = true;
        return memo;
      }, {});
    }

    return disableForConfig;

  });
