'use strict';

angular.module('analyze')
  .service('reportsConfig', function(SitetrackingStateErrors, ReportDateRanges, SitetrackingMetricsDef, pixelService, $q, $auth, fsUtils) {
  
    function Resolve(user) {
      //jshint newcap: false
      this.dateranges = ReportDateRanges();
      this.sitesPromise = user.currentTeam.pixels ? 
        $q.when({ URLS: user.currentTeam.pixels}) : pixelService.getPixelsAsync(user.currentTeam.id); 
    }

    Resolve.prototype = {
      resolve : function(stateParams) {
        var resolve = fsUtils.promise.settle([
          this.resolveDates(stateParams),
          this.resolveSites(stateParams),
          this.resolveMetrics(stateParams)
        ]); 

        return resolve.then(function(results) {
          // if either dates or sites resolve fails
          if(results[0].reason || results[1].reason || results[2].reason) {
            return $q.reject(_.merge(results[0].reason || {}, 
                                     results[1].reason || {},
                                     results[2].reason || {}));
          } 
          return _.merge(results[0].value, results[1].value, results[2].value);
        });

      },

      datesUrlMapping : {
        'now' : 'Now',
        'yesterday' : 'Yesterday',
        'last7' : 'Last 7 Days',
        'last30': 'Last 30 Days'
      },

      resolveMetrics : function(stateParams) {
        var error, correctedParams;
        if(stateParams.metrics !== null) {
          var metrics = stateParams.metrics.split(',').map(_.trim);
          if(metrics.length === 2 && metrics[1] in SitetrackingMetricsDef &&
                                     metrics[0] in SitetrackingMetricsDef) {
            return $q.when({
              activeMetrics : metrics
            });
          }
          if(metrics.length === 1 && metrics[0] in SitetrackingMetricsDef) {
            return $q.when({
              activeMetrics : metrics
            });
          }
        }
        error = {};
        correctedParams = _.assign({}, stateParams, {
          metrics : 'pageViewCount'
        });
        error[SitetrackingStateErrors.INVALID_PARAMS] = {
          correction : correctedParams
        };
        return $q.reject(error);
      },

      resolveDates : function(stateParams) {

        var error, correctedParams;

        var defaultRange, defaultLabel;
        var savedSettings = $auth.retrieveData('sitetracking-settings') || {};
        defaultRange = savedSettings.daterange || 'last7';
        defaultLabel = this.getDatesLabelFromParam(defaultRange);

        var label;
        // daterange has been specified in url params
        if(stateParams.daterange !== null) {

          if(stateParams.daterange === 'custom' && stateParams.customDates !== null) {
            return $q.when({
              selectedDates : stateParams.customDates,
              dateranges : this.dateranges
            });
          }

          label = this.getDatesLabelFromParam(stateParams.daterange);
          // invalid daterange param specified in url
          // return a INVALID_PARAMS error (to be recovered from the correction provided)
          if(label === null) {
            error = {};
            correctedParams = _.assign({}, stateParams, {
              daterange : defaultRange
            });
            error[SitetrackingStateErrors.INVALID_PARAMS] = {
              correction : correctedParams
            };
            return $q.reject(error);
          }
        } else {
          label = defaultLabel;
        }

        return $q.when({
          selectedDates : this.dateranges[label],
          dateranges : this.dateranges
        });
      },

      resolveSites : function(stateParams) {
        return this.sitesPromise.then(function(pixelData) {
          var error = null;
          var sites = pixelData.URLS;

          // if user has no sites registered, reject with NO_SITES error
          // NO_STIES error will be handled we can recover from it
          if(!sites || !sites.length) {
            error = {};
            error[SitetrackingStateErrors.NO_SITES] = true;
            return $q.reject(error);
          }

          var defaultSite;
          var savedSettings = $auth.retrieveData('sitetracking-settings') || {};
          var favSite = savedSettings.site;
          defaultSite = favSite ? (_.find(sites, { host: favSite }) || sites[0]) : sites[0]; 

          var selectedSite, correctedParams;
          // host has been specified in stateParams / url
          if(stateParams.host !== null) {
            selectedSite = _.find(sites, { host: stateParams.host }); 
            // stateParams specified a invalid site host, we reject with INVALID_PARAMS error
            // and provide a correction (using either saved sites or first site in the list of 
            // sites, this error can be recovered from 
            if(!selectedSite) {
              error = {};
              correctedParams = _.assign({}, stateParams, {
                host : defaultSite.host
              });
              error[SitetrackingStateErrors.INVALID_PARAMS] = {
                correction : correctedParams
              };
              return $q.reject(error);
            }
          } else {
            error = {};
            correctedParams = _.assign({}, stateParams, {
              host : defaultSite.host
            });
            error[SitetrackingStateErrors.INVALID_PARAMS] = {
              correction : correctedParams
            };
            return $q.reject(error);
          }

          // successfully resolves to site data
          return {
            sites : sites,
            selectedSite : selectedSite
          };

        }, function(err) {
          // if pixelService http call failed, reject with NO_SITES error and HTTP_ERROR
          var error = {};
          error[SitetrackingStateErrors.NO_SITES] = true;
          error[SitetrackingStateErrors.HTTP_ERROR] = err;
          return $q.reject(error);
        });
      },
      getDatesLabelFromParam : function(param) {
        return this.datesUrlMapping[param] || null;
      } 

    };

    return {
      resolve : function(user, params) {
        var resolver = new Resolve(user);
        return resolver.resolve(params);
      }
    };
    
  });
