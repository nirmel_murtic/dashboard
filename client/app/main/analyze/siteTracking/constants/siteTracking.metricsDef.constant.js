'use strict';

angular.module('analyze')
  .constant('SitetrackingMetricsDef', {
    'pageViewCount' : {
      label: 'Total Pageviews', 
      value: 'pageViewCount', 
      disabled: false, 
      checked: false, 
      format: '0d'
    },
    'dailyAvgNewVisitorCount' : {
      label: 'Daily Avg. New Visitors',
      value: 'dailyAvgNewVisitorCount',
      disabled: false,
      checked: false,
      format: '0d'
    },
    'customEventCount': {
      label: 'Total Tracked Events', 
      value: 'customEventCount', 
      disabled: false, 
      checked: false, 
      format: '0d'
    },
    'dailyAvgVisitCount' : {
      label: 'Daily Avg. Visits', 
      value: 'dailyAvgVisitCount', 
      disabled: false, 
      checked: false, 
      format: '0d'
    },
    'dailyAvgReturningVisitorCount' : {
      label: 'Daily Avg. Retuning Visitors',
      value: 'dailyAvgReturningVisitorCount',
      disabled: false,
      checked: false,
      format: '0d'
    },
    'dailyAvgVisitorCount' : {
      label: 'Daily Avg. Unique Visitors',
      value: 'dailyAvgVisitorCount',
      disabled: false,
      checked: false,
      format: '0d'
    },
    'dailyAvgReturningVisitorRate' : {
      label: 'Avg. Return Rate',
      value: 'dailyAvgReturningVisitorRate',
      disabled: false,
      checked: false,
      convert: 'percentage',
      format: '0d'
    },
    'avgVisitTime' : {
      label: 'Avg. Time on Site',
      value: 'avgVisitTime',
      disabled: false,
      checked: false,
      convert: 'time',
      format: '0d'
    },
    'avgTimeBetweenVisit' : {
      label: 'Avg. Time between Visits',
      value: 'avgTimeBetweenVisit',
      disabled: false,
      checked: false,
      convert: 'time',
      format: '0d'
    },
    'dailyAvgBouncedCount': {
      label: 'Daily Avg. Bounces', 
      value: 'dailyAvgBouncedCount', 
      disabled: false, 
      checked: false, 
      format: '0d'
    },
    'bounceRate' : {
      label: 'Bounce Rate', 
      value: 'bounceRate', 
      convert: 'percentage', 
      disabled: false, 
      checked: false, 
      format: '0d'
    },
    'dailyAvgPageViewsPerVisitCount' : {
      label: 'Avg. Pageviews per visit',
      value: 'dailyAvgPageViewsPerVisitCount',
      disabled: false,
      checked: false,
      format: '0d'
    }
  });
