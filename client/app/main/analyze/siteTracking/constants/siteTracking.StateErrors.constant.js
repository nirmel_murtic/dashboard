'use strict';

angular.module('analyze')
  .constant('SitetrackingStateErrors', {
    NO_SITES : 'NO_SITES',
    HTTP_ERROR : 'HTTP_ERROR',
    INVALID_PARAMS : 'INVALID_PARAMS'
  });
