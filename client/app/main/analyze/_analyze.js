'use strict';

angular.module('analyze', ['main'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.analyze', {
        url: '/reports',
        abstract : true,
        permissions: '<LOGGED_IN> && <ANALYZE>',
        data: {
          displayName: 'Reports',
          breadcrumbState : 'main.analyze'
        },
        resolve: {
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/analyze/analyze');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
