'use strict';

angular.module('analyze')
  .service('StatusReportStatsUtils', function(parseKPIFormula, parseDatumPath, StatusReportKPIs) {

    // data is a object of key value pairs
    // this function ranks data, computes the percentages for each key
    // and outputs an array
    function rankAndNormalize(data, accessor, keyName) {
      accessor = accessor || _.identity;
      keyName = keyName || '$$key';
      
      var total = _.reduce(data, function(acc, item) {
        return acc + (accessor(item) || 0);
      }, 0);

      return _(data).map(function(item, key) {
        var d =  {
          value : accessor(item),
          percentage : (accessor(item) || 0) / total 
        }; 
        d[keyName] = key;
        return d;
      }).sortBy(function(x) {
        return -x.value;
      })
      .value();
    }

    function extractKPIDatum(datum, kpis) {
      kpis = _.isArray(kpis) ? kpis : [kpis];
      var result = {};
      _.each(kpis, function(kpi) {
        result[kpi.key] = parseDatumPath(kpi.datumPath, _.isUndefined(kpi.na) ? 0 : kpi.na)(datum);
      });
      result.date = new Date( +datum.endTime );
      return result;
    }

    function extractKpiTs(tsJson, kpis) {
      return _.map(tsJson, function(datum) {
        return extractKPIDatum(datum, kpis);
      });
    }

    function extractRawKpis(json) {
      // extract whatever we can
      var rawKpis = _.reduce(StatusReportKPIs, function(kpis, kpi) {
        if(kpi.datumPath) {
          kpis.push(kpi);
        } 
        if(kpi.breakdown) {
          _.each(kpi.breakdown, function(bKpi) {
            if(bKpi.datumPath) {
              kpis.push(bKpi);
            }
          });
        }
        return kpis;
      }, []);
      return extractKpiTs(json, rawKpis);
    }

    function computeDerivedDatum(kpi, datum) {
      var val = parseKPIFormula(kpi.formula)(datum); 
      datum[kpi.key] = val;
      return datum;
    }

    function computeDerivedKpis(data) {
      var derivedKpis = _.reduce(StatusReportKPIs, function(kpis, kpi) {
        if(kpi.formula) {
          kpis.push(kpi);
        } 
        if(kpi.breakdown) {
          _.each(kpi.breakdown, function(bKpi) {
            if(bKpi.formula) {
              kpis.push(bKpi);
            }
          });
        }
        return kpis;
      }, []);
      _.each(derivedKpis, function(kpi) {
        _.each(data, function(datum) {
          computeDerivedDatum(kpi, datum);
        });
      });
      return data;
    } 

    // aggregation over time

    function aggregateRawKPI(data, kpi) {
      return _.reduce(data, function(acc, datum) {
        return acc + datum[ kpi.key ];
      }, 0);
    }

    function aggregateRawKPIs(data) {
      var result = {};
      var rawKpis = _.reduce(StatusReportKPIs, function(kpis, kpi) {
        if(kpi.datumPath) {
          kpis.push(kpi);
        } 
        if(kpi.breakdown) {
          _.each(kpi.breakdown, function(bKpi) {
            if(bKpi.datumPath) {
              kpis.push(bKpi);
            }
          });
        }
        return kpis;
      }, []);
      _.each(rawKpis, function(kpi) {
        result[kpi.key] = aggregateRawKPI(data, kpi);
      });
      return result;
    }

    function computeAggregateKPIs(value) {
      var derivedKpis = _.reduce(StatusReportKPIs, function(kpis, kpi) {
        if(kpi.formula) {
          kpis.push(kpi);
        } 
        if(kpi.breakdown) {
          _.each(kpi.breakdown, function(bKpi) {
            if(bKpi.formula) {
              kpis.push(bKpi);
            }
          });
        }
        return kpis;
      }, []);
      _.each(derivedKpis, function(kpi) {
        computeDerivedDatum(kpi, value);
      });
      return value;
    }

    return {
      rankAndNormalize : rankAndNormalize,
      extractAllKpiTimeseries : function(json) {
        var data = extractRawKpis(json); 
        return _.sortBy(computeDerivedKpis(data), 'date');
      },
      computeAggregateKPIs : function(data) {
        var result = aggregateRawKPIs(data);
        return computeAggregateKPIs(result);
      }
    };
  
  })
  .factory('parseDatumPath', function() {
    function accessField(f, d) {
      return d[f];
    }
    /**
     * accessArrayItem
     *
     * @param keyVal
     * @param d
     * @return {* | undefined}
     */
    function accessArrayItem(keyVal, d) {
      if(!_.isArray(d)) {
        throw 'Error accessing field, input is not Array';
      }
      return _.find(d, keyVal);
    }
    /**
     * parseDatumPath 
     *
     * @param path - {string} : path string
     *  - note: object literal path component needs to be valid JSON, i.e. key name should be quoted string
     * @param naVal - {any} : NA value to return when encounters undefiend 
     *
     * @example
     * var f = parseDatumPath('school/students/{ "name" : "Joe" }/age', 'N/A');
     * var joe_age = f({
     *   school : {
     *     name : "Harvard",
     *     students : [
     *       { name: "Joe", age: 21 },
     *       { name: "Jane", age: 22 }
     *     ]   
     *   }
     * });
     * // joe_age -> 21
     *
     * @return {function}
     */
    return _.memoize(function (path, naVal) {
      naVal = arguments.length > 1 ? naVal : null;
      var parts = path.split('/').reverse();
      var query;
      var accessors = [];
      while (query = parts.pop(), query) {
        query = _.trim(query);
        if(_.startsWith(query, '{')) {
          query = JSON.parse(query);
        } 
        if(_.isString(query) || (_.isNumber(query) && query >= 0)) {
          accessors.push(accessField.bind(null, query));
        } else if(_.isObject(query)) {
          accessors.push(accessArrayItem.bind(null, _.assign({}, query)));
        } else {
          throw 'Invalid JSON path';
        }
      }
      return function (datum) {
        var value = datum;
        for(var i=0; i<accessors.length; i++) {
          value = accessors[i](value);
          if(_.isUndefined(value)) {
            return naVal;
          }
        }    
        return value;
      };
    }, function (path, naVal) {
      return path + '{' + naVal.toString();
    });
  })
  .factory('parseKPIFormula', function($parse) {
    return _.memoize(function(formula) {
      return $parse(formula);
    });
  });


