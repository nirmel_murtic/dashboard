'use strict';

angular.module('analyze')
  .service('StatusReportWebApiUtils', function($http, $q, apiUrl, StatusReportStatsUtils) {
    var noData = [];

    function getTotalFans(fbPageId) {
      var url = apiUrl + '/api/v2/monitor/recent/' + fbPageId + '?periods=lifetime&metrics=page_fans';
      return $http.get(url).then(function(res) {
        try {
          var datum = res.data.data.insights;
          if(_.size(datum)) {
            datum = datum[ _.keys(datum)[0] ];
            return datum.insights[0].value;
          } else {
            return 0;
          }
        } catch (err) {
          return NaN;
        }
      }, function() {
        return $q.reject('Error getting page fans.');
      });
    }

    function getTimeseries(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/timeseries/' + fbPageId;
      // var url = 'app/main/analyze/statusReport/dummyData/ts.json';
      var params = {
        from : from.getTime(),
        to : to.getTime(),
        periods : 'day',
        metrics : [
          'site_visits','page_views','new_visitors','total_tracked_events', 'page_impressions', 'page_impressions_organic', 'page_engaged_users', 'page_fan_adds', 'page_fan_removes', 'page_impressions_paid', 'page_impressions_viral', 'page_positive_feedback_by_type', 'page_consumptions_unique', 'page_impressions_unique', 'page_impressions_paid_unique', 'page_impressions_organic_unique', 'page_impressions_viral_unique', 'page_negative_feedback_by_type'].toString()
      };
      return $http.get(url, {
        params : params
      }).then(function(res) {
        return StatusReportStatsUtils.extractAllKpiTimeseries(res.data.data.insights);
      }, function() {
        return $q.reject({
          msg : 'Error Getting ts Breakdown'
        });
      });
    }

    function getBestTimes(fbPageId, date, timezoneOffset) {
      date = date || new Date();
      timezoneOffset = timezoneOffset || Math.floor( date.getTimezoneOffset() / 60 );
      var url = apiUrl + '/api/v2/engage/page/bestTime/' + fbPageId;
      var params = {
        timestamp : date.getTime(),
        timezoneOffset : timezoneOffset
      };
      return $http.get(url, {
        params : params
      }).then(function(res) {
        try {
          return res.data.data.periods;
        } catch(err) {
          return [];
        }
      }, function() {
        return [];
      });
    }

    function getCountryBreakdown(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/demographics/country/' + fbPageId;
      return $http.get(url, {
        params : {
          from : from.getTime(),
          to : to.getTime()
        }
      }).then(function(res) {
        try {
          return StatusReportStatsUtils.rankAndNormalize(res.data.data.values, null, 'country');
        } catch (err) {
          return noData;
        }
      }, function() {
        return $q.reject({
          msg : 'Error Getting Country Breakdown'
        });
      });
    }
    // Publi Apis
    function getCityBreakdown(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/demographics/city/' + fbPageId;
      return $http.get(url, {
        params : {
          from : from.getTime(),
          to : to.getTime()
        }
      }).then(function(res) {
        try {
          // dummy data for now
          return StatusReportStatsUtils.rankAndNormalize(res.data.data.values, function(d) {
            return d.count;
          }, 'city');
        } catch (err) {
          return noData;
        }
      }, function() {
        return $q.reject({
          msg : 'Error Getting City Breakdown'
        });
      });
    }

    function getPosts(fbPageId, params) {
      var url = apiUrl + '/api/v2/monitor/posts/' + fbPageId;
      params.from = params.from.getTime();
      params.to = params.to.getTime();
      return $http.get(url, {
        params : params
      }).then(function(res) {
        return res.data.data;
      }, function() {
        return $q.reject({
          msg : 'Error Getting posts'
        });
      });
    }

    function getGenderBreakdown(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/demographics/gender/' + fbPageId;
      return $http.get(url, {
        params : {
          from : from.getTime(),
          to : to.getTime()
        }
      }).then(function(res) {
        try {
          return StatusReportStatsUtils.rankAndNormalize(res.data.data.values, null, 'gender');
        } catch (err) {
          return noData;
        }
      }, function() {
        return $q.reject({
          msg : 'Error Getting Gender Breakdown'
        });
      });
    }

    function getPostsBreakdown(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/posts/' + fbPageId;
      return $http.get(url, {
        params : {
          from : from.getTime(),
          to : to.getTime()
        }
      }).then(function(res) {
        try {
          return res.data.data.data || noData;
        } catch (err) {
          return noData;
        }
      }, function() {
        return $q.reject({
          msg : 'Error Getting Posts Breakdown'
        });
      });
    }

    function getTopMediaBreakdown(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/posts/' + fbPageId;
      return $http.get(url, {
        params : {
          from : from.getTime(),
          to : to.getTime(),
          filterType: 'photo',
          sortType: 'desc',
          sortColumn: 'engagedUsers'
        }
      }).then(function(res) {
        try {
          return res.data.data.data || noData;
        } catch (err) {
          return noData;
        }
      }, function() {
        return $q.reject({
          msg : 'Error Getting Top Media Breakdown'
        });
      });
    }

    function getMediaBreakdown(fbPageId, from, to) {
      var url = apiUrl + '/api/v2/monitor/recent/' + fbPageId;
      return $http.get(url, {
        params : {
          periods: 'days_28',
          metrics: 'page_consumptions_by_consumption_type',
          from : from.getTime(),
          to : to.getTime()
        }
      }).then(function(res) {
        try {
          var result = null;
          for (var element in res.data.data.insights) {
            if (res.data.data.insights.hasOwnProperty(element)) {
              result = res.data.data.insights[element].insights[0].valueMap;
            }
          }

          var totalValue = 0;

          angular.forEach(result, function(item) {
            totalValue = totalValue + item.value;
          });

          if(totalValue > 0) {
            angular.forEach(result, function(item) {
              item.percentage = item.value/totalValue;
            });
          }

          return result;
        } catch (err) {
          return noData;
        }
      }, function() {
        return $q.reject({
          msg : 'Error Getting Media Breakdown'
        });
      });
    }

    return {
      getTotalFans : getTotalFans,
      getBestTimes : getBestTimes,
      getCountryBreakdown : getCountryBreakdown,
      getCityBreakdown : getCityBreakdown,
      getPosts : getPosts,
      getGenderBreakdown : getGenderBreakdown,
      getTimeseries : getTimeseries,
      getPostsBreakdown : getPostsBreakdown,
      getMediaBreakdown: getMediaBreakdown,
      getTopMediaBreakdown: getTopMediaBreakdown
    };
  });
