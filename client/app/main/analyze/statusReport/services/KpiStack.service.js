'use strict';

angular.module('analyze')
  .service('KpiStack', function() {

    function KpiStack(capacity, mode) {
      this.capacity = capacity || 2;
      this.mode = mode || 'stack';
      this.stack_ = [];
    }

    KpiStack.prototype = {
      size : function() {
        return this.stack_.length;
      },
      isFull : function() {
        return this.size() >= this.capacity;
      },
      get : function(i) {
        return this.stack_[i || 0];
      },
      has : function(kpi) {
        return !!_.find(this.stack_, kpi);
      },
      add : function(kpi) {
        if(this.isFull()) {
          // if mode is stack, we do LIFO, otherwise FIFO (a queue)
          this.stack_[ this.mode === 'stack' ? 'pop' : 'shift']();
        }
        this.stack_.push(kpi);
        return this;
      },
      remove : function(kpi) {
        _.remove(this.stack_, function(x) {
          return x === kpi;
        }); 
        return this;
      },
      toggle : function(kpi, opts) {
        if(this.has(kpi)) {
          if(!opts || !opts.allowEmpty) {
            return this.size() > 1 ? this.remove(kpi) : this; 
          } 
          return this.remove(kpi);
        } else {
          return this.add(kpi);
        }
      } 
    };

    return KpiStack;

  });
