'use strict';

angular.module('analyze')
  .controller('StatusReportActivityBreakdownCtrl', function($scope) {

    $scope.activityBreakdownOptions = {
      enableGridMenu: false,
      selectionRowHeaderWidth: 60,
      rowHeight: 62,
      showGridFooter: false
    };

    if($scope.postsBreakdown) {
      $scope.postsBlob = $scope.postsBreakdown;
    }

    $scope.activityBreakdownOptions.data = [];

    $scope.mergePostsInformations = function(blobData) {

      for(var i=0; i<blobData.length;i++) {
        if(blobData[i].uniqueReachCount === 0 || blobData[i].engagedUsers === 0) {
          blobData[i].engagementRate = 0;
        } else {
          blobData[i].engagementRate = blobData[i].engagedUsers/blobData[i].uniqueReachCount;
          blobData[i].engagementRate = blobData[i].engagementRate.toFixed(2);
        }
        $scope.activityBreakdownOptions.data.push({
          allPosts: blobData[i].message,
          image: blobData[i].picture,
          date : blobData[i].createdAt,
          engagementRate: blobData[i].engagementRate,
          impressions: blobData[i].reachCount,
          engagement: blobData[i].engagedUsers,
          negativeEngagement: blobData[i].postNegativeFeedback,
          comments: blobData[i].commentsCount,
          likes: blobData[i].likesCount,
          shares: blobData[i].sharesCount,
          clicks: blobData[i].postConsumptions
        });
      }
    };

    if($scope.postsBlob) {
      $scope.mergePostsInformations($scope.postsBlob);
    }

    $scope.activityBreakdownOptions.columnDefs = [
      {
        name: 'allPosts',
        displayName: 'All Posts',
        enableColumnMenu: false,
        width: '40%',
        cellTemplate: '<div>' +
        '<div class="ui-grid-cell-contents">' +
        '<img ng-if="row.entity.image" class="cell-image" ng-src="{{row.entity.image}}"/>' +
        '<img ng-if="!row.entity.image" class="cell-image" ng-src="assets/images/no-photo.png"/>' +
        '{{row.entity[col.field] | stringLimiter:max:125}}' +
        '</div>' +
        '</div>'
      },
      {
        name: 'engagementRate',
        displayName: 'Engagement Rate',
        enableColumnMenu: false,
        width: '15%'
      },
      {
        name: 'impressions',
        displayName: 'Impressions',
        enableColumnMenu: false,
        width: '15%'
      },
      {
        name: 'engagement',
        displayName: 'Engagement',
        enableColumnMenu: false,
        width: '15%'
      },
      {
        name: 'negativeEngagement',
        displayName: 'Negative Engagement',
        enableColumnMenu: false,
        width: '15%'
      }
    ];

  });
