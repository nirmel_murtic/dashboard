'use strict';

angular.module('analyze')
  .controller('StatusReportCtrl', function($scope, $state, ngDialog, CreatePost, timeFrames, timeFrame, fbPages, totalFans, bestPostTimes, timeseries, audienceBreakdown, projectService) {
    $scope.timeFrames = timeFrames;

    $scope.totalFans = totalFans;

    // dot pattern
    $scope.activeEntities = {
      timeFrame : timeFrame,
      facebookPages : fbPages.pages,
      facebookPage : fbPages.active
    };
    $scope.daterangePickerOpts = {
      ranges : timeFrames,
      maxDate : new Date(),
      minDate : new Date(2004, 1, 1)
    };
    $scope.updateTimeframe = function(timeFrame) {
      var params = {};
      if(_.isEqual(timeFrame, timeFrames.Now)) {
        params.dateRangePreset = 'now';
        params.timeFrame = null;
      } else if (_.isEqual(timeFrame, timeFrames['Last 7 Days'])) {
        params.dateRangePreset = 'last7';
      } else if (_.isEqual(timeFrame, timeFrames['Last 30 Days'])) {
        params.dateRangePreset = 'last30';
      } else {
        params.dateRangePreset = 'custom-daterange';
        params.timeFrame = timeFrame;
      }
      $state.go('.', params, {
        reload : true,
        location : 'replace'
      });
    };
    $scope.selectFacebookPage = function(page) {
      $scope.activeEntities.facebookPage = page;
      $state.go('main.analyze.status.date.page', { fbPageId : page.pagePageId });
    };

    $scope.boostPost = function(post){
      if(!post || $scope.boostPostDisabled){
        return;
      }


      var fbPage = $scope.activeEntities.facebookPage;
      var currentProject = $scope.fractalAccountInfo.currentTeam.currentProject;
      var postEngagementGoal = _.find(currentProject.goals, function(goal){
        if (goal.goal.name === 'POST_ENGAGEMENT' && post.pageId === goal.page.pagePageId) {
          return goal;
        }
      });

      var successCallback = function(){
        $state.go('main.act.campaignCreation', {post: post}, {reload: true});
      };

      var errorCallback = function(response){
        console.log(response);
      };


      if(!postEngagementGoal){
        currentProject.goals.push({goalId: 1, pageId: fbPage.pageId});
        projectService.addOrUpdateProject(currentProject, successCallback, errorCallback);        
      } else {
        $scope.boostPostDisabled = true;
        $state.go('main.act.campaignCreation', {type: 'post', param: post}, {reload: true});
      }

    };

    $scope.timeseries = timeseries;

    $scope.now = new Date();
    $scope.bestTimes = bestPostTimes;
    function sumValues(acc, d) {
      return acc + (d.value || 0);
    }
    if(audienceBreakdown) {
      $scope.countryBreakdown = audienceBreakdown.countryBreakdown;
      $scope.cityBreakdown = audienceBreakdown.cityBreakdown;
      $scope.genderBreakdown = audienceBreakdown.genderBreakdown;
      $scope.postsBreakdown = audienceBreakdown.postsBreakdown;
      $scope.mediaBreakdown = audienceBreakdown.mediaBreakdown;
      $scope.topMediaBreakdown = audienceBreakdown.topMediaBreakdown;

      $scope.countryBreakdownTotal = _.reduce(audienceBreakdown.countryBreakdown, sumValues, 0);
      $scope.cityBreakdownTotal = _.reduce(audienceBreakdown.cityBreakdown, sumValues, 0);
    }
    $scope.postToBoost = $scope.postsBreakdown ? ($scope.postsBreakdown[0] || null) : null;
    if($scope.postToBoost) {
      $scope.postToBoost.date = new Date( +$scope.postToBoost.createdAt);
    }

    if(!fbPages.active && fbPages.pages.length) {
      $scope.selectFacebookPage(fbPages.pages[0]);
    }

  });
