'use strict';

angular.module('analyze')
  .controller('StatusReportEngagementBreakdownCtrl', function($scope, $state, $filter, StatusReportKPIs, ColorDefs, KpiStack, fsUtils, StatusReportStatsUtils) {
    if(!$scope.timeseries) {
      $scope.timeseries = [];
    }
    // console.log($scope.timeseries);
    $scope.kpiToDate = StatusReportStatsUtils.computeAggregateKPIs($scope.timeseries);
    $scope.kpiToDate.fanGrowth = $scope.kpiToDate.fanGrowth / ($scope.totalFans - $scope.kpiToDate.fanGrowth);
    if(__DEV__) {
      window.scopee = $scope;
      window.statusTs = $scope.timeseries;
    }
    // prepare KPIs
    $scope.kpiDefs = StatusReportKPIs;
    $scope.fbKPIs = _.filter(StatusReportKPIs, function(kpi) {
      return kpi.source === 'facebook';
    });
    $scope.siteKPIs = _.filter(StatusReportKPIs, function(kpi) {
      return kpi.source === 'site';
    });
    // this is on parent $scope in StatusReportCtrl
    $scope.activeEntities.selectedKPIs = $scope.activeEntities.selectedKPIs || new KpiStack();
    if($scope.activeEntities.selectedKPIs.size() === 0) {
      $scope.activeEntities.selectedKPIs.add($scope.fbKPIs[0]);
    }
    // chart show/hide logic helpers

    // memoize functions to prevent different instances of keys array being returned which would cause infinite $digest
    $scope.getFormatter = _.memoize(function(kpi, invertNegative) {
      var filterName = kpi.format;
      var f = filterName ? $filter(filterName) : String;
      if(kpi.key !== 'fanGrowth' || !invertNegative) {
        return f;
      } else {
        return function(val) {
          return f(Math.abs(val));
        };
      }
    }, function(k, i) {
      return k.key + ':' +!!i;
    });
    $scope.getKeys = function(arr) {
      return _.map(arr, function(a) {
        return a.key;
      });
    };
    $scope.getLabels = _.memoize(function(arr) {
      return _.map(arr, function(a) {
        return a.label;
      });
    });
    $scope.useStackChart = function() {
      return $scope.activeEntities.selectedKPIs.size() === 1 &&
             $scope.activeEntities.selectedKPIs.get(0).breakdown &&
             $scope.activeEntities.selectedKPIs.get(0).breakdown.length &&
             !$scope.activeEntities.selectedKPIs.has(StatusReportKPIs.fanGrowth);
    };
    $scope.useLineChart = function() {
      return $scope.activeEntities.selectedKPIs.size() === 1 &&
             ($scope.activeEntities.selectedKPIs.has(StatusReportKPIs.fanGrowth) || !$scope.activeEntities.selectedKPIs.get(0).breakdown);
    };
    $scope.useComparisonChart = function() {
      return $scope.activeEntities.selectedKPIs.size() === 2;
    };

    $scope.lineChartKPI = function() {
      if($scope.useLineChart()) {
        $scope.lineChartKPI_ = $scope.activeEntities.selectedKPIs.get(0);
        return $scope.lineChartKPI_;
      } else {
        return $scope.lineChartKPI_ || StatusReportKPIs.fanGrowth;
      }
    };
    $scope.stackChartKPI = function() {
      if($scope.useStackChart()) {
        $scope.stackChartKPI_ = $scope.activeEntities.selectedKPIs.get(0);
        return $scope.stackChartKPI_;
      } else {
        return $scope.stackChartKPI_ || StatusReportKPIs.impressions;
      }
    };
    $scope.comparisonChartKPIs = function() {
      if($scope.useComparisonChart()) {
        if(!$scope.comparisonChartKPI_) {
          $scope.comparisonChartKPI_ = [$scope.activeEntities.selectedKPIs.get(0), $scope.activeEntities.selectedKPIs.get(1)];
        } else {
          $scope.comparisonChartKPI_[0] = $scope.activeEntities.selectedKPIs.get(0);
          $scope.comparisonChartKPI_[1] = $scope.activeEntities.selectedKPIs.get(1);
        }
        return $scope.comparisonChartKPI_;
      } else {
        if(!$scope.comparisonChartKPI_) {
          $scope.comparisonChartKPI_ = [StatusReportKPIs.fanGrowth, StatusReportKPIs.impressions];
        }
        return $scope.comparisonChartKPI_;
      }
    };

    var noData = [];
    $scope.labelForLineChart = _.memoize(function(kpi) {
      if(kpi.key === 'fanGrowth') {
        return [kpi.label, _.find(kpi.breakdown, { key: 'pageLike' }).label, _.find(kpi.breakdown, { key: 'pageUnlikeInverse'}).label];
      }
      return [kpi.label];
    }, function(kpi) {
      return kpi.key;
    });
    $scope.fieldsForLineChart = _.memoize(function(kpi) {
      if(kpi.key === 'fanGrowth') {
        return [kpi.key, 'pageLike', 'pageUnlike'];
      }
      return [kpi.key];
    }, function(kpi) {
      return kpi.key;
    });
    $scope.labelForComparisonChart = _.memoize(function(kpi1, kpi2) {
      if(kpi1 && kpi2) {
        return [kpi1.label, kpi2.label];
      }
      return noData;
    }, function(a, b) {
      if(a && b) {
        return a.key + b.key;
      } else {
        return '_';
      }
    });
    $scope.fieldsForComparisonChart = _.memoize(function(kpi1, kpi2) {
      if(kpi1 && kpi2) {
        return [kpi1.key, kpi2.key];
      }
      return noData;
    }, function(a, b) {
      if(a && b) {
        return a.key + b.key;
      } else {
        return '_';
      }
    });
    $scope.fieldsForStackChart = _.memoize(function(kpi) {
      if(kpi.breakdown) {
        return $scope.getKeys(kpi.breakdown);
      }
      return [kpi.key];
    }, function(k) {
      return k.key;
    });
    $scope.labelForStackChart = _.memoize(function(kpi) {
      return _.map(kpi.breakdown, function(k) {
        return k.label;
      });
    }, function(k) {
      return k.key;
    });

    $scope.fanGrowthColors = [
      ColorDefs.SeaHeavy.toString(),
      ColorDefs.LeafHeavy.toString(),
      ColorDefs.CherryDark.toString()
    ];

    $scope.greenColors = [
      ColorDefs.NatureHeavy.toString(),
      ColorDefs.LeafHeavy.toString(),
      ColorDefs.LeafSoft.toString(),
      ColorDefs.LeafLight.toString()
    ];
    $scope.greenColorsRev = [
      ColorDefs.LeafLight.toString(),
      ColorDefs.LeafLight.toString(),
      ColorDefs.NatureHeavy.toString(),
      ColorDefs.NatureHeavy.toString(),
    ];

    // $scope.greenColors = [
    //  'rgb(84, 81, 166)',
    //  'rgb(242, 128, 159)',
    //  'rgb(61,144,217)',
    //  'rgb(21,191,174)'
    // ];
  });
