'use strict';

angular.module('analyze')
  .config(function ($stateProvider, $urlRouterProvider, resolveTimeframeProvider) {
    $stateProvider
      .state('main.analyze.status', {
        abstract : true,
        permissions: '<LOGGED_IN> && <ANALYZE>',
        url : '/organic',
        resolve : {
          timeFrames : function(ReportDateRanges) {
            // jshint newcap : false
            var tfs = ReportDateRanges();
            delete tfs.Yesterday;
            resolveTimeframeProvider.setTimeframes(tfs);
            return tfs;
          }
        },
        data: {
          displayName: 'Organic',
          active: 'organic'
        }
      })
      .state('main.analyze.status.date', {
        // url: '/{dateRangePreset:' + resolveTimeframeProvider.getUrlRegex() + '$}',
        url: '/:dateRangePreset',
        abstract : true,
        params : {
          // timeFrame is an array of two dates
          timeFrame : null
        },
        resolve : {
          timeFrame : function($stateParams, resolveTimeframe) {
            return resolveTimeframe($stateParams);
          }
        }
      })
      .state('main.analyze.status.date.page', {
        url: '/:fbPageId',
        views: {
          '@main': {
            templateUrl: 'app/main/analyze/statusReport/templates/statusReport.html',
            controller: 'StatusReportCtrl'
          }
        },
        resolve : {
          fbPages : function(auth, Campaign, $stateParams) {
            var pages = Campaign.getFacebookPages(auth.user.currentTeam);
            var activePage = null;
              angular.forEach(pages, function(page) {
                if(page.pagePageId === $stateParams.fbPageId) {
                  activePage = page;
                }
              });
            return {
              pages : pages,
              active : activePage
            };
          },
          totalFans : function($stateParams, StatusReportWebApiUtils) {
            if($stateParams.fbPageId) {
              var data = StatusReportWebApiUtils.getTotalFans($stateParams.fbPageId);
              return data;
            }
          },
          timeseries : function($stateParams, timeFrame, StatusReportWebApiUtils) {
            if($stateParams.fbPageId) {
              var data = StatusReportWebApiUtils.getTimeseries($stateParams.fbPageId, timeFrame[0], timeFrame[1]);
              return data;
            }
          },
          bestPostTimes : function($stateParams, StatusReportWebApiUtils) {
            if($stateParams.fbPageId) {
              return StatusReportWebApiUtils.getBestTimes($stateParams.fbPageId);
            }
          },
          audienceBreakdown : function($q, $stateParams, timeFrame, StatusReportWebApiUtils) {
            var promises;
            if($stateParams.fbPageId) {
              promises = $q.all([
                StatusReportWebApiUtils.getCountryBreakdown($stateParams.fbPageId, timeFrame[0], timeFrame[1]),
                // for now, latitude info is disgarded, but when we need maps, need to revisit
                StatusReportWebApiUtils.getCityBreakdown($stateParams.fbPageId, timeFrame[0], timeFrame[1]),

                StatusReportWebApiUtils.getGenderBreakdown($stateParams.fbPageId, timeFrame[0], timeFrame[1]),
                // get posts for activity breakdown table and top media section
                StatusReportWebApiUtils.getPostsBreakdown($stateParams.fbPageId, timeFrame[0], timeFrame[1]),
                // get engagement by media type
                StatusReportWebApiUtils.getMediaBreakdown($stateParams.fbPageId, timeFrame[0], timeFrame[1]),
                // get posts filtered by image posts for top media
                StatusReportWebApiUtils.getTopMediaBreakdown($stateParams.fbPageId, timeFrame[0], timeFrame[1])
              ]);
              return promises.then(function(values) {
                return {
                  countryBreakdown : values[0],
                  cityBreakdown : values[1],
                  genderBreakdown : values[2],
                  postsBreakdown: values[3],
                  mediaBreakdown: values[4],
                  topMediaBreakdown: values[5]
                };
              });
            }
          }
        }
      });

      $urlRouterProvider.when(/^\/reports\/organic\/*$/i, '/reports/organic/' + resolveTimeframeProvider.getDefault() + '/');
  });
