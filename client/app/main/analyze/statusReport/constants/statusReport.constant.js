'use strict';

angular.module('analyze')
  .constant('StatusReportKPIs', {
    //TO-DO: adjust to ng-translate labels (label.analyze.FanGrowth)
    fanGrowth : {
      key : 'fanGrowth',
      formula: 'pageLike - pageUnlike',
      source : 'facebook',
      format : 'metricVolumeCompact',
      summaryFormat : 'metricPercentage',
      label : 'Fan Growth',
      breakdown : [
        {
          key : 'pageLike',
          datumPath : 'insights/{ "name": "page_fan_adds" }/value',
          label : 'Page Likes'
        },
        {
          key : 'pageUnlike',
          datumPath : 'insights/{ "name": "page_fan_removes" }/value',
          label : 'Page Unlikes'
        },
        {
          key : 'pageUnlikeInverse',
          label : 'Page Unlikes',
          formula: '-pageUnlike'
        }
      ]
    },
    impressions : {
      key : 'impressions',
      datumPath : 'insights/{ "name": "page_impressions" }/value',
      source : 'facebook',
      format : 'metricVolumeCompact',
      label : 'Impressions',
      breakdown : [
        {
          key : 'impsOrganic',
          datumPath : 'insights/{ "name": "page_impressions_organic" }/value',
          label : 'Organic'
        },
        {
          key : 'impsPaid',
          datumPath : 'insights/{ "name": "page_impressions_paid" }/value',
          label : 'Paid'
        },
        {
          key : 'impsViral',
          fbMetricName : 'page_impressions_viral',
          datumPath : 'insights/{ "name": "page_impressions_viral" }/value',
          label : 'Viral'
        }
      ]
    },
    engagement : {
      key : 'engagement',
      formula : 'shares + likes + comments + clicks',
      source : 'facebook',
      format : 'metricVolumeCompact',
      label : 'Post Engagements',
      breakdown : [
        {
          key : 'shares',
          datumPath : 'insights/{ "name": "page_positive_feedback_by_type" }/valueMap/{ "key" : "link" }/value',
          label : 'Shares'
        },
        {
          key : 'likes',
          datumPath : 'insights/{ "name": "page_positive_feedback_by_type" }/valueMap/{ "key" :  "like" }/value',
          label : 'Likes'
        },
        {
          key : 'comments',
          datumPath : 'insights/{ "name": "page_positive_feedback_by_type" }/valueMap/{ "key" : "comment" }/value',
          label : 'Comments'
        },
        {
          key : 'clicks',
          datumPath : 'insights/{ "name": "page_consumptions_unique" }/value',
          label : 'Clicks'
        }
      ]
    },
    reach : {
      key : 'reach',
      datumPath : 'insights/{ "name": "page_impressions_unique" }/value',
      source : 'facebook',
      format : 'metricVolumeCompact',
      label : 'Reach',
      breakdown : [
        {
          key : 'reachOrganic',
          datumPath : 'insights/{ "name": "page_impressions_organic_unique" }/value',
          label : 'Organic'
        },
        {
          key : 'reachPaid',
          datumPath : 'insights/{ "name": "page_impressions_paid_unique" }/value',
          label : 'Paid'
        },
        {
          key : 'reachViral',
          datumPath : 'insights/{ "name": "page_impressions_viral_unique" }/value',
          label : 'Viral'
        }
      ]
    },
    engagedUsers : {
      key : 'engagedUsers',
      format : 'metricVolumeCompact',
      datumPath : 'insights/{ "name": "page_engaged_users" }/value',
      source : 'facebook',
      label : 'Total Engaged Users'
    },
    engagementRate : {
      key : 'engagementRate',
      na : NaN,
      source : 'facebook',
      formula : 'engagement / impressions',
      format : 'metricPercentage',
      label : 'Engagement Rate',
      /*
      breakdown : [
        {
          key : 'engRateOrganic',
          label : 'Organic'
        },
        {
          key : 'engRatePaid',
          label : 'Paid'
        }
      ]
      */
    },
    negEngagement : {
      key : 'negEngagement',
      formula : 'hide + hideAll + reportedSpam',
      source : 'facebook',
      format : 'metricVolumeCompact',
      label : 'Negative Engagements',
      breakdown : [
        {
          key : 'hide',
          datumPath : 'insights/{ "name": "page_negative_feedback_by_type" }/valueMap/{ "key" : "hide_clicks" }/value',
          label : 'Hide'
        },
        {
          key : 'hideAll',
          datumPath : 'insights/{ "name": "page_negative_feedback_by_type" }/valueMap/{ "key" : "hide_all_clicks" }/value',
          label : 'Hide All'
        },
        {
          key : 'reportedSpam',
          datumPath : 'insights/{ "name": "page_negative_feedback_by_type" }/valueMap/{ "key" : "report_spam_clicks" }/value',
          label : 'Reported Spam'
        }
      ]
    },
    visits : {
      key : 'visits',
      label : 'Site Visits',
      format : 'metricVolumeCompact',
      datumPath : 'insights/{ "name": "site_visits" }/value',
      source : 'site'
    },
    pageviews : {
      key : 'pageviews',
      source : 'site',
      format : 'metricVolumeCompact',
      datumPath : 'insights/{ "name": "page_views" }/value',
      label : 'Page Views'
    },
    visitors : {
      key : 'visitors',
      source : 'site',
      format : 'metricVolumeCompact',
      datumPath : 'insights/{ "name": "new_visitors" }/value',
      label : 'New Visitors'
    },
    events : {
      key : 'events',
      source : 'site',
      format : 'metricVolumeCompact',
      datumPath : 'insights/{ "name": "total_tracked_events" }/value',
      label : 'Total Tracked Events'
    }
  });
