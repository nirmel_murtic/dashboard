'use strict';

angular.module('dashboardApp')
  .service('AnalyzeActivityService', function() {
    var _service={
      scope:null,
      activityBuilder:function(_activities, goal){
        goal.activities=[];
          for(var zz=0;zz<_activities.length;zz++){
            goal.activities.push({displayName:_activities[zz].activity.titleText,timeSeries:[],imp:0,clk:0,cnv:0,cost:0,goalId:goal.goalId});
            var _obj={};
            for(var zzz=0;zzz<_activities[zz].activity.timeSeries.length;zzz++){
              for(var zzzz in _activities[zz].activity.timeSeries[zzz]){
                _obj[zzzz]=_activities[zz].activity.timeSeries[zzz][zzzz];
              }
            goal.activities[zz].timeSeries.push(_obj);
            goal.activities[zz].imp+=_activities[zz].activity.timeSeries[zzz].imp;
            goal.activities[zz].clk+=_activities[zz].activity.timeSeries[zzz].clk;
            goal.activities[zz].cnv+=_activities[zz].activity.timeSeries[zzz].cnv;
            goal.activities[zz].cost+=_activities[zz].activity.timeSeries[zzz].cost;
            }
          goal.activities[zz].totalSpent=goal.activities[zz].cost;
          goal.activities[zz].cpa=goal.activities[zz].cost/goal.activities[zz].cnv;
          goal.activities[zz].ctr=goal.activities[zz].clk/goal.activities[zz].imp;
          goal.activities[zz].cvr=goal.activities[zz].cnv/goal.activities[zz].imp;
          }
          this.bindActivitiesToGoal(goal);
      //    scope.activities=goal.activities;
      },
     bindActivitiesToGoal:function(goal){
        var _cost=0, _imp=0, _clk=0, _cnv=0;
          for(var zz=0;zz<goal.activities.length;zz++){
            _imp+=goal.activities[zz].imp;
            _clk+=goal.activities[zz].clk;
            _cnv+=goal.activities[zz].cnv;
            _cost+=goal.activities[zz].cost;
          }
          goal.totalSpent=_cost;
          goal.cpa=_cost/_cnv;
          goal.impressions=_imp;
          goal.clicks=_clk;
          goal.engagements=_cnv;
          goal.ctr=_clk/_imp;
          goal.cvr=_cnv/_imp;
      }
  };
  return _service;
});