'use strict';

angular.module('dashboardApp')
  .factory('TrafficChartDashboard', function (gapFiller) {
    function calculateRadius(eventCount, total) {
      eventCount = parseInt(eventCount);
      var percentages = Math.round((eventCount || 0) / (total || 1) * 100);
      if (percentages === 0) {
        return {
          bubbleSize: eventCount === 0 ? '0em' : '0.1em',
          countryColor: eventCount === 0 ? 'defaultFill' : 'countryColorForOnePercentages'
        };
      } else if (percentages >= 1 && percentages <= 9) {
        return {
          bubbleSize: '0.25em',
          countryColor: 'countryColorForOnePercentages'
        };
      } else if (percentages >= 10 && percentages <= 19) {
        return {
          bubbleSize: '0.325em',
          countryColor: 'countryColorForTenPercentages'
        };
      } else if (percentages >= 20 && percentages <= 29) {
        return {
          bubbleSize: '0.375em',
          countryColor: 'countryColorForTwentyPercentages'
        };
      } else if (percentages >= 30 && percentages <= 39) {
        return {
          bubbleSize: '0.45em',
          countryColor: 'countryColorForThirtyPercentages'
        };
      } else if (percentages >= 40 && percentages <= 49) {
        return {
          bubbleSize: '0.5em',
          countryColor: 'countryColorForFortyPercentages'
        };
      } else if (percentages >= 50 && percentages <= 59) {
        return {
          bubbleSize: '0.575em',
          countryColor: 'countryColorForFiftyPercentages'
        };
      } else if (percentages >= 60 && percentages <= 69) {
        return {
          bubbleSize: '0.625em',
          countryColor: 'countryColorForSixtyPercentages'
        };
      } else if (percentages >= 70 && percentages <= 79) {
        return {
          bubbleSize: '0.675em',
          countryColor: 'countryColorForSeventyPercentages'
        };
      } else if (percentages >= 80 && percentages <= 89) {
        return {
          bubbleSize: '0.725em',
          countryColor: 'countryColorForEightyPercentages'
        };
      } else if (percentages >= 90 && percentages <= 100) {
        return {
          bubbleSize: '0.775em',
          countryColor: 'countryColorForNinetyPercentages'
        };
      }
    }


    function convertUTCDateToLocalDate(date, setMins) {
      date = new Date(date);
      //Local time converted to UTC
      var localOffset = date.getTimezoneOffset() * 60000;
      var localTime = date.getTime();
      date = localTime - localOffset;
      date = new Date(date);
      if (setMins === true) {
        date.setMinutes(0);
      }
      return date;
    }

    return {
      applyMulti: function (result, metric, startDate, endDate, timeRangePeriod) {
        var rawData = result.data.data;
        var sortedData;
        if (rawData.length !== 0) {
          sortedData = _.sortBy(_.map(rawData, function (oldRow) {
            var newRow = {};
            if (timeRangePeriod && timeRangePeriod === 'Today' || timeRangePeriod === 'OneDayDifference') {
              var localTime = new Date(oldRow.year, oldRow.monthofyear - 1, oldRow.day, oldRow.hour, 0);
              newRow.date = convertUTCDateToLocalDate(localTime, true);
            } else {
              newRow.date = new Date(oldRow.year, oldRow.monthofyear - 1, oldRow.day, 0, 0);
            }
            newRow[metric] = oldRow[metric];
            newRow.separator = metric;
            return newRow;
          }), 'date');
        } else {
          var firstElement = {
            date: new Date(startDate),
            separator: metric
          };
          firstElement[metric] = 0;
          var lastElement = {
            date: new Date(endDate),
            separator: metric
          };
          lastElement[metric] = 0;
          sortedData = [firstElement, lastElement];
        }
        var groupedData = gapFiller.formChartData(sortedData, metric, new Date(startDate), new Date(endDate), timeRangePeriod);
        //console.log('chart data', groupedData);
        return groupedData;
      },
      formPlatformData: function (result, callback) {
        var rawData = result.data.data;
        var platformData = {};
        if (rawData.length !== 0) {
          for (var i = 0; i < rawData.length; i++) {
            if (rawData[i].devicetype === '') {
              rawData[i].devicetype = 'Personal computer';
            }
          }
          var sortedData = _(rawData).chain()
            .sortBy('eventcount')
            .reverse()
            .sortBy('devicetype')
            .value();
          var groupedData = _.groupBy(sortedData, 'devicetype');

          var totalEvents = 0;
          angular.forEach(groupedData, function (value, key) {
            platformData[key] = {};
            var platformObject;
            if (value.length > 3) {
              var others = value.splice(3, value.length - 1);
              var sumEventCountForEachPlatform = _.reduce(others, function (memo, row) {
                return memo + row.eventcount;
              }, 0);
              platformObject = {
                devicetype: key,
                eventcount: sumEventCountForEachPlatform,
                os: 'other'
              };
              value.push(platformObject);
            }
            var sumEventCounts = _.reduce(value, function (memo, row) {
              return memo + row.eventcount;
            }, 0);
            platformData[key] = {
              data: value,
              sumEventCounts: sumEventCounts
            };
            totalEvents = totalEvents + sumEventCounts;
          });
          platformData.totalEvents = totalEvents;

          if (platformData.hasOwnProperty('Personal computer')) {
            platformData.PersonalComputer = platformData['Personal computer'];
            delete platformData['Personal computer'];
          }
        } else {
          platformData = {
            totalEvents: 0,
            PersonalComputer: {
              data: [],
              sumEventCounts: 0
            },
            Tablet: {
              data: [],
              sumEventCounts: 0
            },
            Smartphone: {
              data: [],
              sumEventCounts: 0
            }
          };
        }
        //console.log('platformData', platformData);
        callback(platformData);
      },
      formSourcesData: function (result, callback) {
        var rawData = result.data.data;
        var sourcesData = {};
        if (rawData.length !== 0) {
          var sortedData = _.sortBy(rawData, 'eventcount').reverse();
          var others = sortedData.splice(4, sortedData.length - 1);

          var sumEventCountForEachSource = _.reduce(others, function (memo, row) {
            return memo + row.eventcount;
          }, 0);
          for (var i = 0; i < sortedData.length; i++) {
            if (sortedData[i].referal === '') {
              sortedData[i].referal = 'Direct';
              break;
            }
          }
          var sourceObject = {
            eventcount: sumEventCountForEachSource,
            referal: 'other'
          };
          if (sourceObject.eventcount !== 0) {
            sortedData.push(sourceObject);
          }

          var totalEvents = _.reduce(sortedData, function (memo, row) {
            return memo + row.eventcount;
          }, 0);

          sourcesData = {
            data: sortedData,
            totalEvents: totalEvents
          };
        } else {
          var direct = {
            referal: 'Direct',
            eventcount: 0
          };
          sourcesData = {
            data: [direct],
            totalEvents: 0
          };
        }
        //console.log('sourcesData', sourcesData);
        callback(sourcesData);
      },
      formGeographicalData: function (result, callback) {
        var rawData = result[0].data.data;
        var geographicalData;
        if (rawData.length !== 0) {
          var totalEvents = 0,
            totalNewVisitors = 0;

          var geoData = _.map(rawData, function (geo) {
            geo.pageviewvsnewvisitorcount = geo.pageviewvsnewvisitorcount || '0/0';
            var pageViewVsNewVisitorCount = geo.pageviewvsnewvisitorcount.split('/');
            totalEvents += parseInt(pageViewVsNewVisitorCount[0] || 0);
            totalNewVisitors += parseInt(pageViewVsNewVisitorCount[1] || 0);
            var country = geo.country,
              city = geo.city;
            if (geo.country && !geo.city) {
              city = geo.country + ' - undetermined city';
            } else if (!geo.city && !geo.country) {
              country = 'undetermined';
              city = 'undetermined';
            }
            return {
              eventcount: pageViewVsNewVisitorCount[0] || 0,
              newvisitorcount: pageViewVsNewVisitorCount[1] || 0,
              country: country,
              city: city,
              countrycode: geo.countrycode,
              latitude: geo.latitude,
              longitude: geo.longitude
            };
          });
          var sortedData = _.sortBy(geoData, 'eventcount').reverse();
          var fromCountries = {
            eventCounts: {},
            newVisitors: {}
          };
          var i;
          for (i = 0; i < sortedData.length; i++) {
            sortedData[i].color = calculateRadius(sortedData[i].eventcount, totalEvents).countryColor;
            fromCountries.eventCounts[sortedData[i].country] = (fromCountries.eventCounts[sortedData[i].country] || 0) + parseInt(sortedData[i].eventcount);
            fromCountries.newVisitors[sortedData[i].country] = (fromCountries.newVisitors[sortedData[i].country] || 0) + parseInt(sortedData[i].newvisitorcount);
          }

          var domainRawData = result[1].data.data;
          var totalDomainData = domainRawData[0].pageviewvsnewvisitorcount.split('/');
          var eventCountForOther = parseInt(totalDomainData[0]) - totalEvents;
          var newVisitorsForOther = parseInt(totalDomainData[1]) - totalNewVisitors;
          totalEvents = parseInt(totalDomainData[0]);
          totalNewVisitors = parseInt(totalDomainData[1]);
          var otherCountry = {
            eventcount: eventCountForOther,
            newvisitorcount: newVisitorsForOther,
            country: 'Others',
            city: 'Others',
            radius: 0
          };
          sortedData.push(otherCountry);

          var countries = angular.copy(_.uniq(sortedData, 'country'));
          for (i = 0; i < countries.length; i++) {
            if (fromCountries.eventCounts.hasOwnProperty(countries[i].country) && fromCountries.newVisitors.hasOwnProperty(countries[i].country)) {
              countries[i].eventcount = fromCountries.eventCounts[countries[i].country];
              countries[i].newvisitorcount = fromCountries.newVisitors[countries[i].country];
            }
          }

          geographicalData = {
            totalPageViews: totalEvents,
            totalNewVisitors: totalNewVisitors,
            countries: countries,
            cities: sortedData
          };
        } else {
          var emptyGeoObject = {
            eventcount: 0,
            newvisitorcount: 0,
            city: 'No city',
            country: 'No country'
          };
          geographicalData = {
            totalPageViews: 0,
            totalNewVisitors: 0,
            countries: [emptyGeoObject],
            cities: [emptyGeoObject]
          };
        }
        //console.log('geographicalData', geographicalData);
        callback(geographicalData);
      },
      formBubblesArayForCities: function (cities, totalPageViews) {
        var bubblesArray = _.map(cities, function (city) {
          var bubble = {
            name: city.city || 'empty',
            radius: city.city !== 'Others' ? calculateRadius(city.eventcount, totalPageViews).bubbleSize : 0,
            eventcount: city.eventcount,
            country: city.countrycode,
            latitude: city.latitude,
            longitude: city.longitude,
            fillKey: 'bubbles',
            borderWidth: 0
          };
          return bubble;
        });
        return bubblesArray;
      }

    };
  });
