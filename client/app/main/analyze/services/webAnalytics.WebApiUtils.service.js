'use strict';

angular.module('dashboardApp')
  .factory('Webanalytics.WebApiUtils', function ($http, apiUrl) {

    // Public API here
    return {
      executeQuery: function (propertyId, query, success, dashboard, error) {
      	var url = apiUrl + '/pixel/api/v2/query/json';
        if(__DEV__) {
          console.log('Site report: ', query);
        }
        success = success || angular.noop;
        error = error || angular.noop;
        return $http({
          method: 'GET',
          url: url,
          cache : true,
          params : {
            propertyId : propertyId,
            q : JSON.stringify(query)
          }
      	}).then(
          function(res){
            if(__DEV__) {
              console.log('Site report: ', query, res.data);
            }
            success(res, dashboard);
            return res;
          },
          function (err) {
            if(error) {
              error(err);
            }
            return err;
          }
        );
      },

      //needed for $q.all (same as jquery $.when(promises)
      executeQueryReturnPromise: function (propertyId, query) {
          var url = apiUrl + '/pixel/api/v2/query/json';
          return $http({
              method: 'GET',
              url: url,
              cache : true,
              params : {
                propertyId : propertyId,
                q : JSON.stringify(query)
              }
            });
      },

      executeMetricQuery: function (propertyId, query, dashboard, success) {
        var url = apiUrl + '/pixel/api/v2/query/json';
        query.dimensions = 'monthOfYear,day,year,propertyId';
        return $http({
          params : {
            q : JSON.stringify(query),
            propertyId : propertyId
          },
          method: 'GET',
          cache : true,
          url: url
        }).then(function(res){
          success(res, dashboard);
        });
      },

      downloadCSV: function(propertyId, query, success){
        var url = apiUrl + '/pixel/api/v2/query/json/ascsv';
        query.dimensions = 'monthOfYear,day,year,propertyId';
        return $http({
          params : {
            q : JSON.stringify(query),
            propertyId : propertyId
          },
          method: 'GET',
          url: url
        }).then(function(res){
          success(res);
        });
      },

      executeDimensionQuery: function (propertyId, query, dashboard, dimension, success, tableSuccess) {
        var url = apiUrl + '/pixel/api/v2/query/json';
        return $http({
          params : {
            q : JSON.stringify(query),
            propertyId : propertyId
          },
          method: 'GET',
          cache : true,
          url: url
        }).then(function(res){
          success(res, dashboard, dimension);
          if(tableSuccess){
            tableSuccess(res, dashboard);
          }
        });
      },

      getGeoData: function(propertyId, dimension, query, success){
        if(dimension === 'country'){
          dimension = 'countryCode';
        }

        query.dimensions = 'propertyId,' + dimension;
        var url = apiUrl + '/pixel/api/v2/query/json';
        return $http({
          params : {
            q : JSON.stringify(query),
            propertyId : propertyId
          },
          cache : true,
          method: 'GET',
          url: url
        }).then(function(res){
          success(res);
        });
      },

      executeCountQuery: function (propertyId, query, metric, success) {
        query.dimensions = 'propertyId';
        query.metrics = metric.value;
        var url = apiUrl + '/pixel/api/v2/query/json';
        return $http({
          params : {
            q : JSON.stringify(query),
            propertyId : propertyId
          },
          cache : true,
          method: 'GET',
          url: url
        }).then(function(res){
          success(res, metric);
        });
      }
    };
  });
