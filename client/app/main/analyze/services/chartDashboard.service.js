'use strict';

angular.module('dashboardApp')
  .factory('ChartDashboard', function(Tile, gapFiller) {
    var ChartDashboard = function(metrics){
    	this.tiles = this.initTiles(metrics);
    	this.tsConfig = this.getTsConfig();
    	this.selectedDimension = 'propertyId';
    	this.reportTime = 'day';
    };

    ChartDashboard.prototype.initTiles = function (metrics) {
    	var tiles = [];
    	for(var i = 0; i < metrics.length; i++){
    		tiles.push(new Tile(metrics[i]));
    	}
    	return tiles;
    };

    ChartDashboard.prototype.getTsConfig = function(){
    	return {getterX: function(d){return d.date;},getterY: function(d){return d.value;}};
    };

    ChartDashboard.prototype.selectTile = function(tile){
    	this.selectedTile = tile;
    };

    ChartDashboard.prototype.isTileSelected = function(tile){
    	return this.selectedTile === tile;
    };

    ChartDashboard.prototype.getTileValue = function(data){
      if(this.selectedTile.value === 'avgTimeBetweenVisit'){
        return data.metric_avg_time_off; //jshint ignore:line
      } else {
  		  return data[this.selectedTile.value.toLowerCase()];  		        
      }
  	};

  	ChartDashboard.prototype.getDimensionValue = function(data, value){
  		return data[value.toLowerCase()];  		
  	};

    ChartDashboard.prototype.applyTableData = function(result, self){
      var rawData = result.data.data;
      self.tableData = _.map(rawData, function(d, i){        
        var referalPage = d.custom_referal_page // jshint ignore:line 
        if(referalPage === 'null'){
          referalPage = 'Direct Traffic';
        }
        return {
          'source': referalPage,
          'count': d.count,
          'uniqueCount': d.uniquecount,
          'total': d.totalcount,
          'id': i
        };
      });
      
      self.tableData = self.tableData.splice(0, 10);      
    };


  	ChartDashboard.prototype.applyMultipleSeriesQueryResult = function(result, self, dimension){
  		var rawData = result.data.data;
      var sortedData = _.sortBy(_.map(rawData, function(d) {
      	var datum = {
      		date: new Date(d.year, d.monthofyear - 1, d.day) 
      	};
        datum.value = self.getDimensionValue(d, self.selectedTile.value);
      	datum[dimension] = d[dimension];    
          return datum;
      }), 'date');

      var groupedData = _.toArray(_.groupBy(sortedData, dimension));
      groupedData = gapFiller.fillGaps(groupedData, dimension);


      // self.tsData = groupedData;
      self.msData = groupedData;

      var datum = groupedData[0];
      var valueFields = 
        _(datum)
          .keys()
          .filter(function(key) { return key !== 'date'; })
          .value();

      self.msConfig =  {
        aspectRatio: 2,
        seriesLabels: _.map(valueFields, function(v) {
          return _.trunc(v, 25);
        }),
        timeField : 'date',
        valueFields : valueFields
      };

      self.ssData = null;
      self.isLoading = false;
  	};

    ChartDashboard.prototype.applyQueryResult = function(result, self){
      var rawData = result.data.data;
      self.tsData = {
        name: 'webAnalytics',
        data: _.sortBy(_.map(rawData, function(d) {
          var datum = {
            date: new Date(d.year, d.monthofyear - 1, d.day) 
          };
          var value = self.getTileValue(d);
          if(value === null){
            value = 0;
          }
          datum.value = value;
          return datum;
        }), 'date')
      };

      self.ssData = self.tsData.data;
      self.msData = null;
      self.isLoading = false;
    };

    return ChartDashboard;
  })
  .factory('Tile', function () {
    var Tile = function(metric){
    	this.value = metric.value;
    	this.label = metric.label;
    };

    return Tile;
  });
