'use strict';

angular.module('dashboardApp')
  .factory('Trackedevents.WebApiUtils', function ($http, apiUrl) {
    // Public API here
    return {
      // @Deprecated
      getEventStats: function (host, eventName, startDate, endDate, success, error) {
        var url = apiUrl + '/pixel/api/v2/report/events/stat?starttime=' + startDate + '&endtime=' + endDate + '&host=' + host + '&eventname=' + eventName;
        return $http({
          method: 'GET',
          url: url}).then(function(res){
          success(res);
        }, function(res){
          error(res);
        });
      },

      // @Deprecated
      getEventCount: function(host, eventName, startDate, endDate, success, error){
        var url = apiUrl + '/pixel/api/v2/report/events/stat?starttime='  + startDate +  '&endtime=' + endDate + '&host='  + host +  '&eventname=' + eventName;
        return $http({
          method: 'GET',
          url: url}).then(function(res){
          success(res);
        }, function(res){
          error(res);
        });
      },

      getEvents: function(host, teamId, success){
        var url = apiUrl + '/pixelmapgen/api/v2/mapper/view/?host=' + host + '&teamId=' + teamId;
        return $http({
          method: 'GET',
          url: url}).then(function(res){
          success(res);
        });
      },

      // @Deprecated
      getReferals: function (host, eventName, success) {
        var url = apiUrl + '/pixel/api/v2/report/events/referal?starttime=2014-01-01&endtime=2015-02-01&host=' + host + '&eventname=' + eventName;
        return $http({
          method: 'GET',
          url: url}).then(function(res){
          success(res);
        });
      }
    };
  });
