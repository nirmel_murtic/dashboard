'use strict';

angular.module('dashboardApp')
    .factory('gapFiller', function() {
        return {
            formChartData: function (result, metric, start, end, timePeriod) {
                var timeDifference, timeIncrement, dateFormat;
                if (timePeriod === 'Today' || timePeriod === 'OneDayDifference') {
                    dateFormat = 'YYYY-MM-DD HH:mm';
                    timeDifference = moment(end).diff(moment(start), 'hour') + 1;
                    timeIncrement = 3600 * 1000;
                } else {
                    start.setHours(0);
                    timeDifference = moment(end).diff(moment(start), 'days') + 1;
                    timeIncrement = 24 * 60 * 60 * 1000;
                    dateFormat = 'YYYY-MM-DD';
                }
                start.setMinutes(0);
                var output = [];
                var k = 0;
                for (var i = 0; i < timeDifference; i++) {
                    var object= {
                        date: new Date(start.getTime() + (timeIncrement * i))
                    };
                    var isFound = false;
                    for (var j=k; j<result.length; j++) {
                        if (moment(result[j].date).format(dateFormat) === moment(object.date).format(dateFormat)) {
                            isFound = true;
                            object[metric] = result[j][metric];
                            k += 1;
                            break;
                        }
                    }
                    if (isFound === false) {
                        object[metric] = 0;
                    }
                    output.push(object);
                }
                return output;
            }
        };
    });
