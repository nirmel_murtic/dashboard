'use strict';

describe('Service: webAnalyticsWebApiUtils', function () {

  // load the service's module
  beforeEach(module('dashboardApp'));

  // instantiate service
  var webAnalyticsWebApiUtils;
  beforeEach(inject(function (_webAnalyticsWebApiUtils_) {
    webAnalyticsWebApiUtils = _webAnalyticsWebApiUtils_;
  }));
});
