'use strict';

angular.module('analyze')
  .constant('ReportDateRanges', function() { 
    return {
      'Now' : [new Date(Date.now() - 86400000), new Date()],   
      'Last 7 Days' : [d3.time.day.offset(new Date(), -7), new Date()],   
      'Yesterday': [moment().subtract(1, 'days').startOf('day').toDate(), moment().startOf('day').toDate()],
      'Last 30 Days' : [d3.time.day.offset(new Date(), -30), new Date()],   
    };
  })
  .provider('resolveTimeframe', function() {

    var config = {
      timeFrames : null,
      defaultTo : 'last7',
      urlMapping : {
        'now' : 'Now',
        'last7' : 'Last 7 Days',
        'last30': 'Last 30 Days'
      }
    };

    this.setTimeframes = function(tfs) {
      config.timeFrames = tfs;
      return this;
    };

    this.setDefault = function(defaultStateParam) {
      if(config.urlMapping[defaultStateParam]) {
        config.defaultTo = defaultStateParam;
      } 
      return this;
    };

    this.getDefault = function() {
      return config.defaultTo;
    };

    this.getUrlRegex = function() {
      var keys = _.keys(config.urlMapping); 
      keys.push('custom-daterange');
      return keys.join('|'); 
    };


    this.$get = ['$state', '$q', function($state, $q) {
      return function($stateParams) {
        var label;
        if(!config.timeFrames) {
          throw 'Date range presets not loaded.';
        }
        if($stateParams.timeFrame && $stateParams.dateRangePreset === 'custom-daterange') {
          return $stateParams.timeFrame;
        } else if(config.urlMapping[$stateParams.dateRangePreset]) {
          label = config.urlMapping[$stateParams.dateRangePreset];
          return config.timeFrames[label];
        } else {
          return $q.reject('Unknown daterange.');
        }
      };
    }];
  
  });
