'use strict';

angular.module('dashboardApp')
  .service('AnalyzeGoalsService', function($http, AnalyzeActivityService, $window) {
    var _service={
      // 
      scope:null,
      getProjectGoals: function(scope){ //goalId, successCallback, errorCallback) {
        // This function needs to be updated with a live URL. When this function
        // is called, its successful execution invokes goalBuilder and the child
        // activityBuilder functions. Goals and activities are populated with this call.
        _service.scope=scope;
        return $http({
          method: 'GET',
          url: '/app/main/analyze/project.json'
          // apiUrl + '/api/v2/project/getProjects?teamId=' + teamId + 
          //'&joins=team.fractalAccount,goals.goal;page.socialAccount.
          // social,attachments.creativeImage&fields=' + this.projectProperties + 
          // this.projectGoalProperties + this.goalProperties + this.imageProperties
        })
          .success(function(result) {
            _service.goalBuilder(result.goals, scope);
          })
          .error(function(){console.log('Failed getActivities');});
      },
      getActivities: function(scope){
        // called by activities to bind to the goal.
        _service.activeGoal=scope.goal;
        var arr=_service.activeGoal.activities;
        return arr;
      },
      getGoal: function(goalId) {
        // returns the goal with the correct goalId
        var rtn=null;
        for(var o=0;o<this.goals.length; o++){
          if(this.goals[o].goalId===goalId){
            rtn=this.goals[o];
            break;
          }
        }
        return rtn;
      },
      goalBuilder:function(results, scope){
        // Goals are built when this function is called. Also, its child activities are
        // built at the same time.
        this.goals=[];
        $window.goals=[];
        for(var z=0;z<results.length;z++){
          this.goals.push({
            goalId:results[z].goalId,
            displayName:results[z].goal.displayName
          });
          AnalyzeActivityService.activityBuilder(results[z].goal.activities, this.goals[z]);
        }
        scope.goals=this.goals;
        this.createChart();
      },
      createChart:function(){
        // This is called after goalBuilder and activityBuilder have created all the necessary
        // data structures. The actual structure in the app is:
        // projects[index].goals[index].activities[index]; this is more terse than
        // the project.json file.
        if(this.activeGoal){
        //  this.svg=d3.select('#goalsChart');
          console.log('this.activeGoal');
          console.log(this.activeGoal);
        }
      },
      getCPA:function(spend, conversions){
        return spend/conversions;
      },
      getCTR:function(clicks, impressions){
        return clicks/impressions;
      },
      getCVR:function(spend, impressions){
        return spend/impressions;
      },
      svgTemplate:function(){
        // stube for svg element for charting.
        var _width=960, _height=300;
        return '<div id="svgWrapper"><svg id="goalsChart" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" class="goalsChart" height="'+_height+'" width="'+_width+'" xmlns:ev="http://www.w3.org/2001/xml-events" version="1.1"></svg></div>';
      }
    };
    return _service;
  });

