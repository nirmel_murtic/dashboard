'use strict';

angular.module('main')
  .factory('channelReport.WebApiUtils', function ($http, apiUrl) {
    // AngularJS will instantiate a singleton by calling "new" on this function
     return {
      getReach: function (pageId, startDate, endDate, success, drawChart, dash) {    
        var url = apiUrl + '/api/v1/monitor/reach/' + 101416801784 + '?from=' + startDate + '&to=' + endDate;
        return $http({
          method: 'GET',
          url: url
        }).then(function(res){
          success(res);
          if(drawChart){
          	drawChart(res, dash);
          }
        });
      },
      
      getEngagement: function (pageId, startDate, endDate, success, drawChart, dash) {    
        var url = apiUrl + '/api/v1/monitor/engagements/' + 101416801784 + '?from=' + startDate + '&to=' + endDate;
        return $http({
          method: 'GET',
          url: url
        }).then(function(res){
          success(res);
          if(drawChart){
          	drawChart(res, dash);
          }
        });
      },

	    getGrowth: function (pageId, startDate, endDate, success, drawChart, dash) {    
        var url = apiUrl + '/api/v1/monitor/fangrowth/' + 101416801784 + '?from=' + startDate + '&to=' + endDate;
        return $http({
          method: 'GET',
          url: url
        }).then(function(res){
          success(res);
          if(drawChart){
          	drawChart(res, dash);
          }
        });
      },


      getExternalReferals: function (pageId, startDate, endDate, success) {    
        var url = apiUrl + '/api/v1/monitor/externalreferrals/' + 101416801784 + '?from=' + startDate + '&to=' + endDate;
        return $http({
          method: 'GET',
          url: url
        }).then(function(res){
          success(res);
        });
      }
  	};
  });
