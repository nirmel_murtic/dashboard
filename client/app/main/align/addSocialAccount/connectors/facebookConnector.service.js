'use strict';

angular.module('align')
  .service('facebookConnector', function (Facebook) {
    return {
      login: function() {
        Facebook.fullLogin();
      },
      api: function(request, response) {
        Facebook.api(request, response);
      },
      logout: function(callback) {
        Facebook.fullLogout(callback);
      }
    };
  });
