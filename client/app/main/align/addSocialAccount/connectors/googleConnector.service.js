'use strict';

angular.module('align')
  .service('googleConnector', function ($window) {
    return {
      clientId: $window.configuration.googleClientId,
      scopes: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/adwords',
      /* jshint ignore:start */
      login: function(callback) {
        gapi.auth.authorize({
          client_id: this.clientId,
          scope: this.scopes,
          immediate: false,
          approval_prompt: 'force', //TODO: This should be removed in production
          cookie_policy: 'single_host_origin',
          access_type: 'offline',
          redirect_uri: 'postmessage',
          response_type: 'token id_token code'
        }, callback);
      }
      /* jshint ignore:end */
    };
  });
