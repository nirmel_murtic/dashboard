'use strict';

angular.module('align')
  .service('twitterConnector', function ($rootScope, apiUrl, $auth, $window, $http, $timeout, $interval) {
    $rootScope.twitterCallback = function(accessToken, accessTokenSecret, userId, username) {
      $rootScope.$broadcast('Twitter:onConnect', {
          accessToken: accessToken,
          accessTokenSecret: accessTokenSecret,
          username: username,
          userId: userId
        }
      );
    };

    $rootScope.twitterAdAccountsCallback = function(json) {
      var data = angular.fromJson(json);
      $rootScope.$broadcast('Twitter:onAdsConnect', data);
    };

    return {
      login: function() {
        var twitterWindow = window.open(apiUrl + '/api/v2/social/twitter/signIn?userEmail=' + $auth.user.email + '&access_token=' + $auth.user.access_token, 'Twitter account','height=400,width=800'); // jshint ignore:line

        twitterWindow.onunload = function() {
          var httpPromise = null;

          var promise = $interval(function() {
            if(httpPromise && httpPromise.$$state.status === 0) {
              return;
            }

            httpPromise = $http({
              method: 'GET',
              url: apiUrl + '/api/v2/social/twitter/getInfo?access_token=' + $auth.user.access_token // jshint ignore:line
            })
              .success(function(result) {
                var data = result.data;

                if(data.accessToken) {
                  $timeout(function() {
                    $rootScope.twitterCallback(data.accessToken, data.accessTokenSecret, data.userId, data.username);
                    $rootScope.twitterAdAccountsCallback(data.adAccounts);
                  });

                  $interval.cancel(promise);
                } else if(data.twitterError) {
                  console.log(data.twitterError);
                  $interval.cancel(promise);
                }
              })
              .error(function(error) {
                $interval.cancel(promise);

                console.log(error.error.message);
              });
          }, 1000);
        };
      },
      logout: function() {
        window.open('//twitter.com/logout', 'Twitter logout','height=400,width=800');
      }
    };
  });
