'use strict';

angular.module('align')
  .controller('AddsocialaccountCtrl', function ($scope, $window, facebookConnector, $filter, twitterConnector, googleConnector, teamService, ngDialog, fsConfirm, $timeout) {
    // Added to prevent reading wrong data from parent scope
    $scope.facebookAdAccounts = null;
    $scope.facebookPages = null;
    $scope.googleAdWordsCustomers = null;
    $scope.flaggedAccountExist = null;
    $scope.replaceAccount = null;

    $scope.loadAccounts = function(after, token) {
      facebookConnector.api('/me/accounts?access_token=' + token + (after ? ('&after=' + after) : ''), function (res) {
        angular.forEach(res.data, function(value) {
          if($scope.selectedSocialAccount) {
            var filtered = $scope.selectedSocialAccount.pages.filter(function (obj) {
              return String(obj.pagePageId) === String(value.id);
            });

            if(filtered.length) {
              value.disabled = !filtered[0].removable;
              value.selected = true;
            }
          } else {
            value.selected = false;
          }
        });

        if(!$scope.facebookPages) {
          $scope.facebookPages = [];
        }

        $scope.facebookPages = $scope.facebookPages.concat(res.data);

        if(res.paging && res.paging.next) {
          $scope.loadAccounts(res.paging.cursors.after, token);
        } else {
          if($scope.selectedSocialAccount) {
            angular.forEach($scope.selectedSocialAccount.pages, function (value) {
              var exist = $scope.facebookPages.filter(function (obj) {
                var r = String(obj.id) === String(value.pagePageId);

                if(r) {
                  obj.pageId = value.pageId;
                }

                return r;
              }).length;

              if (!exist) {
                $scope.facebookPages.push({
                  id: value.pagePageId,
                  name: value.pageName,
                  selected: true,
                  disabled: !value.removable,
                  flagged: true
                });

                $scope.flaggedAccountExist = true;
              }
            });
          }
        }
      });
    };

    $scope.loadAdAccounts = function(after, token) {
      facebookConnector.api('/me/adaccounts?fields=name,currency,account_id&access_token=' + token + (after ? ('&after=' + after) : ''), function (res) {
        angular.forEach(res.data, function(value) {
          if($scope.selectedSocialAccount) {
            var filtered = $scope.selectedSocialAccount.fbAccounts.filter(function (obj) {
              var r = String(obj.accountId) === String(value['account_id']); // jshint ignore:line

              if(r) {
                value.myId = obj.id;
              }

              return r;
            });

            if(filtered.length) {
              value.disabled = !filtered[0].removable;
              value.selected = true;
            }
          } else {
            value.selected = false;
          }
        });


        if(!$scope.facebookAdAccounts) {
          $scope.facebookAdAccounts = [];
        }

        $scope.facebookAdAccounts = $scope.facebookAdAccounts.concat(res.data);

        if(res.paging && res.paging.next) {
          $scope.loadAdAccounts(res.paging.cursors.after, token);
        } else {
          if ($scope.selectedSocialAccount) {
            angular.forEach($scope.selectedSocialAccount.fbAccounts, function (value) {
              var exist = $scope.facebookAdAccounts.filter(function (obj) {
                return String(obj['account_id']) === String(value.accountId); // jshint ignore:line
              }).length;

              if (!exist) {
                $scope.facebookAdAccounts.push({
                  myId: value.id,
                  account_id: value.accountId, // jshint ignore:line
                  name: value.name,
                  currency: value.currency,
                  selected: true,
                  disabled: !value.removable,
                  flagged: true
                });

                $scope.flaggedAccountExist = true;
              }
            });
          }
        }
      });
    };

    $scope.$on('Facebook:onConnect', function(event, data) {
      console.log('Facebook connector status: ');
      console.log(data);
      if(data) {
        delete $scope.twitterAccount;

        if($scope.selectedSocialAccount &&
          $scope.selectedSocialAccount.social &&
          data.userId !== $scope.selectedSocialAccount.social.socialUsername) {
          $scope.replaceAccount = true;
        }

        $scope.facebookData = data;

        $scope.userId = data.userId;
        $scope.accountName = data.accountName;

        $scope.loadAccounts(null, data.organicToken);
        $scope.loadAdAccounts(null, data.adsToken);
      }
    });

    $scope.$on('Twitter:onConnect', function(event, data) {
      delete $scope.facebookPages;
      delete $scope.facebookAdAccounts;
      delete $scope.facebookData;

      if($scope.selectedSocialAccount &&
        $scope.selectedSocialAccount.social &&
        data.username !== $scope.selectedSocialAccount.social.socialUsername) {
        $scope.replaceAccount = true;
      }

      $scope.twitterAccount = data;
      $scope.twitterAccount.username = data.username;
      $scope.twitterAccount.userId = data.userId;
      $scope.accountName = data.username;
      $scope.userId = data.userId;


      $scope.$apply();
    });

    $scope.$on('Twitter:onAdsConnect', function(event, res) {
      angular.forEach(res.data, function(value) {
        if($scope.selectedSocialAccount) {
          var filtered = $scope.selectedSocialAccount.twAccounts.filter(function (obj) {
            var r = String(obj.accountId) === String(value.id); // jshint ignore:line

            if(r) {
              value.myId = obj.id;
            }

            return r;
          });

          if(filtered.length) {
            value.disabled = !filtered[0].removable;
            value.selected = true;
          }
        } else {
          value.selected = false;
        }
      });

      if($scope.selectedSocialAccount) {
        angular.forEach($scope.selectedSocialAccount.twAccounts, function (value) {
          var exist = res.data.filter(function (obj) {
            return String(obj.id) === String(value.accountId); // jshint ignore:line
          }).length;

          if (!exist) {
            var acc = {
              myId: value.id,
              id: value.accountId, // jshint ignore:line
              name: value.name,
              selected: true,
              disabled: !value.removable,
              promotable_users: [], // jshint ignore:line
              flagged: true
            };

            // jshint ignore:start
            angular.forEach(value.promotableUsers, function(user) {
                acc['promotable_users'].push({
                  user_id: user.userId,
                  screen_name: user.screenName
                });
            });
            // jshint ignore:end

            res.data.push(acc);

            $scope.flaggedAccountExist = true;
          }
        });
      }

      $scope.twitterAdAccounts = [];

      angular.forEach(res.data, function (value) {
        // jshint ignore:line
        if(value['promotable_users']) { // jshint ignore:line
          $scope.twitterAdAccounts.push(value);
        } // jshint ignore:line
      });

      $scope.$apply();
    });

    $scope.switchFacebookAccount = function() {
      facebookConnector.logout(function() {
        $window.sessionStorage.switchFacebookAccount = true;
        $window.location.reload();
      });
    };

     $scope.switchTwitterAccount = function() {
      twitterConnector.logout(function() {
        $window.sessionStorage.switchTwitterAccount = true;
        $window.location.reload();
      });
    };

    $scope.logoutTwitter = function() {
      twitterConnector.logout();

      delete $scope.twitterAccount;
    };

    $scope.addSocialAccount = function() {
      if(!$scope.replaceAccount) {
        $scope.addSocialAccountInternal();
      } else {

        fsConfirm('generic', {
          title: $filter('translate')('label.align.addSocialAccount.AreYouSureYouWantToSwitchAccountFromTo', {from: $scope.selectedSocialAccount.name, to: $scope.accountName}),
          // body: $filter('translate')('label.align.addSocialAccount.AreYouSureYouWantToSwitchAccountFromTo', ),
          yes: $filter('translate')('label.align.addSocialAccount.SwitchAccount')
        }).then(function() {
          $scope.addSocialAccountInternal();
          $scope.closeThisDialog();
        });

      }
    };

    $scope.addSocialAccountInternal = function() {
      var tokens = {};

      var account = {
      };

      if($scope.selectedSocialAccount) {
        account.accountId = $scope.selectedSocialAccount.accountId;

        var soc = $scope.selectedSocialAccount.social;

        account.social = {
          socialType: soc.socialType,
          socialUsername: soc.socialUsername,
          socialId: soc.socialId
        };
      } else {
        account.social = {};
      }

      if($scope.facebookData) {
        account.social.socialType = 'Facebook';
        account.social.socialUsername = $scope.facebookData.userId;
      } else if($scope.twitterAccount) {
        account.social.socialUsername = $scope.twitterAccount.username;
        account.social.socialType = 'Twitter';

      } else if($scope.googleData) {
        account.social.socialUsername = $scope.userInfo.id;
        account.social.socialType = 'Google';
      }

      if($scope.facebookPages || $scope.facebookAdAccounts) {
        account.pages = [];
        account.fbAccounts = [];

        angular.forEach($scope.facebookPages, function (value) {
          if(value.selected) {
            account.pages.push({pagePageId: value.id, flagged: value.flagged});
          }
        });

        angular.forEach($scope.facebookAdAccounts, function (value) {
          if(value.selected) {
            account.fbAccounts.push({accountId: value['account_id'], id: value.myId, flagged: value.flagged }); // jshint ignore:line
          }
        });
      }

      if($scope.twitterAdAccounts) {
        account.twAccounts = [];

        angular.forEach($scope.twitterAdAccounts, function (value) {
          if(value.selected) {
            var twAccount = {
              accountId: value.id,
              id: value.myId,
              promotableUsers: []
            };

            // jshint ignore:start
            angular.forEach(value['promotable_users'], function(acc) {
              twAccount.promotableUsers.push({
                userId: acc['user_id'],
                screenName: acc['screen_name']
              });
            });
            // jshint ignore:end

            account.twAccounts.push(twAccount);
          }
        });
      }


      if($scope.googleAdWordsCustomers) {
        account.adwordsAccounts = [];

        angular.forEach($scope.googleAdWordsCustomers, function (value) {
          if (value.selected) {
            account.adwordsAccounts.push({accountId: value.customerId, id: value.myId, flagged: value.flagged}); // jshint ignore:line
          }
        });
      }

      if($scope.facebookData) {
        tokens.accessTokenDef = $scope.facebookData.organicToken;
        tokens.accessTokenSec = $scope.facebookData.adsToken;

        account.name = $scope.facebookData.accountName;
      } else if($scope.twitterAccount) {
        tokens.accessTokenDef = $scope.twitterAccount.accessToken;
        tokens.accessTokenSecret = $scope.twitterAccount.accessTokenSecret;
        account.name = $scope.twitterAccount.username;
      } else if($scope.googleData) {
        tokens.accessTokenDef = $scope.googleData['access_token']; // jshint ignore:line
        tokens.accessTokenSec = $scope.googleData.code;

        account.name = $scope.userInfo.name;
      }

      account.team = {
        id: $scope.teamData.id,
        name: $scope.teamData.name
      };

      $scope.accountProcessing = true;
      teamService.addSocialAccount(account, tokens, function(result) {
        var index = $scope.teamData.socialAccounts.indexOf($scope.selectedSocialAccount);
        if(index !== -1) {
          $scope.teamData.socialAccounts.splice(index, 1, result.data.account);
        } else {
          $scope.teamData.socialAccounts.push(result.data.account);
        }

        delete $scope.accountProcessing;
        delete $scope.selectedSocialAccount;

        $scope.closeThisDialog();
      }, function(error) {
        delete $scope.accountProcessing;
        fsConfirm('genericAlert', {
          body: error.error.message
        });
      });
    };

    $scope.initFacebook = function() {
      if($scope.selectedSocialAccount) {
        $scope.accountName = $scope.selectedSocialAccount.name;
        $scope.userId = $scope.selectedSocialAccount.social.socialUsername;
      }

      $timeout(function() {
        facebookConnector.login();
      });
    };

    $scope.initGoogle = function() {
      if($scope.selectedSocialAccount) {
        $scope.accountName = $scope.selectedSocialAccount.name;
        $scope.userId = $scope.selectedSocialAccount.social.socialUsername;
      }
    };

    $scope.initTwitter = function() {
      if($scope.selectedSocialAccount) {
        $scope.accountName = $scope.selectedSocialAccount.name;
        $scope.userId = $scope.selectedSocialAccount.social.socialUsername;
      }

      twitterConnector.login();
    };

    $scope.disabledPage = true;

    $scope.pageClicked = function(page) {
      if(page.disabled) {
        fsConfirm('genericMultipleLines', {
          firstRow: $filter('translate')('label.alignProjects.FacebookPagesAreShareAcrossAllYourTeamsProjects'),
          secondRow: $filter('translate')('label.alignProjects.OnceAndPageWasUsedInACampaignItCannotBeUnselected'),
          thirdRow: $filter('translate')('label.alignProjects.IfNoCampaignWasLaunchedDeleteAnyGoalOrDraftCampaigns')
        });
        return;
      }

      if(page.pageId && !page.selected) {

        fsConfirm('generic', {
          title: $filter('translate')('label.alignProjects.AreYouSureYouWantToUnregisterThisPage'),
          body: page.name,
          yes: $filter('translate')('label.alignProjects.Unregister')
        }).then(function() {
          page.selected = false;
        }, function() {
          page.selected = true;
        });
      }
    };

    $scope.disabledCustomer = true;

    $scope.adAccountClicked = function(adAccount) {
      if(adAccount.disabled) {
        fsConfirm('genericMultipleLines', {
          firstRow: $filter('translate')('label.alignProjects.AdwordsAdAccountsAreSharedAcrossAllYourTeamsProjects'),
          secondRow: $filter('translate')('label.alignProjects.AfterConnectingYourAdwordsAdAccountsCreateAGoalNavigateToCampaigns'),
          thirdRow: $filter('translate')('label.alignProjects.IfNoCampaignWasLaunched')
        });
        return;
      }
      if(adAccount.myId && !adAccount.selected) {

        fsConfirm('generic', {
          title: $filter('translate')('label.alignProjects.AreYouSureYouWantToUnregisterThisAdAccount'),
          body: adAccount.name,
          yes: $filter('translate')('label.alignProjects.Unregister')
        }).then(function() {
          adAccount.selected = false;
        }, function() {
          adAccount.selected = true;
        });
      }
    };

    $window.googleLoaded = function() {
      googleConnector.login(function(data) {
        $scope.googleData = data;

        if(data.code && data['id_token']) { // jshint ignore:line
          var idToken = jwt_decode(data['id_token']); // jshint ignore:line

          if($scope.selectedSocialAccount &&
            $scope.selectedSocialAccount.social &&
            idToken.sub !== $scope.selectedSocialAccount.social.socialUsername) {
            $timeout(function() {
              $scope.replaceAccount = true;
            });
          }

          teamService.getGoogleAccountInfo(data['access_token'], function(result) { // jshint ignore:line
            $scope.customer = result.data.customer;
            $scope.userInfo = result.data.userInfo;

            if($scope.userInfo) {
              $scope.accountName = $scope.userInfo.name;
            }

            $scope.googleAdWordsCustomers = result.data.managedCustomers ? result.data.managedCustomers : [ result.data.customer ];

            angular.forEach($scope.googleAdWordsCustomers, function(value) {
              if($scope.selectedSocialAccount) {
                var filtered = $scope.selectedSocialAccount.adwordsAccounts.filter(function (obj) {
                  var r = String(obj.accountId) === String(value.customerId); // jshint ignore:line

                  if(r) {
                    value.myId = obj.id;
                  }

                  return r;
                });

                if(filtered.length) {
                  value.disabled = !filtered[0].removable;
                  value.selected = true;
                }
              } else {
                value.selected = false;
              }
            });

            if($scope.selectedSocialAccount) {
              angular.forEach($scope.selectedSocialAccount.adwordsAccounts, function (value) {
                var exist = $scope.googleAdWordsCustomers.filter(function (obj) {
                  return String(obj.customerId) === String(value.accountId);
                }).length;

                if (!exist) {
                  $scope.googleAdWordsCustomers.push({
                    myId: value.id,
                    customerId: value.accountId, // jshint ignore:line
                    descriptiveName: value.descriptiveName,
                    name: value.name,
                    canManageClients: value.canManageClients,
                    currencyCode: value.currency,
                    selected: true,
                    disabled: !value.removable,
                    flagged: true
                  });

                  $scope.flaggedAccountExist = true;
                }
              });
            }
          }, function(error) {
            if(error.error.message.indexOf('QuotaCheckError.DEVELOPER_TOKEN_NOT_APPROVED') !== -1) {
              $scope.googleErrorMessage = 'Developer token is not approved. You can use only test accounts in this moment!';
            } else if(error.error.message.indexOf('AuthenticationError.NOT_ADS_USER') !== -1) {
              $scope.googleErrorMessage = 'There is no Ads account for this user. Please create one or login with the Google credentials of a user who does have an AdWords account to connect.';
            }

            console.log(error.error.message);
          });
        }
      });
    };
  });
