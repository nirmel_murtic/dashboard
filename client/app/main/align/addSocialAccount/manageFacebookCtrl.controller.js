'use strict';

angular.module('align')
  .controller('ManageFacebookCtrl', function ($scope, $rootScope, customAudienceApi) {
    $scope.teamId = $rootScope.fractalAccountInfo.currentTeam.id;

    $scope.spinRefresh = false;

  	$scope.initFacebookAdAccounts = function(){
  		$scope.facebookAdAccounts = [];
  		var socialAccounts =  $rootScope.fractalAccountInfo.currentTeam.socialAccounts;
  		for(var i = 0; i < socialAccounts.length; i++){
    			if(socialAccounts[i].fbAccounts.length > 0){
    				$scope.facebookAdAccounts = socialAccounts[i].fbAccounts;
          }
      }
      $scope.selectAdAccount($scope.facebookAdAccounts[0]);
  	};

    $scope.showError = function(response){
      console.log(response);
    };

    $scope.moment = moment;

    $scope.selectAdAccount = function(account){
      $scope.selectedAdAccount = account;
    };

    $scope.applyCustomAudiences = function(response){
      // $scope.scrollBusy = false;
      // if(response.data.length < 21){
      //   $scope.doneFetchingCustom = true;
      // }
      $scope.customAudiences = response.data;
      $scope.fetchingCustom = false;      
      $scope.doneFetchingCustom = false;
    };

    $scope.showError = function(response){
      $scope.spinRefresh = false;
      console.log(response);
    };

    $scope.applyExistingAudiences = function(response){
      console.log(response);
    };

    $scope.updateAudiences = function(){
      $scope.spinRefresh = false;
      var accountId = $scope.selectedAdAccount.accountId;
      customAudienceApi.getAllCustomAudiences(accountId, $scope.teamId, $scope.applyCustomAudiences, $scope.showError);
      customAudienceApi.getAllLookalikeAudiences(accountId, $scope.teamId, $scope.applyLookalikeAudiences, $scope.showError);
      $scope.initAcceptedCustomAudiences();
    };

    $scope.initCustomAudiences = function(){
      if($scope.selectedAdAccount){
        var accountId = $scope.selectedAdAccount.accountId;
        customAudienceApi.syncAudiences(accountId, $scope.teamId, $scope.applyExistingAudiences, $scope.showError);
        customAudienceApi.getAllCustomAudiences(accountId, $scope.teamId, $scope.applyCustomAudiences, $scope.showError);
        $scope.initAcceptedCustomAudiences();
      }
    };

    $scope.applyAcceptedCustomAudiences = function(response){
      // $scope.scrollBusy = false;
      $scope.acceptedCustomAudiences = response.data;
    };

    $scope.initAcceptedCustomAudiences = function(){
      if($scope.selectedAdAccount){
        var accountId = $scope.selectedAdAccount.accountId;
        customAudienceApi.getAllEligibleCustomAudiences(accountId, $scope.teamId, $scope.applyAcceptedCustomAudiences, $scope.showError);
      }
    };

    $scope.dataTypeMapping = {
      'EMAIL_SHA256': 'Email',
      'PHONE_SHA256': 'Phone'
    };

    $scope.forceSyncAudiences = function(){
      if($scope.selectedAdAccount){
        $scope.spinRefresh = true;
        var accountId = $scope.selectedAdAccount.accountId;
        $scope.doneFetchingCustom = false;        
        $scope.fetchingCustom = true;
        $scope.fetchinglookalike = true;
        customAudienceApi.forceSyncAudiences(accountId, $scope.teamId, $scope.updateAudiences, $scope.showError);
      }
    };


    $scope.applyLookalikeAudiences = function (response) {
        if(response.data.length < 21){
          $scope.doneFetchingLookalike = true;
        }

        $scope.fetchingLookalike = false;
        $scope.lookalikeAudiences = _.map(response.data, function(audience) {
        audience.lookalikeSpec = angular.fromJson(audience.lookalikeSpec);
        return audience;
      });        
    };

    $scope.applyMoreCustomAudiences = function(response){
      if(response.data.length < 21){
        $scope.doneFetchingCustom = true;
      }
      $scope.fetchingCustom = false;
      $scope.customAudiences = $scope.customAudiences.concat(response.data);
     };

     $scope.applyMoreLookalikeAudiences = function (response) {
        if(response.data.length < 21){
          $scope.doneFetchingLookalike = true;
        }

        $scope.fetchingLookalike = false;

        var newLookalikes = _.map(response.data, function(audience) {
         audience.lookalikeSpec = angular.fromJson(audience.lookalikeSpec);
         return audience;
       });
       $scope.lookalikeAudiences = $scope.lookalikeAudiences.concat(newLookalikes);        
     };

    $scope.initLookalikeAudiences = function(){
      if($scope.selectedAdAccount){
        var accountId = $scope.selectedAdAccount.accountId;
        customAudienceApi.getAllLookalikeAudiences(accountId, $scope.teamId, $scope.applyLookalikeAudiences, $scope.showError);
      }
    };

    $scope.initFacebookAdAccounts();
    $scope.initLookalikeAudiences();
    $scope.initCustomAudiences();

    $scope.$watch('selectedAdAccount', function (newVal) {
      if(!newVal) {
        return;
      }

      var accountId = newVal.accountId;    
      $scope.doneFetchingCustom = false;
      $scope.doneFetchingLookalike = false;
      $scope.selectedAdAccount = newVal;
      $scope.forceSyncAudiences();
      // customAudienceApi.getAllCustomAudiences(accountId, $scope.teamId, $scope.applyCustomAudiences, $scope.showError);
      customAudienceApi.getAllAcceptedCustomAudiences(accountId, $scope.teamId, $scope.applyAcceptedCustomAudiences, $scope.showError);
      customAudienceApi.getAllLookalikeAudiences(accountId, $scope.teamId, $scope.applyLookalikeAudiences, $scope.showError);
    });

    $scope.getNextCustomGroup = function(){
      $scope.fetchingCustom = true;
      if($scope.doneFetchingCustom){
        return;
      }

      var accountId = $scope.selectedAdAccount.accountId;
      customAudienceApi.getAllCustomAudiences(accountId, $scope.teamId, $scope.applyMoreCustomAudiences, $scope.showError,  $scope.customAudiences.length + 1, 21);
    };

    $scope.getNextLookalikeGroup = function(){
      $scope.fetchingLookalike = true;
      if($scope.doneFetchingLookalike){
        return;
      }

      var accountId = $scope.selectedAdAccount.accountId;
      customAudienceApi.getAllLookalikeAudiences(accountId, $scope.teamId, $scope.applyMoreLookalikeAudiences, $scope.showError, $scope.lookalikeAudiences.length + 1, 21);
    };

  });
