 'use strict';

angular.module('align')
  .controller('ManageCustomAudiencesCtrl', function ($scope, $timeout, customAudienceApi, fsConfirm, $filter, Scroll) {
    $scope.submitted = false;

    
    $scope.onCsvImport = function(){
      $scope.audienceData = _.map(this.result,'CAcsv');
      $scope.checkSize();
      $scope.validateType();
    };

    $scope.changeCopyPaste = function(){
      if($scope.customAudienceData.copyPaste){
        var untrimmedData = $scope.customAudienceData.copyPaste.split('\n');
        $scope.audienceData = _.map(untrimmedData, function(datum){
          return datum.trim();
        });        
      } else {
        $scope.customAudienceData.copyPaste = '';
        $scope.audienceData = '';
      }
      $scope.checkSize();
      $scope.validateType();
    };

    $scope.showSaving = function(response){
      if(!response.data.result.tosaccepted){
        $scope.tosError = true;
        return;
      }
      $scope.customAudienceName = response.data.result.audienceName;
      $scope.tosError = false;
      $scope.showSavingView = true;
      $scope.submitted = false;
      $scope.processing = false;
      $scope.file = null;
      $scope.initCustomAudiences();
    };


    $scope.checkSize = function(){
      $scope.numberOfTargets = $scope.audienceData.length;
      if ($scope.numberOfTargets >= 20){
        $scope.audienceTooSmall = false;
      }

      else{
        $scope.audienceTooSmall = true;
      }

      if($scope.numberOfTargets > 10000){
        $scope.audienceTooLarge = true; 
      } else {
        $scope.audienceTooLarge = false; 
      }
    };

    $scope.validateType = function(){

      var apparentType = $scope.dataType;
      $scope.audienceWrongType = false;
      $scope.wrongAudienceCount = 0;

      //Just a simple check to make sure the emails have @ signs in them and the other things don't
      //We don't want to be too strict, only catch user mistakes before they get too far.
      //It is softened even more by allowing a 10% buffer to allow for crappy csvs or headers or whatever.
      _.forEach($scope.audienceData, function(datum){
        if ( ( apparentType === 'EMAIL_SHA256' && datum.indexOf('@') === -1 ) ||
             ( apparentType !== 'EMAIL_SHA256' && datum.indexOf('@') !== -1 ) ){
          $scope.wrongAudienceCount++;
        }
      });

      var percentWrong = ($scope.wrongAudienceCount / $scope.audienceData.length);
      console.log('percent: ', percentWrong, 'apparent Type: ', apparentType, 'count: ', $scope.wrongAudienceCount, 'data length: ', $scope.audienceData.length);

      // if you want to be more specific, e.g. allowing half the phone numbers to have '@' signs:
      // if ((apparentType === 'EMAIL_SHA256' && percentWrong > 0.1) ||
      //     (apparentType !== 'EMAIL_SHA256' && percentWrong > 0.5)){

      if (percentWrong > 0.1){ 
        $scope.audienceWrongType = true;
      }

    };

    $scope.showError = function(result) {
      console.log(result);
    };


    $scope.saveCustomAudienceCopyPaste = function(name, data, schemaType, audienceType){
      var accountId = $scope.selectedAdAccount.accountId;
      var audienceData = $scope.audienceData;
      $scope.submitted = true;
      if(name && data && ($scope.editingMode !== 'add' && $scope.editingMode !== 'remove')){
        if($scope.audienceData.length < 20 ){
          $scope.audienceTooSmall = true;
        } else if($scope.audienceData.length > 10000) {
          $scope.audienceTooLarge = true;
          // $scope.submitted = true;
        } else {
          $scope.audienceTooLarge = false;
          $scope.audienceTooSmall = false;
          $scope.submitted = false;
          $scope.processing = true;
          customAudienceApi.saveCustomAudienceCopyPaste(accountId, $scope.teamId, name, audienceData.toString(), schemaType, audienceType, $scope.showSaving, $scope.showError);          
        }
      } else if (data && $scope.editingMode === 'add') {
        customAudienceApi.addToCustomAudience($scope.customAudienceId, audienceData.toString(), schemaType, $scope.showSaving);
      } else if (data && $scope.editingMode === 'remove') {
        customAudienceApi.removeFromCustomAudience($scope.customAudienceId, audienceData.toString(), schemaType, $scope.showSaving);
      }
    };

    function addMode(audience) {
      $scope.editingMode = 'add';
      $scope.threeStepsMessage = $filter('translate')('label.align.addSocialAccount.Addpeopletocustomaudience') + 
                                 audience.audienceName + 
                                 $filter('translate')('label.align.addSocialAccount.quotein3easysteps');
    }

    function removeMode(audience) {
      $scope.editingMode = 'remove';
      $scope.threeStepsMessage = $filter('translate')('label.align.addSocialAccount.Removepeoplefromcustomaudience') + 
                                 audience.audienceName + 
                                 $filter('translate')('label.align.addSocialAccount.quotein3easysteps');
    }

    function normalMode() {
      $scope.editingMode = 'new';
      $scope.threeStepsMessage = $filter('translate')('label.align.Addyourcustomaudiencein3easysteps');
    }

    $scope.closeSavingView = function(){
      $scope.showSavingView = false;
      normalMode();
    };

    $scope.closeSavingView();

    $scope.addPeople = function (audience) {
      $scope.showSavingView = false;
      $scope.customAudienceId = audience.id;
      $scope.editingAudience = audience;
      addMode(audience);
      Scroll.element('.ngdialog', 350);
    };

    $scope.removePeople = function (audience) {
      $scope.showSavingView = false;
      $scope.customAudienceId = audience.id;
      $scope.editingAudience = audience;
      removeMode(audience);
      Scroll.element('.ngdialog', 350);
    };

    $scope.deleteCustomAudience = function(index, audience, forceDelete){
      forceDelete = forceDelete || false;
      var accountId = $scope.selectedAdAccount.accountId;

      function forceDeleteCustomAudience(){
        customAudienceApi.deleteCustomAudience(accountId, audience.fbAudienceId, true).then(function(response){
          //if no lookalike audiences or if user forces delete, then delete
          if(response.data.data.status === 'deleted'){
            fsConfirm('genericAlert', {body: response.data.data.message});
            $scope.customAudiences.splice(index, 1);
          }
        });
      }


      function confirmDeleteAudience(){
        customAudienceApi.deleteCustomAudience(accountId, audience.fbAudienceId, forceDelete).then(function(response){

          //if no lookalike audiences or if user forces delete, then delete
          if(response.data.data.status === 'deleted'){
            fsConfirm('genericAlert', {body: response.data.data.message});
            $scope.customAudiences.splice(index, 1);
          }
          //otherwise ask user if they want to delete all lookalikeAudiences associateds
          else if(response.data.data.status === 'hasLookalikeAudiences'){
            fsConfirm('generic', {
              body: response.data.data.message
            }).then(forceDeleteCustomAudience);  
          }

        });
      }

      if(!forceDelete){
        fsConfirm('generic', {
          body: $filter('translate')('label.align.Areyousureyouwanttodelete') + audience.audienceName + '?'
        }).then(confirmDeleteAudience);         
      }
    };

    $scope.selectAttachment = function(attachment){
    	$scope.selectedAttachment = attachment;
    };

    $scope.selectDataType = function(dataType){
      $scope.dataType = dataType;
      if(customAudienceForm.data.value){ //jshint ignore:line
        customAudienceForm.data.value = ''; //jshint ignore:line
      } else if($scope.file){
        $scope.file = null;
      }
    };

    $scope.selectFormat = function(){
      $scope.numberOfTargets = 0;
      $scope.audienceTooSmall = false;
      $scope.audienceData = '';
      $scope.customAudienceData = {};
      $scope.customAudienceName = '';
    };


  });
