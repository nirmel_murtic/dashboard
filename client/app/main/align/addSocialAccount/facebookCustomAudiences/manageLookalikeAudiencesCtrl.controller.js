'use strict';

angular.module('align')
  .controller('ManageLookalikeAudiencesCtrl', function ($scope, FbAtpHelper, customAudienceApi, $timeout, $filter, fsConfirm) {
    var sliderCssOptions = {
        background: {'background-color': '#779f79'},
        before: {'background-color': '#779f79'},  // zone before default value
        default: {'background-color': '#779f79'}, // default value: 1px
        after: {'background-color': '#779f79'},  // zone after default value
        pointer: {'background-color': '#779f79'},   // circle pointer
        range: {'background-color': '#779f79'} // use it if double value
  	};

    $scope.moment = moment;

  	var atpHelper = new FbAtpHelper();

    if($scope.selectedAdAccount){
      $scope.countryAtpConfig = atpHelper.getConfig('country', $scope.selectedAdAccount.accountId);
    }
    
    $scope.showError = function(response){
      $scope.spinRefresh = false;
      $scope.processing = true; 
      fsConfirm('genericAlert', {
        body: response.error.message      
      }).then(function(){

      });   
    };
    

    $scope.applyLookalikeAudiences = function (response) {
    	$scope.lookalikeAudiences = _.map(response.data, function(audience) {
    		audience.lookalikeSpec = angular.fromJson(audience.lookalikeSpec);
    		return audience;
    	});
    };

  	$scope.sizeRatio = {value: 10};
    $scope.sizeRatioOptions = {from: 1, to: 20, step: 1, css: sliderCssOptions};

    $scope.initSlider = function () {
      $scope.sliderInited = true;
      $timeout(function(){
        angular.element(angular.element('.jslider-label')[0]).text('Similarity');
        angular.element(angular.element('.jslider-label')[0]).css({
          'margin-top': '14px',
          'margin-left': '-65px',
          'font-size': '0.75em'
        });

        angular.element('.jslider-value').css({
          'font-size': '0.75em',
          'background-color': '#779f79',
          'color': 'white',
          'padding': '0.25em',
          'margin-top': '-0.75em'
        });

        angular.element(angular.element('.jslider-label')[1]).text('Greater Reach');
        angular.element(angular.element('.jslider-label')[1]).css({
          'margin-top': '14px',
          'margin-right': '-90px',
          'font-size': '0.75em'
        });
        if(!angular.element(angular.element('.jslider-value')[0]).text().includes('%')){
          angular.element(angular.element('.jslider-value')[0]).append('%');          
        }
      }, 1000);
    };

    if(!$scope.sliderInited){
      $scope.initSlider();      
    }

    $scope.selectCustomAudience = function(audience){
    	$scope.selectedCustomAudience = audience;
      angular.element('#country_value').focus();
    };

    $scope.closeSavingView = function(){
      $scope.showSavingView = false;
      $scope.initSlider();
    };

    $scope.showSaving = function(){
    	$scope.showSavingView = true;
      $scope.processing = false; 
      delete $scope.selectedCustomAudience;
      delete $scope.country;
      $scope.sizeRatio = {value: 10};
      $scope.initLookalikeAudiences();
    };

    $scope.saveLookalikeAudience = function(customAudience, country, sizeRatio){
    	var accountId = $scope.selectedAdAccount.accountId;
    	var countryCode = country.country_code //jshint ignore:line
    	var name = 'Lookalike' + ' (' + countryCode + ',' + sizeRatio.value + '\%) - ' + customAudience.audienceName;
    	var lookalikeSpec = {
    		ratio: sizeRatio.value / 100,
    		country: countryCode
    	};
    	console.log(customAudience, countryCode);
      $scope.processing = true; 
    	customAudienceApi.saveLookalikeAudience(accountId, $scope.teamId, customAudience.fbAudienceId, name, lookalikeSpec, $scope.showSaving, $scope.showError);
    };

    $scope.deleteLookalikeAudience = function(audience, index){
      var accountId = $scope.selectedAdAccount.accountId;
      function confirmDeleteAudience(){
        customAudienceApi.deleteLookalikeAudience(accountId, audience.fbAudienceId).then(function(response){
          //if no lookalike audiences or if user forces delete, then delete
          if(response.data.data.status === 'deleted'){
            // fsConfirm('genericAlert', {body: response.data.data.message});
          }          
          $scope.lookalikeAudiences.splice(index, 1);
        });
      }

      fsConfirm('generic', {
        body: $filter('translate')('label.align.Areyousureyouwanttodelete') + audience.audienceName + '?'
      }).then(confirmDeleteAudience);         
    };

    $scope.addLocation = function(e, data){
    	$scope.country = data.value.country_code; //jshint ignore:line
    };

    $scope.$on('ngAtp:autocomplete', $scope.addLocation);

  });
