'use strict';

angular.module('main')
  .service('customAudienceApi', function ($http, apiUrl, $upload) {
    return {
      getAllCustomAudiences: function(accountId, teamId, success, error, offset, limit){
        limit = limit || 21;
        offset = offset || 0;
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/get/all/' + accountId +'?teamId=' + teamId + '&audienceType=CUSTOM_AUDIENCE' + '&offset=' + offset + '&limit=' + limit
        })
        .success(success)
        .error(error);
      },

      syncAudiences: function(accountId, teamId, success, error){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/sync/all/' + accountId +'?teamId=' + teamId
        })
        .success(success)
        .error(error);
      },

      forceSyncAudiences: function(accountId, teamId, success, error){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/sync/all/' + accountId +'?teamId=' + teamId + '&forceSync=true' 
        })
        .success(success)
        .error(error);
      },

      getAllLookalikeAudiences: function(accountId, teamId, success, error, offset, limit){
        limit = limit || 21;
        offset = offset || 0;

        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/get/all/' + accountId +'?teamId=' + teamId + '&audienceType=LOOKALIKE_AUDIENCE' + '&offset=' + offset + '&limit=' + limit
        })
        .success(success)
        .error(error);
      },


      getAllAcceptedCustomAudiences: function(accountId, teamId, success, error){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/get/all/' + accountId + '?teamId=' + teamId + '&audienceType=CUSTOM_AUDIENCE' + '&audienceStatus=ACCEPTED&limit=2000'
        })
        .success(success)
        .error(error);
      },

       getAllEligibleCustomAudiences: function(accountId, teamId, success, error){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/get/all/' + accountId + '?teamId=' + teamId + '&audienceType=CUSTOM_AUDIENCE' + '&audienceStatus=ACCEPTED&limit=2000&minAudienceSize=100'
        })
        .success(success)
        .error(error);
      },

      getAllAcceptedLookalikeAudiences: function(accountId, teamId, success, error){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/customAudience/get/all/' + accountId + '?teamId=' + teamId + '&audienceType=LOOKALIKE_AUDIENCE' + '&audienceStatus=ACCEPTED&limit=2000'
        })
        .success(success)
        .error(error);
      },

      saveLookalikeAudience: function  (accountId, teamId, customAudienceId, name,  lookalikeSpec, success, error) {
        $upload.upload({
          url: apiUrl + '/api/v2/campaign/customAudience/create/fbAudience/' + accountId + '?teamId=' + teamId + '&name=' + encodeURI(name) + '&customAudienceId=' + customAudienceId + '&lookalikeSpec=' + angular.toJson(lookalikeSpec) + '&audienceType=LOOKALIKE_AUDIENCE' + '&isFileUploaded=false',
          file: undefined
        })
        .success(success)
        .error(error);
      },

      saveCustomAudienceCopyPaste: function(accountId, teamId, name, data, schemaType, audienceType, success){
        $upload.upload({
          url: apiUrl + '/api/v2/campaign/customAudience/create/fbAudience/' + accountId + '?teamId=' + teamId + '&name=' + encodeURIComponent(name) + '&schemaType=' + schemaType + '&audienceType=' + audienceType + '&isFileUploaded=false',
          data: {
            rawAudienceEntries: angular.toJson(data)
          },
          file: undefined
        })
        .success(success)
        .error(function(error){
          console.log(error);
        });
      },

      saveCustomAudienceFile: function(accountId, teamId, name, schemaType, audienceType, file, success, error){
        //console.log(file);
        $upload.upload({
          url: apiUrl + '/api/v2/campaign/customAudience/create/fbAudience/' + accountId + '?teamId=' + teamId + '&name=' + name + '&schemaType=' + schemaType + '&audienceType=' + audienceType + '&isFileUploaded=true',
          file: file
        })
        .success(success)
        .error(error);
      },

      deleteCustomAudience: function(accountId, fbAudienceId, forceDelete){
       return $http({
          method: 'POST',
          url: apiUrl + '/api/v2/campaign/customAudience/delete/fbAudience/' + accountId + '?audienceId=' + fbAudienceId + '&isForceDelete=' + forceDelete
        });        
      },
      deleteLookalikeAudience: function(accountId, fbAudienceId){
        return $http({
          method: 'POST',
          url: apiUrl + '/api/v2/campaign/customAudience/delete/fbAudience/' + accountId + '?audienceId=' + fbAudienceId
        });
      },

      addToCustomAudience: function(customAudienceId, data, schemaType, success){
        // if(uploadMethod === 'copyPaste') {
          $upload.upload({
            url: apiUrl + '/api/v2/campaign/customAudience/update/fbAudience/' + customAudienceId + '?audienceAction=' + 'add' + '&isFileUploaded=' + 'false',
            data: {
              rawAudienceEntries: angular.toJson(data)
            },
            file: undefined
          })
          .success(success)
          .error(function(error){
            console.log(error);
          });
        // } else if (uploadMethod === 'csv') {
        //   $upload.upload({
        //     url: apiUrl + '/api/v2/campaign/customAudience/update/fbAudience/' + customAudienceId + '&audienceAction=' + 'add' + '&isFileUploaded=' + 'true',
        //     file: data
        //   })
        //   .success(success)
        //   .error(function(error){
        //     console.log(error);
        //   });
        // }
      },

      removeFromCustomAudience: function(customAudienceId, data, schemaType, success){
        //if(uploadMethod === 'copyPaste') {
          $upload.upload({
            url: apiUrl + '/api/v2/campaign/customAudience/update/fbAudience/' + customAudienceId + '?audienceAction=' + 'remove' + '&isFileUploaded=' + 'false',
            data: {
              rawAudienceEntries: angular.toJson(data)
            },
            file: undefined
          })
          .success(success)
          .error(function(error){
            console.log(error);
          });
        // } else if (uploadMethod === 'csv') {
        //   $upload.upload({
        //     url: apiUrl + '/api/v2/campaign/customAudience/update/fbAudience/' + customAudienceId + '&audienceAction=' + 'remove' + '&isFileUploaded=' + 'true',
        //     file: data
        //   })
        //   .success(success)
        //   .error(function(error){
        //     console.log(error);
        //   });
        // }
      }
    };
  });
