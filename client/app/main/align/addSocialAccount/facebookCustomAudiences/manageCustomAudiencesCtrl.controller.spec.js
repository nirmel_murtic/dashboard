'use strict';

describe('Controller: ManageCustomAudiencesCtrl', function () {

  // load the controller's module
  beforeEach(module('align'));

  var ManageCustomAudiencesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManageCustomAudiencesCtrl = $controller('ManageCustomAudiencesCtrl', {
      $scope: scope
    });
  }));

});
