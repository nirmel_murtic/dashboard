'use strict';

describe('Controller: ManageLookalikeAudiencesCtrl', function () {

  // load the controller's module
  beforeEach(module('dashboardApp'));

  var ManageLookalikeAudiencesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManageLookalikeAudiencesCtrl = $controller('ManageLookalikeAudiencesCtrl', {
      $scope: scope
    });
  }));
});
