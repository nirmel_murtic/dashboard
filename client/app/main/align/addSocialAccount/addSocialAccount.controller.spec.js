'use strict';

describe('Controller: AddsocialaccountCtrl', function () {

  // load the controller's module
  beforeEach(module('align'));

  var AddsocialaccountCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddsocialaccountCtrl = $controller('AddsocialaccountCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
