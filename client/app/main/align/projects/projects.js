'use strict';

angular.module('align')
  .config(function ($stateProvider) {
    var prepareNewProject = function(projectService, auth, $q, $stateParams) {
      var loadGoals = projectService.getGoals(function(result) {
        $stateParams.goals = result.data.goals;
      }, function(error) {
        console.log(error.error.message);
      });

      var waitFor = [loadGoals]; // goals is cached so we can always wait for it

      return $q.all(waitFor);
    };

    var checkFirstProject = function(auth, projectService, $state, fsConfirm){
      var project = {
        name: 'My Project',
        description: '',
        teamId: auth.user.currentTeam.id,
        attachments: [],
        goals: []
      };

      if (auth.user.currentTeam.projects && !auth.user.currentTeam.projects.length) {
        return projectService.addOrUpdateProject(project, function (result) {
          auth.user.currentTeam.projects.push(result.data.project);
        }, function (error) {
          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
      }
    };

    $stateProvider
      .state('main.align.projects', {
        permissions: '<LOGGED_IN> && <ALIGN>',
        url: '/project',
        views: {
          'alignMain@main.align': {
            templateUrl: 'app/main/align/projects/projects.html',
            controller: 'ProjectsCtrl'
          }
        },
        data: {
          displayName: 'Projects'
        },
        loadSocialAccounts: function(auth, teamService) {
          return teamService.getTeamSocialAccounts(auth.user.currentTeam.id, function(result) {
            auth.user.currentTeam.socialAccounts = result.data.team.socialAccounts;
          }, function(error) {
            console.log(error.error.message);
          });
        },
        loadProjects: function(auth, projectService) {
          return projectService.getProjects(auth.user.currentTeam.id, function (result) {
            auth.user.currentTeam.projects = result.data.projects;
          }, function (error) {
            console.log(error.error.message);
          });
        },
        loadPixels: function(auth, pixelService) {
          pixelService.getPixels(auth.user.currentTeam.id, function(result) {
            auth.user.currentTeam.pixels = result.data.URLS;
          }, function(error) {
            console.log('Error while loading pixels: ' + error);
          });
        },
        resolve: {
          prepare: prepareNewProject,
          check: checkFirstProject
        }
      });
  });
