'use strict';

angular.module('align')
  .controller('ProjectsCtrl', function ($scope, apiUrl, projectService, $filter, $stateParams, $state, $rootScope, $window, $auth, fsConfirm, $timeout, pixelService, teamService, auth, ngDialog, emailSetting, subscribePlan, billingService, microService, $sce) {

    $scope.projectOverview = false;

    $scope.newProjectName = '';

    $scope.newAppId = '';

    $scope.apiUrl = apiUrl;

    if(subscribePlan) {
      $timeout(function() {
        billingService.getAccount(function (result) {
          $scope.accountSettingsAccount = result.data;
          $scope.openPlansFirst = true;

          var dialog = ngDialog.open({
            template: 'app/main/align/accountSettings/accountSettings.html',
            controller: 'AccountSettingsCtrl',
            className: 'modal modal-members account-settings-modal',
            id: 'account-settings-dialog',
            scope: $scope
          });

          $scope.accountSettingsDialog = dialog;

        }, function (error) {
          console.log(error.error);
        });
      }, 1000);

      $scope.$on('ngDialog.closed', function (e, $dialog) {
        if($scope.subscribeToPlan && $scope.accountSettingsDialog.id === $dialog.attr('id')) {
          delete $scope.subscribeToPlan;

          ngDialog.open({
            template: 'app/main/align/accountSettings/subscribeToPlan/subscribeToPlan.html',
            controller: 'SubscribetoplanCtrl',
            className: 'modal modal-members subscribe-to-plan-modal',
            scope: $scope
          });
        }
      });
    }

    if(emailSetting) {
      $timeout(function() {
        ngDialog.open({
          template: 'app/main/navbar/myUserSettings/myUserSettings.html',
          controller: 'MyusersettingsCtrl',
          className: 'modal modal-members account-settings-modal'
        });
      }, 1000);
    }

    $scope.toTitleCase = function(string) {
      string = string.replace(/_/g, ' ');
      return string.replace(/\w\S*/g, function(word){
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
      });
    };

    $scope.$watch('fractalAccountInfo.currentTeam.currentProject', function (newValue) {
      if(newValue) {
        $scope.currentProject = _.find($rootScope.fractalAccountInfo.currentTeam.projects, {'id': newValue.id});
      }
    });

    $scope.openFacebookPixelUrl = function(){
       $window.open('//facebook.com/ads/manager/pixel/facebook_pixel/');
    };

    $scope.headerWidth = '70%';

    $scope.trimDropdown = function(value) {
      if(value && $scope.headerWidth === '70%') {
        $scope.headerWidth = '60%';
      } else if(!value && $scope.headerWidth === '60%'){
        $scope.headerWidth = '70%';
      }
    };

    $scope.resetSelectedGoals = function() {
      $scope.selectedSiteGoal = [];

      $scope.selectedSocialGoal = [];

      $scope.selectedAppGoal = [];
    };

    $scope.resetGoals = function() {
      $scope.resetSelectedGoals();

      $scope.radioOption = false;

      $scope.socialDisable = true;
      $scope.siteDisable = true;
      $scope.appDisable = true;
    };

    $scope.onSwitchTeam = function(team) {
      if(!team.socialAccounts && $state.current.loadSocialAccounts) {
        $state.current.loadSocialAccounts(auth, teamService);
        $scope.resetGoals();
      }

      var checkFirstProject = function() {
        if (auth.user.currentTeam.projects.length) {
          $scope.currentProject = $scope.fractalAccountInfo.currentTeam.currentProject ? $scope.fractalAccountInfo.currentTeam.currentProject : auth.user.currentTeam.projects[0];
          auth.user.currentTeam.currentProject = $scope.currentProject;

          $scope.prepareProjects();
        } else {
          if(!$rootScope.createProjectCalled) {
            $rootScope.createProjectCalled = true;

            var project = {
              name: 'My Project',
              description: '',
              teamId: auth.user.currentTeam.id,
              attachments: [],
              goals: []
            };

            projectService.addOrUpdateProject(project, function (result) {
              auth.user.currentTeam.projects.push(result.data.project);

              $scope.currentProject = $scope.fractalAccountInfo.currentTeam.currentProject ? $scope.fractalAccountInfo.currentTeam.currentProject : auth.user.currentTeam.projects[0];
              auth.user.currentTeam.currentProject = $scope.currentProject;

              delete $rootScope.createProjectCalled;
            }, function (error) {
              delete $rootScope.createProjectCalled;

              fsConfirm('genericAlert', {
                body: error.error.message
              });
            });
          }
        }
      };

      if(!team.projects && $state.current.loadProjects) {
        $state.current.loadProjects(auth, projectService).then(checkFirstProject);
      } else {
        checkFirstProject();
      }

      if(!team.pixels && $state.current.loadPixels) {
        $state.current.loadPixels(auth, pixelService);
      }
    };

    $scope.$on('switch-team', function(event, team) {
      $scope.onSwitchTeam(team);
    });

    $scope.prepareProjects = function() {
      $scope.fractalAccountInfo.currentTeam.projects.sort(function (a, b) {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      });

      $scope.currentProject = $scope.fractalAccountInfo.currentTeam.currentProject ? $scope.fractalAccountInfo.currentTeam.currentProject : auth.user.currentTeam.projects[0];
      $scope.fractalAccountInfo.currentTeam.currentProject = $scope.currentProject;
    };

    if($scope.fractalAccountInfo.currentTeam.projects) {
      $scope.prepareProjects();
    } else {
      $scope.onSwitchTeam($scope.fractalAccountInfo.currentTeam);
    }

    $scope.changeProject = function (project, skipCloseDropdown) {
      $scope.resetGoals();
      $scope.currentProject = project;
      $scope.fractalAccountInfo.currentTeam.currentProject = $scope.currentProject;

      if(!skipCloseDropdown) {
        $rootScope.$broadcast('closeDropdown');
      }
    };

    $scope.editProject = function(project) {
      $state.go('main.align.projects.editProject', { projectId: project.id });
    };

    delete $scope.disabledInput;

    $scope.createNewProject = function(newProjectName) {

      if(!$auth.checkPermissions('<MANAGE_GOALS>')) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.common.YouhaveViewOnlyAccessforthisFeature')
        });
        return;
      }

      $scope.disabledInput = true;
      if(!newProjectName.trim()) {
        delete $scope.disabledInput;
        return;
      }

      var project = {
        name: newProjectName,
        description: '',
        teamId: auth.user.currentTeam.id,
        attachments: [],
        goals: []
      };

      projectService.addOrUpdateProject(project, function (result) {
        auth.user.currentTeam.projects.push(result.data.project);

        $scope.fractalAccountInfo.currentTeam.currentProject = result.data.project;
        auth.user.currentTeam.currentProject = $scope.fractalAccountInfo.currentTeam.currentProject;

        delete $scope.disabledInput;
      }, function (error) {
        fsConfirm('genericAlert', {
          body: error.error.message
        });
        delete $scope.disabledInput;
      });
    };

    $scope.openNameEdit = function() {
      $scope.loadedProject = true;
      $scope.showProjectNameEdit = !$scope.showProjectNameEdit;
      $scope.originalProjectName = $scope.currentProject.name;
    };

    $scope.saveProjectName = function(name) {

      if(!$auth.checkPermissions('<MANAGE_GOALS>')) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.common.YouhaveViewOnlyAccessforthisFeature')
        });
        return;
      }

      if(!name) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.alignProjects.PleaseEnterProjectName')
        });
        return;
      }

      var projectData = $scope.currentProject;

      projectData.name = name;

      projectService.addOrUpdateProject(projectData, function(result) {

          $scope.currentProject = result.data.project;

          $scope.openNameEdit();

      }, function(error) {
        fsConfirm('genericAlert', {
          body: error.error.message
        });
      });
    };

    $scope.missingField = false;

    $scope.checkMissingField = function() {
      if($scope.radioOption === 'trackedSites') {
        if(!$scope.selectedSiteGoal.goal.id || !$scope.selectedSiteGoal.host ||
          ($scope.selectedSiteGoal.goal.id === 4 && !$scope.selectedSiteGoal.eventName && !$scope.selectedSiteGoal.thirdPartyPixel) ||
          (($scope.selectedSiteGoal.goal.id === 3 || $scope.selectedSiteGoal.goal.id === 4) && !$scope.selectedSiteGoal.tracker)) {
          $scope.showMissing();
        }
      } else if($scope.radioOption === 'socialChannelGoals') {
        if(!$scope.selectedSocialGoal.goal.id || !($scope.selectedSocialGoal.pageId || $scope.selectedSocialGoal.pageName) || !$scope.selectedSocialGoal.socialChannel) {
          $scope.showMissing();
        }
      } else if($scope.radioOption === 'appIdsGoals') {
        if(!$scope.selectedAppGoal.goal.id || !$scope.selectedAppGoal.applicationId) {
          $scope.showMissing();
        }
      }
    };

    $scope.showMissing = function () {
      $scope.missingField = true;
      $timeout(function() {
        $scope.missingField = false;
      }, 3000);
    };

    $scope.addToGoals = function() {
      //missing field check
      $scope.checkMissingField();
      if($scope.missingField){
        return;
      }

      if(!$auth.checkPermissions('<MANAGE_GOALS>')) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.common.YouhaveViewOnlyAccessforthisFeature')
        });
        return;
      }

      var g;
      $scope.processing = true;
      var projectSocialData;

      if($scope.selectedSiteGoal.goal && $scope.selectedSiteGoal.host) {
        var projectSiteData = $scope.currentProject;

        if($scope.selectedSiteGoal.goal.name === 'WEBSITE_CONVERSIONS' && $scope.selectedSiteGoal.eventName) {
          g = {
            goalId: $scope.selectedSiteGoal.goal.id,
            host: $scope.selectedSiteGoal.host,
            tracker: $scope.selectedSiteGoal.tracker.key,
            event: $scope.selectedSiteGoal.eventName.event,
            notReady: true
          };

          if($scope.selectedSiteGoal.eventName.eventUrl){
            g.eventUrl = $scope.selectedSiteGoal.eventName.eventUrl;
          }

          projectSiteData.goals.push(g);
        } else if ($scope.selectedSiteGoal.goal.name === 'WEBSITE_CLICKS' && $scope.selectedSiteGoal.tracker) {

          g = {
            goalId: $scope.selectedSiteGoal.goal.id,
            host: $scope.selectedSiteGoal.host,
            tracker: $scope.selectedSiteGoal.tracker.key,
            notReady: true
          };
          projectSiteData.goals.push(g);
        } else if ($scope.selectedSiteGoal.goal.id === 4 && $scope.selectedSiteGoal.thirdPartyPixel) {
          g = {
            goalId: $scope.selectedSiteGoal.goal.id,
            host: $scope.selectedSiteGoal.host,
            tracker: $scope.selectedSiteGoal.tracker.key,
            thirdPartyPixel: $scope.selectedSiteGoal.thirdPartyPixel,
            notReady: true
          };
          projectSiteData.goals.push(g);
        } else if($scope.selectedSiteGoal.goal.name === 'WEBSITE_CONVERSIONS' || $scope.selectedSiteGoal.goal.id === 'WEBSITE_CLICKS') {
          delete $scope.processing;
          return;
        }

        $scope.updateProjectGoals(projectSiteData);
      } else if($scope.selectedSocialGoal.socialChannel === 'Twitter'){

        projectSocialData = $scope.currentProject;
        projectSocialData.goals.push({
          goalId: $scope.selectedSocialGoal.goal.id,
          twitterPromotableUserId: $scope.selectedSocialGoal.accountId,
          notReady: true
        });
        $scope.updateProjectGoals(projectSocialData);
      } else if($scope.selectedSocialGoal.goal && $scope.selectedSocialGoal.pageId && $scope.selectedSocialGoal.socialChannel) {
        projectSocialData = $scope.currentProject;
        projectSocialData.goals.push({
          goalId: $scope.selectedSocialGoal.goal.id,
          pageId: $scope.selectedSocialGoal.pageId,
          notReady: true
        });

        $scope.updateProjectGoals(projectSocialData);
      } else if($scope.selectedAppGoal.goal && $scope.selectedAppGoal.applicationId){
        var projectAppData = $scope.currentProject;

        projectAppData.goals.push({
          goalId: $scope.selectedAppGoal.goal.id,
          applicationId: $scope.selectedAppGoal.applicationId,
          notReady: true
        });

        $scope.updateProjectGoals(projectAppData);
      } else {
        delete $scope.processing;
      }
    };

    $scope.updateProjectGoals = function(projectData) {
        projectService.addOrUpdateProject(projectData, function(result) {
          delete $scope.processing;

          var updatedProject = result.data.project;
          $scope.currentProject.goals = _.sortBy(updatedProject.goals, function (goal) {
                                          return -goal.id;
                                        });

          $scope.resetGoals();
        }, function(error) {
          var filterGoals = [];

          angular.forEach(projectData.goals, function(goal) {
            if(!goal.notReady) {
              filterGoals.push(goal);
            }
          });

          projectData.goals = filterGoals;

          delete $scope.processing;
          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
    };

    $scope.socialDisable = true;
    $scope.siteDisable = true;
    $scope.appDisable = true;

    $scope.selectedSiteGoal = [];

    $scope.selectedSocialGoal = [];

    $scope.selectedAppGoal = [];

    $scope.showSingleOption = function(type) {
      if(type === 'trackedSites') {
        $scope.chooseKPI($scope.displayingGoalNames[2]);
        if($scope.fractalAccountInfo.currentTeam.pixels && $scope.fractalAccountInfo.currentTeam.pixels.length === 1) {
          $scope.chooseProperty($scope.fractalAccountInfo.currentTeam.pixels[0]);
        }
      } else if(type === 'socialChannelGoals'){
        if($scope.displaySocialPages.length === 1) {
          $scope.chooseProperty($scope.displaySocialPages[0]);
        }
      } else if(type === 'appIdsGoals') {
        $scope.chooseKPI($scope.displayingGoalNames[4]);
        if($scope.fractalAccountInfo.currentTeam.applications && $scope.fractalAccountInfo.currentTeam.applications.length === 1) {
          $scope.chooseProperty($scope.fractalAccountInfo.currentTeam.applications[0]);
        }
      }
    };

    $scope.$watchCollection('fractalAccountInfo.currentTeam.socialAccounts', function(newAccounts, oldAccounts) {
      if(newAccounts !== oldAccounts) {
        if(!$scope.siteDisable) {
          $scope.getThirdPartyPixels();
        }

        if($scope.selectedSocialGoal.socialChannel) {
          $scope.socialChannelSelect($scope.selectedSocialGoal.socialChannel);
        }
      }
    });

    $scope.getThirdPartyPixels = function(){
      var socialAccounts = $rootScope.fractalAccountInfo.currentTeam.socialAccounts;
      var fbAccounts;
      for(var i = 0; i < socialAccounts.length; i++){
        if(socialAccounts[i].fbAccounts.length){
          fbAccounts = socialAccounts[i].fbAccounts;
        }
      }
      $scope.thirdPartyPixels = [];
      _.map(fbAccounts, function(acct){
        if(acct.thirdPartyPixels){
          for(var j = 0; j < acct.thirdPartyPixels.length; j++){
            $scope.thirdPartyPixels.push(acct.thirdPartyPixels[j]);
          }
        }
      });

      // TEST DATA FOR CONVERSION PIXELS DROP MENU
      // note: delete

      /*$scope.thirdPartyPixels = [
        {
          "name": "FS-ENG-SF1's Pixel",
          "removed": false,
          "channelAccountId": 2,
          "fbPixelVersion": "FB_NEW_PIXEL",
          "fbEventType": "FB_STANDARD_EVENT",
          "customEventType": "OTHER",
          "accompliceId": 219,
          "id": 784456331673615
        },
        {
          "name": "FS-ENG-SF1's Pixel",
          "removed": false,
          "channelAccountId": 2,
          "fbPixelVersion": "FB_NEW_PIXEL",
          "fbEventType": "FB_STANDARD_EVENT",
          "customEventType": "INITIATED_CHECKOUT",
          "accompliceId": 217,
          "id": 784456331673615
        },
        {
          "name": "FS-ENG-SF1's Pixel",
          "removed": false,
          "channelAccountId": 2,
          "fbPixelVersion": "FB_NEW_PIXEL",
          "fbEventType": "FB_STANDARD_EVENT",
          "customEventType": "CONTENT_VIEW",
          "accompliceId": 216,
          "id": 784456331673615
        },
        {
          "name": "FS-ENG-SF1's Pixel",
          "removed": false,
          "channelAccountId": 2,
          "fbPixelVersion": "FB_NEW_PIXEL",
          "fbEventType": "FB_CUSTOM_EVENT",
          "customEventId": 1390250561276487,
          "customEventName": "ccc",
          "pixelRule": "{\"url\":{\"i_contains\":\"ggg\"}}",
          "customEventType": "ADD_TO_CART",
          "accompliceId": 224,
          "id": 784456331673615
        }
      ];*/

      if($scope.selectedSiteGoal.thirdPartyPixel) {
        if(!_.find($scope.thirdPartyPixels, {'id': $scope.selectedSiteGoal.thirdPartyPixel.id})) {
          delete $scope.selectedSiteGoal.thirdPartyPixel;
        }
      }
    };

    $scope.thirdPartyPixelSelect = function(pixel){
      $scope.selectedSiteGoal.thirdPartyPixel = pixel;
    };

    $scope.selectByText = function(value) {
      $scope.radioOption = value;
      $scope.selectedGoalType(value);
    };

    $scope.selectedGoalType = function(value) {
      if(value === 'trackedSites' && $scope.siteDisable) {
        $scope.socialDisable = true;
        $scope.siteDisable = false;
        $scope.appDisable = true;

        $scope.getThirdPartyPixels();

        $scope.resetSelectedGoals();
        $scope.showSingleOption(value);

      } else if(value === 'socialChannelGoals' && $scope.socialDisable) {
        $scope.socialDisable = false;
        $scope.siteDisable = true;
        $scope.appDisable = true;
        $scope.resetSelectedGoals();

        $scope.socialChannelSelect('Facebook');

        if($scope.displaySocialPages) {
          $scope.showSingleOption(value);
        }

        $scope.chooseKPI($scope.displayingGoalNames[0]);
      } else if(value === 'appIdsGoals' && $scope.appDisable) {
        $scope.socialDisable = true;
        $scope.siteDisable = true;
        $scope.appDisable = false;
        $scope.resetSelectedGoals();
        $scope.showSingleOption(value);

      }
    };

    $scope.getGoalByName = function(id) {
      $scope.idGoal = _.find($stateParams.goals, {'name': id});

      if($scope.idGoal) {
        return $scope.idGoal.id;
      }

    };

    $scope.displayingGoalNames = [
      {
        id: $scope.getGoalByName('POST_ENGAGEMENT'),
        name: 'POST_ENGAGEMENT',
        displayName: 'Post Engagement'
      },
      {
        id: $scope.getGoalByName('PAGE_LIKES'),
        name: 'PAGE_LIKES',
        displayName: 'Page Likes'
      },
      {
        id: $scope.getGoalByName('WEBSITE_CLICKS'),
        name: 'WEBSITE_CLICKS',
        displayName: 'Visits to My Site'
      },
      {
        id: $scope.getGoalByName('WEBSITE_CONVERSIONS'),
        name: 'WEBSITE_CONVERSIONS',
        displayName: 'Conversions'
      },
      {
        id: $scope.getGoalByName('MOBILE_APP_INSTALLS'),
        name: 'MOBILE_APP_INSTALLS',
        displayName: 'App Installs'
      },
      {
        id: $scope.getGoalByName('MOBILE_APP_ENGAGEMENT'),
        name: 'MOBILE_APP_ENGAGEMENT',
        displayName: 'App Engagement'
      },
      {
        id: $scope.getGoalByName('OFFER_CLAIMS'),
        name: 'OFFER_CLAIMS',
        displayName: 'Offer Claim'
      },
      {
        id: $scope.getGoalByName('VIDEO_VIEWS'),
        name: 'VIDEO_VIEWS',
        displayName: 'Video Views'
      },
      {
        id: $scope.getGoalByName('TWEET_ENGAGEMENT'),
        name: 'TWEET_ENGAGEMENT',
        displayName: 'Tweet Engagement'
      },
      {
        id: $scope.getGoalByName('GROW_FOLLOWERS'),
        name: 'GROW_FOLLOWERS',
        displayName: 'Followers'
      }
    ];

    $scope.displaySiteEvents = [];
    $scope.displaySitePages = [];

    $scope.displaySiteTrackers = [{
      key: 'AD_CHANNELS',
      displayName: 'Ad Channels'
    },
    {
      key: 'ACCOMPLICE',
      displayName: 'Accomplice Tracking Script'
    }];

    $scope.displayConversionSiteTrackers = [{
      key: 'AD_CHANNELS',
      displayName: 'Facebook Conversion Pixel'
    },
    {
      key: 'ACCOMPLICE',
      displayName: 'Accomplice Tracking Script'
    },
    {
      key: 'ACCOMPLICE_CONVERSION_PIXEL',
      displayName: 'Accomplice Conversion Pixel'
    }];

    $scope.getSiteTrackerByKey = function(key) {
      var result = null;

      angular.forEach($scope.displaySiteTrackers, function(tracker) {
        if(tracker.key === key) {
          result = tracker;

          return true;
        }
      });

      angular.forEach($scope.displayConversionSiteTrackers, function(tracker) {
        if(tracker.key === key) {
          result = tracker;

          return true;
        }
      });


      return result;
    };

    $scope.checkIfPropertyContainsTrackingScript = function (host) {

      microService.urlValidation(
        {
          host: host,
          testUrl: host,
          index: 100,
          nowait: true,
          nocache: true,
          badurlonly: false
        },
        function (response) {
          if (response && response.status === 'ERROR') {
            $scope.displaySiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = 'no-service';
            $scope.displayConversionSiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = 'no-service';

          } else {
            var url = response.api || {};
            var newState = (url.found ? 'valid-url' : 'invalid-url');
            $scope.displaySiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = newState;
            $scope.displayConversionSiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = newState;
          }
        },
        function (error) {
          console.log('webservice ERROR: ', error);
          $scope.displaySiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = 'no-service';
          $scope.displayConversionSiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = 'no-service';
        });
    };

    $scope.scriptStateString = {
      'invalid-url': '<i class="fa fa-exclamation-triangle" title="No tracking script discovered"></i>',
      'valid-url': '<i class="fa fa-check-circle" title="Tracking script is present" ></i>',
      'checking-url': '<i class="fa fa-spinner" title="Validating..."></i>',
      'no-service': '<i class="fa fa-circle-thin" title="Validation service not available"></i>',
      '':''
    };
    $scope.trust = $sce.trustAsHtml;

    $scope.chooseProperty = function(property, skipCloseDropdown) {
      if(property.host) {

        // on-the-fly script validation for this property
        var hostState = ($rootScope.urlState[property.host] ? $rootScope.urlState[property.host].state : false) ;
        if (hostState) {
          // find the ACC entry under "trackers" and set the state to last known state for the host
          $scope.displaySiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = hostState;
          $scope.displayConversionSiteTrackers.filter(function(o){return o.key==='ACCOMPLICE';})[0].state = hostState;
        } else {
          // this would be rare, but for whatever reason go ahead and run the validation test
          $scope.checkIfPropertyContainsTrackingScript(property.host);
        }

        $scope.displayClickEvents = [];
        $scope.displayUrlEvents = [];
        $scope.displayConversionEvents = [];
        $scope.displaySitePages = [];
        delete $scope.selectedSiteGoal.eventName;
        delete $scope.selectedSiteGoal.tracker;
        $scope.chooseKPI($scope.displayingGoalNames[2]);
        $scope.selectedSiteGoal.host = property.host;
        pixelService.getEvents($scope.selectedSiteGoal.host, $scope.fractalAccountInfo.currentTeam.id, function(result) {
           for(var i=0;i<result.data.Event.length; i++) {
             if(result.data.Event[i].eventModel.event === 'click') {
               $scope.displayClickEvents.push({
                 event: result.data.Event[i].eventModel.eventName
               });
             } else if(result.data.Event[i].eventModel.event === 'url') {
               $scope.displayUrlEvents.push({
                 event: result.data.Event[i].eventModel.eventName,
                 eventUrl: result.data.Event[i].eventModel.selector
               });
             } else if(result.data.Event[i].eventModel.event === 'pconv') {
               $scope.displayConversionEvents.push({
                 event: result.data.Event[i].eventModel.eventName
               });
             }
           }
        }, function(error) {
          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
        //twitter
      } else if(property.userId) {
        $scope.selectedSocialGoal.accountId = property.id;
        $scope.selectedSocialGoal.pageName = property.screenName;
        //app
      } else if(property.channelApplications) {
        $scope.selectedAppGoal.applicationId = property.id;
        $scope.selectedAppGoal.appName = property.name;

        if(!skipCloseDropdown) {
          $rootScope.$broadcast('closeDropdown');
        }
        //facebook
      } else if(property.pageId){
        $scope.selectedSocialGoal.pageId = property.pageId;
        $scope.selectedSocialGoal.pageName = property.pageName;
      }
    };

    $scope.chooseKPI = function(entity) {
      delete $scope.selectedSiteGoal.eventName;
      delete $scope.selectedSiteGoal.tracker;

      if(entity.name === 'WEBSITE_CLICKS' || entity.name === 'WEBSITE_CONVERSIONS') {
        $scope.selectedSiteGoal.goal = entity;
      } else if(entity.name === 'POST_ENGAGEMENT' || entity.name === 'PAGE_LIKES' || entity.name === 'VIDEO_VIEWS' ||
        entity.name === 'TWEET_ENGAGEMENT' || entity.name === 'GROW_FOLLOWERS' || entity.name === 'OFFER_CLAIMS') {
        $scope.selectedSocialGoal.goal = entity;
      } else if(entity.name === 'MOBILE_APP_INSTALLS' || entity.name === 'MOBILE_APP_ENGAGEMENT') {
        $scope.selectedAppGoal.goal = entity;
      }
    };

    $scope.socialChannelSelect = function(channel) {
      delete $scope.displaySocialPages;
      $scope.selectedSocialGoal.socialChannel = channel;

      var currentTeam = $scope.fractalAccountInfo.currentTeam;

      if(channel === 'Facebook') {
        $scope.displaySocialPages = [];
        $scope.selectedSocialGoal.goal = _.find($scope.displayingGoalNames, {'name': 'POST_ENGAGEMENT'});
        delete $scope.selectedSocialGoal.pageId;
        delete $scope.selectedSocialGoal.pageName;
        for(var i=0; i<currentTeam.socialAccounts.length; i++) {
          if(currentTeam.socialAccounts[i].social.socialType === channel) {
            $scope.displaySocialPages = currentTeam.socialAccounts[i].pages;
          }
        }
      } else if(channel === 'Twitter') {
        $scope.displaySocialPages = [];
        $scope.selectedSocialGoal.goal = _.find($scope.displayingGoalNames, {'name': 'TWEET_ENGAGEMENT'});
        delete $scope.selectedSocialGoal.pageId;
        delete $scope.selectedSocialGoal.pageName;

        angular.forEach(currentTeam.socialAccounts, function(socialAccount) {
          if(socialAccount.social.socialType === channel) {
            angular.forEach(socialAccount.twAccounts, function(twAccount) {
              angular.forEach(twAccount.promotableUsers, function(promotableUser) {
                if(!promotableUser.removed) {
                  $scope.displaySocialPages.push(promotableUser);
                }
              });
            });
          }
        });
      }

      if($scope.selectedSocialGoal.pageId) {
        if(!_.find($scope.displaySocialPages, {'pageId': $scope.selectedSocialGoal.pageId})) {
          delete $scope.selectedSocialGoal.pageId;
          delete $scope.selectedSocialGoal.pageName;
        }
      }
    };

    $scope.siteEventSelect = function(event) {
      $scope.selectedSiteGoal.eventName = event;

      if($scope.selectedSiteGoal.goal.name === 'WEBSITE_CLICKS') {
        $scope.selectedSiteGoal.tracker = $scope.displaySiteTrackers[1];
      }
    };

    $scope.siteTrackerSelect = function(tracker) {
      if($scope.selectedSiteGoal.tracker && (tracker.key === $scope.selectedSiteGoal.tracker.key)) {
        return;
      }

      $scope.selectedSiteGoal.tracker = tracker;

      if($scope.selectedSiteGoal.eventName || $scope.selectedSiteGoal.thirdPartyPixel) {
        delete $scope.selectedSiteGoal.eventName;
        delete $scope.selectedSiteGoal.thirdPartyPixel;
      }
    };

    $scope.deleteGoalCall = function(goal, confirmCallback) {
      if(goal.tracker) {
        goal.trackerDisplayName = $scope.getSiteTrackerByKey(goal.tracker).displayName;
      }

      fsConfirm('generic', {
        title: $filter('translate')('label.alignProjects.AreYouSureYouWantToRemoveThisGoal'),
        body: goal.formattedGoalName,
        yes: $filter('translate')('label.alignProjects.RemoveGoal')
      }).then(confirmCallback);

    };

    $scope.removeProjectGoal = function(goal) {
      var removeGoal = function () {
        var index = $scope.currentProject.goals.indexOf(goal);

        if(index !== -1) {
          $scope.currentProject.goals.splice(index, 1);

          $scope.processing = true;

          $scope.updateProjectGoals($scope.currentProject);
        }
      };

      $scope.deleteGoalCall(goal, removeGoal);
    };

    $scope.openAddSites = function() {
      ngDialog.open({
        template: 'app/main/align/siteManagement/siteManagementModal.html',
        controller: 'SitemanagementCtrl',
        className: 'modal modal-members member-management-modal',
        scope: $scope
      });
    };

    $scope.openAppManagement = function (app) {
      ngDialog.open({
        template: 'app/main/align/appManagement/manageAppModal.html',
        controller: 'AppManagementCtrl',
        className: 'modal modal-members member-management-modal',
        scope: $scope,
        data: app
      });
    };

    $scope.openAddFbPages = function(account) {
      $scope.selectedSocialAccount = account;
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      if(!account) {
        angular.forEach($scope.teamData.socialAccounts, function(acc) {
          if(acc.social.socialType === 'Facebook') {
            $scope.selectedSocialAccount = acc;

            return false;
          }
        });
      }

      ngDialog.open({
        template: 'app/main/align/addSocialAccount/manageFacebookModal.html',
        controller: 'ManageFacebookCtrl',
        className: 'modal modal-members account-settings-modal modal-large',
        scope: $scope
      });
    };

    $scope.openAddTwPages = function(account) {
      $scope.selectedSocialAccount = account;
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      if(!account) {
        angular.forEach($scope.teamData.socialAccounts, function(acc) {
          if(acc.social.socialType === 'Twitter') {
            $scope.selectedSocialAccount = acc;

            return false;
          }
        });
      }

      ngDialog.open({
        template: 'app/main/align/addSocialAccount/addTwitterSocialAccount.html',
        controller: 'AddsocialaccountCtrl',
        className: 'modal modal-members account-settings-modal modal-large',
        scope: $scope
      });
    };

    $scope.token = $auth.user.access_token; // jshint ignore:line

    $scope.openTagEventsForSite = function(host) {
      if(!host) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.alignProjects.FirstSelectSite')
        });
        return;
      }

      $window.open(angular.element('#selectedSiteTagEvent')[0].href);

    };

    // populate the corresponding dropdowns when user clicks existing goal
    $scope.selectGoalBox = function(goal) {
      $scope.resetGoals();

      $scope.displaySiteEvents = [];
      $scope.displaySitePages = [];

      // Website goal
      if(goal.host) {
        $scope.socialDisable = true;
        $scope.siteDisable = false;
        $scope.appDisable = true;

        $scope.radioOption = 'trackedSites';

        angular.forEach($scope.displayingGoalNames, function(value) {
          if(value.id === goal.goal.id) {
            $scope.selectedSiteGoal.goal = value;
          }
        });
        $scope.selectedSiteGoal.host = goal.host;


        if(goal.tracker) {
          var trackers = [];

          if(goal.event || goal.thirdPartyPixel) {
            trackers = $scope.displayConversionSiteTrackers;
          } else {
            trackers = $scope.displaySiteTrackers;
          }

          angular.forEach(trackers, function(value) {
            if(value.key === goal.tracker) {
              $scope.selectedSiteGoal.tracker = value;
            }
          });
        }

        if(goal.event) {
          pixelService.getEvents($scope.selectedSiteGoal.host, $scope.fractalAccountInfo.currentTeam.id, function(result) {
            for(var i=0;i<result.data.Event.length; i++) {
              if(result.data.Event[i].eventModel.event === 'click') {
                $scope.displaySiteEvents.push({
                  event: result.data.Event[i].eventModel.eventName
                });
              } else if(result.data.Event[i].eventModel.event === 'url') {
                $scope.displaySitePages.push({
                  event: result.data.Event[i].eventModel.eventName,
                  url: result.data.Event[i].eventModel.selector
                });
              }
            }
          }, function(error) {
            fsConfirm('genericAlert', {
              body: error.error.message
            });
          });

          $scope.selectedSiteGoal.eventName = ({event: goal.event});
        }

        if(goal.thirdPartyPixel) {
          $scope.selectedSiteGoal.thirdPartyPixel = goal.thirdPartyPixel;

          $scope.getThirdPartyPixels();
        }

      // Facebook goal
      } else if(goal.pageId) {
        $scope.socialDisable = false;
        $scope.siteDisable = true;
        $scope.appDisable = true;

        $scope.radioOption = 'socialChannelGoals';

        $scope.socialChannelSelect('Facebook');

        angular.forEach($scope.displayingGoalNames, function(value) {
          if(value.id === goal.goal.id) {
            $scope.selectedSocialGoal.goal = value;
          }
        });

        $scope.selectedSocialGoal.pageId = goal.pageId;
        $scope.selectedSocialGoal.pageName = goal.page.pageName;

      // Twitter goal
      } else if (goal.twitterPromotableUserId) {
        $scope.socialDisable = false;
        $scope.siteDisable = true;
        $scope.appDisable = true;

        $scope.radioOption = 'socialChannelGoals';

        // First dropdown ('Social Channel')
        $scope.socialChannelSelect('Twitter');

        // Second dropdown ('Entity + KPI')
        angular.forEach($scope.displayingGoalNames, function(value) {
          if(value.id === goal.goal.id) {
            $scope.selectedSocialGoal.goal = value;
          }
        });

        // Third dropdown ('Choose Property')
        $scope.selectedSocialGoal.accountId = goal.twitterPromotableUser.id;
        $scope.selectedSocialGoal.pageName = goal.twitterPromotableUser.screenName;

      // App goal
      } else if (goal.applicationId) {
        $scope.socialDisable = true;
        $scope.siteDisable = true;
        $scope.appDisable = false;

        $scope.radioOption = 'appIdsGoals';

        angular.forEach($scope.displayingGoalNames, function(value) {
          if(value.id === goal.goal.id) {
            $scope.selectedAppGoal.goal = value;
          }
        });

        $scope.selectedAppGoal.appName = goal.application.name;

        $scope.selectedAppGoal.applicationId = goal.applicationId;

      }
    };
});
