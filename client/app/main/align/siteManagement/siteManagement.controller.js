'use strict';

angular.module('align')
  .controller('SitemanagementCtrl', function ($scope, $rootScope, regex, pixelService, ngDialog, $timeout, $window, $auth, fsConfirm, $filter, $http, microService, urlValidator) {


    // NEW SITE TAB

    $scope.token = $auth.user.access_token;  // jshint ignore:line

    $scope.pasteUrlRegex = regex.SHORTURL;

    $scope.defaultScriptExample = '<script type="text/javascript">(function () { function FSInject() { setTimeout(function () { var element = document.createEle-ment("script"); element.src = "//cdn.fsi11.com/be0097880d4a21cceeea57ef18dc4a5e_track.js" + "?ts=" + Date.now(); document.body.appendChild(element); }, 1); } if (window.addEventListener) window.addEventListener("load", FSInject, false); else if (window.attachEvent) window.attachEvent("onload", FSInject); else if (window.onload) { var oldonload = window.onload; if (typeof window.onload != "function") { window.onload = FSInject; } else { window.onload = function () { if (oldonload) oldonload(); FSInject(); } } } else window["load"] = FSInject; })();</script>';

    $scope.newTrackedSiteData = {
      url: $scope.urlForTrack
    };
    $scope.newConversionPixel = {};

    $scope.eventSaved = false;

    $scope.currentPage = 0;
    $scope.pageSize = 10;

    $scope.paginatedData = [];

    $scope.setPagination = function(data) {
      $scope.paginatedData = data;
      $scope.currentPage = 0;
      $scope.sort = 'eventModel.registeredAt';
      $scope.reverse = false;
    };

    $scope.numberOfPaginationPages = function(data) {
      return Math.ceil(data.length/$scope.pageSize);
    };

    $scope.nextPage = function() {
      $scope.currentPage = $scope.currentPage + 1;
    };

    $scope.prevPage = function() {
      $scope.currentPage = $scope.currentPage - 1;
    };

    $scope.forms = {};

    function addDate () {
      var now = new Date();
      var year = '' + now.getFullYear();
      var month = '' + (now.getMonth() + 1); if (month.length === 1) { month = '0' + month; }
      var day = '' + now.getDate(); if (day.length === 1) { day = '0' + day; }
      var hour = '' + now.getHours(); if (hour.length === 1) { hour = '0' + hour; }
      var minute = '' + now.getMinutes(); if (minute.length === 1) { minute = '0' + minute; }
      var second = '' + now.getSeconds(); if (second.length === 1) { second = '0' + second; }
      return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    }

    $scope.createConversionPixel = function(site){
      $scope.forms.trackingUrl.submitted = true;
      var conversionPixelName;
      if(site.newConversionPixel){
         conversionPixelName = site.newConversionPixel.name;
      }

      if(!conversionPixelName) {
        site.conversionCreationProcessing = false;
        return false;
      }
      //allows the regex error to only show up
      site.conversionCreationProcessing = true;

      function applyConversionPixel(response){
        var conversionPixel = response.data;
        conversionPixel.eventName = conversionPixelName;
        conversionPixel.registeredAt = addDate();
        site.conversionPixels.unshift(conversionPixel);

        if($scope.$parent.$parent.$$childHead.displayConversionEvents) {
          $scope.$parent.$parent.$$childHead.displayConversionEvents.push({
            event: conversionPixel.eventName
          });
        }

        if(!conversionPixel.script){
          $scope.getConversionPixelScript(site, conversionPixel);
        } else {
          conversionPixel.expanded = true;
        }
        site.conversionCreationProcessing = false;
        site.newConversionPixel.name = '';

        $scope.forms.trackingUrl.$setPristine(true);
      }

      $scope.forms.trackingUrl.submitted = false;
      pixelService.createConversionPixel($scope.fractalAccountInfo.currentTeam.id, site.propertyId, conversionPixelName, applyConversionPixel, function(response){
        site.conversionCreationProcessing = false;

        fsConfirm('genericAlert', {
          body: response.error.message
        });
      });
    };

    $scope.getConversionPixelScript = function(site, conversionPixel){
      if(!conversionPixel.expanded && !conversionPixel.script){
        conversionPixel.showScriptLoader = true;
        pixelService.generateConversionScript(site.propertyId, conversionPixel.eventDefinitionId, function(response){
          conversionPixel.script = response;
          conversionPixel.showScriptLoader = false;
          conversionPixel.expanded = true;
        }, function(response){
          console.log(response);
          conversionPixel.showScriptLoader = false;
        });
      }
    };

    $scope.siteProcessing = false;

    $scope.getScriptAction = function() {
      $scope.isUrlInvalid = false;

      //extract domain - http://www.something.com/thing?example=else becomes something.com
      var siteUrl = urlValidator.getDomainWithSubdomain($scope.newTrackedSiteData.url);

      $scope.siteProcessing = true;
      //allows the regex error to only show up
      $scope.forms.newSite.submitted = true;

      if(!siteUrl || siteUrl === 'undefined') {
        $scope.siteProcessing = false;
        $scope.isUrlInvalid = true;
        return false;
      } else {
        $scope.isUrlInvalid = false;
      }

      // invoke the URL validation webservice before further processing of "Add Site" form

      $scope.checkBadURL(
          siteUrl,
          function(){
            // url validation SUCCESS, continue with registration steps.
            // It's possible to get a server error here, too. But we just ignore that and go ahead.
            if (siteUrl.trim().length) {
              if($scope.newTrackedSiteData.scriptForCopy) {
                delete $scope.newTrackedSiteData.scriptForCopy;
              }

              for(var i=0; i<$scope.fractalAccountInfo.currentTeam.pixels.length; i++) {
                if($scope.fractalAccountInfo.currentTeam.pixels[i].URLName === siteUrl) {
                  fsConfirm('genericAlert', {
                    body: $filter('translate')('label.trackedSites.SiteIsAlreadyAssignedToCurrentTeam')
                  });

                  $scope.siteProcessing = false;

                  return; // DO NOT register

                }
              }

              // proceed to register url
              $scope.registerTrackedSite(siteUrl);

            }
          },
          function(){
            // url validation FAIL
            fsConfirm('genericAlert', {
              body: $filter('translate')('label.trackedSites.Wetested') + siteUrl + $filter('translate')('label.trackedSites.butthatdoesnotappeartobe')
            });
            $scope.siteProcessing = false;

            return false;
          }

      );


    };

    $scope.$watch('newTrackedSiteData.url', function (newValue, oldValue) {
      if(!newValue && !oldValue) {
        return false;
      } else if(newValue === oldValue) {
        return false;
      } else {
        var newUrl = $scope.checkURLRegex(newValue);

        //so the regex error doesn't linger forever
        $scope.forms.newSite.submitted = false;

        if(newUrl) {
          if(newUrl.indexOf('#') === -1) {
            $scope.forms.newSite.newTrackedSiteData.$setValidity('valid', true);
          } else {
            $timeout(function(){
              $scope.forms.newSite.newTrackedSiteData.$setValidity('valid', false);
            });
          }
        }

        if(newUrl !== newValue) {

          $timeout(function(){
            $scope.forms.newSite.newTrackedSiteData.$setValidity('valid', false);
          });
        } else {
          $scope.forms.newSite.newTrackedSiteData.$setValidity('valid', true);
        }

      }
    });

    $scope.checkURLRegex = function(url) {
      var urlArray;

      if(url) {
        urlArray = url.match($scope.pasteUrlRegex);

        if(urlArray[0]) {
          return urlArray[0];
        } else {
          return false;
        }
      }
    };

    $scope.registerTrackedSite = function(site) {
      $scope.forms.newSite.submitted = true;

      pixelService.registerHost($scope.fractalAccountInfo.currentTeam.id, site, site, function(response) {
        $scope.forms.newSite.submitted = false;
        $scope.fractalAccountInfo.currentTeam.pixels.push(
          {
            propertyId: response.data.propertyId,
            host: response.data.host,
            URLName: response.data.host,
            eventCount: 0,
            removable: true
          });

        if(response.data.host) {
          $scope.loadScriptForSelected(_.find($scope.fractalAccountInfo.currentTeam.pixels, {'host': response.data.host}), true);
          // Force a full site script validation, because doing so sets up some global vars.
          //    And besides ya never know what to expect.
          $scope.validateURL({
              host: response.data.host,
              testUrl: response.data.host,
              index: 100, // not used here
              nowait: true,
              nocache: false,
              badurlonly: false
            });
        }

        $scope.siteProcessing = false;

        $scope.newTrackedSiteData.url = '';
        if($scope.forms.newSite.$setPristine) {
          $scope.forms.newSite.$setPristine();
        }

      }, function(error) {
        $scope.forms.newSite.submitted = false;
        console.log(error);
      });
    };

    $scope.loadScriptForSelected = function(site, firstLoad) {
      if(firstLoad) {
        angular.forEach($scope.fractalAccountInfo.currentTeam.pixels, function(value) {
          if(value.expanded) {
            value.expanded = false;
          }
        });
      }
      if(!site.scriptForCopy) {
        site.showScriptLoader = true;
        pixelService.generateTrackerScript(site.host, function(result) {
          if(result) {
            site.scriptForCopy = result;
          }
          site.expanded = true;
          delete site.showScriptLoader;
        }, function() {
          delete site.showScriptLoader;
          $scope.pixelScript = 'Error while generating script!';
        });
        if (!site.eventsCount) {
          $scope.getPixelEvents(site);
        }
      }

    };

    // MANAGE SITES TAB

    $scope.getPixelEvents = function(site) {
      site.refreshingEvents = true;
      pixelService.getEvents(site.host, $scope.fractalAccountInfo.currentTeam.id, function (result) {
        site.eventsCount = result.data.Event.length;

        site.clickEvents = [];
        site.pageLandingEvents = [];
        site.conversionPixels = [];

        for (var i = 0; i < result.data.Event.length; i++) {
          if (result.data.Event[i].eventModel.event === 'click') {
            site.clickEvents.push(result.data.Event[i]);
          } else if (result.data.Event[i].eventModel.event === 'url') {
            site.pageLandingEvents.push(result.data.Event[i]);
          } else if (result.data.Event[i].eventModel.event === 'pconv') {
            site.conversionPixels.push(result.data.Event[i].eventModel);
          }
        }

        site.refreshingEvents = false;

      }, function (error) {
        site.refreshingEvents = false;
        console.log(error);
      });

    };

    $scope.deleteTrackedSite = function(site) {
      if(site.removable) {
        pixelService.deactivateHost($scope.fractalAccountInfo.currentTeam.id, site.host, function() {
          var pixels = $scope.fractalAccountInfo.currentTeam.pixels;

          pixels.splice(pixels.indexOf(site), 1);

          $rootScope.somethingIsInvalid = false;

        }, function(error) {
          console.log(error);
        });
      }
    };

    $scope.deactivateTrackedSite = function(site) {
      if(site.removable) {

        fsConfirm('generic', {
          title: $filter('translate')('label.trackedSites.AreYouSureYouWantToRemoveThisSite'),
          body: site.URLName,
          yes: $filter('translate')('label.trackedSites.YesRemoveSite')
        }).then(function() {
          $scope.deleteTrackedSite(site);

          $rootScope.somethingIsInvalid = false;

        });

      } else {

        fsConfirm('genericAlert', {
          body: $filter('translate')('label.trackedSites.SiteCantBeRemoved', {host: site.host})
        });
      }

    };

    $scope.showCopiedToClipboard = function(siteShow) {
      $timeout(function() {
        siteShow.copiedToClipboard = true;
        $timeout(function() {
          siteShow.copiedToClipboard = false;
        }, 2000);
      }, 0);
    };

    $scope.addTracking = function() {
      $window.open('//support.accomplice.io/hc/en-us/articles/205687987');
    };

    $rootScope.urlState = [];               // will be an array of objects describing url state

    if (typeof $rootScope.somethingIsInvalid==='undefined') {
      $rootScope.somethingIsInvalid = false;
    }

    $scope.noUrlValidationService = false;

    // this wrapper is called from buttons so we can also refresh the ui
    $scope.validateURLInit = function(config) {
      $scope.validateURL(config); // this can pull from cache
    };

    $scope.validateURL = function(config) {

      if (typeof config==='object' && config.testUrl) {
        // object type, good to go

      } else if (typeof config==='string') {
        // a raw string is allowed but we need to decorate it
        var myUrl = config;
        config = {
          host: 'na',
          testUrl: myUrl,
          index: 100,         // for tracking state if interesting, probably unused
          nowait: true,       // assume we are in a hurry
          nocache: true,      // assume we don't want anything stale
          badurlonly: false
        };
      } else { return; }

      if( !$rootScope.urlState[ config.testUrl ] ) { $rootScope.urlState[ config.testUrl ] = {}; }

      config.teamId = $scope.fractalAccountInfo.currentTeam.id;
      config.cachekey = config.testUrl;

      var cacheHit = microService.validationApiCache.check(config.cachekey);

      if (config.nocache===true || !cacheHit) {

        var callSpacing = (config.nowait?0:500*config.index); // time-to-wait: this will space things 1 sec apart
        $rootScope.urlState[ config.testUrl ].state = 'checking-url';
        $rootScope.urlState[ config.testUrl ].tooltip = 'URL validation is in process';

        setTimeout( function() {
          microService.urlValidation( config,

            // normal return, means edge service is working at least
            function(response) {

              // We came back with an internal error (probably valerie is DOWN):
              if (response && response.status==='ERROR') {
                $scope.noUrlValidationService = true;
                $rootScope.somethingIsInvalid = true;
                console.log(response.message);
                $rootScope.urlState[ response.api.origUrl ] = {
                    state: 'no-service',
                    returnCode: 'no-service',
                    tooltip: microService.getValidationMsg('no-service')
                  };

                // no additional post-processing required

              } else {

                // We have something to talk about
                var resp = response.api || {};
                if (resp.state==='no-service') {
                  $scope.noUrlValidationService = true;
                  $rootScope.somethingIsInvalid = true;
                }
                microService.validationApiCache.set( resp.origUrl,resp );
                $scope.postProcess(resp);

              }
            },
            function(error) {

              // this probably means the webservice itself is DOWN.
              console.log('Webservice ERROR:',error);
              $scope.noUrlValidationService = true;
              $rootScope.somethingIsInvalid = true;

            }
          );

        }, callSpacing);

      } else {
        var resp = cacheHit || {};
        $scope.postProcess(resp);
      }
    };

    $scope.postProcess = function(resp) {
      $rootScope.urlState[ resp.origUrl ] = {};
      $rootScope.urlState[ resp.origUrl ].state = (resp.found?'valid-url': (resp.error==='no-service'?'no-service':'invalid-url') );
      var returnCode = resp.foundAs || resp.error || 'wut';
      $rootScope.urlState[ resp.origUrl ].returnCode = returnCode;
      $rootScope.urlState[ resp.origUrl ].tooltip = microService.getValidationMsg( returnCode ) || 'undefined-tooltip';
      if (!resp.found || resp.error) { $rootScope.somethingIsInvalid = true; }
    };

    // this is used when trying to register a new site, the 'submit' button fires this fast
    //    sanity check to see if the URL is a real destination before continuing
    $scope.checkBadURL = function(url,successCb,failCb) {
      var config = {
        testUrl: url,
        nowait: true,
        nocache: true,
        index: 100,        // not used in this case
        badurlonly: true   // limit the test to our fast 'good/bad' sanity check
      };

      microService.urlValidation( config,
        function(response) {
          if (response && response.status==='ERROR') {
            // webservice is down, ignore it completely
            successCb('no-service');

          } else {
            var resp = response.api || {};
            if (resp && resp.found) { successCb(resp); }
            else { failCb(resp); }
          }

        },
        function(error) {
          console.log('validateURL server ERROR:',error);

          failCb(url);
        }
      );
    };

    $scope.validateAllUrls = function() {
      $rootScope.somethingIsInvalid = false;
      var pixels = $scope.fractalAccountInfo.currentTeam.pixels;
      if (pixels && pixels.length>0) {
        for (var i=0; i<pixels.length; i++) {
          var site = pixels[i];
          $scope.validateURL({
              host: site.host,
              testUrl: site.URLName,
              index: i,
              nowait: true,
              nocache: false,
              badurlonly: false
            });
        }
      }
    };

    // we need this so we can get a global idea of anything that might be wrong
    $scope.validateAllUrls();

    $scope.manageSitesTab = false;

    $scope.newSitesTab = true;

    if($scope.getScriptForDefinedSite) {

      $scope.manageSitesTab = true;
      $scope.newSitesTab = false;

      angular.forEach($scope.fractalAccountInfo.currentTeam.pixels, function(site) {
        if(site.host === $scope.getScriptForDefinedSite) {
          site.expanded = true;
          $scope.loadScriptForSelected(site);
        } else {
          site.expanded = false;
        }
      });

    }

    $scope.deactivateEvent = function(event, site, type) {
      var eventName,removable;

      if(type === 'pconv') {
        eventName = event.eventName;
        removable = event.goal;
      } else {
        eventName = event.eventModel.eventName;
        removable = event.eventModel.goal;
      }

      if(removable) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.trackedSites.EventCantBeRemoved', {event: eventName})
        });
        return;
      }

      fsConfirm('generic', {
        title: $filter('translate')('label.trackedSites.AreYouSureYouWantToRemoveThisEvent'),
        body: eventName,
        yes: $filter('translate')('label.trackedSites.YesRemoveEvent')
      }).then(function() {

        pixelService.deactivateEvent(eventName, site.host, $scope.fractalAccountInfo.currentTeam.id, function() {
          if(type === 'clicks') {
            var currentClickIndex = site.clickEvents.indexOf(event);

            if(currentClickIndex !== -1) {
              site.clickEvents.splice(currentClickIndex, 1);
            }

            if($scope.currentPage !== 0 && site.clickEvents.length < 11) {
              $scope.currentPage = 0;
            }
          } else if(type === 'url') {
            var currentUrlIndex = site.pageLandingEvents.indexOf(event);

            if(currentUrlIndex !== -1) {
              site.pageLandingEvents.splice(currentUrlIndex, 1);
            }

            if($scope.$parent.$parent.$$childHead.displayUrlEvents) {
              $scope.$parent.$parent.$$childHead.displayUrlEvents.splice(currentUrlIndex, 1);
            }

            if($scope.currentPage !== 0 && site.pageLandingEvents.length < 11) {
              $scope.currentPage = 0;
            }
          } else if(type === 'pconv') {
            var currentConversionIndex = site.conversionPixels.indexOf(event);

            if(currentConversionIndex !== -1) {
              site.conversionPixels.splice(currentConversionIndex, 1);
            }

            if($scope.$parent.$parent.$$childHead.displayConversionEvents) {
              $scope.$parent.$parent.$$childHead.displayConversionEvents.splice(currentConversionIndex, 1);
            }

            if($scope.currentPage !== 0 && site.conversionPixels.length < 11) {
              $scope.currentPage = 0;
            }
          }

        }, function(error) {
          console.log(error);
        });

      });
    };

    $scope.saveEvent = function(site, newpage, newevent) {

      if(newpage && newevent) {

        if(urlValidator.checkUrlDomainMatchesGoalDomain(newpage, site.host )){
          fsConfirm('genericAlert', {
            body: 'Page URL must contain domain URL (' + site.host + '/example.html)'
          });
          return;
        }

        pixelService.saveEvent(newpage, site.host, newevent, $scope.fractalAccountInfo.currentTeam.id, function() {

          $timeout(function() {
            $scope.eventSaved = true;
            $timeout(function() {
              $scope.eventSaved = false;
            }, 2000);
          }, 0);

          site.pageLandingEvents.unshift({
            eventModel: {
              eventName: newevent,
              selector: newpage,
              pageUrl: '',
              event: 'url'
            }
          });

          if($scope.$parent.$parent.$$childHead.displayUrlEvents) {
            $scope.$parent.$parent.$$childHead.displayUrlEvents.push({
              event: newevent,
              eventUrl: newpage
            });
          }

          delete site.newEventName;
          delete site.newPageName;

        }, function(error) {
          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
      }
    };

    $scope.sort = 'eventModel.registeredAt';
    $scope.reverse = false;

    $scope.sortByEventName = function(value) {
      if ($scope.sort === value){
        $scope.reverse = !$scope.reverse;
        return;
      }

      $scope.sort = value;
      $scope.reverse = false;

    };

  })

.filter('startFrom', function() {
  return function(input, start) {
    if (input) {
      start = +start;
      return input.slice(start);
    }
    return [];
  };
});

