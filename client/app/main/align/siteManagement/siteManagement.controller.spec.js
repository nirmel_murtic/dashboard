'use strict';

describe('Controller: SitemanagementCtrl', function () {

  // load the controller's module
  beforeEach(module('align'));

  var scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
  }));
});
