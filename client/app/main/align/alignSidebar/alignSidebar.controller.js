'use strict';

angular.module('align')
  .controller('AlignsidebarCtrl', function ($scope, library, $timeout, teamService, $state, ngDialog, $rootScope, apiUrl, pixelService, $auth, billingService, $window, $http, microService) {
    $scope.token = $auth.user.access_token;  // jshint ignore:line

    $scope.apiUrl = apiUrl;

    $scope.initMembersValue = 3;

    $scope.initSitesValue = 3;

    $scope.initAppsValue = 3;

    $scope.initAdWordsValue = 3;

    $scope.initFbAdValue = 3;

    $scope.initFbPagesValue = 3;

    $scope.initTwitterValue = 3;

    $scope.showAllMembers = function() {
      if($scope.initMembersValue === 3) {
        $scope.initMembersValue = $scope.fractalAccountInfo.currentTeam.activeMembers.length;
      } else if($scope.initMembersValue !== 3) {
        $scope.initMembersValue = 3;
      }
    };

    $scope.showAllSites = function() {
      if($scope.initSitesValue === 3) {
        $scope.initSitesValue = $scope.fractalAccountInfo.currentTeam.pixels.length;
      } else if($scope.initSitesValue !== 3) {
        $scope.initSitesValue = 3;
      }
    };

    $scope.showAllApps = function() {
      if($scope.initAppsValue === 3) {
        $scope.initAppsValue = $scope.fractalAccountInfo.currentTeam.pixels.length;
      } else if($scope.initAppsValue !== 3) {
        $scope.initAppsValue = 3;
      }
    };

    $scope.showAllAdwords = function() {
      if($scope.initAdWordsValue === 3) {
        $scope.initAdWordsValue = $scope.adWordsCounter;
      } else if($scope.initAdWordsValue !== 3) {
        $scope.initAdWordsValue = 3;
      }
    };

    $scope.showAllFbAdAccounts = function() {
      if($scope.initFbAdValue === 3) {
        $scope.initFbAdValue = $scope.fbAdsCounter;
      } else if($scope.initFbAdValue !== 3) {
        $scope.initFbAdValue = 3;
      }
    };

    $scope.showAllFbPages = function() {
      if($scope.initFbPagesValue === 3) {
        $scope.initFbPagesValue = $scope.fbPagesCounter;
      } else if($scope.initFbPagesValue !== 3) {
        $scope.initFbPagesValue = 3;
      }
    };

    $scope.showAllTwitter = function() {
      if($scope.initTwitterValue === 3) {
        $scope.initTwitterValue = $scope.twitterCounter;
      } else if($scope.initTwitterValue !== 3) {
        $scope.initTwitterValue = 3;
      }
    };

    $scope.addFacebook = function(account) {
      $scope.selectedSocialAccount = account;
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      if(!account) {
        angular.forEach($scope.teamData.socialAccounts, function(acc) {
          if(acc.social.socialType === 'Facebook') {
            $scope.selectedSocialAccount = acc;
            return false;
          }
        });
      }

      ngDialog.open({
        template: 'app/main/align/addSocialAccount/manageFacebookModal.html',
        controller: 'ManageFacebookCtrl',
        className: 'modal modal-members account-settings-modal modal-large',
        scope: $scope
      });
    };

    $scope.addTwitter = function(account) {
      $scope.selectedSocialAccount = account;
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      if(!account) {
        angular.forEach($scope.teamData.socialAccounts, function(acc) {
          if(acc.social.socialType === 'Twitter') {
            $scope.selectedSocialAccount = acc;

            return false;
          }
        });
      }

      ngDialog.open({
        template: 'app/main/align/addSocialAccount/addTwitterSocialAccount.html',
        controller: 'AddsocialaccountCtrl',
        className: 'modal modal-members account-settings-modal',
        scope: $scope
      });
    };

    $scope.openEventManagement = function(site) {
      $scope.currentSite = site;
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      $scope.getEvents(site, function() {
        ngDialog.open({
          template: 'app/main/align/sitesTracking/eventManagement/eventManagement.html',
          controller: 'EventmanagementCtrl',
          className: 'modal modal-members event-management-modal',
          scope: $scope
        });
      });
    };

    $scope.getEvents = function(pixel, callback) {
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      pixelService.getEvents(pixel.host, $scope.teamData.id, function(result) {
        pixel.events = result.data.Event;

        callback(pixel);
      }, function(error) {
        console.log(error);
      });
    };

    $scope.$on('ngDialog.closed', function (e, $dialog) {
      if($scope.subscribeToPlan && $scope.accountSettingsDialog.id === $dialog.attr('id')) {
        delete $scope.subscribeToPlan;

        ngDialog.open({
          template: 'app/main/align/accountSettings/subscribeToPlan/subscribeToPlan.html',
          controller: 'SubscribetoplanCtrl',
          className: 'modal modal-members subscribe-to-plan-modal',
          scope: $scope
        });
      }
    });

    $scope.accountSettings = function() {
      billingService.getAccount(function (result) {
        $scope.accountSettingsAccount = result.data;

        var dialog = ngDialog.open({
          template: 'app/main/align/accountSettings/accountSettings.html',
          controller: 'AccountSettingsCtrl',
          className: 'modal modal-members account-settings-modal',
          id: 'account-settings-dialog',
          scope: $scope
        });

        $scope.accountSettingsDialog = dialog;
      }, function (error) {
        console.log(error.error);
      });
    };

    $scope.openTeamManagement = function(member) {

      if(member) {
        $scope.openDefinedMember = member;
      } else {
        delete $scope.openDefinedMember;
      }

      ngDialog.open({
        template: 'app/main/align/memberManagement/memberManagement.html',
        controller: 'MemberManagementCtrl',
        className: 'modal modal-members member-management-modal',
        scope: $scope
      });
    };

    $scope.openTrackedSitesManagement = function(site) {

      if(site) {
        $scope.getScriptForDefinedSite = site;
      } else {
        delete $scope.getScriptForDefinedSite;
      }

      ngDialog.open({
        template: 'app/main/align/siteManagement/siteManagementModal.html',
        controller: 'SitemanagementCtrl',
        className: 'modal modal-members member-management-modal',
        scope: $scope
      });
    };

    $scope.openAppManagement = function (app) {
      ngDialog.open({
        template: 'app/main/align/appManagement/manageAppModal.html',
        controller: 'AppManagementCtrl',
        className: 'modal modal-members member-management-modal',
        scope: $scope,
        data: app
      });
    };

    $scope.openLibrary = function(section) {
      library.openLibrary(section);
    };

    $scope.getNumTwitterPages = function(twitterAccounts){
      var count = 0;
      for(var i = 0; i < twitterAccounts.length; i++){
        for(var j = 0; j < twitterAccounts[i].promotableUsers.length; j++){
          count++;
        }
      }
      return count;
    };

    $scope.addGoogle = function(account) {
      $scope.selectedSocialAccount = account;
      $scope.teamData = $scope.fractalAccountInfo.currentTeam;

      if(!account) {
        angular.forEach($scope.teamData.socialAccounts, function(acc) {
          if(acc.social.socialType === 'Google') {
            $scope.selectedSocialAccount = acc;

            return false;
          }
        });
      }

      ngDialog.open({
        template: 'app/main/align/addSocialAccount/addGoogleSocialAccount.html',
        controller: 'AddsocialaccountCtrl',
        className: 'modal modal-members member-management-modal',
        scope: $scope
      });
    };

    $scope.facebookConnectOption = false;

    $scope.googleConnectOption = false;

    $scope.watchOnFacebook = function(entity) {

      $scope.adWordsCounter = 0;

      $scope.fbAdsCounter = 0;

      $scope.fbPagesCounter = 0;

      $scope.twitterCounter = 0;

      angular.forEach(entity, function(socialAcc) {
        if(socialAcc.social.socialType === 'Facebook') {
          $scope.facebookConnectOption = true;
          if(socialAcc.fbAccounts) {
            $scope.fbAdsCounter = socialAcc.fbAccounts.length;
          }
          if(socialAcc.pages) {
            $scope.fbPagesCounter = socialAcc.pages.length;
          }
        }
        if (socialAcc.social.socialType === 'Google') {
          $scope.googleConnectOption = true;
          if(socialAcc.adwordsAccounts) {
            $scope.adWordsCounter = socialAcc.adwordsAccounts.length;
          }
        }
        if (socialAcc.social.socialType === 'Twitter') {
          $scope.twitterConnectOption = true;
          if(socialAcc.twAccounts) {
            $scope.twitterCounter = socialAcc.twAccounts.length;
          }
        }
      });
    };

    $scope.$watchCollection('fractalAccountInfo.currentTeam.socialAccounts', function(newValue, oldValue) {
      if (!newValue && !oldValue) {
        return false;
      } else {
        $scope.facebookConnectOption = false;
        $scope.googleConnectOption = false;
        $scope.twitterConnectOption = false;
        $scope.watchOnFacebook(newValue);
      }
    }, true);

    $scope.trimGoogle = true;

    $scope.trimFacebook = true;

    $scope.trimSites = true;

    $scope.trimMembers = true;

    $scope.trimApps = true;

    $scope.trimTwitter = true;

    $scope.sidebarBlockCollapse = function(block) {
      if (block === 'google') {
        if ($scope.trimGoogle && !$scope.googleToggler) {
          $scope.trimGoogle = false;
        } else if (!$scope.trimGoogle && $scope.googleToggler){
          $timeout(function () {
            $scope.trimGoogle = true;
          }, 300);
        }
      } else if (block === 'facebook') {
        if ($scope.trimFacebook && !$scope.facebookToggler) {
          $scope.trimFacebook = false;
        } else if(!$scope.trimFacebook && $scope.facebookToggler){
          $timeout(function () {
            $scope.trimFacebook = true;
          }, 300);
        }
      } else if (block === 'sites') {
        if ($scope.trimSites && !$scope.sitesToggler) {
          $scope.trimSites = false;
        } else if(!$scope.trimSites && $scope.sitesToggler){
          $timeout(function () {
            $scope.trimSites = true;
          }, 300);
        }
      } else if (block === 'members') {
        if ($scope.trimMembers && !$scope.membersToggler) {
          $scope.trimMembers = false;
        } else if(!$scope.trimMembers && $scope.membersToggler){
          $timeout(function () {
            $scope.trimMembers = true;
          }, 300);
        }
      } else if (block === 'apps') {
        if ($scope.trimApps && !$scope.appsToggler) {
          $scope.trimApps = false;
        } else if(!$scope.trimApps && $scope.appsToggler){
          $timeout(function () {
            $scope.trimApps = true;
          }, 300);
        }
      } else if (block === 'twitter') {
        if ($scope.trimTwitter && !$scope.twitterToggler) {
          $scope.trimTwitter = false;
        } else if(!$scope.trimTwitter && $scope.twitterToggler){
          $timeout(function () {
            $scope.trimTwitter = true;
          }, 300);
        }
      }
    };

    if($window.sessionStorage.switchFacebookAccount) {
      delete $window.sessionStorage.switchFacebookAccount;
      $scope.addFacebook();
    }

    if($window.sessionStorage.switchTwitterAccount) {
      delete $window.sessionStorage.switchTwitterAccount;
      $scope.addTwitter();
    }


    $rootScope.urlState = [];
    if (typeof $rootScope.somethingIsInvalid==='undefined') {
      $rootScope.somethingIsInvalid = false;
    }
    $scope.noUrlValidationService = false;

    // this wrapper is called from buttons so we can also refresh the ui
    $scope.validateURLInit = function(config) {
      $scope.validateURL(config); // this can pull from cache
    };

    $scope.validateURL = function(config) {

      if (typeof config==='object' && config.testUrl) {
        // object type, good to go

      } else if (typeof config==='string') {
        // a raw string is allowed but we need to decorate it
        var myUrl = config;
        config = {
          host: 'na',
          testUrl: myUrl,
          index: 100,         // for tracking state if interesting, probably unused
          nowait: true,       // assume we are in a hurry
          nocache: true,      // assume we don't want anything stale
          badurlonly: false
        };
      } else { return; }

      if( !$rootScope.urlState[ config.testUrl ] ) { $rootScope.urlState[ config.testUrl ] = {}; }

      config.teamId = $scope.fractalAccountInfo.currentTeam.id;
      config.cachekey = config.testUrl;

      var cacheHit = microService.validationApiCache.check(config.cachekey);

      if (config.nocache===true || !cacheHit) {

        var callSpacing = (config.nowait?0:500*config.index); // time-to-wait: this will space things 1 sec apart
        $rootScope.urlState[ config.testUrl ].state = 'checking-url';
        $rootScope.urlState[ config.testUrl ].tooltip = 'URL validation is in process';

        setTimeout( function() {
          microService.urlValidation( config,

            // normal return, means edge service is working at least
            function(response) {

              // We came back with an internal error (probably valerie is DOWN):
              if (response && response.status==='ERROR') {
                $scope.noUrlValidationService = true;
                $rootScope.somethingIsInvalid = true;
                console.log(response.message);
                $rootScope.urlState[ response.api.origUrl ] = {
                    state: 'no-service',
                    returnCode: 'no-service',
                    tooltip: microService.getValidationMsg('no-service')
                  };

                // no additional post-processing required

              } else {

                // We have something to talk about
                var resp = response.api || {};
                if (resp.error==='no-service') {
                  $scope.noUrlValidationService = true;
                  $rootScope.somethingIsInvalid = true;
                }
                microService.validationApiCache.set( resp.origUrl,resp );
                $scope.postProcess(resp);

              }
            },
            function(error) {

              // this probably means the webservice itself is DOWN.
              console.log('Webservice ERROR:',error);
              $scope.noUrlValidationService = true;
              $rootScope.somethingIsInvalid = true;

            }
          );

        }, callSpacing);

      } else {
        var resp = cacheHit || {};
        $scope.postProcess(resp);
      }
    };

    $scope.postProcess = function(resp) {
      $rootScope.urlState[ resp.origUrl ] = {};
      $rootScope.urlState[ resp.origUrl ].state = (resp.found?'valid-url': (resp.error==='no-service'?'no-service':'invalid-url') );
      var returnCode = resp.foundAs || resp.error || 'wut';
      $rootScope.urlState[ resp.origUrl ].returnCode = returnCode;
      $rootScope.urlState[ resp.origUrl ].tooltip = microService.getValidationMsg( returnCode ) || 'undefined-tooltip';
      if (!resp.found || resp.error) { $rootScope.somethingIsInvalid = true; }
    };

    $scope.validateAllUrls = function() {
      $rootScope.somethingIsInvalid = false;
      var pixels = $scope.fractalAccountInfo.currentTeam.pixels;
      if (pixels && pixels.length>0) {
        for (var i=0; i<pixels.length; i++) {
          var site = pixels[i];
          $scope.validateURL({
              host: site.host,
              testUrl: site.URLName,
              index: i,
              nowait: false,
              nocache: false,
              badurlonly: false
            });
        }
      }
    };

    // we need this so we can get a global idea of anything that might be wrong
    $scope.validateAllUrls();

  });
