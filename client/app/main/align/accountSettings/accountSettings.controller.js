'use strict';

angular.module('align')
  .controller('AccountSettingsCtrl', function ($scope, billingService, ngDialog, $rootScope, $timeout) {
    $scope.tabs = {
      paymentTab: false,
      contactTab: true
    };

    $scope.setAccount = function(account) {
      var existingActiveSubscription = $scope.account ? $scope.account.activeSubscription : null;
      var existingPendingSubscription = $scope.account ? $scope.account.pendingSubscription : null;

      $scope.account = account.accountInfo;
      $scope.account.codeStates = account.codeStates;

      $scope.statesView = _(account.codeStates)
        .pairs()
        .map(function(pair) { return { key: pair[0], label: pair[1] }; })
        .value();

      $scope.statesView = _.sortBy($scope.statesView, 'label');

      if(account.activeSubscription) {
        $scope.account.activeSubscription = account.activeSubscription;
      } else {
        $scope.account.activeSubscription = existingActiveSubscription;
      }

      if(account.pendingSubscription) {
        $scope.account.pendingSubscription = account.pendingSubscription;
      } else {
        $scope.account.pendingSubscription = existingPendingSubscription;
      }
      $scope.account.billing = {
        number: $scope.account.cardMaskedNumber,
        month: $scope.account.cardExpirationMonth ? $scope.account.cardExpirationMonth : null,
        year: $scope.account.cardExpirationYear ? $scope.account.cardExpirationYear : null,
        firstName: $scope.account.cardFirstName,
        lastName: $scope.account.cardLastName,
        cvv: null
      };

      $scope.cardType = $scope.account.cardType;
      $scope.isFreePlan = false; // $scope.account.activeSubscription.subscriptionPlan.amount === 0;

      if($scope.futurePlan) {
        if($scope.account.companyName && $scope.account.firstName && $scope.account.lastName && $scope.account.emailAddress && $scope.account.phone &&
          $scope.account.companyName.length && $scope.account.firstName.length && $scope.account.lastName.length &&
          $scope.account.emailAddress.length && $scope.account.phone.length) {
            $scope.tabs.paymentTab = true;
            $scope.tabs.contactTab = false;
        }

        $scope.isFreePlan = $scope.futurePlan.amount === 0;

        delete $scope.futurePlan;
      }
    };

    $scope.setAccount($scope.accountSettingsAccount);

    $scope.update = function() {
      var billing = $scope.account.billing;

      if(billing.number && billing.cvv && billing.month && billing.year && billing.firstName && billing.lastName && $scope.account.address1) {
        recurly.token({
          number: billing.number,
          cvv: billing.cvv,
          month: billing.month,
          year: billing.year,
          first_name: billing.firstName, // jshint ignore:line
          last_name: billing.lastName, // jshint ignore:line
          address1: $scope.account.address1,
          city: $scope.account.city,
          state: $scope.account.state,
          country: 'US',
          postal_code: $scope.account.zip // jshint ignore:line
        }, function (err, token) {
          if (token) {
            $scope.$apply(function () {
              $scope.account.billing.token = token.id;
            });
          } else {
            delete $scope.account.billing.token;
          }

          $scope.updateAccount();
        });
      } else {
        delete $scope.account.billing.token;

        $scope.updateAccount();
      }
    };

    $scope.updateAccount = function() {
      $scope.clearErrorMessage();

      ngDialog.open({
        template: 'app/main/align/accountSettings/checkPassword/checkPassword.html',
        controller: 'CheckPasswordCtrl',
        className: 'modal modal-members check-password-modal',
        id: 'check-password-dialog',
        scope: $scope
      });
    };

    $scope.continueUpdateAccount = function(password) {
      $scope.processing = true;

      billingService.updateAccount($scope.account, password, function(result) {

        $timeout(function() {
          $scope.contactNotification = {
            message: 'Your account settings have been saved.',
            status: 'show'
          };

          $timeout(function() {
            $scope.setAccount(result.data);
            $scope.processing = false;

            $rootScope.fractalAccountInfo.fractalAccount.name =  $scope.account.companyName;
            $scope.contactNotification.status = 'hide';
            delete $scope.contactNotification.message;
          }, 2000);
        }, 0);
      }, function(error) {
        $scope.errorMessage = error.error.message;

        console.log(error.error);
        $scope.processing = false;
      });
    };

    $scope.removeBillingInfo = function(event) {
      event.preventDefault();

      $scope.processing = true;
      billingService.removeBillingInfo(function(result) {
        if(result.data.success) {
          var billing = $scope.account.billing;

          billing.firstName = billing.lastName = billing.number = billing.cvv = billing.month = billing.year= null;
          delete $scope.account.cardMaskedNumber;
          delete $scope.account.cardExpirationMonth;
          delete $scope.account.cardExpirationYear;
          delete $scope.account.cardFirstName;
          delete $scope.account.cardLastName;
        }

        $scope.processing = false;
      }, function(error) {
        console.log(error.error);

        $scope.processing = false;
      });
    };

    $scope.subscribeToPlan = function() {

      $scope.$parent.subscribeToPlan = true;

      $scope.accountSettingsDialog.close();
    };

    if($scope.openPlansFirst) {
      $timeout(function() {
        $scope.subscribeToPlan();
      }, 1000);
    }

    $scope.clearErrorMessage = function() {
      delete $scope.errorMessage;
    };

    $scope.onCvvChange = function() {
      if($scope.account.billing.cvv) {
        $scope.isFreePlan = false;
      }
    };
  });
