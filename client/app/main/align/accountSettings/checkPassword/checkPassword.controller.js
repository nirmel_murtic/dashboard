'use strict';

angular.module('align')
  .controller('CheckPasswordCtrl', function ($scope, $timeout) {
    $scope.passwordCheck = function() {
      $scope.closeThisDialog();

      $scope.continueUpdateAccount($scope.password);
    };

    $scope.focusPasswordInput = false;

    $timeout(function() {
      $scope.focusPasswordInput = true;
    }, 100);
  });
