'use strict';

angular.module('align')
  .controller('SubscribetoplanCtrl', function ($scope, billingService, $sce, ngDialog, regex, fsConfirm) {

    $scope.account = $scope.accountSettingsAccount;

    $scope.isFreePlan = $scope.account.activeSubscription.subscriptionPlan.amount === 0;

    $scope.subscribeOnRenewal = false;

    $scope.lineBreakRegex = regex.LINEBREAK;

    billingService.getPlans(function(result) {
      $scope.availablePlans = result.data.plans;
      $scope.availablePlans.sort(function(a,b) {

        // To move the enterprise plan to the right
        if (b.planCode.substring(0,1) === 'e') {
          return -1;
        }

        else if(a.amount < b.amount) {
          return -1;
        } else if(a.amount > b.amount) {
          return 1;
        }
        // To move the beta plan plan to the left
        if(b.planCode === 'b101') {
          return 1;
        } else {
            return 0;
        }

      });

      if($scope.account.activeSubscription.subscriptionPlan) {
        $scope.selectedPlan = $scope.getPlanByCode($scope.account.activeSubscription.subscriptionPlan.planCode);

        if($scope.selectedPlan) {
          $scope.selectedPlan.selected = true;
        } else {
          // TODO: Handle case when user use outdated (disabled) package
        }
      }
    }, function(error) {
      console.log(error.error.message);
    });

    $scope.trustAsHtml = function(text) {
      // Replace the line breaks by <br> so it breaks nicely in the UI
      text = text ? text.replace($scope.lineBreakRegex, '<br />') : text;
      return $sce.trustAsHtml(text);
    };

    $scope.selectPlan = function(plan) {
      angular.forEach($scope.availablePlans, function(plan) {
        plan.selected = false;
      });

      plan.selected = true;

      $scope.selectedPlan = plan;

      $scope.validateRenewalRadio();
    };

    $scope.validateRenewalRadio = function() {
      if(!($scope.account.activeSubscription && !$scope.isFreePlan &&
        $scope.selectedPlan.planCode !== $scope.account.activeSubscription.subscriptionPlan.planCode &&
        (!$scope.account.pendingSubscription || $scope.selectedPlan.planCode !== $scope.account.pendingSubscription.subscriptionPlan.planCode))) {
        $scope.subscribeOnRenewal = false;
      }
    };

    $scope.getPlanByCode = function(code) {
      var result = null;

      angular.forEach($scope.availablePlans, function(plan) {
        if(code === plan.planCode) {
          result = plan;
          return false;
        }
      });

      return result;
    };

    $scope.clearErrorMessage = function() {
      delete $scope.errorMessage;
    };

    $scope.subscribeToPlan = function() {

      // Enterprise plan, show the modal popup that asks the user to send an email
      if ($scope.selectedPlan.planCode.substring(0,1) === 'e') {
          fsConfirm('enterprisePlanAlert');
      } else { // For all other plans, regular processing
          $scope.processing = true;

          $scope.clearErrorMessage();

          billingService.subscribeToPlan( $scope.selectedPlan.planCode, $scope.subscribeOnRenewal, function(result) {
              $scope.processing = false;

              $scope.account.activeSubscription = result.data.activeSubscription;
              $scope.account.pendingSubscription = result.data.pendingSubscription;

              $scope.closeThisDialog();
          }, function(error) {
              $scope.errorMessage = error.error.message;
              $scope.processing = false;

              console.log(error.error.message);
          });
      }
    };

    $scope.updateInfo = function() {
      $scope.futurePlan = $scope.selectedPlan;

      $scope.clearErrorMessage();

      ngDialog.open({
        template: 'app/main/align/accountSettings/accountSettings.html',
        controller: 'AccountSettingsCtrl',
        className: 'modal modal-members account-settings-modal',
        id: 'account-settings-dialog',
        scope: $scope
      });
    };
  });
