'use strict';

angular.module('align')
  .controller('SubscriptionPlansCtrl', function ($scope, billingService) {
    billingService.getAllPlans(function(result) {
      $scope.plans = result.data.plans;

      angular.forEach($scope.plans, function(plan) {
        plan.adSpentLimitTemp = plan.adSpentLimit / 100;
      });
    }, function(error) {
      console.log(error.error.message);
    });

    $scope.syncPlans = function() {
      billingService.syncPlans(function(result) {
        $scope.plans = result.data.plans;
      }, function(error) {
        console.log(error.error.message);
      });
    };

    $scope.saveSubscriptionPlans = function() {
      $scope.processing = true;

      var data = [];

      angular.forEach($scope.plans, function(plan) {
        data.push({
          id: plan.id,
          planCode: plan.planCode,
          adSpentLimit: plan.adSpentLimit >= 0 ? plan.adSpentLimit : -1,
          trackedEventsLimit: plan.trackedEventsLimit,
          enabled: plan.enabled
        });
      });

      billingService.updatePlans(data, function() {
        $scope.processing = false;

        $scope.closeThisDialog();
      }, function(error) {
        $scope.processing = false;

        console.log(error.error.message);
      });
    };
  });
