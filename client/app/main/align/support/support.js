'use strict';

angular.module('align')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.align.support', {
        permissions: '<SUPPORT>',
        url: '/support',
        views: {
          '@main': {
            templateUrl: 'app/main/align/support/support.html',
            controller: 'SupportCtrl'
          }
        },
        rightColumn : {
          present : true,
          fixed : true,
          columns : 5
        },
        data: {
          displayName: 'Support',
          active: 'setup'
        },
        resolve: {
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/align/support/support');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
