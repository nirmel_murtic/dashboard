'use strict';

angular.module('align')
  .controller('EmailTemplatesCtrl', function ($scope, emailTemplatesService) {
    $scope.languages = [{
      code: 'en',
      name: 'English'
    }];

    $scope.selectedLanguage = $scope.languages[0];

    $scope.templates = {};

    emailTemplatesService.getTemplates($scope.selectedLanguage.code, function(result) {
      $scope.templates[$scope.selectedLanguage.code] = result.data.templates;
    }, function(error) {
      console.log(error.error.message);
    });

    $scope.onTemplateChange = function() {
      $scope.convertToParamNames($scope.selectedTemplate);
    };

    $scope.updateTemplate = function() {
      $scope.processing = true;

      var data = angular.copy($scope.selectedTemplate);

      $scope.convertToParamNumbers(data);

      emailTemplatesService.updateTemplate(data, function(result) {
        $scope.selectedTemplate.text = result.data.template.text;
        $scope.selectedTemplate.html = result.data.template.html;
        $scope.selectedTemplate.subject = result.data.template.subject;

        $scope.convertToParamNames($scope.selectedTemplate);

        delete $scope.processing;
      }, function(error) {
        console.log(error.error.message);

        delete $scope.processing;
      });
    };

    $scope.convertToParamNames = function(template) {
      var params = template.params.split(',');

      for(var i = 0; i < params.length; i++) {
        var re = new RegExp('\\{' + i +  '\\}', 'g');

        template.text = template.text.replace(re, '{' + params[i] + '}');
        template.html = template.html.replace(re, '{' + params[i] + '}');
      }

      template.text = template.text.replace(new RegExp('\'\'', 'g'), '\'');
      template.html = template.html.replace(new RegExp('\'\'', 'g'), '\'');
    };

    $scope.convertToParamNumbers = function(template) {
      var params = template.params.split(',');

      for(var i = 0; i < params.length; i++) {
        var re = new RegExp('{' + params[i] +  '}', 'g');

        template.text = template.text.replace(re, '{' + i + '}');
        template.html = template.html.replace(re, '{' + i + '}');
      }

      template.text = template.text.replace(new RegExp('\'', 'g'), '\'\'');
      template.html = template.html.replace(new RegExp('\'', 'g'), '\'\'');

      template.text = template.text.replace(new RegExp('\'\'{\'\'', 'g'), '\'{\'');
      template.text = template.text.replace(new RegExp('\'\'}\'\'', 'g'), '\'}\'');
      template.html = template.html.replace(new RegExp('\'\'{\'\'', 'g'), '\'{\'');
      template.html = template.html.replace(new RegExp('\'\'}\'\'', 'g'), '\'}\'');
    };
  });
