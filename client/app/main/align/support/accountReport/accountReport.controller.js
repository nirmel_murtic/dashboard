'use strict';

angular.module('align')
  .controller('AccountReportCtrl', function ($scope, billingService, $filter, apiUrl, $auth, $window) {
    $scope.initStartDate = new Date(new Date().getTime() - 86400000 * 30);
    $scope.initEndDate = new Date();

    $scope.endDate = $filter('date')($scope.initEndDate, 'MM/dd/yyyy');
    $scope.startDate = $filter('date')($scope.initStartDate, 'MM/dd/yyyy');

    $scope.reloadReport = function() {
      var startDate = $scope.startDate.getTime ? $scope.startDate : $scope.initStartDate;
      var endDate = $scope.endDate.getTime ? $scope.endDate : $scope.initEndDate;

      if($scope.allAccounts) {
        billingService.getReport(startDate, endDate, function (result) {
          $scope.reportData = result.data;
        }, function (error) {
          console.log(error.error.message);
        });
      } else {
        billingService.getReportForAccount(startDate, endDate, $scope.selectedAccount.id, function (result) {
          $scope.reportData = result.data;
        }, function (error) {
          console.log(error.error.message);
        });
      }
    };

    $scope.downloadCSV = function() {
      var startDate = $scope.startDate.getTime ? $scope.startDate : $scope.initStartDate;
      var endDate = $scope.endDate.getTime ? $scope.endDate : $scope.initEndDate;

      startDate = $filter('date')(startDate, 'yyyy-MM-dd');
      endDate = $filter('date')(endDate, 'yyyy-MM-dd');

      /*jshint camelcase: false */
      if($scope.allAccounts) {
        $window.open(apiUrl + '/api/v2/campaign/stats/get/amountspent/csv?startTime=' + startDate + '&endTime=' + endDate + '&access_token=' + $auth.user.access_token);
      } else {
        $window.open(apiUrl + '/api/v2/campaign/stats/get/account/' + $scope.selectedAccount.id + '/amountspent/csv?startTime=' + startDate + '&endTime=' + endDate + '&access_token=' + $auth.user.access_token);
      }
      /*jshint camelcase: true */
    };

    $scope.reloadReport();
  });
