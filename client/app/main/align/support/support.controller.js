'use strict';

angular.module('align')
  .controller('SupportCtrl', function ($scope, ngDialog, accountService) {
    $scope.openSubscriptionPlans = function() {
      ngDialog.open({
        template: 'app/main/align/support/subscriptionPlans/subscriptionPlans.html',
        controller: 'SubscriptionPlansCtrl',
        className: 'modal modal-members subscription-plans-modal',
        scope: $scope
      });
    };

    $scope.openEmailTemplates = function() {
      ngDialog.open({
        template: 'app/main/align/support/emailTemplates/emailTemplates.html',
        controller: 'EmailTemplatesCtrl',
        className: 'modal modal-members email-templates-modal',
        scope: $scope
      });
    };

    $scope.openReportForAllAccounts = function() {
      $scope.allAccounts = true;

      ngDialog.open({
        template: 'app/main/align/support/accountReport/accountReport.html',
        controller: 'AccountReportCtrl',
        className: 'modal modal-members account-report-modal',
        scope: $scope
      });
    };

    $scope.openReportForSelectedAccount = function() {
      $scope.allAccounts = false;

      ngDialog.open({
        template: 'app/main/align/support/accountReport/accountReport.html',
        controller: 'AccountReportCtrl',
        className: 'modal modal-members account-report-modal',
        scope: $scope
      });
    };

    accountService.search(null, function(result) {
      $scope.accounts = result.data;
    }, function(error) {
      console.log('Error while loading accounts: ' + error.error.message);
    });
  });
