'use strict';

angular.module('align')
  .factory('AppManagementService', function ($http, apiUrl, identityConverter) {
    return {
      getFacebookAppInfo: function (socialId, appId, successCallback, errorCallback) {
        $http.get(apiUrl + '/api/v2/validate/mobile/appData/?socialId=' + socialId + '&appId=' + appId).then(successCallback, errorCallback);
      },
      registerApp: function (app, teamId, successCallback, errorCallback) {
        $http({
          url: apiUrl + '/api/v2/mobile/app/promotion/?teamId=' + teamId + '&fields=name,type,appId,channelApplications,nativeApplications,socialAccountId,app_name,app_store_url,platform,removable,country',
          method: 'POST',
          data: app,
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },
      deleteApp: function (appId, channelAppId, successCallback, errorCallback) {
        $http.delete(apiUrl + '/api/v2/mobile/app/promotion/' + appId + (channelAppId !== null ? ('/' + channelAppId) : '') + '?fields=name,type,appId,channelApplications,nativeApplications,socialAccountId,app_name,app_store_url,platform,removable')
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      }
    };
  }
);
