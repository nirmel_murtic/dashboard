'use strict';

angular.module('align')
  .controller('AddAppCtrl', function ($rootScope, $scope, ngDialog, Campaign, AppManagementService, awAtpHelper) {
    $scope.twCountryAtpConfig = awAtpHelper.getConfig('iosStoresCountries');
    $scope.twAdAccounts = Campaign.getTwitterAdAccounts($rootScope.fractalAccountInfo.currentTeam);


    $scope.view.existingApps = $rootScope.fractalAccountInfo.currentTeam.applications;

    $scope.chooseAdAcount = function (account) {
      $scope.app.twitter.twitterApp.accountId = account.accountId;
      $scope.app.twitter.twitterApp.accountName = account.name;
    };

    //var storeMapper = {
    //  android: 'googleplayAppUrl',
    //  ipad: 'ipadAppUrl',
    //  iphone: 'iphoneAppUrl'
    //};



    //// Retrieve Facebook app info from typed id
    //$scope.$watch('app.facebook.facebookApp.appId', function (newVal, oldVal) {
    //  if (newVal && newVal !== oldVal && newVal.length > 14 && $scope.app.name) {
    //    var socialAccount = Campaign.getFacebookAdAccounts($rootScope.fractalAccountInfo.currentTeam)[0];
    //    AppManagementService.getFacebookAppInfo(socialAccount.id, newVal, function (result) {
    //      var rawData = result.data.data;
    //      $scope.app.facebook.facebookApp.appId = newVal;
    //      if (_.isEmpty(rawData.result) === false) {
    //        $scope.app.facebook.details = rawData.result;
    //        $scope.app.facebook.isValid = true;
    //        $scope.app.facebook.accountId = socialAccount.accountId;
    //      } else {
    //        //wrong id
    //        $scope.app.facebook.isValid = false;
    //      }
    //    }, function (error) {
    //      console.log(error);
    //    });
    //  }
    //});


    $scope.registerApp = function () {
      var appModel = {
        facebookApp: $scope.app.facebook.facebookApp,
        twitterApp: $scope.app.twitter.twitterApp,
        name: $scope.app.name,
        id: $scope.app.id
      };

      appModel.twitterApp.name = $scope.app.name;
      appModel.facebookApp.name = $scope.app.name;
      appModel.twitterApp.appCountryCode = $scope.app.twitter.twitterApp.country ? $scope.app.twitter.twitterApp.country.iso2 : '';
      var type;

      if ($scope.view.facebook.selected) {
        if ($scope.view.facebook.disabled) {
          if ($scope.view.twitter.android.selected || $scope.view.twitter.ipad.selected || $scope.view.twitter.iphone.selected) {
            type = 'twitter';
          }
        } else {
          if (($scope.view.twitter.android.selected && !$scope.view.twitter.android.disabled) ||
            ($scope.view.twitter.ipad.selected && !$scope.view.twitter.ipad.disabled) ||
            ($scope.view.twitter.iphone.selected && !$scope.view.twitter.iphone.disabled)) {
            type = 'all';
          } else {
            type = 'facebook';
          }
        }
      } else {
        if ($scope.view.twitter.android.selected || $scope.view.twitter.ipad.selected || $scope.view.twitter.iphone.selected) {
          type = 'twitter';
        }
      }

      if(type === 'twitter') {
        delete appModel.facebookApp;
      } else if(type === 'facebook') {
        delete appModel.twitterApp;
      }

      AppManagementService.registerApp(appModel, $rootScope.fractalAccountInfo.currentTeam.id, function (result) {
        var apps = $rootScope.fractalAccountInfo.currentTeam.applications;

        // Check if the application was already registered. If it was, update it. If it was not, add it to the list.
        var registeredApplication = null;
        angular.forEach(apps, function(application) {
          if(application.id === result.data.application.id) {
            registeredApplication =  application;
          }
        });

        if(registeredApplication) {
          apps[apps.indexOf(registeredApplication)] = result.data.application;
        } else {
          apps.push(result.data.application);
        }

      }, function (error) {
        console.log(error);
      });
    };

  });
