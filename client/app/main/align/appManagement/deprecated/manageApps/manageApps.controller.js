'use strict';

angular.module('align')
  .controller('ManageAppsCtrl', function ($rootScope, $scope, Campaign, AppManagementService, $filter, fsConfirm) {

    $scope.deleteApp = function (app, channelApp) {
      var confirmCallBack = function() {
        var channelAppId = channelApp ? channelApp.id : null;

        AppManagementService.deleteApp(app.id, channelAppId, function (result) {
          var apps = $rootScope.fractalAccountInfo.currentTeam.applications;

          var appToDelete = null;

          angular.forEach(apps, function(application) {
            if(application.id === result.data.application.id) {
              appToDelete =  application;
            }
          });

          if(appToDelete) {
            if(channelApp) {
              apps[apps.indexOf(appToDelete)] = result.data.application;
            } else {
              apps.splice(apps.indexOf(appToDelete), 1);
            }
          }
        }, function (error) {
          // Show error message, usually the application can't be deleted because it's currently being used in a goal
          fsConfirm('genericAlert', {
            title:  $filter('translate')('label.align.appManagement.Error'),
            body: error.error.message
          });
        });
      };

      fsConfirm('generic', {
        body: $filter('translate')('label.align.appManagement.Areyousureyouwanttodeletethisapplication')
      }).then(confirmCallBack);
    };
  });
