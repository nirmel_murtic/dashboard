'use strict';

angular.module('align')
  .controller('AppManagementCtrl', function ($rootScope, $scope, ngDialog, Campaign, AppManagementService, awAtpHelper, fsConfirm, $filter, modelOptions) {

    $scope.view = {
      newAppName: '',
      hasFbAccount: Campaign.getFacebookAdAccounts($rootScope.fractalAccountInfo.currentTeam)[0] ? true : false,
      hasTwAccount: Campaign.getTwitterAdAccounts($rootScope.fractalAccountInfo.currentTeam)[0] ? true : false,
      twCountryAtpConfig: awAtpHelper.getConfig('iosStoresCountries'),
      twAdAccounts: Campaign.getTwitterAdAccounts($rootScope.fractalAccountInfo.currentTeam),
      urlModelOptions: modelOptions.newInstance().creativeUrl,
      isSaveButtonDisabled: true,
      nameMaxLength: 64
  };

    $scope.applications = angular.copy($rootScope.fractalAccountInfo.currentTeam.applications);

    function App(name) {
      this.name = name;
      this.facebook = {
        facebookApp: {
          name: '',
          appId: ''
        },
        disabled: false,
        isValid: null
      };
      this.twitter = {
        twitterApp: {
          accountId: '',
          accountName: '',
          googleplayAppId: '',
          ipadAppId: '',
          iphoneAppId: ''
        },
        disabled: {
          android: false,
          ipad: false,
          iphone: false
        },
        isValid: {
          android: null,
          ipad: null,
          iphone: null
        }
      };
    }

    var storeMapper = {
      android: 'googleplayAppUrl',
      ipad: 'ipadAppUrl',
      iphone: 'iphoneAppUrl'
    };

    // If the user clicked on an App in the Align Side Bar, open the app in the modal
    if ($scope.ngDialogData) {
      _.find($scope.applications, {id:$scope.ngDialogData.id}).isExpanded = true;
    }

    $scope.createApp = function (appName) {
      if (!appName) {
        fsConfirm('genericAlert', {body:$filter('translate')('label.align.appManagement.appNameLengthErrorMessage')});
      }
      // Check if there is another app that has the same name
      else if (_.findIndex($scope.applications, {name: appName}) === -1) {
        // Close all other views
        _.forEach($scope.applications, function (application) {
          application.isExpanded = false;
          application.isBeingEdited = false;
        });
        // Create App with the App Name entered
        $scope.currentApp = new App(appName);
        // Open the app details in edit mode
        $scope.currentApp.isExpanded = true;
        $scope.currentApp.isBeingEdited = true;
        // Add app in the top of the list
        $scope.applications.push($scope.currentApp);

        $scope.view.newAppName = '';
      } else {
        fsConfirm('genericAlert', {body:$filter('translate')('label.align.appManagement.appNameErrorMessage')});
      }

    };

    $scope.editApp = function (app) {
      app.isBeingEdited = true;
      $scope.currentApp = generateAppModelForEditMode(app);
    };

    // ------------------------------------ Add App logic -------------------------------------
    // Retrieve Facebook app info from typed id
    $scope.getFacebookAppInfo = function (app) {
      app.facebook.isValid = null;
      $scope.view.isSaveButtonDisabled = true;

      var socialAccount = Campaign.getFacebookAdAccounts($rootScope.fractalAccountInfo.currentTeam)[0];
      AppManagementService.getFacebookAppInfo(socialAccount.id, app.facebook.facebookApp.appId, function (result) {
        var rawData = result.data.data;
        // App ID matches an app on FB, retrieving details
        if (_.isEmpty(rawData.result) === false) {
          app.facebook.details = rawData.result;
          app.facebook.isValid = true;
          app.facebook.accountId = socialAccount.accountId;
          $scope.view.isSaveButtonDisabled = false;
        } else {
          // App Id doesn't exist on FB
          app.facebook.isValid = false;
          $scope.view.isSaveButtonDisabled = true;
        }
      }, function (error) {
        console.log(error);
      });
    };

    $scope.deleteAccompliceApp = function (app) {
      // The app has already been saved in the database in the past, remove it from database and view
      if (app.id) {
        var confirmCallBack = function () {
          AppManagementService.deleteApp(app.id, null, function () {
            _.remove($rootScope.fractalAccountInfo.currentTeam.applications, {id: app.id});
            $scope.applications = angular.copy($rootScope.fractalAccountInfo.currentTeam.applications);

          }, function (error) {
            // Show error message, usually the application can't be deleted because it's currently being used in a goal
            fsConfirm('genericAlert', {
              title: $filter('translate')('label.align.appManagement.Error'),
              body: error.error.message
            });
          });
        };

        fsConfirm('generic', {
          body: $filter('translate')('label.align.appManagement.Areyousureyouwanttodeletethisapplication')
        }).then(confirmCallBack);

      } else { // The app was never saved in the database, this is acting like a cancel button
        _.remove($rootScope.fractalAccountInfo.currentTeam.applications, {name: app.name});
        $scope.applications = angular.copy($rootScope.fractalAccountInfo.currentTeam.applications);
      }
    };

    $scope.deleteChannelApp = function (app, channelApp) {
      var confirmCallBack = function () {
        AppManagementService.deleteApp(app.id, channelApp.id, function (result) {

          // Retrieve index of the App in the RootScope application array and update the app
          var accompliceAppInRootScopeApplicationsIndex = _.findIndex($rootScope.fractalAccountInfo.currentTeam.applications, {id: result.data.application.id});
          $rootScope.fractalAccountInfo.currentTeam.applications[accompliceAppInRootScopeApplicationsIndex] = result.data.application;
          $scope.applications = angular.copy($rootScope.fractalAccountInfo.currentTeam.applications);

        }, function (error) {
          // Show error message, usually the application can't be deleted because it's currently being used in a goal
          fsConfirm('genericAlert', {
            title: $filter('translate')('label.align.appManagement.Error'),
            body: error.error.message
          });
        });
      };

      fsConfirm('generic', {
        body: $filter('translate')('label.align.appManagement.Areyousureyouwanttodeletethisapplication')
      }).then(confirmCallBack);
    };

    $scope.saveChanges = function (app) {
      var appModel = {
        facebookApp: app.facebook.facebookApp,
        twitterApp: app.twitter.twitterApp,
        name: app.name,
        id: app.id
      };

      appModel.twitterApp.name = app.name;
      appModel.facebookApp.name = app.name;
      appModel.twitterApp.appCountryCode = app.twitter.twitterApp.country ? app.twitter.twitterApp.country.iso2 : '';

      var channelsToUpdate = [], type;

      if (app.facebook.disabled && app.twitter.disabled.iphone && app.twitter.disabled.iphone && app.twitter.disabled.android) {
        // Error message, user needs to delete an app before going moving forward
      } else if (!app.facebook.disabled && app.facebook.facebookApp) {
        channelsToUpdate.push('facebook');
      } else if ((!app.twitter.disabled.iphone && app.twitter.twitterApp.iphoneAppId) ||
        (!app.twitter.disabled.iphone && app.twitter.twitterApp.ipadAppId) ||
        (!app.twitter.disabled.android && app.twitter.twitterApp.googleplayAppId)) {
        channelsToUpdate.push('twitter');
      }

      if (channelsToUpdate.length === 2) {
        type = 'all';
      } else if (channelsToUpdate.length === 1) {
        type = channelsToUpdate[0];
      }

      if (type === 'twitter') {
        delete appModel.facebookApp;
        app.twitter.isValid = app.twitter.isValid.iphone && app.twitter.isValid.ipad && app.twitter.isValid.android;
      } else if (type === 'facebook') {
        delete appModel.twitterApp;
      } else if (type === 'all') {
        app.twitter.isValid = app.twitter.isValid.iphone && app.twitter.isValid.ipad && app.twitter.isValid.android;
      }

      var isAccompliceAppValid = true;

      _.forEach(channelsToUpdate, function(channel) {
        isAccompliceAppValid = app[channel].isValid && isAccompliceAppValid;
      });

      if (isAccompliceAppValid) {
        AppManagementService.registerApp(appModel, $rootScope.fractalAccountInfo.currentTeam.id, function (result) {
          // If we register the app for the first time, we add it to the rootScope array
          if (!app.id) {
            app.id = result.data.application.id;
            $rootScope.fractalAccountInfo.currentTeam.applications.push(result.data.application);
          } else { // If we are updating the app, we update the app in the rootScope array too
            var appToUpdateIndex = _.findIndex($rootScope.fractalAccountInfo.currentTeam.applications, {id: result.data.application.id});
            $rootScope.fractalAccountInfo.currentTeam.applications[appToUpdateIndex] = result.data.application;
          }
          $scope.applications = $rootScope.fractalAccountInfo.currentTeam.applications;

        }, function (error) {
          console.log(error);
        });
      } else {
        fsConfirm('genericAlert', {body: 'There are errors for this app. Please correct them before trying to save again'});
      }
    };

    function generateAppModelForEditMode(selectedApp) {
      // Reset the app
      var app = new App(selectedApp.name);
      app.id = selectedApp.id;

      //$scope.view.clearEverything('existing');
      app.facebook.facebookApp.stores = [];
      app.facebook.disabled = false;
      app.removable = selectedApp.removable;

      _.forEach(selectedApp.channelApplications, function (channelApp) {
        if (channelApp.type === 'Facebook') {
          app.facebook.disabled = true;
          app.facebook.facebookApp.appId = Number(channelApp.appId);
          app.facebook.facebookApp.id = channelApp.id;
          _.forEach(channelApp.nativeApplications, function (selectedApp) {
            app.facebook.facebookApp.stores.push({
              link: selectedApp.app_store_url, // jshint ignore:line
              platform: selectedApp.platform
            });
          });
        } else if (channelApp.type === 'Twitter') {
          app.twitter.twitterApp.appId = +channelApp.appId;
          app.twitter.twitterApp.id = channelApp.id;
          app.twitter.twitterApp.country = {
            iso2: channelApp.country
          };

          _.forEach(channelApp.nativeApplications, function (selectedApp) {
            app.twitter.disabled[selectedApp.platform.toLowerCase()] = true;
            app.twitter.twitterApp[storeMapper[selectedApp.platform.toLowerCase()]] = selectedApp.app_store_url; // jshint ignore:line
          });

          var twAccounts = Campaign.getTwitterAdAccounts($rootScope.fractalAccountInfo.currentTeam);

          var appAccount = null;

          _.forEach(twAccounts, function (account) {
            _.forEach(selectedApp.channelApplications, function (channelApp) {
              if (account.accountId === channelApp.socialAccountId) {
                appAccount = account;
                return false;
              }
            });
          });

          if (appAccount) {
            app.twitter.twitterApp.accountId = appAccount.accountId;
            app.twitter.twitterApp.accountName = appAccount.name;
          }
        }
      });
      return app;
    }
  });
