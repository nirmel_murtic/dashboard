'use strict';

angular.module('align')
  .controller('MemberManagementCtrl', function ($scope, $rootScope, userService, teamService, ngDialog, $state, $q, permissionUtils, $filter, regex, fsConfirm) {
    $scope.tabs = {
      inviteMembersTab: true,
      manageMembersTab: false
    };

    $scope.emailRegex = regex.EMAIL;

    $scope.filteredTeams = $rootScope.accountTeams;

    var self = this;

    $scope.permissionGroups = [
      {
        groupId: 0,
        groupName: 'Manage Billing & Account Info',
        permissions: [ 'MBAAI' ]
      },
      {
        groupId: 1,
        groupName: 'Manage & Create Teams',
        permissions: [ 'MTM' ]
      },
      {
        groupId: 2,
        groupName: 'Upload, Edit & Delete Assets in Library',
        permissions: [ 'MCL', 'ACL' ]
      },
      {
        groupId: 3,
        groupName: 'Manage Site & Tracking Script',
        permissions: [ 'MW' ]
      },
      {
        groupId: 4,
        groupName: 'Invite & Manage Members',
        permissions: [ 'MMB' ]
      },
      {
        groupId: 5,
        groupName: 'Create & Manage Projects',
        permissions: [ 'MG' ]
      },
      {
        groupId: 6
      },
      {
        groupId: 7,
        groupName: 'Manage Organic Posts',
        permissions: [ '!RABP', '!IPOC', 'CAO', 'CAP' ]
      },
      {
        groupId: 8
      },
      {
        groupId: 9,
        groupName: 'Launch a paid campaign',
        permissions: [ 'PALC', 'MEC' ]
      },
      {
        groupId: 10,
        groupName: 'Download Reports, Including Spend',
        permissions: [ '!ARAR' ]
      }
    ];

    this.refreshPermissionGroups = function(permissions, user) {
      for(var i=0;i<user.permissionGroups.length;i++){
        user.permissionGroups[i].selected = false;
      }

      if(!user.selectedRole) {
        return;
      }

      /*jshint sub:true*/
      if(!permissions['ARAR']) {
        user.permissionGroups[10].selected = true;
      }

      if(permissions['PALC'] || permissions['MEC']) {
        user.permissionGroups[9].selected = true;
      }

      if(permissions['RABP'] && permissions['IPOC'] && !permissions['CAO'] && !permissions['CAP'] || !permissions['RABP'] && !permissions['IPOC'] && permissions['CAO'] && permissions['CAP']) {
        user.permissionGroups[7].selected = true;
      }

      if(permissions['MG']) {
        user.permissionGroups[5].selected = true;
      }

      if(permissions['MMB']) {
        user.permissionGroups[4].selected = true;
      }

      if(permissions['MW']) {
        user.permissionGroups[3].selected = true;
      }

      if(permissions['MCL'] || permissions['ACL']) {
        user.permissionGroups[2].selected = true;
      }

      if(permissions['MTM']) {
        user.permissionGroups[1].selected = true;
      }

      if(permissions['MBAAI']) {
        user.permissionGroups[0].selected = true;
      }
      /*jshint sub:false*/
    };

    $scope.permissionGroupClicked = function(group, user) {
      for(var i = 0; i < group.permissions.length; i++) {
        var neg = group.permissions[i].indexOf('!') !== -1;

        var perm = neg ? group.permissions[i].replace('!', '') : group.permissions[i];

        user.selection[perm] = neg ? !group.selected : group.selected;

        if(!group.selected && (group.groupId === 7 || group.groupId === 8)) {
          user.selection[perm] = false;
        }

        $scope.permissionClicked(perm, user, true);
      }

      var role = user.selectedRole ? user.selectedRole : user.userRole;

      self.updateCustomRole(role.id, user.selection, user);
    };

    this.emptyUser = {
      emailAddress: '',
      selectedTeams: [],
      firstName: '',
      lastName: '',
      userRoleId: '',
      accessPeriodStart: '',
      accessPeriodEnd: '',
      selection: {},
      permissions: {},
      identifier: 0
    };

    $scope.selectedPermissionGroupsCount = function(user) {
      var count = 0;

      angular.forEach(user.permissionGroups, function(group) {
        if(group.selected) {
          count++;
        }
      });

      return count;
    };

    $scope.inviteUsersData = [
      angular.copy(this.emptyUser)
    ];

    this.populateTeamsList = function(user) {
      user.teamsList = [];

      for(var i=0;i<$scope.filteredTeams.length;i++){
          user.teamsList.push({
              name: $scope.filteredTeams[i].name,
              id: $scope.filteredTeams[i].id,
              originalTeam: $scope.filteredTeams[i],
              user: user
          });
      }
    };

    this.populatePermissionGroups = function(user) {
      user.permissionGroups = [];

      for(var i=0;i<$scope.permissionGroups.length;i++){
        user.permissionGroups.push({
          groupId: $scope.permissionGroups[i].groupId,
          groupName: $scope.permissionGroups[i].groupName,
          permissions: $scope.permissionGroups[i].permissions
        });
      }
    };

    angular.forEach($scope.inviteUsersData, function(user) {
      self.populateTeamsList(user);
      self.populatePermissionGroups(user);
    });

    this.getTeamIndexByName = function(teamList, name) {
      for(var i = 0; i<teamList.length; i++) {
        if(teamList[i].name === name) {
          return i;
        }
      }

      return -1;
    };

    this.getTeamByName = function(teamList, name) {
      var result = null;

      angular.forEach(teamList, function(value) {
        if(value.name.toLowerCase() === name.toLowerCase()) {
          result = value;

          return false;
        }
      });

      return result;
    };

    $scope.teamClicked = function(team, user) {
      if(team.selected) {
        for (var i = 0; i < user.selectedTeams.length; i++) {
          if (user.selectedTeams[i].name === team.name) {
            return;
          }
        }

        user.selectedTeams.push({
          name: team.name,
          id: team.id
        });
      } else {
        var index = self.getTeamIndexByName(user.selectedTeams, team.name);

        if(index !== -1) {
          user.selectedTeams.splice(index, 1);
        }
      }
    };

    $scope.inviteMore = function(newUser) {
      if(!newUser) {
        newUser = angular.copy(self.emptyUser);
        newUser.identifier = $scope.inviteUsersData.length;
      }

      self.populateTeamsList(newUser);
      self.populatePermissionGroups(newUser);

      newUser.permissions = jQuery.extend(true, {}, $scope.permissions);

      $scope.inviteUsersData.push(newUser);
    };

    this.refreshSelectionForRole = function(role, user) {
      var selectionObj = {} ;

      if(!role) {
        user.selection = selectionObj;

        self.refreshPermissionGroups(user.selection, user);

        return;
      }

      delete $scope.selectedRolePermissions;

      for(var i=0;i<$scope.roles.length;i++){
        if($scope.roles[i].id === role.id){
          $scope.selectedRolePermissions = $scope.roles[i];
        }
      }

      if($scope.selectedRolePermissions){
        for(var j=0;j<$scope.selectedRolePermissions.permissions.length;j++){
          var perm = $scope.selectedRolePermissions.permissions[j];

          selectionObj[perm.name] = true;
        }
      }

      user.selection = selectionObj;

      self.refreshPermissionGroups(user.selection, user);
    };

    $scope.roleSelected = function(role, user, skipCloseDropdown) {
      user.selectedRole = role;

      self.refreshSelectionForRole(role, user);

      if(!skipCloseDropdown) {
        $rootScope.$broadcast('closeDropdown');
      }
    };

    this.saveCustomRole = function(roleName, selection, user){
      delete selection.LIN;

      $scope.processing = true;

      userService.addCustomRole(roleName, selection,
        function(data) {
          delete $scope.processing;

          $scope.roles.push(data.data.role);

          $scope.roleSelected(data.data.role, user, false);

          if(user.userId) {
            $scope.updateRoleForUser(data.data.role, user);
          }
        }, function (error) {
          delete $scope.processing;

          console.log(error.error.code);
        });
    };

    this.updateCustomRole = function(roleId, selection){
      delete selection.LIN;

      if($scope.canceler) {
        $scope.canceler.resolve();
      }

      $scope.canceler = $q.defer();

      $scope.processing = true;

      userService.updateCustomRole(roleId, selection, $scope.canceler,
        function(data) {
          delete $scope.processing;

          for(var i=0;i<$scope.roles.length;i++){
            if($scope.roles[i].id === data.data.role.id){
              $scope.roles[i].permissions = data.data.role.permissions;
              $scope.roles[i].name = data.data.role.name;
            }
          }

          angular.forEach($scope.inviteUsersData, function(user) {
            if(user.selectedRole && user.selectedRole.id === data.data.role.id) {
              user.selectedRole.permissions = data.data.role.permissions;
              user.selectedRole.name = data.data.role.name;

              self.refreshSelectionForRole(user.selectedRole, user);
            }

            delete $scope.canceler;
          });

          angular.forEach($rootScope.fractalAccountInfo.currentTeam.allMembers, function(user) {
            if(user.userRole && user.userRole.id === data.data.role.id) {
              user.userRole.permissions = data.data.role.permissions;
              user.userRole.name = data.data.role.name;

              self.refreshSelectionForRole(user.userRole, user);
            }
          });
        }, function (error) {
          if(error) {
            delete $scope.processing;

            delete $scope.canceler;
            console.log(error.error.code);
          }
        });
    };

    $scope.addCustomRole = function(customRoleName, user){
      delete user.selectedRole;

      var selectionObj = {};
      for(var i=0;i<$scope.roles.length;i++){
          if($scope.roles[i].name === 'VIEWER'){
              $scope.permissionsCustomDefault = $scope.roles[i].permissions;
              for(var j=0;j<$scope.permissionsCustomDefault.length;j++){
                  var perm = $scope.permissionsCustomDefault[j];

                selectionObj[perm.name] = true;
              }
          }
      }

      user.selection = selectionObj;

      self.saveCustomRole(customRoleName, user.selection, user);
    };

    $scope.removeCustomRole = function(customRole, user, $event) {
        $event.stopPropagation();

        userService.removeCustomRole(customRole.id, function() {

          delete $scope.rolesMap[customRole.id];

          var index = $scope.roles.indexOf(customRole);

          if(index !== -1) {
            $scope.roles.splice(index, 1);
          }

          angular.forEach($scope.inviteUsersData, function(user) {
            if(user.selectedRole && user.selectedRole.id === customRole.id) {
              user.selectedRole = null;
              user.selection = {};

              self.refreshSelectionForRole(user.selectedRole, user);
            }
          });
        }, function (error) {
          console.log(error.error.code);
        });

      return false;
    };

    this.selectedPermissionsCount = function(selection) {
      var i = 0;

      angular.forEach(selection, function(value, key) {
        if(value && key !== 'LIN') {
          i++;
        }
      });

      return i;
    };

    $scope.permissionClicked = function(permName, user, skipUpdate) {
      var selected = user.selection[permName];

      var oldSelection = angular.copy(user.selection);

      if(selected) {
        var parentPerm = permissionUtils.getParentForPermission(permName, $scope.permissions);

        if(parentPerm) {
          if(!user.selection[parentPerm]) {
            user.selection[parentPerm] = true;

            $scope.permissionClicked(parentPerm, user, true);
          }
        }
      } else {
        var childPermissions = permissionUtils.getChildrensOfPermission(permName, $scope.permissions);

        if(childPermissions) {
          angular.forEach(childPermissions, function(item) {
            if(user.selection[item.key]) {
              user.selection[item.key] = false;

              $scope.permissionClicked(item.key, user, true);
            }
          });
        }
      }

      if(!skipUpdate && self.selectedPermissionsCount(user.selection) < 1) {
        user.selection = oldSelection;
        user.selection[permName] = true;

        return;
      }

      if(!skipUpdate && user.selectedRole && user.selectedRole.id){
        self.updateCustomRole(user.selectedRole.id, user.selection, user);
      }
    };

    userService.getUserRoles(function(result) {
        $scope.roles = result.data.userRoles;
        $scope.rolesMap = {};
        angular.forEach($scope.roles, function(value) {
            $scope.rolesMap[value.id] = value;
        });

        self.populateMembers();
    }, function () {
        console.log('Error while loading roles!');
    });

    this.getUserRoleByName = function(name) {
      var result = null;

      angular.forEach($scope.roles, function(value) {
        if(value.name.toLowerCase() === name.toLowerCase()) {
          result = value;

          return false;
        }
      });

      return result;
    };

    this.getUserByEmail = function(email) {
      var result = null;

      angular.forEach($scope.inviteUsersData, function(user) {
        if(user.emailAddress === email) {
          result = user;
          return false;
        }
      });

      return result;
    };

    userService.getPermissions(function(result){
        $scope.permissions = result.data.result;

        angular.forEach($scope.inviteUsersData, function(user) {
          user.permissions = jQuery.extend(true, {}, $scope.permissions);
        });
    }, function (){
        console.log('Error on loading permissions!');
    });

    $scope.sendInvite = function(){
        var userInvites = [];

        angular.forEach($scope.inviteUsersData, function(user) {
          if(user.success) {
            return true;
          }

          var teamsData = [];
          for(var i=0;i<user.selectedTeams.length;i++) {
              teamsData.push(user.selectedTeams[i].id);
          }

          if(teamsData.length === 0) {
            user.error = $filter('translate')('label.memberManagement.Atleastoneteamneedstobeselected');

            return true;
          }

          userInvites.push({
            emailAddress: user.emailAddress,
            teamIds: teamsData,
            firstName: user.firstName,
            lastName: user.lastName,
            userRoleId: user.selectedRole ? user.selectedRole.id : null
          });
        });

        if(userInvites.length === 0) {
          return;
        }

        $scope.processing = true;

        teamService.inviteUsers(userInvites,
            function(result) {
              delete $scope.processing;

              var hasErrors = false;

              angular.forEach(result.data, function(item, key) {
                var user = self.getUserByEmail(key);

                if(item.data && item.data.success) {
                  user.success = true;
                  delete user.error;
                } else {
                  hasErrors = true;
                  user.error = item.message;
                }
              });

              if(!hasErrors) {
                $state.reload();
                ngDialog.close();
              }
            }, function (error){
              delete $scope.processing;

              angular.forEach($scope.inviteUsersData, function(user) {
                if(!user.success) {
                  user.error = error.error.message;
                }
              });
        });
    };

    $scope.onCsvImport = function() {
      $scope.inviteUsersData = [];

      var identifier = 0;

      angular.forEach(this.result, function(item) {
        var newUser = angular.copy(self.emptyUser);
        newUser.identifier = identifier;
        newUser.firstName = item.firstName;
        newUser.lastName = item.lastName;
        newUser.emailAddress = item.emailAddress;

        $scope.inviteMore(newUser);

        $scope.roleSelected(self.getUserRoleByName(item.role), newUser, true);

        var teams = item.teams ? item.teams.split(';') : [];

        angular.forEach(teams, function(team) {
          var t = self.getTeamByName(newUser.userList, team);

          if(t) {
            t.selected = true;
            $scope.teamClicked(t, newUser);
          }
        });

        identifier +=  1;
      });
    };

    $scope.removeUser = function(index) {
      if($scope.inviteUsersData.length > 1) {
        $scope.inviteUsersData.splice(index, 1);
      }
    };

    // Members management
    $scope.membersPerRole = {};

    self.populateMembers = function() {
      $scope.membersPerRole = {};
      $scope.customRoleNames = [];

      angular.forEach($rootScope.fractalAccountInfo.currentTeam.allMembers, function(user) {
        if(!$scope.membersPerRole[user.userRole.name]) {
          $scope.membersPerRole[user.userRole.name] = [];

          if(user.userRole.accountId) {
            $scope.customRoleNames.push(user.userRole.name);
          }
        }

        self.populatePermissionGroups(user);
        self.populateTeamsList(user);

        var teams = user.allTeams ? user.allTeams : user.activeTeams;

        user.selectedTeams = [];

        angular.forEach(teams, function(team) {
          user.selectedTeams.push({
            name: team.name,
            id: team.id
          });

          var t = self.getTeamByName(user.teamsList, team.name);

          if(t) {
            t.selected = true;
          }
        });

        if(user.userRole) {
          self.refreshSelectionForRole(user.userRole, user);
          $scope.roleSelected(user.userRole, user);
        }

        $scope.membersPerRole[user.userRole.name].push(user);
      });
    };

    $scope.updateRoleForUser = function(role, user) {
      $rootScope.$broadcast('closeDropdown');

      if(user.userRole && user.userRole.id === role.id) {
        return;
      }

      user.updateProcessing = true;

      userService.assignUserRole(user.userId, role.id,
        function () {
          user.userRole = role;

          self.populateMembers();

          user.updateProcessing = false;
        }, function (error) {
          user.updateProcessing = false;

          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
    };

    $scope.teamUpdated = function(team, user) {
      user.updateProcessing = true;

      if(team.selected) {
        for (var i = 0; i < user.selectedTeams.length; i++) {
          if (user.selectedTeams[i].name === team.name) {
            return;
          }
        }

        var userInviteData = {
          emailAddress: user.username,
          teamIds: [team.id]
        };

        teamService.inviteUser(userInviteData,
          function() {
            if(team.originalTeam.enabled) {
              user.activeTeams.push(team.originalTeam);
            }

            user.allTeams.push(team.originalTeam);

            if(team.originalTeam.activeMembers && user.enabled) {
              team.originalTeam.activeMembers.push(user);
            }

            if(team.originalTeam.allMembers) {
              team.originalTeam.allMembers.push(user);
            }

            user.selectedTeams.push({
              name: team.name,
              id: team.id
            });

            user.updateProcessing = false;
          }, function (error){
            team.selected = false;
            user.updateProcessing = false;

            fsConfirm('genericAlert', {
              body: error.error.message
            });
          }
        );
      } else {
        if(user.selectedTeams.length < 2 || (user.userId === $rootScope.fractalAccountInfo.userId && $rootScope.fractalAccountInfo.currentTeam.id === team.id)) {
          team.selected = true;
          user.updateProcessing = false;

          return;
        }

        self.removeMemberTeam(team, user, function(team, user) {
          self.removeTeamFrom(user, team.id, 'activeTeams');
          self.removeTeamFrom(user, team.id, 'allTeams');

          self.removeUserFrom(user, team.originalTeam.allMembers);
          self.removeUserFrom(user, team.originalTeam.activeMembers);

          var index = self.getTeamIndexByName(user.selectedTeams, team.name);

          if(index !== -1) {
            user.selectedTeams.splice(index, 1);
          }

          user.updateProcessing = false;
        });
      }
    };

    self.removeUserFrom = function(user, list) {
      if(list) {
        var index = list.indexOf(user);

        if(index !== -1) {
          list.splice(index, 1);
        }
      }
    };

    self.removeMemberTeam = function(team, user, callback) {
      teamService.removeUser(user.userId, team.id,
        function() {
          callback(team, user);
        }, function (error){
          user.updateProcessing = false;

          team.selected = true;

          fsConfirm('genericAlert', {
            body: error.error.message
          });
        });
    };

    self.removeTeamFrom = function(user, teamId, teamsType) {
      var teams = user[teamsType];

      if(!teams) {
        return false;
      }

      for (var i=0; i<teams.length; i++){
        if(teams[i].id === teamId) {
          var memberIndex = teams.indexOf(teams[i]);
          if(memberIndex > -1){
            teams.splice(memberIndex, 1);

            return true;
          }
        }
      }

      return false;
    };

    if($scope.openDefinedMember) {
      $scope.tabs = {
        inviteMembersTab: false,
        manageMembersTab: true
      };


      angular.forEach($scope.fractalAccountInfo.currentTeam.activeMembers, function(member) {
        if(member.userId === $scope.openDefinedMember.userId) {
          member.expanded = true;
        } else {
          member.expanded = false;
        }
      });
    }
  });
