'use strict';

angular.module('align', ['main', 'ngClipboard'])
  .config(function ($stateProvider) {
    if(window.recurly && !recurly.configured) {
      recurly.configure({publicKey: window.configuration.recurlyPublicToken});
    }

    $stateProvider
      .state('main.align', {
      	permissions: '<LOGGED_IN> && <ALIGN>',
        url: '/setup?emailSetting&subscribePlan',
        views: {
          '@main': {
            templateUrl: 'app/main/align/align.html',
            controller: 'AlignCtrl'
          },
          'alignSidebar@main.align': {
            templateUrl: 'app/main/align/alignSidebar/alignSidebar.html',
            controller: 'AlignsidebarCtrl'
          },
          'alignMain@main.align': {
            controller: function($scope, $state) {
              var parentState = 'main.align',
                defaultChildState = '.projects';
              if($state.current.name.substr(-parentState.length) === parentState) {
                $state.go(defaultChildState);
              }
            }
          }
        },
        data: {
          displayName: 'Setup',
          active: 'align'
        },
        resolve: {
          subscribePlan : function($stateParams) {
            return $stateParams.subscribePlan === 'true';
          },
          emailSetting : function($stateParams) {
            return $stateParams.emailSetting === 'true';
          },
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/align/align');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
