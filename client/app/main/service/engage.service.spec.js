'use strict';

describe('Service: engage', function () {

  // load the service's module
  beforeEach(module('main'));

  // instantiate service
  var engage;
  beforeEach(inject(function (_engage_) {
    engage = _engage_;
  }));

  it('should do something', function () {
    expect(!!engage).toBe(true);
  });

});
