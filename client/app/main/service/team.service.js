'use strict';

angular.module('main')
  .factory('teamService', function ($http, identityConverter, apiUrl, $upload) {
    return {
        activateTeam: function (teamId, successCallback, errorCallback) {
            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/team/activate?fields=id,name,enabled',
                data: $.param({ teamId: teamId })
            })
            .success(function(result) {
                successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        },
        getTeamSocialAccounts: function (teamId, successCallback, errorCallback){
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/team/' + teamId + '?joins=allSocialAccounts.social;allChannelAccounts;allPages&fields=socialAccounts,accountId,name,social,socialId,socialUsername,socialType,pages,fbAccounts,adwordsAccounts,twAccounts,instagramAccounts,username,profilePic,promotableUsers,userId,screenName,currency,pageName,pagePageId,pageCategory,pageId,createdAt,image,flagged,removable,thirdPartyPixels'
          })
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        },
        deactivateTeam: function (teamId, successCallback, errorCallback) {
            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/team/deactivate?fields=id,name,enabled',
                data: $.param({ teamId: teamId })
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        /*
             userInvite = {
                 emailAddress: 'vlad@fractalsciences.com', // REQUIRED
                 teamIds: [1652], // REQUIRED
                 firstName: 'Vlad',
                 lastName: 'Budkov',
                 userRoleId: 1, // If not provided VIEWER role will be associated to user
                 accessPeriodStart: 1413372370491
                 accessPeriodEnd: 1414372370491
             }
         */
        inviteUser: function(userInvite,
                             successCallback, errorCallback) {

            userInvite.activateActionUrl = '#/activate';

            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/team/inviteUser?fields=userId,username,firstName,lastName,teamIds,userRoleId',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: userInvite
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        /*
             userInvites = [
                {
                     emailAddress: 'vlad@fractalsciences.com', // REQUIRED
                     teamIds: [1652], // REQUIRED
                     firstName: 'Vlad',
                     lastName: 'Budkov',
                     userRoleId: 1, // If not provided VIEWER role will be associated to user
                     accessPeriodStart: 1413372370491
                     accessPeriodEnd: 1414372370491
                }
             ]

             Method will partially fail in case one user invite fail.
             Other user invites will be processed successfully.
         */
        inviteUsers: function(userInvites,
                             successCallback, errorCallback) {

            for(var i = 0; i<userInvites.length; i++) {
                userInvites[i].activateActionUrl = '#/activate';
            }

            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/team/inviteUsers?fields=userId,username,firstName,lastName',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: userInvites
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        createTeam: function(teamData, successCallback, errorCallback){
            $http({
              method: 'POST',
              url: apiUrl + '/api/v2/team/createTeam?fields=userId,username,firstName,lastName,userRole,enabled,name,activeTeams,allTeams,defaultTeam,fractalAccount,userLimit,locale,activeMembers,allMembers,image,guid,fullGuid,extension,createdAt,owner,socialAccounts,applications,currentProjectId',
              data: $.param(teamData)
            })
              .success(function(result) {
                successCallback(identityConverter.convert(result, '@fsid'), result);
              })
              .error(errorCallback);
        },
        addSocialAccount: function(account, tokens, successCallback, errorCallback) {
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/social/addAccount?fields=account,socialId,accountId,name,social,socialUsername,socialType,pages,fbAccounts,adwordsAccounts,twAccounts,instagramAccounts,username,profilePic,promotableUsers,userId,screenName,currency,pageName,pagePageId,pageCategory,pageId,image,flagged,removable,thirdPartyPixels&' + $.param(tokens),
            headers: {
              'Content-Type': 'application/json'
            },
            data: account
          })
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        },
        removeSocialAccount: function(accountId, successCallback, errorCallback) {
          $http({
            method: 'DELETE',
            url: apiUrl + '/api/v2/social/account/'+ accountId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        removeUser: function(userId, teamId, successCallback, errorCallback) {
          $http({
            method: 'DELETE',
            url: apiUrl + '/api/v2/team/removeUser?userId=' + userId + '&teamId=' + teamId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        changeTeamName: function (teamId, teamName, successCallback, errorCallback) {
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/team/changeTeamName?fields=id,name',
            data: $.param({ teamId: teamId, teamName: teamName })
          })
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        },
        changeTeamImage: function(teamId, file, successCallback, errorCallback, progressCallback) {
          $upload.upload({
            url: apiUrl + '/api/v2/team/changeImage?fields=teamId,image,guid,fullGuid,extension',
            data: { teamId: teamId },
            file: file
          })
            .progress(progressCallback)
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        },
        removeTeam: function(teamId, successCallback, errorCallback) {
          $http({
            method: 'DELETE',
            url: apiUrl + '/api/v2/team/removeTeam?teamId=' + teamId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        getGoogleAccountInfo: function(accessToken, successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/social/google/accountInfo?accessToken=' + accessToken
          })
            .success(successCallback)
            .error(errorCallback);
        },
        setCurrentProject: function(teamId, projectId, successCallback, errorCallback) {
          return $http({
            method: 'POST',
            data: $.param({ projectId: projectId, teamId: teamId }),
            url: apiUrl + '/api/v2/team/setCurrentProject?fields=currentProjectId'
          })
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        }
    };
  });
