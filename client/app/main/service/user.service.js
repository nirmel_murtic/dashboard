'use strict';

angular.module('main')
  .factory('userService', function ($http, identityConverter, apiUrl, $upload, $window, $rootScope) {
    return {
        getPermissions: function (successCallback, errorCallback){
            $http({
                method: 'GET',
                url: apiUrl + '/api/v2/user/permissionsTree'
            })
            .success(successCallback)
            .error(errorCallback);
        },
        getUser: function (successCallback, errorCallback){
            return $http({
                method: 'GET',
                url: apiUrl + '/api/v2/user/getUser?joins=fractalAccount.teams.applications;teamMembers.user.userRole,image&fields=userId,username,firstName,lastName,userRole,accountId,enabled,name,activeTeams,allTeams,defaultTeam,fractalAccount,userLimit,locale,activeMembers,allMembers,image,guid,fullGuid,extension,createdAt,owner,applications,name,type,appId,channelApplications,nativeApplications,socialAccountId,app_name,app_store_url,platform,removable,currentProjectId,emailNotifications,country'
            })
                .success(function(result) {
                    var r = identityConverter.convert(result, '@fsid');

                    $.extend($window.configuration, r.data.configuration);

                    $rootScope.$broadcast('Configuration:loaded', $window.configuration);

                    successCallback(r, result);
                })
                .error(errorCallback);
        },
        changeFirstLastName: function(firstName, lastName, successCallback, errorCallback) {
            $http({
                method: 'POST',
                data: $.param({ firstName: firstName, lastName: lastName }),
                url: apiUrl + '/api/v2/user/changeFirstLastName?fields=firstName,lastName'
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        changeUserEmail: function(emailData, successCallback, errorCallback){
            $http({
                method: 'POST',
                data: $.param(emailData),
                url: apiUrl + '/api/v2/user/changeEmailAddress'
            })
                .success(successCallback)
                .error(errorCallback);
        },
        changeUserPassword: function(passwordData, successCallback, errorCallback) {
            $http({
                method: 'POST',
                data: $.param(passwordData),
                url: apiUrl + '/api/v2/user/changePassword'
            })
                .success(successCallback)
                .error(errorCallback);
        },
        changeMasterPassword: function(passwordData, successCallback, errorCallback) {
          $http({
            method: 'POST',
            data: $.param(passwordData),
            url: apiUrl + '/api/v2/user/changeMasterPassword'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        getUserRoles: function(successCallback, errorCallback) {
            $http({
                method: 'GET',
                url: apiUrl + '/api/v2/user/getUserRoles?fields=id,accountId,name,permissions'
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        assignUserRole: function(userId, userRoleId, successCallback, errorCallback) {
            $http({
                method: 'POST',
                data: $.param({userRoleId: userRoleId, userId: userId }),
                url: apiUrl + '/api/v2/user/assignUserRole?fields=userRole,id,name'
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        /*
         roleName = "Custom role name"
         permissions = { "AAD": "AAD", "ACL": "ACL", "ADT": "ADT" }
         */
        addCustomRole: function(roleName, permissions, successCallback, errorCallback) {
            var permArray = [];

            angular.forEach(permissions, function(value, key) {
              if(value) {
                permArray.push(key);
              }
            });

            $http({
                method: 'POST',
                data: $.param({roleName: roleName, permissions: permArray.join(',') }),
                url: apiUrl + '/api/v2/user/addCustomRole?fields=id,name,accountId,permissions'
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        /*
         userRoleId = 1001
         permissions = { "AAD": "AAD", "ACL": "ACL", "ADT": "ADT" }
         */
        updateCustomRole: function(userRoleId, permissions, canceler, successCallback, errorCallback) {
            var permArray = [];

            angular.forEach(permissions, function(value, key) {
              if(value) {
                permArray.push(key);
              }
            });

            $http({
                method: 'POST',
                data: $.param({userRoleId: userRoleId, permissions: permArray.join(',') }),
                timeout: canceler.promise,
                url: apiUrl + '/api/v2/user/updateCustomRole?fields=id,name,accountId,permissions'
            })
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        removeCustomRole: function(userRoleId, successCallback, errorCallback) {
            $http({
                method: 'DELETE',
                url: apiUrl + '/api/v2/user/removeCustomRole?userRoleId=' + userRoleId
            })
                .success(successCallback)
                .error(errorCallback);
        },
        changeUserImage: function(userId, file, successCallback, errorCallback, progressCallback) {
            $upload.upload({
                url: apiUrl + '/api/v2/user/changeImage?fields=userId,image,guid,fullGuid,extension',
                data: { userId: userId },
                file: file
            })
                .progress(progressCallback)
                .success(function(result) {
                    successCallback(identityConverter.convert(result, '@fsid'), result);
                })
                .error(errorCallback);
        },
        changeFirstLastNameForUser: function(firstName, lastName, userId, successCallback, errorCallback) {
          $http({
            method: 'POST',
            data: $.param({ firstName: firstName, lastName: lastName , userId: userId}),
            url: apiUrl + '/api/v2/user/changeFirstLastNameForUser?fields=firstName,lastName,userId'
          })
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        },
        changeUserEmailForUser: function(emailData, successCallback, errorCallback){
          $http({
            method: 'POST',
            data: $.param(emailData),
            url: apiUrl + '/api/v2/user/changeEmailAddressForUser'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        deactivateMember: function(userId, successCallback, errorCallback){
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/user/deactivateUser?userId=' + userId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        activateMember: function(userId, successCallback, errorCallback){
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/user/activateUser?userId=' + userId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        changeDefaultTeam: function(teamId, successCallback, errorCallback){
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/user/changeDefaultTeam?teamId=' + teamId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        updateEmailNotifications: function(notifications, successCallback, errorCallback){
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/user/updateEmailNotifications?emailNotifications=' + notifications + '&fields=emailNotifications'
          })
            .success(successCallback)
            .error(errorCallback);
        }
    };
  });
