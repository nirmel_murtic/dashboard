'use strict';

angular.module('main')
  .service('projectService', function ($http, identityConverter, apiUrl, $upload, $rootScope, $filter, fsNotify) {
    var self = this;

    self.displaySiteTrackers = [{
      key: 'AD_CHANNELS',
      displayName: 'Ad Channels'
    },
      {
        key: 'ACCOMPLICE',
        displayName: 'Accomplice Tracking Script'
      }];

    self.getSiteTrackerByKey = function(key) {
      var result = null;

      angular.forEach(self.displaySiteTrackers, function(tracker) {
        if(tracker.key === key) {
          result = tracker;

          return true;
        }
      });

      return result ? result : { displayName: 'Unknown' };
    };

    self.updateGoals = function(project) {
      _.map(project.goals, function(goal){
          goal.formattedGoalName = $filter('fsGoalFmt')(goal);
          return goal;
      });
    };

    var _service= {
        projects:[],
        goalProperties: 'name,displayName,placementTypes,',
        projectProperties: 'name,description,goals,attachments,teamId,createdAt,',
        projectGoalProperties: 'page,pageId,pagePageId,image,twitterPromotableUser,twitterPromotableUserId,userId,screenName,twitterAccount,goal,goalId,pageName,appId,host,event,eventUrl,application,name,channelApplications,nativeApplications,app_name,app_store_url,platform,removable,appName,applicationId,type,removable,tracker,thirdPartyPixel,',
        imageProperties: 'name,fullGuid,width,height,size,extension,',
        getCurrentGoals: function () {
          // Returns goals of current project
          return _service.getCurrentProject().goals;
        },
        getCurrentProject: function () {
          // Returns current project
          return _service.getProject($rootScope.fractalAccountInfo.currentProject.id);
        },
        getProjectGoals: function (projectId) {
          // Returns goals of project with id equal to projectId; must be number
          return _service.getProject(projectId).goals;
        },
        getGoals: function (successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/project/getGoals?fields=' + this.goalProperties,
            cache: true
          })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
        },
        addOrUpdateProject: function(project, successCallback, errorCallback) {
          var data = {
            id: project.id,
            name: project.name,
            description: project.description,
            teamId: project.teamId,
            attachments: [],
            goals: []
          };

          for(var i = 0; i<project.attachments.length; i++) {
            data.attachments.push({
              id: project.attachments[i].id
            });
          }

          for(i = 0; i<project.goals.length; i++) {
              data.goals.push({
                id: project.goals[i].id,
                twitterPromotableUserId: project.goals[i].twitterPromotableUserId,
                goalId: project.goals[i].goalId,
                pageId: project.goals[i].pageId,
                host: project.goals[i].host,
                event: project.goals[i].event,
                thirdPartyPixel: project.goals[i].thirdPartyPixel,
                eventUrl: project.goals[i].eventUrl,
                tracker: project.goals[i].tracker,
                applicationId: project.goals[i].applicationId
              });
          }

          return $http({
            method: 'POST',
            url: apiUrl + '/api/v2/project/createProject?fields=' + this.projectProperties + this.projectGoalProperties + this.goalProperties + this.imageProperties,
            headers: {
              'Content-Type': 'application/json'
            },
            data: data
          })
            .success(function(result) {
              var data = identityConverter.convert(result, '@fsid');
              fsNotify.push(
                $filter('translate')('label.align.UpdatedProject') + ' ' + project.name
              );
              self.updateGoals(data.data.project);

              successCallback(data, result);
            })
            .error(errorCallback);
        },
        getProjects: function(teamId, successCallback, errorCallback) {
          // Initial call for all projects by team. Used in Align.
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/project/getProjects?teamId=' + teamId + '&joins=team.fractalAccount,goals.goal;application;page.socialAccount.social,attachments&fields=' + this.projectProperties + this.projectGoalProperties + this.goalProperties + this.imageProperties
          })
            .success(function(result) {
              var data = identityConverter.convert(result, '@fsid');

              angular.forEach(data.data.projects, function(project) {
                self.updateGoals(project);
              });

              successCallback(data, result);

              return data;
            })
            .error(errorCallback);
        },
        getProject: function(projectId) {
          // This is used to get a specific project with a given ID
          // IMPORTANT-projectId must be an integer, not a string.
          var rtn=null;
          for(var o=0;o<this.projects.length; o++){
            if(this.projects[o].id===projectId){
              rtn=this.projects[o];
              break;
            }
          }
          return rtn;
        },
        uploadAttachment: function(teamId, file, type, successCallback, errorCallback, progressCallback) {
          $upload.upload({
            url: apiUrl + '/api/v2/image/upload?fields=name,size,fullGuid,extension',
            data: { teamId: teamId, type: type },
            file: file
          })
            .progress(progressCallback)
            .success(function(result) {
              successCallback(identityConverter.convert(result, '@fsid'), result);
            })
            .error(errorCallback);
        }
    };
    return _service;
  });
