'use strict';

angular.module('main')
  .factory('emailTemplatesService', function ($http, identityConverter, apiUrl) {
    return {
      getTemplates: function (language, successCallback, errorCallback) {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/support/emailTemplates?fields=id,text,html,language,subject,type,params&language=' + language
        })
          .success(successCallback)
          .error(errorCallback);
      },
      updateTemplate: function (data, successCallback, errorCallback) {
        return $http({
          method: 'POST',
          data: data,
          headers: {
            'Content-Type': 'application/json'
          },
          url: apiUrl + '/api/v2/support/update?fields=id,text,html,language,subject,type,params'
        })
          .success(successCallback)
          .error(errorCallback);
      }
    };
  });
