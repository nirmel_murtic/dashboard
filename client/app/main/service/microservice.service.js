'use strict';

angular.module('main')
  .factory('microService', function ($http, $q) {
    return {
        urlValidation: function(config, successCallback, errorCallback) {

          // We are going to use the user's 'dashboard' auth to access the webservice.
          // ENV is used to tell the backend which JWT decode key to decode the auth token with.
          // Yeah, that's kinda underhanded. Just roll with it.
          var env = window.configuration.environment;
          var auth = localStorage.getItem('auth_headers');
          auth = JSON.parse(auth) || false;
          var wsConf = {
            headers: {
              'Authorization': auth.Authorization,
              'x-environment': env || 'UNDEF'
            },
            ignoreLoadingBar: true
          };

          // /validation/ is probably going to change once the webservice has it's own server...
          var escTestUrl = encodeURIComponent(config.testUrl);
          var wsCall = 'https:'+window.configuration.wsApiUrl+'/validation/' + escTestUrl + (config.badurlonly?'?badurlonly=true':'');

          //console.log('not running:',config.testUrl);
          //errorCallback(); return;

          return $http.get(wsCall,wsConf)
            .success( function (response) { successCallback(response); })
            .error( function(response) { errorCallback(response); });


        },
        bulkUrlValidation: function (urls, successCallback, errorCallback) {
          // var urlsWithIndices = _.zip(urls, _.range(urls.length));
          // debugger;
          // var urlsMap = _.groupBy(urlsWithIndices, function(a) {
          //   return a[0];
          // });
          //same as
          // var urlsMap = _.invert(urlsWithIndices, true);
          var urlsMap = _(urls)
            .groupBy(function(val, index){return index;})
            .invert(true)
            .value();


          var promises = _(urlsMap)
             .map(function(v, key){ return key; })
             .map(function(url) {
                var config = {
                  host: 'na',
                  testUrl: url,
                  index: 700, //pagePost
                  nowait: true,
                  nocache: true,
                  badurlonly: false
                };
                return this.urlValidation(config, angular.noop, angular.noop)
                  .then(function(res) {
                    return {

                      url : url,
                      result: successCallback(res.data)
                    };
                  }, function(res) {
                    return errorCallback(res.data, res.status);
                  });
             }, this)
             .value();
          return $q.all(promises).then(function(validationResults) {
            var a = [];
            _.each(validationResults, function(r) {
              var url = r.url;
              var indices = urlsMap[url];
              _.each(indices, function(i) {
                a[i] = r.result;
              });
            });
            return a;
          });
        },
        getValidationMsg: function (key) {
          var dictionary = {
            'bad-url':             'The site could not be reached, check the URL and try again. [FAILED]',
            'Timeout':             'The site could not be validated in the allowed time. You can try it again. [TIMEOUT]',
            'timeout':             'The site could not be validated in the allowed time. You can try it again. [TIMEOUT]',
            'no-script-found':     'The site does not appear to have a tracking script installed. You can try it again. [NO-SCRIPT]',
            'tracking-injection':  'The tracking script injector has been found.',
            'accomplice-script':   'The tracking script was installed.',
            'team-script':         'The correct tracking script for your team has been found.',
            'tracking-post':       'The tracking script sent data back.',
            'no-service':          'The validation service is not running. Try this again later.'
          };

          if (key && dictionary[key]) { return dictionary[key]; }
          else { return ''; }
        },
        validationApiCache: {
          trove: {},
          now: function () { var ts=Date.now(); return (ts/1000); },
          ttl: function (minutes) { return this.now() + (60*minutes);  },
          check: function (key) {
            if (this.trove[key]!==undefined) {
              var hit = this.trove[key];
              if ( hit.ttl > this.now() ) {
                /*console.log('apiCache HIT:',hit.data.testUrl);*/
                return hit.data;
              } else { /*console.log('apiCache EXPIRED');*/ return false; }
            } else { /*console.log('apiCache MISS');*/ return false; }
          },
          set: function(key,data) {
            this.trove[key] = {};
            this.trove[key].data = data;
            this.trove[key].ttl = this.ttl(5); // minutes;
            return true;
          }
        }

    };
  });
