'use strict';

angular.module('main')
  .service('PaginatedArray', function (Scroll, $rootScope) {
    function PaginatedArray(array, config) {
      this.array = array;
      this.currentPage = 1;
      this.numPerPage = config.numPerPage;
      this.pages = [1];
    }

    PaginatedArray.prototype.goToPage = function(page){
      this.currentPage = page; 
      this.paginate(this.array.length, this.array.length);
    };

    PaginatedArray.prototype.addPage = function(num){
      this.pages.push(num);
    };

    PaginatedArray.prototype.removePage = function(){
      this.pages.splice(-1, 1);
    };

    PaginatedArray.prototype.loadPages = function(newValue){
      for(var i = 2; i <= Math.ceil(newValue / this.numPerPage); i++){
        this.addPage(i);
      }
    };

    PaginatedArray.prototype.paginate = function(newValue, oldValue){
      var self = this;
      if(newValue >= this.numPerPage + 1 && !this.initPagination){
        this.initPagination = true;
        this.loadPages(newValue);
      }

      if(newValue % ((this.numPerPage * this.pages.length) + 1) === 0 && newValue > oldValue){
        this.addPage(this.pages.length + 1);
      } else if(newValue % (this.numPerPage * (this.pages.length - 1)) === 0 && newValue < oldValue){
        this.removePage();
        this.currentPage--;
      }

      // We compare oldValue with newValue in order to determine if a combo was added or deleted. If a combo is added, we want to go back to the first page if not there already.
      // If a combo is deleted, we do not shift pages
      if (newValue > oldValue) {
        if (this.currentPage !== 1) {
          this.goToPage(1);
          Scroll.top().then( function() {
            // Notify the campaign creation controller that a change occured. The animation will happen there
            $rootScope.$emit('paginateArrayChange');
          });
        } else if (document.body.scrollTop <= 500) {
          // When the user is more or less at the top of the page, no need to scroll up
          $rootScope.$emit('paginateArrayChange');
        } else {
          Scroll.top().then( function() {
            $rootScope.$emit('paginateArrayChange');
          });
        }
      }

      this.currentList = _.filter(this.array, function(item, i){
        // i = i + 1;
        if(i >= ((self.currentPage - 1) * self.numPerPage) &&
          i < ((self.currentPage) * self.numPerPage)){
          return true;
        }
        return false;
      });
    };


    return PaginatedArray;

  });
