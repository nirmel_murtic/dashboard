'use strict';

describe('Service: billingService', function () {

  // load the service's module
  beforeEach(module('main'));

  // instantiate service
  var billingService;
  beforeEach(inject(function (_billingService_) {
    billingService = _billingService_;
  }));

  it('should do something', function () {
    expect(!!billingService).toBe(true);
  });

});
