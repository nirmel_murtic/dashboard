'use strict';

angular.module('main')
  .factory('engage', function ($http, identityConverter, apiUrl) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    return {
      deleteScheduledPost: function(postId, successCallback, errorCallback){
        $http({
          method: 'DELETE',
          url: apiUrl + '/api/v2/engage/post/' + postId
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      likePost: function(postId, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/post/' + postId + '/like'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      unlikePost: function(postId, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/post/' + postId + '/unlike'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      likeComment: function(postId, commentId, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/post/' + postId + '/comments/' + commentId + '/like'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      unlikeComment: function(commentId, postId, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/post/' + postId + '/comments/' + commentId + '/unlike'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      commentOnPost: function(comment, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/comment?comment=' + JSON.stringify(comment)
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      retweetTweet: function(pageId, tweetId, userPageId, text, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/twitter/tweet/' + pageId + '/' + tweetId + '/retweet?myPageId=' + userPageId
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      replyToTweet: function(pageId, tweetId, userPageId, text, successCallback, errorCallback){

        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/twitter/tweet/' +  pageId  + '/' + tweetId + '/reply?myPageId=' +  userPageId + '&text=' + text
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      favoriteTweet: function(pageId, tweetId, userPageId, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/twitter/tweet/' + pageId + '/' + tweetId + '/fav?myPageId=' + userPageId
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      unfavoriteTweet: function(pageId, tweetId, userPageId, successCallback, errorCallback){
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/engage/twitter/tweet/' + pageId + '/' + tweetId + '/unfav?myPageId=' + userPageId
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },


      getPostImage: function(postId, successCallback, errorCallback){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v1/engage/post/' + postId + '/image'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      getAllPostsTwitter: function(pageName, successCallback, errorCallback){
        $http({
          url: apiUrl + '/api/v1/monitor/twitter/posts?pageIds=' + pageName
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      getAllPostsFacebook: function(pageId, successCallback, errorCallback){                
        $http({
          method: 'GET',
          url: apiUrl + '/api/v1/monitor/posts?pageIds=' + pageId
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      getPost: function(postId, successCallback, errorCallback){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/engage/post/' + postId + '?fields=message,image,url,thumbnail,headline,description,channel,postId,id'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      },

      getAllScheduledPosts: function(successCallback, errorCallback){
         $http({
          method: 'GET',
          url: apiUrl + '/api/v2/engage/posts?scheduled=true&fields=postId,scheduledTime,userId,message,pageId,day,page'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
        
      },

      getAllSocialPages: function(successCallback, errorCallback){
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/social/list?fields=accountId,pageId,pagePageId,groupId,groupName,pageName,name,picture,social,image'
        })
        .success(function(result) {
          successCallback(identityConverter.convert(result, '@fsid'), result);
        })
        .error(errorCallback);
      }
    };
  });
