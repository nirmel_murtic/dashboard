'use strict';

angular.module('main')
  .factory('accountService', function ($http, identityConverter, apiUrl) {
    return {
        search: function (q, successCallback, errorCallback) {
            var data = {
              q: q
            };

            $http({
                method: 'GET',
                url: apiUrl + '/api/v2/account/search?fields=id,name',
                data: $.param(data)
            })
            .success(successCallback)
            .error(errorCallback);
        }
    };
  });
