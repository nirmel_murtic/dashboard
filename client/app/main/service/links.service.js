'use strict';

angular.module('main')
  .factory('links', function (ngDialog, apiUrl, $http, identityConverter) {
    return {
      openCreateLink: function (parentScope) {
        ngDialog.open({
          template: 'app/main/act/createLinks/createLinks.html',
          controller: 'CreatelinksCtrl',
          className: 'modal modal-links-new',
          scope: parentScope
        });
      },
      getGroupURLs: function (linksGroup, successCallback, errorCallback) {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/get?groupId=' + linksGroup
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      getMultiURLs: function (linksGroup, successCallback, errorCallback) {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/get/urls?groupId=' + linksGroup
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      getProjectURLs: function (projectId, fromTime, toTime, successCallback, errorCallback) {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/get/list?projectId=' + projectId + '&fromTime=' + fromTime + '&toTime=' + toTime
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      createLink: function(linkData, linkCounter, successCallback, errorCallback) {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/create?data=' + JSON.stringify(linkData) +'&count=' + linkCounter
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      validateLink: function (originalUrl, successCallback, errorCallback) {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/verify/url?originalUrl=' + encodeURIComponent(originalUrl)
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      updateLink: function(linkData, successCallback, errorCallback) {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/update?data=' + JSON.stringify(linkData)
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      updateMultiChannelUrls: function(linkData, channelType, paid, successCallback, errorCallback) {
        var linkIds = [];

        angular.forEach(linkData, function(value) {
          if(value.id) {
            linkIds.push(value.id);
          }
        });

        var data = {
          channelType: channelType,
          isPaid: paid,
          urlIds: linkIds.join(',')
        };
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/links/update/url/channel',
          data: $.param(data)
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      updateMultiChannelUrl: function(linkData, successCallback, errorCallback) {
        $http({
          method: 'POST',
          url: apiUrl + '/api/v2/links/update/url/channel',
          data: $.param({ channelType: linkData.channelType, isPaid: linkData.paid, urlIds: linkData.id, channelText: linkData.channelText })
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      addLongUrls: function(linkData, groupId, numberUrls, successCallback, errorCallback) {
        $http({
          method: 'GET',
          url: apiUrl + '/api/v2/links/add/url/long?template=' + JSON.stringify(linkData) + '&groupId=' + groupId + '&count=' + numberUrls
        })
          .success(function(result) {
            successCallback(identityConverter.convert(result, '@fsid'), result);
          })
          .error(errorCallback);
      },
      deactivateUrlGroup: function(groupId, successCallback, errorCallback) {
        $http({
          method: 'DELETE',
          url: apiUrl + '/api/v2/links/deactivate?groupId=' + groupId
        })
          .success(successCallback)
          .error(errorCallback);
      },
      deactivateUrlMultiChannel: function(urlId, successCallback, errorCallback) {
        $http({
          method: 'DELETE',
          url: apiUrl + '/api/v2/links/deactivate/url?urlId=' + urlId
        })
          .success(successCallback)
          .error(errorCallback);
      }
    };
  });
