'use strict';

describe('Service: pixelService', function () {

  // load the service's module
  beforeEach(module('main'));

  // instantiate service
  var pixelService;
  beforeEach(inject(function (_pixelService_) {
    pixelService = _pixelService_;
  }));

  it('should do something', function () {
    expect(!!pixelService).toBe(true);
  });

});
