'use strict';

angular.module('main')
  .factory('billingService', function ($http, identityConverter, apiUrl, $filter) {
    return {
        updateAccount: function (account, password, successCallback, errorCallback) {
            var data = {
              firstName: account.firstName,
              lastName: account.lastName,
              companyName: account.companyName,
              emailAddress: account.emailAddress,
              phone: account.phone,
              address1: account.address1,
              address2: account.address2,
              city: account.city,
              state: account.state,
              zip: account.zip,
              password: password
            };

            if(account.billing.token) {
              data.billingToken = account.billing.token;
            }

            $http({
                method: 'POST',
                url: apiUrl + '/api/v2/billing/updateAccount?fields=id,firstName,lastName,address1,address2,phone,zip,city,state,emailAddress,companyName,cardFirstName,cardLastName,cardMaskedNumber,cardExpirationMonth,cardExpirationYear,cardType,planName,planCode,endsAt,amount,trackedEventsLimit,adSpentLimit,subscriptionPlan,trackedEvents,adSpent',
                data: $.param(data)
            })
            .success(successCallback)
            .error(errorCallback);
        },
        getAccount: function (successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/billing/getAccount?fields=id,firstName,lastName,address1,address2,phone,zip,city,state,emailAddress,companyName,cardFirstName,cardLastName,cardMaskedNumber,cardExpirationMonth,cardExpirationYear,cardType,planName,planCode,endsAt,amount,trackedEventsLimit,adSpentLimit,subscriptionPlan,trackedEvents,adSpent'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        removeBillingInfo: function (successCallback, errorCallback) {
          return $http({
            method: 'DELETE',
            url: apiUrl + '/api/v2/billing/removeBillingInfo'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        getPlans: function (successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/billing/getPlans?fields=id,planName,planCode,enabled,description,amount,trackedEventsLimit,adSpentLimit'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        syncPlans: function (successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/billing/syncPlans?fields=id,planName,planCode,enabled,description,amount,trackedEventsLimit,adSpentLimit'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        getAllPlans: function (successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/api/v2/billing/getAllPlans?fields=id,planName,planCode,enabled,description,amount,trackedEventsLimit,adSpentLimit'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        updatePlans: function (data, successCallback, errorCallback) {
          return $http({
            method: 'POST',
            data: data,
            headers: {
              'Content-Type': 'application/json'
            },
            url: apiUrl + '/api/v2/billing/updatePlans'
          })
            .success(successCallback)
            .error(errorCallback);
        },
        subscribeToPlan: function (planCode, subscribeOnRenewal, successCallback, errorCallback) {
          $http({
            method: 'POST',
            url: apiUrl + '/api/v2/billing/subscribe',
            data: $.param({ planCode: planCode, subscribeOnRenewal: subscribeOnRenewal})
          })
            .success(successCallback)
            .error(errorCallback);
        },
        getReport: function (startTime, endTime, successCallback, errorCallback) {
          startTime = $filter('date')(startTime, 'yyyy-MM-dd');
          endTime = $filter('date')(endTime, 'yyyy-MM-dd');

          $http({
            method: 'GET',
            url: apiUrl + '/api/v2/campaign/stats/get/amountspent?startTime=' + startTime + '&endTime=' + endTime
          })
            .success(successCallback)
            .error(errorCallback);
        },
        getReportForAccount: function (startTime, endTime, accountId, successCallback, errorCallback) {
          startTime = $filter('date')(startTime, 'yyyy-MM-dd');
          endTime = $filter('date')(endTime, 'yyyy-MM-dd');

          $http({
            method: 'GET',
            url: apiUrl + '/api/v2/campaign/stats/get/account/' + accountId + '/amountspent?startTime=' + startTime + '&endTime=' + endTime
          })
            .success(successCallback)
            .error(errorCallback);
      }

    };
  });
