'use strict';

angular.module('main')
  .factory('pixelService', function ($http, identityConverter, apiUrl, fsUtils) {
    var _service = {
        getPixels: function (teamId, successCallback, errorCallback) {
            return $http({
                method: 'GET',
                url: apiUrl + '/pixelmapgen/api/v2/host/get/all?teamId=' + teamId
            })
            .success(successCallback)
            .error(errorCallback);
        },
        assignHost: function(teamId, host, successCallback, errorCallback) {
          $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/host/assign?teamId=' + teamId + '&host=' + host
          })
            .success(successCallback)
            .error(errorCallback);
        },
        registerHost: function(teamId, host, urlName, successCallback, errorCallback) {
          if(!host) {
            return;
          }

            $http({
              method: 'GET',
              url: apiUrl + '/pixelmapgen/api/v2/host/register?teamId=' + teamId + '&urlName=' + urlName + '&host=' + host
            })
              .success(successCallback)
              .error(errorCallback);
        },
        saveEvent: function(selector, host, eventName, teamId, successCallback, errorCallback) {
          $http({
            method: 'POST',
            url: apiUrl + '/pixelmapgen/api/v2/mapper/store?teamId=' + teamId,
            data: {
              'mapping':{
                'event_model':{
                  'event_name':encodeURIComponent(eventName),
                  'host':host,
                  'event':'url',
                  'selector':encodeURIComponent(selector)
                }
              }
            }

          })
            .success(successCallback)
            .error(errorCallback);
        },
        getEvents: function(host, teamId, successCallback, errorCallback) {
          $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/mapper/view?host=' + host + '&teamId=' + teamId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        generateTrackerScript: function(host, successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/generate/generate?host=' + host
          })
            .success(successCallback)
            .error(errorCallback);
        },

        createConversionPixel: function(teamId, propertyId, conversionPixelName, successCallback, errorCallback){
          var params = {
            teamId: teamId,
            propertyId: propertyId,
            eventType: 'pconv',
            eventName: conversionPixelName
          };

          $http({
            method: 'POST',
            url: apiUrl + '/pixelmapgen/api/v2/mapper/store/simple',
            data: $.param(params)
          })
            .success(successCallback)
            .error(errorCallback);
        },

        generateConversionScript: function(propertyId, eventDefinitionId, successCallback, errorCallback) {
          return $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/generate/for/event?propertyId=' + propertyId + '&eventDefinitionId=' + eventDefinitionId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        hasUpdates: function(host, teamId, successCallback, errorCallback) {
          $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/mapper/view/hasupdates?host=' + host + '&teamId=' + teamId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        deactivateEvent: function(event, host, teamId, successCallback, errorCallback) {
          $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/mapper/deactivate?event=' + event + '&host=' + host + '&teamId=' + teamId
          })
            .success(successCallback)
            .error(errorCallback);
        },
        deactivateHost: function(teamId, host, successCallback, errorCallback) {
          $http({
            method: 'GET',
            url: apiUrl + '/pixelmapgen/api/v2/host/deactivate?teamId=' + teamId + '&host=' + host
          })
            .success(successCallback)
            .error(errorCallback);
        }
    };

    _service.getPixelsAsync = fsUtils.promisify(_service.getPixels, _service, function(result) {
        return result.data;
    });

    return _service;

  });
