'use strict';

angular.module('main')
  .controller('NavbarCtrl', function ($scope, $location, apiUrl, teamService, ngDialog, $filter, CreatePost, fsConfirm, $auth, library, $rootScope, userService, $timeout, links, $window, $state) {

    $scope.userMenuOpen = false;
    $scope.teamMenuOpen = false;

    $scope.apiUrl = apiUrl;

    $scope.checkPermissions = $auth.checkPermissions;

    $scope.fractalAccount = $rootScope.fractalAccountInfo.fractalAccount;

    $scope.openNavSection = function(link){
      $location.path(link);
    };

    $scope.openNavPill = function(event, link){
      //make sure pill click doesnt fire twice
      if (event.stopPropagation) {
        event.stopPropagation();
      }
      //just in case browser doesnt detect stopPropagation
      if (event.preventDefault) {
        event.preventDefault();
      }
      try{
        event.cancelBubble = true;
        event.returnValue = false;
      }catch(oops){
      }
      $location.path(link);
    };

    $scope.toggleUserMenu = function(event){
      if(event.target.classList.contains('nav-pill')){
        return;
      } else{
        $scope.userMenuOpen = !$scope.userMenuOpen;
      }
    };


    $scope.toggleTeamMenu = function(event){
      if(event.target.classList.contains('nav-pill')){
        return;
      } else {
        if($scope.switchMenuOpen) {
          $scope.switchMenuOpen = !$scope.switchMenuOpen;
          $scope.teamMenuOpen = true;
        } else {
          $scope.teamMenuOpen = !$scope.teamMenuOpen;
          if($scope.teamMenuOpen) {
            $scope.toggleTeamMenuStyle = 10.8;
          } else {
            $scope.toggleTeamMenuStyle = 6.5;
          }
        }
      }
    };

    $scope.clickToOpenCampaignWorkflow = function(){
      var shouldReload = $state.includes('main.act.campaignCreation') || $state.is('main.act.campaignCreation');
      $state.go('main.act.campaignCreation', {id: null, type:null}, {reload:shouldReload});
    };

    $scope.clickToOpenLinkWorkflow = function(){
      links.openCreateLink($scope);
    };

    $scope.clickToOpenPostWorkflow = function() {
        CreatePost.openPostModal($scope);
    };

    $scope.isActive = function(route) {
      $scope.$state = $state;

      var currentRoute = $state.current.data.active || 'dashboard';

      if(currentRoute.indexOf('/') > -1){
        currentRoute = currentRoute.substring(0, currentRoute.indexOf('/'));
      }

      return route === currentRoute ? 'active' : '';
    };
    $scope.myUserSettings = function() {
        ngDialog.open({
            template: 'app/main/navbar/myUserSettings/myUserSettings.html',
            controller: 'MyusersettingsCtrl',
            className: 'modal modal-members account-settings-modal'
        });
    };

    $scope.setDefaultTeam = function(team) {

      if($scope.fractalAccountInfo.currentTeam.id === team.id){
        return;
      }

      userService.changeDefaultTeam(team.id, function(result) {
        if(result.data.success) {
          // Reset any global URL Validation flags
          $rootScope.somethingIsInvalid = false;

          $state.go('main.align.projects');
          $scope.fadeLoggedTeam();
          $rootScope.fractalAccountInfo.switchDefaultAndCurrentTeam(team.id);
        }
      }, function(error) {
        console.log(error.error.message);
      });

    };

    //Publisher Methods for library
    $scope.loadImageFromLibrary = function(post){
        // Close Manage Audience Modal view
        ngDialog.close();
        $scope.post = post;
        // Open Library - Load audience modal view
        $scope.clickToOpenLibrary('Images');
    };

    $scope.openWalkMe = function() {
        if(angular.element('#walkme-menu').css('display') === 'none') {
          $timeout(function() {
            angular.element('#walkme-player').trigger('click');
          });
        }
    };

//    $scope.closeLibraryModal = function(image){
//      $scope.post.image = image;
//      CreatePost.openPostModal($scope);
//    };

    var window = $window;

    $scope.showTeamsDropdown = false;

    $scope.toggleMenu = function(event) {
      for(var i=0; i<event.target.classList.length; i++) {
        if(event.target.classList[i] === 'add-team-pill' || event.target.classList[i] === 'fa-times' ||
          event.target.classList[i] === 'input-control-add-team' || event.target.classList[i] === 'save-team-block' ||
          event.target.classList[i] === 'fa-check' || event.target.classList[i] === 'team-button' || event.target.classList[i] === 'label-add-team') {
          return;
        }
      }
      $scope.showInputsNewTeam = false;
      $scope.newTeamName = null;
      $scope.showTeamsDropdown = !($scope.showTeamsDropdown);
      event.stopPropagation();
    };

    window.onclick = function(event) {
      for(var i=0; i<event.target.classList.length; i++) {
        if(event.target.classList[i] === 'add-team-pill' || event.target.classList[i] === 'fa-times' ||
          event.target.classList[i] === 'input-control-add-team' || event.target.classList[i] === 'save-team-block' ||
          event.target.classList[i] === 'fa-check' || event.target.classList[i] === 'team-button' || event.target.classList[i] === 'label-add-team') {
          return;
        }
      }
      if ($scope.showTeamsDropdown) {
        $scope.showInputsNewTeam = false;
        $scope.newTeamName = null;
        $scope.showTeamsDropdown = false;
        $scope.$apply();
      }
    };

    $scope.cancelAddAction = function() {
      $scope.showInputsNewTeam = false;
      $scope.newTeamName = null;
    };

    $scope.teamNameError = false;

    $scope.createNewTeam = function(newName) {

      if(!$auth.checkPermissions('<MANAGE_TEAMS>')) {
        fsConfirm('genericAlert', {
          body: $filter('translate')('label.common.YouhaveViewOnlyAccessforthisFeature')
        });
        return;
      }

      $scope.processingCreation = true;

      var newTeamName = newName;

      if(!newTeamName) {
        $timeout(function() {
          $scope.teamNameError = true;
          $timeout(function() {
            $scope.teamNameError = false;
          }, 3000);
        }, 0);
        delete $scope.processingCreation;
        return;
      }

      if(newTeamName.trim().length > 16){

        fsConfirm('genericAlert', {
          body: $filter('translate')('label.navbar.TeamNameMax')
        });

        delete $scope.processingCreation;
        return false;
      } else if(newTeamName.trim().length < 3){

        fsConfirm('genericAlert', {
          body: $filter('translate')('label.navbar.TeamNameMin')
        });

        delete $scope.processingCreation;
        return false;
      } else {

        teamService.createTeam({
            teamName: newTeamName,
            fractalAccountId: $scope.fractalAccount.id
          },
          function (data) {

            $scope.showInputsNewTeam = false;
            $scope.newTeamName = null;
            $scope.showTeamsDropdown = false;

            newTeamName = '';

            if($rootScope.fractalAccountInfo.allTeams) {
              $rootScope.fractalAccountInfo.allTeams.push(data.data.team);
            }

            if($scope.fractalAccount.allTeams) {
              $scope.fractalAccount.allTeams.push(data.data.team);
            }

            $rootScope.fractalAccountInfo.activeTeams.push(data.data.team);
            $scope.fractalAccount.activeTeams.push(data.data.team);

            userService.changeDefaultTeam(data.data.team.id, function(result) {
              delete $scope.processingCreation;

              if(result.data.success) {
                // Reset any global URL Validation flags
                $rootScope.somethingIsInvalid = false;

                if($state.current.name !== 'main.align.projects') {
                  $state.go('main.align.projects');
                } else {
                  $state.go('main.align');
                }

                $scope.fadeLoggedTeam();
                $rootScope.fractalAccountInfo.switchDefaultAndCurrentTeam(data.data.team.id);
              }

            }, function(error) {
              console.log(error.error.message);
              delete $scope.processingCreation;
            });
          }, function (error) {
            newTeamName = '';

            fsConfirm('genericAlert', {
              body: error.error.message
            });

            $scope.newTeamName = null;
            delete $scope.processingCreation;
          }
        );
      }
    };

    $scope.openSupportDashboard = function() {
      $window.open('//support.accomplice.io/hc/en-us');
    };

  });
