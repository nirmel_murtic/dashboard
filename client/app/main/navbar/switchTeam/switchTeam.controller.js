'use strict';

angular.module('main')
  .controller('SwitchteamCtrl', function ($scope, $rootScope, ngDialog, $timeout, apiUrl, $state) {
    $scope.apiUrl = apiUrl;

    $scope.fractalAccount = $rootScope.fractalAccountInfo.fractalAccount;
    $scope.currentTeam = $rootScope.fractalAccountInfo.currentTeam;
    $scope.activeTeams = $scope.fractalAccount.allTeams ? $scope.fractalAccount.allTeams : $scope.fractalAccount.activeTeams;

    $scope.switchToTeam = function(team) {
      if($scope.fractalAccountInfo.currentTeam.id === team.id){
        return;
      }

      $rootScope.fractalAccountInfo.switchCurrentTeam(team.id);

      $state.go('main.align');

      $scope.currentTeam = team;
      $rootScope.teamSwitchProccess = true;
      $scope.fadeLoggedTeam();
      $timeout(function () {
        ngDialog.close();
      }, 500);
    };
  });
