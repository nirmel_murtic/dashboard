'use strict';

describe('Controller: SwitchteamCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));

  var scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
