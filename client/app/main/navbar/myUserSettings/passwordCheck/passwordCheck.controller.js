'use strict';

angular.module('main')
  .controller('PasswordCheckCtrl', function ($scope, $timeout) {
    $scope.passwordCheck = function() {
      $scope.closeThisDialog();

      $scope.continueEmailChange($scope.password);
    };

    $scope.focusPasswordInput = false;

    $timeout(function() {
      $scope.focusPasswordInput = true;
    }, 100);
  });
