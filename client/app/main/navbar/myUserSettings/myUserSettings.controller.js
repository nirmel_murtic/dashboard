'use strict';

angular.module('main')
  .controller('MyusersettingsCtrl', function ($scope, $rootScope, userService, ngDialog, $auth, $state, apiUrl, $q, $timeout, fsConfirm, $filter) {
    if(!$rootScope.fractalAccountInfo.lastName){
        $rootScope.fractalAccountInfo.lastName = '';
    }

    $scope.firstName = $rootScope.fractalAccountInfo.firstName;
    $scope.lastName = $rootScope.fractalAccountInfo.lastName;
    $scope.myUserEmail = $rootScope.fractalAccountInfo.username;
    $scope.myUserTeams = $rootScope.accountTeams;
    $scope.myUserRole = $rootScope.fractalAccountInfo.userRole.name;
    $scope.image = $rootScope.fractalAccountInfo.image;
    $scope.apiUrl = apiUrl;

    $scope.token = $auth.user.access_token; // jshint ignore:line

    String.prototype.trimRight = function(){
        return this.replace(/\s+$/,'');
    };

    $scope.$watchCollection('[firstName, lastName]', function(newValue, oldValue) {
        if (newValue === oldValue) {
            return false;
        } else {
            var namesInfo = newValue;
            var firstName = namesInfo[0];
            var lastName = namesInfo[1];
            if(lastName){
                lastName = lastName.trimRight();
            }
            if(firstName && lastName){
                userService.changeFirstLastName(firstName,lastName,
                    function(data) {

                      $timeout(function() {
                        $scope.myUserNotification = {
                          message: $filter('translate')('label.myUserSettings.Yournamehasbeensaved'),
                          status: 'show'
                        };

                        $timeout(function() {
                          $rootScope.fractalAccountInfo.firstName = data.data.user.firstName;
                          $rootScope.fractalAccountInfo.lastName = data.data.user.lastName;
                          $scope.myUserNotification.status = 'hide';
                          delete $scope.myUserNotification.message;
                        }, 2000);
                      }, 0);

                    }, function(error) {

                        fsConfirm('genericAlert', {
                          body: error.error.message
                        });
                        console.log(error.error.message);
                    }
                );
            }
        }
    }, true);

    $scope.$watch('myUserEmail', function(newValue, oldValue) {
        if (newValue === oldValue) {
            return false;
        } else if($scope.defaultEmail !== newValue){
            $scope.changeEmailData = {
                newEmailAddress: newValue,
                confirmEmailAddress: newValue
            };

            $scope.defaultEmail = oldValue;

            ngDialog.open({
              template: 'app/main/navbar/myUserSettings/passwordCheck/passwordCheck.html',
              controller: 'PasswordCheckCtrl',
              className: 'modal modal-members check-password-modal',
              id: 'password-check-dialog',
              scope: $scope
            }).closePromise.then(function(data) {
              if (data.value === '$closeButton') {
                $scope.myUserEmail = $scope.defaultEmail;
              }
            });

        }
    }, true);

    $scope.continueEmailChange = function(password) {
      $scope.changeEmailData.password = password;
      userService.changeUserEmail($scope.changeEmailData,
        function(){
          $timeout(function() {
            $scope.myUserNotification = {
              message: $filter('translate')('label.myUserSettings.Youremailhasbeensaved'),
              status: 'show'
            };

            $timeout(function() {
              $scope.myUserNotification.status = 'hide';
              delete $scope.myUserNotification.message;
              ngDialog.close();
              $auth.signOut();
              $state.go('anonymous.login');
            }, 2000);
          }, 0);
        }, function(error){
          $scope.myUserEmail = $scope.defaultEmail;
          fsConfirm('genericAlert', {
            body: error.error.message
          });
          console.log(error.error.message);
        }
      );
    };

    $scope.PasswordSuccess = false;

    $scope.changeUserSettingsForm = function(){

        var passwordData = {
          currentPassword: $scope.currentPassword,
          newPassword: $scope.newPassword,
          confirmPassword: $scope.confirmPassword
        };

        if(passwordData.currentPassword && passwordData.newPassword && passwordData.confirmPassword){
            userService.changeUserPassword(passwordData,
                function() {
                  $timeout(function() {
                    $scope.myUserNotification = {
                      message: $filter('translate')('label.myUserSettings.Yourpasswordhasbeensaved'),
                      status: 'show'
                    };
                    $scope.currentPassword = '';
                    $scope.newPassword = '';
                    $scope.confirmPassword = '';
                    $timeout(function() {
                      $scope.myUserNotification.status = 'hide';
                      delete $scope.myUserNotification.message;
                    }, 2000);
                  }, 0);

                }, function(error) {
                    fsConfirm('genericAlert', {
                      body: error.error.message
                    });

                    console.log(error.error.message);
                    $scope.currentPassword = '';
                    $scope.newPassword = '';
                    $scope.confirmPassword = '';
                }
            );
        }
    };

    $scope.MPSuccess = false;

    $scope.changeMasterPassword = function(){
      var masterPasswordData = {
        newPassword: $scope.newMasterPassword,
        confirmPassword: $scope.confirmMasterPassword
      };

      if(masterPasswordData.newPassword && masterPasswordData.confirmPassword){
        userService.changeMasterPassword(masterPasswordData,
          function() {

            $timeout(function() {
              $scope.myUserNotification = {
                message: $filter('translate')('label.myUserSettings.Yourmasterpasswordhasbeensaved'),
                status: 'show'
              };
              $timeout(function() {
                $scope.newMasterPassword = '';
                $scope.confirmMasterPassword = '';
                $scope.myUserNotification.status = 'hide';
                delete $scope.myUserNotification.message;
              }, 2000);
            }, 0);
          }, function(error) {
            fsConfirm('genericAlert', {
              body: error.error.message
            });

            console.log(error.error.message);
            $scope.newMasterPassword = '';
            $scope.confirmMasterPassword = '';
          }
        );
      }
    };

    $scope.onFileSelect = function($files) {
        if($files && $files.length) {
            var file = $files[0];

            userService.changeUserImage($scope.fractalAccountInfo.userId, file, function(data) {
                $rootScope.fractalAccountInfo.image = data.data.user.image;
            },
            function(error) {
                console.log(error.error.message);
            },
            function(evt) {
                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            });
        }
    };

    $scope.refreshNotifications = function() {
      $scope.notifications = {
        'green': false,
        'orange': false,
        'red': false
      };

      angular.forEach($rootScope.fractalAccountInfo.emailNotifications, function (notification) {
        if (notification === '1') {
          $scope.notifications.green = true;
        } else if (notification === '2') {
          $scope.notifications.orange = true;
        } else if (notification === '3') {
          $scope.notifications.red = true;
        }
      });
    };

    $scope.refreshNotifications();

    $scope.updateNotifications = function() {

      var updateData = [];

      angular.forEach($scope.notifications, function(value, key) {
        if(value) {
          if(key === 'green') {
            updateData.push('1');
          } else if(key === 'orange') {
            updateData.push('2');
          } else if(key === 'red') {
            updateData.push('3');
          }
        }
      });

      userService.updateEmailNotifications(updateData,
        function(result) {

          $rootScope.fractalAccountInfo.emailNotifications = result.data.user.emailNotifications;

          $scope.refreshNotifications();

        }, function(error) {
          console.log(error.error.message);
        }
      );
    };

    $scope.logOut = function() {
      var timeoutPromise = $q.defer();
      $timeout(function() {
        $auth.signOut();
        ngDialog.close();
      }, 0)
        .then(function() {
          return $timeout(function() {
            $state.go('anonymous.login');
            timeoutPromise.resolve();
          });
        });
      return timeoutPromise.promise;
    };

  });
