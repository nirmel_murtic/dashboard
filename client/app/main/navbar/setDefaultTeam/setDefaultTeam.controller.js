'use strict';

angular.module('main')
  .controller('SetdefaultteamCtrl', function ($scope, userService, $rootScope, ngDialog, apiUrl) {
    $scope.apiUrl = apiUrl;

    $scope.fractalAccount = $rootScope.fractalAccountInfo.fractalAccount;
    $scope.currentTeam = $rootScope.fractalAccountInfo.currentTeam;
    $scope.activeTeams = $scope.fractalAccount.allTeams ? $scope.fractalAccount.allTeams : $scope.fractalAccount.activeTeams;

    $scope.teamProcessing = true;

    $scope.setDefault = function(team) {
      $scope.currentTeam = team;
      $scope.selectedTeam = team;
      if($scope.fractalAccountInfo.defaultTeam.id === team.id) {
        $scope.teamProcessing = true;
        return;
      }
      $scope.teamProcessing = false;
    };

    $scope.setAsDefault = function() {
      var msg = confirm('Set '+ $scope.selectedTeam.name +' team as default?');
      if (msg) {
        $scope.teamProcessing = true;
        userService.changeDefaultTeam($scope.selectedTeam.id, function(result) {
          if(result.data.success) {
            delete $scope.teamProcessing;

            $rootScope.fractalAccountInfo.switchDefaultAndCurrentTeam($scope.currentTeam.id);

            ngDialog.close();
          }
        }, function(error) {
          console.log(error.error.message);
        });
      } else {
        console.log('false');
      }

    };
  });
