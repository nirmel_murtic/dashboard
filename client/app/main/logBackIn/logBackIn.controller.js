'use strict';

angular.module('main')
  .controller('LogBackInCtrl', function($scope, $translatePartialLoader, $auth, $rootScope, $state, fsConfirm) {
    $translatePartialLoader.addPart('app/anonymous/anonymous');
    $scope.processForm = function() {
        var loginData = {
            email: $scope.email,
            password: $scope.password
        };
        if (!$scope.email || !$scope.password){
          $scope.errorCtr = true;
        } else if ($scope.email !== $scope.originalEmail) {
          $auth.submitLogin($.param(loginData))
            .then(function() {
              // logged back in as different user
              window.location.reload();
            })
            .catch(function(err) {
              console.log(err);
              $scope.errorCtr = true;
            });
        } else {
        	$auth.submitLogin($.param(loginData))
            .then(function(result) {
              // logged back in as different user
              if(result.email !== $scope.email) {
                $scope.fsDenyIt();
                fsConfirm('genericAlert', { body : 'You have logged back into a different account!' })
                  .then(function() {
                    window.location.reload();
                  });
              }
              $rootScope.sharedLoginToken = result.jti;
              $scope.errorCtr = false;
              $scope.fsConfirmIt();
            })
            .catch(function(err) {
              console.log(err);
              $scope.errorCtr = true;
            });
        }
    };
  
  });
