'use strict';

angular.module('main')
  .config(function(fsConfirmProvider) {
  
    fsConfirmProvider.config('login', {
      templateUrl: 'app/main/logBackIn/logBackIn.html',
      controller : 'LogBackInCtrl',
      dark: true,
      dismiss : {
        click : false, // The user can't click away from the modal
        esc : false,
        routing : false
      },
      defaultParams : {
        password : '' 
      }
    });
  })
  .run(function(fsConfirm) {
    window.fsConfirm = fsConfirm;
  });
