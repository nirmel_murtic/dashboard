'use strict';

angular.module('eventLog', ['main'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.eventLog', {
        url: '/activity',
        permissions: '<LOGGED_IN>',
        templateUrl: 'app/main/eventLog/eventLog.html',
        controller: 'EventlogCtrl',
        data: {
            displayName: 'Activity',
            active: 'activity'
        },
        resolve: {
          labels: function($translatePartialLoader, $rootScope) {
            $translatePartialLoader.addPart('app/main/eventLog/eventLog');
            return $rootScope.refreshTranslations();
          }
        }
      });
  });
