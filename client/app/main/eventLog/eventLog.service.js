'use strict';

angular.module('eventLog')
  .service('EventLog', function ($http, $rootScope, apiUrl, $filter) {
    // AngularJS will instantiate a singleton by calling 'new' on this function

    return {

      // ---------------------------------------- ENDPOINTS --------------------------------------------
      getEvents: function (teamId, size, offset, sort, week) {

        sort = sort || 'createdTime:desc,priority:desc';

        var timestamp = new Date ().getTime() - (week - 1) * 604800000; // 604800000 is the number of milliseconds in one week

        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/eventlog/events/' + teamId + '?size=' + size + '&offset=' + offset + '&sort=' + sort + '&timestamp='+timestamp
        });
      },

      getMetadata: function () {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/eventlog/keys'
        });
      },

      getListOfCampaigns: function (teamId, status) {
        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/stats/manager/get/listSuperCampaign?teamId=' + teamId + '&filter=' + status
        });
      },

      getCampaignStatus: function (cmpId) {
        var accountId = 123; // Not used so we can put whatever

        return $http({
          method: 'GET',
          url: apiUrl + '/api/v2/campaign/get/super/' + accountId + '?superId=' + cmpId
        });
      },
      // ---------------------------------------- END ENDPOINTS --------------------------------------------
      // ---------------------------------------- SERVICE METHODS --------------------------------------------
      mapEventsForDisplay: function (eventsArray) {
        var eventsDisplayArray = [];

        var priorityMapping = {
          1: 'update-color',
          2: 'notice-color',
          3: 'alert-color'
        };

        function capitalize (word) {
          return $filter('capitalize')(word);
        }

        function formatCommaSeparatedValuesProperly(values) {
          if (values) {
            // Add space after the comma and capitalize
            values = values.split(',');
            values = _.map(values, capitalize);
            values = values.join(', ');

            var lastComma = values.lastIndexOf(',');
            // Replace last comma by &
            if (lastComma > 0) {
              values = values.substr(0, lastComma) + ' &' + values.substr(lastComma + 1);
            }
            return values;
          }
        }

        // Format the goal the right way, with relevant parameters according to the goal type and tracker if applicable
        function formatGoal(propertyName, goalCode, currentEventInArray) {

          var tracker, trackerProperty = '';

          // Get the attribute targeted by the goal depending on the goal type
          switch (goalCode) {
            case 'WEBSITE_CLICKS':
              // Get the tracker, capitalize it and replace the underscore by a space
              // Fault tolerant so it doesn't break if we have website clicks goals without trackers at some point
              trackerProperty = _.find(currentEventInArray.values, {'key': 'TRACKER'});
              tracker = trackerProperty ? $filter('capitalize')(trackerProperty.value).split('_').join(' ') : null;

              propertyName += ' ' + _.find(currentEventInArray.values, {'key': 'SITE'}).value;
              if (tracker) {
                propertyName += ' ' + $filter('translate')('label.eventLog.trackedby') + ' ' + tracker;
              }
              break;
            case 'WEBSITE_CONVERSIONS':
              // Get the tracker, capitalize it and replace the underscore by a space
              // Fault tolerant so it doesn't break if we have website clicks goals without trackers at some point
              trackerProperty = _.find(currentEventInArray.values, {'key': 'TRACKER'});
              tracker = trackerProperty ? $filter('capitalize')(trackerProperty.value).split('_').join(' ') : null;

              // For backward compatibility. Originally, added website conversion goals didn't have the event attached
              if (_.find(currentEventInArray.values, {'key': 'EVENT'})) {
                propertyName += ' "' + _.find(currentEventInArray.values, {'key': 'EVENT'}).value + '" for ' + _.find(currentEventInArray.values, {'key': 'SITE'}).value;
              } else {
                propertyName += ' ' + _.find(currentEventInArray.values, {'key': 'SITE'}).value;
              }
              if (tracker) {
                propertyName += ' ' + $filter('translate')('label.eventLog.trackedby') + ' ' + tracker;
              }
              break;
            case 'MOBILE_APP_ENGAGEMENT':
            case 'MOBILE_APP_INSTALLS':
              propertyName += ' ' + _.find(currentEventInArray.values, {'key': 'APPLICATION_NAME'}).value;
              break;
            case 'PAGE_LIKES':
            case 'POST_ENGAGEMENT':
            case 'VIDEO_VIEWS':
              if (!_.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}) && _.find(currentEventInArray.values, {'key': 'PAGE_NAME'}) ||
                _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}) && _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}).value === 'FACEBOOK') {
                propertyName += ' ' + _.find(currentEventInArray.values, {'key': 'PAGE_NAME'}).value;
              } else if (_.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}).value === 'TWITTER') {
                propertyName += ' ' + _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_NAME'}).value;
              }
              break;
            case 'GROW_FOLLOWERS':
              propertyName += ' ' + $filter('translate')('label.eventLog.of') + ' @' + _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_NAME'}).value;
              break;
            case 'TWEET_ENGAGEMENT':
              propertyName += ' @' + _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_NAME'}).value;
          }
          return propertyName;
        }

        for (var i = 0, j = eventsArray.length; i < j; i++) {
          var event = {}, propertyName, username, priorityColor, projectName, propertyId, platform, adAccount, pageName, channel, oldStatus, newStatus, goalCode;

          var currentEventInArray = eventsArray[i];

          // Common properties to all event types
          priorityColor = priorityMapping[currentEventInArray.priority];

          var usernameProperty = _.find(currentEventInArray.values, {'key': 'USER_FULL_NAME'}),
            projectNameProperty = _.find(currentEventInArray.values, {'key': 'PROJECT_NAME'}),
            channelProperty = _.find(currentEventInArray.values, {'key': 'CHANNEL'}),
            firstNameProperty = _.find(currentEventInArray.values, {'key': 'USER_FIRST_NAME'}),
            lastNameProperty = _.find(currentEventInArray.values, {'key': 'USER_LAST_NAME'});


          // If first name is available, assign first name and last name. If not, check for username. If not, empty string
          username = firstNameProperty ? firstNameProperty.value + ' ' + lastNameProperty.value : usernameProperty ? usernameProperty.value : $filter('translate')('label.eventLog.N/A');
          projectName = projectNameProperty ? projectNameProperty.value : $filter('translate')('label.eventLog.N/A');
          channel = channelProperty ? channelProperty.value : null;

          goalCode = _.find(currentEventInArray.values, {'key': 'GOAL_NAME'}) ? _.find(currentEventInArray.values, {'key': 'GOAL_NAME'}).value : '';

          //Errors
          var errorProperty = _.find(currentEventInArray.values, {'key': 'ERROR_MSG'});
          if (errorProperty) {
            var e = errorProperty.value;
            //cutoff everything before the first open bracket, then parse the remaining JSON
            //event.error = JSON.parse(e.slice(e.indexOf('{')));
            //Nevermind, the errors come back in lots of different formats, so we'll just leave it raw for now.
            //This may increase in complexity as we handle special errors in special ways.
            event.error = e;
          }


          var campaignManagerCampaignViewState = 'main.act.campaignManager.home.single';
          var campaignManagerSubCampaignViewState = 'main.act.campaignManager.home.single.subCampaign';
          channel = formatCommaSeparatedValuesProperly(channel);

          switch (currentEventInArray.event) {
            //--------------------- Align -----------------------
            case 'ADD_PROJECT':
              propertyName = _.find(currentEventInArray.values, {'key': 'PROJECT_NAME'}).value;
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Theproject') + ' ' + propertyName,
                status: $filter('translate')('label.eventLog.hasbeenadded')
              };
              break;
            case 'ADD_AD_ACCOUNT':
              propertyName = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value;
              platform = _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}).value.toLowerCase();

              event.message = {
                // If the ad account has a name, show it. If not, just show that an ad account is added
                preStatus: propertyName ?
                $filter('capitalize')(platform) + ' ' + $filter('translate')('label.eventLog.adaccount') + ' ' + propertyName :
                $filter('capitalize')(platform) + ' ' + $filter('translate')('label.eventLog.adaccount'),
                status: $filter('translate')('label.eventLog.hasbeenadded')
              };
              break;
            case 'AD_ACCOUNT_REMOVED':
              propertyId = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value;
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Theadaccounthasbeen', {
                  channel: channel,
                  adAccountName: propertyId
                }),
                status: $filter('translate')('label.eventLog.deleted')
              };
              break;
            case 'ADD_SOCIAL_ACCOUNT':
              propertyName = _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_NAME'}).value;
              platform = _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}).value.toLowerCase();

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Theaccount', {
                  platform: platform,
                  accountName: propertyName
                }),
                status: $filter('translate')('label.eventLog.hasbeenadded')
              };
              break;
            case 'ADD_FACEBOOK_PAGE':
              propertyName = _.find(currentEventInArray.values, {'key': 'PAGE_NAME'}).value;
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Thefacebookpage') + ' ' + propertyName,
                status: $filter('translate')('label.eventLog.hasbeenadded')
              };
              break;
            case 'FACEBOOK_PAGE_REMOVED':
              propertyId = _.find(currentEventInArray.values, {'key': 'PAGE_NAME'}).value;
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Thefacebookpagehasbeen', {pageName: propertyId}),
                status: $filter('translate')('label.eventLog.deleted')
              };
              break;
            case 'ADD_PROJECT_GOAL':
              // Get the goal name
              propertyName = _.find(currentEventInArray.values, {'key': 'GOAL_DISPLAY_NAME'}).value;
              propertyName = formatGoal(propertyName, goalCode, currentEventInArray);

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Agoalto') + ' ' + propertyName,
                status: $filter('translate')('label.eventLog.hasbeenadded')
              };
              break;
            case 'PROJECT_GOAL_REMOVED':
              propertyName = _.find(currentEventInArray.values, {'key': 'GOAL_DISPLAY_NAME'}).value;
              // Get the attribute targeted by the goal depending on the goal type
              propertyName = formatGoal(propertyName, goalCode, currentEventInArray);
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Thegoalto') + ' ' + propertyName,
                status: $filter('translate')('label.eventLog.hasbeendeleted')
              };
              break;
            case 'ADD_SITE':
              propertyName = _.find(currentEventInArray.values, {'key': 'SITE'}).value;
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Thewebsite') + ' ' + propertyName,
                status: $filter('translate')('label.eventLog.hasbeenadded')
              };
              break;
            case 'SITE_REMOVED':
              propertyId = _.find(currentEventInArray.values, {'key': 'SITE'}).value;
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Thewebsite') + ' ' + propertyName,
                status: $filter('translate')('label.eventLog.hasbeendeleted')
              };
              break;
            //--------------------- End Align -----------------------
            //--------------------- Campaign ---------------------------
            case 'CAMPAIGN_DRAFTED':
              newStatus = currentEventInArray.event.substr(currentEventInArray.event.indexOf('_') + 1).toLowerCase();
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Campaignhasbeen', {campaignName: propertyName}),
                status: newStatus,
                postStatus: '('+channel+')'
              };
              event.section = {
                state: 'main.act.campaignCreation',
                params: {
                  id: propertyId,
                  type: 'draft'
                }
              };
              break;
            case 'CAMPAIGN_DRAFT_DELETED':
              newStatus = $filter('translate')('label.eventLog.deleted');
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Campaigndrafthasbeen', {campaignName: propertyName}),
                status: newStatus
              };
              break;
            case 'CAMPAIGN_STARTED':
            case 'CAMPAIGN_ENDED':
            case 'CAMPAIGN_FAILED':
              newStatus = currentEventInArray.event.substr(currentEventInArray.event.indexOf('_') + 1).toLowerCase();
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Campaignhas', {campaignName: propertyName}),
                status: newStatus,
                postStatus: $filter('translate')('label.eventLog.on') + ' ' + channel
              };
              event.section = {
                state: campaignManagerCampaignViewState,
                params: {
                  superId: propertyId
                }
              };
              break;
            case 'CAMPAIGN_SCHEDULED':
            case 'CAMPAIGN_PAUSED':
            case 'CAMPAIGN_STOPPED':
              newStatus = currentEventInArray.event.substr(currentEventInArray.event.indexOf('_') + 1).toLowerCase();
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Campaignhasbeen', {campaignName: propertyName}),
                status: newStatus,
                postStatus: $filter('translate')('label.eventLog.on') + ' ' + channel
              };
              event.section = {
                state: campaignManagerCampaignViewState,
                params: {
                  superId: propertyId
                }
              };
              break;
            case 'CAMPAIGN_PENDING':
              newStatus = currentEventInArray.event.substr(currentEventInArray.event.indexOf('_') + 1).toLowerCase();
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Campaignis', {campaignName: propertyName}),
                status: newStatus,
                postStatus: $filter('translate')('label.eventLog.on') + ' ' + channel
              };
              event.section = {
                state: campaignManagerCampaignViewState,
                params: {
                  superId: propertyId
                }
              };
              break;
            //--------------------- End Campaign ---------------------------
            //--------------------- Sub-Campaign, Parent Campaign & Super Campaign ---------------------------
            case 'SUBCAMPAIGN_STATUS_UPDATE':
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              oldStatus = _.find(currentEventInArray.values, {'key': 'OLD_STATUS'}).value.toLowerCase();
              newStatus = _.find(currentEventInArray.values, {'key': 'NEW_STATUS'}).value;
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;

              var subCampaignId = _.find(currentEventInArray.values, {'key': 'SUBCAMPAIGN_ID'}).value;

              var subCampaignNative = _.find(currentEventInArray.values, {'key': 'SUBCAMPAIGN_NATIVE_ID'}) ? _.find(currentEventInArray.values, {'key': 'SUBCAMPAIGN_NATIVE_ID'}).value : subCampaignId;

              newStatus = newStatus.substr(newStatus.indexOf('_') + 1).toLowerCase();

              event.message = {
                preStatus: $filter('translate')('label.eventLog.AdidstatusinCampaign', {
                  subCampaignId: subCampaignNative,
                  campaignName: propertyName,
                  oldStatus: oldStatus
                }),
                status: newStatus,
                postStatus: $filter('translate')('label.eventLog.on') + ' ' + channel
              };
              event.section = {
                state: campaignManagerSubCampaignViewState,
                params: {
                  superId: propertyId,
                  subCampaignId: subCampaignId,
                  channel: channel.toLowerCase(),
                  filter: 'unknown'  // Yiran: this is very optional, adding it here only for documentation purposes,
                                     // Campaign Manager will check filters, and if not found or unknown, ask backend for it
                }
              };
              break;
            case 'CAMPAIGN_PARENT_STATUS_UPDATE':
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              oldStatus = _.find(currentEventInArray.values, {'key': 'OLD_STATUS'}).value.toLowerCase();
              newStatus = _.find(currentEventInArray.values, {'key': 'NEW_STATUS'}).value;
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;

              newStatus = newStatus.substr(newStatus.indexOf('_') + 1).toLowerCase();

              event.message = {
                preStatus: $filter('translate')('label.eventLog.StatusforCampaignchangedfromto', {
                  campaignName: propertyName,
                  oldStatus: oldStatus
                }),
                status: newStatus,
                postStatus: $filter('translate')('label.eventLog.on') + ' ' + channel
              };
              event.section = {
                state: campaignManagerCampaignViewState,
                params: {
                  superId: propertyId
                }
              };
              break;
            //--------------------- End Sub-Campaign, Parent Campaign & Super Campaign ---------------------------
            //--------------------- Custom Audience ---------------------------
            case 'CUSTOM_AUDIENCES_SENT':
              propertyName = _.find(currentEventInArray.values, {'key': 'CUSTOM_AUDIENCE_NAME'}).value;
              adAccount = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}) ?
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value :
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.CustomAudienceinadaccountis', {
                  customAudienceName: propertyName,
                  adAccountName: adAccount
                }),
                status: $filter('translate')('label.eventLog.pending'),
                postStatus: $filter('translate')('label.eventLog.onfacebook')
              };
              break;
            case 'CUSTOM_AUDIENCES_ACCEPTED':
              propertyName = _.find(currentEventInArray.values, {'key': 'CUSTOM_AUDIENCE_NAME'}).value;
              adAccount = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}) ?
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value :
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.CustomAudienceinadaccounthasbeen', {
                  customAudienceName: propertyName,
                  adAccountName: adAccount
                }),
                status: $filter('translate')('label.eventLog.accepted'),
                postStatus: $filter('translate')('label.eventLog.onfacebook')
              };
              break;
            case 'CUSTOM_AUDIENCES_DELETED':
              propertyName = _.find(currentEventInArray.values, {'key': 'CUSTOM_AUDIENCE_NAME'}).value;
              adAccount = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}) ?
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value :
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.CustomAudienceinadaccounthasbeen', {
                  customAudienceName: propertyName,
                  adAccountName: adAccount
                }),
                status: $filter('translate')('label.eventLog.deleted'),
                postStatus: $filter('translate')('label.eventLog.onfacebook')
              };
              break;
            case 'CUSTOM_AUDIENCES_REJECTED':
              propertyName = _.find(currentEventInArray.values, {'key': 'CUSTOM_AUDIENCE_NAME'}).value;
              adAccount = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}) ?
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value :
                _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_ID'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.CustomAudienceinadaccounthasbeen', {
                  customAudienceName: propertyName,
                  adAccountName: adAccount
                }),
                status: $filter('translate')('label.eventLog.rejected'),
                postStatus: $filter('translate')('label.eventLog.onfacebook')
              };
              break;
            case 'CUSTOM_AUDIENCES_UPDATED':
              propertyName = _.find(currentEventInArray.values, {'key': 'CUSTOM_AUDIENCE_NAME'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.FacebookCustomAudiencehasbeen', {
                  customAudienceName: propertyName,
                }),
                status: $filter('translate')('label.eventLog.edited')
              };
              break;
            //--------------------- End Custom Audience ---------------------------
            //--------------------- Post ---------------------------
            case 'POST_PUBLISHED':
              pageName = _.find(currentEventInArray.values, {'key': 'PAGE_NAME'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Postonhasbeen', {pageName: pageName}),
                status: $filter('translate')('label.eventLog.published'),
                postStatus: $filter('translate')('label.eventLog.on') + ' ' + channel
              };
              break;
            //--------------------- End Post ---------------------------
            //--------------------- Cpa Limit ---------------------------
            case 'CAMPAIGN_PARENT_ABOVE_CPA_LIMIT':
              propertyId = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_ID'}).value;
              var cpaLimit = _.find(currentEventInArray.values, {'key': 'CPA_LIMIT'}).value;
              var realizedCpa = _.find(currentEventInArray.values, {'key': 'REALIZED_CPA'}).value;
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.CampaigncurrentCPA', {campaignName: propertyName}) + ' (',
                status: realizedCpa,
                postStatus: ') ' + $filter('translate')('label.eventLog.hasreachedthecampaignCPAlimiton', {
                  cpaLimit: cpaLimit,
                  channel: channel
                })
              };
              event.section = {
                state: campaignManagerCampaignViewState,
                params: {
                  superId: propertyId
                }
              };
              break;
            //--------------------- End Cpa Limit ---------------------------
            case 'SOCIAL_ACCOUNT_FLAGGED':
              propertyName = _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_NAME'}).value;
              var socialAccountType = _.find(currentEventInArray.values, {'key': 'SOCIAL_ACCOUNT_TYPE'}).value;

              var preStatusMessage;
              if (socialAccountType === 'GOOGLE') {
                preStatusMessage = $filter('translate')('label.eventLog.FlaggedAccountPreGoogle', {
                  accountName: propertyName,
                  accountType: socialAccountType.toLowerCase()
                });
              } else {
                preStatusMessage = $filter('translate')('label.eventLog.FlaggedAccountPre', {
                  accountName: propertyName,
                  accountType: socialAccountType.toLowerCase()
                });
              }

              event.message = {
                preStatus: preStatusMessage,
                status: $filter('translate')('label.eventLog.FlaggedAccount'),
                postStatus: $filter('translate')('label.eventLog.FlaggedAccountPost')
              };
              break;
            case 'CAMPAIGN_PARENT_NO_CONVERSIONS':
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;

              event.message = {
                preStatus: $filter('translate')('label.eventLog.Thereis'),
                status: $filter('translate')('label.eventLog.noconversion'),
                postStatus: $filter('translate')('label.eventLog.forthecampaignontoday', {
                  campaignName: propertyName,
                  channel: channel
                })
              };
              break;
            case 'INCREASE_CAMPAIGN_BUDGET_OR_DURATION':
              propertyName = _.find(currentEventInArray.values, {'key': 'CAMPAIGN_NAME'}).value;
              var budgetPolicy = _.find(currentEventInArray.values, {'key': 'BUDGET_POLICY'}).value;
              var oldLifetimeBudget = _.find(currentEventInArray.values, {'key': 'OLD_LIFETIME_BUDGET'}) ? Number(_.find(currentEventInArray.values, {'key': 'OLD_LIFETIME_BUDGET'}).value) : 0;
              var newLifetimeBudget = _.find(currentEventInArray.values, {'key': 'NEW_LIFETIME_BUDGET'}) ? Number(_.find(currentEventInArray.values, {'key': 'NEW_LIFETIME_BUDGET'}).value) : 0;
              var oldLifetimeBudgetPerChannel = _.find(currentEventInArray.values, {'key': 'OLD_LIFETIME_BUDGET_PER_CHANNEL'}) ? angular.fromJson(_.find(currentEventInArray.values, {'key': 'OLD_LIFETIME_BUDGET_PER_CHANNEL'}).value) : 0;
              var newLifetimeBudgetPerChannel = _.find(currentEventInArray.values, {'key': 'NEW_LIFETIME_BUDGET_PER_CHANNEL'}) ? angular.fromJson(_.find(currentEventInArray.values, {'key': 'NEW_LIFETIME_BUDGET_PER_CHANNEL'}).value) : 0;

              var currency = _.find(currentEventInArray.values, {'key': 'CURRENCY'}) ? _.find(currentEventInArray.values, {'key': 'CURRENCY'}).value : '';

              // We need to divide by 1000 in order to get seconds
              var oldTime = _.find(currentEventInArray.values, {'key': 'OLD_TIME'}) ? _.find(currentEventInArray.values, {'key': 'OLD_TIME'}).value / 1000 : 0;
              var newTime = _.find(currentEventInArray.values, {'key': 'NEW_TIME'}) ? _.find(currentEventInArray.values, {'key': 'NEW_TIME'}).value / 1000 : 0;

              var budgetIncrease = 0;

              var message = '';

              if (oldLifetimeBudget < newLifetimeBudget) {
                if (budgetPolicy === 'FLOATING') {
                  budgetIncrease = newLifetimeBudget - oldLifetimeBudget;
                  message += $filter('translate')('label.eventLog.budgetwasincreasedby', {budgetIncrease: budgetIncrease, currency: currency});
                }
                // For each channel, compare the new value and old value. If new value > old value, then log it
                else if (budgetPolicy === 'FIXED') {
                  //var nbOfChannels = Object.keys(newLifetimeBudgetPerChannel).length;
                  var channelsRemaining, nbOfChannels = Object.keys(newLifetimeBudgetPerChannel).length;

                  for (channel in oldLifetimeBudgetPerChannel) {
                    if (oldLifetimeBudgetPerChannel.hasOwnProperty(channel)) {
                      if (oldLifetimeBudgetPerChannel[channel].value < newLifetimeBudgetPerChannel[channel].value) {
                        budgetIncrease = newLifetimeBudgetPerChannel[channel].value - oldLifetimeBudgetPerChannel[channel].value;
                        if (channelsRemaining === nbOfChannels) { // First one, need the full message
                          message += $filter('translate')('label.eventLog.budgetwasincreasedbyonchannel', {
                            budgetIncrease: budgetIncrease, currency:newLifetimeBudgetPerChannel[channel].currency, channel:channel});
                        } else if (channelsRemaining > 1) { // If there is one more left, display 'and' at the end of the sentence
                          message += $filter('translate')('label.eventLog.budgetwasincreasedbyonchannel2', {
                            budgetIncrease: budgetIncrease, currency:newLifetimeBudgetPerChannel[channel].currency, channel:channel});
                        } else {
                          message += $filter('translate')('label.eventLog.budgetwasincreasedbyonchannel3', { // Last channel, no 'and'
                            budgetIncrease: budgetIncrease, currency:newLifetimeBudgetPerChannel[channel].currency, channel:channel});
                        }
                        channelsRemaining--;
                      }
                    }
                  }
                }
              }
              if (oldTime && newTime) {
                if (oldLifetimeBudget < newLifetimeBudget) {
                  message += $filter('translate')('label.eventLog.andthe');
                }

                var oldTimeMoment = moment.unix(oldTime);
                var newTimeMoment = moment.unix(newTime);

                var minutes = Math.abs(newTimeMoment.diff(oldTimeMoment, 'minutes')%60);
                var hours = Math.abs(newTimeMoment.diff(oldTimeMoment, 'hours')%24);
                var days = Math.abs(newTimeMoment.diff(oldTimeMoment, 'days')%7);
                var weeks = Math.floor(Math.abs(newTimeMoment.diff(oldTimeMoment, 'days') / 7));

                message += oldTime < newTime ? $filter('translate')('label.eventLog.durationwasincreasedby') : $filter('translate')('label.eventLog.durationwasdecreasedby');
                message += weeks ? weeks + (weeks > 1 ? $filter('translate')('label.eventLog.weeks') : $filter('translate')('label.eventLog.week')) : '';
                message += days ? days + (days > 1 ? $filter('translate')('label.eventLog.days') : $filter('translate')('label.eventLog.day')) : '';
                message += hours ? hours + ( hours > 1 ? $filter('translate')('label.eventLog.hours') : $filter('translate')('label.eventLog.hour')) : '';
                message += minutes ? minutes + (minutes > 1 ? $filter('translate')('label.eventLog.minutes') : $filter('translate')('label.eventLog.minute')) : '';

              }
              event.message = {
                preStatus: $filter('translate')('label.eventLog.Campaign', {campaignName:propertyName}),
                status: message,
                postStatus: null
              };
              break;
            case 'AD_ACCOUNT_OUT_OF_FOUNDS':
              var accountId = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_ID'}).value;
              propertyName = _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}) ? _.find(currentEventInArray.values, {'key': 'AD_ACCOUNT_NAME'}).value : $filter('translate')('label.eventLog.ID:') + accountId;

              var link;
              switch (channel) {
                case 'Facebook':
                  link = 'https://www.facebook.com/ads/manager/billing/spend_limit/?act='+accountId+'&pid=p1';
              }

              event.message = {
                preStatus: $filter('translate')('label.eventLog.adAccountOutOfFundsPreStatus' , {channel:channel}),
                status: propertyName+'.',
                postStatus: $filter('translate')('label.eventLog.adAccountOutOfFundsPostStatus', {link:link})
              };

              event.externalLink = link;

              break;
          }

          event.createdTime = currentEventInArray.createdTime;
          event.username = username;
          event.priority = currentEventInArray.priority;
          event.projectName = projectName;
          event.priorityColor = priorityColor;
          event.propertyName = propertyName;
          event.propertyId = propertyId;

          // Add event to the array. Need to check the message to avoid adding empty events
          if (event.message) {
            eventsDisplayArray.push(event);
          }
        }
        return eventsDisplayArray;
      }
      // ---------------------------------------- END SERVICE METHODS --------------------------------------------

    };
  });
