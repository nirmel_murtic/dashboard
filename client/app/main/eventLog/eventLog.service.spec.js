'use strict';

describe('Service: EventLog', function () {

  // load the service's module
  beforeEach(module('eventLog'));

  // instantiate service
  var EventLog;
  beforeEach(inject(function (_EventLog_) {
      EventLog = _EventLog_;
  }));

  it('should do something', function () {
    expect(!!EventLog).toBe(true);
  });

});
