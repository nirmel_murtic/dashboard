'use strict';

describe('Controller: EventlogCtrl', function () {

  // load the controller's module
  beforeEach(module('eventLog'));

  var EventlogCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventlogCtrl = $controller('EventlogCtrl', {
      $scope: scope
    });
  }));

});
