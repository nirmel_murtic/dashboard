'use strict';

angular.module('eventLog')
    .controller('EventlogCtrl', function ($scope, EventLog, $rootScope, $state, $q, fsConfirm, $filter, $window) {

        $scope.view = {
            events: null,
            priorityCount: null,
            pagesArray: null,
            currentPage: 1,
            numberOfPages: null,
            size: 20, // Where we decide how many events we show in one page
            selectedWeek: 1, // By default show last 7 days
            weeks: [
                {
                    display: $filter('translate')('label.eventLog.dropdownWeekMessageOne'),
                    value: 1
                },
                {
                    display: $filter('translate')('label.eventLog.dropdownWeekMessageTwo'),
                    value: 2
                },
                {
                    display: $filter('translate')('label.eventLog.dropdownWeekMessageThree'),
                    value: 3
                },
                {
                    display: $filter('translate')('label.eventLog.dropdownWeekMessageFour'),
                    value: 4
                }
            ]
        };

        // Method to build the pages array
        function buildPagesArray () {
            // numberOfPagesDisplayedOnEachSideIncludingTheCurrentPage means that if we are at the 10th page (and there are a lot more pages)
            // then we will display 1...8 9 10 11 12 ... Last       8 9 10 -> 3 pages       10 11 12 -> 3 pages
            var pages = [], i, numberOfPagesDisplayedOnEachSideIncludingTheCurrentPage = 3, maxNumberOfPagesDisplayed = 5;

            function displayDotDotDotAfterFirstPageIfNecessary () {
                // // Do not add ... if the last number is 100 and the second to last is 99
                if ($scope.view.currentPage !== numberOfPagesDisplayedOnEachSideIncludingTheCurrentPage +1) {
                  pages.push('...');
                }
            }
            function displayDotDotDotBeforeLastPageIfNecessary() {
                // // Do not add ... if the last number is 100 and the second to last is 99
                if ($scope.view.currentPage !== ($scope.view.numberOfPages - numberOfPagesDisplayedOnEachSideIncludingTheCurrentPage)) {
                  pages.push('...');
                }
            }

            // All pages are displayed if the number of pages is lower than the number of pages displayed in the menu
            if ($scope.view.numberOfPages <= maxNumberOfPagesDisplayed) {
                for (i = 1; i <= $scope.view.numberOfPages; i++){
                    pages.push(i);
                }
            } // First 3 pages
            else if ($scope.view.currentPage <= numberOfPagesDisplayedOnEachSideIncludingTheCurrentPage && $scope.view.numberOfPages > maxNumberOfPagesDisplayed) {
                for (i = 1; i <= $scope.view.currentPage + 2; i++){
                    pages.push(i);
                }
                displayDotDotDotBeforeLastPageIfNecessary();
                pages.push($scope.view.numberOfPages);
            } // Last 3 pages
            else if ($scope.view.numberOfPages - $scope.view.currentPage <= numberOfPagesDisplayedOnEachSideIncludingTheCurrentPage && $scope.view.numberOfPages > maxNumberOfPagesDisplayed) {
                pages.push(1);
                displayDotDotDotAfterFirstPageIfNecessary();
                for (i = $scope.view.currentPage - 2; i <= $scope.view.numberOfPages; i++){
                    pages.push(i);
                }
            }
            // Rest of the middle pages
            else {
                pages.push(1);
                displayDotDotDotAfterFirstPageIfNecessary();
                for (i = $scope.view.currentPage - 2; i <= $scope.view.currentPage + 2; i++){
                    pages.push(i);
                }
                displayDotDotDotBeforeLastPageIfNecessary();
                pages.push($scope.view.numberOfPages);
            }

            return pages;
        }
        // Retrieve events
        $scope.getEvents = function(page) {

            var size = $scope.view.size;
            var offset = (page - 1) * size;

            var sort = $scope.view.eventsSortedBy;
            var week = $scope.view.selectedWeek;

            EventLog.getEvents(teamId, size, offset, sort, week).then(function (response) {
                var eventsResponse = response.data.data;

                $scope.view.priorityCount = eventsResponse.prioritiesBreakdown;
                $scope.view.events = EventLog.mapEventsForDisplay(eventsResponse.events);
                // Round to the upper closest integer
                $scope.view.numberOfPages = Math.ceil(($scope.view.priorityCount[1] + $scope.view.priorityCount[2] + $scope.view.priorityCount[3]) / size);

                $scope.view.currentPage = page;

                $scope.view.pagesArray = buildPagesArray();
            });
        };

        $scope.view.sortEventsBy = {
            option1: {
                label: $filter('translate')('label.eventLog.Urgency'),
                value: 'priority:desc,createdTime:desc'
            },
            option2: {
                label: $filter('translate')('label.eventLog.Recency'),
                value: 'createdTime:desc,priority:desc'
            }
        };

        // Whenever the user changes the sort filter, we call the backend to retrieve a sorted list of events
        // This also triggers the first call to get the events when the user lands on the page
        $scope.$watch('view.eventsSortedBy', function() {
          $scope.getEvents($scope.view.currentPage);
        });

        var teamId = $rootScope.fractalAccountInfo.currentTeam.id;

        // Select Time by default
        $scope.view.eventsSortedBy = $scope.view.sortEventsBy.option2.value;
        // Get the first 20 events
        //$scope.getEvents(1);

        // Retrieve the number of live campaigns for the current team
        EventLog.getListOfCampaigns(teamId, 'active').success(function(response){
            $scope.view.numberOfLiveCampaigns = response.data.data.length;
        });

        // Retrieve the campaign draft list (in order to check if the draft still exists before redirecting the user to campaign creation)
        $scope.checkIfDraftStillExists = function(section) {

            var deferred = $q.defer();

            // If we already queried the backend, the data has been cached. We can reuse it.
            if ($scope.view.draftCampaignsList) {
                deferred.resolve(_.some($scope.view.draftCampaignsList, {'superCmpId': Number(section.params.id)}));
            } else {
                EventLog.getListOfCampaigns(teamId, 'drafted').success(function(response){
                    $scope.view.draftCampaignsList = response.data.data;
                    deferred.resolve(_.some($scope.view.draftCampaignsList, {'superCmpId': Number(section.params.id)}));
                }).error(function(error){
                    deferred.reject('Couldn\'t retrieve the list of drafted campaigns: '+error);
                });
            }

            return deferred.promise;
        };

        $scope.goToPage = function (page) {
            if ( (page < $scope.view.currentPage && page >= 1) || // Previous page exists
                page > $scope.view.currentPage && page <= $scope.view.numberOfPages) { // Next page exists
                $scope.getEvents(page);
            }
        };

        $scope.handleClickOnEvent = function(event){
            if (event.error) {
                clickToOpenErrorsModal(event);
            } else if (event.externalLink) {
                $window.open(event.externalLink, '_blank');
            } else {
                clickToGoToSection(event);
            }
        };

        $scope.onWeekChange = function () {
            $scope.getEvents(1); // Start with first events for that week
        };

        function clickToGoToSection(event){
            // Check that the draft still exists
            var section = event.section;
            if (section && _.includes(section.state, 'campaignCreation')) {
                $scope.checkIfDraftStillExists(section).then(function(isDraftAlive) {
                    if (isDraftAlive) {
                        $state.go(section.state, section.params);
                    } else {
                        fsConfirm('genericAlert', {
                            body: $filter('translate')('label.eventLog.ThisDraftNoLongerExists')
                        });
                    }
                });
            } else if (section) {
                $state.go(section.state, section.params);
            }
        }

        function clickToOpenErrorsModal (event) {
            var section = event.section;

            fsConfirm('errors', {
                title: $filter('translate')('label.eventLog.Errorsforcampaign') + ' ' + event.propertyName + ':',
                body: event.error,
                yes: $filter('translate')('label.eventLog.Managethiscampaign')
            }).then( function () {
                $state.go(section.state, section.params);
            });
        }

    });
