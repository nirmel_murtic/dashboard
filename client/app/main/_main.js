'use strict';

angular.module('main', ['common', 'align', 'act', 'analyze', 'dashboard','eventLog', 'infinite-scroll',
	'library', 'angularFileUpload', 'angular-loading-bar', 'ngAnimate',
	'angularMoment', 'ngMessages', 'ngDragDrop', 'linkify', 'ng-atp', 'angularAwesomeSlider',
  'QuickList', 'ui.grid', 'ui.grid.edit', 'ui.grid.selection', 'ui.grid.rowEdit', 'ui.grid.exporter', 'ui.grid.cellNav', 'ui.grid.pagination'])
	.config(function(cfpLoadingBarProvider){
		cfpLoadingBarProvider.includeSpinner = false;
	})
	.config(function ($stateProvider) {
		$stateProvider
		.state('main', {
			url: '?feature',
			abstract: true,
			templateUrl: 'app/main/main.html',
			controller: 'MainCtrl',
			loadSocialAccounts: function(auth, teamService) {
					return teamService.getTeamSocialAccounts(auth.user.currentTeam.id, function(result) {
							auth.user.currentTeam.socialAccounts = result.data.team.socialAccounts;
					}, function(error) {
							console.log(error.error.message);
					});
			},
			resolve: {

				feature: function($stateParams, experimentalFeatures) {
					experimentalFeatures.instagram = $stateParams.feature === 'instagram';
				},

				auth : function($q, $auth, $rootScope, $location, $state, $filter, $translatePartialLoader, $translate, userService, $stateParams, teamService, projectService, pixelService) {
					var self = this;

					return $auth.resolveMain($rootScope, $location, $state, $translatePartialLoader, $translate, true).then(function() {
						if($auth.user.signedIn) {
							var user, e;
							return userService.getUser(function (result) {
								$stateParams.user = user	= result.data.user;

								user.currentTeam = user.defaultTeam;

								if (!user.firstName && !user.lastName) {
									user.firstName = user.fractalAccount.name;
								}

								user.myTeams = user.allTeams ? user.allTeams : user.activeTeams;

								user.getTeamById = function(id) {
									var result = null;

									angular.forEach(this.myTeams, function(team) {
										if(team.id === parseInt(id)) {
											result = team;
											return false;
										}
									});

									return result;
								};

								user.switchCurrentTeam = function(id) {
									this.currentTeam = this.getTeamById(id);

									$rootScope.$broadcast('switch-team', this.currentTeam);
								};

								user.switchDefaultAndCurrentTeam = function(id) {
									this.defaultTeam = this.currentTeam = this.getTeamById(id);

									$rootScope.$broadcast('switch-team', this.currentTeam);
								};

								user.switchToBestFitTeam = function() {
									if(this.defaultTeam) {
										this.switchCurrentTeam(this.defaultTeam.id);

										return;
									}

									var defaultId = null;

									for (var i=0; i<this.activeTeams.length; i++){
										if(!this.activeTeams[i].defaultTeam) {
											this.switchCurrentTeam(this.activeTeams[i].id);

											return;
										} else {
											defaultId = this.activeTeams[i].id;
										}
									}

									if(defaultId) {
										this.switchCurrentTeam(defaultId);
									}
								};

								user.removeTeamFrom = function(teamId, teamsType, fractalAccount) {
									var teams = this[teamsType];

									if(fractalAccount) {
										teams = this.fractalAccount[teamsType];
									}

									if(!teams) {
										return false;
									}

									for (var i=0; i<teams.length; i++){
										if(teams[i].id === teamId) {
											var memberIndex = teams.indexOf(teams[i]);
											if(memberIndex > -1){
												teams.splice(memberIndex, 1);

												if(!fractalAccount) {
													this.removeTeamFrom(teamId, teamsType, true);
												}

												return true;
											}
										}
									}

									return false;
								};

								$rootScope.fractalAccountInfo = user;
								$rootScope.accountTeams = user.fractalAccount.allTeams ? user.fractalAccount.allTeams : user.fractalAccount.activeTeams;

                // Listen for currentProject change and update value in database on change
                $rootScope.$watch('fractalAccountInfo.currentTeam.currentProject', function (newValue) {
                  if(newValue &&
                    $rootScope.fractalAccountInfo.currentTeam.currentProjectId !== newValue.id &&
                    newValue.teamId === $rootScope.fractalAccountInfo.currentTeam.id) {
                    teamService.setCurrentProject($rootScope.fractalAccountInfo.currentTeam.id, newValue.id, function(result) {
                      $rootScope.fractalAccountInfo.currentTeam.currentProjectId = result.data.teamMember.currentProjectId;
                    }, function(error) {
                      console.log(error.error.message);
                    });
                  }
                });

								console.log('User loaded:');
								console.log(result.data.user);
							}, function (error) {
								e = error.error;
								console.log(error.error);
							}).then(function() {
								var deferSocialAccounts = $q.defer();
								var deferProjects = $q.defer();
                var deferPixels = $q.defer();
								var accountInfo = { user: user };
								// load social accounts
								self.loadSocialAccounts(accountInfo, teamService).then(function() {
									deferSocialAccounts.resolve(accountInfo);
								}, function() {
									deferSocialAccounts.reject('Error loading social accounts');
								});
								// load projects for current team
								var teamId = accountInfo.user.currentTeam.id;
								projectService
									.getProjects(teamId, function(res) {
										//format project goal names for display
										var projects = _.map(res.data.projects, function(project){
											for(var i = 0; i < project.goals.length; i++){
												project.goals[i].formattedGoalName = $filter('fsGoalFmt')(project.goals[i]);
											}
											return project;
										});

										accountInfo.user.currentTeam.projects = projects;


                    // Set current project
                    if($rootScope.fractalAccountInfo.currentTeam.currentProjectId) {
                      $rootScope.fractalAccountInfo.currentTeam.currentProject = _.find(projects, {'id': $rootScope.fractalAccountInfo.currentTeam.currentProjectId});
                    } else if(projects.length) {
                      $rootScope.fractalAccountInfo.currentTeam.currentProject = projects[0];
                    }

										deferProjects.resolve(accountInfo);
										return accountInfo;
									}, function() {
										deferProjects.reject('Error Loading projects');
									});

                  pixelService.getPixels(teamId, function(result) {
                    accountInfo.user.currentTeam.pixels = result.data.URLS;

                    deferPixels.resolve(accountInfo);
                  }, function(error) {
                  	if(error.error && error.error.message){
                    	console.log('Error while loading pixels: ' + error.error.message);
                    } else {
                    	console.log('Error while loading pixels: ' , error);
                    }
                    // Only reason why we are not rejecting pixels in this case is to ba able to use application without pixelmapgen application deployed
                    deferPixels.resolve(accountInfo);
                  });
								return $q.all([deferSocialAccounts.promise, deferProjects.promise, deferPixels.promise]).then(function() {
									return accountInfo;
								});
							}, function() {
								return e;
							});
						} else {
							return false;
						}
					});
				}
			}
		});
	})
  .config(function(fsConfirmProvider) {
    fsConfirmProvider.config('multiLineAlert', {
      templateUrl: 'components/providers/fsConfirm/multi_line_alert.html',
      dismiss : {
        click : true,
        esc : false,
        routing : true
      }
    });
  })
	.run(function($rootScope, $document) {
		var body = angular.element( document.body );
		$rootScope.$watch('navState.open', function(open) {
			body.toggleClass('open-nav', !!open);
		});

    $document.on('click', function() {
      $rootScope.$apply(function() {
        if($rootScope.navState) {
          $rootScope.navState.open = false;
        }
      });
    });

    $rootScope.$on('$stateChangeSuccess', function() {
      if($rootScope.navState) {
        $rootScope.navState.open = false;
      }
    });
	});
