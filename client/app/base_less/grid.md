Fractal Grid System Documentation Draft
=================================

Overview
-----------

The current working draft of v2 grid system drew inspiration primarily from **Bootstrap 3**<sup>[1]</sup> and **Golden Grid System**<sup>[2]</sup>. Spiritually, we follow GGS first and rely on battle-proven Bootstrap to implement the responsive grid. Currently, the following are implemented from GGS:

* Folding columns, use 12-column grid from Bootstrap rather than GGS's suggested 18
* Elastic gutter, set column gutter to 2em as per GGS practice rather than Bootstrap's 30px
* Zoomable baseline grid, directly adopted GGS's typesetting for base html tags h1 - h4, p, ul, li, etc., with em based prop values
* normalize.css<sup>[3]</sup>, with necessary customization to make GGS baseline work properly

Getting Started Example
-----------------------------
This [Codepen](http://codepen.io/yiransheng/pen/hfFkL) implements a simple responsive design using the current grid system draft. In terms of complexity, it's nothing close to our product, it's provided here to illustrate basic usage. 

### The Design
The sample website has four components: 

1. 48px tall black and empty header
2. Main Content in the form of a document
3. Site-wise navigation with two links
4. On-page navigation with two links

Now in responsive setting, we want block 1 to always stick to the top, in other words its layout does not change based on devices, so we simply cross it out of the problem space. 

**SM**: For the sake of mobile first: we want block 2-4 to be 100% wide on mobile, following the order of 3, 4, 2 or site-wise navigation -> on-page navigation -> content. 

**MD**: On medium sized devices, we want a two column design. The left column should contain the two navigation blocks stacked vertically, with site-wise navigation on top, and the right column should contain the content. We have decided to assign 1/3 of screen space to the nav column.

**LG**: On large devices / desktops, we want a three column design. To maintain consistency, we still want the navigation blocks to take 1/3 of screen width and content occupying the remaining 2/3. We also want the site-wise navigation to be only 1/3 of the width of the on-page navigation (an arbitrary decision). 

### The Code

Now that we fully outlined the responsive behavior of the components, here's the implementation, as our grid is derived from Bootstrap less, the html marke is exactly the same how you would implement the design with Bootstrap. 
```
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12 col-md-4">
      <div class="row">
        <div class="col-lg-3">
          {Navigation on Site}
        </div>
        <div class="col-lg-9">
          {Navigation on Page}
        </div>
      </div><!-- End of .row -->
    </div><!-- End of .col-md-4 -->
    <div class="col-sm-12 col-md-8">    
      { Content }
    </div><!-- End of .col-md-8 -->
  </div>
</div>
```    
The `container-fluid` class is a top-level container that always 100% wide of its parent (in this case body), and have a 1em padding on left and right. Bootstrap's de facto standard is employed here: .container > .row > .column. We used a nested grid to implement the design. The first-level **row** contains two columns: nav and content, with corresponding class `.col-sm-12 .col-md-4` and `.col-sm-12 .col-md-8`. Take the nav column for example, the classes tell the browser to do the following:

1. On small devices and above, use a 12 column width (100% wide)
2. On medium devices and above, use a 4 column width (33.333% wide)

Rule #2 automatically overwrites rule #1 when the device is medium or larger.  With this design goal **MD** and **LG** are achieved (almost), in terms of 1/3 and 2/3 nav vs. content width proportion. 

Next we put another `.row` class inside the nav column (`.col-sm-12 .col-md-4`), and use the same principle to set the width of site-wise nav and on-page nav in a responsive fashion. We assign `.col-lg-3` and `.col-lg-9` for these two columns, specifying the width of site-wise nav should be 3/12 = 25% wide, and the width of on-page nav should be 9/12 = 75% wide on large screen. Notice, for devices smaller than large, we omitted the necessary `.col-sm-12` this time - this is due to the design of Bootstrap's mobile first philosophy, by default, columns are treated on mobile, that is 100% wide stretching full screen (standard `div` style). 

Finally, in the demo you can toggle horizontal rules/grids by double click on the page, and shift it up and down with pressing ctrl and arrow keys. Notice how each line of text falls nicely on an invisible grid - this is the effect of GGS's zoombale baseline setting. 

Documentation of Grid Less Source
-----------------------------------------

### Files

The entry point for the grid system is `bootstrap.less`, with the following contents:
```
// Core variables and mixins
@import "variables.less";
@import "mixins.less";

// Reset and dependencies
@import "normalize.less";
@import "print.less";

// Core CSS
@import "scaffolding.less";
@import "grid.less";


// Utility classes
@import "utilities.less";
```

What's included here are:

1. Global variables and mixins
2. normalize.css <sup>[3]</sup>
3. CSS scaffolding (GGS zoomable baseline is injected here)
4. Styles for print media
5. Actual grid

### Variables

Global config on media breakpoints, base font size can be found in `variables.less`. These values have all been converted into em from Bootstraps' default px values, with a baseline  `1em = 16px`. In addition, the `@screen-hg` variable has been added (117em=1872px). It's currently not used in the grid system, rather, it's here for future extension. Furthermore, it's recommended do *not* use xs (30em) as a synonym for phones as suggested by Bootstrap, 30em or 480px has been the standard screen size for older phones, but newer ones all have higher resolution. For instance, iphone 4 and iphone 5 both have a 640px (40em) screen size, so it's recommended to use *md* labeled classes for non-phone devices (768px and above).
```
//== Font Baseline Variables
// GGS-based: https://github.com/jonikorpi/Golden-Grid-System/blob/master/GGS.less
@base-font-size: 16;
@line: @base-font-size * 1.5;
@unitem: @base-font-size*1em;
@base-rem-size: unit(@base-font-size, px);

//== Media queries breakpoints
//
//## Define the breakpoints at which your layout will change, adapting to different screen sizes.

//Raw Bootstrap uses pixel based measurements, we convert all to em based

// Extra small screen / phone
//** Deprecated `@screen-xs` as of v3.0.1
@screen-xs:                  30em;
//** Deprecated `@screen-xs-min` as of v3.2.0
@screen-xs-min:              @screen-xs;
//** Deprecated `@screen-phone` as of v3.0.1
@screen-phone:               @screen-xs-min;

// Small screen / tablet
//** Deprecated `@screen-sm` as of v3.0.1
@screen-sm:                  48em;
@screen-sm-min:              @screen-sm;
//** Deprecated `@screen-tablet` as of v3.0.1
@screen-tablet:              @screen-sm-min;

// Medium screen / desktop
//** Deprecated `@screen-md` as of v3.0.1
@screen-md:                  62em;
@screen-md-min:              @screen-md;
//** Deprecated `@screen-desktop` as of v3.0.1
@screen-desktop:             @screen-md-min;

// Large screen / wide desktop
//** Deprecated `@screen-lg` as of v3.0.1
@screen-lg:                  75em;
@screen-lg-min:              @screen-lg;
//** Deprecated `@screen-lg-desktop` as of v3.0.1
@screen-lg-desktop:          @screen-lg-min;

// Huge screen 
@screen-hg:                  117em;
@screen-hg-min:              @screen-hg;

// So media queries don't overlap when required, provide a maximum
// the maximum is expressed in min - 1px, and converted to em values
.convert2em(1px);
@screen-xs-max:              (@screen-sm-min - @em);
@screen-sm-max:              (@screen-md-min - @em);
@screen-md-max:              (@screen-lg-min - @em);
@screen-lg-max:              (@screen-hg-min - @em);

```      

### Font Baselining

In `scaffolding.less`, there are the font base-line settings from GGS, with a few renaming of variables. The baseline is based on the following settings:

1. Base font-size: 16px (1em, 1rem)
2. Base line-height 1.5em (24px)

The principle in play is to config **in em** the margin-top and bottom, line-height and font-size for basic html tags: `<h1> - <h4>`, `<p>`, `<ul>`, `<ol>`, `<li>`, so that the bounding box for all of them should ideally have a height of multiples of 1.5rem, regardless of how many lines the tag contain. 

For example, the compiled css for `<h2>`:
```
h2 {
  /* 26 / 36px */
  font-size: 1.625em;
  line-height: 1.38461538em;
  margin: 0.92307692em 0 1.38461538em;
}
```
In pixel value, `<h2>` has margin-top: 24px, margin-bottom: 36px and line-height: 36px and font-size 26px. For odd number of lines inside `<h2>`, its bounding box height will always be: 2n*36px + 24px, which is a multiple of 24px(1.5rem), for even number of lines inside `<h2>` its bounding box will be a multiple of 12px, or half the height of base line-height.  

Similarly, for `<h1>` on **SM** and above:

```
h1 {
    /* 42px / 48px */
    font-size: 2.625em; /* 42px */
    line-height: 1.14285714em; /* 48px */
    margin: 1.14285714em 0 0.57142857em; /* 48px top, 24px bottom */
}
``` 

Regardless of how many lines a `<h1>` contains, its bounding box is always (n+1) * 48px + 24px, and this is a multiple of 24px(1.5rem). 

For the detailed configuration and computation, refer to the source file: `scaffolding.less` and the section on zoomable baseline. 

Responsive Base Font Size
-------------------------------

Last feature from GGS but not present in Bootstrap is responsive base font size, which is the **zoomable** part of zoomable baseline. The following lines of less achieves this:

```
@media screen and (min-width: @screen-hg) {

  body {
    /* Zoom baseline grid to 17/16 = 1.0625 */
    font-size: (@base-font-size + 1) / @unitem;
  }

}
```

For huge screens (above 1872px / 117em @ 16px), make the base font-size 17px instead of 16px. As we already convert all grid related Bootstrap variables into ems, merely adjusting base em value have no effect on the integrity of the system. Also, as all the margin/line-height/font-size for `<h1>` tags etc. are set as em values, all font baselining are maintained automatically. This is directly borrowed from GGS less source, it might not be the most optimal setting. However, with the work of joining GGS and Bootstrap so far in place, we can config base font-size whatever we want without breaking anything.  

To-dos and Further Research
----------------------------------
With the base grid system covered, here are a few things remain to be done:

* Determine whether or not to create `col-hg-*` classes for huge screens, and the proper media break points
* Determine whether 12 is the correct number of columns to use based on design and wireframes
* Responsive animations upon window resize
* Vet individual Bootstrap components' CSS and make decisions on what and how much to keep and what to discard
* Analyzing wire-frames and document layout/behavior of page components on all devices (**XS**, **SM**, **MD**, **LG**, **HG**), prototype a few of these 


Footnote
-----------
1: http://getbootstrap.com/
2: http://goldengridsystem.com 
3: http://necolas.github.io/normalize.css/

[1]: http://getbootstrap.com/
[2]: http://goldengridsystem.com 
[3]: http://necolas.github.io/normalize.css/

